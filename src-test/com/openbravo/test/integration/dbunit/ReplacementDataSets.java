/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.dbunit;

import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;

import org.dbunit.dataset.Column;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ReplacementDataSet;

/**
 * Noninstantiable utility class to handle DBUnit replacement data sets.
 *
 * @author elopio
 *
 */
public class ReplacementDataSets {

  // Suppress default constructor for noninstantiability.
  private ReplacementDataSets() {
    throw new AssertionError();
  }

  /** Regular expression that matches replacement objects. */
  private static final String REGEXP_REPLACEMENT_OBJECT = "\\[(.*?)\\]";

  /**
   * Collect all the placeholder objects of a replacementDataSet
   *
   * @param replacementDataSet
   *          The replacement data set.
   * @return A set with all the placeholder objects.
   * @throws DataSetException
   */
  private static HashSet<String> getPlaceholderObjects(ReplacementDataSet replacementDataSet)
      throws DataSetException {
    HashSet<String> placeholderObjectsSet = new HashSet<String>();
    // Iterate through all tables in the data set.
    ITableIterator tableIterator = replacementDataSet.iterator();
    while (tableIterator.next()) {
      ITable table = tableIterator.getTable();
      // Iterate through all the rows in the table.
      Column[] columns = table.getTableMetaData().getColumns();
      int rowCount = table.getRowCount();
      int rowIndex, columnIndex;
      for (rowIndex = 0; rowIndex < rowCount; rowIndex++) {
        // Iterate through all the columns in the row.
        for (columnIndex = 0; columnIndex < columns.length; columnIndex++) {
          String columnValue = (String) table.getValue(rowIndex,
              columns[columnIndex].getColumnName());
          if (columnValue != null && columnValue.matches(REGEXP_REPLACEMENT_OBJECT)) {
            placeholderObjectsSet.add(columnValue);
          }
        }
      }

    }
    return placeholderObjectsSet;
  }

  /**
   * Replace all placeholder objects with UUIDs.
   *
   * @param replacementDataSet
   *          The replacement data set.
   */
  public static void replaceWithUUIDs(ReplacementDataSet replacementDataSet)
      throws DataSetException {
    replacementDataSet.addReplacementObject("[NULL]", null);
    HashSet<String> placeholderObjectsSet = getPlaceholderObjects(replacementDataSet);
    // Replace the collected placeholder objects with UUIDs.
    replacementDataSet.setStrictReplacement(true);
    Iterator<String> replacementSetIterator = placeholderObjectsSet.iterator();
    while (replacementSetIterator.hasNext()) {
      String originalString = replacementSetIterator.next();
      replacementDataSet.addReplacementObject(originalString,
          UUID.randomUUID().toString().replace("-", ""));
    }
  }
}
