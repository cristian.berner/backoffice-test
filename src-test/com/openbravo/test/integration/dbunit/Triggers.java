/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.dbunit;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Noninstantiable utility class to handle database triggers on dbunit tests.
 *
 * @author elopio
 *
 */
public class Triggers {

  // Suppress default constructor for noninstantiability.
  private Triggers() {
    throw new AssertionError();
  }

  /**
   * Format string used to disable triggers from a postgresql database table.
   */
  private static final String FORMAT_POSTGRESQL_DISABLE_TRIGGERS = "ALTER TABLE %s DISABLE TRIGGER USER";
  /**
   * Format string used to enable triggers from a postgresql database table.
   */
  private static final String FORMAT_POSTGRESQL_ENABLE_TRIGGERS = "ALTER TABLE %s ENABLE TRIGGER USER";
  /** Format string used to disable triggers from a oracle database table. */
  private static final String FORMAT_ORACLE_DISABLE_TRIGGERS = "ALTER TABLE %s DISABLE ALL TRIGGERS";
  /** Format string used to enable triggers from a oracle database table. */
  private static final String FORMAT_ORACLE_ENABLE_TRIGGERS = "ALTER TABLE %s ENABLE ALL TRIGGERS";

  /**
   * Disable the triggers of a list of tables.
   *
   * @param connection
   *          The SQL connection handler.
   * @param tables
   *          The list of tables.
   */
  public static void disableTriggers(Connection connection, String... tables) throws SQLException {
    String database = connection.getMetaData().getDatabaseProductName();
    for (String table : tables) {
      if (database.equals("PostgreSQL")) {
        connection.prepareStatement(String.format(FORMAT_POSTGRESQL_DISABLE_TRIGGERS, table))
            .execute();
      } else if (database.equals("Oracle")) {
        connection.prepareStatement(String.format(FORMAT_ORACLE_DISABLE_TRIGGERS, table)).execute();
      } else {
        throw new SQLException("Not supported database: " + database);
      }
    }
  }

  /**
   * Enable the triggers of a list of tables.
   *
   * @param connection
   *          The SQL connection handler.
   * @param tables
   *          The list of tables.
   */
  public static void enableTriggers(Connection connection, String... tables) throws SQLException {
    String database = connection.getMetaData().getDatabaseProductName();
    for (String table : tables) {
      if (database.equals("PostgreSQL")) {
        connection.prepareStatement(String.format(FORMAT_POSTGRESQL_ENABLE_TRIGGERS, table))
            .execute();
      } else if (database.equals("Oracle")) {
        connection.prepareStatement(String.format(FORMAT_ORACLE_ENABLE_TRIGGERS, table)).execute();
      } else {
        throw new SQLException("Not supported database: " + database);
      }
    }
  }

}
