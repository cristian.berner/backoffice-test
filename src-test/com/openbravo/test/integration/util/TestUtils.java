/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      _________________________________
 ************************************************************************
 */
package com.openbravo.test.integration.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Utility methods.
 *
 * @author mtaal
 */
public class TestUtils {

  private static final String FIELD_SEPARATOR = "$";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Converts an input string like product.id to an input string that uses the correct separator:
   * 'product' + window.OB.Constants.FIELDSEPARATOR + 'id'. If the fieldName does not contain a dot,
   * then the method will only add the ' before and at the end '.
   *
   * @param fieldName
   *          The field name containing dots which are to be replaced by a reference to the client
   *          side constant for the field separator.
   * @return String with/without the field name wrapped in quotes, depending of the wrapInQuotes
   *         parameter value
   */
  public static String convertFieldSeparator(String fieldName) {
    return convertFieldSeparator(fieldName, true);
  }

  /**
   * @see #convertFieldSeparator(String)
   * @param fieldName
   *          the field name containing dots which are to be replaced by a reference to the client
   *          side constant for the field separator.
   * @param wrapInQuotes
   *          if true then the returned string is wrapped in quotes, if false then no quotes are
   *          added around the field parts
   * @return String with/without the field name wrapped in quotes, depending of the wrapInQuotes
   *         parameter value
   */
  public static String convertFieldSeparator(String fieldName, boolean wrapInQuotes) {
    final String quote = wrapInQuotes ? "'" : "";
    return quote + fieldName.replace(".", FIELD_SEPARATOR) + quote;
  }

  /**
   * Converts a numerical value, regardless of its format, to a format which only contains
   * meaningful digits when the current DB is Oracle (p.e.: input '2.00', outputs '2').
   *
   * @param val
   *          An object which contains a numerical value, which is expected to be a String type
   * @return A numerical value in String
   */
  // XXX This method is part of a workaround to avoid issue
  // https://issues.openbravo.com/view.php?id=31374
  public static Object convertNumericalValueToMeaningfulNumber(Object val) {
    Object finalValue = val;
    if (val.toString().contains(" - ")) {
      String vals = val.toString();
      String[] parts = vals.split(" - ");
      // Getting decimal separator
      char deciSep = ConfigurationProperties.INSTANCE.getDecimalSeparator();
      // Checks whether the currently used DB is Oracle
      boolean isDbOracle = ConfigurationProperties.INSTANCE.getDbName().matches("ORACLE");
      // Setting thousands separator
      char thsnSep;
      if (deciSep == '.') {
        thsnSep = ',';
      } else {
        thsnSep = '.';
      }
      for (int i = 1; i < parts.length; i++) {
        if (parts[i].contains(".") || parts[i].contains(",")) {
          // Removing thousands separator
          parts[i] = parts[i].replace(thsnSep, '\0');
          if (isDbOracle) {
            // Local variables to inform on the change
            String s1, s2;
            s1 = parts[i];
            // Removing non meaningful decimal digits (zeroes)
            int pos = parts[i].length() - 1;
            if (parts[i].charAt(pos - 2) == deciSep) {
              if (parts[i].charAt(pos) == '0') {
                parts[i] = parts[i].substring(0, pos);
                if (parts[i].charAt(pos - 1) == '0') {
                  // (pos - 2) to remove also the decimal
                  // separator (',' or '.')
                  parts[i] = parts[i].substring(0, pos - 2);
                }
              }
            }
            s2 = parts[i];
            logger.info("Changing {} by {} for Oracle number formatting", s1, s2);
          }
        }
      }

      // Joining parts
      String newVal = String.join(" - ", parts);

      // Ensuring the changes made don't affect the type of "Object" received
      if (!val.toString().equals(newVal)) {
        finalValue = val.getClass().cast(newVal);
      }
      logger.debug("String obtained after analizing for number values: {}", finalValue);
    }
    return finalValue;
  }

}
