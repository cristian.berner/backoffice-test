/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.util.format;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Helper class for number formatting.
 *
 * @author elopio
 *
 */
public class Number {

  /**
   * Apply format a number.
   *
   * @param number
   *          The number to format.
   * @param formatOutput
   *          The format to apply.
   * @param decimalSeparator
   *          The character used to separated decimals.
   * @param groupingSeparator
   *          The character to group digits.
   * @return The formatted number.
   */
  public static String format(String number, String formatOutput, char decimalSeparator,
      char groupingSeparator) {
    final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    decimalFormatSymbols.setDecimalSeparator(decimalSeparator);
    decimalFormatSymbols.setGroupingSeparator(groupingSeparator);
    final DecimalFormat decimalFormat = new DecimalFormat(formatOutput, decimalFormatSymbols);
    return decimalFormat.format(Double.parseDouble(number));
  }
}
