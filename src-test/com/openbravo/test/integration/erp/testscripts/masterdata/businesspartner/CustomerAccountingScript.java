/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner;

import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerAccountingData;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartner.BusinessPartnerWindow;
import com.openbravo.test.integration.erp.gui.masterdata.businesspartner.CustomerAccountingTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Customer Accounting tab of OpenbravoERP.
 *
 * @author rqueralta
 *
 */
class CustomerAccountingScript {

  /**
   * Verify a record.
   *
   * @param mainPage
   *          The application main page.
   * @param customerAccountingData
   *          The data of the verified record.
   */
  public static void verify(MainPage mainPage, CustomerAccountingData customerAccountingData) {
    final BusinessPartnerWindow businessPartnerWindow = (BusinessPartnerWindow) mainPage
        .getView(BusinessPartnerWindow.TITLE);

    final CustomerAccountingTab customerAccountingTab = businessPartnerWindow
        .selectCustomerAccountingTab();
    customerAccountingTab.assertData(customerAccountingData);
  }

  /**
   * Verify a record count in the tab.
   *
   * @param mainPage
   *          The application main page.
   * @param expectedCount
   *          The count of records to verify in the tab.
   */
  public static void verifyExpectedCount(MainPage mainPage, int expectedCount) {
    final BusinessPartnerWindow businessPartnerWindow = (BusinessPartnerWindow) mainPage
        .getView(BusinessPartnerWindow.TITLE);

    final CustomerAccountingTab customerAccountingTab = businessPartnerWindow
        .selectCustomerAccountingTab();
    customerAccountingTab.assertCount(expectedCount);
  }

}
