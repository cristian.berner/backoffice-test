/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelistschema;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema.PriceListSchemaHeaderData;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelistschema.HeaderTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelistschema.PriceListSchemaWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Price List Schema Header tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class PriceListSchemaHeaderScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param priceListSchemaHeaderData
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, PriceListSchemaHeaderData priceListSchemaHeaderData) {
    PriceListSchemaWindow priceListSchemaWindow = new PriceListSchemaWindow();
    mainPage.openView(priceListSchemaWindow);

    HeaderTab headerTab = priceListSchemaWindow.selectHeaderTab();
    headerTab.createRecord(priceListSchemaHeaderData);
    headerTab.assertSaved();
  }

  /**
   * Select a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param priceListSchemaHeaderData
   *          The data of the record that will be created.
   */
  static void select(MainPage mainPage, PriceListSchemaHeaderData priceListSchemaHeaderData) {
    PriceListSchemaWindow priceListSchemaWindow = new PriceListSchemaWindow();
    mainPage.openView(priceListSchemaWindow);

    HeaderTab headerTab = priceListSchemaWindow.selectHeaderTab();
    headerTab.select(priceListSchemaHeaderData);
  }

}
