/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Jonathan Bueno <jonathan.bueno@openbravo.com>,
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicValueData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the window.
 *
 * @author jbueno
 */
public class ProductCharacteristic {

  public static class ProductCharacteristicsTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param productCharacteristicData
     *
     */
    public static void create(MainPage mainPage,
        ProductCharacteristicData productCharacteristicData) {
      if (mainPage.isOnNewLayout()) {
        ProductCharacteristicScript.create(mainPage, productCharacteristicData);
      }
    }
  }

  public static class ProductCharacteristicValueTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param productCharacteristicValueData
     *
     */
    public static void create(MainPage mainPage,
        ProductCharacteristicValueData productCharacteristicValueData) {
      if (mainPage.isOnNewLayout()) {
        ProductCharacteristicScript.create(mainPage, productCharacteristicValueData);
      }
    }
  }

}
