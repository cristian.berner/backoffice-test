/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Jonathan Bueno <jonathan.bueno@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicValueData;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductCharacteristicTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductCharacteristicValueTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductCharacteristicWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Product Characteristic tab of OpenbravoERP.
 *
 * @author jbueno
 *
 */
class ProductCharacteristicScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, ProductCharacteristicData data) {
    ProductCharacteristicWindow productCharacteristicWindow = new ProductCharacteristicWindow();
    mainPage.openView(productCharacteristicWindow);

    ProductCharacteristicTab productCharacteristicTab = productCharacteristicWindow
        .selectProductCharacteristicTab();
    productCharacteristicTab.createRecord(data);
    productCharacteristicTab.assertSaved();
  }

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, ProductCharacteristicValueData data) {
    ProductCharacteristicWindow productCharacteristicWindow = new ProductCharacteristicWindow();
    mainPage.openView(productCharacteristicWindow);

    ProductCharacteristicValueTab productCharacteristicValueTab = productCharacteristicWindow
        .selectProductCharacteristicValueTab();
    productCharacteristicValueTab.createRecord(data);
    productCharacteristicValueTab.assertSaved();
  }

}
