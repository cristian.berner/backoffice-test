/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.businesspartnersetup.invoiceschedule;

import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.invoiceschedule.InvoiceScheduleData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Invoice Schedule classic window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class InvoiceSchedule {

  /**
   * Executes and verifies actions on the Invoice Schedule tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class InvoiceScheduleTab {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          Invoice Schedule data.
     */
    public static void create(MainPage mainPage, InvoiceScheduleData data) {
      if (mainPage.isOnNewLayout()) {
        InvoiceScheduleScript.create(mainPage, data);
      } else {
        InvoiceScheduleClassicScript.create(mainPage, data);
      }
    }

  }

}
