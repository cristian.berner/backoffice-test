package com.openbravo.test.integration.erp.testscripts.masterdata.pricing.pricelist;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.ProductPriceData;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListWindow;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.ProductPriceTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

public class ProductPriceScript {

  public static void verify(MainPage mainPage, ProductPriceData productPriceData) {
    final PriceListWindow productPriceWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final ProductPriceTab productPriceTab = productPriceWindow.selectProductPriceTab();
    productPriceTab.assertData(productPriceData);
  }

  static void select(MainPage mainPage, ProductPriceData data) throws OpenbravoERPTestException {
    final PriceListWindow priceListWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final ProductPriceTab productPriceTab = priceListWindow.selectProductPriceTab();
    productPriceTab.select(data);
  }

  public static void create(MainPage mainPage, ProductPriceData productPriceData) {
    final PriceListWindow priceListWindow = (PriceListWindow) mainPage
        .getView(PriceListWindow.TITLE);

    final ProductPriceTab productPriceTab = priceListWindow.selectProductPriceTab();
    productPriceTab.create(productPriceData);
  }

}
