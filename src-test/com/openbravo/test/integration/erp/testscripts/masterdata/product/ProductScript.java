/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.CharacteristicsData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.gui.masterdata.product.CharacteristicsTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductCharacteristicConfigurationTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Product tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class ProductScript {

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, ProductData data) {
    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);

    ProductTab productTab = productWindow.selectProductTab();
    productTab.createRecord(data);
    productTab.assertSaved();
  }

  /**
   * Select a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be selected.
   */

  static void select(MainPage mainPage, ProductData data) {
    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);

    ProductTab productTab = productWindow.selectProductTab();
    productTab.select(data);
  }

  /**
   * Get record count.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be counted.
   */
  static int getRecordCount(MainPage mainPage, ProductData data) {
    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);

    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(data);
    productTab.waitForDataToLoad();
    int num = productTab.getRecordCount();
    productTab.clearFilters();
    return num;
  }

  /**
   * Create a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, CharacteristicsData data) {
    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);

    CharacteristicsTab productTab = productWindow.selectCharacteristicsTab();
    productTab.createRecord(data);
    productTab.assertSaved();
  }

  /**
   * Check number of records in tab.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param count
   *          The amount of records that must exist.
   */
  static void verifyRecordNumber(MainPage mainPage, int count) {
    ProductWindow productWindow = new ProductWindow();
    mainPage.openView(productWindow);

    ProductCharacteristicConfigurationTab productTab = productWindow
        .selectCharacteristicConfigurationTab();
    productTab.assertCount(count);
  }

}
