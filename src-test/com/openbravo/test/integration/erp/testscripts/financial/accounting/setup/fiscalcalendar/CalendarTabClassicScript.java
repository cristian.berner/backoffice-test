/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.accounting.setup.fiscalcalendar;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.CalendarData;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic.CalendarTab;
import com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic.FiscalCalendarWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Calendar tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class CalendarTabClassicScript {

  /**
   * Create a calendar record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param calendarData
   *          The data of the calendar that will be created.
   */
  static void create(MainPage mainPage, CalendarData calendarData) {
    final FiscalCalendarWindow fiscalCalendarWindow = new FiscalCalendarWindow();
    mainPage.openView(fiscalCalendarWindow);

    final CalendarTab calendarTab = fiscalCalendarWindow.selectCalendarTab();
    calendarTab.create(calendarData);
  }
}
