/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.paymentmethod;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.paymentmethod.PaymentMethodData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.setup.paymentmethod.PaymentMethodTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.setup.paymentmethod.PaymentMethodWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Payment Method tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class PaymentMethodScript {

  /**
   * Create a record.
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be created.
   */
  static void create(MainPage mainPage, PaymentMethodData data) {
    PaymentMethodWindow paymentMethodWindow = new PaymentMethodWindow();
    mainPage.openView(paymentMethodWindow);

    PaymentMethodTab paymentMethodTab = paymentMethodWindow.selectPaymentMethod();
    Sleep.trySleep();
    paymentMethodTab.createRecordNoWait(data);
    paymentMethodTab.assertSaved();
  }

  /**
   * Filter and return the record count.
   *
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be filtered.
   * @return The number of record that match the selection
   */
  static int getRecordCount(MainPage mainPage, PaymentMethodData data) {
    PaymentMethodWindow paymentMethodWindow = new PaymentMethodWindow();
    mainPage.openView(paymentMethodWindow);

    PaymentMethodTab paymentMethodTab = paymentMethodWindow.selectPaymentMethod();
    Sleep.trySleep();
    paymentMethodTab.filter(data);
    return paymentMethodTab.getRecordCount();
  }
}
