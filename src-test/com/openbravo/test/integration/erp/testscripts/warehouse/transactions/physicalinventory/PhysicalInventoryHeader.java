/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.warehouse.transactions.physicalinventory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryHeaderData;
import com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory.HeaderTab;
import com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory.PhysicalInventoryWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Physical Inventory Header tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class PhysicalInventoryHeader extends SmokeTabScript<PhysicalInventoryHeaderData, HeaderTab>
    implements SmokeWindowScript<PhysicalInventoryWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public PhysicalInventoryHeader(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Physical Inventory window.
   *
   * @return this Physical Inventory Header script.
   */
  @Override
  public PhysicalInventoryHeader open() {
    PhysicalInventoryWindow window = new PhysicalInventoryWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Physical Inventory Header tab.
   *
   * Before this, the Physical Inventory window must be opened.
   *
   * @return the Physical Inventory Header tab GUI object.
   */
  @Override
  protected HeaderTab selectTab() {
    PhysicalInventoryWindow window = (PhysicalInventoryWindow) mainPage
        .getView(PhysicalInventoryWindow.TITLE);
    mainPage.getTabSetMain().selectTab(PhysicalInventoryWindow.TITLE);
    Sleep.trySleep();
    HeaderTab headerTab = (HeaderTab) window.getTopLevelTab();
    headerTab.waitUntilSelected();
    return headerTab;
  }

  /**
   * Create Inventory Count List.
   *
   * @param data
   *          The data of the Inventory Count that will be create.
   * @param inserted
   *          The number of records inserted.
   * @param updated
   *          The number of records updated
   */
  public void createInventoryCountList(CreateInventoryCountListData data, int inserted,
      int updated) {
    logger.info("Create inventory count list with data: {}.", data);
    HeaderTab tab = selectTab();
    tab.createInventoryCountList(data);
    tab.verifyProcessCompletedSuccessfully(inserted, updated);
  }

  /**
   * Process the Inventory Count.
   *
   * @throws OpenbravoERPTestException
   */
  public void processInventoryCount() throws OpenbravoERPTestException {
    logger.info("Process inventory count list.");
    HeaderTab tab = selectTab();
    tab.process();

    // TODO: Check what can be done to avoid reading the message left by the previous process.
    Sleep.trySleep();
    // TODO see issue 17291 - https://issues.openbravo.com/view.php?id=17291
    // tab.assertProcessCompletedSuccessfully();
  }
}
