/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.warehouse.transactions.physicalinventory;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryHeaderData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the User classic window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class PhysicalInventory {

  /**
   * Executes and verifies actions on the User tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class Header {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          Physical Inventory Header data
     */
    public static void create(MainPage mainPage, PhysicalInventoryHeaderData data) {
      PhysicalInventoryHeaderClassicScript.create(mainPage, data);
    }

    /**
     * Create Inventory Count List.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          The data of the Inventory Count that will be create.
     * @param inserted
     *          The number of records inserted.
     * @param updated
     *          The number of records updated
     */
    public static void createInventoryCountList(MainPage mainPage,
        CreateInventoryCountListData data, int inserted, int updated) {
      PhysicalInventoryHeaderClassicScript.createInventoryCountList(mainPage, data, inserted,
          updated);
    }

    /**
     * Process the Inventory Count.
     *
     * @param mainPage
     *          The application main page.
     * @throws OpenbravoERPTestException
     * @throws OpenbravoERPTestException
     */
    public static void processInventoryCount(MainPage mainPage) throws OpenbravoERPTestException {
      PhysicalInventoryHeaderClassicScript.processInventoryCount(mainPage);
    }
  }

  /**
   * Executes and verifies actions on the User tab of OpenbravoERP.
   *
   * @author plujan
   *
   */
  public static class Lines {

    /**
     * Creates the record.
     *
     * @param mainPage
     *          The application main page.
     * @param data
     *          Line data
     * @throws OpenbravoERPTestException
     */
    public static void edit(MainPage mainPage, PhysicalInventoryLinesData data)
        throws OpenbravoERPTestException {
      PhysicalInventoryLinesClassicScript.edit(mainPage, data);
    }

  }
}
