/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation;

import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.gui.warehouse.transactions.stockreservation.ReservationTab;
import com.openbravo.test.integration.erp.gui.warehouse.transactions.stockreservation.StockReservationWindow;
import com.openbravo.test.integration.erp.gui.warehouse.transactions.stockreservation.StockTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Stock Reservation window.
 *
 * @author aferraz
 */
public class StockReservation extends SmokeTabScript<ReservationData, ReservationTab>
    implements SmokeWindowScript<StockReservationWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public StockReservation(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Stock Reservation window.
   *
   * @return this Stock Reservation script.
   */
  @Override
  public StockReservation open() {
    StockReservationWindow window = new StockReservationWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Reservation tab.
   *
   * Before this, the Stock Reservation window must be opened.
   *
   * @return the Reservation tab GUI object.
   */
  @Override
  protected ReservationTab selectTab() {
    StockReservationWindow window = (StockReservationWindow) mainPage
        .getView(StockReservationWindow.TITLE);
    mainPage.getTabSetMain().selectTab(StockReservationWindow.TITLE);
    ReservationTab tab = (ReservationTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Executes and verifies actions on the Stock Reservation Stock tab.
   *
   * @author aferraz
   *
   */
  public class Stock extends SmokeTabScript<StockData, StockTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Stock(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Stock tab.
     *
     * Before this, the Stock Reservation window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Reservation tab GUI object.
     */
    @Override
    protected StockTab selectTab() {
      StockReservationWindow window = (StockReservationWindow) mainPage
          .getView(StockReservationWindow.TITLE);
      StockTab tab = (StockTab) window.selectTab(StockTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }
}
