/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts;

/**
 * This class is used to listen failures in tests
 * @author lorenzo.fidalgo
 *
 */

import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;

public class FailureListener extends RunListener {

  private RunNotifier runNotifier;

  public FailureListener(RunNotifier runNotifier) {
    super();
    this.runNotifier = runNotifier;
  }

  @Override
  public void testFailure(Failure failure) throws Exception {
    super.testFailure(failure);
    // pleaseStop() method induces the test that is being run stops before starting the next test.
    this.runNotifier.pleaseStop();
  }
}
