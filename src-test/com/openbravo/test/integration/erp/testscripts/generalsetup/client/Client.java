/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.client;

import com.openbravo.test.integration.erp.data.generalsetup.client.client.ClientData;
import com.openbravo.test.integration.erp.gui.general.client.ClientHeaderTab;
import com.openbravo.test.integration.erp.gui.general.client.ClientWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 *
 * @author nonofce
 *
 */
public class Client extends SmokeTabScript<ClientData, ClientHeaderTab>
    implements SmokeWindowScript<ClientWindow> {

  public Client(MainPage mainPage) {
    super(mainPage);
  }

  @Override
  public SmokeWindowScript<ClientWindow> open() {
    final ClientWindow clientWindow = new ClientWindow();
    mainPage.openView(clientWindow);
    return this;
  }

  @Override
  protected ClientHeaderTab selectTab() {

    ClientWindow clientWindow = (ClientWindow) mainPage.getView(ClientWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ClientWindow.TITLE);
    ClientHeaderTab tab = (ClientHeaderTab) clientWindow.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }
}
