/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.intanceactivation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.ActivateOnlineData;
import com.openbravo.test.integration.erp.gui.general.application.instanceactivation.InstanceActivationForm;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Instance Activation form of OpenbravoERP.
 *
 * @author elopio
 */
public class InstanceActivation {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Activate the instance online.
   *
   * @param mainPage
   *          The application main page.
   * @param activateOnlineData
   *          The data for online activation.
   * @return true if the instance was activated. Otherwise, false.
   */
  public static boolean activateInstanceOnline(MainPage mainPage,
      ActivateOnlineData activateOnlineData) {
    InstanceActivationForm instanceActivationForm = new InstanceActivationForm();
    mainPage.openView(instanceActivationForm);

    boolean activated = false;
    if (!instanceActivationForm.isActive()) {
      logger.info("Activate instance.");
      instanceActivationForm.activateInstanceOnline(activateOnlineData);

      instanceActivationForm.verifyProcessCompletedSuccessfully2();
      instanceActivationForm.verifyActiveStatus();
      activated = true;
    } else {
      logger.info("Instance is already activated.");
    }
    return activated;
  }

  /**
   * Reactivate the instance online.
   *
   * @param mainPage
   *          The application main page.
   * @param activateOnlineData
   *          The data for online activation.
   */
  public static void reactivateInstanceOnline(MainPage mainPage,
      ActivateOnlineData activateOnlineData) {
    InstanceActivationForm instanceActivationForm = new InstanceActivationForm();
    mainPage.openView(instanceActivationForm);

    instanceActivationForm.activateInstanceOnline(activateOnlineData);
    instanceActivationForm.verifyProcessCompletedSuccessfully2();
    instanceActivationForm.verifyActiveStatus();
  }

  /**
   * Deactivate the instance.
   *
   * @param mainPage
   *          The application main page.
   * @return true if the instance was deactivated. Otherwise, false.
   */
  public static boolean deactivateInstance(MainPage mainPage) {
    InstanceActivationForm instanceActivationForm = new InstanceActivationForm();
    mainPage.openView(instanceActivationForm);

    boolean deactivated = false;
    if (instanceActivationForm.isActive()) {
      logger.info("Deactivate instance.");
      instanceActivationForm.deactivateInstance();

      instanceActivationForm.verifyProcessCompletedSuccessfully2();
      instanceActivationForm.verifyInactiveStatus();
      deactivated = true;
    } else {
      logger.info("Instance is already deactivated.");
    }
    return deactivated;
  }
}
