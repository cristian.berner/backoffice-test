/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.AddModulesFormTab;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.InstallModulePopUp;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.ModuleManagementWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Add Modules form tab of OpenbravoERP.
 *
 * @author elopio
 */
class AddModulesTabScript {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Install a module from central repository.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param module
   *          The name of the module.
   * @return true if the module was installed. Otherwise, false.
   */
  static boolean installModuleFromCentralRepository(MainPage mainPage, ModuleReferenceData module) {
    logger.info("Installing module {} from Central Repository", module);

    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);

    AddModulesFormTab addModulesTab = moduleManagementWindow.selectAddModulesTab();

    addModulesTab.searchModule(module.getName());

    boolean installed = false;
    // XXX this doesn't imply that the module is already installed.
    // TODO send the data object to the functions.
    if (addModulesTab.isModuleAvailable(module.getName())) {
      addModulesTab.getModuleVersion(module.getName());
      addModulesTab.clickInstallNow(module.getName());
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      final InstallModulePopUp installModulePopUp = new InstallModulePopUp();
      // XXX the heart beat has to be enabled. Otherwise an additional
      // screen
      // is displayed.
      installModulePopUp.confirmModulesToInstall();
      installModulePopUp.acceptAllLicenseAgreements();
      // FIXME the message box is not being verified.
      // see issue 11771 https://issues.openbravo.com/view.php?id=11771
      // A new function for packs installation has to be created.
      installModulePopUp.clickOK();

      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      addModulesTab.waitForFrame();
      installed = true;
    } else {
      logger.info("Module was not available for installation.");
    }
    return installed;
  }

  /**
   * Install a module from central repository.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param module
   *          The name of the module.
   * @return true if the module was installed. Otherwise, false.
   */
  static boolean installModuleFromFileSystem(MainPage mainPage, ModuleReferenceData module) {
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);

    AddModulesFormTab addModulesTab = moduleManagementWindow.selectAddModulesTab();
    // TODO L2: Remove the following sleep once Add Modules tab has no more the performance issue.
    // Workaround with sleeps for now
    Sleep.trySleep(15000);

    logger.info("Install the module {}.", module::getObxPath);

    new MessageBox();
    boolean installed = false;
    // XXX this doesn't imply that the module is already installed.
    // TODO send the data object to the functions.
    if ((new File(module.getObxPath())).exists()) {
      addModulesTab.waitForDataToLoad();
      // TODO L2: Remove the following sleep once Add Modules tab has no more the performance issue.
      // Workaround with sleeps for now
      Sleep.trySleep(6000);
      addModulesTab.clickBrowseFileSystem();
      // TODO L2: Remove the following sleep once Add Modules tab has no more the performance issue.
      // Workaround with sleeps for now
      Sleep.trySleep(6000);
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      // TODO L2: Remove the following sleep once Add Modules tab has no more the performance issue.
      // Workaround with sleeps for now
      Sleep.trySleep(6000);
      final InstallModulePopUp installModulePopUp = new InstallModulePopUp();
      installModulePopUp.selectModuleFile(module.getObxPath());
      // XXX the heart beat has to be enabled. Otherwise an additional
      // screen
      // is displayed.
      installModulePopUp.confirmModulesToInstall();
      installModulePopUp.acceptAllLicenseAgreements();
      // FIXME the message box is not being verified.
      // see issue 11771 https://issues.openbravo.com/view.php?id=11771
      // A new function for packs installation has to be created.
      installModulePopUp.clickOK();

      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      installed = true;
    } else {
      logger.info("Module file ({}) was not found.", module.getObxPath());
    }
    return installed;
  }

}
