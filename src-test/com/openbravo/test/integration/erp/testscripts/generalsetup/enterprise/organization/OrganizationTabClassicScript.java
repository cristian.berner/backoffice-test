/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.organization;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.classic.OrganizationTab;
import com.openbravo.test.integration.erp.gui.general.enterprise.organization.classic.OrganizationWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Organization tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class OrganizationTabClassicScript {

  /**
   * Select a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param organizationData
   *          The record to select.
   * @throws OpenbravoERPTestException
   */
  static void select(MainPage mainPage, OrganizationData organizationData)
      throws OpenbravoERPTestException {
    final OrganizationWindow organizationWindow = new OrganizationWindow();
    mainPage.openView(organizationWindow);

    final OrganizationTab organizationTab = organizationWindow.selectOrganizationTab();
    organizationTab.clickRecordWithText(OrganizationTab.COLUMN_NAME,
        organizationData.getDataField("name").toString());
  }

  /**
   * Edit a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The edited data.
   * @throws OpenbravoERPTestException
   */
  static void edit(MainPage mainPage, OrganizationData data) throws OpenbravoERPTestException {
    final OrganizationWindow organizationWindow = new OrganizationWindow();
    mainPage.openView(organizationWindow);

    final OrganizationTab organizationTab = organizationWindow.selectOrganizationTab();
    organizationTab.edit(data);
    organizationTab.verifyUpdate();
  }

  /**
   * Set the organization as ready.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @throws OpenbravoERPTestException
   */
  static void setAsReady(MainPage mainPage) throws OpenbravoERPTestException {
    final OrganizationWindow organizationWindow = new OrganizationWindow();
    mainPage.openView(organizationWindow);

    final OrganizationTab organizationTab = organizationWindow.selectOrganizationTab();
    organizationTab.setReady();
    organizationWindow.verifyProcessCompletedSuccessfully();
  }

}
