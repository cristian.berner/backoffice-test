/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.classic.HeartbeatConfigurationPopUp;
import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.classic.HeartbeatConfigurationTab;
import com.openbravo.test.integration.erp.gui.general.application.heartbeatconfiguration.classic.HeartbeatConfigurationWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on the Heartbeat Configuration tab of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
class HeartbeatConfigurationTabClassicScript {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Enable the heartbeat.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   */
  static void enableHeartbeat(MainPage mainPage) {
    final HeartbeatConfigurationWindow heartbeatConfigurationWindow = new HeartbeatConfigurationWindow();
    mainPage.openView(heartbeatConfigurationWindow);

    final HeartbeatConfigurationTab heartbeatConfigurationTab = heartbeatConfigurationWindow
        .selectHeartbeatConfigurationTab();

    // TODO we are not verifying that both buttons are displayed.
    if (!heartbeatConfigurationTab.isEnabled()) {
      logger.info("Enable Heartbeat.");
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      heartbeatConfigurationTab.changeHeartbeatStatus();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      final HeartbeatConfigurationPopUp heartbeatConfigurationPopUp = new HeartbeatConfigurationPopUp();
      heartbeatConfigurationPopUp.assertProcessCompletedSuccessfully();
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      heartbeatConfigurationWindow.waitForFrame();
      heartbeatConfigurationWindow.waitUntilViewIsVisible();
      heartbeatConfigurationTab.verifyEnabledStatus();
    } else {
      logger.info("Heartbeat is already enabled.");
    }
  }

  /**
   * Deactivate the hearbeat.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   */
  static void disableHeartbeat(MainPage mainPage) {
    final HeartbeatConfigurationWindow heartbeatConfigurationWindow = new HeartbeatConfigurationWindow();
    mainPage.openView(heartbeatConfigurationWindow);

    final HeartbeatConfigurationTab heartbeatConfigurationTab = heartbeatConfigurationWindow
        .selectHeartbeatConfigurationTab();

    if (heartbeatConfigurationTab.isEnabled()) {
      logger.info("Disable Heartbeat.");
      String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      heartbeatConfigurationTab.changeHeartbeatStatus();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      final HeartbeatConfigurationPopUp heartbeatConfigurationPopUp = new HeartbeatConfigurationPopUp();
      heartbeatConfigurationPopUp.assertProcessCompletedSuccessfully();
      SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
      heartbeatConfigurationWindow.waitForFrame();
      heartbeatConfigurationWindow.waitUntilViewIsVisible();
    } else {
      logger.info("Heartbeat is already disabled.");
    }
  }

  /**
   * Checks that Hearbeat is disabled
   *
   * @param mainPage
   *          The application main page.
   */
  public static void verifyDisabled(MainPage mainPage) {
    logger.debug("Starting verifying method.");
    final HeartbeatConfigurationWindow heartbeatConfigurationWindow = new HeartbeatConfigurationWindow();
    logger.debug("Opening view.");
    mainPage.openView(heartbeatConfigurationWindow);

    final HeartbeatConfigurationTab heartbeatConfigurationTab = heartbeatConfigurationWindow
        .selectHeartbeatConfigurationTab();

    // TODO: Enrich the verification. Check that proper buttons are displayed.
    logger.debug("Checking status.");
    heartbeatConfigurationTab.verifyDisabledStatus();
  }
}
