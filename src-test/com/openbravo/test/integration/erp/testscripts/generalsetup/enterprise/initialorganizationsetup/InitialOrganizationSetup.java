/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Luján <plu@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Arun kumar<arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.initialorganizationsetup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.initialorganizationsetup.InitialOrganizationSetupData;
import com.openbravo.test.integration.erp.gui.general.enterprise.initialorganizationsetup.InitialOrganizationSetupProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Test the Initial Organization Setup form of Openbravo.
 *
 * @author Arunkumar
 */
public class InitialOrganizationSetup {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Setup the Initial Client.
   *
   * @param mainPage
   *          The application main page.
   * @param initialOrganizationSetupData
   *          The initial organization setup data.
   * @throws OpenbravoERPTestException
   */
  public static void setupInitialOrganization(MainPage mainPage,
      InitialOrganizationSetupData initialOrganizationSetupData) throws OpenbravoERPTestException {
    InitialOrganizationSetupProcess initialOrganizationSetupProcess = new InitialOrganizationSetupProcess();
    mainPage.openView(initialOrganizationSetupProcess);

    logger.info("Set up the organization:\n{}", initialOrganizationSetupData::toString);

    initialOrganizationSetupProcess.setUp(initialOrganizationSetupData);
    initialOrganizationSetupProcess.verifyProcessCompletedSuccessfully2();

    // final Organization organizationWindow = new Organization();
    // organizationWindow.clickRecordWithText(Organization.COLUMN_NAME, initialOrganizationSetupData
    // .getOrganization());

    // FIXME all these verifications should be moved somewhere else.
    // verify that the user has been created
    // ClassicFormView userView = new ClassicFormView("User", Menu.GENERAL_SETUP, Menu.SECURITY,
    // Menu.USER);
    // mainPage.openView(userView);

    // final User user = new User();
    // user.clickRecordWithText(User.COLUMN_NAME, initialOrganizationSetupData
    // .getOrganizationUserName());

    // verify that the accounting schema has been created
    // ClassicFormView accountingSchemaView = new ClassicFormView("Accounting Schema",
    // Menu.FINANCIAL_MANAGEMENT, Menu.ACCOUNTING, Menu.ACCOUNTING_SETUP, Menu.ACCOUNTING_SCHEMA);
    // mainPage.openView(accountingSchemaView);

    /*
     * final AccountingSchema accountingSchema = new AccountingSchema();
     * accountingSchema.clickRecordWithText(AccountingSchema.COLUMN_NAME,
     * initialOrganizationSetupData .getOrganization() + " US/A/" +
     * initialOrganizationSetupData.getInitialSetupData().getCurrency()); // FIXME who sets the
     * currency ISO code? // accountingSchema.verify(initialOrganizationSetupData.getOrganization(),
     * currencyISOCode);
     *
     * accountingSchema.selectTab(AccountingSchemaElement.TAB_ACCOUNTING_SCHEMA_ELEMENT); final
     * AccountingSchemaElement accountingShemaElement = new AccountingSchemaElement();
     * accountingShemaElement.switchToGridView(); assertThat("Number of accounting schema elements",
     * accountingShemaElement.getRowCount(), is(not(0)));
     *
     * accountingShemaElement.selectTab(AccountingSchemaDefaults.TAB_DEFAULTS); final
     * AccountingSchemaDefaults accountingSchemaDefaults = new AccountingSchemaDefaults();
     * accountingSchemaDefaults.switchToGridView();
     * assertThat("Number of accounting schema defaults", accountingSchemaDefaults.getRowCount(),
     * is(equalTo(1)));
     *
     * ClassicFormView accoutingTreeView = new ClassicFormView("Account Tree",
     * Menu.FINANCIAL_MANAGEMENT, Menu.ACCOUNTING, Menu.ACCOUNTING_SETUP, Menu.ACCOUNTING_TREE);
     * mainPage.openView(accoutingTreeView);
     *
     * final AccountingTreeElement accountingTreeElement = new AccountingTreeElement();
     * accountingTreeElement.switchToGridView(); assertThat("Number of accounting tree elements",
     * accountingTreeElement.getRowCount(), is(equalTo(1)));
     * accountingTreeElement.verify(initialOrganizationSetupData.getOrganization());
     *
     *
     *
     *
     *
     *
     *
     *
     * accountingTreeElement.selectTab(AccountingTreeElementValue.TAB_ACCOUNTING_TREE_ELEMENT_VALUE)
     * ;
     */

    // TODO check the accounts are the same as in the COA
  }
}
