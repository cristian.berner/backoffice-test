/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.role;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.WindowAccessData;
import com.openbravo.test.integration.erp.gui.general.security.role.RoleWindow;
import com.openbravo.test.integration.erp.gui.general.security.role.WindowAccessTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Window Access tab of OpenbravoERP.
 *
 * @author inaki_garcia
 */
class WindowAccessScript {

  /**
   * Creates the window access.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param windowAccessData
   *          The window access data.
   */
  static void create(MainPage mainPage, WindowAccessData windowAccessData) {
    RoleWindow roleWindow = (RoleWindow) mainPage.getView(RoleWindow.TITLE);

    WindowAccessTab windowAccessTab = roleWindow.selectWindowAccessTab();
    windowAccessTab.createOnForm(windowAccessData);
    windowAccessTab.assertSaved();
  }

}
