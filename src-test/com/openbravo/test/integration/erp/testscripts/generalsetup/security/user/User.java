/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.security.user;

import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserRolesData;
import com.openbravo.test.integration.erp.gui.general.security.user.UserRolesTab;
import com.openbravo.test.integration.erp.gui.general.security.user.UserTab;
import com.openbravo.test.integration.erp.gui.general.security.user.UserWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the User window of OpenbravoERP.
 *
 * @author Pablo Lujan
 */
public class User extends SmokeTabScript<UserData, UserTab>
    implements SmokeWindowScript<UserWindow> {

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public User(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the User window.
   *
   * @return the opened User window.
   */
  @Override
  public User open() {
    UserWindow window = new UserWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the User tab.
   *
   * Before this, the User window must be opened.
   *
   * @return the User tab GUI object.
   */
  @Override
  protected UserTab selectTab() {
    UserWindow window = (UserWindow) mainPage.getView(UserWindow.TITLE);
    mainPage.getTabSetMain().selectTab(UserWindow.TITLE);
    UserTab tab = (UserTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Executes and verifies actions on the User Roles tab.
   *
   * @author elopio
   *
   */
  public class UserRoles extends SmokeTabScript<UserRolesData, UserRolesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public UserRoles(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the User Roles tab.
     *
     * Before this, the User window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the User Roles tab GUI object.
     */
    @Override
    protected UserRolesTab selectTab() {
      UserWindow window = (UserWindow) mainPage.getView(UserWindow.TITLE);
      UserRolesTab tab = (UserRolesTab) window.selectTab(UserRolesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
