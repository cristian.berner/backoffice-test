/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.gui.general.application.RebuildSystemPopUp;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.InstalledModulesFormTab;
import com.openbravo.test.integration.erp.gui.general.application.modulemanagement.ModuleManagementWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on the Installed Modules form tab of OpenbravoERP.
 *
 * @author elopio
 */
class InstalledModulesTabScript {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Enable all installed modules.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   */
  static void enableAllModules(MainPage mainPage) {
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);

    InstalledModulesFormTab installedModulesTab = moduleManagementWindow
        .selectInstalledModulesTab();

    logger.info("Enable all modules.");
    installedModulesTab.enableAllModules();
  }

  /**
   * Disable all installed modules.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   */
  static void disableModules(MainPage mainPage) {
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);

    InstalledModulesFormTab installedModulesTab = moduleManagementWindow
        .selectInstalledModulesTab();

    logger.info("Disable all modules, except Core.");
    installedModulesTab.disableAllModules();
  }

  /**
   * Rebuild the system and restart the servlet container.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   */
  static void rebuildSystemAndRestartServletContainer(MainPage mainPage) {
    ModuleManagementWindow moduleManagementWindow = new ModuleManagementWindow();
    mainPage.openView(moduleManagementWindow);

    InstalledModulesFormTab installedModulesTab = moduleManagementWindow
        .selectInstalledModulesTab();

    logger.info("Rebuild the system.");
    // FIXME what if rebuild is not available?
    installedModulesTab.clickRebuild();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final RebuildSystemPopUp rebuildSystemPopUp = new RebuildSystemPopUp();
    rebuildSystemPopUp.rebuildTheSystem();
    logger.info("Restart the servlet container.");
    rebuildSystemPopUp.restartServletContainer();

    // TODO who waits for the servlet to be restarted?
  }
}
