/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostCostData;
import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostReceiptData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost.LandedCostCostTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost.LandedCostHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost.LandedCostReceiptTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost.LandedCostWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class LandedCost extends SmokeTabScript<LandedCostHeaderData, LandedCostHeaderTab>
    implements SmokeWindowScript<LandedCostWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public LandedCost(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public LandedCost open() {
    final LandedCostWindow window = new LandedCostWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected LandedCostHeaderTab selectTab() {
    final LandedCostWindow window = (LandedCostWindow) mainPage.getView(LandedCostWindow.TITLE);
    mainPage.getTabSetMain().selectTab(LandedCostWindow.TITLE);
    final LandedCostHeaderTab tab = (LandedCostHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Complete the record.
   *
   */
  public void process() {
    logger.info("Processing the landed cost.");
    final LandedCostHeaderTab tab = selectTab();
    tab.process();
  }

  /**
   * Executes and verifies actions on the Landed Cost - Cost tab.
   *
   * @author plujan
   *
   */
  public class Cost extends SmokeTabScript<LandedCostCostData, LandedCostCostTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Cost(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab. SalesInvoiceWindow Before this, the Sales Order window must
     * be opened and this tab has to be accessible from the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected LandedCostCostTab selectTab() {
      final LandedCostWindow window = (LandedCostWindow) mainPage.getView(LandedCostWindow.TITLE);
      final LandedCostCostTab tab = (LandedCostCostTab) window
          .selectTab(LandedCostCostTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Receipt tab.
   *
   * @author plujan
   *
   */
  public class Receipt extends SmokeTabScript<LandedCostReceiptData, LandedCostReceiptTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Receipt(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Payment In Plan tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected LandedCostReceiptTab selectTab() {
      LandedCostWindow window = (LandedCostWindow) mainPage.getView(LandedCostWindow.TITLE);
      LandedCostReceiptTab tab = (LandedCostReceiptTab) window
          .selectTab(LandedCostReceiptTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }
}
