/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.ManageRequisitionsHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.ManageRequisitionsLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.MatchedPOLinesData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions.ManageRequisitionsHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions.ManageRequisitionsLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions.ManageRequisitionsWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.managerequisitions.MatchedPOLinesTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class ManageRequisitions
    extends SmokeTabScript<ManageRequisitionsHeaderData, ManageRequisitionsHeaderTab>
    implements SmokeWindowScript<ManageRequisitionsWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ManageRequisitions(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public ManageRequisitions open() {
    final ManageRequisitionsWindow window = new ManageRequisitionsWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected ManageRequisitionsHeaderTab selectTab() {
    final ManageRequisitionsWindow window = (ManageRequisitionsWindow) mainPage
        .getView(ManageRequisitionsWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ManageRequisitionsWindow.TITLE);
    final ManageRequisitionsHeaderTab tab = (ManageRequisitionsHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Create a purchase order.
   *
   * @param orderDate
   *          The date of the order.
   * @param organization
   *          The name of the organization.
   * @param warehouse
   *          The name of the warehouse.
   */
  public void createPurchaseOrder(String orderDate, String organization, String warehouse) {
    logger.info("Create purchase order.");
    final ManageRequisitionsHeaderTab tab = selectTab();
    tab.createPurchaseOrder(orderDate, organization, warehouse);
  }

  /**
   * Assert that the order creation message was displayed.
   *
   */
  public void assertOrderCreateSuccessfully() {
    final ManageRequisitionsHeaderTab tab = selectTab();
    tab.assertOrderCreateSuccessfully();
  }

  /**
   * Executes and verifies actions on the Manage Requisitions Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines
      extends SmokeTabScript<ManageRequisitionsLinesData, ManageRequisitionsLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Manage Requisitions window must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected ManageRequisitionsLinesTab selectTab() {
      final ManageRequisitionsWindow window = (ManageRequisitionsWindow) mainPage
          .getView(ManageRequisitionsWindow.TITLE);
      final ManageRequisitionsLinesTab tab = (ManageRequisitionsLinesTab) window
          .selectTab(ManageRequisitionsLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Executes and verifies actions on the Matched PO Lines tab.
     *
     * @author elopio
     *
     */
    public class MatchedPOLines extends SmokeTabScript<MatchedPOLinesData, MatchedPOLinesTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public MatchedPOLines(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Lines tab.
       *
       * Before this, the Manage Requisitions window must be opened and this tab has to be
       * accessible from the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected MatchedPOLinesTab selectTab() {
        final ManageRequisitionsWindow window = (ManageRequisitionsWindow) mainPage
            .getView(ManageRequisitionsWindow.TITLE);
        final MatchedPOLinesTab tab = (MatchedPOLinesTab) window
            .selectTab(MatchedPOLinesTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }
    }

  }
}
