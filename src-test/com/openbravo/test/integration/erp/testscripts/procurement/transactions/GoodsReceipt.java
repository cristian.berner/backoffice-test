/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.procurement.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt.CreateLinesFromData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt.GoodsReceiptHeaderTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt.GoodsReceiptLinesTab;
import com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt.GoodsReceiptWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the window.
 *
 * @author Pablo Lujan
 */
public class GoodsReceipt extends SmokeTabScript<GoodsReceiptHeaderData, GoodsReceiptHeaderTab>
    implements SmokeWindowScript<GoodsReceiptWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public GoodsReceipt(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the window.
   *
   * @return the opened window.
   */
  @Override
  public GoodsReceipt open() {
    final GoodsReceiptWindow window = new GoodsReceiptWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the tab.
   *
   * Before this, the window must be opened.
   *
   * @return the tab GUI object.
   */
  @Override
  protected GoodsReceiptHeaderTab selectTab() {
    final GoodsReceiptWindow window = (GoodsReceiptWindow) mainPage
        .getView(GoodsReceiptWindow.TITLE);
    mainPage.getTabSetMain().selectTab(GoodsReceiptWindow.TITLE);
    final GoodsReceiptHeaderTab tab = (GoodsReceiptHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    logger.info("Completing goods receipt.");
    final GoodsReceiptHeaderTab tab = selectTab();
    tab.complete();
  }

  /**
   * Complete the order.
   *
   */
  public void close() {
    logger.info("Voiding goods receipt.");
    final GoodsReceiptHeaderTab tab = selectTab();
    tab.close();
  }

  /**
   * Create lines from an order.
   *
   * @param warehouseAlias
   *          The warehouse alias.
   * @param order
   *          The order identifier. It is a string of the form {document number} - {order date} -
   *          {total amount}.
   * @param attribute
   *          The line attribute.
   *
   */
  public void createLinesFrom(String warehouseAlias, String order, String attribute) {
    this.createLinesFrom(warehouseAlias, order, attribute, 0);
  }

  /**
   * Create lines from an order choosing a product with no attribute set
   *
   * @param warehouseAlias
   *          The warehouse alias.
   * @param order
   *          The order identifier. It is a string of the form {document number} - {order date} -
   *          {total amount}.
   *
   */
  public void createLinesFrom(String warehouseAlias, String order) {
    this.createLinesFrom(warehouseAlias, order, "", 0);
  }

  /**
   * Create goods receipt lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param attribute
   *          The line attribute.
   * @param lineNo
   *          The order line number to select or 0 to select all
   */
  public void createLinesFrom(String warehouseAlias, String order, String attribute, int lineNo) {
    this.createLinesFrom(new CreateLinesFromData.Builder().warehouse(warehouseAlias)
        .purchaseOrder(order)
        .attribute(attribute)
        .lineNumber(lineNo)
        .build());
  }

  public void createLinesFrom(CreateLinesFromData linesData) {
    logger.info("Create lines from order '{}'.", linesData.getDataField("purchaseOrder"));
    GoodsReceiptHeaderTab tab = selectTab();
    tab.createLinesFrom(linesData);
  }

  /**
   * Generate Invoice from Receipt choosing a price list version
   *
   * @param priceListVersion
   *          The price list version.
   */
  public void generateInvoice(String priceListVersion) {
    logger.info("Generate Invoice from Receipt.");
    final GoodsReceiptHeaderTab tab = selectTab();
    tab.generateInvoice(priceListVersion);
  }

  /**
   * Executes and verifies actions on the Goods Receipt Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<GoodsReceiptLinesData, GoodsReceiptLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Sales Order window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected GoodsReceiptLinesTab selectTab() {
      final GoodsReceiptWindow window = (GoodsReceiptWindow) mainPage
          .getView(GoodsReceiptWindow.TITLE);
      final GoodsReceiptLinesTab tab = (GoodsReceiptLinesTab) window
          .selectTab(GoodsReceiptLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Clear the filters.
     */
    public void clearFilters() {
      GoodsReceiptLinesTab tab = selectTab();
      tab.clearFilters();
    }
  }

}
