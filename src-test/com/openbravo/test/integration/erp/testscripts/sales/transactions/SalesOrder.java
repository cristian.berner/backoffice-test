/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Nono Carballo <nonofce@gmail.com>,
 *   Mark Molina <markmm82@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.DiscountsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.ManageReservationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.TaxData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.BasicDiscountsTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.PaymentInDetailsTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.PaymentInPlanTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.TaxTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Sales Order window.
 *
 * @author elopio
 */
public class SalesOrder extends SmokeTabScript<SalesOrderHeaderData, HeaderTab>
    implements SmokeWindowScript<SalesOrderWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public SalesOrder(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Sales Order window.
   *
   * @return the opened Sales Order window.
   */
  @Override
  public SalesOrder open() {
    SalesOrderWindow window = new SalesOrderWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Sales Order window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected HeaderTab selectTab() {
    SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
    mainPage.getTabSetMain().selectTab(SalesOrderWindow.TITLE);
    HeaderTab tab = (HeaderTab) window.getTopLevelTab();
    return tab;
  }

  /**
   * Book the order.
   *
   */
  public void book() {
    logger.info("Book sales order.");
    HeaderTab tab = selectTab();
    tab.book();
  }

  /**
   * Close the order.
   *
   */
  public void close() {
    logger.info("Close sales order.");
    HeaderTab tab = selectTab();
    tab.close();
  }

  /**
   * Reactivate the order.
   *
   */
  public void reactivate() {
    logger.info("Reactivate sales order.");
    HeaderTab tab = selectTab();
    tab.reactivate();
  }

  /**
   * Add a payment and process it.
   *
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String payment, String action) {
    logger.info("Add payment.");
    HeaderTab tab = selectTab();
    tab.addPayment(null, payment, action, null);
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   *          The verification data.
   */
  public void addPayment(String account, String action, AddPaymentPopUpData addPaymentPopUpData) {
    logger.info("Add payment.");
    HeaderTab tab = selectTab();
    tab.addPayment(account, null, action, addPaymentPopUpData);
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   *          The verification data.
   */
  public void addPayment(String account, String payment, String action,
      AddPaymentPopUpData addPaymentPopUpData) {
    logger.info("Add payment.");
    HeaderTab tab = selectTab();
    tab.addPayment(account, payment, action, addPaymentPopUpData);
  }

  /**
   * Open Add Payment pop
   */
  public AddPaymentProcess openAddPayment() {
    logger.info("Opening Add Payment.");
    HeaderTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.openAddPayment();
    return addPaymentProcess;
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    HeaderTab tab = selectTab();
    tab.assertAddPaymentInData(data);
  }

  public void assertPaymentCreatedSuccessfully() {
    selectTab().assertPaymentCreatedSuccessfully();
  }

  public void copyRecord() {
    HeaderTab tab = selectTab();
    tab.copyRecord();
  }

  public PickAndExecuteWindow<SalesOrderHeaderData> copyFromOrders() {
    HeaderTab tab = selectTab();
    return tab.copyFromOrders();
  }

  /**
   * Executes and verifies actions on the Sales Order Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<SalesOrderLinesData, LinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
      // Selenium was working too fast and due to a problem with the focus, the following static
      // sleep has been added. Changing from
      // GoodShipment to GoodShipmentLines the focus did not change to GoodShipmentLines
      // and stayed in GoodShipment.
      Sleep.trySleep(500);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Sales Order window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected LinesTab selectTab() {
      SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
      LinesTab tab = (LinesTab) window.selectTab(LinesTab.IDENTIFIER);
      return tab;
    }

    /**
     * Manage stock reservation.
     */
    public void manageReservation() {
      LinesTab tab = selectTab();
      tab.manageReservation();
    }

    /**
     * Manage stock reservation.
     */
    public void manageReservation(ManageReservationData data) {
      LinesTab tab = selectTab();
      tab.manageReservation(data);
    }

    /**
     * Manage stock reservation.
     */
    public void manageReservation(ManageReservationData filterData, ManageReservationData data) {
      LinesTab tab = selectTab();
      tab.manageReservation(filterData, data);
    }

    /**
     * Manage stock reservation with error.
     */
    public void manageReservationWithError(ManageReservationData data) {
      LinesTab tab = selectTab();
      tab.manageReservationWithError(data);
    }

  }

  /**
   * Executes and verifies actions on the Sales Order Tax tab.
   *
   * @author elopio
   *
   */
  public class Tax extends SmokeTabScript<TaxData, TaxTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Tax(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Tax tab.
     *
     * Before this, the Sales Order window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected TaxTab selectTab() {
      SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
      TaxTab tab = (TaxTab) window.selectTab(TaxTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Payment In Plan tab.
   *
   * @author aferraz
   *
   */
  public class PaymentInPlan extends SmokeTabScript<PaymentPlanData, PaymentInPlanTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public PaymentInPlan(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Payment In Plan tab.
     *
     * Before this, the Sales Order window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PaymentInPlanTab selectTab() {
      SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
      PaymentInPlanTab tab = (PaymentInPlanTab) window.selectTab(PaymentInPlanTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Executes and verifies actions on the Payment In Details tab.
     *
     * @author aferraz
     *
     */
    public class PaymentInDetails extends SmokeTabScript<PaymentDetailsData, PaymentInDetailsTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public PaymentInDetails(MainPage mainPage) {
        super(mainPage);
      }

      /**
       * Select and return the Payment In Details tab.
       *
       * Before this, the Sales Order window must be opened and this tab has to be accessible from
       * the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected PaymentInDetailsTab selectTab() {
        SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
        PaymentInDetailsTab tab = (PaymentInDetailsTab) window
            .selectTab(PaymentInDetailsTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }
    }
  }

  /**
   * Executes and verifies actions on the Sales Order Basic Discounts tab.
   *
   * @author collazoandy4
   *
   */
  public class BasicDiscounts extends SmokeTabScript<DiscountsData, BasicDiscountsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public BasicDiscounts(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Basic Discounts tab.
     *
     * Before this, the Sales Order window must be opened and this tab has to be accessible from the
     * currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected BasicDiscountsTab selectTab() {
      SalesOrderWindow window = (SalesOrderWindow) mainPage.getView(SalesOrderWindow.TITLE);
      BasicDiscountsTab tab = (BasicDiscountsTab) window.selectTab(BasicDiscountsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

}
