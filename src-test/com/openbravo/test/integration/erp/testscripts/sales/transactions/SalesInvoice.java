/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Luján <plu@openbravo.com>,
 *   David Miguélez <david.miguelez@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.DiscountsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.TaxData;
import com.openbravo.test.integration.erp.data.sharedtabs.ExchangeRatesData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.BasicDiscountsTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.ExchangeRatesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.PaymentInDetailsTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.PaymentInPlanTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceHeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceLinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceWindow;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.TaxTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on the Sales Invoice window.
 *
 * @author elopio
 */
public class SalesInvoice extends SmokeTabScript<SalesInvoiceHeaderData, SalesInvoiceHeaderTab>
    implements SmokeWindowScript<SalesInvoiceWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public SalesInvoice(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Sales Invoice window.
   *
   * @return this Sales Invoice script.
   */
  @Override
  public SalesInvoice open() {
    SalesInvoiceWindow window = new SalesInvoiceWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Sales Invoice window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected SalesInvoiceHeaderTab selectTab() {
    SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
    mainPage.getTabSetMain().selectTab(SalesInvoiceWindow.TITLE);
    SalesInvoiceHeaderTab tab = (SalesInvoiceHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Create sales invoice lines from order.
   *
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void createLinesFrom(String order) {
    logger.info("Create lines from order '{}'.", order);
    SalesInvoiceHeaderTab tab = selectTab();
    tab.createLinesFrom(order);
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   */
  public void createLinesFromShipment(String shipment) {
    logger.info("Create lines from shipment.");
    final SalesInvoiceHeaderTab tab = selectTab();
    tab.createLinesFromShipment(shipment);
  }

  /**
   * Create lines from order.
   *
   * @param order
   *          The order identifier.
   * @param lineNo
   *          The order line No.
   */
  public void createLinesFrom(String order, int lineNo) {
    logger.info("Create lines from order '{}'.", order);
    SalesInvoiceHeaderTab tab = selectTab();
    tab.createLinesFrom(order, lineNo);
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    logger.info("Complete sales order.");
    SalesInvoiceHeaderTab tab = selectTab();
    tab.complete();
  }

  /**
   * Void the invoice.
   *
   */
  public void close() {
    logger.info("Void sales invoice.");
    SalesInvoiceHeaderTab tab = selectTab();
    tab.close();
  }

  @Deprecated
  // This method is deprecated due to non natural way of asserting
  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    SalesInvoiceHeaderTab tab = selectTab();
    tab.assertAddPaymentInData(data);
  }

  /**
   * Add a payment and process it.
   *
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String payment, String action) {
    logger.info("Add payment.");
    SalesInvoiceHeaderTab tab = selectTab();
    tab.addPayment(payment, action);
  }

  /**
   * Open Add Payment pop
   */
  public AddPaymentProcess openAddPayment() {
    logger.info("Opening Add Payment.");
    SalesInvoiceHeaderTab tab = selectTab();
    AddPaymentProcess addPaymentProcess = tab.openAddPayment();
    return addPaymentProcess;
  }

  /**
   * Check if the sales invoice is already posted.
   */
  public boolean isPosted() {
    logger.info("Checking if the sales invoice is already posted.");
    SalesInvoiceHeaderTab tab = selectTab();
    return tab.isPosted();
  }

  /**
   * Post the sales invoice.
   */
  public void post() {
    logger.info("Post the sales invoice.");
    SalesInvoiceHeaderTab tab = selectTab();
    tab.post();
  }

  /**
   * Unpost the sales invoice.
   */
  public void unpost() {
    logger.info("Unpost the sales invoice.");
    SalesInvoiceHeaderTab tab = selectTab();
    tab.unpost();
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully() {
    SalesInvoiceHeaderTab tab = selectTab();
    tab.assertPaymentCreatedSuccessfully();
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully2() {
    SalesInvoiceHeaderTab tab = selectTab();
    tab.assertPaymentCreatedSuccessfully2();
  }

  /**
   * Assert that the refunded payment creation completed successfully.
   */
  public void assertRefundedPaymentCreatedSuccessfully() {
    SalesInvoiceHeaderTab tab = selectTab();
    tab.assertRefundedPaymentCreatedSuccessfully();
  }

  /**
   * Assert that The Period does not exist or it is not opened.
   */
  public void assertPeriodDoesNotExistOrItIsNotOpened() {
    SalesInvoiceHeaderTab tab = selectTab();
    tab.assertPeriodDoesNotExistOrItIsNotOpened();
  }

  /**
   * Assert display logic visibility from AddPayment Popup
   *
   * @param visibleDisplayLogic
   *          The AddPaymentDisplayLogic with the fields whose visibility has to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.info("Asserting display logic visibility from AddPayment Popup.");
    final SalesInvoiceHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
  }

  /**
   * Assert display logic enable status from AddPayment Popup
   *
   * @param enabledDisplayLogic
   *          DataOject with the fields whose enable status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.info("Asserting display logic enable status from AddPayment Popup.");
    final SalesInvoiceHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
  }

  /**
   * Assert display logic expanded status from AddPayment Popup
   *
   * @param expandedDisplayLogic
   *          DataOject with the fields whose expanded status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.info("Asserting display logic expanded status from AddPayment Popup.");
    final SalesInvoiceHeaderTab tab = selectTab();
    tab.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
  }

  /**
   * Executes and verifies actions on the Sales Invoice Lines tab.
   *
   * @author elopio
   *
   */
  public class Lines extends SmokeTabScript<SalesInvoiceLinesData, SalesInvoiceLinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
      // Selenium was working too fast and due to a problem with the focus, the following static
      // sleep has been added. Changing from
      // PurchaseInvoice to PurchaseInvoiceLines the focus did not change to PurchaseInvoiceLines
      // and stayed in PurchaseInvoice.
      Sleep.trySleep(500);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected SalesInvoiceLinesTab selectTab() {
      SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
      SalesInvoiceLinesTab tab = (SalesInvoiceLinesTab) window
          .selectTab(SalesInvoiceLinesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the Payment In Plan tab.
   *
   * @author elopio
   *
   */
  public class PaymentInPlan extends SmokeTabScript<PaymentPlanData, PaymentInPlanTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public PaymentInPlan(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Payment In Plan tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected PaymentInPlanTab selectTab() {
      SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
      PaymentInPlanTab tab = (PaymentInPlanTab) window.selectTab(PaymentInPlanTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

    /**
     * Modify the payment plan.
     */
    public void modifyPaymentPlan() {
      PaymentInPlanTab tab = selectTab();
      tab.modifyPaymentPlan();
    }

    /**
     * Çlear the aplied filter
     */
    public void clearFilters() {
      PaymentInPlanTab tab = selectTab();
      tab.clearFilters();
    }

    public void unselectAll() {
      PaymentInPlanTab tab = selectTab();
      tab.unselectAll();
    }

    /**
     * Executes and verifies actions on the Payment In Details tab.
     *
     * @author elopio
     *
     */
    public class PaymentInDetails extends SmokeTabScript<PaymentDetailsData, PaymentInDetailsTab> {

      /**
       * Class constructor.
       *
       * @param mainPage
       *          The application main page.
       */
      public PaymentInDetails(MainPage mainPage) {
        super(mainPage);
        // There have been performance problems. Asserts after changing to Lines tab were too fast.
        // The app had not finished from loading yet. Sleep was added to avoid false positives
        Sleep.trySleep(1500);
      }

      /**
       * Select and return the Payment In Plan tab.
       *
       * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
       * the currently selected one.
       *
       * @return the Header tab GUI object.
       */
      @Override
      protected PaymentInDetailsTab selectTab() {
        SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
        PaymentInDetailsTab tab = (PaymentInDetailsTab) window
            .selectTab(PaymentInDetailsTab.IDENTIFIER);
        tab.waitUntilSelected();
        return tab;
      }

      public void clearFilters() {
        PaymentInDetailsTab tab = selectTab();
        tab.clearFilters();
      }
    }
  }

  /**
   * Executes and verifies actions on the Archived Payment In Plan tab.
   *
   * @author plujan
   *
   */

  /**
   * Executes and verifies actions on the Sales Invoice Exchange rates tab.
   *
   * @author David Miguélez
   *
   */
  public class ExchangeRates extends SmokeTabScript<ExchangeRatesData, ExchangeRatesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public ExchangeRates(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Exchange rates tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected ExchangeRatesTab selectTab() {
      SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
      ExchangeRatesTab tab = (ExchangeRatesTab) window.selectTab(ExchangeRatesTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }
  }

  /**
   * Executes and verifies actions on the Sales Invoice Basic Discounts tab.
   *
   * @author Atul Gaware
   *
   */

  public class BasicDiscounts extends SmokeTabScript<DiscountsData, BasicDiscountsTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public BasicDiscounts(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Basic Discounts tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */
    @Override
    protected BasicDiscountsTab selectTab() {
      SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
      BasicDiscountsTab tab = (BasicDiscountsTab) window.selectTab(BasicDiscountsTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

  /**
   * Executes and verifies actions on the Sales Invoice Tax tab.
   *
   * @author Atul Gaware
   *
   */
  public class Tax extends SmokeTabScript<TaxData, TaxTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Tax(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Tax tab.
     *
     * Before this, the Sales Invoice window must be opened and this tab has to be accessible from
     * the currently selected one.
     *
     * @return the Header tab GUI object.
     */

    @Override
    protected TaxTab selectTab() {
      SalesInvoiceWindow window = (SalesInvoiceWindow) mainPage.getView(SalesInvoiceWindow.TITLE);
      TaxTab tab = (TaxTab) window.selectTab(TaxTab.IDENTIFIER);
      tab.waitUntilSelected();
      return tab;
    }

  }

}
