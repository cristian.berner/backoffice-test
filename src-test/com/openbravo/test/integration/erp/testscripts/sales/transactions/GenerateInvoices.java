/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import com.openbravo.test.integration.erp.gui.sales.transactions.generateinvoices.GenerateInvoicesPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;

/**
 * Executes and verifies actions on the Generate Invoices pop up.
 *
 * @author elopio
 */
public class GenerateInvoices {

  /** The application main page. */
  protected final MainPage mainPage;

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public GenerateInvoices(MainPage mainPage) {
    this.mainPage = mainPage;
  }

  /**
   * Open the Generate Invoices pop up..
   *
   * @return this script.
   */
  public GenerateInvoices open() {
    mainPage.selectWindow();
    mainPage.getNavigationBar().getComponentMenu().select(GenerateInvoicesPopUp.MENU_PATH);
    return this;
  }

  /**
   * Generate invoices.
   *
   * @param invoiceDate
   *          The date of the invoices.
   * @param organization
   *          The name of the organization.
   */
  public void generateInvoices(String invoiceDate, String organization) {
    GenerateInvoicesPopUp generateInvoicesPopUp = new GenerateInvoicesPopUp();
    generateInvoicesPopUp.generateInvoices(invoiceDate, organization);
  }
}
