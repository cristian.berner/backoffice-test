/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2012-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   David Miguélez <david.miguelez@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.sales.transactions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerLinesData;
import com.openbravo.test.integration.erp.data.selectors.ReturnFromCustomerLineSelectorData;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer.PaymentInDetailsTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer.PaymentInPlanTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer.ReturnFromCustomerWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.SmokeTabScript;
import com.openbravo.test.integration.erp.testscripts.SmokeWindowScript;

/**
 * Executes and verifies actions on the Return From Customer.
 *
 * @author David Miguélez
 */
public class ReturnFromCustomer
    extends SmokeTabScript<ReturnFromCustomerHeaderData, ReturnFromCustomerHeaderTab>
    implements SmokeWindowScript<ReturnFromCustomerWindow> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param mainPage
   *          The application main page.
   */
  public ReturnFromCustomer(MainPage mainPage) {
    super(mainPage);
  }

  /**
   * Open the Return From Customer window.
   *
   * @return this Return From Customer script.
   */
  @Override
  public ReturnFromCustomer open() {
    ReturnFromCustomerWindow window = new ReturnFromCustomerWindow();
    mainPage.openView(window);
    return this;
  }

  /**
   * Select and return the Header tab.
   *
   * Before this, the Return From Customer window must be opened.
   *
   * @return the Header tab GUI object.
   */
  @Override
  protected ReturnFromCustomerHeaderTab selectTab() {
    ReturnFromCustomerWindow window = (ReturnFromCustomerWindow) mainPage
        .getView(ReturnFromCustomerWindow.TITLE);
    mainPage.getTabSetMain().selectTab(ReturnFromCustomerWindow.TITLE);
    ReturnFromCustomerHeaderTab tab = (ReturnFromCustomerHeaderTab) window.getTopLevelTab();
    tab.waitUntilSelected();
    return tab;
  }

  /**
   * Book the return order.
   *
   */
  public void book() {
    logger.info("Book return from customer.");
    ReturnFromCustomerHeaderTab tab = selectTab();
    tab.book();
  }

  /**
   * Pick And Edit
   */
  public void pickAndEditLines(String order) {
    logger.info("Pick And Edit Lines");
    ReturnFromCustomerHeaderTab tab = selectTab();
    tab.pickAndEditLines(order);
  }

  public PickAndExecuteWindow<ReturnFromCustomerLineSelectorData> openPickAndEditLines() {
    return selectTab().openPickAndEditLines();
  }

  /**
   * Executes and verifies actions on the Return from Customer Lines tab.
   *
   * @author lorenzo.fidalgo
   *
   */
  public class Lines extends SmokeTabScript<ReturnFromCustomerLinesData, LinesTab> {

    /**
     * Class constructor.
     *
     * @param mainPage
     *          The application main page.
     */
    public Lines(MainPage mainPage) {
      super(mainPage);
    }

    /**
     * Select and return the Lines tab.
     *
     * Before this, the ReturnFromCustomerWindow must be opened and this tab has to be accessible
     * from the currently selected one.
     *
     * @return the Lines tab GUI object.
     */
    @Override
    protected LinesTab selectTab() {
      ReturnFromCustomerWindow window = (ReturnFromCustomerWindow) mainPage
          .getView(ReturnFromCustomerWindow.TITLE);
      LinesTab tab = (LinesTab) window.selectTab(LinesTab.IDENTIFIER);
      return tab;
    }

  }

  public class PaymentInPlan extends SmokeTabScript<PaymentInPlanData, PaymentInPlanTab> {

    public PaymentInPlan(MainPage mainPage) {
      super(mainPage);
    }

    @Override
    protected PaymentInPlanTab selectTab() {
      final ReturnFromCustomerWindow window = (ReturnFromCustomerWindow) mainPage
          .getView(ReturnFromCustomerWindow.TITLE);
      final PaymentInPlanTab tab = (PaymentInPlanTab) window.selectTab(PaymentInPlanTab.IDENTIFIER);
      return tab;
    }

    public PaymentInPlanTab selectPaymentOutPlanTab() {
      return this.selectTab();
    }

    public class PaymentInDetails
        extends SmokeTabScript<PaymentInDetailsData, PaymentInDetailsTab> {

      public PaymentInDetails(MainPage mainPage) {
        super(mainPage);
      }

      @Override
      protected PaymentInDetailsTab selectTab() {
        final ReturnFromCustomerWindow window = (ReturnFromCustomerWindow) mainPage
            .getView(ReturnFromCustomerWindow.TITLE);
        window.selectTab(PaymentInPlanTab.IDENTIFIER);
        final PaymentInDetailsTab tab = (PaymentInDetailsTab) window
            .selectTab(PaymentInDetailsTab.IDENTIFIER);
        return tab;
      }

    }

  }

}
