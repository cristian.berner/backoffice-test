/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.videorecording;

import java.awt.AWTException;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.monte.media.Format;
import org.monte.media.Registry;
import org.monte.screenrecorder.ScreenRecorder;

import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Specialized Screen Recorded to override certain Monte Media Library settings
 *
 * @author Lorenzo Fidalgo
 */
public class SpecializedScreenRecorder extends ScreenRecorder {

  private String name;
  File sSRecordFile;

  public SpecializedScreenRecorder(GraphicsConfiguration cfg, Rectangle captureArea,
      Format fileFormat, Format screenFormat, Format mouseFormat, Format audioFormat,
      File movieFolder, String name) throws IOException, AWTException {
    super(cfg, captureArea, fileFormat, screenFormat, mouseFormat, audioFormat, movieFolder);
    this.name = name;
  }

  @Override
  protected File createMovieFile(Format fileFormat) throws IOException {
    if (!movieFolder.exists()) {
      movieFolder.mkdirs();
    } else if (!movieFolder.isDirectory()) {
      throw new IOException("\"" + movieFolder + "\" is not a directory.");
    }

    // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
    SimpleDateFormat dateFormat = new SimpleDateFormat(
        ConfigurationProperties.INSTANCE.getDateFormat() + " HH:mm:ss");

    return sSRecordFile = new File(movieFolder, name + " || " + dateFormat.format(new Date()) + "."
        + Registry.getInstance().getExtension(fileFormat));
  }

  public File getsSRecordFile() {
    return sSRecordFile;
  }

  public void setsSRecordFile(File sSRecordFile) {
    this.sSRecordFile = sSRecordFile;
  }

}
