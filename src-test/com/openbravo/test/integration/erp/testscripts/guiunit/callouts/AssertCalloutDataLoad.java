/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testscripts.guiunit.callouts;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the data loaded by a callout.
 *
 * @author elopio
 *
 */
public abstract class AssertCalloutDataLoad extends OpenbravoERPTest {

  /* Data for this test. */
  /** The original data object that contains the values that will launch a callout. */
  private final DataObject originalDataObject;
  /** The object with the expected data after the callout execution. */
  private final DataObject expectedDataObject;

  /**
   * Class constructor.
   *
   * @param originalDataObject
   *          The original data object that contains the values that will launch a callout.
   * @param expectedDataObject
   *          The object with the expected data after the callout execution.
   */
  public AssertCalloutDataLoad(DataObject originalDataObject, DataObject expectedDataObject) {
    this.originalDataObject = originalDataObject;
    this.expectedDataObject = expectedDataObject;
  }

  /**
   * Test the data loaded by a callout.
   */
  @Test
  public void calloutShouldLoadData() {
    GeneratedTab<DataObject> tab = getTab();
    tab.clickCreateNewInForm();
    tab.fillForm(originalDataObject);
    tab.assertData(expectedDataObject);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();

}
