/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.setup.commission;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for CommissionLinesData
 *
 * @author plujan
 *
 */
public class CommissionLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the trxOrganization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The trxOrganization value.
     * @return The builder for this class.
     */
    public Builder trxOrganization(String value) {
      this.dataFields.put("trxOrganization", value);
      return this;
    }

    /**
     * Set the commissionOnlySpecifiedOrders value
     *
     * Description: Commission only Orders or Invoices where this Sales Rep is entered
     *
     * @param value
     *          The commissionOnlySpecifiedOrders value.
     * @return The builder for this class.
     */
    public Builder commissionOnlySpecifiedOrders(Boolean value) {
      this.dataFields.put("commissionOnlySpecifiedOrders", value);
      return this;
    }

    /**
     * Set the subtractQuantity value
     *
     * Description: Quantity to subtract when generating commissions
     *
     * @param value
     *          The subtractQuantity value.
     * @return The builder for this class.
     */
    public Builder subtractQuantity(String value) {
      this.dataFields.put("subtractQuantity", value);
      return this;
    }

    /**
     * Set the multiplierQuantity value
     *
     * Description: Value to multiply quantities by for generating commissions.
     *
     * @param value
     *          The multiplierQuantity value.
     * @return The builder for this class.
     */
    public Builder multiplierQuantity(String value) {
      this.dataFields.put("multiplierQuantity", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the positiveOnly value
     *
     * Description: Do not generate negative commissions
     *
     * @param value
     *          The positiveOnly value.
     * @return The builder for this class.
     */
    public Builder positiveOnly(Boolean value) {
      this.dataFields.put("positiveOnly", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the salesRegion value
     *
     * Description: A defined section of the world where sales efforts will be focused.
     *
     * @param value
     *          The salesRegion value.
     * @return The builder for this class.
     */
    public Builder salesRegion(String value) {
      this.dataFields.put("salesRegion", value);
      return this;
    }

    /**
     * Set the commission value
     *
     * Description: Commission Identifier
     *
     * @param value
     *          The commission value.
     * @return The builder for this class.
     */
    public Builder commission(String value) {
      this.dataFields.put("commission", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the subtractAmount value
     *
     * Description: Subtract Amount for generating commissions
     *
     * @param value
     *          The subtractAmount value.
     * @return The builder for this class.
     */
    public Builder subtractAmount(String value) {
      this.dataFields.put("subtractAmount", value);
      return this;
    }

    /**
     * Set the multiplierAmount value
     *
     * Description: Multiplier Amount for generating commissions
     *
     * @param value
     *          The multiplierAmount value.
     * @return The builder for this class.
     */
    public Builder multiplierAmount(String value) {
      this.dataFields.put("multiplierAmount", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CommissionLinesData build() {
      return new CommissionLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CommissionLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
