/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ShipmentReceiptLineSelectorData;

/**
 *
 * Class for SalesInvoiceLinesData
 *
 * @author plujan
 *
 */
public class SalesInvoiceLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the taxableAmount value
     *
     * Description: The total sum on which taxes are added.
     *
     * @param value
     *          The taxableAmount value.
     * @return The builder for this class.
     */
    public Builder taxableAmount(String value) {
      this.dataFields.put("taxableAmount", value);
      return this;
    }

    /**
     * Set the financialInvoiceLine value
     *
     * Description: This flag will decide whether we are going to book the invoice line on directly
     * general ledger or not.
     *
     * @param value
     *          The financialInvoiceLine value.
     * @return The builder for this class.
     */
    public Builder financialInvoiceLine(Boolean value) {
      this.dataFields.put("financialInvoiceLine", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the goodsShipmentLine value
     *
     * Description: A statement displaying one item charge or movement in a shipment.
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(ShipmentReceiptLineSelectorData value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the invoicedQuantity value
     *
     * Description: The total number of a product included in an invoice to a business partner.
     *
     * @param value
     *          The invoicedQuantity value.
     * @return The builder for this class.
     */
    public Builder invoicedQuantity(String value) {
      this.dataFields.put("invoicedQuantity", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the unitPrice value
     *
     * Description: The price that will be paid for a specified item.
     *
     * @param value
     *          The unitPrice value.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSimpleSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the lineNetAmount value
     *
     * Description: The final amount of a specified line based only on quantities and prices.
     *
     * @param value
     *          The lineNetAmount value.
     * @return The builder for this class.
     */
    public Builder lineNetAmount(String value) {
      this.dataFields.put("lineNetAmount", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: A unique and often automatically generated identifier for a sales order line.
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the invoice value
     *
     * Description: A document listing products quantities and prices payment terms etc.
     *
     * @param value
     *          The invoice value.
     * @return The builder for this class.
     */
    public Builder invoice(String value) {
      this.dataFields.put("invoice", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the operative quantity
     *
     * @param value
     *          The quantity
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the operative UOM
     *
     * @param value
     *          The Alternative UOM
     * @return The builder for this class.
     */
    public Builder operativeUOM(String value) {
      this.dataFields.put("operativeUOM", value);
      return this;
    }

    /**
     * Set the base gross unit price
     *
     * @param value
     *          The BaseGross Unit Price
     * @return The builder for this class.
     */
    public Builder baseGrossUnitPrice(String value) {
      this.dataFields.put("baseGrossUnitPrice", value);
      return this;
    }

    /**
     * Set the gross unit price
     *
     * @param value
     *          The Gross Unit Price
     * @return The builder for this class.
     */
    public Builder grossUnitPrice(String value) {
      this.dataFields.put("grossUnitPrice", value);
      return this;
    }

    /**
     * Set the line gross amount
     *
     * @param value
     *          The Line Gross Amount
     * @return The builder for this class.
     */
    public Builder lineGrossAmount(String value) {
      this.dataFields.put("grossAmount", value);
      return this;
    }

    /**
     * Set the gross list price
     *
     * @param value
     *          The Gross List Price
     * @return The builder for this class.
     */
    public Builder grossListPrice(String value) {
      this.dataFields.put("grossListPrice", value);
      return this;
    }

    /**
     * Set the is deferred revenue value
     *
     * @param value
     *          True if it is a deferred revenue, false in other case.
     * @return The builder for this class.
     */
    public Builder isDeferredRevenue(Boolean value) {
      this.dataFields.put("deferred", value);
      return this;
    }

    /**
     * Set the revenue plan type value
     *
     * @param value
     *          The Revenue Plan Type value
     * @return The builder for this class.
     */
    public Builder revenuePlanType(String value) {
      this.dataFields.put("deferredPlanType", value);
      return this;
    }

    /**
     * Set the period number value
     *
     * @param value
     *          The Period Number value
     * @return The builder for this class.
     */
    public Builder periodNumber(String value) {
      this.dataFields.put("periodNumber", value);
      return this;
    }

    /**
     * Set the default period value
     *
     * @param value
     *          The Default Period value
     * @return The builder for this class.
     */
    public Builder defaultPeriod(String value) {
      this.dataFields.put("period", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public SalesInvoiceLinesData build() {
      return new SalesInvoiceLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private SalesInvoiceLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
