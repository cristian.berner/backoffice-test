/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.salesorder;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for SalesOrderHeaderData
 *
 * @author plujan
 *
 */
public class SalesOrderHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the orderDate value.
     *
     * Description: The time listed on the order.
     *
     * @param value
     *          The orderDate value.
     * @return The builder for this class.
     */
    public Builder orderDate(String value) {
      this.dataFields.put("orderDate", value);
      return this;
    }

    /**
     * Set the priceList value.
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the documentNo value.
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the documentStatus value.
     *
     * Description: The Document Status indicates the status of a document at this time.
     *
     * @param value
     *          The documentStatus value.
     * @return The builder for this class.
     */
    public Builder documentStatus(String value) {
      this.dataFields.put("documentStatus", value);
      return this;
    }

    /**
     * Set the deliveryStatus value.
     *
     * Description: The deliveryStatus.
     *
     * @param value
     *          The deliveryStatus value.
     * @return The builder for this class.
     */
    public Builder deliveryStatus(String value) {
      this.dataFields.put("deliveryStatus", value);
      return this;
    }

    /**
     * Set the invoiceStatus value.
     *
     * Description: The invoiceStatus.
     *
     * @param value
     *          The invoiceStatus value.
     * @return The builder for this class.
     */
    public Builder invoiceStatus(String value) {
      this.dataFields.put("invoiceStatus", value);
      return this;
    }

    /**
     * Set the paymentStatus value.
     *
     * Description: The status of the payment.
     *
     * @param value
     *          The paymentStatus value.
     * @return The builder for this class.
     */
    public Builder paymentStatus(String value) {
      this.dataFields.put("paymentStatus", value);
      return this;
    }

    /**
     * Set the transactionDocument value
     *
     * Description: The specific document type which should be used for the document.
     *
     * @param value
     *          The transactionDocument value.
     * @return The builder for this class.
     */
    public Builder transactionDocument(String value) {
      this.dataFields.put("transactionDocument", value);
      return this;
    }

    /**
     * Set the description value.
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the scheduledDeliveryDate value.
     *
     * Description: The date that a task process or action is to be completed or delivered by.
     *
     * @param value
     *          The scheduledDeliveryDate value.
     * @return The builder for this class.
     */
    public Builder scheduledDeliveryDate(String value) {
      this.dataFields.put("scheduledDeliveryDate", value);
      return this;
    }

    /**
     * Set the salesRepresentative value.
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the paymentTerms value.
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(String value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the invoiceAddress value.
     *
     * Description: The location where the invoice payment request will be sent.
     *
     * @param value
     *          The invoiceAddress value.
     * @return The builder for this class.
     */
    public Builder invoiceAddress(String value) {
      this.dataFields.put("invoiceAddress", value);
      return this;
    }

    /**
     * Set the currency value.
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the invoiceTerms value.
     *
     * Description: Frequency and method of invoicing
     *
     * @param value
     *          The invoiceTerms value.
     * @return The builder for this class.
     */
    public Builder invoiceTerms(String value) {
      this.dataFields.put("invoiceTerms", value);
      return this;
    }

    /**
     * Set the summedLineAmount value.
     *
     * Description: The final sum of all line totals made to a specified document or transaction
     * (not including taxes).
     *
     * @param value
     *          The summedLineAmount value.
     * @return The builder for this class.
     */
    public Builder summedLineAmount(String value) {
      this.dataFields.put("summedLineAmount", value);
      return this;
    }

    /**
     * Set the grandTotalAmount value.
     *
     * Description: The final monetary amount (including taxes) charge listed in a document.
     *
     * @param value
     *          The grandTotalAmount value.
     * @return The builder for this class.
     */
    public Builder grandTotalAmount(String value) {
      this.dataFields.put("grandTotalAmount", value);
      return this;
    }

    /**
     * Set the warehouse value.
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the businessPartner value.
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the organization value.
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the orderReference value.
     *
     * Description: A reference or document order number as listed in business partner application.
     *
     * @param value
     *          The orderReference value.
     * @return The builder for this class.
     */
    public Builder orderReference(String value) {
      this.dataFields.put("orderReference", value);
      return this;
    }

    /**
     * Set the partnerAddress value.
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the formOfPayment value.
     *
     * Description: The method used for payment of this transaction.
     *
     * @param value
     *          The formOfPayment value.
     * @return The builder for this class.
     */
    public Builder formOfPayment(String value) {
      this.dataFields.put("formOfPayment", value);
      return this;
    }

    /**
     * Set the deliveryLocation value.
     *
     * Description: The specific place or address an order will be shipped to or carried out from.
     *
     * @param value
     *          The deliveryLocation value.
     * @return The builder for this class.
     */
    public Builder deliveryLocation(String value) {
      this.dataFields.put("deliveryLocation", value);
      return this;
    }

    /**
     * Set the paymentMethod value.
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Set the fINPaymentPriorityID value.
     *
     * Description: Sets the priority of the payment plans generated when processing the invoice or
     * order.
     *
     * @param value
     *          The fINPaymentPriorityID value.
     * @return The builder for this class.
     */
    public Builder fINPaymentPriorityID(String value) {
      this.dataFields.put("fINPaymentPriorityID", value);
      return this;
    }

    /**
     * Set the reservationStatus value
     *
     * Description: Reservation status.
     *
     * @param value
     *          The reservationStatus value.
     * @return The builder for this class.
     */
    public Builder reservationStatus(String value) {
      this.dataFields.put("reservationStatus", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public SalesOrderHeaderData build() {
      return new SalesOrderHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private SalesOrderHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
