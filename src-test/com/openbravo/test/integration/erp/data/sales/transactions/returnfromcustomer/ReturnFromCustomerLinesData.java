/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

public class ReturnFromCustomerLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author lorenzo.fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the returnedQuantity value
     *
     * @param value
     *          The returnedQuantity value.
     * @return The builder for this class.
     */
    public Builder returnedQuantity(String value) {
      this.dataFields.put("orderedQuantity", value);
      return this;
    }

    /**
     * Set the taxableAmount value
     *
     * Description: The total sum on which taxes are added.
     *
     * @param value
     *          The taxableAmount value.
     * @return The builder for this class.
     */
    public Builder taxableAmount(String value) {
      this.dataFields.put("taxableAmount", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the lineGrossAmount value
     *
     *
     * @param value
     *          The lineGrossAmount value.
     * @return The builder for this class.
     */
    public Builder lineGrossAmount(String value) {
      this.dataFields.put("lineGrossAmount", value);
      return this;
    }

    /**
     * Set the lineNetAmount value
     *
     * Description: The final amount of a specified line based only on quantities and prices.
     *
     * @param value
     *          The lineNetAmount value.
     * @return The builder for this class.
     */
    public Builder lineNetAmount(String value) {
      this.dataFields.put("lineNetAmount", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the unitPrice value
     *
     * Description: The price that will be paid for a specified item.
     *
     * @param value
     *          The unitPrice value.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the grossUnitPrice value
     *
     * Description: The grossUnitPrice that will be paid for a specified item.
     *
     * @param value
     *          The grossUnitPrice value.
     * @return The builder for this class.
     */
    public Builder grossUnitPrice(String value) {
      this.dataFields.put("grossUnitPrice", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the goodsShipmentLine value
     *
     * @param value
     *          The goodsShipmentLine value.
     * @return The builder for this class.
     */
    public Builder goodsShipmentLine(String value) {
      this.dataFields.put("goodsShipmentLine", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the invoicedQuantity value
     *
     * Description: The total number of a product included in an invoice to a business partner.
     *
     * @param value
     *          The invoicedQuantity value.
     * @return The builder for this class.
     */
    public Builder invoicedQuantity(String value) {
      this.dataFields.put("invoicedQuantity", value);
      return this;
    }

    /**
     * Set the deliveredQuantity value
     *
     * Description: Delivered Quantity
     *
     * @param value
     *          The deliveredQuantity value.
     * @return The builder for this class.
     */
    public Builder deliveredQuantity(String value) {
      this.dataFields.put("deliveredQuantity", value);
      return this;
    }

    /**
     * Set the operativeQuantity value
     *
     *
     * @param value
     *          The operativeQuantity value.
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("operativeQuantity", value);
      return this;
    }

    /**
     * Set the orderedQuantity value
     *
     * Description: The number of an item involved in a transaction given in standard units. It is
     * used to determine price per unit.
     *
     * @param value
     *          The orderedQuantity value.
     * @return The builder for this class.
     */
    public Builder orderedQuantity(String value) {
      this.dataFields.put("orderedQuantity", value);
      return this;
    }

    /**
     * Set the operativeUOM value
     *
     *
     * @param value
     *          The operativeUOM value.
     * @return The builder for this class.
     */
    public Builder operativeUOM(String value) {
      this.dataFields.put("operativeUOM", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the salesOrder value
     *
     * Description: A unique and often automatically generated identifier for a sales order.
     *
     * @param value
     *          The salesOrder value.
     * @return The builder for this class.
     */
    public Builder salesOrder(String value) {
      this.dataFields.put("salesOrder", value);
      return this;
    }

    /**
     * Set the reservationStatus value
     *
     * Description: Reservation status.
     *
     * @param value
     *          The reservationStatus value.
     * @return The builder for this class.
     */
    public Builder reservationStatus(String value) {
      this.dataFields.put("reservationStatus", value);
      return this;
    }

    /**
     * Set the returnReason value
     *
     * Description: returnReason status.
     *
     * @param value
     *          The returnReason value.
     * @return The builder for this class.
     */
    public Builder returnReason(String value) {
      this.dataFields.put("returnReason", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnFromCustomerLinesData build() {
      return new ReturnFromCustomerLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnFromCustomerLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
