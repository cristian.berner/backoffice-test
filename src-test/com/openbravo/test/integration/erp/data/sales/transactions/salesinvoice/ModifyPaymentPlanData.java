/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentPlanData
 *
 * @author plujan
 *
 */
public class ModifyPaymentPlanData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the dueDate value
     *
     * Description: Due Date
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the paymentMethod value
     *
     * Description: Payment Method
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Set the outstanding value
     *
     * Description: Outstanding
     *
     * @param value
     *          The outstanding value.
     * @return The builder for this class.
     */
    public Builder outstanding(String value) {
      this.dataFields.put("outstanding", value);
      return this;
    }

    /**
     * Set the expected value
     *
     * Description: Expected Amount
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("expected", value);
      return this;
    }

    /**
     * Set the received value
     *
     * Description: Received
     *
     * @param value
     *          The received value.
     * @return The builder for this class.
     */
    public Builder received(String value) {
      this.dataFields.put("received", value);
      return this;
    }

    /**
     * Set the awaitingExecutionAmount value
     *
     * Description: Awaiting Execution Amount
     *
     * @param value
     *          The awaitingExecutionAmount value.
     * @return The builder for this class.
     */
    public Builder awaitingExecutionAmount(String value) {
      this.dataFields.put("awaitingExecutionAmount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: Currency
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ModifyPaymentPlanData build() {
      return new ModifyPaymentPlanData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ModifyPaymentPlanData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
