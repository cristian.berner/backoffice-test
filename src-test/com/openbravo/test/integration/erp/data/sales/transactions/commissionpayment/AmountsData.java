/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.sales.transactions.commissionpayment;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AmountsData
 *
 * @author plujan
 *
 */
public class AmountsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the commissionRun value
     *
     * Description: Commission Run or Process
     *
     * @param value
     *          The commissionRun value.
     * @return The builder for this class.
     */
    public Builder commissionRun(String value) {
      this.dataFields.put("commissionRun", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Commission Amount
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the actualQuantity value
     *
     * Description: The actual quantity
     *
     * @param value
     *          The actualQuantity value.
     * @return The builder for this class.
     */
    public Builder actualQuantity(String value) {
      this.dataFields.put("actualQuantity", value);
      return this;
    }

    /**
     * Set the convertedAmount value
     *
     * Description: The monetary sum at which one unit of measure is changed to another.
     *
     * @param value
     *          The convertedAmount value.
     * @return The builder for this class.
     */
    public Builder convertedAmount(String value) {
      this.dataFields.put("convertedAmount", value);
      return this;
    }

    /**
     * Set the commissionLine value
     *
     * Description: Commission Line
     *
     * @param value
     *          The commissionLine value.
     * @return The builder for this class.
     */
    public Builder commissionLine(String value) {
      this.dataFields.put("commissionLine", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AmountsData build() {
      return new AmountsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AmountsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
