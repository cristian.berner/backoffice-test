/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:20
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.setup.warehouseandstoragebins;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BinContentsData
 *
 * @author plujan
 *
 */
public class BinContentsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the onHandOrderQuanity value
     *
     * Description: On Hand Order Quanity
     *
     * @param value
     *          The onHandOrderQuanity value.
     * @return The builder for this class.
     */
    public Builder onHandOrderQuanity(String value) {
      this.dataFields.put("onHandOrderQuanity", value);
      return this;
    }

    /**
     * Set the orderUOM value
     *
     * Description: The unit of measure being used for the request.
     *
     * @param value
     *          The orderUOM value.
     * @return The builder for this class.
     */
    public Builder orderUOM(String value) {
      this.dataFields.put("orderUOM", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the quantityOrderInDraftTransactions value
     *
     * Description: Quantity Order in draft transactions
     *
     * @param value
     *          The quantityOrderInDraftTransactions value.
     * @return The builder for this class.
     */
    public Builder quantityOrderInDraftTransactions(String value) {
      this.dataFields.put("quantityOrderInDraftTransactions", value);
      return this;
    }

    /**
     * Set the lastInventoryCountDate value
     *
     * Description: Date of Last Inventory Count
     *
     * @param value
     *          The lastInventoryCountDate value.
     * @return The builder for this class.
     */
    public Builder lastInventoryCountDate(String value) {
      this.dataFields.put("lastInventoryCountDate", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the quantityInDraftTransactions value
     *
     * Description: Quantity in draft transactions
     *
     * @param value
     *          The quantityInDraftTransactions value.
     * @return The builder for this class.
     */
    public Builder quantityInDraftTransactions(String value) {
      this.dataFields.put("quantityInDraftTransactions", value);
      return this;
    }

    /**
     * Set the quantityOnHand value
     *
     * Description: On Hand Quantity
     *
     * @param value
     *          The quantityOnHand value.
     * @return The builder for this class.
     */
    public Builder quantityOnHand(String value) {
      this.dataFields.put("quantityOnHand", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: A set of coordinates (x y z) which help locate an item in a warehouse.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BinContentsData build() {
      return new BinContentsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BinContentsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
