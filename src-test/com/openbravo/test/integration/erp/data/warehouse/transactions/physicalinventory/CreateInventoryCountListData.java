/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory;

/**
 * Class for Create Inventory Count List data.
 *
 * @author elopio
 *
 */
public class CreateInventoryCountListData {

  /* Data fields. */
  /** Storage Bin. */
  private String storagebin;
  /** Inventory quantity. */
  private String inventoryQuantity;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /* Builder fields. */
    /** Storage Bin. */
    private String storagebin = null;
    /** Inventory quantity. */
    private String inventoryQuantity = null;

    /**
     * Set the Storage Bin value.
     *
     * @param value
     *          The Storage Bin value.
     * @return The builder for this class.
     */
    public Builder storagebin(String value) {
      this.storagebin = value;
      return this;
    }

    /**
     * Set the Inventory quantity value.
     *
     * @param value
     *          The Inventory quantity value.
     * @return The builder for this class.
     */
    public Builder inventoryQuantity(String value) {
      this.inventoryQuantity = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CreateInventoryCountListData build() {
      return new CreateInventoryCountListData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CreateInventoryCountListData(Builder builder) {
    storagebin = builder.storagebin;
    inventoryQuantity = builder.inventoryQuantity;
  }

  /**
   * Get the storage bin.
   *
   * @return the storage bin.
   */
  public String getStoragebin() {
    return storagebin;
  }

  /**
   * Get the inventory quantity.
   *
   * @return the inventory quantity.
   */
  public String getInventoryQuantity() {
    return inventoryQuantity;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return "CreateInventoryCountListData [storagebin=" + storagebin + ", inventoryQuantity="
        + inventoryQuantity + "]";
  }
}
