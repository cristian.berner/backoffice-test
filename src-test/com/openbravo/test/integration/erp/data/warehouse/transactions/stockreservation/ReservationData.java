/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:20
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for StockReservationReservationData
 *
 * @author aferraz
 *
 */
public class ReservationData extends DataObject {

  /**
   * Class builder
   *
   * @author aferraz
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: The organization.
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: The product.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The quantity.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the reservedQuantity value
     *
     * Description: The reservedQuantity.
     *
     * @param value
     *          The reservedQuantity value.
     * @return The builder for this class.
     */
    public Builder reservedQuantity(String value) {
      this.dataFields.put("reservedQty", value);
      return this;
    }

    /**
     * Set the releasedQuantity value
     *
     * Description: The releasedQuantity.
     *
     * @param value
     *          The releasedQuantity value.
     * @return The builder for this class.
     */
    public Builder releasedQuantity(String value) {
      this.dataFields.put("released", value);
      return this;
    }

    /**
     * Set the uom value
     *
     * Description: The uom.
     *
     * @param value
     *          The uom value.
     * @return The builder for this class.
     */
    public Builder uom(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: The salesOrderLine.
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The warehouse.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * Description: The storageBin.
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: The attributeSetValue.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: The status.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("rESStatus", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReservationData build() {
      return new ReservationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReservationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
