/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-01-25 18:28:18
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.comt>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

/**
 *
 * Class for business partner selector data.
 *
 * @author elopio.
 *
 */
public class BusinessPartnerSelectorData extends SelectorDataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the name value.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the value value.
     *
     * @param value
     *          The value value.
     * @return The builder for this class.
     */
    public Builder value(String value) {
      this.dataFields.put("value", value);
      return this;
    }

    /**
     * Set the creditAvailable value.
     *
     * @param value
     *          The creditAvailable value.
     * @return The builder for this class.
     */
    public Builder creditAvailable(String value) {
      this.dataFields.put("creditAvailable", value);
      return this;
    }

    /**
     * Set the creditUsed value.
     *
     * @param value
     *          The creditUsed value.
     * @return The builder for this class.
     */
    public Builder creditUsed(String value) {
      this.dataFields.put("creditUsed", value);
      return this;
    }

    /**
     * Set the customer value.
     *
     * @param value
     *          The customer value.
     * @return The builder for this class.
     */
    public Builder customer(Boolean value) {
      this.dataFields.put("customer", value);
      return this;
    }

    /**
     * Set the vendor value.
     *
     * @param value
     *          The vendor value.
     * @return The builder for this class.
     */
    public Builder vendor(Boolean value) {
      this.dataFields.put("vendor", value);
      return this;
    }

    /**
     * Set the income value.
     *
     * @param value
     *          The income value.
     * @return The builder for this class.
     */
    public Builder income(String value) {
      this.dataFields.put("income", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BusinessPartnerSelectorData build() {
      return new BusinessPartnerSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BusinessPartnerSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the search key of the business partner selector data object.
   *
   * @return the search key of the business partner selector data object.
   */
  @Override
  public String getSearchKey() {
    return (String) dataFields.get("value");
  }

  /**
   * Get the display value of the selector data object.
   *
   * @return the value displayed by the selector data object.
   */
  @Override
  public String getDisplayValue() {
    return (String) dataFields.get("name");
  }

}
