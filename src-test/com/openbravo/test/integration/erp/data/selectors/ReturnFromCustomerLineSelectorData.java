/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-08-15 12:37:14
 * Contributor(s):
 *   Andy Armaignac <collazoandy4@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.data.selectors;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for Return From Customer Line Selector
 *
 * @author Andy Armaignac
 *
 */
public class ReturnFromCustomerLineSelectorData extends DataObject {

  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the shipmentNumber value.
     *
     * @param value
     *          The Shipment number.
     * @return The builder for this class.
     */
    public Builder shipmentNumber(String value) {
      this.dataFields.put("shipmentNumber", value);
      return this;
    }

    /**
     * Set the product value.
     *
     * @param value
     *          The Product.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the returnReason value.
     *
     * @param value
     *          The Reason for returning the product.
     * @return The builder for this class.
     */
    public Builder returnReason(String value) {
      this.dataFields.put("returnReason", value);
      return this;
    }

    /**
     * Set the returned value.
     *
     * @param value
     *          The Quantity returned.
     * @return The builder for this class.
     */
    public Builder returned(String value) {
      this.dataFields.put("returned", value);
      return this;
    }

    /**
     * Set the returned quantity in other RM value.
     *
     * @param value
     *          The Quantity returned.
     * @return The builder for this class.
     */
    public Builder returnedQtyOtherRM(String value) {
      this.dataFields.put("returnQtyOtherRM", value);
      return this;
    }

    /**
     * Set the returned UOM value
     *
     * @param value
     *          The returned UOM
     * @return The builder for this class.
     */
    public Builder returnedUOM(String value) {
      this.dataFields.put("returnedUOM", value);
      return this;
    }

    /**
     * Set the returned value.
     *
     * @param value
     *          The Quantity returned.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the attributeSetValue value.
     *
     * @param value
     *          The Attribute Set value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnFromCustomerLineSelectorData build() {
      return new ReturnFromCustomerLineSelectorData(this);
    }
  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnFromCustomerLineSelectorData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
