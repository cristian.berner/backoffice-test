/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingtemplates;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TemplatesData
 *
 * @author plujan
 *
 */
public class TemplatesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the aDClientID value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The aDClientID value.
     * @return The builder for this class.
     */
    public Builder aDClientID(String value) {
      this.dataFields.put("aDClientID", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the isactive value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The isactive value.
     * @return The builder for this class.
     */
    public Builder isactive(Boolean value) {
      this.dataFields.put("isactive", value);
      return this;
    }

    /**
     * Set the classname value
     *
     * Description: x not implemented
     *
     * @param value
     *          The classname value.
     * @return The builder for this class.
     */
    public Builder classname(String value) {
      this.dataFields.put("classname", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the aDTableID value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The aDTableID value.
     * @return The builder for this class.
     */
    public Builder aDTableID(String value) {
      this.dataFields.put("aDTableID", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TemplatesData build() {
      return new TemplatesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TemplatesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
