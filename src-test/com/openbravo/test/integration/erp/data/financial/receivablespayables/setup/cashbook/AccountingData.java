/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.cashbook;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingData
 *
 * @author plujan
 *
 */
public class AccountingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the cashTransfer value
     *
     * Description: Cash Transfer Clearing Account
     *
     * @param value
     *          The cashTransfer value.
     * @return The builder for this class.
     */
    public Builder cashTransfer(AccountSelectorData value) {
      this.dataFields.put("cashTransfer", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the cashBookReceipt value
     *
     * Description: Cash Book Receipts Account
     *
     * @param value
     *          The cashBookReceipt value.
     * @return The builder for this class.
     */
    public Builder cashBookReceipt(AccountSelectorData value) {
      this.dataFields.put("cashBookReceipt", value);
      return this;
    }

    /**
     * Set the cashBookExpense value
     *
     * Description: Cash Book Expense Account
     *
     * @param value
     *          The cashBookExpense value.
     * @return The builder for this class.
     */
    public Builder cashBookExpense(AccountSelectorData value) {
      this.dataFields.put("cashBookExpense", value);
      return this;
    }

    /**
     * Set the cashBookDifferences value
     *
     * Description: Cash Book Differences Account
     *
     * @param value
     *          The cashBookDifferences value.
     * @return The builder for this class.
     */
    public Builder cashBookDifferences(AccountSelectorData value) {
      this.dataFields.put("cashBookDifferences", value);
      return this;
    }

    /**
     * Set the cashBookAsset value
     *
     * Description: Cash Book Asset Account
     *
     * @param value
     *          The cashBookAsset value.
     * @return The builder for this class.
     */
    public Builder cashBookAsset(AccountSelectorData value) {
      this.dataFields.put("cashBookAsset", value);
      return this;
    }

    /**
     * Set the cashbook value
     *
     * Description: A document used to manage all cash transactions.
     *
     * @param value
     *          The cashbook value.
     * @return The builder for this class.
     */
    public Builder cashbook(String value) {
      this.dataFields.put("cashbook", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingData build() {
      return new AccountingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
