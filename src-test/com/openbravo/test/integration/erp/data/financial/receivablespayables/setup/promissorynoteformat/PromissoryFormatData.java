/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.promissorynoteformat;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PromissoryFormatData
 *
 * @author plujan
 *
 */
public class PromissoryFormatData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the sourceYearToDay value
     *
     * Description: Source Year to Day
     *
     * @param value
     *          The sourceYearToDay value.
     * @return The builder for this class.
     */
    public Builder sourceYearToDay(String value) {
      this.dataFields.put("sourceYearToDay", value);
      return this;
    }

    /**
     * Set the sourceMonthToDay value
     *
     * Description: Source Month to Day
     *
     * @param value
     *          The sourceMonthToDay value.
     * @return The builder for this class.
     */
    public Builder sourceMonthToDay(String value) {
      this.dataFields.put("sourceMonthToDay", value);
      return this;
    }

    /**
     * Set the sourceDayToday value
     *
     * Description: Source Day Today
     *
     * @param value
     *          The sourceDayToday value.
     * @return The builder for this class.
     */
    public Builder sourceDayToday(String value) {
      this.dataFields.put("sourceDayToday", value);
      return this;
    }

    /**
     * Set the sourceAmountText value
     *
     * Description: Source Amount Text
     *
     * @param value
     *          The sourceAmountText value.
     * @return The builder for this class.
     */
    public Builder sourceAmountText(String value) {
      this.dataFields.put("sourceAmountText", value);
      return this;
    }

    /**
     * Set the sourceToBusinessPartner value
     *
     * Description: Source to Business Partner
     *
     * @param value
     *          The sourceToBusinessPartner value.
     * @return The builder for this class.
     */
    public Builder sourceToBusinessPartner(String value) {
      this.dataFields.put("sourceToBusinessPartner", value);
      return this;
    }

    /**
     * Set the sourceAmount value
     *
     * Description: Source Amount
     *
     * @param value
     *          The sourceAmount value.
     * @return The builder for this class.
     */
    public Builder sourceAmount(String value) {
      this.dataFields.put("sourceAmount", value);
      return this;
    }

    /**
     * Set the sourceLastYear value
     *
     * Description: Source Last Year
     *
     * @param value
     *          The sourceLastYear value.
     * @return The builder for this class.
     */
    public Builder sourceLastYear(String value) {
      this.dataFields.put("sourceLastYear", value);
      return this;
    }

    /**
     * Set the sourcePlannedMonth value
     *
     * Description: Source Planned Month
     *
     * @param value
     *          The sourcePlannedMonth value.
     * @return The builder for this class.
     */
    public Builder sourcePlannedMonth(String value) {
      this.dataFields.put("sourcePlannedMonth", value);
      return this;
    }

    /**
     * Set the sourcePlannedDay value
     *
     * Description: Source Planned Day
     *
     * @param value
     *          The sourcePlannedDay value.
     * @return The builder for this class.
     */
    public Builder sourcePlannedDay(String value) {
      this.dataFields.put("sourcePlannedDay", value);
      return this;
    }

    /**
     * Set the printBankLocation value
     *
     * Description: Print Bank Location
     *
     * @param value
     *          The printBankLocation value.
     * @return The builder for this class.
     */
    public Builder printBankLocation(Boolean value) {
      this.dataFields.put("printBankLocation", value);
      return this;
    }

    /**
     * Set the param43 value
     *
     * Description: Param. 43
     *
     * @param value
     *          The param43 value.
     * @return The builder for this class.
     */
    public Builder param43(String value) {
      this.dataFields.put("param43", value);
      return this;
    }

    /**
     * Set the param42 value
     *
     * Description: Param 42
     *
     * @param value
     *          The param42 value.
     * @return The builder for this class.
     */
    public Builder param42(String value) {
      this.dataFields.put("param42", value);
      return this;
    }

    /**
     * Set the param41 value
     *
     * Description: Param. 41
     *
     * @param value
     *          The param41 value.
     * @return The builder for this class.
     */
    public Builder param41(String value) {
      this.dataFields.put("param41", value);
      return this;
    }

    /**
     * Set the param32 value
     *
     * Description: Param. 32
     *
     * @param value
     *          The param32 value.
     * @return The builder for this class.
     */
    public Builder param32(String value) {
      this.dataFields.put("param32", value);
      return this;
    }

    /**
     * Set the param31 value
     *
     * Description: Param. 31
     *
     * @param value
     *          The param31 value.
     * @return The builder for this class.
     */
    public Builder param31(String value) {
      this.dataFields.put("param31", value);
      return this;
    }

    /**
     * Set the param13 value
     *
     * Description: Param 13
     *
     * @param value
     *          The param13 value.
     * @return The builder for this class.
     */
    public Builder param13(String value) {
      this.dataFields.put("param13", value);
      return this;
    }

    /**
     * Set the param12 value
     *
     * Description: Param. 12
     *
     * @param value
     *          The param12 value.
     * @return The builder for this class.
     */
    public Builder param12(String value) {
      this.dataFields.put("param12", value);
      return this;
    }

    /**
     * Set the param11 value
     *
     * Description: Param. 11
     *
     * @param value
     *          The param11 value.
     * @return The builder for this class.
     */
    public Builder param11(String value) {
      this.dataFields.put("param11", value);
      return this;
    }

    /**
     * Set the line4Left value
     *
     * Description: Distance from the left side for the 4th line.
     *
     * @param value
     *          The line4Left value.
     * @return The builder for this class.
     */
    public Builder line4Left(String value) {
      this.dataFields.put("line4Left", value);
      return this;
    }

    /**
     * Set the line4Top value
     *
     * Description: Line 4 Top
     *
     * @param value
     *          The line4Top value.
     * @return The builder for this class.
     */
    public Builder line4Top(String value) {
      this.dataFields.put("line4Top", value);
      return this;
    }

    /**
     * Set the line3Left value
     *
     * Description: Distance from the left side for the 3rd line.
     *
     * @param value
     *          The line3Left value.
     * @return The builder for this class.
     */
    public Builder line3Left(String value) {
      this.dataFields.put("line3Left", value);
      return this;
    }

    /**
     * Set the line3Top value
     *
     * Description: Line 3 Top
     *
     * @param value
     *          The line3Top value.
     * @return The builder for this class.
     */
    public Builder line3Top(String value) {
      this.dataFields.put("line3Top", value);
      return this;
    }

    /**
     * Set the line2Left value
     *
     * Description: Distance from the left side for the 2nd line.
     *
     * @param value
     *          The line2Left value.
     * @return The builder for this class.
     */
    public Builder line2Left(String value) {
      this.dataFields.put("line2Left", value);
      return this;
    }

    /**
     * Set the line2Top value
     *
     * Description: Line 2 Top
     *
     * @param value
     *          The line2Top value.
     * @return The builder for this class.
     */
    public Builder line2Top(String value) {
      this.dataFields.put("line2Top", value);
      return this;
    }

    /**
     * Set the line1Left value
     *
     * Description: Line 1 Left
     *
     * @param value
     *          The line1Left value.
     * @return The builder for this class.
     */
    public Builder line1Left(String value) {
      this.dataFields.put("line1Left", value);
      return this;
    }

    /**
     * Set the line1Top value
     *
     * Description: Line 1 Top
     *
     * @param value
     *          The line1Top value.
     * @return The builder for this class.
     */
    public Builder line1Top(String value) {
      this.dataFields.put("line1Top", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: A monetary account of funds held in a recognized banking institution.
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PromissoryFormatData build() {
      return new PromissoryFormatData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PromissoryFormatData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
