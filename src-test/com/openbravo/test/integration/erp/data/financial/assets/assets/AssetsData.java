/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.assets.assets;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for AssetsData
 *
 * @author plujan
 *
 */
public class AssetsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the usableLifeMonths value
     *
     * Description: Months of the usable life of the asset
     *
     * @param value
     *          The usableLifeMonths value.
     * @return The builder for this class.
     */
    public Builder usableLifeMonths(String value) {
      this.dataFields.put("usableLifeMonths", value);
      return this;
    }

    /**
     * Set the previouslyDepreciatedAmt value
     *
     * Description: Previously Depreciated Amt.
     *
     * @param value
     *          The previouslyDepreciatedAmt value.
     * @return The builder for this class.
     */
    public Builder previouslyDepreciatedAmt(String value) {
      this.dataFields.put("previouslyDepreciatedAmt", value);
      return this;
    }

    /**
     * Set the calculateType value
     *
     * Description: Calculate type
     *
     * @param value
     *          The calculateType value.
     * @return The builder for this class.
     */
    public Builder calculateType(String value) {
      this.dataFields.put("calculateType", value);
      return this;
    }

    /**
     * Set the depreciatedPlan value
     *
     * Description: Depreciated plan
     *
     * @param value
     *          The depreciatedPlan value.
     * @return The builder for this class.
     */
    public Builder depreciatedPlan(String value) {
      this.dataFields.put("depreciatedPlan", value);
      return this;
    }

    /**
     * Set the depreciatedValue value
     *
     * Description: Depreciated value
     *
     * @param value
     *          The depreciatedValue value.
     * @return The builder for this class.
     */
    public Builder depreciatedValue(String value) {
      this.dataFields.put("depreciatedValue", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the amortize value
     *
     * Description: Asset schedule
     *
     * @param value
     *          The amortize value.
     * @return The builder for this class.
     */
    public Builder amortize(String value) {
      this.dataFields.put("amortize", value);
      return this;
    }

    /**
     * Set the depreciationAmt value
     *
     * Description: Depreciation Amount
     *
     * @param value
     *          The depreciationAmt value.
     * @return The builder for this class.
     */
    public Builder depreciationAmt(String value) {
      this.dataFields.put("depreciationAmt", value);
      return this;
    }

    /**
     * Set the depreciationType value
     *
     * Description: Depreciation Type
     *
     * @param value
     *          The depreciationType value.
     * @return The builder for this class.
     */
    public Builder depreciationType(String value) {
      this.dataFields.put("depreciationType", value);
      return this;
    }

    /**
     * Set the residualAssetValue value
     *
     * Description: Residual asset value amount
     *
     * @param value
     *          The residualAssetValue value.
     * @return The builder for this class.
     */
    public Builder residualAssetValue(String value) {
      this.dataFields.put("residualAssetValue", value);
      return this;
    }

    /**
     * Set the assetValue value
     *
     * Description: Asset value
     *
     * @param value
     *          The assetValue value.
     * @return The builder for this class.
     */
    public Builder assetValue(String value) {
      this.dataFields.put("assetValue", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the annualDepreciation value
     *
     * Description: Depreciation annual %
     *
     * @param value
     *          The annualDepreciation value.
     * @return The builder for this class.
     */
    public Builder annualDepreciation(String value) {
      this.dataFields.put("annualDepreciation", value);
      return this;
    }

    /**
     * Set the depreciationEndDate value
     *
     * Description: Depreciation end date
     *
     * @param value
     *          The depreciationEndDate value.
     * @return The builder for this class.
     */
    public Builder depreciationEndDate(String value) {
      this.dataFields.put("depreciationEndDate", value);
      return this;
    }

    /**
     * Set the depreciationStartDate value
     *
     * Description: Depreciation Start Date
     *
     * @param value
     *          The depreciationStartDate value.
     * @return The builder for this class.
     */
    public Builder depreciationStartDate(String value) {
      this.dataFields.put("depreciationStartDate", value);
      return this;
    }

    /**
     * Set the cancellationDate value
     *
     * Description: Cancellation date
     *
     * @param value
     *          The cancellationDate value.
     * @return The builder for this class.
     */
    public Builder cancellationDate(String value) {
      this.dataFields.put("cancellationDate", value);
      return this;
    }

    /**
     * Set the purchaseDate value
     *
     * Description: Purchase date
     *
     * @param value
     *          The purchaseDate value.
     * @return The builder for this class.
     */
    public Builder purchaseDate(String value) {
      this.dataFields.put("purchaseDate", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the depreciate value
     *
     * Description: x not implemented
     *
     * @param value
     *          The depreciate value.
     * @return The builder for this class.
     */
    public Builder depreciate(Boolean value) {
      this.dataFields.put("depreciate", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the usableLifeYears value
     *
     * Description: Years of the usable life of the asset
     *
     * @param value
     *          The usableLifeYears value.
     * @return The builder for this class.
     */
    public Builder usableLifeYears(String value) {
      this.dataFields.put("usableLifeYears", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the assetCategory value
     *
     * Description: A classification of assets based on similar characteristics.
     *
     * @param value
     *          The assetCategory value.
     * @return The builder for this class.
     */
    public Builder assetCategory(String value) {
      this.dataFields.put("assetCategory", value);
      return this;
    }

    /**
     * Set the fullyDepreciated value
     *
     * Description: The asset is fully depreciated
     *
     * @param value
     *          The fullyDepreciated value.
     * @return The builder for this class.
     */
    public Builder fullyDepreciated(Boolean value) {
      this.dataFields.put("fullyDepreciated", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AssetsData build() {
      return new AssetsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AssetsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
