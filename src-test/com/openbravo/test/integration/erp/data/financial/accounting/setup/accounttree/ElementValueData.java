/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accounttree;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ElementValueData
 *
 * @author plujan
 *
 */
public class ElementValueData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the showValueCondition value
     *
     * Description: Show Value Condition
     *
     * @param value
     *          The showValueCondition value.
     * @return The builder for this class.
     */
    public Builder showValueCondition(String value) {
      this.dataFields.put("showValueCondition", value);
      return this;
    }

    /**
     * Set the elementLevel value
     *
     * Description: Element Level
     *
     * @param value
     *          The elementLevel value.
     * @return The builder for this class.
     */
    public Builder elementLevel(String value) {
      this.dataFields.put("elementLevel", value);
      return this;
    }

    /**
     * Set the elementShown value
     *
     * Description: Show element
     *
     * @param value
     *          The elementShown value.
     * @return The builder for this class.
     */
    public Builder elementShown(Boolean value) {
      this.dataFields.put("elementShown", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: A monetary account of funds held in a recognized banking institution.
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the foreignCurrencyAccount value
     *
     * Description: Balances in foreign currency accounts are held in the nominated currency
     *
     * @param value
     *          The foreignCurrencyAccount value.
     * @return The builder for this class.
     */
    public Builder foreignCurrencyAccount(Boolean value) {
      this.dataFields.put("foreignCurrencyAccount", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the summaryLevel value
     *
     * Description: A means of grouping fields in order to view or hide additional information.
     *
     * @param value
     *          The summaryLevel value.
     * @return The builder for this class.
     */
    public Builder summaryLevel(Boolean value) {
      this.dataFields.put("summaryLevel", value);
      return this;
    }

    /**
     * Set the accountSign value
     *
     * Description: Indicates the Natural Sign of the Account as a Debit or Credit
     *
     * @param value
     *          The accountSign value.
     * @return The builder for this class.
     */
    public Builder accountSign(String value) {
      this.dataFields.put("accountSign", value);
      return this;
    }

    /**
     * Set the accountType value
     *
     * Description: Indicates the type of account
     *
     * @param value
     *          The accountType value.
     * @return The builder for this class.
     */
    public Builder accountType(String value) {
      this.dataFields.put("accountType", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the accountingElement value
     *
     * Description: A unique identifier for an account type.
     *
     * @param value
     *          The accountingElement value.
     * @return The builder for this class.
     */
    public Builder accountingElement(String value) {
      this.dataFields.put("accountingElement", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ElementValueData build() {
      return new ElementValueData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ElementValueData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
