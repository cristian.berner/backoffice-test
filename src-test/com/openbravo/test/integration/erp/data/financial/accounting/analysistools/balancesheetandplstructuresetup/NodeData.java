/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.analysistools.balancesheetandplstructuresetup;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for NodeData
 *
 * @author plujan
 *
 */
public class NodeData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the accountElement value
     *
     * Description: A identification code for an account type.
     *
     * @param value
     *          The accountElement value.
     * @return The builder for this class.
     */
    public Builder accountElement(String value) {
      this.dataFields.put("accountElement", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the group value
     *
     * Description: Acct Rpt Group
     *
     * @param value
     *          The group value.
     * @return The builder for this class.
     */
    public Builder group(String value) {
      this.dataFields.put("group", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public NodeData build() {
      return new NodeData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private NodeData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
