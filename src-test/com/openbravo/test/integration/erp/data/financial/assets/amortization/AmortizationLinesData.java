/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.assets.amortization;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AmortizationLinesData
 *
 * @author plujan
 *
 */
public class AmortizationLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the amortizationAmount value
     *
     * Description: Amortization Amount
     *
     * @param value
     *          The amortizationAmount value.
     * @return The builder for this class.
     */
    public Builder amortizationAmount(String value) {
      this.dataFields.put("amortizationAmount", value);
      return this;
    }

    /**
     * Set the amortizationPercentage value
     *
     * Description: Amortization Percentage
     *
     * @param value
     *          The amortizationPercentage value.
     * @return The builder for this class.
     */
    public Builder amortizationPercentage(String value) {
      this.dataFields.put("amortizationPercentage", value);
      return this;
    }

    /**
     * Set the asset value
     *
     * Description: An item which is owned and exchangeable for cash.
     *
     * @param value
     *          The asset value.
     * @return The builder for this class.
     */
    public Builder asset(String value) {
      this.dataFields.put("asset", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the amortization value
     *
     * Description: The depreciation or reduction of a product value over time.
     *
     * @param value
     *          The amortization value.
     * @return The builder for this class.
     */
    public Builder amortization(String value) {
      this.dataFields.put("amortization", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AmortizationLinesData build() {
      return new AmortizationLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AmortizationLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
