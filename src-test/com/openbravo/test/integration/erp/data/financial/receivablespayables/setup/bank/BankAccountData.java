/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.bank;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BankAccountData
 *
 * @author plujan
 *
 */
public class BankAccountData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the displayedAccount value
     *
     * Description: Text that will identify this bank account
     *
     * @param value
     *          The displayedAccount value.
     * @return The builder for this class.
     */
    public Builder displayedAccount(String value) {
      this.dataFields.put("displayedAccount", value);
      return this;
    }

    /**
     * Set the showIBAN value
     *
     * Description: Show IBAN
     *
     * @param value
     *          The showIBAN value.
     * @return The builder for this class.
     */
    public Builder showIBAN(Boolean value) {
      this.dataFields.put("showIBAN", value);
      return this;
    }

    /**
     * Set the showSpanish value
     *
     * Description: Show Spanish
     *
     * @param value
     *          The showSpanish value.
     * @return The builder for this class.
     */
    public Builder showSpanish(Boolean value) {
      this.dataFields.put("showSpanish", value);
      return this;
    }

    /**
     * Set the showGeneric value
     *
     * Description: Show Generic
     *
     * @param value
     *          The showGeneric value.
     * @return The builder for this class.
     */
    public Builder showGeneric(Boolean value) {
      this.dataFields.put("showGeneric", value);
      return this;
    }

    /**
     * Set the genericAccount value
     *
     * Description: Number of the bank account in a generic format. Usefull if the bank does not use
     * nor IBAN nor our country bank account number's structure
     *
     * @param value
     *          The genericAccount value.
     * @return The builder for this class.
     */
    public Builder genericAccount(String value) {
      this.dataFields.put("genericAccount", value);
      return this;
    }

    /**
     * Set the iBAN value
     *
     * Description: International standard for identifying bank accounts across national borders
     *
     * @param value
     *          The iBAN value.
     * @return The builder for this class.
     */
    public Builder iBAN(String value) {
      this.dataFields.put("iBAN", value);
      return this;
    }

    /**
     * Set the controlDigit value
     *
     * Description: Digit control
     *
     * @param value
     *          The controlDigit value.
     * @return The builder for this class.
     */
    public Builder controlDigit(String value) {
      this.dataFields.put("controlDigit", value);
      return this;
    }

    /**
     * Set the partialAccountNo value
     *
     * Description: Code of the account
     *
     * @param value
     *          The partialAccountNo value.
     * @return The builder for this class.
     */
    public Builder partialAccountNo(String value) {
      this.dataFields.put("partialAccountNo", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the bank value
     *
     * Description: Bank
     *
     * @param value
     *          The bank value.
     * @return The builder for this class.
     */
    public Builder bank(String value) {
      this.dataFields.put("bank", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BankAccountData build() {
      return new BankAccountData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BankAccountData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
