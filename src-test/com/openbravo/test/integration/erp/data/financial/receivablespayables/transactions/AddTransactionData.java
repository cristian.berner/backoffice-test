/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for Add Transaction Data.
 *
 * @author elopio
 *
 */
public class AddTransactionData extends DataObject {

  /**
   * Class builder
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Transaction Type value.
     *
     * @param value
     *          The Transaction Type value.
     * @return The builder for this class.
     */
    public Builder trxtype(String value) {
      this.dataFields.put("trxtype", value);
      return this;
    }

    /**
     * Set the Transaction Date value.
     *
     * @param value
     *          The Transaction Date value.
     * @return The builder for this class.
     */
    public Builder trxdate(String value) {
      this.dataFields.put("trxdate", value);
      return this;
    }

    /**
     * Set the Accounting Date value.
     *
     * @param value
     *          The Accounting Date value.
     * @return The builder for this class.
     */
    public Builder dateacct(String value) {
      this.dataFields.put("dateacct", value);
      return this;
    }

    /**
     * Set the Payment value.
     *
     * @param value
     *          The Payment value.
     * @return The builder for this class.
     */
    public Builder finPaymentId(String value) {
      this.dataFields.put("finPaymentId", value);
      return this;
    }

    /**
     * Set the GL Item value.
     *
     * @param value
     *          The GL Item value.
     * @return The builder for this class.
     */
    public Builder gLItem(String value) {
      this.dataFields.put("c_glitem_id", value);
      return this;
    }

    /**
     * Set the Description value.
     *
     * @param value
     *          The Description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the Currency value.
     *
     * @param value
     *          The Currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("c_currency_id", value);
      return this;
    }

    /**
     * Set the Deposit Amount value.
     *
     * @param value
     *          The Deposit Amount value.
     * @return The builder for this class.
     */
    public Builder depositamt(String value) {
      this.dataFields.put("depositamt", value);
      return this;
    }

    /**
     * Set the Withdrawal Amount value.
     *
     * @param value
     *          The Withdrawal Amount value.
     * @return The builder for this class.
     */
    public Builder withdrawalamt(String value) {
      this.dataFields.put("withdrawalamt", value);
      return this;
    }

    /**
     * Set the Organization Dimension value.
     *
     * @param value
     *          The Organization Dimension value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("ad_org_id", value);
      return this;
    }

    /**
     * Set the Business Partner Dimension value.
     *
     * @param value
     *          The Business Partner Dimension value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("c_bpartner_id", value);
      return this;
    }

    /**
     * Set the Project Dimension value.
     *
     * @param value
     *          The Project Dimension value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("c_project_id", value);
      return this;
    }

    /**
     * Set the Product Dimension value.
     *
     * @param value
     *          The Product Dimension value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("m_product_id", value);
      return this;
    }

    /**
     * Set the Cost Center Dimension value.
     *
     * @param value
     *          The Cost Center Dimension value.
     * @return The builder for this class.
     */
    public Builder costCenter(String value) {
      this.dataFields.put("costCenter", value);
      return this;
    }

    /**
     * Set the User 1 Dimension value.
     *
     * @param value
     *          The User 1 Dimension value.
     * @return The builder for this class.
     */
    public Builder user1(String value) {
      this.dataFields.put("user1", value);
      return this;
    }

    /**
     * Set the User 2 Dimension value.
     *
     * @param value
     *          The User 2 Dimension value.
     * @return The builder for this class.
     */
    public Builder user2(String value) {
      this.dataFields.put("user2", value);
      return this;
    }

    /**
     * Set the Campaign Dimension value.
     *
     * @param value
     *          The Campaign Dimension value.
     * @return The builder for this class.
     */
    public Builder campaign(String value) {
      this.dataFields.put("campaign", value);
      return this;
    }

    /**
     * Set the Activity Dimension value.
     *
     * @param value
     *          The Activity Dimension value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the Sales Region Dimension value.
     *
     * @param value
     *          The Sales Region Dimension value.
     * @return The builder for this class.
     */
    public Builder salesRegion(String value) {
      this.dataFields.put("salesRegion", value);
      return this;
    }

    /**
     * Set the Foreign Currency value.
     *
     * @param value
     *          The Foreign Currency value.
     * @return The builder for this class.
     */
    public Builder foreignCurrency(String value) {
      this.dataFields.put("foreignCurrency", value);
      return this;
    }

    /**
     * Set the Foreign Amount value.
     *
     * @param value
     *          The Foreign Amount value.
     * @return The builder for this class.
     */
    public Builder foreignAmount(String value) {
      this.dataFields.put("foreignAmount", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AddTransactionData build() {
      return new AddTransactionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AddTransactionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
