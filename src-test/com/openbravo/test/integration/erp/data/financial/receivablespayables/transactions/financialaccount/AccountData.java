/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;

/**
 *
 * Class for AccountData
 *
 * @author plujan
 *
 */
public class AccountData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the matchingAlgorithm value
     *
     * Description: Identifies the algorithm used to match the imported bank statement lines
     *
     * @param value
     *          The matchingAlgorithm value.
     * @return The builder for this class.
     */
    public Builder matchingAlgorithm(String value) {
      this.dataFields.put("matchingAlgorithm", value);
      return this;
    }

    /**
     * Set the initialBalance value
     *
     * Description: Initial balance of the financial account. Amount of the account at the time of
     * registering the financial account.
     *
     * @param value
     *          The initialBalance value.
     * @return The builder for this class.
     */
    public Builder initialBalance(String value) {
      this.dataFields.put("initialBalance", value);
      return this;
    }

    /**
     * Set the type value
     *
     * Description: A distinct item characteristic used for processes and sometimes grouped within a
     * category.
     *
     * @param value
     *          The type value.
     * @return The builder for this class.
     */
    public Builder type(String value) {
      this.dataFields.put("type", value);
      return this;
    }

    /**
     * Set the swiftCode value
     *
     * Description: Swift Code (Society of Worldwide Interbank Financial Telecommunications)
     *
     * @param value
     *          The swiftCode value.
     * @return The builder for this class.
     */
    public Builder swiftCode(String value) {
      this.dataFields.put("swiftCode", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the iBAN value
     *
     * Description: International standard for identifying bank accounts across national borders
     *
     * @param value
     *          The iBAN value.
     * @return The builder for this class.
     */
    public Builder iBAN(String value) {
      this.dataFields.put("iBAN", value);
      return this;
    }

    /**
     * Set the iNENo value
     *
     * Description: INE Number.
     *
     * @param value
     *          The iNENo value.
     * @return The builder for this class.
     */
    public Builder iNENo(String value) {
      this.dataFields.put("iNENo", value);
      return this;
    }

    /**
     * Set the currentBalance value
     *
     * Description: Current Balance
     *
     * @param value
     *          The currentBalance value.
     * @return The builder for this class.
     */
    public Builder currentBalance(String value) {
      this.dataFields.put("currentBalance", value);
      return this;
    }

    /**
     * Set the creditLimit value
     *
     * Description: Amount of Credit allowed
     *
     * @param value
     *          The creditLimit value.
     * @return The builder for this class.
     */
    public Builder creditLimit(String value) {
      this.dataFields.put("creditLimit", value);
      return this;
    }

    /**
     * Set the branchCode value
     *
     * Description: Code of the branch
     *
     * @param value
     *          The branchCode value.
     * @return The builder for this class.
     */
    public Builder branchCode(String value) {
      this.dataFields.put("branchCode", value);
      return this;
    }

    /**
     * Set the bankCode value
     *
     * Description: Code of the bank
     *
     * @param value
     *          The bankCode value.
     * @return The builder for this class.
     */
    public Builder bankCode(String value) {
      this.dataFields.put("bankCode", value);
      return this;
    }

    /**
     * Set the partialAccountNo value
     *
     * Description: Code of the account
     *
     * @param value
     *          The partialAccountNo value.
     * @return The builder for this class.
     */
    public Builder partialAccountNo(String value) {
      this.dataFields.put("partialAccountNo", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the bankDigitcontrol value
     *
     * Description: Bank_Digitcontrol
     *
     * @param value
     *          The bankDigitcontrol value.
     * @return The builder for this class.
     */
    public Builder bankDigitcontrol(String value) {
      this.dataFields.put("bankDigitcontrol", value);
      return this;
    }

    /**
     * Set the accountNo value
     *
     * Description: Account Number
     *
     * @param value
     *          The accountNo value.
     * @return The builder for this class.
     */
    public Builder accountNo(String value) {
      this.dataFields.put("accountNo", value);
      return this;
    }

    /**
     * Set the accountDigitcontrol value
     *
     * Description: Account_Digitcontrol
     *
     * @param value
     *          The accountDigitcontrol value.
     * @return The builder for this class.
     */
    public Builder accountDigitcontrol(String value) {
      this.dataFields.put("accountDigitcontrol", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountData build() {
      return new AccountData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
