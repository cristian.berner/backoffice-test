/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.documenttype;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for EmailDefinitionsData
 *
 * @author plujan
 *
 */
public class EmailDefinitionsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the subject value
     *
     * Description: The Subject of the mail
     *
     * @param value
     *          The subject value.
     * @return The builder for this class.
     */
    public Builder subject(String value) {
      this.dataFields.put("subject", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the pocDocumentType value
     *
     * Description: The document type
     *
     * @param value
     *          The pocDocumentType value.
     * @return The builder for this class.
     */
    public Builder pocDocumentType(String value) {
      this.dataFields.put("pocDocumentType", value);
      return this;
    }

    /**
     * Set the body value
     *
     * Description: The body side of the email
     *
     * @param value
     *          The body value.
     * @return The builder for this class.
     */
    public Builder body(String value) {
      this.dataFields.put("body", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public EmailDefinitionsData build() {
      return new EmailDefinitionsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private EmailDefinitionsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
