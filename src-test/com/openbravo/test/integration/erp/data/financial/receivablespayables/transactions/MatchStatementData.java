/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for Add Payment In Data.
 *
 * @author lorenzo.fidalgo
 *
 */
public class MatchStatementData extends DataObject {

  /**
   * Class builder
   *
   * @author lorenzo.fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the bank statement type value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The bank statement type value.
     * @return The builder for this class.
     */
    public Builder bankStatementType(String value) {
      this.dataFields.put("type", value);
      return this;
    }

    /**
     * Set the date value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The date value.
     * @return The builder for this class.
     */
    public Builder date(String value) {
      this.dataFields.put("banklineDate", value);
      return this;
    }

    /**
     * Set the sender value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The sender value.
     * @return The builder for this class.
     */
    public Builder sender(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the bpartnername value
     *
     * Description: bpartnername
     *
     * @param value
     *          The bpartnername value.
     * @return The builder for this class.
     */
    public Builder bpartnername(String value) {
      this.dataFields.put("bpartnername", value);
      return this;
    }

    /**
     * Set the referenceNo value
     *
     * Description: The number for a specific reference.
     *
     * @param value
     *          The referenceNo value.
     * @return The builder for this class.
     */
    public Builder referenceNo(String value) {
      this.dataFields.put("referenceNo", value);
      return this;
    }

    /**
     * Set the G/L item value
     *
     * Description: The number for a specific reference.
     *
     * @param value
     *          The G/L item value.
     * @return The builder for this class.
     */
    public Builder glitem(String value) {
      this.dataFields.put("gLItem", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: description
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the transaction description value
     *
     * Description: transaction description
     *
     * @param value
     *          The transaction description value.
     * @return The builder for this class.
     */
    public Builder trxDescription(String value) {
      this.dataFields.put("fatDescription", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Amount
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the affinity value
     *
     * Description: Level of affinity for the given line
     *
     * @param value
     *          The affinity value.
     * @return The builder for this class.
     */
    public Builder affinity(String value) {
      this.dataFields.put("affinity", value);
      return this;
    }

    /**
     * Set the matchedDocument value
     *
     * Description: Type of matched document for the given line
     *
     * @param value
     *          The matchedDocument value.
     * @return The builder for this class.
     */
    public Builder matchedDocument(String value) {
      this.dataFields.put("matchedDocument", value);
      return this;
    }

    /**
     * Set the matchingtype value
     *
     * Description: Type of matching for the given line
     *
     * @param value
     *          The matchingtype value.
     * @return The builder for this class.
     */
    public Builder matchingType(String value) {
      this.dataFields.put("matchingType", value);
      return this;
    }

    /**
     * Set the transactionDate value
     *
     * Description: The date that a specified transaction is entered into the application.
     *
     * @param value
     *          The transactionDate value.
     * @return The builder for this class.
     */
    public Builder transactionDate(String value) {
      this.dataFields.put("trxDate", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("transactionBPName", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Amount
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder transactionAmount(String value) {
      this.dataFields.put("transactionAmount", value);
      return this;
    }

    /**
     * Set the transaction GL Item value
     *
     * Description: transaction GL Item
     *
     * @param value
     *          The transactionGLItem value.
     * @return The builder for this class.
     */
    public Builder transactionGLItem(String value) {
      this.dataFields.put("transactionGLItem", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MatchStatementData build() {
      return new MatchStatementData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MatchStatementData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
