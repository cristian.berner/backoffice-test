/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AccountSchemaTablesData
 *
 * @author plujan
 *
 */
public class AccountSchemaTablesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the isBackgroundDisabled value
     *
     * Description: Disables the selected table for accounting in background process
     *
     * @param value
     *          The isBackgroundDisabled value.
     * @return The builder for this class.
     */
    public Builder isBackgroundDisabled(Boolean value) {
      this.dataFields.put("isBackgroundDisabled", value);
      return this;
    }

    /**
     * Set the aDCreatefactTemplateID value
     *
     * Description: Accounting Template
     *
     * @param value
     *          The aDCreatefactTemplateID value.
     * @return The builder for this class.
     */
    public Builder aDCreatefactTemplateID(String value) {
      this.dataFields.put("aDCreatefactTemplateID", value);
      return this;
    }

    /**
     * Set the sQLDescription value
     *
     * Description: Acct description
     *
     * @param value
     *          The sQLDescription value.
     * @return The builder for this class.
     */
    public Builder sQLDescription(String value) {
      this.dataFields.put("sQLDescription", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountSchemaTablesData build() {
      return new AccountSchemaTablesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountSchemaTablesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
