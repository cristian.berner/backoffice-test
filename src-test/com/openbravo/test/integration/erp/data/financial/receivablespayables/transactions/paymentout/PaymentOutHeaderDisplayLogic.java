/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentOutHeaderData
 *
 * @author lorenzo.fidalgo
 *
 */
public class PaymentOutHeaderDisplayLogic extends DataObject {

  /**
   * Class builder
   *
   * @author lorenzo.fidalgo
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the referenceNo value
     *
     * Description: The number for a specific reference.
     *
     * @param value
     *          The referenceNo value.
     * @return The builder for this class.
     */
    public Builder referenceNo(boolean value) {
      this.dataFields.put("referenceNo", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(boolean value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Business Partner
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(boolean value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(boolean value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Amount
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(boolean value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the writeOffAmount value
     *
     * Description: A monetary sum that can be deducted from tax obligations.
     *
     * @param value
     *          The writeOffAmount value.
     * @return The builder for this class.
     */
    public Builder writeOffAmount(boolean value) {
      this.dataFields.put("writeOffAmount", value);
      return this;
    }

    /**
     * Set the paymentDate value
     *
     * Description: Payment Date
     *
     * @param value
     *          The paymentDate value.
     * @return The builder for this class.
     */
    public Builder paymentDate(boolean value) {
      this.dataFields.put("paymentDate", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(boolean value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the usedCredit value
     *
     * Description: Used_Credit
     *
     * @param value
     *          The usedCredit value.
     * @return The builder for this class.
     */
    public Builder usedCredit(boolean value) {
      this.dataFields.put("usedCredit", value);
      return this;
    }

    /**
     * Set the generatedCredit value
     *
     * Description: Generated_Credit
     *
     * @param value
     *          The generatedCredit value.
     * @return The builder for this class.
     */
    public Builder generatedCredit(boolean value) {
      this.dataFields.put("generatedCredit", value);
      return this;
    }

    /**
     * Set the documentType value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(boolean value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(boolean value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the account value
     *
     * Description: Account
     *
     * @param value
     *          The account value.
     * @return The builder for this class.
     */
    public Builder account(boolean value) {
      this.dataFields.put("account", value);
      return this;
    }

    /**
     * Set the paymentMethod value
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(boolean value) {
      this.dataFields.put("paymentMethod", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(boolean value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentOutHeaderDisplayLogic build() {
      return new PaymentOutHeaderDisplayLogic(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentOutHeaderDisplayLogic(Builder builder) {
    dataFields = builder.dataFields;
  }

}
