/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.assets.assets;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingData
 *
 * @author plujan
 *
 */
public class AccountingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the accumulatedDepreciation value
     *
     * Description: Accumulated Depreciation
     *
     * @param value
     *          The accumulatedDepreciation value.
     * @return The builder for this class.
     */
    public Builder accumulatedDepreciation(AccountSelectorData value) {
      this.dataFields.put("accumulatedDepreciation", value);
      return this;
    }

    /**
     * Set the depreciation value
     *
     * Description: Depreciation account
     *
     * @param value
     *          The depreciation value.
     * @return The builder for this class.
     */
    public Builder depreciation(AccountSelectorData value) {
      this.dataFields.put("depreciation", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingData build() {
      return new AccountingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
