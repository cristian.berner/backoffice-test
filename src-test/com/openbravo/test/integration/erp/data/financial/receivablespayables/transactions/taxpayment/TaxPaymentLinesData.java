/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.taxpayment;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TaxPaymentLinesData
 *
 * @author plujan
 *
 */
public class TaxPaymentLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the noVATAmount value
     *
     * Description: No VAT Amount
     *
     * @param value
     *          The noVATAmount value.
     * @return The builder for this class.
     */
    public Builder noVATAmount(String value) {
      this.dataFields.put("noVATAmount", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the exemptAmount value
     *
     * Description: Exempt amount
     *
     * @param value
     *          The exemptAmount value.
     * @return The builder for this class.
     */
    public Builder exemptAmount(String value) {
      this.dataFields.put("exemptAmount", value);
      return this;
    }

    /**
     * Set the taxRegister value
     *
     * Description: Tax register
     *
     * @param value
     *          The taxRegister value.
     * @return The builder for this class.
     */
    public Builder taxRegister(String value) {
      this.dataFields.put("taxRegister", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the taxAmount value
     *
     * Description: The total sum of money requested by the government of the specified transaction.
     *
     * @param value
     *          The taxAmount value.
     * @return The builder for this class.
     */
    public Builder taxAmount(String value) {
      this.dataFields.put("taxAmount", value);
      return this;
    }

    /**
     * Set the taxableAmount value
     *
     * Description: The total sum on which taxes are added.
     *
     * @param value
     *          The taxableAmount value.
     * @return The builder for this class.
     */
    public Builder taxableAmount(String value) {
      this.dataFields.put("taxableAmount", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the invoiceDate value
     *
     * Description: Invoice Date
     *
     * @param value
     *          The invoiceDate value.
     * @return The builder for this class.
     */
    public Builder invoiceDate(String value) {
      this.dataFields.put("invoiceDate", value);
      return this;
    }

    /**
     * Set the invoiceTax value
     *
     * Description: Invoice Tax
     *
     * @param value
     *          The invoiceTax value.
     * @return The builder for this class.
     */
    public Builder invoiceTax(String value) {
      this.dataFields.put("invoiceTax", value);
      return this;
    }

    /**
     * Set the undeductableAmount value
     *
     * Description: Tax Undeductable Amount
     *
     * @param value
     *          The undeductableAmount value.
     * @return The builder for this class.
     */
    public Builder undeductableAmount(String value) {
      this.dataFields.put("undeductableAmount", value);
      return this;
    }

    /**
     * Set the totalAmount value
     *
     * Description: Total Amount
     *
     * @param value
     *          The totalAmount value.
     * @return The builder for this class.
     */
    public Builder totalAmount(String value) {
      this.dataFields.put("totalAmount", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TaxPaymentLinesData build() {
      return new TaxPaymentLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TaxPaymentLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
