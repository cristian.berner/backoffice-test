/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.bank;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.AccountSelectorData;

/**
 *
 * Class for AccountingData
 *
 * @author plujan
 *
 */
public class AccountingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the bankRevaluationLoss value
     *
     * Description: Bank Revaluation Loss Account
     *
     * @param value
     *          The bankRevaluationLoss value.
     * @return The builder for this class.
     */
    public Builder bankRevaluationLoss(AccountSelectorData value) {
      this.dataFields.put("bankRevaluationLoss", value);
      return this;
    }

    /**
     * Set the bankInTransit value
     *
     * Description: Bank In Transit Account
     *
     * @param value
     *          The bankInTransit value.
     * @return The builder for this class.
     */
    public Builder bankInTransit(AccountSelectorData value) {
      this.dataFields.put("bankInTransit", value);
      return this;
    }

    /**
     * Set the bankAccount value
     *
     * Description: A monetary account of funds held in a recognized banking institution.
     *
     * @param value
     *          The bankAccount value.
     * @return The builder for this class.
     */
    public Builder bankAccount(String value) {
      this.dataFields.put("bankAccount", value);
      return this;
    }

    /**
     * Set the bankRevaluationGain value
     *
     * Description: Bank Revaluation Gain Account
     *
     * @param value
     *          The bankRevaluationGain value.
     * @return The builder for this class.
     */
    public Builder bankRevaluationGain(AccountSelectorData value) {
      this.dataFields.put("bankRevaluationGain", value);
      return this;
    }

    /**
     * Set the bankExpense value
     *
     * Description: Bank Expense Account
     *
     * @param value
     *          The bankExpense value.
     * @return The builder for this class.
     */
    public Builder bankExpense(AccountSelectorData value) {
      this.dataFields.put("bankExpense", value);
      return this;
    }

    /**
     * Set the bankAsset value
     *
     * Description: Bank Asset Account
     *
     * @param value
     *          The bankAsset value.
     * @return The builder for this class.
     */
    public Builder bankAsset(AccountSelectorData value) {
      this.dataFields.put("bankAsset", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AccountingData build() {
      return new AccountingData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AccountingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
