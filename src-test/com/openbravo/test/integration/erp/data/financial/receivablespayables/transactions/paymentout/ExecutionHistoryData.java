/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ExecutionHistoryData
 *
 * @author plujan
 *
 */
public class ExecutionHistoryData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the executionDate value
     *
     * Description: Execution Date
     *
     * @param value
     *          The executionDate value.
     * @return The builder for this class.
     */
    public Builder executionDate(String value) {
      this.dataFields.put("executionDate", value);
      return this;
    }

    /**
     * Set the paymentRun value
     *
     * Description: Payment Run
     *
     * @param value
     *          The paymentRun value.
     * @return The builder for this class.
     */
    public Builder paymentRun(String value) {
      this.dataFields.put("paymentRun", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the paymentRunStatus value
     *
     * Description: Payment Run Status
     *
     * @param value
     *          The paymentRunStatus value.
     * @return The builder for this class.
     */
    public Builder paymentRunStatus(String value) {
      this.dataFields.put("paymentRunStatus", value);
      return this;
    }

    /**
     * Set the sourceOfTheExecution value
     *
     * Description: Source of the Execution
     *
     * @param value
     *          The sourceOfTheExecution value.
     * @return The builder for this class.
     */
    public Builder sourceOfTheExecution(String value) {
      this.dataFields.put("sourceOfTheExecution", value);
      return this;
    }

    /**
     * Set the paymentRunMessage value
     *
     * Description: Payment Run Message
     *
     * @param value
     *          The paymentRunMessage value.
     * @return The builder for this class.
     */
    public Builder paymentRunMessage(String value) {
      this.dataFields.put("paymentRunMessage", value);
      return this;
    }

    /**
     * Set the paymentExecutionResult value
     *
     * Description: Payment Execution Result
     *
     * @param value
     *          The paymentExecutionResult value.
     * @return The builder for this class.
     */
    public Builder paymentExecutionResult(String value) {
      this.dataFields.put("paymentExecutionResult", value);
      return this;
    }

    /**
     * Set the paymentExecutionMessage value
     *
     * Description: Payment Execution Message
     *
     * @param value
     *          The paymentExecutionMessage value.
     * @return The builder for this class.
     */
    public Builder paymentExecutionMessage(String value) {
      this.dataFields.put("paymentExecutionMessage", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ExecutionHistoryData build() {
      return new ExecutionHistoryData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ExecutionHistoryData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
