/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentOutLinesData
 *
 * @author plujan
 *
 */
public class PaymentOutLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the orderPaymentPlan value
     *
     * Description: Set of payments planned to be collected/paid for an order
     *
     * @param value
     *          The orderPaymentPlan value.
     * @return The builder for this class.
     */
    public Builder orderPaymentPlan(String value) {
      this.dataFields.put("orderPaymentPlan", value);
      return this;
    }

    /**
     * Set the paidAmount value
     *
     * Description: Paid Amount
     *
     * @param value
     *          The paid value.
     * @return The builder for this class.
     */
    public Builder paid(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the invoicePaymentPlan value
     *
     * Description: Set of payments planned to be collected/paid for an invoice
     *
     * @param value
     *          The invoicePaymentPlan value.
     * @return The builder for this class.
     */
    public Builder invoicePaymentPlan(String value) {
      this.dataFields.put("invoicePaymentPlan", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: Payment event
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the glitemname value
     *
     * Description: Glitem name
     *
     * @param value
     *          The glitemname value.
     * @return The builder for this class.
     */
    public Builder glitemname(String value) {
      this.dataFields.put("paymentDetails$gLItem", value);
      return this;
    }

    /**
     * Set the invoiceAmount value
     *
     * Description: The monetary sum that is invoiced for a specified item or service.
     *
     * @param value
     *          The invoiceAmount value.
     * @return The builder for this class.
     */
    public Builder invoiceAmount(String value) {
      this.dataFields.put("invoiceAmount", value);
      return this;
    }

    /**
     * Set the expected value
     *
     * Description: Expected Amount
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("expected", value);
      return this;
    }

    /**
     * Set the orderno value
     *
     * Description: Order Number
     *
     * @param value
     *          The orderno value.
     * @return The builder for this class.
     */
    public Builder orderno(String value) {
      this.dataFields.put("orderPaymentSchedule$order$documentNo", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the invoiceno value
     *
     * Description: Invoice Number
     *
     * @param value
     *          The invoiceno value.
     * @return The builder for this class.
     */
    public Builder invoiceno(String value) {
      this.dataFields.put("invoicePaymentSchedule$invoice$documentNo", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentOutLinesData build() {
      return new PaymentOutLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentOutLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
