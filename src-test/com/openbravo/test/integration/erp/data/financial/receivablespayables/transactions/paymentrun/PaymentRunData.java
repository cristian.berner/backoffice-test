/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentrun;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentRunData
 *
 * @author plujan
 *
 */
public class PaymentRunData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the creationDate value
     *
     * Description: Indicates the date that this payment run was executed.
     *
     * @param value
     *          The creationDate value.
     * @return The builder for this class.
     */
    public Builder creationDate(String value) {
      this.dataFields.put("creationDate", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the message value
     *
     * Description: Message
     *
     * @param value
     *          The message value.
     * @return The builder for this class.
     */
    public Builder message(String value) {
      this.dataFields.put("message", value);
      return this;
    }

    /**
     * Set the paymentExecutionProcess value
     *
     * Description: Payment Execution Process
     *
     * @param value
     *          The paymentExecutionProcess value.
     * @return The builder for this class.
     */
    public Builder paymentExecutionProcess(String value) {
      this.dataFields.put("paymentExecutionProcess", value);
      return this;
    }

    /**
     * Set the sourceOfTheExecution value
     *
     * Description: Source of the Execution
     *
     * @param value
     *          The sourceOfTheExecution value.
     * @return The builder for this class.
     */
    public Builder sourceOfTheExecution(String value) {
      this.dataFields.put("sourceOfTheExecution", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentRunData build() {
      return new PaymentRunData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentRunData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
