/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentDetailsData
 *
 * @author plujan
 *
 */
public class PaymentDetailsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the paymentDate value
     *
     * Description: Payment Date
     *
     * @param value
     *          The paymentDate value.
     * @return The builder for this class.
     */
    public Builder paymentDate(String value) {
      this.dataFields.put("paymentDate", value);
      return this;
    }

    /**
     * Set the expected value
     *
     * Description: Expected Amount
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("expected", value);
      return this;
    }

    /**
     * Set the payment value
     *
     * Description: Payment event
     *
     * @param value
     *          The payment value.
     * @return The builder for this class.
     */
    public Builder payment(String value) {
      this.dataFields.put("payment", value);
      return this;
    }

    /**
     * Set the paidAmount value
     *
     * Description: Paid Amount
     *
     * @param value
     *          The paid value.
     * @return The builder for this class.
     */
    public Builder paid(String value) {
      this.dataFields.put("paidAmount", value);
      return this;
    }

    /**
     * Set the writeoffAmount value
     *
     * Description: A monetary sum that can be deducted from tax obligations.
     *
     * @param value
     *          The writeoff value.
     * @return The builder for this class.
     */
    public Builder writeoff(String value) {
      this.dataFields.put("writeoffAmount", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: Payment Status
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the expected converted value
     *
     * Description: Expected Amount (Account Currency).
     *
     * @param value
     *          The expectedConverted value.
     * @return The builder for this class.
     */
    public Builder expectedConverted(String value) {
      this.dataFields.put("expectedConverted", value);
      return this;
    }

    /**
     * Set the paid converted value
     *
     * Description: Paid Amount (Account Currency).
     *
     * @param value
     *          The paidConverted value.
     * @return The builder for this class.
     */
    public Builder paidConverted(String value) {
      this.dataFields.put("paidConverted", value);
      return this;
    }

    /**
     * Set the exchange rate value
     *
     * Description: Exchange rate.
     *
     * @param value
     *          The exchangeRate value.
     * @return The builder for this class.
     */
    public Builder exchangeRate(String value) {
      this.dataFields.put("finaccTxnConvertRate", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentDetailsData build() {
      return new PaymentDetailsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentDetailsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
