/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2010-12-28 18:03:53
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt;

/**
 *
 * @author plujan
 *
 */
public class GoodsReceiptHeaderCreateLinesFromData {

  /* Data fields */
  /** Purchase Order Document No */
  private String purchaseOrderDocumentNo;
  /** Storage Bin */
  private String storageBin;
  /** AttributeSerialNumber */
  private String attributeSerialNumber;

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /* Builder fields */
    /** Purchase Order Document No */
    private String purchaseOrderDocumentNo = null;
    /** Storage Bin */
    private String storageBin = null;
    /** AttributeSerialNumber */
    private String attributeSerialNumber = null;

    /**
     * Set the Purchase Order Document No value.
     *
     * @param value
     *          The Purchase Order Document No value.
     * @return The builder for this class.
     */
    public Builder purchaseOrderDocumentNo(String value) {
      this.purchaseOrderDocumentNo = value;
      return this;
    }

    /**
     * Set the Storage Bin value.
     *
     * @param value
     *          The Storage Bin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.storageBin = value;
      return this;
    }

    /**
     * Set the AttributeSerialNumber value.
     *
     * @param value
     *          The AttributeSerialNumber value.
     * @return The builder for this class.
     */
    public Builder attributeSerialNumber(String value) {
      this.attributeSerialNumber = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param purchaseOrderDocumentNoField
     *          The Purchase Order Document No value.
     * @param storageBinField
     *          The Storage Bin value.
     * @param attributeSerialNumberField
     *          The AttributeSerialNumber value.
     * @return The builder for this class
     */
    public Builder requiredFields(String purchaseOrderDocumentNoField, String storageBinField,
        String attributeSerialNumberField) {
      this.purchaseOrderDocumentNo = purchaseOrderDocumentNoField;
      this.storageBin = storageBinField;
      this.attributeSerialNumber = attributeSerialNumberField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public GoodsReceiptHeaderCreateLinesFromData build() {
      return new GoodsReceiptHeaderCreateLinesFromData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private GoodsReceiptHeaderCreateLinesFromData(Builder builder) {
    purchaseOrderDocumentNo = builder.purchaseOrderDocumentNo;
    storageBin = builder.storageBin;
    attributeSerialNumber = builder.attributeSerialNumber;

  }

  public String getPurchaseOrderDocumentNo() {
    return purchaseOrderDocumentNo;
  }

  public String getStorageBin() {
    return storageBin;
  }

  public String getAttributeSerialNumber() {
    return attributeSerialNumber;
  }

  @Override
  public String toString() {
    return "GoodsReceiptHeaderCreateLinesFromData [purchaseOrderDocumentNo="
        + purchaseOrderDocumentNo + ", storageBin=" + storageBin + ", attributeSerialNumber="
        + attributeSerialNumber + "]";
  }

}
