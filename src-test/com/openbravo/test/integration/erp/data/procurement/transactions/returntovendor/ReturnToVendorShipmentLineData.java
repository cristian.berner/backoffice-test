/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for Return to Vendor Header Data
 *
 * @author Rafael Queralta
 *
 */
public class ReturnToVendorShipmentLineData extends DataObject {

  /**
   * Class builder
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the rMOrderNo value
     *
     * @param value
     *          The rMOrderNo value.
     * @return The builder for this class.
     */
    public Builder rMOrderNo(String value) {
      this.dataFields.put("rMOrderNo", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the product value
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the returned value
     *
     * @param value
     *          The returned value.
     * @return The builder for this class.
     */
    public Builder returned(String value) {
      this.dataFields.put("returned", value);
      return this;
    }

    /**
     * Set the alternative UOM
     *
     * @param value
     *          The alternative UOM
     * @return The builder for this class.
     */
    public Builder alternativeUOM(String value) {
      this.dataFields.put("alternativeUOM", value);
      return this;
    }

    /**
     * Set the alternative UOM
     *
     * @param value
     *          The alternative UOM
     * @return The builder for this class.
     */
    public Builder operativeUOM(String value) {
      this.dataFields.put("operativeUOM", value);
      return this;
    }

    /**
     * Set the returned UOM
     *
     * @param value
     *          The returned UOM
     * @return The builder for this class.
     */
    public Builder returnedUOM(String value) {
      this.dataFields.put("returnedUOM", value);
      return this;
    }

    /**
     * Set the quantity in alternative UOM
     *
     * @param value
     *          the quantity in alternative UOM
     * @return The builder for this class.
     */
    public Builder quantityInAlternativeUOM(String value) {
      this.dataFields.put("quantityInAlternativeUOM", value);
      return this;
    }

    /**
     * Set the quantity in alternative UOM
     *
     * @param value
     *          the quantity in alternative UOM
     * @return The builder for this class.
     */
    public Builder operativeQuantity(String value) {
      this.dataFields.put("quantityInAlternativeUOM", value);
      return this;
    }

    /**
     * Set the returnReason value
     *
     * @param value
     *          The returnReason value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the pending value
     *
     * @param value
     *          The pending value.
     * @return The builder for this class.
     */
    public Builder pending(String value) {
      this.dataFields.put("pending", value);
      return this;
    }

    /**
     * Set the movementQuantity value
     *
     * @param value
     *          The movementQuantity value.
     * @return The builder for this class.
     */
    public Builder movementQuantity(String value) {
      this.dataFields.put("movementQuantity", value);
      return this;
    }

    /**
     * Set the storageBin value
     *
     * @param value
     *          The storageBin value.
     * @return The builder for this class.
     */
    public Builder storageBin(String value) {
      this.dataFields.put("storageBin", value);
      return this;
    }

    /**
     * Set the availableQty value
     *
     * @param value
     *          The availableQty value.
     * @return The builder for this class.
     */
    public Builder availableQty(String value) {
      this.dataFields.put("availableQty", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ReturnToVendorShipmentLineData build() {
      return new ReturnToVendorShipmentLineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ReturnToVendorShipmentLineData(Builder builder) {
    dataFields = builder.dataFields;
  }
}
