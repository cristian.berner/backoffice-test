/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentPlanData
 *
 * @author plujan
 *
 */
public class PaymentPlanData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the lastPaymentDate value
     *
     * Description: Last Payment Date
     *
     * @param value
     *          The lastPaymentDate value.
     * @return The builder for this class.
     */
    public Builder lastPaymentDate(String value) {
      this.dataFields.put("lastPaymentDate", value);
      return this;
    }

    /**
     * Set the numberOfPayments value
     *
     * Description: Number of Payments
     *
     * @param value
     *          The numberOfPayments value.
     * @return The builder for this class.
     */
    public Builder numberOfPayments(String value) {
      this.dataFields.put("numberOfPayments", value);
      return this;
    }

    /**
     * Set the paidAmount value
     *
     * Description: Paid Amount
     *
     * @param value
     *          The paid value.
     * @return The builder for this class.
     */
    public Builder paid(String value) {
      this.dataFields.put("paidAmount", value);
      return this;
    }

    /**
     * Set the outstandingAmount value
     *
     * Description: Outstanding Amount
     *
     * @param value
     *          The outstanding value.
     * @return The builder for this class.
     */
    public Builder outstanding(String value) {
      this.dataFields.put("outstandingAmount", value);
      return this;
    }

    /**
     * Set the dueDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The dueDate value.
     * @return The builder for this class.
     */
    public Builder dueDate(String value) {
      this.dataFields.put("dueDate", value);
      return this;
    }

    /**
     * Set the finPaymentmethod value
     *
     * Description: It is the method by which payment is expected to be made or received.
     *
     * @param value
     *          The paymentMethod value.
     * @return The builder for this class.
     */
    public Builder paymentMethod(String value) {
      this.dataFields.put("finPaymentmethod", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: Expected Amount
     *
     * @param value
     *          The expected value.
     * @return The builder for this class.
     */
    public Builder expected(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the daysOverdue value
     *
     * Description: The Days Over Due.
     *
     * @param value
     *          The daysOverdue value.
     * @return The builder for this class.
     */
    public Builder daysOverdue(String value) {
      this.dataFields.put("daysOverdue", value);
      return this;
    }

    /**
     * Set the expectedDate value
     *
     * Description: The date when a specified request must be carried out by.
     *
     * @param value
     *          The expectedDate value.
     * @return The builder for this class.
     */
    public Builder expectedDate(String value) {
      this.dataFields.put("expectedDate", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentPlanData build() {
      return new PaymentPlanData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentPlanData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
