/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data;

import java.util.LinkedHashMap;

/**
 * Class for a data object.
 *
 * @author elopio
 *
 */
public class DataObject {

  /** The data fields of the object. */
  protected LinkedHashMap<String, Object> dataFields;

  /**
   * Class constructor.
   */
  public DataObject() {
    dataFields = new LinkedHashMap<String, Object>();
  }

  /**
   * Add a data field to the object.
   *
   * @param name
   *          The name of the data field.
   * @param value
   *          The value of the data field.
   * @return this data object with the new value added.
   */
  public DataObject addDataField(String name, Object value) {
    dataFields.put(name, value);
    return this;
  }

  /**
   * Get the value of a data field.
   *
   * @param name
   *          The name of the data field.
   * @return the value of the data field.
   */
  public Object getDataField(String name) {
    return dataFields.get(name);
  }

  /**
   * Get the data fields of the object.
   *
   * @return the data fields of the object.
   */
  public LinkedHashMap<String, Object> getDataFields() {
    return dataFields;
  }

  /**
   * Get the name of the next field. If there is only one, it will show that one. In case of
   * datafield is empty, it will throw an exception.
   *
   * @return the name of the next data field.
   */
  public String getNextFieldName() {
    return dataFields.entrySet().iterator().next().getKey().toString();
  }

  /**
   * Check if the data object is empty.
   *
   * @return true if the data object is empty. Otherwise, false.
   */
  public boolean isEmpty() {
    return dataFields.isEmpty();
  }

  /**
   * Get the string representation of the data object.
   *
   * @return the string representation of this data object.
   */
  @Override
  public String toString() {
    return dataFields.toString();
  }
}
