/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.data;

public class ChangeExpiredPasswordData {

  private static final String FORMAT_STRING_REPRESENTATION = "[Change expired password data: new password=%s, confirm password=%s]";

  private String newPassword;
  private String confirmPassword;

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private ChangeExpiredPasswordData(Builder builder) {
    newPassword = builder.newPassword;
    confirmPassword = builder.confirmPassword;
  }

  /**
   * Get the new password.
   *
   * @return the new password.
   */
  public String getNewPassword() {
    return newPassword;
  }

  /**
   * Get the confirmation of the new password.
   *
   * @return the confirmation of the new password.
   */
  public String getConfirmPassword() {
    return confirmPassword;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, newPassword, confirmPassword);
  }

  /**
   * Class builder.
   */
  public static class Builder {
    private String newPassword;
    private String confirmPassword;

    /**
     * Set the new password.
     *
     * @param value
     *          The new password.
     * @return the builder for this class.
     */
    public Builder newPassword(String value) {
      this.newPassword = value;
      return this;
    }

    /**
     * Set the confirmation of the new password.
     *
     * @param value
     *          The confirmation password.
     * @return the builder for this class.
     */
    public Builder confirmPassword(String value) {
      this.confirmPassword = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public ChangeExpiredPasswordData build() {
      return new ChangeExpiredPasswordData(this);
    }
  }
}
