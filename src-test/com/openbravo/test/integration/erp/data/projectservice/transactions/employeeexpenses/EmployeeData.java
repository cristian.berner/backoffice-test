/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.transactions.employeeexpenses;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for EmployeeData
 *
 * @author plujan
 *
 */
public class EmployeeData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the name2 value
     *
     * Description: Additional space to write the name of a business partner.
     *
     * @param value
     *          The name2 value.
     * @return The builder for this class.
     */
    public Builder name2(String value) {
      this.dataFields.put("name2", value);
      return this;
    }

    /**
     * Set the purchasePricelist value
     *
     * Description: A catalog of selected products which can be purchased each with a specified
     * price.
     *
     * @param value
     *          The purchasePricelist value.
     * @return The builder for this class.
     */
    public Builder purchasePricelist(String value) {
      this.dataFields.put("purchasePricelist", value);
      return this;
    }

    /**
     * Set the pOPaymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The pOPaymentTerms value.
     * @return The builder for this class.
     */
    public Builder pOPaymentTerms(String value) {
      this.dataFields.put("pOPaymentTerms", value);
      return this;
    }

    /**
     * Set the employee value
     *
     * Description: A business partner who will be working for an organization.
     *
     * @param value
     *          The employee value.
     * @return The builder for this class.
     */
    public Builder employee(Boolean value) {
      this.dataFields.put("employee", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public EmployeeData build() {
      return new EmployeeData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private EmployeeData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
