/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.setup.resource;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for PriceData
 *
 * @author plujan
 *
 */
public class PriceData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the priceLimit value
     *
     * Description: The lowest price a specified item may be sold for.
     *
     * @param value
     *          The priceLimit value.
     * @return The builder for this class.
     */
    public Builder priceLimit(String value) {
      this.dataFields.put("priceLimit", value);
      return this;
    }

    /**
     * Set the standardPrice value
     *
     * Description: The regular or normal price of a product in the respective price list.
     *
     * @param value
     *          The standardPrice value.
     * @return The builder for this class.
     */
    public Builder standardPrice(String value) {
      this.dataFields.put("standardPrice", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the priceListVersion value
     *
     * Description: A price list with a specified validity range.
     *
     * @param value
     *          The priceListVersion value.
     * @return The builder for this class.
     */
    public Builder priceListVersion(String value) {
      this.dataFields.put("priceListVersion", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PriceData build() {
      return new PriceData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PriceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
