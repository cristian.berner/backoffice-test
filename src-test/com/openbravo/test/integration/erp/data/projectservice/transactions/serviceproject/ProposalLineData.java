/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:15
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.transactions.serviceproject;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for ProposalLineData
 *
 * @author plujan
 *
 */
public class ProposalLineData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the productDescription value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The productDescription value.
     * @return The builder for this class.
     */
    public Builder productDescription(String value) {
      this.dataFields.put("productDescription", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A identifier for a document which can be used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the identifier value
     *
     * Description: A value specified in many forms.
     *
     * @param value
     *          The identifier value.
     * @return The builder for this class.
     */
    public Builder identifier(String value) {
      this.dataFields.put("identifier", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the price value
     *
     * Description: The cost or value of a good or service.
     *
     * @param value
     *          The price value.
     * @return The builder for this class.
     */
    public Builder price(String value) {
      this.dataFields.put("price", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: Line No.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the projectProposal value
     *
     * Description: Project Proposal
     *
     * @param value
     *          The projectProposal value.
     * @return The builder for this class.
     */
    public Builder projectProposal(String value) {
      this.dataFields.put("projectProposal", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProposalLineData build() {
      return new ProposalLineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProposalLineData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
