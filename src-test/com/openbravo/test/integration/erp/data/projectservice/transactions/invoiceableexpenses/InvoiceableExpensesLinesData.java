/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.projectservice.transactions.invoiceableexpenses;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for InvoiceableExpensesLinesData
 *
 * @author plujan
 *
 */
public class InvoiceableExpensesLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the projectTask value
     *
     * Description: A job to be carried out during the phase of a project.
     *
     * @param value
     *          The projectTask value.
     * @return The builder for this class.
     */
    public Builder projectTask(String value) {
      this.dataFields.put("projectTask", value);
      return this;
    }

    /**
     * Set the projectPhase value
     *
     * Description: A defined section of a project.
     *
     * @param value
     *          The projectPhase value.
     * @return The builder for this class.
     */
    public Builder projectPhase(String value) {
      this.dataFields.put("projectPhase", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the invoicePrice value
     *
     * Description: The price at which something may be invoiced shown in the currency of the
     * invoiced business partner.
     *
     * @param value
     *          The invoicePrice value.
     * @return The builder for this class.
     */
    public Builder invoicePrice(String value) {
      this.dataFields.put("invoicePrice", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the reInvoice value
     *
     * Description: An indication that a transaction may be invoiced to a business partner.
     *
     * @param value
     *          The reInvoice value.
     * @return The builder for this class.
     */
    public Builder reInvoice(Boolean value) {
      this.dataFields.put("reInvoice", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: A line stating the position of this request in the document.
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the convertedAmount value
     *
     * Description: The monetary sum at which one unit of measure is changed to another.
     *
     * @param value
     *          The convertedAmount value.
     * @return The builder for this class.
     */
    public Builder convertedAmount(String value) {
      this.dataFields.put("convertedAmount", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the expenseSheet value
     *
     * Description: A list of billable expenses related to time travel and basis business conduct.
     *
     * @param value
     *          The expenseSheet value.
     * @return The builder for this class.
     */
    public Builder expenseSheet(String value) {
      this.dataFields.put("expenseSheet", value);
      return this;
    }

    /**
     * Set the expenseDate value
     *
     * Description: Date of expense
     *
     * @param value
     *          The expenseDate value.
     * @return The builder for this class.
     */
    public Builder expenseDate(String value) {
      this.dataFields.put("expenseDate", value);
      return this;
    }

    /**
     * Set the expenseAmount value
     *
     * Description: Amount for this expense
     *
     * @param value
     *          The expenseAmount value.
     * @return The builder for this class.
     */
    public Builder expenseAmount(String value) {
      this.dataFields.put("expenseAmount", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the timeSheet value
     *
     * Description: An indication that the charge is due to time not typically justifiable by
     * receipt.
     *
     * @param value
     *          The timeSheet value.
     * @return The builder for this class.
     */
    public Builder timeSheet(Boolean value) {
      this.dataFields.put("timeSheet", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InvoiceableExpensesLinesData build() {
      return new InvoiceableExpensesLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InvoiceableExpensesLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
