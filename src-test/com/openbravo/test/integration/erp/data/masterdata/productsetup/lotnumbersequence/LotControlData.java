/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.productsetup.lotnumbersequence;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for LotControlData
 *
 * @author plujan
 *
 */
public class LotControlData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the incrementBy value
     *
     * Description: An addition to a starting number by a specified value.
     *
     * @param value
     *          The incrementBy value.
     * @return The builder for this class.
     */
    public Builder incrementBy(String value) {
      this.dataFields.put("incrementBy", value);
      return this;
    }

    /**
     * Set the startingNo value
     *
     * Description: The first number that will be used in a standard or control sequence.
     *
     * @param value
     *          The startingNo value.
     * @return The builder for this class.
     */
    public Builder startingNo(String value) {
      this.dataFields.put("startingNo", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the nextAssignedNumber value
     *
     * Description: The next number that will be assigned to an item.
     *
     * @param value
     *          The nextAssignedNumber value.
     * @return The builder for this class.
     */
    public Builder nextAssignedNumber(String value) {
      this.dataFields.put("nextAssignedNumber", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the suffix value
     *
     * Description: One or many characters which are added at the end of a statement or number.
     *
     * @param value
     *          The suffix value.
     * @return The builder for this class.
     */
    public Builder suffix(String value) {
      this.dataFields.put("suffix", value);
      return this;
    }

    /**
     * Set the prefix value
     *
     * Description: Characters which are added at the beginning of a statement or number.
     *
     * @param value
     *          The prefix value.
     * @return The builder for this class.
     */
    public Builder prefix(String value) {
      this.dataFields.put("prefix", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public LotControlData build() {
      return new LotControlData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private LotControlData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
