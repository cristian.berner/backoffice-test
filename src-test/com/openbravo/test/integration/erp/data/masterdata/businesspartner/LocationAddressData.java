/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;

/**
 *
 * Class for LocationAddressData
 *
 * @author plujan
 *
 */
public class LocationAddressData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the shipToAddress value
     *
     * Description: Business Partner address to ship goods to
     *
     * @param value
     *          The shipToAddress value.
     * @return The builder for this class.
     */
    public Builder shipToAddress(Boolean value) {
      this.dataFields.put("shipToAddress", value);
      return this;
    }

    /**
     * Set the invoiceToAddress value
     *
     * Description: Indicates that this address is the Invoice to Address
     *
     * @param value
     *          The invoiceToAddress value.
     * @return The builder for this class.
     */
    public Builder invoiceToAddress(Boolean value) {
      this.dataFields.put("invoiceToAddress", value);
      return this;
    }

    /**
     * Set the alternativePhone value
     *
     * Description: A second contact telephone number for a business partner.
     *
     * @param value
     *          The alternativePhone value.
     * @return The builder for this class.
     */
    public Builder alternativePhone(String value) {
      this.dataFields.put("alternativePhone", value);
      return this;
    }

    /**
     * Set the phone value
     *
     * Description: A telephone number for a specified business partner.
     *
     * @param value
     *          The phone value.
     * @return The builder for this class.
     */
    public Builder phone(String value) {
      this.dataFields.put("phone", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the fax value
     *
     * Description: A fax number for a specified business partner.
     *
     * @param value
     *          The fax value.
     * @return The builder for this class.
     */
    public Builder fax(String value) {
      this.dataFields.put("fax", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public LocationAddressData build() {
      return new LocationAddressData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private LocationAddressData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
