/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importbudget;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ImportBudgetData
 *
 * @author plujan
 *
 */
public class ImportBudgetData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the elementvalueident value
     *
     * Description: Elementvalueident
     *
     * @param value
     *          The elementvalueident value.
     * @return The builder for this class.
     */
    public Builder elementvalueident(String value) {
      this.dataFields.put("elementvalueident", value);
      return this;
    }

    /**
     * Set the acctschemaident value
     *
     * Description: Acctschemaident
     *
     * @param value
     *          The acctschemaident value.
     * @return The builder for this class.
     */
    public Builder acctschemaident(String value) {
      this.dataFields.put("acctschemaident", value);
      return this;
    }

    /**
     * Set the accountingSchema value
     *
     * Description: The structure used in accounting including costing methods currencies and the
     * calendar.
     *
     * @param value
     *          The accountingSchema value.
     * @return The builder for this class.
     */
    public Builder accountingSchema(String value) {
      this.dataFields.put("accountingSchema", value);
      return this;
    }

    /**
     * Set the accountElement value
     *
     * Description: A identification code for an account type.
     *
     * @param value
     *          The accountElement value.
     * @return The builder for this class.
     */
    public Builder accountElement(String value) {
      this.dataFields.put("accountElement", value);
      return this;
    }

    /**
     * Set the currencyIdentification value
     *
     * Description: Currency Identification
     *
     * @param value
     *          The currencyIdentification value.
     * @return The builder for this class.
     */
    public Builder currencyIdentification(String value) {
      this.dataFields.put("currencyIdentification", value);
      return this;
    }

    /**
     * Set the importProcessComplete value
     *
     * Description: A indication that the desired process has been completed successfully.
     *
     * @param value
     *          The importProcessComplete value.
     * @return The builder for this class.
     */
    public Builder importProcessComplete(Boolean value) {
      this.dataFields.put("importProcessComplete", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the amount value
     *
     * Description: A monetary total.
     *
     * @param value
     *          The amount value.
     * @return The builder for this class.
     */
    public Builder amount(String value) {
      this.dataFields.put("amount", value);
      return this;
    }

    /**
     * Set the price value
     *
     * Description: The cost or value of a good or service.
     *
     * @param value
     *          The price value.
     * @return The builder for this class.
     */
    public Builder price(String value) {
      this.dataFields.put("price", value);
      return this;
    }

    /**
     * Set the quantity value
     *
     * Description: The number of a certain item.
     *
     * @param value
     *          The quantity value.
     * @return The builder for this class.
     */
    public Builder quantity(String value) {
      this.dataFields.put("quantity", value);
      return this;
    }

    /**
     * Set the budget value
     *
     * Description: A group of planned expenses for a task.
     *
     * @param value
     *          The budget value.
     * @return The builder for this class.
     */
    public Builder budget(String value) {
      this.dataFields.put("budget", value);
      return this;
    }

    /**
     * Set the productCategoryID value
     *
     * Description: Product Category ID
     *
     * @param value
     *          The productCategoryID value.
     * @return The builder for this class.
     */
    public Builder productCategoryID(String value) {
      this.dataFields.put("productCategoryID", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the bPGroupIdentitification value
     *
     * Description: BP Group Identitification
     *
     * @param value
     *          The bPGroupIdentitification value.
     * @return The builder for this class.
     */
    public Builder bPGroupIdentitification(String value) {
      this.dataFields.put("bPGroupIdentitification", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the activityIdentification value
     *
     * Description: Activity Identification
     *
     * @param value
     *          The activityIdentification value.
     * @return The builder for this class.
     */
    public Builder activityIdentification(String value) {
      this.dataFields.put("activityIdentification", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the campaignIdentification value
     *
     * Description: Campaign Identification
     *
     * @param value
     *          The campaignIdentification value.
     * @return The builder for this class.
     */
    public Builder campaignIdentification(String value) {
      this.dataFields.put("campaignIdentification", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the projectIdentification value
     *
     * Description: Project Identification
     *
     * @param value
     *          The projectIdentification value.
     * @return The builder for this class.
     */
    public Builder projectIdentification(String value) {
      this.dataFields.put("projectIdentification", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(String value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the salesRegionIdentification value
     *
     * Description: Sales Region Identification
     *
     * @param value
     *          The salesRegionIdentification value.
     * @return The builder for this class.
     */
    public Builder salesRegionIdentification(String value) {
      this.dataFields.put("salesRegionIdentification", value);
      return this;
    }

    /**
     * Set the salesRegion value
     *
     * Description: A defined section of the world where sales efforts will be focused.
     *
     * @param value
     *          The salesRegion value.
     * @return The builder for this class.
     */
    public Builder salesRegion(String value) {
      this.dataFields.put("salesRegion", value);
      return this;
    }

    /**
     * Set the organizationTrxIdentification value
     *
     * Description: Organization Trx Identification
     *
     * @param value
     *          The organizationTrxIdentification value.
     * @return The builder for this class.
     */
    public Builder organizationTrxIdentification(String value) {
      this.dataFields.put("organizationTrxIdentification", value);
      return this;
    }

    /**
     * Set the trxOrganization value
     *
     * Description: The organization which performs or initiates the transaction.
     *
     * @param value
     *          The trxOrganization value.
     * @return The builder for this class.
     */
    public Builder trxOrganization(String value) {
      this.dataFields.put("trxOrganization", value);
      return this;
    }

    /**
     * Set the partnerIdentification value
     *
     * Description: Partner Identification
     *
     * @param value
     *          The partnerIdentification value.
     * @return The builder for this class.
     */
    public Builder partnerIdentification(String value) {
      this.dataFields.put("partnerIdentification", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(String value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the productIdentification value
     *
     * Description: Product Identification
     *
     * @param value
     *          The productIdentification value.
     * @return The builder for this class.
     */
    public Builder productIdentification(String value) {
      this.dataFields.put("productIdentification", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the periodIdentification value
     *
     * Description: Period Identification
     *
     * @param value
     *          The periodIdentification value.
     * @return The builder for this class.
     */
    public Builder periodIdentification(String value) {
      this.dataFields.put("periodIdentification", value);
      return this;
    }

    /**
     * Set the period value
     *
     * Description: A specified time period.
     *
     * @param value
     *          The period value.
     * @return The builder for this class.
     */
    public Builder period(String value) {
      this.dataFields.put("period", value);
      return this;
    }

    /**
     * Set the budgetLine value
     *
     * Description: Budget Line
     *
     * @param value
     *          The budgetLine value.
     * @return The builder for this class.
     */
    public Builder budgetLine(String value) {
      this.dataFields.put("budgetLine", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportBudgetData build() {
      return new ImportBudgetData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportBudgetData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
