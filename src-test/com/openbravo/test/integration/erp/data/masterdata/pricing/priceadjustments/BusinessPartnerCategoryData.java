/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.pricing.priceadjustments;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BusinessPartnerCategoryData
 *
 * @author plujan
 *
 */
public class BusinessPartnerCategoryData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the priceAdjustment value
     *
     * Description: The ability to raise or lower prices.
     *
     * @param value
     *          The priceAdjustment value.
     * @return The builder for this class.
     */
    public Builder priceAdjustment(String value) {
      this.dataFields.put("priceAdjustment", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BusinessPartnerCategoryData build() {
      return new BusinessPartnerCategoryData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BusinessPartnerCategoryData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
