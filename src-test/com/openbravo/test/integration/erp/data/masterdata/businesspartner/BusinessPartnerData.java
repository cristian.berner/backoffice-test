/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for BusinessPartnerData
 *
 * @author plujan
 *
 */
public class BusinessPartnerData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the consumptionDays value
     *
     * Description: Consumption Days
     *
     * @param value
     *          The consumptionDays value.
     * @return The builder for this class.
     */
    public Builder consumptionDays(String value) {
      this.dataFields.put("consumptionDays", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the name2 value
     *
     * Description: Additional Name
     *
     * @param value
     *          The name2 value.
     * @return The builder for this class.
     */
    public Builder name2(String value) {
      this.dataFields.put("name2", value);
      return this;
    }

    /**
     * Set the uRL value
     *
     * Description: An address which can be accessed via internet.
     *
     * @param value
     *          The uRL value.
     * @return The builder for this class.
     */
    public Builder uRL(String value) {
      this.dataFields.put("uRL", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the taxID value
     *
     * Description: The government defined unique number for assigning and paying taxes.
     *
     * @param value
     *          The taxID value.
     * @return The builder for this class.
     */
    public Builder taxID(String value) {
      this.dataFields.put("taxID", value);
      return this;
    }

    /**
     * Set the referenceNo value
     *
     * Description: The number for a specific reference.
     *
     * @param value
     *          The referenceNo value.
     * @return The builder for this class.
     */
    public Builder referenceNo(String value) {
      this.dataFields.put("referenceNo", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: Alphanumeric identifier of the entity
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the current balance value
     *
     * @param value
     *          The current balance value.
     * @return The builder for this class.
     */
    public Builder creditUsed(String value) {
      this.dataFields.put("creditUsed", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public BusinessPartnerData build() {
      return new BusinessPartnerData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private BusinessPartnerData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
