/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.pricing.pricelistschema;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for PriceListSchemaLinesData
 *
 * @author plujan
 *
 */
public class PriceListSchemaLinesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the conversionDate value
     *
     * Description: Date for selecting conversion rate
     *
     * @param value
     *          The conversionDate value.
     * @return The builder for this class.
     */
    public Builder conversionDate(String value) {
      this.dataFields.put("conversionDate", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the fixedStandardPrice value
     *
     * Description: Fixed Standard Price (not calculated)
     *
     * @param value
     *          The fixedStandardPrice value.
     * @return The builder for this class.
     */
    public Builder fixedStandardPrice(String value) {
      this.dataFields.put("fixedStandardPrice", value);
      return this;
    }

    /**
     * Set the fixedListPrice value
     *
     * Description: Fixes List Price (not calculated)
     *
     * @param value
     *          The fixedListPrice value.
     * @return The builder for this class.
     */
    public Builder fixedListPrice(String value) {
      this.dataFields.put("fixedListPrice", value);
      return this;
    }

    /**
     * Set the standardPriceDiscount value
     *
     * Description: Discount percentage to subtract from base price
     *
     * @param value
     *          The standardPriceDiscount value.
     * @return The builder for this class.
     */
    public Builder standardPriceDiscount(String value) {
      this.dataFields.put("standardPriceDiscount", value);
      return this;
    }

    /**
     * Set the surchargeStandardPriceAmount value
     *
     * Description: Amount added to a price as a surcharge
     *
     * @param value
     *          The surchargeStandardPriceAmount value.
     * @return The builder for this class.
     */
    public Builder surchargeStandardPriceAmount(String value) {
      this.dataFields.put("surchargeStandardPriceAmount", value);
      return this;
    }

    /**
     * Set the standardBasePrice value
     *
     * Description: Base price for calculating new standard price
     *
     * @param value
     *          The standardBasePrice value.
     * @return The builder for this class.
     */
    public Builder standardBasePrice(String value) {
      this.dataFields.put("standardBasePrice", value);
      return this;
    }

    /**
     * Set the listPriceDiscount value
     *
     * Description: Discount from list price as a percentage
     *
     * @param value
     *          The listPriceDiscount value.
     * @return The builder for this class.
     */
    public Builder listPriceDiscount(String value) {
      this.dataFields.put("listPriceDiscount", value);
      return this;
    }

    /**
     * Set the listPriceDiscount value
     *
     * Description: Discount from list price as a percentage
     *
     * @param value
     *          The listPriceDiscount value.
     * @return The builder for this class.
     */
    public Builder listPriceMargin(String value) {
      this.dataFields.put("listPriceMargin", value);
      return this;
    }

    /**
     * Set the unitPriceDiscount value
     *
     * Description: Discount from unit price as a percentage
     *
     * @param value
     *          The unitPriceDiscount value.
     * @return The builder for this class.
     */
    public Builder unitPriceMargin(String value) {
      this.dataFields.put("unitPriceMargin", value);
      return this;
    }

    /**
     * Set the surchargeListPriceAmount value
     *
     * Description: List Price Surcharge Amount
     *
     * @param value
     *          The surchargeListPriceAmount value.
     * @return The builder for this class.
     */
    public Builder surchargeListPriceAmount(String value) {
      this.dataFields.put("surchargeListPriceAmount", value);
      return this;
    }

    /**
     * Set the baseListPrice value
     *
     * Description: Price used as the basis for price list calculations
     *
     * @param value
     *          The baseListPrice value.
     * @return The builder for this class.
     */
    public Builder baseListPrice(String value) {
      this.dataFields.put("baseListPrice", value);
      return this;
    }

    /**
     * Set the conversionRateType value
     *
     * Description: A distinct conversion rate characteristic used for processes.
     *
     * @param value
     *          The conversionRateType value.
     * @return The builder for this class.
     */
    public Builder conversionRateType(String value) {
      this.dataFields.put("conversionRateType", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the priceListSchema value
     *
     * Description: A set of guidelines applied to current Price List in order to create a price
     * list.
     *
     * @param value
     *          The priceListSchema value.
     * @return The builder for this class.
     */
    public Builder priceListSchema(String value) {
      this.dataFields.put("priceListSchema", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PriceListSchemaLinesData build() {
      return new PriceListSchemaLinesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PriceListSchemaLinesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
