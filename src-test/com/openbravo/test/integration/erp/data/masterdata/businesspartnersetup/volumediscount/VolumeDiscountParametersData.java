/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.volumediscount;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for VolumeDiscountParametersData
 *
 * @author plujan
 *
 */
public class VolumeDiscountParametersData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the maxAmount value
     *
     * Description: Amount to
     *
     * @param value
     *          The maxAmount value.
     * @return The builder for this class.
     */
    public Builder maxAmount(String value) {
      this.dataFields.put("maxAmount", value);
      return this;
    }

    /**
     * Set the minAmount value
     *
     * Description: Amount from
     *
     * @param value
     *          The minAmount value.
     * @return The builder for this class.
     */
    public Builder minAmount(String value) {
      this.dataFields.put("minAmount", value);
      return this;
    }

    /**
     * Set the discount value
     *
     * Description: The proportional discount given to an item without respect to any previously
     * defined discounts.
     *
     * @param value
     *          The discount value.
     * @return The builder for this class.
     */
    public Builder discount(String value) {
      this.dataFields.put("discount", value);
      return this;
    }

    /**
     * Set the volumeDiscount value
     *
     * Description: A promotion given at a specific time of year based on purchase amounts.
     *
     * @param value
     *          The volumeDiscount value.
     * @return The builder for this class.
     */
    public Builder volumeDiscount(String value) {
      this.dataFields.put("volumeDiscount", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public VolumeDiscountParametersData build() {
      return new VolumeDiscountParametersData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private VolumeDiscountParametersData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
