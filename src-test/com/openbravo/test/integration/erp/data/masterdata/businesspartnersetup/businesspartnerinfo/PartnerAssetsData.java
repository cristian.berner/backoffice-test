/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnerinfo;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for PartnerAssetsData
 *
 * @author plujan
 *
 */
public class PartnerAssetsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the asset value
     *
     * Description: An item which is owned and exchangeable for cash.
     *
     * @param value
     *          The asset value.
     * @return The builder for this class.
     */
    public Builder asset(String value) {
      this.dataFields.put("asset", value);
      return this;
    }

    /**
     * Set the assetCategory value
     *
     * Description: A classification of assets based on similar characteristics.
     *
     * @param value
     *          The assetCategory value.
     * @return The builder for this class.
     */
    public Builder assetCategory(String value) {
      this.dataFields.put("assetCategory", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the inServiceDate value
     *
     * Description: Date when Asset was put into service
     *
     * @param value
     *          The inServiceDate value.
     * @return The builder for this class.
     */
    public Builder inServiceDate(String value) {
      this.dataFields.put("inServiceDate", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the expirationDate value
     *
     * Description: The date upon which an item is guaranteed to be of good quality.
     *
     * @param value
     *          The expirationDate value.
     * @return The builder for this class.
     */
    public Builder expirationDate(String value) {
      this.dataFields.put("expirationDate", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the serialNo value
     *
     * Description: An attribute used as a unique identifier for a product.
     *
     * @param value
     *          The serialNo value.
     * @return The builder for this class.
     */
    public Builder serialNo(String value) {
      this.dataFields.put("serialNo", value);
      return this;
    }

    /**
     * Set the lotName value
     *
     * Description: A group of identical or similar items organized and placed into inventory under
     * one number.
     *
     * @param value
     *          The lotName value.
     * @return The builder for this class.
     */
    public Builder lotName(String value) {
      this.dataFields.put("lotName", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PartnerAssetsData build() {
      return new PartnerAssetsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PartnerAssetsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
