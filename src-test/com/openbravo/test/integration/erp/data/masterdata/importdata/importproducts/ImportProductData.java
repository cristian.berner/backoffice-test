/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importproducts;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for ImportProductData
 *
 * @author plujan
 *
 */
public class ImportProductData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the eDICode value
     *
     * Description: UOM EDI Code
     *
     * @param value
     *          The eDICode value.
     * @return The builder for this class.
     */
    public Builder eDICode(String value) {
      this.dataFields.put("eDICode", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the purchaseOrderPrice value
     *
     * Description: The price charged to purchase a specific item.
     *
     * @param value
     *          The purchaseOrderPrice value.
     * @return The builder for this class.
     */
    public Builder purchaseOrderPrice(String value) {
      this.dataFields.put("purchaseOrderPrice", value);
      return this;
    }

    /**
     * Set the descriptionURL value
     *
     * Description: An address for the product description which can be accessed via internet.
     *
     * @param value
     *          The descriptionURL value.
     * @return The builder for this class.
     */
    public Builder descriptionURL(String value) {
      this.dataFields.put("descriptionURL", value);
      return this;
    }

    /**
     * Set the uPCEAN value
     *
     * Description: A bar code with a number to identify a product.
     *
     * @param value
     *          The uPCEAN value.
     * @return The builder for this class.
     */
    public Builder uPCEAN(String value) {
      this.dataFields.put("uPCEAN", value);
      return this;
    }

    /**
     * Set the minimumOrderQty value
     *
     * Description: The minimum number of an item that must be purchased at one time from a vendor.
     *
     * @param value
     *          The minimumOrderQty value.
     * @return The builder for this class.
     */
    public Builder minimumOrderQty(String value) {
      this.dataFields.put("minimumOrderQty", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the imageURL value
     *
     * Description: An address for the product image which can be accessed via internet.
     *
     * @param value
     *          The imageURL value.
     * @return The builder for this class.
     */
    public Builder imageURL(String value) {
      this.dataFields.put("imageURL", value);
      return this;
    }

    /**
     * Set the shelfWidth value
     *
     * Description: Shelf width required
     *
     * @param value
     *          The shelfWidth value.
     * @return The builder for this class.
     */
    public Builder shelfWidth(String value) {
      this.dataFields.put("shelfWidth", value);
      return this;
    }

    /**
     * Set the unitsPerPallet value
     *
     * Description: Units Per Pallet
     *
     * @param value
     *          The unitsPerPallet value.
     * @return The builder for this class.
     */
    public Builder unitsPerPallet(String value) {
      this.dataFields.put("unitsPerPallet", value);
      return this;
    }

    /**
     * Set the discontinuedBy value
     *
     * Description: The name of the person who discontinues an item.
     *
     * @param value
     *          The discontinuedBy value.
     * @return The builder for this class.
     */
    public Builder discontinuedBy(String value) {
      this.dataFields.put("discontinuedBy", value);
      return this;
    }

    /**
     * Set the shelfDepth value
     *
     * Description: Shelf depth required
     *
     * @param value
     *          The shelfDepth value.
     * @return The builder for this class.
     */
    public Builder shelfDepth(String value) {
      this.dataFields.put("shelfDepth", value);
      return this;
    }

    /**
     * Set the manufacturer value
     *
     * Description: The business partner that makes a specific product.
     *
     * @param value
     *          The manufacturer value.
     * @return The builder for this class.
     */
    public Builder manufacturer(String value) {
      this.dataFields.put("manufacturer", value);
      return this;
    }

    /**
     * Set the shelfHeight value
     *
     * Description: Shelf height required
     *
     * @param value
     *          The shelfHeight value.
     * @return The builder for this class.
     */
    public Builder shelfHeight(String value) {
      this.dataFields.put("shelfHeight", value);
      return this;
    }

    /**
     * Set the businessPartnerSearchKey value
     *
     * Description: An abbreviated name (or a code) to help find a business partner.
     *
     * @param value
     *          The businessPartnerSearchKey value.
     * @return The builder for this class.
     */
    public Builder businessPartnerSearchKey(String value) {
      this.dataFields.put("businessPartnerSearchKey", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the productType value
     *
     * Description: An important classification used to determine the accounting and management of a
     * product.
     *
     * @param value
     *          The productType value.
     * @return The builder for this class.
     */
    public Builder productType(String value) {
      this.dataFields.put("productType", value);
      return this;
    }

    /**
     * Set the importProcessComplete value
     *
     * Description: A indication that the desired process has been completed successfully.
     *
     * @param value
     *          The importProcessComplete value.
     * @return The builder for this class.
     */
    public Builder importProcessComplete(Boolean value) {
      this.dataFields.put("importProcessComplete", value);
      return this;
    }

    /**
     * Set the listPrice value
     *
     * Description: The official price of a product in a specified currency.
     *
     * @param value
     *          The listPrice value.
     * @return The builder for this class.
     */
    public Builder listPrice(String value) {
      this.dataFields.put("listPrice", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(String value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the volume value
     *
     * Description: Volume of a product
     *
     * @param value
     *          The volume value.
     * @return The builder for this class.
     */
    public Builder volume(String value) {
      this.dataFields.put("volume", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the discontinued value
     *
     * Description: A statement mentioning that this product will no longer be available on the
     * market.
     *
     * @param value
     *          The discontinued value.
     * @return The builder for this class.
     */
    public Builder discontinued(Boolean value) {
      this.dataFields.put("discontinued", value);
      return this;
    }

    /**
     * Set the royaltyAmount value
     *
     * Description: (Included) Amount for copyright etc.
     *
     * @param value
     *          The royaltyAmount value.
     * @return The builder for this class.
     */
    public Builder royaltyAmount(String value) {
      this.dataFields.put("royaltyAmount", value);
      return this;
    }

    /**
     * Set the sKU value
     *
     * Description: A "stock keeping unit" used to track items sold to business partners.
     *
     * @param value
     *          The sKU value.
     * @return The builder for this class.
     */
    public Builder sKU(String value) {
      this.dataFields.put("sKU", value);
      return this;
    }

    /**
     * Set the vendorCategory value
     *
     * Description: A classification of vendors based on similar characteristics or attributes.
     *
     * @param value
     *          The vendorCategory value.
     * @return The builder for this class.
     */
    public Builder vendorCategory(String value) {
      this.dataFields.put("vendorCategory", value);
      return this;
    }

    /**
     * Set the weight value
     *
     * Description: Weight of a product
     *
     * @param value
     *          The weight value.
     * @return The builder for this class.
     */
    public Builder weight(String value) {
      this.dataFields.put("weight", value);
      return this;
    }

    /**
     * Set the fixedCostPerOrder value
     *
     * Description: The additional added fixed cost for a purchased product.
     *
     * @param value
     *          The fixedCostPerOrder value.
     * @return The builder for this class.
     */
    public Builder fixedCostPerOrder(String value) {
      this.dataFields.put("fixedCostPerOrder", value);
      return this;
    }

    /**
     * Set the purchasingLeadTime value
     *
     * Description: Number of days between placing an order and the actual delivery as promised by
     * the vendor.
     *
     * @param value
     *          The purchasingLeadTime value.
     * @return The builder for this class.
     */
    public Builder purchasingLeadTime(String value) {
      this.dataFields.put("purchasingLeadTime", value);
      return this;
    }

    /**
     * Set the classification value
     *
     * Description: x not implemented
     *
     * @param value
     *          The classification value.
     * @return The builder for this class.
     */
    public Builder classification(String value) {
      this.dataFields.put("classification", value);
      return this;
    }

    /**
     * Set the quantityPerPackage value
     *
     * Description: The number of a specific item that comes in one package.
     *
     * @param value
     *          The quantityPerPackage value.
     * @return The builder for this class.
     */
    public Builder quantityPerPackage(String value) {
      this.dataFields.put("quantityPerPackage", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the productCategoryKey value
     *
     * Description: Product Category Key
     *
     * @param value
     *          The productCategoryKey value.
     * @return The builder for this class.
     */
    public Builder productCategoryKey(String value) {
      this.dataFields.put("productCategoryKey", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the priceEffectiveFrom value
     *
     * Description: The time from which a price is valid.
     *
     * @param value
     *          The priceEffectiveFrom value.
     * @return The builder for this class.
     */
    public Builder priceEffectiveFrom(String value) {
      this.dataFields.put("priceEffectiveFrom", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the vendorProductNo value
     *
     * Description: The identifier used by a vendor to indentify a product being purchased by their
     * business partners.
     *
     * @param value
     *          The vendorProductNo value.
     * @return The builder for this class.
     */
    public Builder vendorProductNo(String value) {
      this.dataFields.put("vendorProductNo", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the iSOCode value
     *
     * Description: A coding standard for countries.
     *
     * @param value
     *          The iSOCode value.
     * @return The builder for this class.
     */
    public Builder iSOCode(String value) {
      this.dataFields.put("iSOCode", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportProductData build() {
      return new ImportProductData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportProductData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
