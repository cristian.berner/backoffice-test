/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for UnitOfMeasureData
 *
 * @author plujan
 *
 */
public class UnitOfMeasureData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the costingPrecision value
     *
     * Description: Rounding used costing calculations
     *
     * @param value
     *          The costingPrecision value.
     * @return The builder for this class.
     */
    public Builder costingPrecision(String value) {
      this.dataFields.put("costingPrecision", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the standardPrecision value
     *
     * Description: Rule for rounding calculated amounts
     *
     * @param value
     *          The standardPrecision value.
     * @return The builder for this class.
     */
    public Builder standardPrecision(String value) {
      this.dataFields.put("standardPrecision", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the symbol value
     *
     * Description: An abbreviated description used to define a unit of measure or currency.
     *
     * @param value
     *          The symbol value.
     * @return The builder for this class.
     */
    public Builder symbol(String value) {
      this.dataFields.put("symbol", value);
      return this;
    }

    /**
     * Set the eDICode value
     *
     * Description: UOM EDI Code
     *
     * @param value
     *          The eDICode value.
     * @return The builder for this class.
     */
    public Builder eDICode(String value) {
      this.dataFields.put("eDICode", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public UnitOfMeasureData build() {
      return new UnitOfMeasureData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private UnitOfMeasureData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
