/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.paymentterm;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PaymentTermHeaderData
 *
 * @author plujan
 *
 */
public class PaymentTermHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the maturityDate3 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate3 value.
     * @return The builder for this class.
     */
    public Builder maturityDate3(String value) {
      this.dataFields.put("maturityDate3", value);
      return this;
    }

    /**
     * Set the maturityDate2 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate2 value.
     * @return The builder for this class.
     */
    public Builder maturityDate2(String value) {
      this.dataFields.put("maturityDate2", value);
      return this;
    }

    /**
     * Set the overduePaymentDayRule value
     *
     * Description: A plan to give until a specific day after the agreed payment term to pay without
     * penalty.
     *
     * @param value
     *          The overduePaymentDayRule value.
     * @return The builder for this class.
     */
    public Builder overduePaymentDayRule(String value) {
      this.dataFields.put("overduePaymentDayRule", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the nextBusinessDay value
     *
     * Description: Payment due on the next business day
     *
     * @param value
     *          The nextBusinessDay value.
     * @return The builder for this class.
     */
    public Builder nextBusinessDay(Boolean value) {
      this.dataFields.put("nextBusinessDay", value);
      return this;
    }

    /**
     * Set the fixedDueDate value
     *
     * Description: Payment is due on a fixed date
     *
     * @param value
     *          The fixedDueDate value.
     * @return The builder for this class.
     */
    public Builder fixedDueDate(Boolean value) {
      this.dataFields.put("fixedDueDate", value);
      return this;
    }

    /**
     * Set the offsetMonthDue value
     *
     * Description: Number of months (0=same 1=following)
     *
     * @param value
     *          The offsetMonthDue value.
     * @return The builder for this class.
     */
    public Builder offsetMonthDue(String value) {
      this.dataFields.put("offsetMonthDue", value);
      return this;
    }

    /**
     * Set the maturityDate1 value
     *
     * Description: The day of the month that invoices are due. 3 maturity dates can be defined.
     *
     * @param value
     *          The maturityDate1 value.
     * @return The builder for this class.
     */
    public Builder maturityDate1(String value) {
      this.dataFields.put("maturityDate1", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the overduePaymentDaysRule value
     *
     * Description: A plan to give additional days after the agreed payment term to pay without
     * penalty.
     *
     * @param value
     *          The overduePaymentDaysRule value.
     * @return The builder for this class.
     */
    public Builder overduePaymentDaysRule(String value) {
      this.dataFields.put("overduePaymentDaysRule", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PaymentTermHeaderData build() {
      return new PaymentTermHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PaymentTermHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
