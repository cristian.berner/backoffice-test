/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for ContactData
 *
 * @author plujan
 *
 */
public class ContactData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the lastName value
     *
     * Description: Last name of the contact
     *
     * @param value
     *          The lastName value.
     * @return The builder for this class.
     */
    public Builder lastName(String value) {
      this.dataFields.put("lastName", value);
      return this;
    }

    /**
     * Set the firstName value
     *
     * Description: Name of the contact
     *
     * @param value
     *          The firstName value.
     * @return The builder for this class.
     */
    public Builder firstName(String value) {
      this.dataFields.put("firstName", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the position value
     *
     * Description: A defined job title or ranking within a company.
     *
     * @param value
     *          The position value.
     * @return The builder for this class.
     */
    public Builder position(String value) {
      this.dataFields.put("position", value);
      return this;
    }

    /**
     * Set the phone value
     *
     * Description: A telephone number for a specified business partner.
     *
     * @param value
     *          The phone value.
     * @return The builder for this class.
     */
    public Builder phone(String value) {
      this.dataFields.put("phone", value);
      return this;
    }

    /**
     * Set the alternativePhone value
     *
     * Description: A second contact telephone number for a business partner.
     *
     * @param value
     *          The alternativePhone value.
     * @return The builder for this class.
     */
    public Builder alternativePhone(String value) {
      this.dataFields.put("alternativePhone", value);
      return this;
    }

    /**
     * Set the email value
     *
     * Description: An email address for a specified business partner.
     *
     * @param value
     *          The email value.
     * @return The builder for this class.
     */
    public Builder email(String value) {
      this.dataFields.put("email", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ContactData build() {
      return new ContactData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ContactData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
