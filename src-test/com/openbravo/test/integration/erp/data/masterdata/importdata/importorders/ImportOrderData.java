/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importorders;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProjectSelectorData;

/**
 *
 * Class for ImportOrderData
 *
 * @author plujan
 *
 */
public class ImportOrderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the activity value
     *
     * Description: A distinct activity defined and used in activity based management.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the taxSearchKey value
     *
     * Description: A fast method for finding a specific tax.
     *
     * @param value
     *          The taxSearchKey value.
     * @return The builder for this class.
     */
    public Builder taxSearchKey(String value) {
      this.dataFields.put("taxSearchKey", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the uPCEAN value
     *
     * Description: A bar code with a number to identify a product.
     *
     * @param value
     *          The uPCEAN value.
     * @return The builder for this class.
     */
    public Builder uPCEAN(String value) {
      this.dataFields.put("uPCEAN", value);
      return this;
    }

    /**
     * Set the businessPartnerSearchKey value
     *
     * Description: An abbreviated name (or a code) to help find a business partner.
     *
     * @param value
     *          The businessPartnerSearchKey value.
     * @return The builder for this class.
     */
    public Builder businessPartnerSearchKey(String value) {
      this.dataFields.put("businessPartnerSearchKey", value);
      return this;
    }

    /**
     * Set the salesRepresentative value
     *
     * Description: The person in charge of a document.
     *
     * @param value
     *          The salesRepresentative value.
     * @return The builder for this class.
     */
    public Builder salesRepresentative(String value) {
      this.dataFields.put("salesRepresentative", value);
      return this;
    }

    /**
     * Set the importProcessComplete value
     *
     * Description: A indication that the desired process has been completed successfully.
     *
     * @param value
     *          The importProcessComplete value.
     * @return The builder for this class.
     */
    public Builder importProcessComplete(Boolean value) {
      this.dataFields.put("importProcessComplete", value);
      return this;
    }

    /**
     * Set the unitPrice value
     *
     * Description: The price that will be paid for a specified item.
     *
     * @param value
     *          The unitPrice value.
     * @return The builder for this class.
     */
    public Builder unitPrice(String value) {
      this.dataFields.put("unitPrice", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the documentNo value
     *
     * Description: An often automatically generated identifier for all documents.
     *
     * @param value
     *          The documentNo value.
     * @return The builder for this class.
     */
    public Builder documentNo(String value) {
      this.dataFields.put("documentNo", value);
      return this;
    }

    /**
     * Set the salesOrder value
     *
     * Description: A unique and often automatically generated identifier for a sales order.
     *
     * @param value
     *          The salesOrder value.
     * @return The builder for this class.
     */
    public Builder salesOrder(String value) {
      this.dataFields.put("salesOrder", value);
      return this;
    }

    /**
     * Set the salesOrderLine value
     *
     * Description: A unique and often automatically generated identifier for a sales order line.
     *
     * @param value
     *          The salesOrderLine value.
     * @return The builder for this class.
     */
    public Builder salesOrderLine(String value) {
      this.dataFields.put("salesOrderLine", value);
      return this;
    }

    /**
     * Set the paymentTermKey value
     *
     * Description: Key of the Payment Term
     *
     * @param value
     *          The paymentTermKey value.
     * @return The builder for this class.
     */
    public Builder paymentTermKey(String value) {
      this.dataFields.put("paymentTermKey", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the addressLine1 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine1 value.
     * @return The builder for this class.
     */
    public Builder addressLine1(String value) {
      this.dataFields.put("addressLine1", value);
      return this;
    }

    /**
     * Set the project value
     *
     * Description: Identifier of a project defined within the Project Service Management module.
     *
     * @param value
     *          The project value.
     * @return The builder for this class.
     */
    public Builder project(ProjectSelectorData value) {
      this.dataFields.put("project", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the documentType value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentType value.
     * @return The builder for this class.
     */
    public Builder documentType(String value) {
      this.dataFields.put("documentType", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the paymentTerms value
     *
     * Description: The setup and timing defined to complete a specified payment.
     *
     * @param value
     *          The paymentTerms value.
     * @return The builder for this class.
     */
    public Builder paymentTerms(String value) {
      this.dataFields.put("paymentTerms", value);
      return this;
    }

    /**
     * Set the taxAmount value
     *
     * Description: The total sum of money requested by the government of the specified transaction.
     *
     * @param value
     *          The taxAmount value.
     * @return The builder for this class.
     */
    public Builder taxAmount(String value) {
      this.dataFields.put("taxAmount", value);
      return this;
    }

    /**
     * Set the email value
     *
     * Description: An email address for a specified business partner.
     *
     * @param value
     *          The email value.
     * @return The builder for this class.
     */
    public Builder email(String value) {
      this.dataFields.put("email", value);
      return this;
    }

    /**
     * Set the tax value
     *
     * Description: The percentage of money requested by the government for this specified product
     * or transaction.
     *
     * @param value
     *          The tax value.
     * @return The builder for this class.
     */
    public Builder tax(String value) {
      this.dataFields.put("tax", value);
      return this;
    }

    /**
     * Set the shippingCompany value
     *
     * Description: The name of the company making the shipment.
     *
     * @param value
     *          The shippingCompany value.
     * @return The builder for this class.
     */
    public Builder shippingCompany(String value) {
      this.dataFields.put("shippingCompany", value);
      return this;
    }

    /**
     * Set the warehouse value
     *
     * Description: The location where products arrive to or are sent from.
     *
     * @param value
     *          The warehouse value.
     * @return The builder for this class.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the orderedQuantity value
     *
     * Description: The number of an item involved in a transaction given in standard units. It is
     * used to determine price per unit.
     *
     * @param value
     *          The orderedQuantity value.
     * @return The builder for this class.
     */
    public Builder orderedQuantity(String value) {
      this.dataFields.put("orderedQuantity", value);
      return this;
    }

    /**
     * Set the lineDescription value
     *
     * Description: An additional detail regarding one item or charge.
     *
     * @param value
     *          The lineDescription value.
     * @return The builder for this class.
     */
    public Builder lineDescription(String value) {
      this.dataFields.put("lineDescription", value);
      return this;
    }

    /**
     * Set the regionName value
     *
     * Description: The name of an area in a specific country.
     *
     * @param value
     *          The regionName value.
     * @return The builder for this class.
     */
    public Builder regionName(String value) {
      this.dataFields.put("regionName", value);
      return this;
    }

    /**
     * Set the sKU value
     *
     * Description: A "stock keeping unit" used to track items sold to business partners.
     *
     * @param value
     *          The sKU value.
     * @return The builder for this class.
     */
    public Builder sKU(String value) {
      this.dataFields.put("sKU", value);
      return this;
    }

    /**
     * Set the trxOrganization value
     *
     * Description: The organization which performs or initiates the transaction.
     *
     * @param value
     *          The trxOrganization value.
     * @return The builder for this class.
     */
    public Builder trxOrganization(String value) {
      this.dataFields.put("trxOrganization", value);
      return this;
    }

    /**
     * Set the addressLine2 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine2 value.
     * @return The builder for this class.
     */
    public Builder addressLine2(String value) {
      this.dataFields.put("addressLine2", value);
      return this;
    }

    /**
     * Set the invoiceAddress value
     *
     * Description: The location where the invoice payment request will be sent.
     *
     * @param value
     *          The invoiceAddress value.
     * @return The builder for this class.
     */
    public Builder invoiceAddress(String value) {
      this.dataFields.put("invoiceAddress", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the documentTypeName value
     *
     * Description: A value defining what sequence and process setup are used to handle this
     * document.
     *
     * @param value
     *          The documentTypeName value.
     * @return The builder for this class.
     */
    public Builder documentTypeName(String value) {
      this.dataFields.put("documentTypeName", value);
      return this;
    }

    /**
     * Set the productSearchKey value
     *
     * Description: A fast method for finding an item.
     *
     * @param value
     *          The productSearchKey value.
     * @return The builder for this class.
     */
    public Builder productSearchKey(String value) {
      this.dataFields.put("productSearchKey", value);
      return this;
    }

    /**
     * Set the postalCode value
     *
     * Description: A identification code used to help get items to a specific location.
     *
     * @param value
     *          The postalCode value.
     * @return The builder for this class.
     */
    public Builder postalCode(String value) {
      this.dataFields.put("postalCode", value);
      return this;
    }

    /**
     * Set the salesTransaction value
     *
     * Description: An indication that a transfer of goods and money between business partners is
     * occurring.
     *
     * @param value
     *          The salesTransaction value.
     * @return The builder for this class.
     */
    public Builder salesTransaction(Boolean value) {
      this.dataFields.put("salesTransaction", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the cityName value
     *
     * Description: A populated defined area located within a larger area such as a state province
     * or country.
     *
     * @param value
     *          The cityName value.
     * @return The builder for this class.
     */
    public Builder cityName(String value) {
      this.dataFields.put("cityName", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the contactName value
     *
     * Description: The name of the person to get in touch with.
     *
     * @param value
     *          The contactName value.
     * @return The builder for this class.
     */
    public Builder contactName(String value) {
      this.dataFields.put("contactName", value);
      return this;
    }

    /**
     * Set the phone value
     *
     * Description: A telephone number for a specified business partner.
     *
     * @param value
     *          The phone value.
     * @return The builder for this class.
     */
    public Builder phone(String value) {
      this.dataFields.put("phone", value);
      return this;
    }

    /**
     * Set the iSOCountryCode value
     *
     * Description: The geographic country code for a country based on the ISO standard.
     *
     * @param value
     *          The iSOCountryCode value.
     * @return The builder for this class.
     */
    public Builder iSOCountryCode(String value) {
      this.dataFields.put("iSOCountryCode", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the salesCampaign value
     *
     * Description: An advertising effort aimed at increasing sales.
     *
     * @param value
     *          The salesCampaign value.
     * @return The builder for this class.
     */
    public Builder salesCampaign(String value) {
      this.dataFields.put("salesCampaign", value);
      return this;
    }

    /**
     * Set the orderDate value
     *
     * Description: The time listed on the order.
     *
     * @param value
     *          The orderDate value.
     * @return The builder for this class.
     */
    public Builder orderDate(String value) {
      this.dataFields.put("orderDate", value);
      return this;
    }

    /**
     * Set the orderReferenceNo value
     *
     * Description: Order Reference No.
     *
     * @param value
     *          The orderReferenceNo value.
     * @return The builder for this class.
     */
    public Builder orderReferenceNo(String value) {
      this.dataFields.put("orderReferenceNo", value);
      return this;
    }

    /**
     * Set the scheduledDeliveryDate value
     *
     * Description: The date that a task process or action is to be completed or delivered by.
     *
     * @param value
     *          The scheduledDeliveryDate value.
     * @return The builder for this class.
     */
    public Builder scheduledDeliveryDate(String value) {
      this.dataFields.put("scheduledDeliveryDate", value);
      return this;
    }

    /**
     * Set the businessPartnerUPC value
     *
     * Description: Business Partner UPC
     *
     * @param value
     *          The businessPartnerUPC value.
     * @return The builder for this class.
     */
    public Builder businessPartnerUPC(String value) {
      this.dataFields.put("businessPartnerUPC", value);
      return this;
    }

    /**
     * Set the uPCAddress value
     *
     * Description: UPC Address
     *
     * @param value
     *          The uPCAddress value.
     * @return The builder for this class.
     */
    public Builder uPCAddress(String value) {
      this.dataFields.put("uPCAddress", value);
      return this;
    }

    /**
     * Set the billToUPC value
     *
     * Description: Bill to UPC
     *
     * @param value
     *          The billToUPC value.
     * @return The builder for this class.
     */
    public Builder billToUPC(String value) {
      this.dataFields.put("billToUPC", value);
      return this;
    }

    /**
     * Set the vendorUPC value
     *
     * Description: Vendor UPC
     *
     * @param value
     *          The vendorUPC value.
     * @return The builder for this class.
     */
    public Builder vendorUPC(String value) {
      this.dataFields.put("vendorUPC", value);
      return this;
    }

    /**
     * Set the attributeSetValue value
     *
     * Description: An attribute associated with a product as part of an attribute set.
     *
     * @param value
     *          The attributeSetValue value.
     * @return The builder for this class.
     */
    public Builder attributeSetValue(String value) {
      this.dataFields.put("attributeSetValue", value);
      return this;
    }

    /**
     * Set the attributesetident value
     *
     * Description: The identifier for the attribute set. This identifier is the result of all the
     * columns that are marked as isidentifier
     *
     * @param value
     *          The attributesetident value.
     * @return The builder for this class.
     */
    public Builder attributesetident(String value) {
      this.dataFields.put("attributesetident", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportOrderData build() {
      return new ImportOrderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportOrderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
