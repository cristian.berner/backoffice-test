/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.importdata.importbusinesspartner;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;

/**
 *
 * Class for ImportBusinessPartnerData
 *
 * @author plujan
 *
 */
public class ImportBusinessPartnerData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the groupKey value
     *
     * Description: Business Partner Group Key
     *
     * @param value
     *          The groupKey value.
     * @return The builder for this class.
     */
    public Builder groupKey(String value) {
      this.dataFields.put("groupKey", value);
      return this;
    }

    /**
     * Set the contactName value
     *
     * Description: The name of the person to get in touch with.
     *
     * @param value
     *          The contactName value.
     * @return The builder for this class.
     */
    public Builder contactName(String value) {
      this.dataFields.put("contactName", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the taxID value
     *
     * Description: The government defined unique number for assigning and paying taxes.
     *
     * @param value
     *          The taxID value.
     * @return The builder for this class.
     */
    public Builder taxID(String value) {
      this.dataFields.put("taxID", value);
      return this;
    }

    /**
     * Set the fax value
     *
     * Description: A fax number for a specified business partner.
     *
     * @param value
     *          The fax value.
     * @return The builder for this class.
     */
    public Builder fax(String value) {
      this.dataFields.put("fax", value);
      return this;
    }

    /**
     * Set the regionName value
     *
     * Description: The name of an area in a specific country.
     *
     * @param value
     *          The regionName value.
     * @return The builder for this class.
     */
    public Builder regionName(String value) {
      this.dataFields.put("regionName", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the addressLine1 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine1 value.
     * @return The builder for this class.
     */
    public Builder addressLine1(String value) {
      this.dataFields.put("addressLine1", value);
      return this;
    }

    /**
     * Set the addressLine2 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine2 value.
     * @return The builder for this class.
     */
    public Builder addressLine2(String value) {
      this.dataFields.put("addressLine2", value);
      return this;
    }

    /**
     * Set the contactDescription value
     *
     * Description: Description of Contact
     *
     * @param value
     *          The contactDescription value.
     * @return The builder for this class.
     */
    public Builder contactDescription(String value) {
      this.dataFields.put("contactDescription", value);
      return this;
    }

    /**
     * Set the cityName value
     *
     * Description: A populated defined area located within a larger area such as a state province
     * or country.
     *
     * @param value
     *          The cityName value.
     * @return The builder for this class.
     */
    public Builder cityName(String value) {
      this.dataFields.put("cityName", value);
      return this;
    }

    /**
     * Set the email value
     *
     * Description: An email address for a specified business partner.
     *
     * @param value
     *          The email value.
     * @return The builder for this class.
     */
    public Builder email(String value) {
      this.dataFields.put("email", value);
      return this;
    }

    /**
     * Set the partnerAddress value
     *
     * Description: The location of the selected business partner.
     *
     * @param value
     *          The partnerAddress value.
     * @return The builder for this class.
     */
    public Builder partnerAddress(String value) {
      this.dataFields.put("partnerAddress", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the password value
     *
     * Description: A secret code used to allow access to a specified window or tab.
     *
     * @param value
     *          The password value.
     * @return The builder for this class.
     */
    public Builder password(String value) {
      this.dataFields.put("password", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the importErrorMessage value
     *
     * Description: A message that appears when errors occur during the importing process.
     *
     * @param value
     *          The importErrorMessage value.
     * @return The builder for this class.
     */
    public Builder importErrorMessage(String value) {
      this.dataFields.put("importErrorMessage", value);
      return this;
    }

    /**
     * Set the importProcessComplete value
     *
     * Description: A indication that the desired process has been completed successfully.
     *
     * @param value
     *          The importProcessComplete value.
     * @return The builder for this class.
     */
    public Builder importProcessComplete(Boolean value) {
      this.dataFields.put("importProcessComplete", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the greeting value
     *
     * Description: A description often abbreviated of how to address a business partner.
     *
     * @param value
     *          The greeting value.
     * @return The builder for this class.
     */
    public Builder greeting(String value) {
      this.dataFields.put("greeting", value);
      return this;
    }

    /**
     * Set the postalAdd value
     *
     * Description: Additional ZIP or Postal code
     *
     * @param value
     *          The postalAdd value.
     * @return The builder for this class.
     */
    public Builder postalAdd(String value) {
      this.dataFields.put("postalAdd", value);
      return this;
    }

    /**
     * Set the nAICSSIC value
     *
     * Description: Standard Industry Code or its successor NAIC -
     * http://www.osha.gov/oshstats/sicser.html
     *
     * @param value
     *          The nAICSSIC value.
     * @return The builder for this class.
     */
    public Builder nAICSSIC(String value) {
      this.dataFields.put("nAICSSIC", value);
      return this;
    }

    /**
     * Set the birthday value
     *
     * Description: An anniversary of birth for a business partner.
     *
     * @param value
     *          The birthday value.
     * @return The builder for this class.
     */
    public Builder birthday(String value) {
      this.dataFields.put("birthday", value);
      return this;
    }

    /**
     * Set the position value
     *
     * Description: A defined job title or ranking within a company.
     *
     * @param value
     *          The position value.
     * @return The builder for this class.
     */
    public Builder position(String value) {
      this.dataFields.put("position", value);
      return this;
    }

    /**
     * Set the name2 value
     *
     * Description: Additional space to write the name of a business partner.
     *
     * @param value
     *          The name2 value.
     * @return The builder for this class.
     */
    public Builder name2(String value) {
      this.dataFields.put("name2", value);
      return this;
    }

    /**
     * Set the postalCode value
     *
     * Description: A identification code used to help get items to a specific location.
     *
     * @param value
     *          The postalCode value.
     * @return The builder for this class.
     */
    public Builder postalCode(String value) {
      this.dataFields.put("postalCode", value);
      return this;
    }

    /**
     * Set the dUNS value
     *
     * Description: Dun Bradstreet Number
     *
     * @param value
     *          The dUNS value.
     * @return The builder for this class.
     */
    public Builder dUNS(String value) {
      this.dataFields.put("dUNS", value);
      return this;
    }

    /**
     * Set the processed value
     *
     * Description: A confirmation that the associated documents or requests are processed.
     *
     * @param value
     *          The processed value.
     * @return The builder for this class.
     */
    public Builder processed(Boolean value) {
      this.dataFields.put("processed", value);
      return this;
    }

    /**
     * Set the alternativePhone value
     *
     * Description: A second contact telephone number for a business partner.
     *
     * @param value
     *          The alternativePhone value.
     * @return The builder for this class.
     */
    public Builder alternativePhone(String value) {
      this.dataFields.put("alternativePhone", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the phone value
     *
     * Description: A telephone number for a specified business partner.
     *
     * @param value
     *          The phone value.
     * @return The builder for this class.
     */
    public Builder phone(String value) {
      this.dataFields.put("phone", value);
      return this;
    }

    /**
     * Set the comments value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The comments value.
     * @return The builder for this class.
     */
    public Builder comments(String value) {
      this.dataFields.put("comments", value);
      return this;
    }

    /**
     * Set the bPContactGreeting value
     *
     * Description: Greeting for Business Partner Contact
     *
     * @param value
     *          The bPContactGreeting value.
     * @return The builder for this class.
     */
    public Builder bPContactGreeting(String value) {
      this.dataFields.put("bPContactGreeting", value);
      return this;
    }

    /**
     * Set the iSOCountryCode value
     *
     * Description: The geographic country code for a country based on the ISO standard.
     *
     * @param value
     *          The iSOCountryCode value.
     * @return The builder for this class.
     */
    public Builder iSOCountryCode(String value) {
      this.dataFields.put("iSOCountryCode", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ImportBusinessPartnerData build() {
      return new ImportBusinessPartnerData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ImportBusinessPartnerData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
