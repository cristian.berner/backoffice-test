/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *      Nono Carballo <nonofce@gmail.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for PriceListData
 *
 * @author plujan
 *
 */
public class PriceListData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the salesPriceList value
     *
     * Description: This is a Sales Price List
     *
     * @param value
     *          The salesPriceList value.
     * @return The builder for this class.
     */
    public Builder salesPriceList(Boolean value) {
      this.dataFields.put("salesPriceList", value);
      return this;
    }

    /**
     * Set the costBasedPriceList value
     *
     * Description: This is a Price List Based on Cost
     *
     * @param value
     *          The costBasedPriceList value.
     * @return The builder for this class.
     */
    public Builder costBasedPriceList(Boolean value) {
      this.dataFields.put("costBasedPriceList", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Whether price includes taxes
     *
     * @param value
     *          The priceIncludesTax value
     * @return The builder for this class.
     */
    public Builder priceIncludesTax(Boolean value) {
      this.dataFields.put("priceIncludesTax", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PriceListData build() {
      return new PriceListData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PriceListData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
