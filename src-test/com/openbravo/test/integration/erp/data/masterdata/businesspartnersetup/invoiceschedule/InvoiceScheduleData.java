/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.invoiceschedule;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for InvoiceScheduleData
 *
 * @author plujan
 *
 */
public class InvoiceScheduleData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the dayOfTheWeekCutOff value
     *
     * Description: Last day in the week for shipments to be included
     *
     * @param value
     *          The dayOfTheWeekCutOff value.
     * @return The builder for this class.
     */
    public Builder dayOfTheWeekCutOff(String value) {
      this.dataFields.put("dayOfTheWeekCutOff", value);
      return this;
    }

    /**
     * Set the invoiceCutOffDay value
     *
     * Description: Last day for including shipments
     *
     * @param value
     *          The invoiceCutOffDay value.
     * @return The builder for this class.
     */
    public Builder invoiceCutOffDay(String value) {
      this.dataFields.put("invoiceCutOffDay", value);
      return this;
    }

    /**
     * Set the invoiceOnEvenWeeks value
     *
     * Description: Send invoices on even weeks
     *
     * @param value
     *          The invoiceOnEvenWeeks value.
     * @return The builder for this class.
     */
    public Builder invoiceOnEvenWeeks(Boolean value) {
      this.dataFields.put("invoiceOnEvenWeeks", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the dayOfTheMonth value
     *
     * Description: Day of Invoice Generation
     *
     * @param value
     *          The dayOfTheMonth value.
     * @return The builder for this class.
     */
    public Builder dayOfTheMonth(String value) {
      this.dataFields.put("dayOfTheMonth", value);
      return this;
    }

    /**
     * Set the dayOfTheWeek value
     *
     * Description: Day to generate invoices
     *
     * @param value
     *          The dayOfTheWeek value.
     * @return The builder for this class.
     */
    public Builder dayOfTheWeek(String value) {
      this.dataFields.put("dayOfTheWeek", value);
      return this;
    }

    /**
     * Set the invoiceFrequency value
     *
     * Description: How often invoices will be generated
     *
     * @param value
     *          The invoiceFrequency value.
     * @return The builder for this class.
     */
    public Builder invoiceFrequency(String value) {
      this.dataFields.put("invoiceFrequency", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InvoiceScheduleData build() {
      return new InvoiceScheduleData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InvoiceScheduleData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
