/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data;

import java.util.LinkedHashMap;

/**
 * Class for profile data.
 *
 * @author elopio
 *
 */
public class ProfileData extends DataObject {

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the role.
     *
     * @param value
     *          The role name.
     * @return the object builder.
     */
    public Builder role(String value) {
      this.dataFields.put("role", value);
      return this;
    }

    /**
     * Set the client.
     *
     * @param value
     *          The client name.
     * @return the object builder.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the organization.
     *
     * @param value
     *          The organization name.
     * @return the object builder.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the warehouse.
     *
     * @param value
     *          The warehouse name.
     * @return the object builder.
     */
    public Builder warehouse(String value) {
      this.dataFields.put("warehouse", value);
      return this;
    }

    /**
     * Set the language.
     *
     * @param value
     *          The language name.
     * @return the object builder.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the default flag.
     *
     * @param value
     *          Indicates if this is the default profile.
     * @return the builder object.
     */
    public Builder isDefault(boolean value) {
      this.dataFields.put("isDefault", value);
      return this;
    }

    /**
     * Build the profile object.
     *
     * @return the profile object.
     */
    public ProfileData build() {
      return new ProfileData(this);
    }

  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private ProfileData(Builder builder) {
    dataFields = builder.dataFields;
  }

  /**
   * Get the role.
   *
   * @return the role
   */
  public String getRole() {
    return (String) dataFields.get("role");
  }

  /**
   * Get the client.
   *
   * @return the client.
   */
  public String getClient() {
    return (String) dataFields.get("client");
  }

  /**
   * Get the organization.
   *
   * @return the organization.
   */
  public String getOrganization() {
    return (String) dataFields.get("organization");
  }

  /**
   * Get the warehouse.
   *
   * @return the warehouse.
   */
  public String getWarehouse() {
    return (String) dataFields.get("warehouse");
  }

  /**
   * Get the language.
   *
   * @return the language.
   */
  public String getLanguage() {
    return (String) dataFields.get("language");
  }

  /**
   * Get if it is the default profile.
   *
   * @return the isDefault.
   */
  public boolean isDefault() {
    Object isDefault = dataFields.get("isDefault");
    if (isDefault == null) {
      return false;
    } else {
      return (Boolean) isDefault;
    }
  }
}
