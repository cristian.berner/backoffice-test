/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.dataset;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TableData
 *
 * @author plujan
 *
 */
public class TableData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the secondarywhereclause value
     *
     * Description: This is a secondary SQL where clause used by dbsourcemanager to filter data
     * inside a table.
     *
     * @param value
     *          The secondarywhereclause value.
     * @return The builder for this class.
     */
    public Builder secondarywhereclause(String value) {
      this.dataFields.put("secondarywhereclause", value);
      return this;
    }

    /**
     * Set the isBusinessObject value
     *
     * Description: Include Business Object
     *
     * @param value
     *          The isBusinessObject value.
     * @return The builder for this class.
     */
    public Builder isBusinessObject(Boolean value) {
      this.dataFields.put("isBusinessObject", value);
      return this;
    }

    /**
     * Set the sQLWhereClause value
     *
     * Description: The Where Clause indicates the HQL/SQL WHERE clause used for filtering data
     *
     * @param value
     *          The sQLWhereClause value.
     * @return The builder for this class.
     */
    public Builder sQLWhereClause(String value) {
      this.dataFields.put("sQLWhereClause", value);
      return this;
    }

    /**
     * Set the includeAllColumns value
     *
     * Description: Include all columns from the table in the dataset
     *
     * @param value
     *          The includeAllColumns value.
     * @return The builder for this class.
     */
    public Builder includeAllColumns(Boolean value) {
      this.dataFields.put("includeAllColumns", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the dataset value
     *
     * Description: Dataset
     *
     * @param value
     *          The dataset value.
     * @return The builder for this class.
     */
    public Builder dataset(String value) {
      this.dataFields.put("dataset", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the excludeAuditInfo value
     *
     * Description: Mark if the dataset table should exclude the audit info when comparing data
     *
     * @param value
     *          The excludeAuditInfo value.
     * @return The builder for this class.
     */
    public Builder excludeAuditInfo(Boolean value) {
      this.dataFields.put("excludeAuditInfo", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TableData build() {
      return new TableData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TableData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
