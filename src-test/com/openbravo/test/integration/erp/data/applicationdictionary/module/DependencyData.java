/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.module;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DependencyData
 *
 * @author plujan
 *
 */
public class DependencyData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the userEditableEnforcement value
     *
     * Description: Defines whether the enforcement can be overwritten by user
     *
     * @param value
     *          The userEditableEnforcement value.
     * @return The builder for this class.
     */
    public Builder userEditableEnforcement(Boolean value) {
      this.dataFields.put("userEditableEnforcement", value);
      return this;
    }

    /**
     * Set the dependencyEnforcement value
     *
     * Description: Dependency enforcement defines which are the versions of the dependent module
     * that are compatible with the parent one.
     *
     * @param value
     *          The dependencyEnforcement value.
     * @return The builder for this class.
     */
    public Builder dependencyEnforcement(String value) {
      this.dataFields.put("dependencyEnforcement", value);
      return this;
    }

    /**
     * Set the dependentModule value
     *
     * Description: Dependent Module
     *
     * @param value
     *          The dependentModule value.
     * @return The builder for this class.
     */
    public Builder dependentModule(String value) {
      this.dataFields.put("dependentModule", value);
      return this;
    }

    /**
     * Set the firstVersion value
     *
     * Description: First compatible version
     *
     * @param value
     *          The firstVersion value.
     * @return The builder for this class.
     */
    public Builder firstVersion(String value) {
      this.dataFields.put("firstVersion", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the lastVersion value
     *
     * Description: Last compatible version
     *
     * @param value
     *          The lastVersion value.
     * @return The builder for this class.
     */
    public Builder lastVersion(String value) {
      this.dataFields.put("lastVersion", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DependencyData build() {
      return new DependencyData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DependencyData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
