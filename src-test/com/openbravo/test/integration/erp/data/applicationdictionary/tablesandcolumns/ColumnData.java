/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.tablesandcolumns;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ColumnData
 *
 * @author plujan
 *
 */
public class ColumnData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the isautosave value
     *
     * Description: Defines if a button triggers autosave or not
     *
     * @param value
     *          The isautosave value.
     * @return The builder for this class.
     */
    public Builder isautosave(Boolean value) {
      this.dataFields.put("isautosave", value);
      return this;
    }

    /**
     * Set the validateOnNew value
     *
     * Description: Validations and callouts are executed when a new record is created.
     *
     * @param value
     *          The validateOnNew value.
     * @return The builder for this class.
     */
    public Builder validateOnNew(Boolean value) {
      this.dataFields.put("validateOnNew", value);
      return this;
    }

    /**
     * Set the position value
     *
     * Description: Determines the physical column position
     *
     * @param value
     *          The position value.
     * @return The builder for this class.
     */
    public Builder position(String value) {
      this.dataFields.put("position", value);
      return this;
    }

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the transient value
     *
     * Description: Mark if a column is transient and will be ignored when comparing data
     *
     * @param value
     *          The transient value.
     * @return The builder for this class.
     */
    public Builder transientValue(Boolean value) {
      this.dataFields.put("transient", value);
      return this;
    }

    /**
     * Set the transientCondition value
     *
     * Description: Java expression that will be evaluated and if the result is false the column
     * will be marked as transient
     *
     * @param value
     *          The transientCondition value.
     * @return The builder for this class.
     */
    public Builder transientCondition(String value) {
      this.dataFields.put("transientCondition", value);
      return this;
    }

    /**
     * Set the developmentStatus value
     *
     * Description: Development Status
     *
     * @param value
     *          The developmentStatus value.
     * @return The builder for this class.
     */
    public Builder developmentStatus(String value) {
      this.dataFields.put("developmentStatus", value);
      return this;
    }

    /**
     * Set the deEncryptable value
     *
     * Description: Is desencryptable
     *
     * @param value
     *          The deEncryptable value.
     * @return The builder for this class.
     */
    public Builder deEncryptable(Boolean value) {
      this.dataFields.put("deEncryptable", value);
      return this;
    }

    /**
     * Set the secondaryKey value
     *
     * Description: Is secondary key
     *
     * @param value
     *          The secondaryKey value.
     * @return The builder for this class.
     */
    public Builder secondaryKey(Boolean value) {
      this.dataFields.put("secondaryKey", value);
      return this;
    }

    /**
     * Set the storedInSession value
     *
     * Description: Is session attribute
     *
     * @param value
     *          The storedInSession value.
     * @return The builder for this class.
     */
    public Builder storedInSession(Boolean value) {
      this.dataFields.put("storedInSession", value);
      return this;
    }

    /**
     * Set the readOnlyLogic value
     *
     * Description: Logic to determine if field is read only (applies only when field is read-write)
     *
     * @param value
     *          The readOnlyLogic value.
     * @return The builder for this class.
     */
    public Builder readOnlyLogic(String value) {
      this.dataFields.put("readOnlyLogic", value);
      return this;
    }

    /**
     * Set the filterColumn value
     *
     * Description: Is this column used for finding rows in windows
     *
     * @param value
     *          The filterColumn value.
     * @return The builder for this class.
     */
    public Builder filterColumn(Boolean value) {
      this.dataFields.put("filterColumn", value);
      return this;
    }

    /**
     * Set the minValue value
     *
     * Description: The lowest possible value an object can take.
     *
     * @param value
     *          The minValue value.
     * @return The builder for this class.
     */
    public Builder minValue(String value) {
      this.dataFields.put("minValue", value);
      return this;
    }

    /**
     * Set the maxValue value
     *
     * Description: The highest possible value an item can have.
     *
     * @param value
     *          The maxValue value.
     * @return The builder for this class.
     */
    public Builder maxValue(String value) {
      this.dataFields.put("maxValue", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the updatable value
     *
     * Description: An indication that an item can be updated by the user.
     *
     * @param value
     *          The updatable value.
     * @return The builder for this class.
     */
    public Builder updatable(Boolean value) {
      this.dataFields.put("updatable", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the applicationElement value
     *
     * Description: An element that consolidates help descriptions and terms for a database column
     * and allows for a central maintenance.
     *
     * @param value
     *          The applicationElement value.
     * @return The builder for this class.
     */
    public Builder applicationElement(String value) {
      this.dataFields.put("applicationElement", value);
      return this;
    }

    /**
     * Set the callout value
     *
     * Description: A series of actions that occur when data is modified.
     *
     * @param value
     *          The callout value.
     * @return The builder for this class.
     */
    public Builder callout(String value) {
      this.dataFields.put("callout", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the validation value
     *
     * Description: A validation rule that defines how an entry is determined to be valid or
     * invalid.
     *
     * @param value
     *          The validation value.
     * @return The builder for this class.
     */
    public Builder validation(String value) {
      this.dataFields.put("validation", value);
      return this;
    }

    /**
     * Set the referenceSearchKey value
     *
     * Description: The exact reference specification for a list or a table.
     *
     * @param value
     *          The referenceSearchKey value.
     * @return The builder for this class.
     */
    public Builder referenceSearchKey(String value) {
      this.dataFields.put("referenceSearchKey", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Set the displayEncription value
     *
     * Description: An indication noting if the input box of a field will present full text or just
     * asterisks.
     *
     * @param value
     *          The displayEncription value.
     * @return The builder for this class.
     */
    public Builder displayEncription(Boolean value) {
      this.dataFields.put("displayEncription", value);
      return this;
    }

    /**
     * Set the identifier value
     *
     * Description: This column is part of the record identifier
     *
     * @param value
     *          The identifier value.
     * @return The builder for this class.
     */
    public Builder identifier(Boolean value) {
      this.dataFields.put("identifier", value);
      return this;
    }

    /**
     * Set the translation value
     *
     * Description: An indication that an item is translated.
     *
     * @param value
     *          The translation value.
     * @return The builder for this class.
     */
    public Builder translation(Boolean value) {
      this.dataFields.put("translation", value);
      return this;
    }

    /**
     * Set the mandatory value
     *
     * Description: An indication noting that completing in a field is required to proceed.
     *
     * @param value
     *          The mandatory value.
     * @return The builder for this class.
     */
    public Builder mandatory(Boolean value) {
      this.dataFields.put("mandatory", value);
      return this;
    }

    /**
     * Set the linkToParentColumn value
     *
     * Description: This column is a link to the parent table (e.g. header from lines) - incl.
     * Association key columns
     *
     * @param value
     *          The linkToParentColumn value.
     * @return The builder for this class.
     */
    public Builder linkToParentColumn(Boolean value) {
      this.dataFields.put("linkToParentColumn", value);
      return this;
    }

    /**
     * Set the keyColumn value
     *
     * Description: This column is the key in this table
     *
     * @param value
     *          The keyColumn value.
     * @return The builder for this class.
     */
    public Builder keyColumn(Boolean value) {
      this.dataFields.put("keyColumn", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: The first non-null value in a set of values. It is used as a default value for a
     * field when creating a record.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(String value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the length value
     *
     * Description: An indication of the column length as defined in the database.
     *
     * @param value
     *          The length value.
     * @return The builder for this class.
     */
    public Builder length(String value) {
      this.dataFields.put("length", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the dBColumnName value
     *
     * Description: The name of a column within the database.
     *
     * @param value
     *          The dBColumnName value.
     * @return The builder for this class.
     */
    public Builder dBColumnName(String value) {
      this.dataFields.put("dBColumnName", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ColumnData build() {
      return new ColumnData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ColumnData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
