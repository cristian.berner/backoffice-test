/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.reference;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for TableReferenceData
 *
 * @author plujan
 *
 */
public class TableReferenceData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the hqlwhereclause value
     *
     * Description: Hqlwhereclause
     *
     * @param value
     *          The hqlwhereclause value.
     * @return The builder for this class.
     */
    public Builder hqlwhereclause(String value) {
      this.dataFields.put("hqlwhereclause", value);
      return this;
    }

    /**
     * Set the hqlorderbyclause value
     *
     * Description: Hqlorderbyclause
     *
     * @param value
     *          The hqlorderbyclause value.
     * @return The builder for this class.
     */
    public Builder hqlorderbyclause(String value) {
      this.dataFields.put("hqlorderbyclause", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the displayedValue value
     *
     * Description: Displays Value column with the Display column
     *
     * @param value
     *          The displayedValue value.
     * @return The builder for this class.
     */
    public Builder displayedValue(Boolean value) {
      this.dataFields.put("displayedValue", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the sQLOrderByClause value
     *
     * Description: A specification of the SQL ORDER BY clause used for a displayed default sort of
     * records.
     *
     * @param value
     *          The sQLOrderByClause value.
     * @return The builder for this class.
     */
    public Builder sQLOrderByClause(String value) {
      this.dataFields.put("sQLOrderByClause", value);
      return this;
    }

    /**
     * Set the sQLWhereClause value
     *
     * Description: A specification of the SQL WHERE clause used for permanently filtering displayed
     * data.
     *
     * @param value
     *          The sQLWhereClause value.
     * @return The builder for this class.
     */
    public Builder sQLWhereClause(String value) {
      this.dataFields.put("sQLWhereClause", value);
      return this;
    }

    /**
     * Set the displayedColumn value
     *
     * Description: Column that will display
     *
     * @param value
     *          The displayedColumn value.
     * @return The builder for this class.
     */
    public Builder displayedColumn(String value) {
      this.dataFields.put("displayedColumn", value);
      return this;
    }

    /**
     * Set the keyColumn value
     *
     * Description: Unique identifier of a record
     *
     * @param value
     *          The keyColumn value.
     * @return The builder for this class.
     */
    public Builder keyColumn(String value) {
      this.dataFields.put("keyColumn", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the reference value
     *
     * Description: The data type of this field.
     *
     * @param value
     *          The reference value.
     * @return The builder for this class.
     */
    public Builder reference(String value) {
      this.dataFields.put("reference", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public TableReferenceData build() {
      return new TableReferenceData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private TableReferenceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
