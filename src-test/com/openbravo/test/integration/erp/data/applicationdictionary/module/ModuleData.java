/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.module;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ModuleData
 *
 * @author plujan
 *
 */
public class ModuleData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the applyConfigurationScript value
     *
     * Description: Apply the configuration script when building the application
     *
     * @param value
     *          The applyConfigurationScript value.
     * @return The builder for this class.
     */
    public Builder applyConfigurationScript(Boolean value) {
      this.dataFields.put("applyConfigurationScript", value);
      return this;
    }

    /**
     * Set the enabled value
     *
     * Description: Is the module enabled
     *
     * @param value
     *          The enabled value.
     * @return The builder for this class.
     */
    public Builder enabled(Boolean value) {
      this.dataFields.put("enabled", value);
      return this;
    }

    /**
     * Set the tier value
     *
     * Description: Tier for commercial modules
     *
     * @param value
     *          The tier value.
     * @return The builder for this class.
     */
    public Builder tier(String value) {
      this.dataFields.put("tier", value);
      return this;
    }

    /**
     * Set the commercial value
     *
     * Description: Commercial Module
     *
     * @param value
     *          The commercial value.
     * @return The builder for this class.
     */
    public Builder commercial(Boolean value) {
      this.dataFields.put("commercial", value);
      return this;
    }

    /**
     * Set the versionLabel value
     *
     * Description: Human readable version identifer
     *
     * @param value
     *          The versionLabel value.
     * @return The builder for this class.
     */
    public Builder versionLabel(String value) {
      this.dataFields.put("versionLabel", value);
      return this;
    }

    /**
     * Set the versionID value
     *
     * Description: Version identifier
     *
     * @param value
     *          The versionID value.
     * @return The builder for this class.
     */
    public Builder versionID(String value) {
      this.dataFields.put("versionID", value);
      return this;
    }

    /**
     * Set the referenceDataDescription value
     *
     * Description: Description for the reference data contained within the module
     *
     * @param value
     *          The referenceDataDescription value.
     * @return The builder for this class.
     */
    public Builder referenceDataDescription(String value) {
      this.dataFields.put("referenceDataDescription", value);
      return this;
    }

    /**
     * Set the updateInformation value
     *
     * Description: Information about the update
     *
     * @param value
     *          The updateInformation value.
     * @return The builder for this class.
     */
    public Builder updateInformation(String value) {
      this.dataFields.put("updateInformation", value);
      return this;
    }

    /**
     * Set the isTranslationModule value
     *
     * Description: This is a translation module
     *
     * @param value
     *          The isTranslationModule value.
     * @return The builder for this class.
     */
    public Builder isTranslationModule(Boolean value) {
      this.dataFields.put("isTranslationModule", value);
      return this;
    }

    /**
     * Set the hasReferenceData value
     *
     * Description: The module contains reference data
     *
     * @param value
     *          The hasReferenceData value.
     * @return The builder for this class.
     */
    public Builder hasReferenceData(Boolean value) {
      this.dataFields.put("hasReferenceData", value);
      return this;
    }

    /**
     * Set the hasChartOfAccounts value
     *
     * Description: The module contains a chart of accounts
     *
     * @param value
     *          The hasChartOfAccounts value.
     * @return The builder for this class.
     */
    public Builder hasChartOfAccounts(Boolean value) {
      this.dataFields.put("hasChartOfAccounts", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the javaPackage value
     *
     * Description: Java package for the module
     *
     * @param value
     *          The javaPackage value.
     * @return The builder for this class.
     */
    public Builder javaPackage(String value) {
      this.dataFields.put("javaPackage", value);
      return this;
    }

    /**
     * Set the licenseType value
     *
     * Description: Type of license
     *
     * @param value
     *          The licenseType value.
     * @return The builder for this class.
     */
    public Builder licenseType(String value) {
      this.dataFields.put("licenseType", value);
      return this;
    }

    /**
     * Set the author value
     *
     * Description: Module author/licensor
     *
     * @param value
     *          The author value.
     * @return The builder for this class.
     */
    public Builder author(String value) {
      this.dataFields.put("author", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the version value
     *
     * Description: Module version
     *
     * @param value
     *          The version value.
     * @return The builder for this class.
     */
    public Builder version(String value) {
      this.dataFields.put("version", value);
      return this;
    }

    /**
     * Set the uRL value
     *
     * Description: An address which can be accessed via internet.
     *
     * @param value
     *          The uRL value.
     * @return The builder for this class.
     */
    public Builder uRL(String value) {
      this.dataFields.put("uRL", value);
      return this;
    }

    /**
     * Set the type value
     *
     * Description: A distinct item characteristic used for processes and sometimes grouped within a
     * category.
     *
     * @param value
     *          The type value.
     * @return The builder for this class.
     */
    public Builder type(String value) {
      this.dataFields.put("type", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the licenseText value
     *
     * Description: Module license text
     *
     * @param value
     *          The licenseText value.
     * @return The builder for this class.
     */
    public Builder licenseText(String value) {
      this.dataFields.put("licenseText", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the inDevelopment value
     *
     * Description: Determines wether the module is currently in development process.
     *
     * @param value
     *          The inDevelopment value.
     * @return The builder for this class.
     */
    public Builder inDevelopment(Boolean value) {
      this.dataFields.put("inDevelopment", value);
      return this;
    }

    /**
     * Set the helpComment value
     *
     * Description: A comment that adds additional information to help users work with fields.
     *
     * @param value
     *          The helpComment value.
     * @return The builder for this class.
     */
    public Builder helpComment(String value) {
      this.dataFields.put("helpComment", value);
      return this;
    }

    /**
     * Set the translationRequired value
     *
     * Description: This module requires of tranlsation
     *
     * @param value
     *          The translationRequired value.
     * @return The builder for this class.
     */
    public Builder translationRequired(Boolean value) {
      this.dataFields.put("translationRequired", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: Language for the current module
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ModuleData build() {
      return new ModuleData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ModuleData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
