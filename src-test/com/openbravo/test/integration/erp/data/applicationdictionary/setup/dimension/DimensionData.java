/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.setup.dimension;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DimensionData
 *
 * @author plujan
 *
 */
public class DimensionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the joinGroup2 value
     *
     * Description: Join Group 2
     *
     * @param value
     *          The joinGroup2 value.
     * @return The builder for this class.
     */
    public Builder joinGroup2(String value) {
      this.dataFields.put("joinGroup2", value);
      return this;
    }

    /**
     * Set the dBTableName value
     *
     * Description: Name of the table in the database
     *
     * @param value
     *          The dBTableName value.
     * @return The builder for this class.
     */
    public Builder dBTableName(String value) {
      this.dataFields.put("dBTableName", value);
      return this;
    }

    /**
     * Set the joinGroup1 value
     *
     * Description: Join Group 1
     *
     * @param value
     *          The joinGroup1 value.
     * @return The builder for this class.
     */
    public Builder joinGroup1(String value) {
      this.dataFields.put("joinGroup1", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the lineNo value
     *
     * Description: Unique line for this document
     *
     * @param value
     *          The lineNo value.
     * @return The builder for this class.
     */
    public Builder lineNo(String value) {
      this.dataFields.put("lineNo", value);
      return this;
    }

    /**
     * Set the dBColumnName value
     *
     * Description: The name of a column within the database.
     *
     * @param value
     *          The dBColumnName value.
     * @return The builder for this class.
     */
    public Builder dBColumnName(String value) {
      this.dataFields.put("dBColumnName", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DimensionData build() {
      return new DimensionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DimensionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
