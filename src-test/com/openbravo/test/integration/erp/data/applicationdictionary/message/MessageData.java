/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:11
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.message;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for MessageData
 *
 * @author plujan
 *
 */
public class MessageData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the module value
     *
     * Description: Module
     *
     * @param value
     *          The module value.
     * @return The builder for this class.
     */
    public Builder module(String value) {
      this.dataFields.put("module", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the hint value
     *
     * Description: A help or advice provided regarding the application initiated message.
     *
     * @param value
     *          The hint value.
     * @return The builder for this class.
     */
    public Builder hint(String value) {
      this.dataFields.put("hint", value);
      return this;
    }

    /**
     * Set the messageText value
     *
     * Description: The text/content of the spplication initiated message.
     *
     * @param value
     *          The messageText value.
     * @return The builder for this class.
     */
    public Builder messageText(String value) {
      this.dataFields.put("messageText", value);
      return this;
    }

    /**
     * Set the messageType value
     *
     * Description: Type of message (Informational Menu or Error)
     *
     * @param value
     *          The messageType value.
     * @return The builder for this class.
     */
    public Builder messageType(String value) {
      this.dataFields.put("messageType", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MessageData build() {
      return new MessageData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MessageData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
