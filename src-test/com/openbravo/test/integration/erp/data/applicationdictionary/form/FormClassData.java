/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.form;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for FormClassData
 *
 * @author plujan
 *
 */
public class FormClassData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the javaClassName value
     *
     * Description: x not implemented
     *
     * @param value
     *          The javaClassName value.
     * @return The builder for this class.
     */
    public Builder javaClassName(String value) {
      this.dataFields.put("javaClassName", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the specialForm value
     *
     * Description: The name of the form being edited.
     *
     * @param value
     *          The specialForm value.
     * @return The builder for this class.
     */
    public Builder specialForm(String value) {
      this.dataFields.put("specialForm", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public FormClassData build() {
      return new FormClassData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private FormClassData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
