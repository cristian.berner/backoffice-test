/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:10
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.applicationdictionary.automatedtest.command;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for DocumentTextData
 *
 * @author plujan
 *
 */
public class DocumentTextData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the checkPrefix value
     *
     * Description: Check Prefix
     *
     * @param value
     *          The checkPrefix value.
     * @return The builder for this class.
     */
    public Builder checkPrefix(String value) {
      this.dataFields.put("checkPrefix", value);
      return this;
    }

    /**
     * Set the argType1 value
     *
     * Description: Arg. Type 1
     *
     * @param value
     *          The argType1 value.
     * @return The builder for this class.
     */
    public Builder argType1(String value) {
      this.dataFields.put("argType1", value);
      return this;
    }

    /**
     * Set the argType2 value
     *
     * Description: Arg. Type 2
     *
     * @param value
     *          The argType2 value.
     * @return The builder for this class.
     */
    public Builder argType2(String value) {
      this.dataFields.put("argType2", value);
      return this;
    }

    /**
     * Set the argType3 value
     *
     * Description: Arg. Type 3
     *
     * @param value
     *          The argType3 value.
     * @return The builder for this class.
     */
    public Builder argType3(String value) {
      this.dataFields.put("argType3", value);
      return this;
    }

    /**
     * Set the text4 value
     *
     * Description: Text 4
     *
     * @param value
     *          The text4 value.
     * @return The builder for this class.
     */
    public Builder text4(String value) {
      this.dataFields.put("text4", value);
      return this;
    }

    /**
     * Set the arg3 value
     *
     * Description: Arg. 3
     *
     * @param value
     *          The arg3 value.
     * @return The builder for this class.
     */
    public Builder arg3(String value) {
      this.dataFields.put("arg3", value);
      return this;
    }

    /**
     * Set the text3 value
     *
     * Description: Text 3
     *
     * @param value
     *          The text3 value.
     * @return The builder for this class.
     */
    public Builder text3(String value) {
      this.dataFields.put("text3", value);
      return this;
    }

    /**
     * Set the arg2 value
     *
     * Description: Arg. 2
     *
     * @param value
     *          The arg2 value.
     * @return The builder for this class.
     */
    public Builder arg2(String value) {
      this.dataFields.put("arg2", value);
      return this;
    }

    /**
     * Set the text2 value
     *
     * Description: Text 2
     *
     * @param value
     *          The text2 value.
     * @return The builder for this class.
     */
    public Builder text2(String value) {
      this.dataFields.put("text2", value);
      return this;
    }

    /**
     * Set the arg1 value
     *
     * Description: Arg. 1
     *
     * @param value
     *          The arg1 value.
     * @return The builder for this class.
     */
    public Builder arg1(String value) {
      this.dataFields.put("arg1", value);
      return this;
    }

    /**
     * Set the text1 value
     *
     * Description: Text 1
     *
     * @param value
     *          The text1 value.
     * @return The builder for this class.
     */
    public Builder text1(String value) {
      this.dataFields.put("text1", value);
      return this;
    }

    /**
     * Set the testDocument value
     *
     * Description: Test Document
     *
     * @param value
     *          The testDocument value.
     * @return The builder for this class.
     */
    public Builder testDocument(String value) {
      this.dataFields.put("testDocument", value);
      return this;
    }

    /**
     * Set the command value
     *
     * Description: The identifier of a table.
     *
     * @param value
     *          The command value.
     * @return The builder for this class.
     */
    public Builder command(String value) {
      this.dataFields.put("command", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public DocumentTextData build() {
      return new DocumentTextData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private DocumentTextData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
