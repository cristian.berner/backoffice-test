/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.materialrequirementmrp.transactions.purchasingplan;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;

/**
 *
 * Class for PurchasingPlanHeaderData
 *
 * @author plujan
 *
 */
public class PurchasingPlanHeaderData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the safetyLeadTime value
     *
     * Description: Safety Lead Time
     *
     * @param value
     *          The safetyLeadTime value.
     * @return The builder for this class.
     */
    public Builder safetyLeadTime(String value) {
      this.dataFields.put("safetyLeadTime", value);
      return this;
    }

    /**
     * Set the timeHorizon value
     *
     * Description: Time Horizon
     *
     * @param value
     *          The timeHorizon value.
     * @return The builder for this class.
     */
    public Builder timeHorizon(String value) {
      this.dataFields.put("timeHorizon", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the businessPartnerCategory value
     *
     * Description: A classification of business partners based on defined similarities.
     *
     * @param value
     *          The businessPartnerCategory value.
     * @return The builder for this class.
     */
    public Builder businessPartnerCategory(String value) {
      this.dataFields.put("businessPartnerCategory", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the productCategory value
     *
     * Description: A classification of items based on similar characteristics or attributes.
     *
     * @param value
     *          The productCategory value.
     * @return The builder for this class.
     */
    public Builder productCategory(String value) {
      this.dataFields.put("productCategory", value);
      return this;
    }

    /**
     * Set the vendor value
     *
     * Description: A business partner who sells products or services.
     *
     * @param value
     *          The vendor value.
     * @return The builder for this class.
     */
    public Builder vendor(String value) {
      this.dataFields.put("vendor", value);
      return this;
    }

    /**
     * Set the planner value
     *
     * Description: The person in charge of making an MRP plan.
     *
     * @param value
     *          The planner value.
     * @return The builder for this class.
     */
    public Builder planner(String value) {
      this.dataFields.put("planner", value);
      return this;
    }

    /**
     * Set the documentDate value
     *
     * Description: The time listed on the document.
     *
     * @param value
     *          The documentDate value.
     * @return The builder for this class.
     */
    public Builder documentDate(String value) {
      this.dataFields.put("documentDate", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public PurchasingPlanHeaderData build() {
      return new PurchasingPlanHeaderData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private PurchasingPlanHeaderData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
