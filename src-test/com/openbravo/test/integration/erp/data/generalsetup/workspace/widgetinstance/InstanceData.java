/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widgetinstance;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for InstanceData
 *
 * @author plujan
 *
 */
public class InstanceData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the relativePriority value
     *
     * Description: Relative Priority
     *
     * @param value
     *          The relativePriority value.
     * @return The builder for this class.
     */
    public Builder relativePriority(String value) {
      this.dataFields.put("relativePriority", value);
      return this;
    }

    /**
     * Set the visibleAtUser value
     *
     * Description: Visible at User
     *
     * @param value
     *          The visibleAtUser value.
     * @return The builder for this class.
     */
    public Builder visibleAtUser(String value) {
      this.dataFields.put("visibleAtUser", value);
      return this;
    }

    /**
     * Set the visibleAtRole value
     *
     * Description: Defines Role visibility.
     *
     * @param value
     *          The visibleAtRole value.
     * @return The builder for this class.
     */
    public Builder visibleAtRole(String value) {
      this.dataFields.put("visibleAtRole", value);
      return this;
    }

    /**
     * Set the columnPosition value
     *
     * Description: Column number where the widget is shown in Workspace tab
     *
     * @param value
     *          The columnPosition value.
     * @return The builder for this class.
     */
    public Builder columnPosition(String value) {
      this.dataFields.put("columnPosition", value);
      return this;
    }

    /**
     * Set the sequenceInColumn value
     *
     * Description: Position in the column where the widget is shown
     *
     * @param value
     *          The sequenceInColumn value.
     * @return The builder for this class.
     */
    public Builder sequenceInColumn(String value) {
      this.dataFields.put("sequenceInColumn", value);
      return this;
    }

    /**
     * Set the widgetClass value
     *
     * Description: Identifies a widget class definition
     *
     * @param value
     *          The widgetClass value.
     * @return The builder for this class.
     */
    public Builder widgetClass(String value) {
      this.dataFields.put("widgetClass", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the copiedFrom value
     *
     * Description: Copied From
     *
     * @param value
     *          The copiedFrom value.
     * @return The builder for this class.
     */
    public Builder copiedFrom(String value) {
      this.dataFields.put("copiedFrom", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InstanceData build() {
      return new InstanceData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InstanceData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
