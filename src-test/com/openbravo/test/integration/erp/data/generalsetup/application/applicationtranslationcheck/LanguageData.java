/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.applicationtranslationcheck;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for LanguageData
 *
 * @author plujan
 *
 */
public class LanguageData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the iSOCountryCode value
     *
     * Description: The geographic country code for a country based on the ISO standard.
     *
     * @param value
     *          The iSOCountryCode value.
     * @return The builder for this class.
     */
    public Builder iSOCountryCode(String value) {
      this.dataFields.put("iSOCountryCode", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the iSOLanguageCode value
     *
     * Description: Lower-case two-letter ISO-3166 code -
     * http://www.ics.uci.edu/pub/ietf/http/related/iso639.txt
     *
     * @param value
     *          The iSOLanguageCode value.
     * @return The builder for this class.
     */
    public Builder iSOLanguageCode(String value) {
      this.dataFields.put("iSOLanguageCode", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public LanguageData build() {
      return new LanguageData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private LanguageData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
