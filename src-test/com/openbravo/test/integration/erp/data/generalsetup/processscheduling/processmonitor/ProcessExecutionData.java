/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processmonitor;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ProcessExecutionData
 *
 * @author plujan
 *
 */
public class ProcessExecutionData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the endTime value
     *
     * Description: The time the process execution finished.
     *
     * @param value
     *          The endTime value.
     * @return The builder for this class.
     */
    public Builder endTime(String value) {
      this.dataFields.put("endTime", value);
      return this;
    }

    /**
     * Set the channel value
     *
     * Description: The method by which this Process request was executed/scheduled.
     *
     * @param value
     *          The channel value.
     * @return The builder for this class.
     */
    public Builder channel(String value) {
      this.dataFields.put("channel", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the startTime value
     *
     * Description: The time at which an item is due to or did start.
     *
     * @param value
     *          The startTime value.
     * @return The builder for this class.
     */
    public Builder startTime(String value) {
      this.dataFields.put("startTime", value);
      return this;
    }

    /**
     * Set the duration value
     *
     * Description: Elapsed time from when the Process began to when the Process finished its
     * execution.
     *
     * @param value
     *          The duration value.
     * @return The builder for this class.
     */
    public Builder duration(String value) {
      this.dataFields.put("duration", value);
      return this;
    }

    /**
     * Set the processLog value
     *
     * Description: The log of the Process execution.
     *
     * @param value
     *          The processLog value.
     * @return The builder for this class.
     */
    public Builder processLog(String value) {
      this.dataFields.put("processLog", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProcessExecutionData build() {
      return new ProcessExecutionData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProcessExecutionData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
