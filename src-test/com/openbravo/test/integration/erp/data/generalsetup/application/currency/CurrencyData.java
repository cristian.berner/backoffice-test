/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.currency;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CurrencyData
 *
 * @author plujan
 *
 */
public class CurrencyData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the currencySymbolAtTheRight value
     *
     * Description: Indicates if the currency symbol is at the right side of the amount
     *
     * @param value
     *          The currencySymbolAtTheRight value.
     * @return The builder for this class.
     */
    public Builder currencySymbolAtTheRight(Boolean value) {
      this.dataFields.put("currencySymbolAtTheRight", value);
      return this;
    }

    /**
     * Set the costingPrecision value
     *
     * Description: Rounding used costing calculations
     *
     * @param value
     *          The costingPrecision value.
     * @return The builder for this class.
     */
    public Builder costingPrecision(String value) {
      this.dataFields.put("costingPrecision", value);
      return this;
    }

    /**
     * Set the pricePrecision value
     *
     * Description: Price precision
     *
     * @param value
     *          The pricePrecision value.
     * @return The builder for this class.
     */
    public Builder pricePrecision(String value) {
      this.dataFields.put("pricePrecision", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the standardPrecision value
     *
     * Description: Rule for rounding calculated amounts
     *
     * @param value
     *          The standardPrecision value.
     * @return The builder for this class.
     */
    public Builder standardPrecision(String value) {
      this.dataFields.put("standardPrecision", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the symbol value
     *
     * Description: An abbreviated description used to define a unit of measure or currency.
     *
     * @param value
     *          The symbol value.
     * @return The builder for this class.
     */
    public Builder symbol(String value) {
      this.dataFields.put("symbol", value);
      return this;
    }

    /**
     * Set the iSOCode value
     *
     * Description: A coding standard for countries.
     *
     * @param value
     *          The iSOCode value.
     * @return The builder for this class.
     */
    public Builder iSOCode(String value) {
      this.dataFields.put("iSOCode", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CurrencyData build() {
      return new CurrencyData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CurrencyData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
