/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.client.client;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for InformationData
 *
 * @author plujan
 *
 */
public class InformationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the groupInvoiceLinesInAccounting value
     *
     * Description: Group Accounting Invoice Lines
     *
     * @param value
     *          The groupInvoiceLinesInAccounting value.
     * @return The builder for this class.
     */
    public Builder groupInvoiceLinesInAccounting(Boolean value) {
      this.dataFields.put("groupInvoiceLinesInAccounting", value);
      return this;
    }

    /**
     * Set the checkOrderOrganization value
     *
     * Description: The checkorderorg identifies the order organization and the business partner
     * organization
     *
     * @param value
     *          The checkOrderOrganization value.
     * @return The builder for this class.
     */
    public Builder checkOrderOrganization(Boolean value) {
      this.dataFields.put("checkOrderOrganization", value);
      return this;
    }

    /**
     * Set the checkShipmentOrganization value
     *
     * Description: The Checkinoutorg identifies the shipment organization and the business partner
     * organization.
     *
     * @param value
     *          The checkShipmentOrganization value.
     * @return The builder for this class.
     */
    public Builder checkShipmentOrganization(Boolean value) {
      this.dataFields.put("checkShipmentOrganization", value);
      return this;
    }

    /**
     * Set the allowNegativeStock value
     *
     * Description: Allow negative stock
     *
     * @param value
     *          The allowNegativeStock value.
     * @return The builder for this class.
     */
    public Builder allowNegativeStock(Boolean value) {
      this.dataFields.put("allowNegativeStock", value);
      return this;
    }

    /**
     * Set the priceList value
     *
     * Description: A catalog of selected items with prices defined generally or for a specific
     * partner.
     *
     * @param value
     *          The priceList value.
     * @return The builder for this class.
     */
    public Builder priceList(String value) {
      this.dataFields.put("priceList", value);
      return this;
    }

    /**
     * Set the discountCalculatedFromLineAmounts value
     *
     * Description: Payment Discount calculation does not include Taxes and Charges
     *
     * @param value
     *          The discountCalculatedFromLineAmounts value.
     * @return The builder for this class.
     */
    public Builder discountCalculatedFromLineAmounts(Boolean value) {
      this.dataFields.put("discountCalculatedFromLineAmounts", value);
      return this;
    }

    /**
     * Set the productForFreight value
     *
     * Description: Product for Freight
     *
     * @param value
     *          The productForFreight value.
     * @return The builder for this class.
     */
    public Builder productForFreight(String value) {
      this.dataFields.put("productForFreight", value);
      return this;
    }

    /**
     * Set the templateBPartner value
     *
     * Description: Business Partner used for creating new Business Partners on the fly
     *
     * @param value
     *          The templateBPartner value.
     * @return The builder for this class.
     */
    public Builder templateBPartner(String value) {
      this.dataFields.put("templateBPartner", value);
      return this;
    }

    /**
     * Set the primaryTreeSalesRegion value
     *
     * Description: Primary Tree Sales Region
     *
     * @param value
     *          The primaryTreeSalesRegion value.
     * @return The builder for this class.
     */
    public Builder primaryTreeSalesRegion(String value) {
      this.dataFields.put("primaryTreeSalesRegion", value);
      return this;
    }

    /**
     * Set the primaryTreeProject value
     *
     * Description: Project Tree
     *
     * @param value
     *          The primaryTreeProject value.
     * @return The builder for this class.
     */
    public Builder primaryTreeProject(String value) {
      this.dataFields.put("primaryTreeProject", value);
      return this;
    }

    /**
     * Set the primaryTreeProduct value
     *
     * Description: Product Tree
     *
     * @param value
     *          The primaryTreeProduct value.
     * @return The builder for this class.
     */
    public Builder primaryTreeProduct(String value) {
      this.dataFields.put("primaryTreeProduct", value);
      return this;
    }

    /**
     * Set the primaryTreeOrganization value
     *
     * Description: Organization Tree
     *
     * @param value
     *          The primaryTreeOrganization value.
     * @return The builder for this class.
     */
    public Builder primaryTreeOrganization(String value) {
      this.dataFields.put("primaryTreeOrganization", value);
      return this;
    }

    /**
     * Set the primaryTreeMenu value
     *
     * Description: Tree Menu
     *
     * @param value
     *          The primaryTreeMenu value.
     * @return The builder for this class.
     */
    public Builder primaryTreeMenu(String value) {
      this.dataFields.put("primaryTreeMenu", value);
      return this;
    }

    /**
     * Set the primaryTreeBPartner value
     *
     * Description: Business Partner Tree
     *
     * @param value
     *          The primaryTreeBPartner value.
     * @return The builder for this class.
     */
    public Builder primaryTreeBPartner(String value) {
      this.dataFields.put("primaryTreeBPartner", value);
      return this;
    }

    /**
     * Set the uOMForTime value
     *
     * Description: Standard Unit of Measure for Time
     *
     * @param value
     *          The uOMForTime value.
     * @return The builder for this class.
     */
    public Builder uOMForTime(String value) {
      this.dataFields.put("uOMForTime", value);
      return this;
    }

    /**
     * Set the uOMForLength value
     *
     * Description: Standard Unit of Measure for Length
     *
     * @param value
     *          The uOMForLength value.
     * @return The builder for this class.
     */
    public Builder uOMForLength(String value) {
      this.dataFields.put("uOMForLength", value);
      return this;
    }

    /**
     * Set the uOMForWeight value
     *
     * Description: Standard Unit of Measure for Weight
     *
     * @param value
     *          The uOMForWeight value.
     * @return The builder for this class.
     */
    public Builder uOMForWeight(String value) {
      this.dataFields.put("uOMForWeight", value);
      return this;
    }

    /**
     * Set the uOMForVolume value
     *
     * Description: Standard Unit of Measure for Volume
     *
     * @param value
     *          The uOMForVolume value.
     * @return The builder for this class.
     */
    public Builder uOMForVolume(String value) {
      this.dataFields.put("uOMForVolume", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InformationData build() {
      return new InformationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InformationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
