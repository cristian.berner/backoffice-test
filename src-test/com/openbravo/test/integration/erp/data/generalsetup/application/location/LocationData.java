/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.location;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for LocationData
 *
 * @author plujan
 *
 */
public class LocationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the country value
     *
     * Description: A state or a nation.
     *
     * @param value
     *          The country value.
     * @return The builder for this class.
     */
    public Builder country(String value) {
      this.dataFields.put("country", value);
      return this;
    }

    /**
     * Set the region value
     *
     * Description: An area of a specific country.
     *
     * @param value
     *          The region value.
     * @return The builder for this class.
     */
    public Builder region(String value) {
      this.dataFields.put("region", value);
      return this;
    }

    /**
     * Set the cityName value
     *
     * Description: A populated defined area located within a larger area such as a state province
     * or country.
     *
     * @param value
     *          The cityName value.
     * @return The builder for this class.
     */
    public Builder cityName(String value) {
      this.dataFields.put("cityName", value);
      return this;
    }

    /**
     * Set the postalCode value
     *
     * Description: A identification code used to help get items to a specific location.
     *
     * @param value
     *          The postalCode value.
     * @return The builder for this class.
     */
    public Builder postalCode(String value) {
      this.dataFields.put("postalCode", value);
      return this;
    }

    /**
     * Set the addressLine2 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine2 value.
     * @return The builder for this class.
     */
    public Builder addressLine2(String value) {
      this.dataFields.put("addressLine2", value);
      return this;
    }

    /**
     * Set the addressLine1 value
     *
     * Description: A space to write the location of a business partner.
     *
     * @param value
     *          The addressLine1 value.
     * @return The builder for this class.
     */
    public Builder addressLine1(String value) {
      this.dataFields.put("addressLine1", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public LocationData build() {
      return new LocationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private LocationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
