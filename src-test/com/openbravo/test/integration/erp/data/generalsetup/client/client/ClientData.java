/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.client.client;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ClientData
 *
 * @author plujan
 *
 */
public class ClientData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the currency value
     *
     * Description: Base Currency of the Client.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the sMTPAuthentification value
     *
     * Description: Your mail server requires Authentification
     *
     * @param value
     *          The sMTPAuthentification value.
     * @return The builder for this class.
     */
    public Builder sMTPAuthentification(Boolean value) {
      this.dataFields.put("sMTPAuthentification", value);
      return this;
    }

    /**
     * Set the multilingualDocuments value
     *
     * Description: Documents are Multi Lingual
     *
     * @param value
     *          The multilingualDocuments value.
     * @return The builder for this class.
     */
    public Builder multilingualDocuments(Boolean value) {
      this.dataFields.put("multilingualDocuments", value);
      return this;
    }

    /**
     * Set the requestUserPassword value
     *
     * Description: Password of the user name (ID) for mail processing
     *
     * @param value
     *          The requestUserPassword value.
     * @return The builder for this class.
     */
    public Builder requestUserPassword(String value) {
      this.dataFields.put("requestUserPassword", value);
      return this;
    }

    /**
     * Set the requestUser value
     *
     * Description: User Name (ID) of the email owner
     *
     * @param value
     *          The requestUser value.
     * @return The builder for this class.
     */
    public Builder requestUser(String value) {
      this.dataFields.put("requestUser", value);
      return this;
    }

    /**
     * Set the requestFolder value
     *
     * Description: EMail folder to process incoming emails; if empty INBOX is used
     *
     * @param value
     *          The requestFolder value.
     * @return The builder for this class.
     */
    public Builder requestFolder(String value) {
      this.dataFields.put("requestFolder", value);
      return this;
    }

    /**
     * Set the requestEmail value
     *
     * Description: EMail address to send automated mails from or receive mails for automated
     * processing (fully qualified)
     *
     * @param value
     *          The requestEmail value.
     * @return The builder for this class.
     */
    public Builder requestEmail(String value) {
      this.dataFields.put("requestEmail", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the mailHost value
     *
     * Description: Hostname of Mail Server for SMTP and IMAP
     *
     * @param value
     *          The mailHost value.
     * @return The builder for this class.
     */
    public Builder mailHost(String value) {
      this.dataFields.put("mailHost", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    public Builder centralMaintenance(Boolean value) {
      this.dataFields.put("acctdimCentrallyMaintained", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ClientData build() {
      return new ClientData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ClientData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
