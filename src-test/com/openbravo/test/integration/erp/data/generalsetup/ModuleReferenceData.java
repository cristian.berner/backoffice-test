/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup;

/**
 * Class for Reference data.
 *
 * @author elopio
 *
 */
public class ModuleReferenceData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Reference Data:\n" + "name=%s,\n"
      + "version=%s,\n" + "description=%s,\n" + "language=%s,\n]";

  /* Data for this test */
  /** The module name. */
  private String name;
  /** The module version. */
  private String version;
  /** The module description. */
  private String description;
  /** The module language. */
  private String language;
  /** The module OBX file path. */
  private String obxPath;

  // Other fields: help, author, URL

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {

    /** The module name. */
    private String name = null;
    /** The module version. */
    private String version = null;
    /** The module description. */
    private String description = null;
    /** The module language. */
    private String language = null;
    /** The module OBX file path. */
    private String obxPath = null;

    /**
     * Set the module name.
     *
     * @param value
     *          The module name.
     * @return the builder for this class.
     */
    public Builder name(String value) {
      this.name = value;
      return this;
    }

    /**
     * Set the module version.
     *
     * @param value
     *          The module version.
     * @return the builder for this class.
     */
    public Builder version(String value) {
      this.version = value;
      return this;
    }

    /**
     * Set the module description.
     *
     * @param value
     *          The module description.
     * @return the builder for this class.
     */
    public Builder description(String value) {
      this.description = value;
      return this;
    }

    /**
     * Set the module language.
     *
     * @param value
     *          The module language.
     * @return the builder for this class.
     */
    public Builder language(String value) {
      this.language = value;
      return this;
    }

    /**
     * Set the module OBX file path.
     *
     * @param value
     *          The module OBX file path.
     * @return the builder for this class.
     */
    public Builder obxPath(String value) {
      this.obxPath = value;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public ModuleReferenceData build() {
      return new ModuleReferenceData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private ModuleReferenceData(Builder builder) {
    name = builder.name;
    version = builder.version;
    description = builder.description;
    language = builder.language;
    obxPath = builder.obxPath;
  }

  /**
   * Get the module name.
   *
   * @return the module name.
   */
  public String getName() {
    return name;
  }

  /**
   * Get the module version.
   *
   * @return the module version.
   */
  public String getVersion() {
    return version;
  }

  /**
   * Get the module description.
   *
   * @return the module description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Get the module language.
   *
   * @return the module language.
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Get the module OBX file path.
   *
   * @return the module OBX file path.
   */
  public String getObxPath() {
    return obxPath;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, name, version, description, language,
        obxPath);
  }

}
