/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ProcessRequestData
 *
 * @author plujan
 *
 */
public class ProcessRequestData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the repetitions value
     *
     * Description: The number of times this element will repeat.
     *
     * @param value
     *          The repetitions value.
     * @return The builder for this class.
     */
    public Builder repetitions(String value) {
      this.dataFields.put("repetitions", value);
      return this;
    }

    /**
     * Set the numRepetitions value
     *
     * Description: The number of times this element will repeat.
     *
     * @param value
     *          The numRepetitions value.
     * @return The builder for this class.
     */
    public Builder numRepetitions(String value) {
      this.dataFields.put("numRepetitions", value);
      return this;
    }

    /**
     * Set the numberOfRepetitions value
     *
     * Description: The number of times this element will repeat.
     *
     * @param value
     *          The numberOfRepetitions value.
     * @return The builder for this class.
     */
    public Builder numberOfRepetitions(String value) {
      this.dataFields.put("numberOfRepetitions", value);
      return this;
    }

    /**
     * Set the finishes value
     *
     * Description: Specifiy that this item should finish.
     *
     * @param value
     *          The finishes value.
     * @return The builder for this class.
     */
    public Builder finishes(Boolean value) {
      this.dataFields.put("finishes", value);
      return this;
    }

    /**
     * Set the dailyOption value
     *
     * Description: Option to run a Process Request on a daily basis.
     *
     * @param value
     *          The dailyOption value.
     * @return The builder for this class.
     */
    public Builder dailyOption(String value) {
      this.dataFields.put("dailyOption", value);
      return this;
    }

    /**
     * Set the cronExpression value
     *
     * Description: A Quartz cron expression to schedule this Process Request.
     *
     * @param value
     *          The cronExpression value.
     * @return The builder for this class.
     */
    public Builder cronExpression(String value) {
      this.dataFields.put("cronExpression", value);
      return this;
    }

    /**
     * Set the status value
     *
     * Description: A defined state or position of a payment.
     *
     * @param value
     *          The status value.
     * @return The builder for this class.
     */
    public Builder status(String value) {
      this.dataFields.put("status", value);
      return this;
    }

    /**
     * Set the startTime value
     *
     * Description: The time at which an item is due to or did start.
     *
     * @param value
     *          The startTime value.
     * @return The builder for this class.
     */
    public Builder startTime(String value) {
      this.dataFields.put("startTime", value);
      return this;
    }

    /**
     * Set the startDate value
     *
     * Description: The date on which an item is due to start.
     *
     * @param value
     *          The startDate value.
     * @return The builder for this class.
     */
    public Builder startDate(String value) {
      this.dataFields.put("startDate", value);
      return this;
    }

    /**
     * Set the intervalInSeconds value
     *
     * Description: Interval in hours between an event (such as a process execution).
     *
     * @param value
     *          The intervalInSeconds value.
     * @return The builder for this class.
     */
    public Builder intervalInSeconds(String value) {
      this.dataFields.put("intervalInSeconds", value);
      return this;
    }

    /**
     * Set the dayInMonth value
     *
     * Description: The specific date within each month.
     *
     * @param value
     *          The dayInMonth value.
     * @return The builder for this class.
     */
    public Builder dayInMonth(String value) {
      this.dataFields.put("dayInMonth", value);
      return this;
    }

    /**
     * Set the monthlyOption value
     *
     * Description: An option by which to schedule a Process in a monthly manner.
     *
     * @param value
     *          The monthlyOption value.
     * @return The builder for this class.
     */
    public Builder monthlyOption(String value) {
      this.dataFields.put("monthlyOption", value);
      return this;
    }

    /**
     * Set the dayOfTheWeek value
     *
     * Description: A day within a week.
     *
     * @param value
     *          The dayOfTheWeek value.
     * @return The builder for this class.
     */
    public Builder dayOfTheWeek(String value) {
      this.dataFields.put("dayOfTheWeek", value);
      return this;
    }

    /**
     * Set the intervalInMinutes value
     *
     * Description: Interval in minutes between an event (such as a process execution).
     *
     * @param value
     *          The intervalInMinutes value.
     * @return The builder for this class.
     */
    public Builder intervalInMinutes(String value) {
      this.dataFields.put("intervalInMinutes", value);
      return this;
    }

    /**
     * Set the securityBasedOnRole value
     *
     * Description: A flag indicating whether access to an item should be based on a user's role
     * within the system.
     *
     * @param value
     *          The securityBasedOnRole value.
     * @return The builder for this class.
     */
    public Builder securityBasedOnRole(Boolean value) {
      this.dataFields.put("securityBasedOnRole", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the hourlyInterval value
     *
     * Description: Interval in hours between an event (such as a process execution).
     *
     * @param value
     *          The hourlyInterval value.
     * @return The builder for this class.
     */
    public Builder hourlyInterval(String value) {
      this.dataFields.put("hourlyInterval", value);
      return this;
    }

    /**
     * Set the frequency value
     *
     * Description: The number of times something occurs during a specified time period.
     *
     * @param value
     *          The frequency value.
     * @return The builder for this class.
     */
    public Builder frequency(String value) {
      this.dataFields.put("frequency", value);
      return this;
    }

    /**
     * Set the finishTime value
     *
     * Description: The time that this item will finish.
     *
     * @param value
     *          The finishTime value.
     * @return The builder for this class.
     */
    public Builder finishTime(String value) {
      this.dataFields.put("finishTime", value);
      return this;
    }

    /**
     * Set the finishDate value
     *
     * Description: The date that this item will finish.
     *
     * @param value
     *          The finishDate value.
     * @return The builder for this class.
     */
    public Builder finishDate(String value) {
      this.dataFields.put("finishDate", value);
      return this;
    }

    /**
     * Set the dailyInterval value
     *
     * Description: The daily interval between executions of a process.
     *
     * @param value
     *          The dailyInterval value.
     * @return The builder for this class.
     */
    public Builder dailyInterval(String value) {
      this.dataFields.put("dailyInterval", value);
      return this;
    }

    /**
     * Set the wednesday value
     *
     * Description: The day of the week Wednesday.
     *
     * @param value
     *          The wednesday value.
     * @return The builder for this class.
     */
    public Builder wednesday(Boolean value) {
      this.dataFields.put("wednesday", value);
      return this;
    }

    /**
     * Set the tuesday value
     *
     * Description: The day of the week Tuesday.
     *
     * @param value
     *          The tuesday value.
     * @return The builder for this class.
     */
    public Builder tuesday(Boolean value) {
      this.dataFields.put("tuesday", value);
      return this;
    }

    /**
     * Set the thursday value
     *
     * Description: The day of the week Thursday.
     *
     * @param value
     *          The thursday value.
     * @return The builder for this class.
     */
    public Builder thursday(Boolean value) {
      this.dataFields.put("thursday", value);
      return this;
    }

    /**
     * Set the sunday value
     *
     * Description: The day of the week Sunday.
     *
     * @param value
     *          The sunday value.
     * @return The builder for this class.
     */
    public Builder sunday(Boolean value) {
      this.dataFields.put("sunday", value);
      return this;
    }

    /**
     * Set the saturday value
     *
     * Description: The day of the week Saturday.
     *
     * @param value
     *          The saturday value.
     * @return The builder for this class.
     */
    public Builder saturday(Boolean value) {
      this.dataFields.put("saturday", value);
      return this;
    }

    /**
     * Set the monday value
     *
     * Description: The day of the week Monday.
     *
     * @param value
     *          The monday value.
     * @return The builder for this class.
     */
    public Builder monday(Boolean value) {
      this.dataFields.put("monday", value);
      return this;
    }

    /**
     * Set the friday value
     *
     * Description: The day of the week Friday.
     *
     * @param value
     *          The friday value.
     * @return The builder for this class.
     */
    public Builder friday(Boolean value) {
      this.dataFields.put("friday", value);
      return this;
    }

    /**
     * Set the process value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The process value.
     * @return The builder for this class.
     */
    public Builder process(String value) {
      this.dataFields.put("process", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the timing value
     *
     * Description: When to run a Process Request - either immediately at a later date or on a
     * recurring basis.
     *
     * @param value
     *          The timing value.
     * @return The builder for this class.
     */
    public Builder timing(String value) {
      this.dataFields.put("timing", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProcessRequestData build() {
      return new ProcessRequestData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProcessRequestData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
