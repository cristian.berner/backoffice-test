/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.systeminfo;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for SystemInfoData
 *
 * @author plujan
 *
 */
public class SystemInfoData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the showForgeLogoInLogin value
     *
     * Description: Show Openbravo Forge Logo in Login.
     *
     * @param value
     *          The showForgeLogoInLogin value.
     * @return The builder for this class.
     */
    public Builder showForgeLogoInLogin(Boolean value) {
      this.dataFields.put("showForgeLogoInLogin", value);
      return this;
    }

    /**
     * Set the yourCompanyURL value
     *
     * Description: URL of your Comapany.
     *
     * @param value
     *          The yourCompanyURL value.
     * @return The builder for this class.
     */
    public Builder yourCompanyURL(String value) {
      this.dataFields.put("yourCompanyURL", value);
      return this;
    }

    /**
     * Set the showCommunityBranding value
     *
     * Description: Show_Community_Branding
     *
     * @param value
     *          The showCommunityBranding value.
     * @return The builder for this class.
     */
    public Builder showCommunityBranding(Boolean value) {
      this.dataFields.put("showCommunityBranding", value);
      return this;
    }

    /**
     * Set the isusageauditenabled value
     *
     * Description: Isusageauditenabled
     *
     * @param value
     *          The isusageauditenabled value.
     * @return The builder for this class.
     */
    public Builder isusageauditenabled(Boolean value) {
      this.dataFields.put("isusageauditenabled", value);
      return this;
    }

    /**
     * Set the instancePurpose value
     *
     * Description: Instance_Purpose
     *
     * @param value
     *          The instancePurpose value.
     * @return The builder for this class.
     */
    public Builder instancePurpose(String value) {
      this.dataFields.put("instancePurpose", value);
      return this;
    }

    /**
     * Set the proxyUser value
     *
     * Description: User for the authenticated proxy
     *
     * @param value
     *          The proxyUser value.
     * @return The builder for this class.
     */
    public Builder proxyUser(String value) {
      this.dataFields.put("proxyUser", value);
      return this;
    }

    /**
     * Set the proxyPassword value
     *
     * Description: Password for the authenticated proxy
     *
     * @param value
     *          The proxyPassword value.
     * @return The builder for this class.
     */
    public Builder proxyPassword(String value) {
      this.dataFields.put("proxyPassword", value);
      return this;
    }

    /**
     * Set the requiresProxyAuthentication value
     *
     * Description: Is authentication requiered to use the proxy
     *
     * @param value
     *          The requiresProxyAuthentication value.
     * @return The builder for this class.
     */
    public Builder requiresProxyAuthentication(Boolean value) {
      this.dataFields.put("requiresProxyAuthentication", value);
      return this;
    }

    /**
     * Set the supportContact value
     *
     * Description: IT service support contact
     *
     * @param value
     *          The supportContact value.
     * @return The builder for this class.
     */
    public Builder supportContact(String value) {
      this.dataFields.put("supportContact", value);
      return this;
    }

    /**
     * Set the webServerVersion value
     *
     * Description: The version of Web Server
     *
     * @param value
     *          The webServerVersion value.
     * @return The builder for this class.
     */
    public Builder webServerVersion(String value) {
      this.dataFields.put("webServerVersion", value);
      return this;
    }

    /**
     * Set the webServer value
     *
     * Description: The Web Server being used by Openbravo
     *
     * @param value
     *          The webServer value.
     * @return The builder for this class.
     */
    public Builder webServer(String value) {
      this.dataFields.put("webServer", value);
      return this;
    }

    /**
     * Set the servletContainerVersion value
     *
     * Description: The version of Servlet Container Openbravo runs in.
     *
     * @param value
     *          The servletContainerVersion value.
     * @return The builder for this class.
     */
    public Builder servletContainerVersion(String value) {
      this.dataFields.put("servletContainerVersion", value);
      return this;
    }

    /**
     * Set the servletContainer value
     *
     * Description: The type of servlet container Openbravo runs in.
     *
     * @param value
     *          The servletContainer value.
     * @return The builder for this class.
     */
    public Builder servletContainer(String value) {
      this.dataFields.put("servletContainer", value);
      return this;
    }

    /**
     * Set the proxyServer value
     *
     * Description: Proxy server used to access the internet.
     *
     * @param value
     *          The proxyServer value.
     * @return The builder for this class.
     */
    public Builder proxyServer(String value) {
      this.dataFields.put("proxyServer", value);
      return this;
    }

    /**
     * Set the proxyPort value
     *
     * Description: Proxy port on the proxy server used to access the internet.
     *
     * @param value
     *          The proxyPort value.
     * @return The builder for this class.
     */
    public Builder proxyPort(String value) {
      this.dataFields.put("proxyPort", value);
      return this;
    }

    /**
     * Set the proxyRequired value
     *
     * Description: Proxy configuration required to access internet.
     *
     * @param value
     *          The proxyRequired value.
     * @return The builder for this class.
     */
    public Builder proxyRequired(Boolean value) {
      this.dataFields.put("proxyRequired", value);
      return this;
    }

    /**
     * Set the customizationAllowed value
     *
     * Description: Allow customizations in CORE module.
     *
     * @param value
     *          The customizationAllowed value.
     * @return The builder for this class.
     */
    public Builder customizationAllowed(Boolean value) {
      this.dataFields.put("customizationAllowed", value);
      return this;
    }

    /**
     * Set the codeRevision value
     *
     * Description: The code revision of the installed system
     *
     * @param value
     *          The codeRevision value.
     * @return The builder for this class.
     */
    public Builder codeRevision(String value) {
      this.dataFields.put("codeRevision", value);
      return this;
    }

    /**
     * Set the antVersion value
     *
     * Description: The version of Ant used to build the application.
     *
     * @param value
     *          The antVersion value.
     * @return The builder for this class.
     */
    public Builder antVersion(String value) {
      this.dataFields.put("antVersion", value);
      return this;
    }

    /**
     * Set the systemStatus value
     *
     * Description: System_Status
     *
     * @param value
     *          The systemStatus value.
     * @return The builder for this class.
     */
    public Builder systemStatus(String value) {
      this.dataFields.put("systemStatus", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public SystemInfoData build() {
      return new SystemInfoData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private SystemInfoData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
