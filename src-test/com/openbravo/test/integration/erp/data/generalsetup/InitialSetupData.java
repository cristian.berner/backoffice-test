/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for Initial Setup data. This data is shared by Initial Client Setup and Initial
 * Organization Setup.
 *
 * @author elopio
 *
 */
public class InitialSetupData {

  /** Formatting string used to get a string representation of this object. */
  private static final String FORMAT_STRING_REPRESENTATION = "[Initial Setup data:\n"
      + "currency=%s,\n" + "include accounting=%b,\n" + "accounting file=%s,\n"
      + "reference data=%s,\n" + "business partner dimension=%b,\n" + "product dimension=%b,\n"
      + "project dimension=%b,\n" + "campaign dimension=%b,\n" + "sales region dimension=%b]";

  /* Data fields. */
  /** The client or organization currency. */
  private String currency;
  /** Indicates whether accounting should be included or not. */
  private Boolean includeAccounting;
  /** The client or organization accounting file. */
  private String accountingFile;
  /** The client or organization reference data. */
  private List<ModuleReferenceData> referenceDataList;
  /* Accounting dimensions. */
  /** Business partner dimension */
  private Boolean dimensionBusinessPartner;
  /** Product dimension */
  private Boolean dimensionProduct;
  /** Project dimension */
  private Boolean dimensionProject;
  /** Campaign dimension */
  private Boolean dimensionCampaign;
  /** Sales region dimension */
  private Boolean dimensionSalesRegion;

  /**
   * Class builder.
   *
   * @author elopio
   *
   */
  public static class Builder {
    /** The client or organization currency. */
    private String currency = null;
    /** Indicates whether accounting should be included or not. */
    private Boolean includeAccounting = null;
    /** The client or organization accounting file. */
    private String accountingFile = null;
    /** The client or organization reference data. */
    private List<ModuleReferenceData> referenceDataList = null;
    /** Business partner dimension */
    private Boolean dimensionBusinessPartner = null;
    /** Product dimension */
    private Boolean dimensionProduct = null;
    /** Project dimension */
    private Boolean dimensionProject = null;
    /** Campaign dimension */
    private Boolean dimensionCampaign = null;
    /** Sales region dimension */
    private Boolean dimensionSalesRegion = null;

    /**
     * Set the currency value.
     *
     * @param value
     *          The client or organization currency.
     * @return the builder for this class.
     */
    public Builder currency(String value) {
      this.currency = value;
      return this;
    }

    /**
     * Set the include accounting value.
     *
     * @param value
     *          Indicates whether accounting should be included or not.
     * @return the builder for this class.
     */
    public Builder includeAccounting(Boolean value) {
      this.includeAccounting = value;
      return this;
    }

    /**
     * Set the accounting file value.
     *
     * @param value
     *          The client or organization accounting file.
     * @return the builder for this class.
     */
    public Builder accountingFile(String value) {
      this.accountingFile = value;
      return this;
    }

    /**
     * Set the reference data value.
     *
     * @param value
     *          The client or organization reference data.
     * @return the builder for this class.
     */
    public Builder referenceDataList(List<ModuleReferenceData> value) {
      this.referenceDataList = value;
      return this;
    }

    /**
     * Add a reference data value to the list.
     *
     * @param value
     *          The reference data value.
     * @return the builder for this class.
     */
    public Builder addReferenceData(ModuleReferenceData value) {
      if (this.referenceDataList == null) {
        this.referenceDataList = new ArrayList<ModuleReferenceData>();
      }
      this.referenceDataList.add(value);
      return this;
    }

    /**
     * Set the business partner dimension.
     *
     * @param value
     *          The business partner dimension.
     * @return the builder for this class.
     */
    public Builder dimensionBusinessPartner(Boolean value) {
      this.dimensionBusinessPartner = value;
      return this;
    }

    /**
     * Set the product dimension.
     *
     * @param value
     *          The product dimension.
     * @return the builder for this class.
     */
    public Builder dimensionProduct(Boolean value) {
      this.dimensionProduct = value;
      return this;
    }

    /**
     * Set the project dimension.
     *
     * @param value
     *          The project dimension.
     * @return the builder for this class.
     */
    public Builder dimensionProject(Boolean value) {
      this.dimensionProject = value;
      return this;
    }

    /**
     * Set the campaign dimension.
     *
     * @param value
     *          The campaign dimension.
     * @return the builder for this class.
     */
    public Builder dimensionCampaign(Boolean value) {
      this.dimensionCampaign = value;
      return this;
    }

    /**
     * Set the sales region dimension.
     *
     * @param value
     *          The sales region dimension.
     * @return the builder for this class.
     */
    public Builder dimensionSalesRegion(Boolean value) {
      this.dimensionSalesRegion = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param currencyValue
     *          The client or organization currency.
     *
     * @return the builder for this class.
     */
    public Builder requiredFields(String currencyValue) {
      this.currency = currencyValue;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public InitialSetupData build() {
      return new InitialSetupData(this);
    }
  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private InitialSetupData(Builder builder) {
    currency = builder.currency;
    includeAccounting = builder.includeAccounting;
    accountingFile = builder.accountingFile;
    referenceDataList = builder.referenceDataList;
    dimensionBusinessPartner = builder.dimensionBusinessPartner;
    dimensionProduct = builder.dimensionProduct;
    dimensionProject = builder.dimensionProject;
    dimensionCampaign = builder.dimensionCampaign;
    dimensionSalesRegion = builder.dimensionSalesRegion;
  }

  /**
   * Get the client or organization currency.
   *
   * @return the client or organization currency.
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * Indicates whether accounting should be included or not.
   *
   * @return true if accounting should be included. Otherwise, false.
   */
  public Boolean incluceAccounting() {
    return includeAccounting;
  }

  /**
   * Get the client or organization accounting file.
   *
   * @return the client or organization accounting file.
   */
  public String getAccountingFile() {
    return accountingFile;
  }

  /**
   * Get the list of client or organization reference data.
   *
   * @return the list of client reference data.
   */
  public List<ModuleReferenceData> getReferenceDataList() {
    return referenceDataList;
  }

  /**
   * Get the business partner dimension.
   *
   * @return true if the organization or client has the business partner dimension. Otherwise,
   *         false.
   */
  public Boolean hasBusinessPartnerDimension() {
    return dimensionBusinessPartner;
  }

  /**
   * Get the product dimension.
   *
   * @return true if the organization or client has the product dimension. Otherwise, false.
   */
  public Boolean hasProductDimension() {
    return dimensionProduct;
  }

  /**
   * Get the project dimension.
   *
   * @return true if the organization or client has the project dimension. Otherwise, false.
   */
  public Boolean hasProjectDimension() {
    return dimensionProject;
  }

  /**
   * Get the campaign dimension.
   *
   * @return true if the organization or client has the campaign dimension. Otherwise, false.
   */
  public Boolean hasCampaignDimension() {
    return dimensionCampaign;
  }

  /**
   * Get the sales region dimension.
   *
   * @return true if the organization or client has the sales region dimension. Otherwise, false.
   */
  public Boolean hasSalesRegionDimension() {
    return dimensionSalesRegion;
  }

  /**
   * Get the string representation of this data object.
   *
   * @return the string representation.
   */
  @Override
  public String toString() {
    return String.format(FORMAT_STRING_REPRESENTATION, currency, includeAccounting, accountingFile,
        referenceDataList.toString(), dimensionBusinessPartner, dimensionProduct, dimensionProject,
        dimensionCampaign, dimensionSalesRegion);
  }
}
