/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widget;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for WidgetTranslationData
 *
 * @author plujan
 *
 */
public class WidgetTranslationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the title value
     *
     * Description: Widget Title
     *
     * @param value
     *          The title value.
     * @return The builder for this class.
     */
    public Builder title(String value) {
      this.dataFields.put("title", value);
      return this;
    }

    /**
     * Set the widgetClass value
     *
     * Description: Identifies a widget class definition
     *
     * @param value
     *          The widgetClass value.
     * @return The builder for this class.
     */
    public Builder widgetClass(String value) {
      this.dataFields.put("widgetClass", value);
      return this;
    }

    /**
     * Set the translation value
     *
     * Description: An indication that an item is translated.
     *
     * @param value
     *          The translation value.
     * @return The builder for this class.
     */
    public Builder translation(Boolean value) {
      this.dataFields.put("translation", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public WidgetTranslationData build() {
      return new WidgetTranslationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private WidgetTranslationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
