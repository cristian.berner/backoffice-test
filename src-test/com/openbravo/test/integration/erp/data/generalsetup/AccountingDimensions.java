/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup;

/**
 * Class for accounting dimensions data
 *
 * @author elopio
 *
 */
public class AccountingDimensions {

  /** business partner dimension */
  private Boolean businessPartner;
  /** product dimension */
  private Boolean product;
  /** project dimension */
  private Boolean project;
  /** campaign dimension */
  private Boolean campaign;
  /** sales region dimension */
  private Boolean salesRegion;

  /**
   * Class constructor
   *
   * @param businessPartner
   *          businessPartner dimension
   * @param product
   *          product dimension
   * @param project
   *          project dimension
   * @param campaign
   *          campaign dimension
   * @param salesRegion
   *          sales region dimension
   */
  public AccountingDimensions(Boolean businessPartner, Boolean product, Boolean project,
      Boolean campaign, Boolean salesRegion) {
    this.businessPartner = businessPartner;
    this.product = product;
    this.project = project;
    this.campaign = campaign;
    this.salesRegion = salesRegion;

  }

  /**
   * @return the businessPartner dimension
   */
  public Boolean isBusinessPartner() {
    return businessPartner;
  }

  /**
   * @return the product dimension
   */
  public Boolean isProduct() {
    return product;
  }

  /**
   * @return the project dimension
   */
  public Boolean isProject() {
    return project;
  }

  /**
   * @return the campaign dimension
   */
  public Boolean isCampaign() {
    return campaign;
  }

  /**
   * @return the sales region
   */
  public Boolean isSalesRegion() {
    return salesRegion;
  }

}
