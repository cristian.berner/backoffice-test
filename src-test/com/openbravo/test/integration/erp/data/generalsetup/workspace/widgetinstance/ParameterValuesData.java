/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.workspace.widgetinstance;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ParameterValuesData
 *
 * @author plujan
 *
 */
public class ParameterValuesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the valueString value
     *
     * Description: Value String
     *
     * @param value
     *          The valueString value.
     * @return The builder for this class.
     */
    public Builder valueString(String value) {
      this.dataFields.put("valueString", value);
      return this;
    }

    /**
     * Set the valueNumber value
     *
     * Description: Value Number
     *
     * @param value
     *          The valueNumber value.
     * @return The builder for this class.
     */
    public Builder valueNumber(String value) {
      this.dataFields.put("valueNumber", value);
      return this;
    }

    /**
     * Set the valueDate value
     *
     * Description: Value Date
     *
     * @param value
     *          The valueDate value.
     * @return The builder for this class.
     */
    public Builder valueDate(String value) {
      this.dataFields.put("valueDate", value);
      return this;
    }

    /**
     * Set the parameter value
     *
     * Description: Parameter
     *
     * @param value
     *          The parameter value.
     * @return The builder for this class.
     */
    public Builder parameter(String value) {
      this.dataFields.put("parameter", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the eMObkmoWidgetInstanceID value
     *
     * Description: EM_Obkmo_Widget_Instance_ID
     *
     * @param value
     *          The eMObkmoWidgetInstanceID value.
     * @return The builder for this class.
     */
    public Builder eMObkmoWidgetInstanceID(String value) {
      this.dataFields.put("eMObkmoWidgetInstanceID", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ParameterValuesData build() {
      return new ParameterValuesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ParameterValuesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
