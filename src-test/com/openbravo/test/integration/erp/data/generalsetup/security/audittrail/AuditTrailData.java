/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.security.audittrail;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for AuditTrailData
 *
 * @author plujan
 *
 */
public class AuditTrailData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the eventTime value
     *
     * Description: Date and time for the event
     *
     * @param value
     *          The eventTime value.
     * @return The builder for this class.
     */
    public Builder eventTime(String value) {
      this.dataFields.put("eventTime", value);
      return this;
    }

    /**
     * Set the table value
     *
     * Description: A dictionary table used for this tab that points to the database table.
     *
     * @param value
     *          The table value.
     * @return The builder for this class.
     */
    public Builder table(String value) {
      this.dataFields.put("table", value);
      return this;
    }

    /**
     * Set the recordID value
     *
     * Description: An record identifier in the dictionary.
     *
     * @param value
     *          The recordID value.
     * @return The builder for this class.
     */
    public Builder recordID(String value) {
      this.dataFields.put("recordID", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the column value
     *
     * Description: A link to the database column of the table.
     *
     * @param value
     *          The column value.
     * @return The builder for this class.
     */
    public Builder column(String value) {
      this.dataFields.put("column", value);
      return this;
    }

    /**
     * Set the oldValue value
     *
     * Description: The old file data
     *
     * @param value
     *          The oldValue value.
     * @return The builder for this class.
     */
    public Builder oldValue(String value) {
      this.dataFields.put("oldValue", value);
      return this;
    }

    /**
     * Set the userContact value
     *
     * Description: An acquaintance to reach for information related to the business partner.
     *
     * @param value
     *          The userContact value.
     * @return The builder for this class.
     */
    public Builder userContact(String value) {
      this.dataFields.put("userContact", value);
      return this;
    }

    /**
     * Set the action value
     *
     * Description: A drop down list box indicating the next step to take.
     *
     * @param value
     *          The action value.
     * @return The builder for this class.
     */
    public Builder action(String value) {
      this.dataFields.put("action", value);
      return this;
    }

    /**
     * Set the recordRevision value
     *
     * Description: Record Revision
     *
     * @param value
     *          The recordRevision value.
     * @return The builder for this class.
     */
    public Builder recordRevision(String value) {
      this.dataFields.put("recordRevision", value);
      return this;
    }

    /**
     * Set the processtype value
     *
     * Description: Type of process
     *
     * @param value
     *          The processtype value.
     * @return The builder for this class.
     */
    public Builder processtype(String value) {
      this.dataFields.put("processtype", value);
      return this;
    }

    /**
     * Set the processDescription value
     *
     * Description: Process Description
     *
     * @param value
     *          The processDescription value.
     * @return The builder for this class.
     */
    public Builder processDescription(String value) {
      this.dataFields.put("processDescription", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the newValue value
     *
     * Description: New field value
     *
     * @param value
     *          The newValue value.
     * @return The builder for this class.
     */
    public Builder newValue(String value) {
      this.dataFields.put("newValue", value);
      return this;
    }

    /**
     * Set the processID value
     *
     * Description: Process
     *
     * @param value
     *          The processID value.
     * @return The builder for this class.
     */
    public Builder processID(String value) {
      this.dataFields.put("processID", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public AuditTrailData build() {
      return new AuditTrailData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private AuditTrailData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
