/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OrgSchemaData
 *
 * @author plujan
 *
 */
public class OrgSchemaData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the sequanceForBusinessPartner value
     *
     * Description: The sequence will be used to generate number for creating the sub-account for
     * Business Partner.
     *
     * @param value
     *          The sequanceForBusinessPartner value.
     * @return The builder for this class.
     */
    public Builder sequanceForBusinessPartner(String value) {
      this.dataFields.put("sequanceForBusinessPartner", value);
      return this;
    }

    /**
     * Set the sequenceForProduct value
     *
     * Description: The sequence will be used to generate number for creating the sub-account for
     * Product.
     *
     * @param value
     *          The sequenceForProduct value.
     * @return The builder for this class.
     */
    public Builder sequenceForProduct(String value) {
      this.dataFields.put("sequenceForProduct", value);
      return this;
    }

    /**
     * Set the subAccountLength value
     *
     * Description: This field indicates length of the sub-account number.
     *
     * @param value
     *          The subAccountLength value.
     * @return The builder for this class.
     */
    public Builder subAccountLength(String value) {
      this.dataFields.put("subAccountLength", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the createNewAcountForProduct value
     *
     * Description: Create New Acount for Product
     *
     * @param value
     *          The createNewAcountForProduct value.
     * @return The builder for this class.
     */
    public Builder createNewAcountForProduct(Boolean value) {
      this.dataFields.put("createNewAcountForProduct", value);
      return this;
    }

    /**
     * Set the accountLength value
     *
     * Description: Length of the account
     *
     * @param value
     *          The accountLength value.
     * @return The builder for this class.
     */
    public Builder accountLength(String value) {
      this.dataFields.put("accountLength", value);
      return this;
    }

    /**
     * Set the createNewAccountForBusinessParnter value
     *
     * Description: Create the new account for Business Partner
     *
     * @param value
     *          The createNewAccountForBusinessParnter value.
     * @return The builder for this class.
     */
    public Builder createNewAccountForBusinessParnter(Boolean value) {
      this.dataFields.put("createNewAccountForBusinessParnter", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OrgSchemaData build() {
      return new OrgSchemaData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OrgSchemaData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
