/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.countryregionandcity;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CountryData
 *
 * @author plujan
 *
 */
public class CountryData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the iBANLength value
     *
     * Description: The length of the IBAN code of a bank account is variable depending on the
     * country.
     *
     * @param value
     *          The iBANLength value.
     * @return The builder for this class.
     */
    public Builder iBANLength(String value) {
      this.dataFields.put("iBANLength", value);
      return this;
    }

    /**
     * Set the iBANCode value
     *
     * Description: Country Code asigned to a country in the IBAN structure.
     *
     * @param value
     *          The iBANCode value.
     * @return The builder for this class.
     */
    public Builder iBANCode(String value) {
      this.dataFields.put("iBANCode", value);
      return this;
    }

    /**
     * Set the language value
     *
     * Description: A method of communication being used.
     *
     * @param value
     *          The language value.
     * @return The builder for this class.
     */
    public Builder language(String value) {
      this.dataFields.put("language", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the iSOCountryCode value
     *
     * Description: The geographic country code for a country based on the ISO standard.
     *
     * @param value
     *          The iSOCountryCode value.
     * @return The builder for this class.
     */
    public Builder iSOCountryCode(String value) {
      this.dataFields.put("iSOCountryCode", value);
      return this;
    }

    /**
     * Set the additionalPostalFormat value
     *
     * Description: Additional Postal Format
     *
     * @param value
     *          The additionalPostalFormat value.
     * @return The builder for this class.
     */
    public Builder additionalPostalFormat(String value) {
      this.dataFields.put("additionalPostalFormat", value);
      return this;
    }

    /**
     * Set the additionalPostalCode value
     *
     * Description: Has Additional Postal Code
     *
     * @param value
     *          The additionalPostalCode value.
     * @return The builder for this class.
     */
    public Builder additionalPostalCode(Boolean value) {
      this.dataFields.put("additionalPostalCode", value);
      return this;
    }

    /**
     * Set the postalCodeFormat value
     *
     * Description: Postal code Format
     *
     * @param value
     *          The postalCodeFormat value.
     * @return The builder for this class.
     */
    public Builder postalCodeFormat(String value) {
      this.dataFields.put("postalCodeFormat", value);
      return this;
    }

    /**
     * Set the addressPrintFormat value
     *
     * Description: Format for printing this Address
     *
     * @param value
     *          The addressPrintFormat value.
     * @return The builder for this class.
     */
    public Builder addressPrintFormat(String value) {
      this.dataFields.put("addressPrintFormat", value);
      return this;
    }

    /**
     * Set the phoneNoFormat value
     *
     * Description: Phone Format
     *
     * @param value
     *          The phoneNoFormat value.
     * @return The builder for this class.
     */
    public Builder phoneNoFormat(String value) {
      this.dataFields.put("phoneNoFormat", value);
      return this;
    }

    /**
     * Set the regionName value
     *
     * Description: The name of an area in a specific country.
     *
     * @param value
     *          The regionName value.
     * @return The builder for this class.
     */
    public Builder regionName(String value) {
      this.dataFields.put("regionName", value);
      return this;
    }

    /**
     * Set the hasRegions value
     *
     * Description: Country contains Regions
     *
     * @param value
     *          The hasRegions value.
     * @return The builder for this class.
     */
    public Builder hasRegions(Boolean value) {
      this.dataFields.put("hasRegions", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CountryData build() {
      return new CountryData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CountryData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
