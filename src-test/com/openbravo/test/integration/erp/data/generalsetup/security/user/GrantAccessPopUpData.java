/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.security.user;

/**
 * Class for Create User data.
 *
 * @author plujan
 *
 */
public class GrantAccessPopUpData {

  // /** Formatting string used to get a string representation of this object. */
  // private static final String FORMAT_STRING_REPRESENTATION = "[User data:\n" +
  // "organization=%s,\n"
  // + "user name=%s]";

  /* Data fields. */
  /** The Role to assign to the user. */
  private String module;
  private String access;

  /**
   * Class builder.
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The user name to create. */
    private String module = null;
    private String access = null;

    /**
     * @param value
     *          The module value
     * @return the builder for this class.
     */
    public Builder module(String value) {
      this.module = value;
      return this;
    }

    /**
     * @param value
     *          The access value
     * @return the builder for this class.
     */
    public Builder access(String value) {
      this.access = value;
      return this;
    }

    /**
     * Set the fields for this object that are required on the ERP.
     *
     * @param moduleField
     *          The module value
     * @param accessField
     *          The access value
     *
     * @return the builder for this class.
     *
     */
    public Builder requiredFields(String moduleField, String accessField) {
      this.module = moduleField;
      this.access = accessField;
      return this;
    }

    /**
     * Build the data object.
     *
     * @return the data object.
     */
    public GrantAccessPopUpData build() {
      return new GrantAccessPopUpData(this);
    }

  }

  /**
   * Class constructor.
   *
   * @param builder
   *          The object builder.
   */
  private GrantAccessPopUpData(Builder builder) {
    module = builder.module;
    access = builder.access;
  }

  /**
   * @return The module of this object
   *
   */
  public String getModule() {
    return module;
  }

  /**
   * @return The access value of this object
   *
   */
  public String getAccess() {
    return access;
  }

  // /**
  // * Get the string representation of this data object.
  // *
  // * @return the string representation.
  // */
  // @Override
  // public String toString() {
  // return String.format(FORMAT_STRING_REPRESENTATION, organization, userName);
  // }
}
