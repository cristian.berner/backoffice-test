/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;

/**
 *
 * Class for InformationData
 *
 * @author plujan
 *
 */
public class InformationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the automaticWriteoffAmt value
     *
     * Description: Automatic_Writeoff_Amt
     *
     * @param value
     *          The automaticWriteoffAmt value.
     * @return The builder for this class.
     */
    public Builder automaticWriteoffAmt(String value) {
      this.dataFields.put("automaticWriteoffAmt", value);
      return this;
    }

    /**
     * Set the businessPartner value
     *
     * Description: Anyone who takes part in daily business operations by acting as a customer
     * employee etc.
     *
     * @param value
     *          The businessPartner value.
     * @return The builder for this class.
     */
    public Builder businessPartner(BusinessPartnerSelectorData value) {
      this.dataFields.put("businessPartner", value);
      return this;
    }

    /**
     * Set the taxID value
     *
     * Description: The government defined unique number for assigning and paying taxes.
     *
     * @param value
     *          The taxID value.
     * @return The builder for this class.
     */
    public Builder taxID(String value) {
      this.dataFields.put("taxID", value);
      return this;
    }

    /**
     * Set the dUNS value
     *
     * Description: Dun Bradstreet Number
     *
     * @param value
     *          The dUNS value.
     * @return The builder for this class.
     */
    public Builder dUNS(String value) {
      this.dataFields.put("dUNS", value);
      return this;
    }

    /**
     * Set the locationAddress value
     *
     * Description: A specific place or residence.
     *
     * @param value
     *          The locationAddress value.
     * @return The builder for this class.
     */
    public Builder locationAddress(LocationSelectorData value) {
      this.dataFields.put("locationAddress", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the aPRMPaymentDescription value
     *
     * Description: Purchase Invoice's Reference for Payment Description
     *
     * @param value
     *          The aPRMPaymentDescription value.
     * @return The builder for this class.
     */
    public Builder purchaseInvoiceReference(String value) {
      this.dataFields.put("aPRMPaymentDescription", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public InformationData build() {
      return new InformationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private InformationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
