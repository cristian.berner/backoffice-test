/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:13
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.enterprise.organizationtype;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OrganizationTypeData
 *
 * @author plujan
 *
 */
public class OrganizationTypeData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the businessUnit value
     *
     * Description: An organization with its independent accounts. It belongs with other business
     * units to a legal organization.
     *
     * @param value
     *          The businessUnit value.
     * @return The builder for this class.
     */
    public Builder businessUnit(Boolean value) {
      this.dataFields.put("businessUnit", value);
      return this;
    }

    /**
     * Set the legalEntity value
     *
     * Description: An organization which is a legal corporation.
     *
     * @param value
     *          The legalEntity value.
     * @return The builder for this class.
     */
    public Builder legalEntity(Boolean value) {
      this.dataFields.put("legalEntity", value);
      return this;
    }

    /**
     * Set the client value
     *
     * Description: Client for this installation.
     *
     * @param value
     *          The client value.
     * @return The builder for this class.
     */
    public Builder client(String value) {
      this.dataFields.put("client", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the legalEntityWithAccounting value
     *
     * Description: Defines if the Legal Entity has accounting
     *
     * @param value
     *          The legalEntityWithAccounting value.
     * @return The builder for this class.
     */
    public Builder legalEntityWithAccounting(Boolean value) {
      this.dataFields.put("legalEntityWithAccounting", value);
      return this;
    }

    /**
     * Set the transactionsAllowed value
     *
     * Description: Defines if transactions are allowed for the organization type
     *
     * @param value
     *          The transactionsAllowed value.
     * @return The builder for this class.
     */
    public Builder transactionsAllowed(Boolean value) {
      this.dataFields.put("transactionsAllowed", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OrganizationTypeData build() {
      return new OrganizationTypeData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OrganizationTypeData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
