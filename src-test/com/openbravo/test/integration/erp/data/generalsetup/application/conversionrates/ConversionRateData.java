/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.generalsetup.application.conversionrates;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ConversionRateData
 *
 * @author plujan
 *
 */
public class ConversionRateData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Set the toCurrency value
     *
     * Description: Target currency
     *
     * @param value
     *          The toCurrency value.
     * @return The builder for this class.
     */
    public Builder toCurrency(String value) {
      this.dataFields.put("toCurrency", value);
      return this;
    }

    /**
     * Set the currency value
     *
     * Description: An accepted medium of monetary exchange that may vary across countries.
     *
     * @param value
     *          The currency value.
     * @return The builder for this class.
     */
    public Builder currency(String value) {
      this.dataFields.put("currency", value);
      return this;
    }

    /**
     * Set the divideRateBy value
     *
     * Description: The rate by which the base unit will be divided by to create the converted unit.
     *
     * @param value
     *          The divideRateBy value.
     * @return The builder for this class.
     */
    public Builder divideRateBy(String value) {
      this.dataFields.put("divideRateBy", value);
      return this;
    }

    /**
     * Set the multipleRateBy value
     *
     * Description: The rate by which the base unit will be mutiplied by to create the converted
     * unit.
     *
     * @param value
     *          The multipleRateBy value.
     * @return The builder for this class.
     */
    public Builder multipleRateBy(String value) {
      this.dataFields.put("multipleRateBy", value);
      return this;
    }

    /**
     * Set the validToDate value
     *
     * Description: A parameter stating the ending time of a specified request.
     *
     * @param value
     *          The validToDate value.
     * @return The builder for this class.
     */
    public Builder validToDate(String value) {
      this.dataFields.put("validToDate", value);
      return this;
    }

    /**
     * Set the validFromDate value
     *
     * Description: A parameter stating the starting time of a specified request.
     *
     * @param value
     *          The validFromDate value.
     * @return The builder for this class.
     */
    public Builder validFromDate(String value) {
      this.dataFields.put("validFromDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ConversionRateData build() {
      return new ConversionRateData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ConversionRateData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
