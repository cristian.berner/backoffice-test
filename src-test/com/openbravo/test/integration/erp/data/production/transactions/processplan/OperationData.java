/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.processplan;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OperationData
 *
 * @author plujan
 *
 */
public class OperationData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the defaultValue value
     *
     * Description: A value that is shown whenever a record is created.
     *
     * @param value
     *          The defaultValue value.
     * @return The builder for this class.
     */
    public Builder defaultValue(Boolean value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Set the costCenterCost value
     *
     * Description: Cost Center Cost
     *
     * @param value
     *          The costCenterCost value.
     * @return The builder for this class.
     */
    public Builder costCenterCost(String value) {
      this.dataFields.put("costCenterCost", value);
      return this;
    }

    /**
     * Set the outsourcingCost value
     *
     * Description: Outsourcing Cost
     *
     * @param value
     *          The outsourcingCost value.
     * @return The builder for this class.
     */
    public Builder outsourcingCost(String value) {
      this.dataFields.put("outsourcingCost", value);
      return this;
    }

    /**
     * Set the outsourced value
     *
     * Description: A decision to have a task or phase completed by an external business partner.
     *
     * @param value
     *          The outsourced value.
     * @return The builder for this class.
     */
    public Builder outsourced(Boolean value) {
      this.dataFields.put("outsourced", value);
      return this;
    }

    /**
     * Set the globalUse value
     *
     * Description: Global Use
     *
     * @param value
     *          The globalUse value.
     * @return The builder for this class.
     */
    public Builder globalUse(Boolean value) {
      this.dataFields.put("globalUse", value);
      return this;
    }

    /**
     * Set the emptyCellsAreZero value
     *
     * Description: Empty Cells are Zero
     *
     * @param value
     *          The emptyCellsAreZero value.
     * @return The builder for this class.
     */
    public Builder emptyCellsAreZero(Boolean value) {
      this.dataFields.put("emptyCellsAreZero", value);
      return this;
    }

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the multiplier value
     *
     * Description: Multiplier
     *
     * @param value
     *          The multiplier value.
     * @return The builder for this class.
     */
    public Builder multiplier(String value) {
      this.dataFields.put("multiplier", value);
      return this;
    }

    /**
     * Set the searchKey value
     *
     * Description: A fast method for finding a particular record.
     *
     * @param value
     *          The searchKey value.
     * @return The builder for this class.
     */
    public Builder searchKey(String value) {
      this.dataFields.put("searchKey", value);
      return this;
    }

    /**
     * Set the preparationTime value
     *
     * Description: Preparation Time
     *
     * @param value
     *          The preparationTime value.
     * @return The builder for this class.
     */
    public Builder preparationTime(String value) {
      this.dataFields.put("preparationTime", value);
      return this;
    }

    /**
     * Set the costCenterUseTime value
     *
     * Description: The amount of time a process takes to complete.
     *
     * @param value
     *          The costCenterUseTime value.
     * @return The builder for this class.
     */
    public Builder costCenterUseTime(String value) {
      this.dataFields.put("costCenterUseTime", value);
      return this;
    }

    /**
     * Set the name value
     *
     * Description: A non-unique identifier for a record/document often used as a search tool.
     *
     * @param value
     *          The name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the processPlanVersion value
     *
     * Description: Process Plan Version
     *
     * @param value
     *          The processPlanVersion value.
     * @return The builder for this class.
     */
    public Builder processPlanVersion(String value) {
      this.dataFields.put("processPlanVersion", value);
      return this;
    }

    /**
     * Set the activity value
     *
     * Description: A series of actions carried out in sequential order.
     *
     * @param value
     *          The activity value.
     * @return The builder for this class.
     */
    public Builder activity(String value) {
      this.dataFields.put("activity", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the description value
     *
     * Description: A space to write additional related information.
     *
     * @param value
     *          The description value.
     * @return The builder for this class.
     */
    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OperationData build() {
      return new OperationData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OperationData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
