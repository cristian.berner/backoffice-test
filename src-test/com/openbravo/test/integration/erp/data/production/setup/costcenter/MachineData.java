/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.setup.costcenter;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for MachineData
 *
 * @author plujan
 *
 */
public class MachineData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the usageCoefficient value
     *
     * Description: Usage Coefficient
     *
     * @param value
     *          The usageCoefficient value.
     * @return The builder for this class.
     */
    public Builder usageCoefficient(String value) {
      this.dataFields.put("usageCoefficient", value);
      return this;
    }

    /**
     * Set the machine value
     *
     * Description: A tool use to aid in or fully complete a task.
     *
     * @param value
     *          The machine value.
     * @return The builder for this class.
     */
    public Builder machine(String value) {
      this.dataFields.put("machine", value);
      return this;
    }

    /**
     * Set the costCenterVersion value
     *
     * Description: The cost center being used during a specified time period.
     *
     * @param value
     *          The costCenterVersion value.
     * @return The builder for this class.
     */
    public Builder costCenterVersion(String value) {
      this.dataFields.put("costCenterVersion", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public MachineData build() {
      return new MachineData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private MachineData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
