/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.setup.machine;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for CostData
 *
 * @author plujan
 *
 */
public class CostData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the uOMAnnualCost value
     *
     * Description: UOM Annual Cost
     *
     * @param value
     *          The uOMAnnualCost value.
     * @return The builder for this class.
     */
    public Builder uOMAnnualCost(String value) {
      this.dataFields.put("uOMAnnualCost", value);
      return this;
    }

    /**
     * Set the idleTimeYear value
     *
     * Description: Idle Time/Year
     *
     * @param value
     *          The idleTimeYear value.
     * @return The builder for this class.
     */
    public Builder idleTimeYear(String value) {
      this.dataFields.put("idleTimeYear", value);
      return this;
    }

    /**
     * Set the workHoursDay value
     *
     * Description: Work Hours/Day
     *
     * @param value
     *          The workHoursDay value.
     * @return The builder for this class.
     */
    public Builder workHoursDay(String value) {
      this.dataFields.put("workHoursDay", value);
      return this;
    }

    /**
     * Set the workDaysYear value
     *
     * Description: Work Days/Year
     *
     * @param value
     *          The workDaysYear value.
     * @return The builder for this class.
     */
    public Builder workDaysYear(String value) {
      this.dataFields.put("workDaysYear", value);
      return this;
    }

    /**
     * Set the valueYear value
     *
     * Description: Value/Year
     *
     * @param value
     *          The valueYear value.
     * @return The builder for this class.
     */
    public Builder valueYear(String value) {
      this.dataFields.put("valueYear", value);
      return this;
    }

    /**
     * Set the amortization value
     *
     * Description: The depreciation or reduction of a product value over time.
     *
     * @param value
     *          The amortization value.
     * @return The builder for this class.
     */
    public Builder amortization(String value) {
      this.dataFields.put("amortization", value);
      return this;
    }

    /**
     * Set the toolsetAmount value
     *
     * Description: Toolset Amount
     *
     * @param value
     *          The toolsetAmount value.
     * @return The builder for this class.
     */
    public Builder toolsetAmount(String value) {
      this.dataFields.put("toolsetAmount", value);
      return this;
    }

    /**
     * Set the purchaseAmount value
     *
     * Description: Purchase Amount
     *
     * @param value
     *          The purchaseAmount value.
     * @return The builder for this class.
     */
    public Builder purchaseAmount(String value) {
      this.dataFields.put("purchaseAmount", value);
      return this;
    }

    /**
     * Set the costUOM value
     *
     * Description: The unit of measure associated with the cost located adjacent to this field.
     *
     * @param value
     *          The costUOM value.
     * @return The builder for this class.
     */
    public Builder costUOM(String value) {
      this.dataFields.put("costUOM", value);
      return this;
    }

    /**
     * Set the validFromDate value
     *
     * Description: A parameter stating the starting time of a specified request.
     *
     * @param value
     *          The validFromDate value.
     * @return The builder for this class.
     */
    public Builder validFromDate(String value) {
      this.dataFields.put("validFromDate", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the cost value
     *
     * Description: A charge related to conducting business.
     *
     * @param value
     *          The cost value.
     * @return The builder for this class.
     */
    public Builder cost(String value) {
      this.dataFields.put("cost", value);
      return this;
    }

    /**
     * Set the machine value
     *
     * Description: A tool use to aid in or fully complete a task.
     *
     * @param value
     *          The machine value.
     * @return The builder for this class.
     */
    public Builder machine(String value) {
      this.dataFields.put("machine", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public CostData build() {
      return new CostData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private CostData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
