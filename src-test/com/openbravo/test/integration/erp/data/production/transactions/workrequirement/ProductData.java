/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.workrequirement;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;

/**
 *
 * Class for ProductData
 *
 * @author plujan
 *
 */
public class ProductData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the componentCost value
     *
     * Description: Component Cost
     *
     * @param value
     *          The componentCost value.
     * @return The builder for this class.
     */
    public Builder componentCost(String value) {
      this.dataFields.put("componentCost", value);
      return this;
    }

    /**
     * Set the movementQuantity value
     *
     * Description: The number of items being moved from one location to another.
     *
     * @param value
     *          The movementQuantity value.
     * @return The builder for this class.
     */
    public Builder movementQuantity(String value) {
      this.dataFields.put("movementQuantity", value);
      return this;
    }

    /**
     * Set the orderUOM value
     *
     * Description: The unit of measure being used for the request.
     *
     * @param value
     *          The orderUOM value.
     * @return The builder for this class.
     */
    public Builder orderUOM(String value) {
      this.dataFields.put("orderUOM", value);
      return this;
    }

    /**
     * Set the orderQuantity value
     *
     * Description: The number of a certain item involved in the transaction shown in units which
     * differ from the standard UOM.
     *
     * @param value
     *          The orderQuantity value.
     * @return The builder for this class.
     */
    public Builder orderQuantity(String value) {
      this.dataFields.put("orderQuantity", value);
      return this;
    }

    /**
     * Set the uOM value
     *
     * Description: A non monetary unit of measure.
     *
     * @param value
     *          The uOM value.
     * @return The builder for this class.
     */
    public Builder uOM(String value) {
      this.dataFields.put("uOM", value);
      return this;
    }

    /**
     * Set the productionType value
     *
     * Description: A classification stating whether something has been created or used in the
     * sequence.
     *
     * @param value
     *          The productionType value.
     * @return The builder for this class.
     */
    public Builder productionType(String value) {
      this.dataFields.put("productionType", value);
      return this;
    }

    /**
     * Set the product value
     *
     * Description: An item produced by a process.
     *
     * @param value
     *          The product value.
     * @return The builder for this class.
     */
    public Builder product(ProductCompleteSelectorData value) {
      this.dataFields.put("product", value);
      return this;
    }

    /**
     * Set the wRPhase value
     *
     * Description: WR Phase
     *
     * @param value
     *          The wRPhase value.
     * @return The builder for this class.
     */
    public Builder wRPhase(String value) {
      this.dataFields.put("wRPhase", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ProductData build() {
      return new ProductData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ProductData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
