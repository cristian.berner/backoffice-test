/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.qualitycontrolreport;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for ValuesData
 *
 * @author plujan
 *
 */
public class ValuesData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the sequenceNumber value
     *
     * Description: The order of records in a specified document.
     *
     * @param value
     *          The sequenceNumber value.
     * @return The builder for this class.
     */
    public Builder sequenceNumber(String value) {
      this.dataFields.put("sequenceNumber", value);
      return this;
    }

    /**
     * Set the check value
     *
     * Description: Check
     *
     * @param value
     *          The check value.
     * @return The builder for this class.
     */
    public Builder check(Boolean value) {
      this.dataFields.put("check", value);
      return this;
    }

    /**
     * Set the value value
     *
     * Description: A statement of worth or importance given in many forms.
     *
     * @param value
     *          The value value.
     * @return The builder for this class.
     */
    public Builder value(String value) {
      this.dataFields.put("value", value);
      return this;
    }

    /**
     * Set the text value
     *
     * Description: A place to add observations related to a specified check point.
     *
     * @param value
     *          The text value.
     * @return The builder for this class.
     */
    public Builder text(String value) {
      this.dataFields.put("text", value);
      return this;
    }

    /**
     * Set the valueType value
     *
     * Description: Value Type
     *
     * @param value
     *          The valueType value.
     * @return The builder for this class.
     */
    public Builder valueType(String value) {
      this.dataFields.put("valueType", value);
      return this;
    }

    /**
     * Set the criticalControlPoint value
     *
     * Description: Critical Control Point
     *
     * @param value
     *          The criticalControlPoint value.
     * @return The builder for this class.
     */
    public Builder criticalControlPoint(String value) {
      this.dataFields.put("criticalControlPoint", value);
      return this;
    }

    /**
     * Set the measurementTime value
     *
     * Description: Measurement Time
     *
     * @param value
     *          The measurementTime value.
     * @return The builder for this class.
     */
    public Builder measurementTime(String value) {
      this.dataFields.put("measurementTime", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public ValuesData build() {
      return new ValuesData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private ValuesData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
