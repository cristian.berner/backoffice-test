/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */

package com.openbravo.test.integration.erp.data.production.transactions.workeffort;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * Class for OutsourcedData
 *
 * @author plujan
 *
 */
public class OutsourcedData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the cost value
     *
     * Description: A charge related to conducting business.
     *
     * @param value
     *          The cost value.
     * @return The builder for this class.
     */
    public Builder cost(String value) {
      this.dataFields.put("cost", value);
      return this;
    }

    /**
     * Set the invoiceLine value
     *
     * Description: A statement displaying one item or charge in an invoice.
     *
     * @param value
     *          The invoiceLine value.
     * @return The builder for this class.
     */
    public Builder invoiceLine(String value) {
      this.dataFields.put("invoiceLine", value);
      return this;
    }

    /**
     * Set the productionPlan value
     *
     * Description: The proposal for a how production will be carried out.
     *
     * @param value
     *          The productionPlan value.
     * @return The builder for this class.
     */
    public Builder productionPlan(String value) {
      this.dataFields.put("productionPlan", value);
      return this;
    }

    /**
     * Set the active value
     *
     * Description: A flag indicating whether this record is available for use or de-activated.
     *
     * @param value
     *          The active value.
     * @return The builder for this class.
     */
    public Builder active(Boolean value) {
      this.dataFields.put("active", value);
      return this;
    }

    /**
     * Set the organization value
     *
     * Description: Organizational entity within client
     *
     * @param value
     *          The organization value.
     * @return The builder for this class.
     */
    public Builder organization(String value) {
      this.dataFields.put("organization", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public OutsourcedData build() {
      return new OutsourcedData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private OutsourcedData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
