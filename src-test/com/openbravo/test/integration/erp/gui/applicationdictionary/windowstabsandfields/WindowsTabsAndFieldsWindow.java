/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.applicationdictionary.windowstabsandfields;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Windows, Tabs and Fields standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class WindowsTabsAndFieldsWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  private static final String TITLE = "Windows, Tabs, and Fields";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.APPLICATION_DICTIONARY,
      Menu.WINDOWS_TABS_AND_FIELDS };

  /**
   * Class constructor.
   *
   */
  public WindowsTabsAndFieldsWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    WindowTab windowTab = new WindowTab();
    addTopLevelTab(windowTab);
    super.load();
  }

  /**
   * Select and return the Tab tab.
   *
   * @return the Tab tab.
   */
  public TabTab selectTabTab() {
    return (TabTab) selectTab(TabTab.IDENTIFIER);
  }

  /**
   * Select and return the Window Translation tab.
   *
   * @return the Window Translation tab.
   */
  public WindowTranslationTab selectWindowTranslationTab() {
    return (WindowTranslationTab) selectTab(WindowTranslationTab.IDENTIFIER);
  }

  /**
   * Select and return the Access tab.
   *
   * @return the Access tab.
   */
  public AccessTab selectAccessTab() {
    return (AccessTab) selectTab(AccessTab.IDENTIFIER);
  }

  /**
   * Select and return the Tab Translation tab.
   *
   * @return the Tab Translation tab.
   */
  public TabTranslationTab selectTabTranslationTab() {
    return (TabTranslationTab) selectTab(TabTranslationTab.IDENTIFIER);
  }

  /**
   * Select and return the Field tab.
   *
   * @return the Field tab.
   */
  public FieldTab selectFieldTab() {
    return (FieldTab) selectTab(FieldTab.IDENTIFIER);
  }

  /**
   * Select and return the Field Translation tab.
   *
   * @return the Field Translation tab.
   */
  public FieldTranslationTab selectFieldTranslationTab() {
    return (FieldTranslationTab) selectTab(FieldTranslationTab.IDENTIFIER);
  }
}
