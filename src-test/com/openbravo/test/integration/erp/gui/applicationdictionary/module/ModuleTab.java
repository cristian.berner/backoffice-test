/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.applicationdictionary.module;

import com.openbravo.test.integration.erp.data.applicationdictionary.module.ModuleData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Module tab.
 *
 * @author elopio
 *
 */
public class ModuleTab extends GeneratedTab<ModuleData> {

  /** The tab title. */
  static final String TITLE = "Module";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "F53E35A11C564F20BE4082A7B8CFF6B7";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor.
   *
   */
  public ModuleTab() {
    super(TITLE, IDENTIFIER);
    // TOOD add children
  }

  public void assertErrorMessage(String errorMessage) {
    standardView.assertErrorMessage(errorMessage);
  }

}
