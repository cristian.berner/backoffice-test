package com.openbravo.test.integration.erp.gui.applicationdictionary.processdefinition;

import com.openbravo.test.integration.erp.data.applicationdictionary.processdefinition.ParameterData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

public class ParameterTab extends GeneratedTab<ParameterData> {

  /** The tab title. */
  static final String TITLE = "Parameter";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "FF80818132D8C36D0132D8C4936F0006";
  /** The tab level. */
  private static final int LEVEL = 1;

  public ParameterTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
