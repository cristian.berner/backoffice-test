/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartner.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Business Partner tab.
 *
 * @author elopio
 *
 */
public class BusinessPartnerTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Business Partner";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Business Partner tab. */
  public static final String TAB = "tabname220";

  /** Number of the name column. */
  public static final int COLUMN_NAME = 2;

  /** Identifier of the search key text field. */
  private static final String TEXT_FIELD_SEARCH_KEY = "Value";
  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the business partner category combo box. */
  private static final String COMBO_BOX_BUSINESS_PARTNER_CATEGORY = "reportC_BP_Group_ID_S";

  /**
   * Class constructor.
   *
   */
  public BusinessPartnerTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new CustomerTab());
    addSubTab(new VendorCreditorTab());
    addSubTab(new LocationAddressTab());
  }

  /**
   * Create a new Business Partner.
   *
   * @param data
   *          The data of the Business Partner that will be created.
   */
  public void create(BusinessPartnerData data) {
    clickCreateNewRecord();
    if (data.getDataField("searchKey") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SEARCH_KEY)
          .sendKeys(data.getDataField("searchKey").toString());
    }
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("businessPartnerCategory") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_BUSINESS_PARTNER_CATEGORY))
          .selectByVisibleText(data.getDataField("businessPartnerCategory").toString());
    }

    clickSave();
  }

}
