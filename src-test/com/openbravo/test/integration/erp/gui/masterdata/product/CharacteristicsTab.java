/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Jonathan Bueno <jonathan.bueno@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.CharacteristicsData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

public class CharacteristicsTab extends GeneratedTab<CharacteristicsData> {

  /** The tab title. */
  static final String TITLE = "Characteristics";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "769BB4BF6B0B4C39AD28E9A00D260F33";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor
   *
   */
  public CharacteristicsTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new ProductCharacteristicConfigurationTab(this));
  }

}
