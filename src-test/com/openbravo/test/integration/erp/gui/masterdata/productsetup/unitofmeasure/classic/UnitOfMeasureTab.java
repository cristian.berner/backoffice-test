/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.classic;

import com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure.UnitOfMeasureData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Unit of Measure tab.
 *
 * @author elopio
 *
 */
public class UnitOfMeasureTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Unit of Measure";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the tab. */
  public static final String TAB = "tabname133";

  /** Identifier of the EDI code text field */
  private static final String TEXT_FIELD_EDI_CODE = "X12DE355";
  /** Identifier of the symbol text field */
  private static final String TEXT_FIELD_SYMBOL = "UOMSymbol";
  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the standard precision text field */
  private static final String TEXT_FIELD_STANDARD_PRECISION = "StdPrecision";
  /** Identifier of the costing precision text field */
  private static final String TEXT_FIELD_COSTING_PRECISION = "CostingPrecision";

  /**
   * Class constructor
   *
   */
  public UnitOfMeasureTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a Unit of Measure.
   *
   * @param data
   *          The data of the Unit of Measure that will be created.
   */
  public void create(UnitOfMeasureData data) {
    clickCreateNewRecord();
    if (data.getDataField("eDICode") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_EDI_CODE)
          .sendKeys(data.getDataField("eDICode").toString());
    }
    if (data.getDataField("symbol") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_SYMBOL)
          .sendKeys(data.getDataField("symbol").toString());
    }
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("standardPrecision") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_STANDARD_PRECISION)
          .sendKeys(data.getDataField("standardPrecision").toString());
    }
    if (data.getDataField("costingPrecision") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_COSTING_PRECISION)
          .sendKeys(data.getDataField("costingPrecision").toString());
    }

    clickSave();
  }
}
