package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verifies actions on OpenbravoERP Product Characteristic window.
 *
 * @author jbueno
 *
 */
public class ProductCharacteristicWindow extends StandardWindow {
  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Product Characteristic";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.PRODUCT_SETUP, Menu.PRODUCT_CHARACTERISTIC };

  /**
   * Class constructor.
   */
  public ProductCharacteristicWindow() {
    super(TITLE, MENU_PATH);
  }

  @Override
  public void load() {
    ProductCharacteristicTab productTab = new ProductCharacteristicTab();
    addTopLevelTab(productTab);
    super.load();
  }

  /**
   * Select and return the Product Characteristic tab.
   *
   * @return the Product Characteristic tab.
   */
  public ProductCharacteristicTab selectProductCharacteristicTab() {
    return (ProductCharacteristicTab) selectTab(ProductCharacteristicTab.IDENTIFIER);
  }

  /**
   * Select and return the Product Characteristic Value tab.
   *
   * @return the Product Characteristic Valuetab.
   */
  public ProductCharacteristicValueTab selectProductCharacteristicValueTab() {
    return (ProductCharacteristicValueTab) selectTab(ProductCharacteristicValueTab.IDENTIFIER);
  }

}
