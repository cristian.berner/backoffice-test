/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartnersetup.invoiceschedule.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.invoiceschedule.InvoiceScheduleData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Invoice Schedule tab.
 *
 * @author elopio
 *
 */
public class InvoiceScheduleTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Invoice Schedule";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Header tab. */
  public static final String TAB = "tabname193";

  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the invoice frecuency combo box */
  private static final String COMBO_BOX_INVOICE_FREQUENCY = "reportInvoiceFrequency_S";
  /** Identifier of the day of the mont text field */
  private static final String TEXT_FIELD_DAY_OF_THE_MONTH = "InvoiceDay";
  /** Identifier of the invoice cut-off day text field */
  private static final String TEXT_FIELD_INVOICE_CUTOFF_DAY = "InvoiceDayCutoff";

  /**
   * Class constructor
   *
   */
  public InvoiceScheduleTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Invoice Schedule.
   *
   * @param data
   *          The data of the Invoice Schedule that will be created.
   */
  public void create(InvoiceScheduleData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("invoiceFrequency") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_INVOICE_FREQUENCY))
          .selectByVisibleText(data.getDataField("invoiceFrequency").toString());
    }
    if (data.getDataField("dayOfTheMonth") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_DAY_OF_THE_MONTH)
          .sendKeys(data.getDataField("dayOfTheMonth").toString());
    }
    if (data.getDataField("invoiceCutOffDay") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_INVOICE_CUTOFF_DAY)
          .sendKeys(data.getDataField("invoiceCutOffDay").toString());
    }
    clickSave();
  }

}
