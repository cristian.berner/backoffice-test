/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.product.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Price tab.
 *
 * @author elopio
 *
 */
public class PriceTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Price";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the Price tab. */
  public static final String TAB = "tabname183";

  /** Identifier of the price list version combo box. */
  private static final String COMBO_BOX_PRICE_LIST_VERSION = "reportM_PriceList_Version_ID_S";
  /** Identifier of the list price text field. */
  private static final String TEXT_FIELD_LIST_PRICE = "PriceList";
  /** Identifier of the standard price text field. */
  private static final String TEXT_FIELD_STANDARD_PRICE = "PriceStd";
  /** Identifier of the limit price text field. */
  @SuppressWarnings("unused")
  private static final String TEXT_FIELD_LIMIT_PRICE = "PriceLimit";

  /**
   * Class constructor
   *
   */
  public PriceTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a Price.
   *
   * @param data
   *          The data of the Price that will be created.
   */
  public void create(PriceData data) {
    clickCreateNewRecord();
    if (data.getDataField("priceListVersion") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PRICE_LIST_VERSION))
          .selectByVisibleText(data.getDataField("priceListVersion").toString());
    }
    if (data.getDataField("listPrice") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_LIST_PRICE)
          .sendKeys(data.getDataField("listPrice").toString());
    }
    if (data.getDataField("standardPrice") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_STANDARD_PRICE)
          .sendKeys(data.getDataField("standardPrice").toString());
    }
    clickSave();
  }

}
