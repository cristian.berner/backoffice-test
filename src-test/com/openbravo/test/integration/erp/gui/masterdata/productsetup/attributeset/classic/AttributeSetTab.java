/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.productsetup.attributeset.classic;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.data.masterdata.productsetup.attributeset.AttributeSetData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Attribute Set tab.
 *
 * @author elopio
 *
 */
public class AttributeSetTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Attribute Set";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the tab. */
  public static final String TAB = "tabname461";

  /** Identifier of the name text field */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the is lot check box */
  private static final String CHECK_BOX_IS_LOT = "IsLot";
  /** Identifier of the is serial number check box */
  private static final String CHECK_BOX_IS_SERIAL_NUMBER = "IsSerNo";

  /**
   * Class constructor
   *
   */
  public AttributeSetTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create an attribute set
   *
   * @param data
   *          The data of the Price List that will be created.
   */
  public void create(AttributeSetData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    if (data.getDataField("lot") != null) {
      WebElement checkBoxIsLot = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_IS_LOT);
      if (!data.getDataField("lot").equals(checkBoxIsLot.isSelected())) {
        checkBoxIsLot.click();
      }
    }
    if (data.getDataField("serialNo") != null) {
      WebElement checkBoxIsSerialNumber = SeleniumSingleton.INSTANCE
          .findElementById(CHECK_BOX_IS_SERIAL_NUMBER);
      if (!data.getDataField("serialNo").equals(checkBoxIsSerialNumber.isSelected())) {
        checkBoxIsSerialNumber.click();
      }
    }

    clickSave();
  }
}
