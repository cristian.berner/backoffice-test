/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.businesspartner;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Business Partner standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class BusinessPartnerWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Business Partner";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.BUSINESS_PARTNER };

  /**
   * Class constructor.
   *
   */
  public BusinessPartnerWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    BusinessPartnerTab businessPartnerTab = new BusinessPartnerTab();
    addTopLevelTab(businessPartnerTab);
    super.load();
  }

  /**
   * Select and return the Business Partner tab.
   *
   * @return the Business Partner tab.
   */
  public BusinessPartnerTab selectBusinessPartnerTab() {
    return (BusinessPartnerTab) selectTab(BusinessPartnerTab.IDENTIFIER);
  }

  /**
   * Select and return the Customer tab.
   *
   * @return the Customer tab.
   */
  public CustomerTab selectCustomerTab() {
    return (CustomerTab) selectTab(CustomerTab.IDENTIFIER);
  }

  /**
   * Select and return the VendorCreditor tab.
   *
   * @return the VendorCreditor tab.
   */
  public VendorCreditorTab selectVendorCreditorTab() {
    return (VendorCreditorTab) selectTab(VendorCreditorTab.IDENTIFIER);
  }

  /**
   * Select and return the Location/Address tab.
   *
   * @return the Location/Address tab.
   */
  public LocationAddressTab selectLocationAddressTab() {
    return (LocationAddressTab) selectTab(LocationAddressTab.IDENTIFIER);
  }

  /**
   * Select and return the Customer Accounting tab.
   *
   * @return the Customer Accounting tab.
   */
  public CustomerAccountingTab selectCustomerAccountingTab() {
    return (CustomerAccountingTab) selectTab(CustomerAccountingTab.IDENTIFIER);
  }

  /**
   * Select and return the Customer Accounting tab.
   *
   * @return the Customer Accounting tab.
   */
  public VendorAccountingTab selectVendorAccountingTab() {
    return (VendorAccountingTab) selectTab(VendorAccountingTab.IDENTIFIER);
  }

}
