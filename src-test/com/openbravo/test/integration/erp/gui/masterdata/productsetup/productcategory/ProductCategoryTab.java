/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.productsetup.productcategory;

import com.openbravo.test.integration.erp.data.masterdata.productsetup.productcategory.ProductCategoryData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Product Category tab.
 *
 * @author elopio
 *
 */
public class ProductCategoryTab extends GeneratedTab<ProductCategoryData> {

  /** The tab title. */
  static final String TITLE = "Product Category";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "189";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor
   *
   */
  public ProductCategoryTab() {
    super(TITLE, IDENTIFIER);
  }

}
