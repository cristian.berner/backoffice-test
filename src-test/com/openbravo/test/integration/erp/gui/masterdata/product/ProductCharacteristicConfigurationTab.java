package com.openbravo.test.integration.erp.gui.masterdata.product;

import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicValueData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Product Characteristic Configuration tab.
 *
 * @author jbueno
 *
 */
public class ProductCharacteristicConfigurationTab
    extends GeneratedTab<ProductCharacteristicValueData> {

  /** The tab title. */
  public static final String TITLE = "Characteristic Configuration";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "77B382924865466E8333415AAA1263CC";
  /** The tab level. */
  private static final int LEVEL = 2;

  public ProductCharacteristicConfigurationTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
