/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.productsetup.attributeset.classic;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicWindowView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Attribute Set classic window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class AttributeSetWindow extends ClassicWindowView {

  /** The window title. */
  private static final String TITLE = "Attribute Set";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.PRODUCT_SETUP, Menu.ATTRIBUTE_SET };

  /**
   * Class constructor.
   *
   */
  public AttributeSetWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    super.load();
    final AttributeSetTab attributeSetTab = new AttributeSetTab();
    attributeSetTab.waitUntilTabIsVisible();
    addTab(attributeSetTab);
    setCurrentTab(attributeSetTab);
  }

  /**
   * Select and return the Attribute Set tab.
   *
   * @return the Attribute Set tab.
   */
  public AttributeSetTab selectAttributeSetTab() {
    return (AttributeSetTab) selectTab(AttributeSetTab.TITLE);
  }

}
