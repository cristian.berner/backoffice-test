/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist;

import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Price List Version tab.
 *
 * @author elopio
 *
 */
public class PriceListVersionTab extends GeneratedTab<PriceListVersionData> {

  /** The tab title. */
  static final String TITLE = "Price List Version";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "238";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor
   *
   * @param parentTab
   *          The parent of the tab.
   *
   */
  public PriceListVersionTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new ProductPriceTab(this));
  }

  /**
   * Process the transaction
   */
  public void process() throws OpenbravoERPTestException {
    final Button buttonCreatePriceList = new Button(
        TestRegistry.getObjectString("org.openbravo.client.application.toolbar.button.create.238"));
    buttonCreatePriceList.click();

    final OBClassicPopUp createPeriods = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PriceList/PriceListVersion_Edition.html"));
    createPeriods.selectPopUpWithOKButton();
    createPeriods.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();

    // FIXME: This message is failing very often for Discount Price List
    // // TODO: Check why sometimes is not checking properly the message.
    Sleep.trySleep();
    //
    // waitUntilMessageVisible();
  }
}
