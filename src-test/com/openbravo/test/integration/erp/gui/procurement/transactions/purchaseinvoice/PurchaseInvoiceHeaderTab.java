/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.outgoingshipment.ShipmentOutData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Sales Invoice Header tab.
 *
 * @author elopio
 *
 */
public class PurchaseInvoiceHeaderTab extends GeneratedTab<PurchaseInvoiceHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "290";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.290";
  /** Registry key of the create lines from order button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_ORDER = "org.openbravo.client.application.toolbar.button.createLinesFromOrder.290";
  /** Registry key of the create lines from Shipment/Receipt button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_RECEIPT = "org.openbravo.client.application.toolbar.button.createLinesFromShipment.290";
  /** Registry key of the Create Lines From pick and edit */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";
  /** Registry key of the post button. */
  private static final String REGISTRY_KEY_BUTTON_POST = "org.openbravo.client.application.toolbar.button.posted.290";
  /** Registry key of the Add Payment button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT = "org.openbravo.client.application.toolbar.button.aPRMAddpayment.290";
  /** Registry key of the Void button. */
  private static final String REGISTRY_KEY_BUTTON_VOID = "org.openbravo.client.application.toolbar.button.aPRMProcessinvoice.290";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String DELETE_CHECKBOX = "inpEliminar";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The create lines from Order button. */
  private static Button buttonCreateLinesFromOrder;
  /** The create lines from Shipment/Receipt button. */
  private static Button buttonCreateLinesFromReceipt;
  /** The add payment button. */
  private static Button buttonAddPaymentInOut;
  /** The add void button. */
  private static Button buttonVoid;

  /**
   * Class constructor.
   *
   */
  public PurchaseInvoiceHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new PurchaseInvoiceLinesTab(this));
    addChildTab(new PaymentOutPlanTab(this));
    addChildTab(new ExchangeRatesTab(this));
    addChildTab(new BasicDiscountsTab(this));
    addChildTab(new TaxTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCreateLinesFromOrder = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_ORDER));
    buttonCreateLinesFromReceipt = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_RECEIPT));
    buttonVoid = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_VOID));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process purchase invoice header with action '{}'.", action);
    // XXX workaround issue https://issues.openbravo.com/view.php?id=17409
    buttonComplete.smartClientClick();
    // TODO L1: It seems the following static sleep is required. Check in a future to delete that
    // sleep
    Sleep.trySleep();
    final RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./PurchaseInvoice/Header_Edition.html");
    popUp.process(action);
    // TODO L1: It seems the following static sleep is required. Check in a future to delete that
    // sleep
    Sleep.trySleep();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep(7500);
    buttonAddPaymentInOut = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
  }

  /**
   * Void the invoice.
   *
   */
  public void close() {
    buttonVoid.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/ProcessInvoice.html");
    popUp.process("Void");
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Create lines from order.
   *
   * @param order
   *          The order identifier.
   */
  public void createLinesFrom(String order) {
    logger.debug("Creating lines from order '{}'.", order);
    buttonCreateLinesFromOrder.click();
    PickAndExecuteWindow<SalesOrderHeaderData> popup = new PickAndExecuteWindow<SalesOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoOrderDateGrandTotal = order.split(" - ");
    popup.filter(new SalesOrderHeaderData.Builder().documentNo(documentNoOrderDateGrandTotal[0])
        .orderDate(documentNoOrderDateGrandTotal[1])
        .grandTotalAmount(documentNoOrderDateGrandTotal[2])
        .build());
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   */
  public void createLinesFromShipment(String shipment) {
    logger.debug("Creating lines from receipt '{}'.", shipment);
    buttonCreateLinesFromReceipt.click();
    PickAndExecuteWindow<ShipmentOutData> popup = new PickAndExecuteWindow<ShipmentOutData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoMovementDateBP = shipment.split(" - ");
    popup.filter(new ShipmentOutData.Builder().documentNo(documentNoMovementDateBP[0])
        .movementDate(documentNoMovementDateBP[1])
        .build());
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   * @param lineNo
   *          The shipment line No
   */
  public void createLinesFromShipment(String shipment, int lineNo) {
    logger.debug("Creating lines from shipment '{}' and line no '{}'.", shipment,
        String.valueOf(lineNo));
    buttonCreateLinesFromReceipt.click();
    PickAndExecuteWindow<ShipmentOutData> popup = new PickAndExecuteWindow<ShipmentOutData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoMovementDateBP = shipment.split(" - ");
    ShipmentOutData shipmentData = new ShipmentOutData.Builder()
        .documentNo(documentNoMovementDateBP[0])
        .movementDate(documentNoMovementDateBP[1])
        .build();
    shipmentData.addDataField("lineNo", String.valueOf(lineNo * 10));
    popup.filter(shipmentData);
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Create lines from order.
   *
   * @param order
   *          The order identifier.
   * @param lineNo
   *          The order line No
   */
  public void createLinesFrom(String order, int lineNo) {
    logger.debug("Creating lines from order '{}' and line no '{}'.", order, String.valueOf(lineNo));
    buttonCreateLinesFromOrder.click();
    PickAndExecuteWindow<SalesOrderHeaderData> popup = new PickAndExecuteWindow<SalesOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoOrderDateGrandTotal = order.split(" - ");
    SalesOrderHeaderData orderData = new SalesOrderHeaderData.Builder()
        .documentNo(documentNoOrderDateGrandTotal[0])
        .orderDate(documentNoOrderDateGrandTotal[1])
        .grandTotalAmount(documentNoOrderDateGrandTotal[2])
        .build();
    orderData.addDataField("lineNo", String.valueOf(lineNo * 10));
    popup.filter(orderData);
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Check whether the purchase invoice is posted or not.
   *
   * @return boolean with value as true if its text is equals to "Unpost"
   */
  public boolean isPosted() {
    logger.debug("Checking whether the purchase invoice is posted or not.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    return (buttonPost.getText().equals("Unpost"));
  }

  /**
   * Post the sales invoice.
   */
  public void post() {
    logger.debug("Posting the invoice.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    Sleep.smartWaitButtonVisible(REGISTRY_KEY_BUTTON_POST, 800);
    buttonPost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PurchaseInvoice/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // TODO L: The following "waitForDataToLoad" is required after updating to FF 60esr.
    // post.assertJournalLinesCount was loading too slow
    waitForDataToLoad();
    // XXX: <performance issues> Waiting until the all Journal Entries window are open to prevent
    // overlap and/or interruption by the actions following
    Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver driver) {
        String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
        String strComp = "Journal Entries Report";
        String strComp2 = " - Main";
        String strComp3 = " - USA U...";
        String strComp4 = " - F&B I...";
        if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
          logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
          Sleep.trySleep(5000);
        }
        return strOfJsScript.toLowerCase().equals(strComp.toLowerCase())
            || strOfJsScript.toLowerCase().contains(strComp3.toLowerCase())
            || strOfJsScript.toLowerCase().contains(strComp4.toLowerCase());
      }
    });
  }

  /**
   * UnPost the sales invoice.
   */
  public void unpost() {
    logger.debug("Unposting the invoice.");
    Button buttonUnpost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    buttonUnpost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PurchaseInvoice/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.verifyElementsAreVisible(new String[] { DELETE_CHECKBOX });
    if (!SeleniumSingleton.INSTANCE.findElementByName(DELETE_CHECKBOX).isSelected()) {
      SeleniumSingleton.INSTANCE.findElementByName(DELETE_CHECKBOX).click();
    }
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.trySleep();
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    logger.debug("Opening add payment process popup");
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess();
    addPaymentProcess.assertData(data);
    addPaymentProcess.close();
  }

  /**
   * Add a payment and process it.
   *
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String action) {
    logger.debug("Adding payment.");
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.process(action);
  }

  public void addPayment(String action, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    logger.debug("Adding payment.");
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process(action);
  }

  /**
   * Open Add Payment popup
   *
   * @return The Add Payment PopUp
   */
  public AddPaymentProcess openAddPayment() {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    return addPaymentProcess;
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param visibleDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose visibility have to be asserted.
   */
  public void assertDisplayLogicVisibleAddPaymentOut(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.debug("Asserting visible display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param enabledDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose enable status have to be asserted.
   */
  public void assertDisplayLogicEnabledAddPaymentOut(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.debug("Asserting enabled display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param expandedDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose expansion status have to be asserted.
   */
  public void assertDisplayLogicExpandedAddPaymentOut(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.debug("Asserting expanded display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT);
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully2() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT2);
  }
}
