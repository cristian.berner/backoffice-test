/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.landedcost;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostHeaderData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;

/**
 * Executes and verifies actions on OpenbravoERP Sales Invoice Header tab.
 *
 * @author elopio
 *
 */
public class LandedCostHeaderTab extends GeneratedTab<LandedCostHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "F25CBC61CDD64F5E8A6FDC41C6E23C96";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_PROCESS = "org.openbravo.client.application.toolbar.button.process.F25CBC61CDD64F5E8A6FDC41C6E23C96";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonProcess;

  /**
   * Class constructor.
   *
   */
  public LandedCostHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new LandedCostCostTab(this));
    addChildTab(new LandedCostReceiptTab(this));
    buttonProcess = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PROCESS));
  }

  /**
   * Process the landed cost.
   */
  public void process() {
    logger.debug("Process landed cost header.");
    // XXX workaround issue https://issues.openbravo.com/view.php?id=17409
    buttonProcess.smartClientClick();
    final LandedCostProcess popUp = new LandedCostProcess(this);
    popUp.process();
  }
}
