/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <plu@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.WarnDialog;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Purchase Order Header tab.
 *
 * @author plujan
 *
 */
public class PurchaseOrderHeaderTab extends GeneratedTab<PurchaseOrderHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Purchase Order";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "294";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Registry keys. */
  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.294";
  /** Registry key of the Add Payment button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT = "org.openbravo.client.application.toolbar.button.aPRMAddPayment.294";
  /** Registry key of the request process pop up. */
  private static final String REGISTRY_KEY_POP_UP_REQUEST_PROCESS = "org.openbravo.classicpopup./PurchaseOrder/Header_Edition.html";
  /** Registry key of the copy record button. */
  private static final String REGISTRY_KEY_BUTTON_CLONE = "org.openbravo.client.application.toolbar.button.clone.294";
  /** Registry key of the Copy Lines button. */
  private static final String REGISTRY_KEY_BUTTON_COPY_LINES = "org.openbravo.client.application.toolbar.button.copyFrom.294";
  /** Registry key of the copy from orders button. */
  private static final String REGISTRY_KEY_BUTTON_COPY_FROM_ORDERS = "org.openbravo.client.application.toolbar.button.copyFromPO.294";
  /** Registry key of the copy from orders button pop up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The add payment button. */
  private static Button buttonAddPaymentInOut;
  /** The copy record button */
  private static Button buttonCopyRecord;
  /** The copy lines button. */
  private static Button buttonCopyLines;
  /** The copy record button */
  private static Button buttonCopyFromOrders;

  /**
   * Class constructor
   *
   */
  public PurchaseOrderHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new PurchaseOrderLinesTab(this));
    addChildTab(new PaymentOutPlanTab(this));
    addChildTab(new TaxTab(this));
    addChildTab(new BasicDiscountsTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCopyRecord = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CLONE));
    buttonCopyFromOrders = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COPY_FROM_ORDERS));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process purchase order header with action '{}'.", action);
    buttonComplete.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(REGISTRY_KEY_POP_UP_REQUEST_PROCESS);
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Book the order.
   *
   */
  public void book() {
    process("Book");
  }

  public void assertPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT);
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   *          The verification data.
   */
  public void addPayment(String account, String payment, String action,
      AddPaymentPopUpData addPaymentPopUpData) {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    if (account != null) {
      addPaymentProcess.setParameterValue("fin_financial_account_id", account);
    }
    if (payment != null) {
      addPaymentProcess.editOrderInvoiceRecord(0,
          new SelectedPaymentData.Builder().amount(payment).build());
      addPaymentProcess.getField("reference_no").focus();
    }
    if (addPaymentPopUpData != null) {
      addPaymentProcess.assertData(addPaymentPopUpData);
    }
    addPaymentProcess.process(action);

    waitUntilMessageVisible();
  }

  /**
   * Open Add Payment popup
   *
   * @return The Add Payment PopUp
   */
  public AddPaymentProcess openAddPayment() {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    return addPaymentProcess;
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param visibleDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose visibility have to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.debug("Asserting visible display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param enabledDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose enable status have to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.debug("Asserting enabled display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param expandedDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose expansion status have to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.debug("Asserting expanded display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  public void copyRecord() {
    buttonCopyRecord.click();

    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickYes();
  }

  public void copyLines(boolean isUomManagementEnabled) {
    if (buttonCopyLines == null) {
      buttonCopyLines = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COPY_LINES));
    }
    buttonCopyLines.click();
    CopyLinesPopUp popup = new CopyLinesPopUp();
    popup.copyLines(isUomManagementEnabled);

  }

  public PickAndExecuteWindow<PurchaseOrderHeaderData> copyFromOrders() {
    buttonCopyFromOrders.click();
    PickAndExecuteWindow<PurchaseOrderHeaderData> popup = new PickAndExecuteWindow<PurchaseOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popup;
  }
}
