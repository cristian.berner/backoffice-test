package com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseorder;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

public class CopyLinesPopUp extends OBClassicPopUp {

  /** Identifier of the lines check box. */
  private static final String CHECK_BOX_LINES = "inpRownumId";

  public CopyLinesPopUp() {
    super(TestRegistry
        .getObjectString("org.openbravo.classicpopup./ad_actionButton/CopyFromOrder.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("process");
  }

  public void copyLines(boolean isUomManagementEnabled) {
    selectPopUpWithOKButton();

    List<WebElement> element = SeleniumSingleton.INSTANCE.findElementsByName(CHECK_BOX_LINES);
    if (element.size() > 0) {
      element.get(0).click();
    }
    if (isUomManagementEnabled) {
      element = SeleniumSingleton.INSTANCE.findElementsByName("inpaumquantity".concat("1"));
    } else {
      element = SeleniumSingleton.INSTANCE.findElementsByName("inpquantity".concat("1"));
    }
    if (element.size() > 0) {
      element.get(0).sendKeys("10");
    }
    clickOK();
  }

}
