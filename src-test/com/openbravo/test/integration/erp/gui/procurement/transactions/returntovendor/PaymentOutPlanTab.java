package com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutPlanData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

public class PaymentOutPlanTab extends GeneratedTab<PaymentOutPlanData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Payment Out Plan";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "3D342267CAAE4CA5B191082E20F87660";
  /** The tab level. */
  private static final int LEVEL = 1;

  public PaymentOutPlanTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    logger.debug("Creating Return to Vendor Payment Out Plan Tab");
    addChildTab(new PaymentOutDetailsTab(this));
  }

}
