/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.gui.procurement.transactions.returntovendor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutDetailsData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 *
 * @author nonofce
 *
 */
public class PaymentOutDetailsTab extends GeneratedTab<PaymentOutDetailsData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Payment Out Details";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "FA619778CCD24476BD89873D139824BB";
  /** The tab level. */
  private static final int LEVEL = 2;

  public PaymentOutDetailsTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    logger.debug("Creating Return to Vendor Payment Out Detail Tab");
  }
}
