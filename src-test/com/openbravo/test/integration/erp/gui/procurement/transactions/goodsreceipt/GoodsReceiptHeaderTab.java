/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessInvoicePopUp;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Goods Receipt Header tab.
 *
 * @author plujan
 *
 */
public class GoodsReceiptHeaderTab extends GeneratedTab<GoodsReceiptHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Goods Receipt";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "296";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.processGoodsJava.296";
  /** Registry key of the create lines from button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM = "org.openbravo.client.application.toolbar.button.createLinesFrom.296";
  /** Registry key of the generate invoice from receipt button. */
  private static final String REGISTRY_KEY_BUTTON_GENERATE_INVOICE = "org.openbravo.client.application.toolbar.button.generateTo.296";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The create lines from button. */
  private static Button buttonCreateLinesFrom;
  /** The generate invoice from receipt button. */
  private static Button buttonGenerateInvoice;

  /**
   * Class constructor
   *
   */
  public GoodsReceiptHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new GoodsReceiptLinesTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCreateLinesFrom = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM));
    buttonGenerateInvoice = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_GENERATE_INVOICE));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process goods receipt header with action '{}'.", action);
    buttonComplete.click();
    RequestProcessPopUp popUp = new RequestProcessInvoicePopUp(
        "org.openbravo.classicpopup./org.openbravo.erpCommon.ad_actionButton/ProcessGoods.html");
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep();
  }

  /**
   * Complete the order.
   *
   */
  public void close() {
    process("Void");
  }

  /**
   * Creates lines from an order.
   *
   * @param warehouseAlias
   *          The warehouse alias.
   * @param order
   *          The order identifier. It is a string of the form {document number} - {order date} -
   *          {total amount}.
   * @param attribute
   *          The line attribute.
   *
   */
  public void createLinesFrom(String warehouseAlias, String order, String attribute) {
    this.createLinesFrom(warehouseAlias, order, attribute, 0);
  }

  /**
   * Creates lines from an order choosing a product with no attribute set
   *
   * @param warehouseAlias
   *          The warehouse alias.
   * @param order
   *          The order identifier. It is a string of the form {document number} - {order date} -
   *          {total amount}.
   *
   */
  public void createLinesFrom(String warehouseAlias, String order) {
    this.createLinesFrom(warehouseAlias, order, "", 0);
  }

  /**
   * Create goods receipt lines from order.
   *
   * @param warehouseAlias
   *          The alias of the warehouse.
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param attribute
   *          The line attribute.
   * @param lineNo
   *          The order line number to select or 0 to select all
   */
  public void createLinesFrom(String warehouseAlias, String order, String attribute, int lineNo) {
    this.createLinesFrom(new CreateLinesFromData.Builder().warehouse(warehouseAlias)
        .purchaseOrder(order)
        .attribute(attribute)
        .lineNumber(lineNo)
        .build());
  }

  /**
   * Create goods receipt lines from order.
   *
   * @param linesData
   *          The data to create the lines from
   */
  public void createLinesFrom(CreateLinesFromData linesData) {
    logger.debug("Creating lines from order '{}'.", linesData.getDataField("purchaseOrder"));
    buttonCreateLinesFrom.click();
    CreateLinesFromPopUp popUp = new CreateLinesFromPopUp();
    popUp.createLinesFrom(linesData);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
  }

  /**
   * Generate Invoice from Receipt choosing a price list version
   *
   * @param priceListVersion
   *          The price list version.
   */
  public void generateInvoice(String priceListVersion) {
    logger.debug("Generate Invoice from Receipt with price list version '{}'.", priceListVersion);
    buttonGenerateInvoice.click();
    RequestProcessPopUp popUp = new RequestProcessInvoicePopUp(
        "org.openbravo.classicpopup./GoodsReceipt/Header_Edition.html");
    popUp.processReceipt(priceListVersion);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }
}
