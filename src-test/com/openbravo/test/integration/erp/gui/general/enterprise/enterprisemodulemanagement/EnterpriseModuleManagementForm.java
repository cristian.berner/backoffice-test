/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.enterprisemodulemanagement;

import static org.junit.Assert.assertFalse;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.ModuleReferenceData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.enterprisemodulemanagement.EnterpriseModuleManagementData;
import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicFormView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Enterprise Module Management screen.
 *
 * @author elopio
 *
 */
public class EnterpriseModuleManagementForm extends ClassicFormView {

  /** The window title. */
  private static final String TITLE = "Enterprise module management";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.ENTERPRISE,
      Menu.ENTERPRISE_MODULE_MANAGEMENT };

  /** Identifier of the Organization combo box. */
  private static final String COMBO_BOX_ORGANIZATION = "paramOrganization";
  /** Formatting string used to get the label of the module. */
  private static final String FORMAT_MODULE_LABEL = "%s - %s - %s";
  /** Formatting string used to locate the check box of a module. */
  private static final String FORMAT_CHECKBOX_MODULE = "(//div[@id='name' and text()='%s']/../..)//input[@type='checkbox']";
  /** Identifier of the ok button */
  protected static final String BUTTON_OK = "buttonOK";

  /**
   * Class constructor.
   *
   */
  public EnterpriseModuleManagementForm() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Get the module label as displayed on this tab.
   *
   * @param moduleReferenceData
   *          The data of the module.
   */
  private String getModuleLabel(ModuleReferenceData moduleReferenceData) {
    return String.format(FORMAT_MODULE_LABEL, moduleReferenceData.getName(),
        moduleReferenceData.getVersion(), moduleReferenceData.getLanguage());
  }

  /**
   * Click the check box next to a module.
   *
   * @param moduleReferenceData
   *          The data of the module.
   */
  private void selectModule(ModuleReferenceData moduleReferenceData) {
    String moduleLabel = getModuleLabel(moduleReferenceData);
    final String checkboxLocator = String.format(FORMAT_CHECKBOX_MODULE, moduleLabel);
    WebElement checkBox = SeleniumSingleton.INSTANCE.findElementByXPath(checkboxLocator);
    assertFalse("Module is already selected.", checkBox.isSelected());
    // XXX workaround because the click function doesn't work with these checkboxes.
    String checkBoxId = checkBox.getAttribute(Attribute.ID.getName());
    SeleniumSingleton.INSTANCE
        .executeScript(String.format("document.getElementById('%s').checked = true", checkBoxId));
  }

  /**
   * Apply modules to an organization.
   *
   * @param enterpriseModuleManagementData
   *          The enterprise module management data.
   */
  public void applyModules(EnterpriseModuleManagementData enterpriseModuleManagementData) {
    // select the organization.
    if (enterpriseModuleManagementData.getOrganization() != null) {
      Select currentOrganizationComboBox = new Select(
          SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION));
      String currentOrganization = currentOrganizationComboBox.getFirstSelectedOption().getText();
      if (!currentOrganization.equals(enterpriseModuleManagementData.getOrganization())) {
        currentOrganizationComboBox
            .selectByVisibleText(enterpriseModuleManagementData.getOrganization());
      }
    }
    // select the modules.
    if (enterpriseModuleManagementData.getReferenceDataList() != null) {
      for (ModuleReferenceData moduleReferenceData : enterpriseModuleManagementData
          .getReferenceDataList()) {
        selectModule(moduleReferenceData);
      }
    }
    clickOK();
  }

  /**
   * Click OK button and wait for the page to load.
   */
  public void clickOK() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
  }
}
