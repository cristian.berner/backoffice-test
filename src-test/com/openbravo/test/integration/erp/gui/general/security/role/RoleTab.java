/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.role;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Role tab.
 *
 * @author elopio
 *
 */
public class RoleTab extends GeneratedTab<RoleData> {

  /** The tab title. */
  static final String TITLE = "Role";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "119";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor
   *
   */
  public RoleTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new OrgAccessTab(this));
    addChildTab(new UserAssignmentTab(this));
    addChildTab(new WindowAccessTab(this));
    addChildTab(new FormAccessTab(this));
    addChildTab(new ProcessAccessTab(this));
  }

  // /**
  // * Grant access to a role.
  // *
  // * @param grantAccessData
  // * The data of the access.
  // */
  // public void grantAccess(GrantAccessPopUpData grantAccessData) {
  // SeleniumSingleton.INSTANCE.click(BUTTON_GRANT_ACCESS);
  // InsertAccess insertAccessPopUp = new InsertAccess();
  // insertAccessPopUp.insertAccess(grantAccessData);
  //
  // SeleniumSingleton.INSTANCE.selectWindow("");
  // }
}
