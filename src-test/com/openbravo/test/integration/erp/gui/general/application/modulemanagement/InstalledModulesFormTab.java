/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import static org.junit.Assert.assertFalse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.erp.gui.tabs.FormTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verify actions on Installed Modules form tab.
 *
 * @author elopio
 *
 */
public class InstalledModulesFormTab extends FormTab {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Installed Modules";

  /** Identifier of the Installed Modules tab. */
  public static final String TAB = "tabDefault";

  /** Locator of the Select All link. */
  private static final String LINK_ALL = "All";

  /** Locator of the Enable link. */
  private static final String LINK_ENABLE = "Enable";

  // TODO see issue 11255 - https://issues.openbravo.com/view.php?id=11255.
  /** Locator of the rebuild now link. */
  private static final String LINK_REBUILD_NOW = "rebuild now";
  /** Identifier of the uninstall button. */
  private static final String BUTTON_UNINSTALL = "buttonUninstall";
  /** Identifier of the Disable Selected button. */
  private static final String BUTTON_DISABLE_SELECTED = "buttonDisable";

  /** Formatting string used to locate the checkbox of a module. */
  // private static final String FORMAT_CHECKBOX_MODULE =
  // "xpath=(//div[@id='name' and text()='%s']/../..)//input[@type='checkbox']"
  // ;
  private static final String FORMAT_CHECKBOX_MODULE = "inpNodes_%s";
  /** Text of the confirmation shown when trying to enable modules. */
  @SuppressWarnings("unused")
  private static final String CONFIRMATION_ENABLE_MODULES = "The module will be enabled. Are you sure you want to continue?";
  /** Text of the confirmation shown when trying to disable modules. */
  @SuppressWarnings("unused")
  private static final String CONFIRMATION_DISABLE_MODULES = "The module(s) will be disabled. Are you sure you want to continue?";

  /**
   * Class constructor.
   *
   */
  public InstalledModulesFormTab() {
    super(TITLE, TAB);
  }

  /**
   * Click the rebuild link.
   */
  public void clickRebuild() {
    SeleniumSingleton.INSTANCE.findElementByLinkText(LINK_REBUILD_NOW).click();
  }

  /**
   * Click the uninstall button.
   */
  public void clickUninstallButton() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_UNINSTALL).click();
  }

  /**
   * Uninstall the selected module.
   */
  public void uninstallModule() {
    clickUninstallButton();
    final Alert confirmation = SeleniumSingleton.INSTANCE.switchTo().alert();
    // TODO Assert the confirmation message contents.
    // assertThat(confirmation.getText(), is(equalTo(CONFIRMATION_DISABLE_MODULES)));
    confirmation.accept();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Click the checkbox next to a module.
   *
   * @param moduleId
   *          The id of the module to select.
   */
  public void selectModule(String moduleId) {
    final String checkboxLocator = String.format(FORMAT_CHECKBOX_MODULE, moduleId);
    final WebElement checkBoxModule = SeleniumSingleton.INSTANCE.findElementById(checkboxLocator);
    assertFalse("Module is already selected.", checkBoxModule.isSelected());
    checkBoxModule.click();
  }

  /**
   * Checks if the module is installed.
   *
   * @param moduleId
   *          The id of the module to select.
   * @return true if the module is installed. Otherwise, false.
   */
  public boolean isModuleInstalled(String moduleId) {
    final String checkboxLocator = String.format(FORMAT_CHECKBOX_MODULE, moduleId);
    try {
      SeleniumSingleton.INSTANCE.findElementById(checkboxLocator);
    } catch (final NoSuchElementException exception) {
      return false;
    }
    return true;
  }

  /**
   * Disable all modules. The core module will not be selected to disable, because it is not
   * possible to disable it.
   */
  public void disableAllModules() {
    Sleep.explicitWaitForElementToBeClickable(
        SeleniumSingleton.INSTANCE.findElementByLinkTextWithFWait(LINK_ALL));
    SeleniumSingleton.INSTANCE.findElementByLinkText(LINK_ALL).click();
    SeleniumSingleton.INSTANCE.executeScript("window.confirm = function(msg) { return true; }");
    try {
      final WebElement buttonDisableSelected = SeleniumSingleton.INSTANCE
          .findElementById(BUTTON_DISABLE_SELECTED);
      buttonDisableSelected.click();
      /*
       * Alert confirmation = SeleniumSingleton.INSTANCE.switchTo().alert();
       * assertThat(confirmation.getText(), is(equalTo(CONFIRMATION_DISABLE_MODULES)));
       * confirmation.accept(); SeleniumSingleton.INSTANCE.switchTo().defaultContent();
       */
    } catch (final NoSuchElementException exception) {
      logger.warn("Disable Selected button was not present.");
      logger.warn(exception);
    }
  }

  /**
   * Enable all disabled modules.
   */
  public void enableAllModules() {
    // XXX findElementsByKinText is not working. After the first element in the list, it gives an
    // exception because the element is not in cache.
    while (true) {
      try {
        final WebElement linkEnable = SeleniumSingleton.INSTANCE.findElementByLinkText(LINK_ENABLE);
        SeleniumSingleton.INSTANCE.executeScript("window.confirm = function(msg) { return true; }");
        linkEnable.click();
        /*
         * final Alert confirmation = SeleniumSingleton.INSTANCE.switchTo().alert();
         * assertThat(confirmation.getText(), is(equalTo(CONFIRMATION_ENABLE_MODULES)));
         * confirmation.accept(); SeleniumSingleton.INSTANCE.switchTo().defaultContent();
         */
        Sleep.trySleep();
      } catch (final NoSuchElementException exception) {
        break;
      }
    }
  }

}
