/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.security.role.classic;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Role tab.
 *
 * @author elopio
 *
 */
public class OrgAccessTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Org Access";
  /** The tab level. */
  private static final int LEVEL = 1;

  /** Identifier of the org access tab */
  public static final String TAB = "tabname351";

  /** Identifier of the organization combo box */
  private static final String COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";

  /**
   * Class constructor
   *
   */
  public OrgAccessTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create an organization access.
   *
   * @param orgAccessData
   *          The organization access data.
   *
   */
  public void create(OrgAccessData orgAccessData) {
    clickCreateNewRecord();
    if (orgAccessData.getDataField("organization") != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_ORGANIZATION))
          .selectByVisibleText(orgAccessData.getDataField("organization").toString());
    }
    clickSave();
    // XXX why is this sleep necessary?
    // Does it work without it on other browsers?
    Sleep.trySleep();
  }
}
