/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions and verifies actions on install module pop up.
 *
 * @author elopio
 *
 */
public class InstallModulePopUp extends OBClassicButtonPopUp {

  /** Identifier of the file text field. */
  private static final String TEXT_FIELD_FILE = "inpFile";
  /** Identifier of the continue button. */
  private static final String BUTTON_CONTINUE = "buttonContinue";
  /** Identifier of the accept option in license agreements radio button. */
  private static final String OPTION_ACCEPT_ALL_LICENSE_AGREEMENTS = "acceptOption";
  /**
   * Format string used to check the message displayed after installing a module.
   */
  private static final String FORMAT_MODULE_SUCCESSFULLY_INSTALLED = "Module successfully installed %s - %s";
  /**
   * Format string used to check if a module is not an update.
   */
  private static final String FORMAT_MODULE_IS_NOT_AN_UPDATE = "%s  %s is not an update to already installed version";

  /**
   * Class constructor.
   */
  public InstallModulePopUp() {
    super();
  }

  /**
   * Select the .obx file to be installed.
   *
   * @param moduleFilePath
   *          The path to the .obx file.
   */
  public void selectModuleFile(String moduleFilePath) {
    selectPopUp(TEXT_FIELD_FILE);
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FILE).sendKeys(moduleFilePath);
    clickOK();
  }

  /**
   * Returns true if the module is an update. It has to be executed after selecting the module to
   * install.
   *
   * @param moduleName
   *          The name of the module that was installed.
   * @param moduleVersion
   *          The version of the installed module.
   * @return true if the module is an update, otherwise false.
   */
  public boolean isAnUpdate(String moduleName, String moduleVersion) {
    final MessageBox messageBox = new MessageBox();
    return !messageBox.isVisible() && !messageBox.isWarning() && !messageBox.getText()
        .startsWith(String.format(FORMAT_MODULE_IS_NOT_AN_UPDATE, moduleName, moduleVersion));
  }

  /**
   * Click OK button and wait for the page to load
   */
  public void clickContinue() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CONTINUE).click();
  }

  /**
   * Confirm the modules that will be installed. This is the first step when installing from central
   * repository, and the second when installing from file system.
   */
  public void confirmModulesToInstall() {
    selectPopUp(BUTTON_CONTINUE);
    clickContinue();
    Sleep.trySleep();
  }

  /**
   * Accept all the license agreements for the modules that are going to be installed.
   */
  public void acceptAllLicenseAgreements() {
    SeleniumSingleton.INSTANCE.findElementById(OPTION_ACCEPT_ALL_LICENSE_AGREEMENTS).click();
    clickContinue();
    // XXX waiting until the OK button becomes clickable again as last step of module installation
    Sleep.explicitWaitForElementToBeClickable(
        SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK, 90000));
  }

  /**
   * Verify that the process completed successfully.
   *
   * @param moduleName
   *          The name of the module that was installed.
   * @param moduleVersion
   *          The version of the installed module.
   */
  public void verifyProcessCompletedSuccessfully(String moduleName, String moduleVersion) {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        String.format(FORMAT_MODULE_SUCCESSFULLY_INSTALLED, moduleName, moduleVersion));
  }
}
