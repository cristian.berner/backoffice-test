/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicWindowView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verifies actions on the Heartbeat Configuration classic window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class ModuleManagementWindow extends ClassicWindowView {

  /** The window title. */
  private static final String TITLE = "Module Management";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.APPLICATION,
      Menu.MODULE_MANAGEMENT };

  /**
   * Class constructor.
   *
   */
  public ModuleManagementWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    super.load();
    InstalledModulesFormTab installedModulesFormTab = new InstalledModulesFormTab();
    installedModulesFormTab.waitUntilTabIsVisible();
    addTab(installedModulesFormTab);
    setCurrentTab(installedModulesFormTab);
    addTab(new AddModulesFormTab());
    addTab(new SettingsFormTab());
  }

  /**
   * Select and return the Installed Modules tab.
   *
   * @return the Installed Modules tab.
   */
  public InstalledModulesFormTab selectInstalledModulesTab() {
    return (InstalledModulesFormTab) selectTab(InstalledModulesFormTab.TITLE);
  }

  /**
   * Select and return the Add Modules tab.
   *
   * @return the Add Modules tab.
   */
  public AddModulesFormTab selectAddModulesTab() {
    return (AddModulesFormTab) selectTab(AddModulesFormTab.TITLE);
  }

  /**
   * Select and return the Settings tab.
   *
   * @return the Settings tab.
   */
  public SettingsFormTab selectSettingsTab() {
    return (SettingsFormTab) selectTab(SettingsFormTab.TITLE);
  }
}
