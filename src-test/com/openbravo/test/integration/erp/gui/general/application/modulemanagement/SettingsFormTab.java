/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.modulemanagement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.gui.tabs.FormTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verify actions on Settings form tab.
 *
 * @author elopio
 *
 */
public class SettingsFormTab extends FormTab {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Settings";

  /** Identifier of the tab. */
  public static final String TAB = "tabSettings";

  /* Component identifiers. */
  /** Identifier of the scan status combo box. */
  private static final String IDENTIFIER_COMBO_BOX_SCAN_STATUS = "inpScanLevel";
  /** Name of the module combo box. */
  @SuppressWarnings("unused")
  private static final String NAME_COMBO_BOX_MODULE = "inpModule";
  /** Identifier of the module status combo box. */
  @SuppressWarnings("unused")
  private static final String IDENTIFIER_COMBO_BOX_MODULE_STATUS = "inpModuleLevel";
  /** Identifier of the add button. */
  @SuppressWarnings("unused")
  private static final String IDENTIFIER_BUTTON_ADD = "buttonAdd";
  /** Identifier of the search status combo box. */
  private static final String IDENTIFIER_COMBO_BOX_SEARCH_STATUS = "inpSearchLevel";
  /** Identifier of the save button. */
  private static final String IDENTIFIER_BUTTON_SAVE = "buttonSave";
  /** Identifier of the cancel button. */
  @SuppressWarnings("unused")
  private static final String IDENTIFIER_BUTTON_CANCEL = "buttonCancel";

  /** The message displayed when the settings were successfully saved. */
  private static final String MESSAGE_BODY_SETTINGS_SAVED_SUCCESSFULLY = "The settings have been successfully saved.";

  /**
   * Class constructor.
   *
   */
  public SettingsFormTab() {
    super(TITLE, TAB);
  }

  /**
   * Set the status for the scan and search
   *
   * @param scanStatus
   *          The status that will be set for the scan.
   * @param searchStatus
   *          The status that will be set for the search.
   * @return true if at least one of the status was changed. False if both statuses were already the
   *         requested.
   */
  public boolean setStatus(String scanStatus, String searchStatus) {
    boolean statusChanged = setScanStatus(scanStatus) | setSearchStatus(searchStatus);
    return statusChanged;
  }

  /**
   * Set the scan status.
   *
   * @param status
   *          The status that will be set.
   * @return true if the status was changed. False if the status was already the required.
   */
  public boolean setScanStatus(String status) {
    logger.debug("Set scan status to {}.", status);
    Select comboBoxScanStatus = new Select(
        SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_SCAN_STATUS));
    boolean setStatus = !comboBoxScanStatus.getFirstSelectedOption().getText().equals(status);
    if (setStatus) {
      comboBoxScanStatus.selectByVisibleText(status);
    }
    return setStatus;
  }

  /**
   * Set the search status.
   *
   * @param status
   *          The status that will be set.
   * @return true if the status was changed. False if the status was already the desired.
   */
  public boolean setSearchStatus(String status) {
    logger.debug("Set search status to {}.", status);
    Select comboBoxSearchStatus = new Select(
        SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_SEARCH_STATUS));
    boolean setStatus = !comboBoxSearchStatus.getFirstSelectedOption().getText().equals(status);
    if (setStatus) {
      comboBoxSearchStatus.selectByVisibleText(status);
    }
    return setStatus;
  }

  /**
   * Click the save button.
   */
  public void clickSave() {
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_BUTTON_SAVE).click();
  }

  /**
   * Assert that the settings were successfully saved.
   */
  public void asssertSettingsSavedSuccessfully() {
    MessageBox messageBox = new MessageBox();
    messageBox.verify("", MESSAGE_BODY_SETTINGS_SAVED_SUCCESSFULLY);
  }

}
