/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.enterprise.organization;

import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Organization tab.
 *
 * @author elopio
 *
 */
public class OrganizationTab extends GeneratedTab<OrganizationData> {

  /** The tab title. */
  static final String TITLE = "Organization";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "143";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry keys. */
  private static final String REGISTRY_KEY_BUTTON_SET_AS_READY = "org.openbravo.client.application.toolbar.button.ready.143";

  /**
   * Class constructor.
   *
   */
  public OrganizationTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new InformationTab(this));
    addChildTab(new WarehouseTab(this));
  }

  /**
   * Set the organization as ready.
   */
  public void setAsReady() {
    Button buttonSetAsReady = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_SET_AS_READY));
    buttonSetAsReady.click();
    SetAsReadyPopUp popUp = new SetAsReadyPopUp();
    popUp.setAsReady();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }
}
