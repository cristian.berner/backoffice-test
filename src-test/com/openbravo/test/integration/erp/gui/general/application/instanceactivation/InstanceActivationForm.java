/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.general.application.instanceactivation;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.erp.data.generalsetup.application.instanceactivation.ActivateOnlineData;
import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicFormView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verify actions on Instance Activation screen.
 *
 * @author elopio
 *
 */
public class InstanceActivationForm extends ClassicFormView {

  /** The window title. */
  private static final String TITLE = "Instance Activation";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, Menu.APPLICATION,
      Menu.INSTANCE_ACTIVATION };

  /** Locator of the Inactive Subscription Status label. */
  public static final String LABEL_SUBSCRIPTION_STATUS_INACTIVE = "subStatus_inactive";
  /** Locator of the Active Subscription Status label. */
  public static final String LABEL_SUBSCRIPTION_STATUS_ACTIVE = "subStatus_active";
  /** Identifier of the activate online button. */
  private static final String BUTTON_ACTIVATE_ONLINE = "buttonActivateOnline";
  /** Identifier of the deactivate button. */
  private static final String BUTTON_DEACTIVATE = "buttonDeactivate";

  /**
   * Enumerator with the values of Subscription Status.
   *
   * @author elopio
   *
   */
  public enum Status {
    ACTIVE("Active"), INACTIVE("Inactive");

    private final String status;

    private Status(String status) {
      this.status = status;
    }

    public String status() {
      return status;
    }
  }

  /**
   * Class constructor.
   *
   */
  public InstanceActivationForm() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Returns true if the system is active.
   *
   * @return true if the system is active, otherwise false.
   */
  public boolean isActive() {
    try {
      WebElement labelStatusActive = SeleniumSingleton.INSTANCE
          .findElementById(LABEL_SUBSCRIPTION_STATUS_ACTIVE);
      return labelStatusActive.getText().equals(Status.ACTIVE.status());
    } catch (NoSuchElementException exception) {
      return false;
    }
  }

  /**
   * Verify the subscription status of the application.
   *
   * @param status
   *          The expected status.
   * @param labelLocator
   *          The locator of the label.
   */
  public void verifyStatus(String status, String labelLocator) {
    WebElement labelStatus = SeleniumSingleton.INSTANCE.findElementById(labelLocator);
    assertThat("Wrong status", labelStatus.getText(), is(equalTo(status)));
  }

  /**
   * Verify that the subscription status is inactive.
   */
  public void verifyInactiveStatus() {
    verifyStatus(Status.INACTIVE.status(), LABEL_SUBSCRIPTION_STATUS_INACTIVE);
  }

  /**
   * Verify that the subscription status is active.
   */
  public void verifyActiveStatus() {
    verifyStatus(Status.ACTIVE.status(), LABEL_SUBSCRIPTION_STATUS_ACTIVE);
  }

  /**
   * Dismisses alert if needed
   */
  private void dismissAlert() {
    try {
      // New from selenium 2.23.1 the alert is now visible
      // TODO: <chromedriver> improving alert handling to make it compatible with Chrome too
      WebDriverWait tempWait = new WebDriverWait(SeleniumSingleton.INSTANCE, 30);
      tempWait.until(ExpectedConditions.alertIsPresent());
      SeleniumSingleton.INSTANCE.switchTo().alert().accept();
    } catch (UnhandledAlertException uae) {
      SeleniumSingleton.INSTANCE.switchTo().alert().accept();
    } catch (TimeoutException te) {
      logger.trace("Alert took too long to appear, or was missing. Continuing execution. \n {}",
          te::getStackTrace);
    } catch (NoAlertPresentException e) {
      logger.warn("No alert was found.\n {}", e::getStackTrace);
    }
  }

  /**
   * Deactivate the instance.
   */
  public void deactivateInstance() {
    String mainWindow = SeleniumSingleton.INSTANCE.getWindowHandle();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_DEACTIVATE).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final DeactivatePopUp deactivatePopUp = new DeactivatePopUp();
    deactivatePopUp.deactivate();
    SeleniumSingleton.INSTANCE.switchTo().window(mainWindow);

    dismissAlert();
    waitForFrame();
    new MessageBox();
    // TODO Selenium2
    // messageBox.waitForMessageVisible();
  }

  /**
   * Activate the instance online.
   *
   * @param activateOnlineData
   *          The data for online activation.
   */
  public void activateInstanceOnline(ActivateOnlineData activateOnlineData) {
    String mainWindow = SeleniumSingleton.INSTANCE.getWindowHandle();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_ACTIVATE_ONLINE).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final ActivateOnlinePopUp activateOnlinePopUp = new ActivateOnlinePopUp();
    activateOnlinePopUp.activate(activateOnlineData);
    Sleep.execSingletonActionWithFWait(SeleniumSingleton.INSTANCE.switchTo().window(mainWindow),
        NoSuchWindowException.class, 10000);

    dismissAlert();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitForFrame();
    // TODO Selenium2
    Sleep.trySleep();
    // final MessageBox messageBox = new MessageBox();
    // messageBox.waitForMessageVisible();
  }
}
