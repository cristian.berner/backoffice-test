/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory.classic;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.PhysicalInventoryHeaderData;
import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicButtonPopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Physical Inventory Header tab.
 *
 * @author elopio
 *
 */
public class HeaderTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Header tab. */
  public static final String TAB = "tabname255";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";
  /** Identifier of the create inventory count list button. */
  private static final String BUTTON_CREATE_INVETORY_COUNT_LIST = "GenerateList_linkBTN";

  /**
   * Format string used to check the message displayed after creating inventory count list.
   */
  private static final String FORMAT_MESSAGE_CREATE_INVENTORY_COUNT_LIST = "Inserted = %d, Updated=%d";

  /**
   * Class constructor
   *
   */
  public HeaderTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new LinesTab());
  }

  /**
   * Create a new Physical Inventory header.
   *
   * @param data
   *          The data of the Physical Inventory header that will be created.
   */
  public void create(PhysicalInventoryHeaderData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    clickSave();

  }

  /**
   * Create the inventory count list.
   *
   * @param data
   *          The data of the Inventory Count to create.
   */
  public void creatInventoryCountList(CreateInventoryCountListData data) {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_CREATE_INVETORY_COUNT_LIST).click();
    String windowHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final CreateInventoryCountListPopUp createInventoryCountListPopUp = new CreateInventoryCountListPopUp();
    createInventoryCountListPopUp.create(data);
    SeleniumSingleton.INSTANCE.switchTo().window(windowHandle);
    waitForFrame();
  }

  /**
   * Verify that the process completed successfully.
   *
   * @param inserted
   *          The number of inserted records.
   * @param updated
   *          The number of updated records.
   */
  public void verifyProcessCompletedSuccessfully(int inserted, int updated) {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_PROCESS_COMPLETED_SUCCESSFULLY,
        String.format(FORMAT_MESSAGE_CREATE_INVENTORY_COUNT_LIST, inserted, updated));
  }

  /**
   * Process inventory count.
   */
  @Override
  public void process() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESSING).click();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    final OBClassicButtonPopUp processPopUp = new OBClassicButtonPopUp() {
      /**
       * Select the pop up frame.
       */
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("BUTTON");
        SeleniumSingleton.INSTANCE.switchTo().frame("mainframe");
      }
    };
    processPopUp.selectPopUpWithOKButton();
    processPopUp.clickOK();
    waitForFrame();
  }
}
