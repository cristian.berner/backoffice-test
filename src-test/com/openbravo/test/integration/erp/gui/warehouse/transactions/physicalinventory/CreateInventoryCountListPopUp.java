/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.physicalinventory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.warehouse.transactions.physicalinventory.CreateInventoryCountListData;
import com.openbravo.test.integration.erp.gui.popups.LocatorSelectorPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on create inventory count list PopUp
 *
 * @author elopio
 *
 */
public class CreateInventoryCountListPopUp extends OBClassicPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Identifier of the inventory quantity combo box */
  private static final String COMBO_BOX_INVENTORY_QUANTITY = "reportQtyRange_S";

  /**
   * Class constructor
   */
  public CreateInventoryCountListPopUp() {
    super(TestRegistry
        .getObjectString("org.openbravo.classicpopup./PhysicalInventory/Header_Edition.html"));
  }

  /**
   * Create an inventory count list.
   *
   * @param data
   *          The data of the Inventory Count to create.
   */
  public void create(CreateInventoryCountListData data) {
    logger.debug("Before selectPopUpWithOKButton();");
    selectPopUpWithOKButton();
    logger.debug("After selectPopUpWithOKButton();");
    if (data.getStoragebin() != null) {
      logger.debug("Inside \"data.getStoragebin() != null\"");
      String popUpHandle = SeleniumSingleton.INSTANCE.getWindowHandle();
      SeleniumSingleton.INSTANCE.findElementById(LocatorSelectorPopUp.BUTTON_LOCATOR).click();
      final LocatorSelectorPopUp locatorSelector = new LocatorSelectorPopUp();
      locatorSelector.selectLocator(data.getStoragebin());
      SeleniumSingleton.INSTANCE.switchTo().window(popUpHandle);
      waitForFrame();
      logger.debug("Going out from \"data.getStoragebin() != null\"");
    }
    if (data.getInventoryQuantity() != null) {
      logger.debug("Inside \"data.getInventoryQuantity() != null\"");
      try {
        new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_INVENTORY_QUANTITY))
            .selectByVisibleText(data.getInventoryQuantity());
      } catch (NoSuchElementException nsee) {
        // TODO L4: Testing static sleeps to avoid NoSuchElementException due to performance
        // problems
        Sleep.trySleep(8000);
        new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_INVENTORY_QUANTITY))
            .selectByVisibleText(data.getInventoryQuantity());
      }
      logger.debug("Going out from \"data.getInventoryQuantity() != null\"");
    }
    clickOK();
  }
}
