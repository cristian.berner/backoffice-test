/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.warehouse.transactions.stockreservation;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Stock Reservation standard window of OpenbravoERP.
 *
 * @author aferraz
 *
 */
public class StockReservationWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Stock Reservation";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.WAREHOUSE_MANAGEMENT,
      Menu.WAREHOUSE_TRANSACTIONS, Menu.STOCK_RESERVATION };

  /**
   * Class constructor.
   *
   */
  public StockReservationWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Select and return the Reservation tab.
   */
  @Override
  public void load() {
    ReservationTab reservationTab = new ReservationTab();
    addTopLevelTab(reservationTab);
  }

  /**
   * Select and return the Stock tab.
   *
   * @return the Stock tab.
   */
  public StockTab selectStockTab() {
    return (StockTab) selectTab(StockTab.IDENTIFIER);
  }

}
