/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Arun kumar <arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.tabs;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.gui.MessageBox;
import com.openbravo.test.integration.erp.gui.popups.FilterPopUp;
import com.openbravo.test.integration.erp.gui.popups.PopUp;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Class that represents a Generated Tab on OpenbravoERP.
 *
 * @author elopio
 *
 */
public abstract class GeneratedTab extends Tab {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Class of the selected toolbar button. */
  private static final String CLASS_TOOLBAR_BUTTON_SELECTED = "Main_ToolBar_Button_Selected";

  /** Identifier of the edition button */
  protected static final String BUTTON_EDITION = "buttonEdition";
  /** Class of the selected toolbar button. */
  /** Class of the edition link when new was selected */
  @SuppressWarnings("unused")
  private static final String CLASS_BUTTON_EDITION_NEW_SELECTED = " Main_ToolBar_Button_Icon_Edition_new";

  /** Identifier of the edition link */
  private static final String LINK_EDITION = "linkButtonEdition";
  /** Class of the edition link when new was selected */
  private static final String CLASS_LINK_EDITION_NEW_SELECTED = "edition_new_selected";
  /** Identifier of the relation button */
  protected static final String BUTTON_RELATION = "buttonRelation";
  /** Identifier of the relation link */
  @SuppressWarnings("unused")
  private static final String LINK_RELATION = "linkButtonRelation";

  /** Identifier of the new button */
  protected static final String BUTTON_NEW = "buttonNew";
  /** Identifier of the Delete button */
  protected static final String BUTTON_DELETE = "buttonDelete";
  /** Identifier of the save button */
  protected static final String BUTTON_SAVE = "buttonSave";
  /** Identifier of the save link (enabled) */
  protected static final String LINK_BUTTON_SAVE = "linkButtonSave";
  /** Identifier of the search button */
  protected static final String BUTTON_SEARCH = "buttonSearch";
  /** Identifier of the search filtered button */
  protected static final String BUTTON_SEARCH_FILTERED = "buttonSearchFiltered";
  /** Identifier of the first button */
  protected static final String BUTTON_FIRST = "buttonFirst";
  /** Identifier of the previous button */
  protected static final String BUTTON_PREVIOUS = "buttonPrevious";
  /** Identifier of the next button */
  protected static final String BUTTON_NEXT = "buttonNext";
  /** Identifier of the bookmark label */
  protected static final String LABEL_BOOKMARK = "bookmark";

  /** Locator of the Linked Items link */
  protected static final String LINK_LINKED_ITEMS = "link=Linked Items";

  /** Identifier of the processing button */
  protected static final String BUTTON_PROCESSING = "Processing_linkBTN";
  /** Identifier of the process button */
  protected static final String BUTTON_PROCESS = "Processed_linkBTN";
  /** Identifier of the posted button */
  protected static final String BUTTON_POSTED = "Posted_BTNname";

  /** The text displayed on the bookmark when the grid is empty */
  private static final String BOOKMARK_EMPTY_GRID = "0 - 0 / 0";

  // TODO see issue 11201 - https://issues.openbravo.com/view.php?id=11201
  /** Locator of the header of the table displayed on grid view. */
  protected static final String TABLE_HEADER_GRID_VIEW = "//table[@id='undefined_table_header']";
  // TODO see issue 11201 - https://issues.openbravo.com/view.php?id=11201
  /**
   * Locator of the table displayed on grid view.
   */
  protected static final String TABLE_GRID_VIEW = "//table[@id='undefined_table']";

  /** Index of the row number column */
  private static final int COLUMN_ROW_NUMBER = 0;

  /** Format string used to check insertion messages */
  private static final String FORMAT_ROWS_INSERTED_MESSAGE = "%d row/s inserted";
  /** Format string used to check update messages */
  private static final String FORMAT_ROWS_UPDATED_MESSAGE = "%d row/s updated";
  /** Format string used to check delete messages */
  private static final String FORMAT_ROWS_DELETED_MESSAGE = "%d row/s deleted";

  /** Confirmation message shown after clicking the delete button. */
  @SuppressWarnings("unused")
  private static final String DELETE_CONFIRMATION_MESSAGE = "^The record will be deleted\\. Are you sure you want to continue[\\s\\S]$";
  // TODO: 2009-10-13-PLU: If this identifier is not used should be removed
  /**
   * Confirmation message shown when the changes in current record hasn't been saved
   */
  @SuppressWarnings("unused")
  private static final String CONFIRMATION_UNSAVED_CHANGES_IN_RECORD = "There are changes in the current record. Do you want to continue? (You will lose your changes)";

  /* Bookmarks */
  /** The index of the first visible row, starting from 1 */
  private int firstVisibleIndex;
  /** The index of the last visible row, starting from 1 */
  private int lastVisibleIndex;
  /** The number of rows in the screen */
  private int rowCount;

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the tab.
   * @param level
   *          The level of the tab.
   * @param tab
   *          The identifier of the tab.
   */
  public GeneratedTab(String title, int level, String tab) {
    super(title, level, tab);
  }

  /**
   * Returns true if this window is on form view.
   *
   * @return boolean indicating whether this window is on form view
   */
  protected boolean isOnFormView() {
    return SeleniumSingleton.INSTANCE.findElementById(BUTTON_EDITION)
        .getAttribute(Attribute.CLASS.getName())
        .contains(CLASS_TOOLBAR_BUTTON_SELECTED);
  }

  /**
   * Returns true if this window is on form grid.
   *
   * @return boolean indicating whether this window is on grid view
   */
  protected boolean isOnGridView() {
    return SeleniumSingleton.INSTANCE.findElementById(BUTTON_RELATION)
        .getAttribute(Attribute.CLASS.getName())
        .contains(CLASS_TOOLBAR_BUTTON_SELECTED);
  }

  /**
   * Perform the actions required to create a new record TODO find a better name for this method, as
   * it does more than clicking
   */
  public void clickCreateNewRecord() {
    if (!isOnFormView()) {
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_NEW).click();
      waitForDataToLoad();
    } else if (!SeleniumSingleton.INSTANCE.findElementById(LINK_EDITION)
        .getAttribute(Attribute.CLASS.getName())
        .contains(CLASS_LINK_EDITION_NEW_SELECTED)) {
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_NEW).click();
      // TODO here we will possible require to accept a confirmation
      waitForDataToLoad();
    }
  }

  /**
   * Delete the selected record.
   */
  public void deleteRecord() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_DELETE).click();
    // TODO Selenium2
    // assertTrue(SeleniumSingleton.INSTANCE.getConfirmation().matches(DELETE_CONFIRMATION_MESSAGE));
  }

  /**
   * Switch to form view
   */
  public void switchToFormView() throws OpenbravoERPTestException {
    waitForDataToLoad();
    if (!isOnFormView()) {
      waitForGridToLoad();
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_EDITION).click();
      waitForDataToLoad();
      if (!isOnFormView()) {
        throw new OpenbravoERPTestException("Couldn't switch to form view");
      }
    }
  }

  /**
   * Switch to grid view and wait until the grid has been loaded
   */
  public void switchToGridView() throws OpenbravoERPTestException {
    waitForDataToLoad();
    if (!isOnGridView()) {
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_RELATION).click();
      // XXX this was added to vercome issue 13297 -
      // https://issues.openbravo.com/view.php?id=13297 -
      // but a better way to handle confirmation has to be implemented.
      // TODO Selenium2
      // if (SeleniumSingleton.INSTANCE.isConfirmationPresent()) {
      // SeleniumSingleton.INSTANCE.getConfirmation();
      // // TODO check the message displayed by the confirmation
      // }
      waitForGridToLoad();
      waitForDataToLoad();
      if (!isOnGridView()) {
        throw new OpenbravoERPTestException("Couldn't switch to grid view");
      }
    } else {
      waitForGridToLoad();
    }
  }

  /**
   * Wait until the grid has completely loaded
   */
  protected void waitForGridToLoad() throws OpenbravoERPTestException {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for bookmark label to be loaded");
        try {
          SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK);
        } catch (NoSuchElementException exception) {
          return false;
        }
        return true;
      }
    });
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for bookmark text to be loaded.");
        final String bookmark = SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK)
            .getText();
        boolean isBookmarkLoaded;
        try {
          isBookmarkLoaded = !bookmark.equals("")
              && (bookmark.equals(BOOKMARK_EMPTY_GRID) || getRowCount() != 0);
        } catch (final OpenbravoERPTestException e) {
          isBookmarkLoaded = false;
          e.printStackTrace();
        }
        return isBookmarkLoaded;
      }
    });
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for table to be loaded.");
        try {
          SeleniumSingleton.INSTANCE.findElementByXPath(TABLE_GRID_VIEW);
        } catch (NoSuchElementException exception) {
          return false;
        }
        return true;
      }
    });
  }

  /**
   * Click save button and wait for the page to load
   */
  protected void clickSave() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_SAVE).click();
  }

  /**
   * Click search button
   */
  public void clickSearch() {
    try {
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_SEARCH).click();
    } catch (NoSuchElementException exception) {
      SeleniumSingleton.INSTANCE.findElementById(BUTTON_SEARCH_FILTERED).click();
    }
  }

  /**
   * Click the first button
   */
  public void clickFirst() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_FIRST).click();
  }

  /**
   * Click the previous button
   */
  public void clickPrevious() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PREVIOUS).click();
  }

  /**
   * Click the next button.
   */
  public void clickNext() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_NEXT).click();
  }

  /**
   * Verify that the record was saved.
   */
  public void verifySave() {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_EMPTY, formatInsertedRowsMessage(1));
  }

  /**
   * Verify that the record was updated.
   */
  public void verifyUpdate() {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_EMPTY, formatUpdatedRowsMessage(1));
  }

  /**
   * Verify that the record was deleted.
   */
  public void verifyDelete() {
    final MessageBox messageBox = new MessageBox();
    messageBox.verify(MessageBox.TITLE_EMPTY, formatDeletedRowsMessage(1));
  }

  /**
   * Format the message displayed on row insertions
   *
   * @param number
   *          the number of inserted rows
   * @return the message of rows inserted
   */
  private String formatInsertedRowsMessage(int number) {
    return String.format(FORMAT_ROWS_INSERTED_MESSAGE, number);
  }

  /**
   * Format the message displayed on row updates
   *
   * @param number
   *          the number of updated rows
   * @return the message of rows updated
   */
  private String formatUpdatedRowsMessage(int number) {
    return String.format(FORMAT_ROWS_UPDATED_MESSAGE, number);
  }

  /**
   * Format the message displayed on row deletes
   *
   * @param number
   *          the number of deleted rows
   * @return the message of rows deleted
   */
  private String formatDeletedRowsMessage(int number) {
    return String.format(FORMAT_ROWS_DELETED_MESSAGE, number);
  }

  /**
   * Split the bookmark to get the first and last index, and the row count
   *
   * @return An array of the Strings that form the bookmark
   */
  private String[] splitBookmark() throws OpenbravoERPTestException {
    if (isOnGridView()) {
      SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
          logger.debug("Waiting for the bookmark not to be empty");
          return (!SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK).getText().isEmpty());
        }
      });
      final String bookmark = SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK).getText();
      // XXX the regular expression is wrong, it's necessary to escape the
      // -
      return bookmark.split("[( - )( / )]");
    } else {
      throw new OpenbravoERPTestException("The window is not on grid view");
    }
  }

  /**
   * Get the number of the first visible row from the bookmark
   *
   * @return The number of the first visible row
   * @throws OpenbravoERPTestException
   */
  private int getFirstVisibleRow() throws OpenbravoERPTestException {
    final String[] split = splitBookmark();
    return Integer.parseInt(split[0]);
  }

  /**
   * Get the number of the last visible row from the bookmark
   *
   * @return The number of the last visible row
   */
  private int getLastVisibleRow() throws OpenbravoERPTestException {
    final String[] split = splitBookmark();
    return Integer.parseInt(split[2]);
  }

  /**
   * Get the number of rows in the grid
   *
   * @return The number of rows in the grid
   */
  public int getRowCount() throws OpenbravoERPTestException {
    final String[] split = splitBookmark();
    return Integer.parseInt(split[5]);
  }

  /**
   * Update the values taken from bookmark
   */
  private void updateBookmarkValues() throws OpenbravoERPTestException {
    firstVisibleIndex = getFirstVisibleRow();
    lastVisibleIndex = getLastVisibleRow();
    rowCount = getRowCount();
  }

  /**
   * Make a row visible on grid view
   *
   * @param row
   *          The number of the row, starting from 0
   */
  private void makeRowVisible(int row) throws OpenbravoERPTestException {
    if (isOnGridView()) {
      updateBookmarkValues();
      if (row < firstVisibleIndex - 1) {
        Tables.clickCell(TABLE_GRID_VIEW, 0, 1);
        while (row < firstVisibleIndex - 1) {
          final String bookmark = SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK)
              .getText();
          clickPrevious();
          waitForBookmarkToChange(bookmark);
          updateBookmarkValues();
        }
      }
      if (row > lastVisibleIndex - 1) {
        Tables.clickCell(TABLE_GRID_VIEW, lastVisibleIndex - firstVisibleIndex, 1);
        while (row > lastVisibleIndex - 1) {
          final String bookmark = SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK)
              .getText();
          clickNext();
          waitForBookmarkToChange(bookmark);
          updateBookmarkValues();
        }
      }
    } else {
      throw new OpenbravoERPTestException("The window is not on grid view");
    }
  }

  /**
   * Pause the execution until the bookmark changes
   *
   * @param previousBookmark
   *          The text of the previous bookmark
   */
  private void waitForBookmarkToChange(final String previousBookmark) {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the bookmark to change.");
        final String currentBookmark = SeleniumSingleton.INSTANCE.findElementById(LABEL_BOOKMARK)
            .getText();
        return (!previousBookmark.equals(currentBookmark));
      }
    });
  }

  /**
   * Get the number of the row in the HTML table
   *
   * @param row
   *          The number of the required row, starting from 0
   * @return The number of the row in the HTML table
   */
  protected int getHTMLRowNumber(int row) throws OpenbravoERPTestException {
    makeRowVisible(row);
    return row - firstVisibleIndex + 1;
  }

  /**
   * Click a record by it's number in the table.
   *
   * @param number
   *          The number of the record in the table.
   */
  public void clickRecord(String number) throws OpenbravoERPTestException {
    clickRecordWithText(COLUMN_ROW_NUMBER, number);
  }

  /**
   * Click a record with the specified text
   *
   * @param column
   *          The column where the text will be searched
   * @param text
   *          The text of the record to click
   */
  public void clickRecordWithText(int column, String text) throws OpenbravoERPTestException {
    logger.trace("Function clickRecordWithText({},{})", column, text);
    switchToGridView();
    updateBookmarkValues();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_FIRST).click();
    int i;
    logger.trace("Row count : {}", rowCount);
    for (i = 0; i < rowCount; i++) {
      final int row = getHTMLRowNumber(i);
      final String cellContent = Tables.getCellText(TABLE_GRID_VIEW, row, column);
      logger.debug("Text in column {} in row {} is '{}'", column, row, cellContent);
      if (cellContent.equals(text)) {
        Tables.clickCell(TABLE_GRID_VIEW, row, column);
        break;
      }
    }
    if (i == rowCount) {
      throw new OpenbravoERPTestException(
          "Text '" + text + "' was not found on the column " + column);
    }
  }

  /**
   * XXX This is inefficient, because in the most common case verifying after verifying that a
   * record exists, it will be clicked with clickRecordWithText. On both functions, if the record
   * that's being searched is the last one, all the grid rows should be analyzed starting from the
   * first. In this case, this function will left the pointer on the last record, and will be
   * returned to the first by the next function repeating the analysis of rows again.
   */
  /**
   * Check if a record with a specified text on a column exists.
   *
   * @param column
   *          The column where the text will be searched
   * @param text
   *          The text of the record to click
   * @return true if the record exists. Otherwise, false.
   */
  public boolean existsRecordWithText(int column, String text) throws OpenbravoERPTestException {
    boolean recordExists = false;
    switchToGridView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_FIRST).click();
    int i;
    for (i = 0; i < rowCount; i++) {
      final int row = getHTMLRowNumber(i);
      if (Tables.getCellText(TABLE_GRID_VIEW, row, column).equals(text)) {
        recordExists = true;
      }
    }
    return recordExists;
  }

  /**
   * Process the transaction
   */
  public void process() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_PROCESS).click();
    final PopUp postPopUp = new PopUp();
    postPopUp.selectPopUpWithOKButton();
    postPopUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Post the transaction
   */
  public void post() throws OpenbravoERPTestException {
    switchToFormView();
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_POSTED).click();
    final PopUp postPopUp = new PopUp();
    postPopUp.selectPopUpWithOKButton();
    postPopUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Sort the grid view table clicking a column.
   *
   * @param columnIndex
   *          The index of the column to click.
   */
  public void sort(int columnIndex) throws OpenbravoERPTestException {
    switchToGridView();
    Tables.clickHeader(TABLE_HEADER_GRID_VIEW, columnIndex);
    waitForDataToLoad();
  }

  /**
   * Filter Records.
   */
  public void filter() {
    clickSearch();
    final FilterPopUp filterPopUp = new FilterPopUp();
    filterPopUp.filter();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }
}
