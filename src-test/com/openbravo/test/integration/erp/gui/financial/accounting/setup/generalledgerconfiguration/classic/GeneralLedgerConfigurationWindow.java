/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.classic;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicWindowView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the General Ledger Configuration Window classic window of
 * OpenbravoERP.
 *
 * @author aferraz
 *
 */
public class GeneralLedgerConfigurationWindow extends ClassicWindowView {

  /** The window title. */
  private static final String TITLE = "Business Partner Tax Category";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_SETUP, Menu.GENERAL_LEDGER_CONFIGURATION };

  /**
   * Class constructor.
   *
   */
  public GeneralLedgerConfigurationWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    super.load();
    final GeneralLedgerConfigurationTab generalLedgerConfigurationTab = new GeneralLedgerConfigurationTab();
    generalLedgerConfigurationTab.waitUntilTabIsVisible();
    addTab(generalLedgerConfigurationTab);
    setCurrentTab(generalLedgerConfigurationTab);
  }

  /**
   * Select and return the Business Partner Tax Category tab.
   *
   * @return the Business Partner Tax Category tab.
   */
  public GeneralLedgerConfigurationTab selectGeneralLedgerConfigurationTab() {
    return (GeneralLedgerConfigurationTab) selectTab(GeneralLedgerConfigurationTab.TITLE);
  }

  /**
   * Select and return the General Accounts tab.
   *
   * @return the General Accounts tab.
   */
  public GeneralAccountsTab selectGeneralAccountsTab() {
    return (GeneralAccountsTab) selectTab(GeneralAccountsTab.TITLE);
  }
}
