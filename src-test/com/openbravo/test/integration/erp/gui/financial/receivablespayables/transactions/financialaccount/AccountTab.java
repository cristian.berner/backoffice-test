/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Account tab.
 *
 * @author elopio
 *
 */
public class AccountTab extends GeneratedTab<AccountData> {

  /** The tab title. */
  static final String TITLE = "Account";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "2845D761A8394468BD3BA4710AA888D4";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /* Component identifiers. */
  /** Registry key of the add transaction button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_TRANSACTION = "org.openbravo.client.application.toolbar.button.aPRMAddTransactions.2845D761A8394468BD3BA4710AA888D4";
  /** Registry key of the reconcile button. */
  private static final String REGISTRY_KEY_BUTTON_RECONCILE = "org.openbravo.client.application.toolbar.button.aPRMReconcile.2845D761A8394468BD3BA4710AA888D4";
  /** Registry key of the Match Transaction button. */
  private static final String REGISTRY_KEY_BUTTON_MATCH_TRANSACTION = "org.openbravo.client.application.toolbar.button.aPRMMatchTransactions.2845D761A8394468BD3BA4710AA888D4";

  private static final String FORMAT_SMART_CLIENT_POPUP_ALGORITHM_OK = "isc.Dialog.Warn.okClick()";
  private static final String FORMAT_SMART_CLIENT_POPUP_ALGORITHM_CANCEL = "isc.Dialog.Warn.cancelClick()";

  /** The Match Statement button. */
  private static Button buttonMatchStatement;

  /**
   * Class constructor.
   *
   */
  public AccountTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new TransactionTab(this));
    addChildTab(new PaymentMethodTab(this));
    addChildTab(new ReconciliationsTab(this));
    addChildTab(new BankStatementsTab(this));
  }

  /**
   * Add a transaction.
   *
   * @param document
   *          The document type.
   * @param description
   *          The description of the document.
   */
  public void addTransaction(String document, String description) {
    Button buttonAddTransaction = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_TRANSACTION));
    buttonAddTransaction.click();
    AddTransactionPopUp popUp = new AddTransactionPopUp();
    popUp.addTransaction(document, description);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Add Payment In/Out
   *
   * @param document
   *          The document type.
   * @param toBePaidTo
   *          The business partner to be paid.
   * @param paymentMethod
   *          The payment method.
   * @param purchaseInvoiceNumber
   *          The number of the purchase invoice.
   * @param action
   *          The action to execute on the document.
   */
  public void addPaymentInOut(String document, String toBePaidTo, String paymentMethod,
      String purchaseInvoiceNumber, String action) {
    Button buttonAddTransaction = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_TRANSACTION));
    buttonAddTransaction.click();
    AddTransactionPopUp popUp = new AddTransactionPopUp();
    popUp.addPaymentInOut(document, toBePaidTo, paymentMethod, purchaseInvoiceNumber, action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.trySleep();
  }

  /**
   * Process a reconciliation.
   *
   * @param endingBalance
   *          The reconciliation ending balance.
   * @param hideTransactionsAfterStatementDate
   *          Indicates if the transactions after statement date should be hidden.
   * @param description
   *          The description of the transaction to clear.
   */
  public void reconcile(String endingBalance, boolean hideTransactionsAfterStatementDate,
      String description) {
    Button buttonReconcile = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_RECONCILE));
    buttonReconcile.click();
    ReconciliationPopUp popUp = new ReconciliationPopUp();
    popUp.reconcile(endingBalance, hideTransactionsAfterStatementDate, description);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Open Add Payment popup
   *
   * @param runAlgorithm
   *          boolean that indicates what action is going to be taken.
   * @return The just created MatchStatementProcess
   */
  public MatchStatementProcess<?> openMatchStatement(boolean runAlgorithm) {
    if (buttonMatchStatement == null) {
      buttonMatchStatement = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_MATCH_TRANSACTION));
    }
    buttonMatchStatement.click();

    Sleep.trySleep(3000);
    String action = runAlgorithm ? FORMAT_SMART_CLIENT_POPUP_ALGORITHM_OK
        : FORMAT_SMART_CLIENT_POPUP_ALGORITHM_CANCEL;
    SeleniumSingleton.INSTANCE.executeScript(action);

    @SuppressWarnings({ "rawtypes", "unchecked" })
    MatchStatementProcess<?> matchStatementProcess = new MatchStatementProcess(this);
    return matchStatementProcess;
  }
}
