/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.generalledgerconfiguration.classic;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.accountingschema.GeneralLedgerData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP General Accounts screen
 *
 * @author aferraz
 *
 */
public class GeneralAccountsTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "General Accounts";
  /** The tab level. */
  private static final int LEVEL = 1;

  /* GUI components. */
  /** Identifier of the General Accounts tab */
  public static final String TAB = "tabname200";

  /** Identifier of the fiscal year text field */
  private static final String TEXT_FIELD_FISCAL_YEAR = "GeneralAccountsTab";

  /**
   * Class constructor
   *
   */
  public GeneralAccountsTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new GeneralLedgerConfigurationTab());
  }

  /**
   * Create a new fiscal year
   *
   * @param data
   *          The GeneralLedgerData
   */
  public void create(GeneralLedgerData data) {
    clickCreateNewRecord();
    if (data.getDataField("fiscalYear") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_FISCAL_YEAR)
          .sendKeys(data.getDataField("fiscalYear").toString());
    }
    clickSave();
  }
}
