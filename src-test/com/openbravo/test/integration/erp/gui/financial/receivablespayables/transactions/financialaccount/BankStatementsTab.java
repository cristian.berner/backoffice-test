/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@opebravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TimeoutException;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Transaction tab.
 *
 * @author elopio
 *
 */
public class BankStatementsTab extends GeneratedTab<BankStatementHeaderData> {
  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /** Registry key of the process button in the Import Bank Statement tab. */
  private static final String REGISTRY_KEY_BUTTON_PROCESS_BANKSTATEMENT = "org.openbravo.client.application.toolbar.button.aPRMProcessBankStatement.C56E698100314ABBBBD3A89626CA551C";
  /** The tab title. */
  static final String TITLE = "Imported Bank Statements";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "C56E698100314ABBBBD3A89626CA551C";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent tab.
   */
  public BankStatementsTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new BankStatementLinesTab(this));
  }

  /**
   * Process the transaction
   */
  public void process() {

    Button buttonProcess = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PROCESS_BANKSTATEMENT));
    buttonProcess.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./FinancialAccount/ImportedBankStatements_Edition.html");
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    try {
      waitUntilMessageVisible();
    } catch (TimeoutException toe) {
      logger.info(
          "A TimeoutException was thrown. It is probable it could not be possible to click in the OK button of the popup. Trying again.");
      popUp.selectPopUpWithOKButton();
      popUp.clickOK();
      SeleniumSingleton.INSTANCE.switchTo().defaultContent();
      waitUntilMessageVisible();
    }
  }
}
