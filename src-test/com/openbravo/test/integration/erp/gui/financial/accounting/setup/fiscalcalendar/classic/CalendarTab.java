/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar.classic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.fiscalcalendar.CalendarData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Fiscal Calendar tab.
 *
 * @author elopio
 *
 */
public class CalendarTab extends GeneratedTab {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Calendar";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Calendar tab. */
  public static final String TAB = "tabname128";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";

  /**
   * Class constructor
   *
   */
  public CalendarTab() {
    super(TITLE, LEVEL, TAB);
    addSubTab(new YearTab());
  }

  /**
   * Create a new calendar.
   *
   * @param calendarData
   *          The calendar data.
   */
  public void create(CalendarData calendarData) {
    logger.info("Create the calendar.");
    clickCreateNewRecord();
    if (calendarData.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(calendarData.getDataField("name").toString());
    }
    clickSave();
  }
}
