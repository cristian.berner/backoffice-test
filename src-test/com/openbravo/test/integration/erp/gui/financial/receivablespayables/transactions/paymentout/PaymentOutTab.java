/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Payment Out tab.
 *
 * @author elopio
 *
 */
public class PaymentOutTab extends GeneratedTab<PaymentOutHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "PaymentOut";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "F7A52FDAAA0346EFA07D53C125B40404";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the create lines from button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_DETAILS = "org.openbravo.client.application.toolbar.button.aPRMAddScheduledpayments.F7A52FDAAA0346EFA07D53C125B40404";
  private static final String REGISTRY_KEY_BUTTON_EXECUTE = "org.openbravo.client.application.toolbar.button.aprmExecutepayment.F7A52FDAAA0346EFA07D53C125B40404";
  @SuppressWarnings("unused")
  private static final String REGISTRY_KEY_BUTTON_REVERSE = "org.openbravo.client.application.toolbar.button.aPRMReversePayment.F7A52FDAAA0346EFA07D53C125B40404";
  private static final String REGISTRY_KEY_BUTTON_REACTIVATE = "org.openbravo.client.application.toolbar.button.aPRMProcessPayment.F7A52FDAAA0346EFA07D53C125B40404";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonAddDetails;
  /** The execute payment button. */
  private static Button buttonExecute;
  /** The reverse payment button. */
  private static Button buttonReverse;
  /** The reactivate payment button. */
  private static Button buttonReactivate;

  /**
   * Class constructor.
   *
   */
  public PaymentOutTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new PaymentOutLinesTab(this));
    addChildTab(new ExchangeRatesTab(this));
    addChildTab(new UsedCreditSourceTab(this));
    buttonAddDetails = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_DETAILS));
    buttonExecute = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_EXECUTE));
    buttonReactivate = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_REACTIVATE));
    // buttonReverse = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_REVERSE));
  }

  /**
   * Assert the data of the add payment out popup.
   *
   * @param payingTo
   *          The business partner that will be payed.
   * @param amount
   *          The amount of the payment.
   * @param transactionType
   *          The type of the transaction.
   */
  public void assertAddPaymentOutData(String payingTo, String amount, String transactionType) {
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    AddPaymentPopUpData paymentData = new AddPaymentPopUpData.Builder().received_from(payingTo)
        .expected_payment(amount)
        .actual_payment(amount)
        .transaction_type(transactionType)
        .build();
    addPaymentProcess.assertData(paymentData);
    addPaymentProcess.close();
  }

  /**
   * Add payment out details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPaymentOut(String transactionType, String documentNumber, String action) {
    logger.debug("Adding payment out. Transaction type: '{}', Document number: '{}'.",
        transactionType, documentNumber);
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.process(transactionType, documentNumber, action);
  }

  public AddPaymentProcess addPaymentOutOpen() {
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    return addPaymentProcess;
  }

  /**
   * Execute a Payment Out.
   */
  public void execute() {
    logger.debug("Executing action: Execute Payment");
    // TODO L0: Sleep to avoid problems clicking Execute Process Button. The following sleeps should
    // be reduced. Try with 3500ms
    Sleep.trySleep(3500);
    buttonExecute.click();
    Sleep.trySleep(3500);
    logger.debug("Execute button clicked");
    ExecutePaymentOutPopUp popUp = new ExecutePaymentOutPopUp();
    Sleep.trySleep(3500);
    popUp.process();
    // TODO L1: It seems following sleep is required after updating to FF 60esr. Try to reduce it to
    // 3500ms
    Sleep.trySleep(3500);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Reactivate a Payment Out.
   */
  public void reactivate(String action) {
    logger.debug("Executing action: '{}'.", action);
    buttonReactivate.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./PaymentOut/Header_Edition.html");
    popUp.processPayment(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Process a Payment Out.
   *
   * @param number
   *          The process action to request for the payment.
   */
  public void execute(String number) {
    logger.debug("Executing action: Execute Payment");
    buttonExecute.click();
    ExecutePaymentOutPopUp popUp = new ExecutePaymentOutPopUp();
    popUp.process(number);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Reverse a Payment In.
   *
   * @param action
   *          The reverse action to request for the payment.
   * @param date
   *          The reverse date to request for the payment.
   */
  public void reverse(String action, String date) {
    logger.debug("Executing action: Reverse Payment");
    buttonReverse.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./PaymentOut/Header_Edition.html");
    popUp.reversePayment(action, date);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param visibleDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose visibility have to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.debug("Asserting visible display logic");
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param enabledDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose enable status have to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.debug("Asserting enabled display logic");
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param expandedDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose expansion status have to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.debug("Asserting expanded display logic");
    buttonAddDetails.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  public void assertPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT);
  }

  public void assertPaymentExecutedSuccessfully() {
    standardView.assertProcessExecutionSuccededContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_EXECUTED_PAYMENT);
  }

  public void assertRefundedPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_REFUNDED_PAYMENT);
  }

  public void assertFinancialAccountInactive(String financialAccount) {
    standardView.assertWarningMessage(String.format(
        AddPaymentProcess.REGEXP_WARNING_MESSAGE_FINANCIAL_ACCOUNT_INACTIVE, financialAccount));
  }

  public void assertPaymentMethodInactive(String paymentMethod) {
    standardView.assertWarningMessage(String
        .format(AddPaymentProcess.REGEXP_WARNING_MESSAGE_PAYMENT_METHOD_INACTIVE, paymentMethod));
  }

  public void assertFinancialAccountAndPaymentMethodInactive(String financialAccount,
      String paymentMethod) {
    standardView.assertWarningMessage(String.format(
        AddPaymentProcess.REGEXP_WARNING_MESSAGE_FINANCIAL_ACCOUNT_AND_PAYMENT_METHOD_INACTIVE,
        financialAccount, paymentMethod));
  }

}
