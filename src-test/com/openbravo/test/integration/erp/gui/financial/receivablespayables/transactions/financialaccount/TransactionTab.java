/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   David Miguélez <david.miguelez@opebravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentSelectorField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Transaction tab.
 *
 * @author elopio
 *
 */
public class TransactionTab extends GeneratedTab<TransactionsData> {
  /** Registry key of the process button in the transaction tab. */
  private static final String REGISTRY_KEY_BUTTON_PROCESS_TRANSACTION = "org.openbravo.client.application.toolbar.button.aprmProcessed.23691259D1BD4496BCC5F32645BCA4B9";
  /** The tab title. */
  static final String TITLE = "Transaction";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "23691259D1BD4496BCC5F32645BCA4B9";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent tab.
   */
  public TransactionTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new ExchangeRatesTab(this));
  }

  /**
   * Assert that deposit and payment amounts are zero.
   */
  public void assertAmountsAreZero() {
    assertThat(standardView.getMessageContents(),
        is(equalTo(AddPaymentProcess.REGEXP_ERROR_MESSAGE_AMOUNTS_ARE_ZERO)));
  }

  /**
   * Assert that payment and glitem are empty.
   */
  public void assertPaymentAndGlitemAreEmpty() {
    assertThat(standardView.getMessageContents(),
        is(equalTo(AddPaymentProcess.REGEXP_ERROR_MESSAGE_PAYMENT_AND_GLITEM_ARE_EMPTY)));
  }

  /**
   * Assert that the period does not exist or it is not opened.
   */
  public void assertPeriodDoesNotExistOrItIsNotOpened() {
    assertThat(standardView.getMessageContents(),
        is(equalTo(AddPaymentProcess.REGEXP_ERROR_MESSAGE_PERIOD_NOT_OPENED)));
  }

  /**
   * Process the transaction
   */
  public void process() {
    Button buttonProcess = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_PROCESS_TRANSACTION));

    Sleep.smartWaitButtonEnabled(REGISTRY_KEY_BUTTON_PROCESS_TRANSACTION, 500);

    buttonProcess.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./FinancialAccount/Transaction_Edition.html");
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Open Add Payment popup
   *
   * @return The Add Payment PopUp
   */
  public AddPaymentProcess openAddPayment() {
    ((AddPaymentSelectorField) this.getForm().getField("finPayment")).openPopUpToProcess();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    return addPaymentProcess;
  }

  /**
   * Get the value present on the filter of a selector field.
   *
   * @param fieldName
   *          The name of the field whose filter value will be returned.
   */
  public Object getSelectorFieldFilterClearedValue(String fieldName) {
    return ((AddPaymentSelectorField) this.getForm().getField("finPayment"))
        .getClearedFilterValue(fieldName);
  }
}
