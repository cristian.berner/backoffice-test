/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBParameterWindowForm;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SectionItem;
import com.openbravo.test.integration.selenium.Sleep;

import junit.framework.TestCase;

@SuppressWarnings("rawtypes")
public class AddPaymentProcess extends OBParameterWindow {

  private static Logger log = LogManager.getLogger();

  private static final String ADD_PAYMENT_PROCESS_ID = "9BED7889E1034FE68BD85D5D16857320";
  private static final String FORMAT_REGISTRY_KEY_FORM_CONTAINER = "org.openbravo.client.application.ParameterWindow_FormContainerLayout_%s";

  private static final String FORMAT_SMART_CLIENT_GRID_FIELD = "%s.getEditForm().getField('%s')";

  protected static final String PARAM_ACTUAL_PAYMENT = "actual_payment";
  private static final String PARAM_DOC_ACTION = "document_action";

  protected static final String PARAM_TRX_TYPE = "transaction_type";
  protected static final String PARAM_OVERPYMT_ACTION = "overpayment_action";

  private static final String FORMAT_REGISTRY_KEY_GRID_ORDER_INVOICE = "org.openbravo.client.application.ParameterWindow_Grid_order_invoice_%s";
  private static final String FORMAT_REGISTRY_KEY_GRID_GLITEM = "org.openbravo.client.application.ParameterWindow_Grid_glitem_%s";
  private static final String FORMAT_REGISTRY_KEY_CREDIT_TO_USE = "org.openbravo.client.application.ParameterWindow_Grid_credit_to_use_%s";

  private static final String FORMAT_REGISTRY_KEY_BUTTON_NEW_GL_ITEM = "org.openbravo.client.application.ParameterWindow_Grid_AddNewglitem_%s";

  protected static final String FIELD_GROUP_ORDER = "0C672A3B7CDF416F9522DF3FA5AE4022";
  protected static final String FIELD_GROUP_GLITEM = "7B6B5F5475634E35A85CF7023165E50B";
  protected static final String FIELD_GROUP_CREDIT = "CB265F2D7ACF439F9FB5EFBFA0B50363";
  protected static final String FIELD_GROUP_TOTALS = "BFFF70E721654110AD5BACF3D4216D3A";

  private AddPaymentGrid gridOrderInvoice;
  private AddPaymentGrid gridGLItem;
  private AddPaymentGrid gridCreditToUse;
  private Button addGlItemButton;
  private Canvas containerLayout;

  private final OBParameterWindowForm form;

  /** Regular expression to match with the message of the payment creation. */
  public static final String REGEXP_MESSAGE_CREATED_PAYMENT = "Created Payment: .*\\. ";
  public static final String REGEXP_MESSAGE_CREATED_PAYMENT2 = "Payment No. .* has been created";
  public static final String REGEXP_MESSAGE_EXECUTED_PAYMENT = "The next payments have been created and executed: .*";
  public static final String REGEXP_MESSAGE_REFUNDED_PAYMENT = "Refunded payment: .*\\. Created Payment: .*\\. ";
  public static final String REGEXP_ERROR_MESSAGE_PERIOD_NOT_OPENED = "<b>The Period does not exist or it is not opened</b>";
  public static final String REGEXP_ERROR_MESSAGE_AMOUNTS_ARE_ZERO = "<b>Error</b><br/>Saving failed. Either deposited amount or payment amount must be different than zero";
  public static final String REGEXP_ERROR_MESSAGE_PAYMENT_AND_GLITEM_ARE_EMPTY = "<b>Error</b><br/>Saving failed. This transaction type must define either a Payment or a G/L Item";
  public static final String REGEXP_ERROR_MESSAGE_GLITEM_AMOUNTS_ARE_ZERO = "<b>Error</b><br/>Received In or Paid Out amounts should be different to zero in GL Item grid";
  public static final String REGEXP_ERROR_MESSAGE_CREDIT_GREATER_THAN_TOTAL = "<b>Error</b><br/>Credit amount should not be greater than total amount";
  public static final String REGEXP_ERROR_MESSAGE_PAYMENT_AMOUNT_IS_ZERO = "<b>Error</b><br/>It is not possible to add a payment of zero amount to a transaction";
  public static final String REGEXP_WARNING_MESSAGE_FINANCIAL_ACCOUNT_INACTIVE = "The default financial account %s is inactive. Please, consider to activate it.";
  public static final String REGEXP_WARNING_MESSAGE_PAYMENT_METHOD_INACTIVE = "The default payment method %s for the financial account Spain Bank - EUR is inactive. Please, consider to activate it.";
  public static final String REGEXP_WARNING_MESSAGE_FINANCIAL_ACCOUNT_AND_PAYMENT_METHOD_INACTIVE = "The default financial account %s and the default payment method %s are inactive. Please, consider to activate them before continuing.";

  /* Fields to assert */
  protected static final String PARAM_PYMT_DOCUMENTNO = "payment_documentno";
  protected static final String PARAM_BP_FROM = "received_from";
  protected static final String PARAM_REFERENCENO = "reference_no";
  protected static final String PARAM_PYMT_METHOD = "fin_paymentmethod_id";
  protected static final String PARAM_CURRENCY = "c_currency_id";
  protected static final String PARAM_CURRENCYTO = "c_currency_to_id";
  protected static final String PARAM_EXPECTED_PAYMENT = "expected_payment";
  protected static final String PARAM_FIN_ACCT = "fin_financial_account_id";
  protected static final String PARAM_CONV_AMT = "converted_amount";
  protected static final String PARAM_CONV_RATE = "conversion_rate";
  protected static final String PARAM_AMTGLITEMS = "amount_gl_items";
  protected static final String PARAM_ORDER_INV = "amount_inv_ords";
  protected static final String PARAM_TOTAL = "total";
  protected static final String PARAM_DIFFERENCE = "difference";
  protected static final String PARAM_DOCACTION = "document_action";

  /* End needed constants to performance display logic by descendent classes */

  public AddPaymentProcess() {
    this(null, ADD_PAYMENT_PROCESS_ID, true);
  }

  public AddPaymentProcess(GeneratedTab<?> tab) {
    this(tab, ADD_PAYMENT_PROCESS_ID, true);
  }

  public AddPaymentProcess(String processDefinitionId) {
    this(null, processDefinitionId, true);
  }

  @SuppressWarnings("unchecked")
  public AddPaymentProcess(GeneratedTab<?> tab, String processDefinitionId, Boolean waitForLoad) {
    super(ADD_PAYMENT_PROCESS_ID, tab);
    this.form = new OBParameterWindowForm(processDefinitionId, waitForLoad);

    containerLayout = new Canvas(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_FORM_CONTAINER, processDefinitionId)), waitForLoad);

    gridOrderInvoice = new AddPaymentGrid(TestRegistry.getObjectString(
        String.format(FORMAT_REGISTRY_KEY_GRID_ORDER_INVOICE, processDefinitionId)));
    gridGLItem = new AddPaymentGrid(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_GRID_GLITEM, processDefinitionId)));
    gridCreditToUse = new AddPaymentGrid(TestRegistry
        .getObjectString(String.format(FORMAT_REGISTRY_KEY_CREDIT_TO_USE, processDefinitionId)));
  }

  protected void editCurrentRecord(AddPaymentGrid grid, DataObject data) {
    // fill the fields
    LinkedHashMap<String, Object> fields = data.getDataFields();
    for (String fieldName : fields.keySet()) {
      InputField field = GridInputFieldFactory.getInputFieldObject(
          String.format(FORMAT_SMART_CLIENT_GRID_FIELD, grid.getObjectString(), fieldName), false);
      field.setValue(fields.get(fieldName));
    }
  }

  @Override
  public Canvas getField(String fieldName) {
    if (form.getField(fieldName) instanceof Canvas) {
      return (Canvas) form.getField(fieldName);
    } else {
      log.error("A non expected class type has been found. Check that class is instance of Canvas");
      throw new NullPointerException();
    }
  }

  @Override
  public SectionItem getFieldSection(String fieldName) {
    return form.getFieldSection(fieldName);
  }

  protected void editGrid(AddPaymentGrid grid, int rowNum, DataObject data) {
    // make first column visible
    grid.scrollToLeft();
    grid.scrollToRight();
    // click on it to start edition
    grid.clickCellByRowNumber(rowNum, data.getNextFieldName());
    editCurrentRecord(grid, data);
  }

  @Override
  public void scrollToBottom() {
    containerLayout.scrollToBottom();
  }

  @Override
  public void scrollToTop() {
    containerLayout.scrollToTop();
  }

  @Override
  public Object getParameterValue(String parameterName) {
    return form.getData(parameterName);
  }

  @Override
  public void setParameterValue(String parameterName, Object value) {
    if (parameterName.equals(PARAM_ACTUAL_PAYMENT)
        && form.getField(parameterName).getValue().equals(value)) {
      log.trace(
          "The value of 'Actual Payment' field already has the value to be set, no further action required."
              + "\n This is set to prevent failure of APRRegression33500In12 and APRRegression33500In2 tests");
      return;
    }
    form.fill(parameterName, value);
    gridOrderInvoice.waitForDataToLoad();
  }

  @Override
  public OBParameterWindowForm getForm() {
    return form;
  }

  @Override
  protected boolean isPopup() {
    return true;
  }

  /**
   * Process a payment.
   *
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   */
  public void process(String payment, String action) {
    setParameterValue(PARAM_ACTUAL_PAYMENT, payment);
    process(action);
  }

  /**
   * Process payment out details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   */
  public void process(String transactionType, String documentNumber, String action) {
    process(transactionType, documentNumber, null, action, null, null);
  }

  /**
   * Process payment details.
   *
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String action, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    process(null, null, null, action, null, addPaymentTotalsVerificationData);
  }

  /**
   * Process payment details.
   *
   * @param action
   *          The process action to request after the payment.
   * @param overpayment
   *          The overpayment action.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String action, String overpayment,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    process(null, null, null, action, overpayment, addPaymentTotalsVerificationData);
  }

  /**
   * Process payment details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String transactionType, String documentNumber, String action,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    process(transactionType, documentNumber, null, action, null, addPaymentTotalsVerificationData);
  }

  /**
   * Process payment details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   * @param overpayment
   *          The overpayment action.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String transactionType, String documentNumber, String action,
      String overpayment, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    process(transactionType, documentNumber, null, action, overpayment,
        addPaymentTotalsVerificationData);
  }

  /**
   * Process payment details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param creditNumber
   *          The number of the credit.
   * @param action
   *          The process action to request after the payment.
   * @param overpayment
   *          The overpayment action.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String transactionType, String documentNumber, String creditNumber,
      String action, String overpayment, AddPaymentPopUpData addPaymentTotalsVerificationData) {
    process(transactionType, null, documentNumber, creditNumber, action, overpayment,
        addPaymentTotalsVerificationData);
  }

  /**
   * Process payment details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param actualPayment
   *          The actual payment.
   * @param documentNumber
   *          The number of the document.
   * @param creditNumber
   *          The number of the credit.
   * @param action
   *          The process action to request after the payment.
   * @param overpayment
   *          The overpayment action.
   * @param addPaymentTotalsVerificationData
   *          Data to verify total fields
   */
  public void process(String transactionType, String actualPayment, String documentNumber,
      String creditNumber, String action, String overpayment,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    editAddPayment(transactionType, actualPayment, documentNumber, creditNumber, overpayment,
        addPaymentTotalsVerificationData);
    process(action);
  }

  public void process(String action) {
    processAddPayment(action, null);
  }

  public void processWithError(String action, String error) {
    processWithError(action, null, error);
  }

  public void processWithError(String action, String overpayment, String error) {
    processWithError(null, null, null, action, overpayment, null, error);
  }

  public void processWithError(String transactionType, String documentNumber, String creditNumber,
      String action, String overpayment, AddPaymentPopUpData addPaymentTotalsVerificationData,
      String error) {
    editAddPayment(transactionType, null, documentNumber, creditNumber, overpayment,
        addPaymentTotalsVerificationData);
    processAddPayment(action, error);
  }

  private void editAddPayment(String transactionType, String actualPayment, String documentNumber,
      String creditNumber, String overpayment,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    String filterField = null;
    if (transactionType != null) {
      setParameterValue(PARAM_TRX_TYPE, transactionType);
      if ("Orders".equals(transactionType)) {
        filterField = "salesOrderNo";
      } else {
        filterField = "invoiceNo";
      }
    }

    if (actualPayment != null) {
      setParameterValue(PARAM_ACTUAL_PAYMENT, actualPayment);
    }

    if (documentNumber != null) {
      // unselect all records before filtering
      Sleep.trySleep(1000); // TODO: sync wait for data
      gridOrderInvoice.unselectAll();
      gridOrderInvoice.filter(filterField, documentNumber);
      Sleep.trySleep(1000); // TODO: sync wait for data
      gridOrderInvoice.waitForDataToLoad();
      gridOrderInvoice.selectRowByNumber(0);
    }

    if (creditNumber != null) {
      // unselect all records before filtering
      Sleep.trySleep(1000); // TODO: sync wait for data
      gridCreditToUse.unselectAll();
      gridCreditToUse.filter("documentNo", creditNumber);
      Sleep.trySleep(1000); // TODO: sync wait for data
      gridCreditToUse.waitForDataToLoad();
      gridCreditToUse.selectRowByNumber(0);
    }

    if (overpayment != null) {
      if (documentNumber == null) {
        Sleep.trySleep(1000); // TODO: sync wait for data
        gridOrderInvoice.unselectAll();
      }
      setParameterValue(PARAM_OVERPYMT_ACTION, overpayment);
    }

    if (addPaymentTotalsVerificationData != null) {
      assertTotalsData(addPaymentTotalsVerificationData);
    }
  }

  private void processAddPayment(String action, String error) {
    scrollToBottom();
    // TODO L1: Reduce the following static sleep if not required
    Sleep.trySleep(1400);
    setParameterValue(PARAM_DOC_ACTION, action);

    if (tab != null) {
      tab.closeMessageIfVisible();
    }

    if (isOkButtonEnabled()) {
      pressOkButton();
    } else {
      // Wait for all displaylogic/onChanges to be executed
      // TODO: Substitute this smart sleep with a fluent wait
      for (int i = 0; i < 20 && !isOkButtonEnabled(); i++) {
        Sleep.trySleep(500);
      }
      if (isOkButtonEnabled()) {
        pressOkButton();
      } else {
        TestCase.fail("DONE button in Add Payment window is not enabled");
      }
    }

    if (error != null) {
      assertThat(this.getMessageContents(), is(equalTo(error)));
    } else {
      this.waitUntilNotVisible();
    }
  }

  public void editOrderInvoiceRecord(int rowNum, DataObject data) {
    editGrid(gridOrderInvoice, rowNum, data);
  }

  public void editCreditToUseRecord(int rowNum, DataObject data) {
    editGrid(gridCreditToUse, rowNum, data);
  }

  public void addGLItem(DataObject data) {
    getForm().getSection(FIELD_GROUP_ORDER).collapse();
    getForm().getSection(FIELD_GROUP_GLITEM).expand();

    if (addGlItemButton == null) {
      addGlItemButton = new Button(TestRegistry.getObjectString(
          String.format(FORMAT_REGISTRY_KEY_BUTTON_NEW_GL_ITEM, processDefinitionId)));
    }
    addGlItemButton.click();
    gridGLItem.scrollToRight();
    gridGLItem.scrollToLeft();

    Sleep.trySleep(3000);
    editCurrentRecord(gridGLItem, data);

  }

  public void removeGLItem(int rowNum) {
    gridGLItem.scrollToLeft();
    gridGLItem.clickCell(rowNum, 0);
  }

  /**
   * The method to be invoked locally to assertData. Assert the data displayed in the add payment in
   * pop up.
   *
   * @param data
   *          The data of the payment.
   */
  private void assertDataProcess(AddPaymentPopUpData data) {

    for (final String key : data.getDataFields().keySet()) {
      log.trace("[assertOnForm] Searching for key: " + key + " with value "
          + data.getDataFields().get(key));
      form.assertField(key, data.getDataFields().get(key));
    }
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertData(AddPaymentPopUpData data) {
    log.debug("Asserting data in add Payment Process");
    assertDataProcess(data);
  }

  /**
   * Assert the total fields displayed in the add payment pop up.
   *
   * @param data
   *          The data of the payment total fields.
   */
  public void assertTotalsData(AddPaymentPopUpData data) {
    log.debug("Asserting totals data in add Payment Process");
    Sleep.trySleep(1000);
    assertDataProcess(data);
  }

  /**
   * Assert the Multicurrency fields displayed in the add payment pop up.
   *
   * @param data
   *          The data of the payment Multicurrency fields.
   */
  public void assertMulticurrencyData(AddPaymentPopUpData data) {
    log.debug("Asserting Multicurrency data in add Payment Process");
    assertDataProcess(data);
  }

  /**
   * @return Return Order/Invoice AddPaymentGrid
   */
  public AddPaymentGrid getOrderInvoiceGrid() {
    return gridOrderInvoice;
  }

  /**
   * @return Return GL Item AddPaymentGrid
   */
  public AddPaymentGrid getGLItemGrid() {
    return gridGLItem;
  }

  /**
   * @return Return Credit AddPaymentGrid
   */
  public AddPaymentGrid getCreditToUseGrid() {
    return gridCreditToUse;
  }

  /**
   * Assert that the number of records of this grid matches with the param recordsNumber
   *
   * @param recordsNumber
   *          Number of expected records in this grid
   * @param grid
   *          The grid where it is going to be checked
   */
  public void assertRecordsNumber(AddPaymentGrid grid, int recordsNumber) {
    int aux = grid.getLastVisibleRowIndex();
    boolean matches = (aux == (recordsNumber - 1));
    if (aux == -1) {
      assertTrue("There are not records in this grid. " + recordsNumber + " expected.", matches);
    } else {
      assertTrue(
          "There are " + (aux + 1) + " records in this grid. " + recordsNumber + " expected.",
          matches);
    }
  }

  /**
   * Assert that there are records in this grid.
   *
   * @param grid
   *          The grid where it is going to be checked
   */
  public void assertExistRecords(AddPaymentGrid grid) {
    int firstVisible = grid.getFirstVisibleRowIndex();
    boolean thereAreRecords = (firstVisible > -1);
    assertTrue("There are not records. " + firstVisible + " code returned.", thereAreRecords);
  }

  /**
   * Assert that there are records in gridOrderInvoice grid.
   */
  public void assertExistRecordsInOrderInvoice() {
    assertExistRecords(gridOrderInvoice);
  }

  /**
   * Assert that the number of records of gridOrderInvoice grid matches with the param recordsNumber
   *
   * @param recordsNumber
   *          Number of expected records in this grid
   */
  public void assertRecordsNumberInOrderInvoice(int recordsNumber) {
    assertRecordsNumber(gridOrderInvoice, recordsNumber);
  }

  public int NumberOfRowsInOrderInvoiceGrid() {
    return NumberOfRowsInGrid(gridOrderInvoice);
  }

  public int NumberOfRowsInGrid(AddPaymentGrid grid) {
    int aux = grid.getLastVisibleRowIndex() + 1;
    log.debug("There are " + aux + " records in this grid.");
    return aux;
  }

  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {

    for (final String key : visibleDisplayLogic.getDataFields().keySet()) {
      if (visibleDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not visible", this.getForm().getSection(key).isVisible());
      } else {
        assertFalse(key + " is visible. It shouldn't", this.getForm().getSection(key).isVisible());
      }
    }
  }

  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {

    for (final String key : enabledDisplayLogic.getDataFields().keySet()) {
      if (enabledDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not enabled", this.getForm().getSection(key).isEnabled());
      } else {
        assertFalse(key + " is enabled. It should't", this.getForm().getSection(key).isEnabled());
      }
    }
  }

  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {

    for (final String key : expandedDisplayLogic.getDataFields().keySet()) {
      if (expandedDisplayLogic.getDataFields().get(key).equals(true)) {
        assertTrue(key + " is not expanded", this.getForm().getSection(key).isExpanded());
      } else {
        assertFalse(key + " is expanded. It shouldn't",
            this.getForm().getSection(key).isExpanded());
      }
    }
  }

  /**
   * This method selects an Order/Invoice record in AddPayment/AddDetails PopUp with the first
   * DataObject and, after that, it is selected certain credit documentNo specified in the second
   * DataObject linking with the first record.
   *
   * @param paymentRecord
   *          Record that is going to be paid
   * @param creditToUse
   *          Record where is placed the credit to use
   */
  public void selectPaymentAndSelectCredit(SelectedPaymentData paymentRecord,
      SelectedPaymentData creditToUse) {
    this.getOrderInvoiceGrid().waitForDataToLoad();
    this.getOrderInvoiceGrid().filter(paymentRecord);
    Sleep.trySleep(2000);
    this.getOrderInvoiceGrid().selectRecord(0);
    this.getCreditToUseGrid().waitForDataToLoad();
    this.getCreditToUseGrid().filter(creditToUse);
    Sleep.trySleep(2000);
    this.getCreditToUseGrid().selectRecord(0);
    this.getField("reference_no").focus();
  }

  /**
   * This method selects a payment record in AddPayment/AddDetails PopUp with Order/Invoice
   * DataObject
   *
   * @param paymentRecord
   *          Record that is going to be selected
   */
  public void selectPayment(SelectedPaymentData paymentRecord) {
    this.getOrderInvoiceGrid().waitForDataToLoad();
    this.getOrderInvoiceGrid().filter(paymentRecord);
    Sleep.trySleep(2000);
    this.getOrderInvoiceGrid().selectRecord(0);
  }

  /**
   * This method selects a GL Item record in AddPayment/AddDetails PopUp with GL Item DataObject
   *
   * @param gLItem
   *          Record that is going to be selected
   */
  public void selectGLItemRecord(SelectedPaymentData gLItem) {
    this.getGLItemGrid().waitForDataToLoad();
    this.getGLItemGrid().filter(gLItem);
    Sleep.trySleep(2000);
    this.getGLItemGrid().selectRecord(0);
  }

  /**
   * This method selects a credit record in AddPayment/AddDetails PopUp with Credit To Use
   * DataObject
   *
   * @param creditToUse
   *          Record that is going to be selected
   */
  public void selectCredit(SelectedPaymentData creditToUse) {
    this.getCreditToUseGrid().waitForDataToLoad();
    this.getCreditToUseGrid().filter(creditToUse);
    Sleep.trySleep(2000);
    this.getCreditToUseGrid().selectRecord(0);
  }

  public void collapseTotals() {
    getForm().getSection(FIELD_GROUP_TOTALS).collapse();
  }

  public void collapseCredit() {
    getForm().getSection(FIELD_GROUP_CREDIT).collapse();
  }

  public void collapseOrder() {
    getForm().getSection(FIELD_GROUP_ORDER).collapse();
  }

  public void expandCredit() {
    getForm().getSection(FIELD_GROUP_CREDIT).expand();
  }

  public void expandOrder() {
    getForm().getSection(FIELD_GROUP_ORDER).expand();
  }
}
