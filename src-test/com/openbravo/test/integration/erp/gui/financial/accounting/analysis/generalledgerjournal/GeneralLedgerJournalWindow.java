/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Luján <plu@openbravo.com>,
 *  David Miguélez <david.miguelez@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.analysis.generalledgerjournal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.client.application.gui.ClassicProcessView;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP General Ledger Journal window.
 *
 * @author elopio
 *
 */
public class GeneralLedgerJournalWindow extends ClassicProcessView {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The window title. */
  public static final String TITLE = "General Ledger Journal";

  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_ANALYSIS, Menu.GENERAL_LEDGER_JOURNAL };

  /** Identifier of the HTML format button. */
  private static final String BUTTON_HTML_FORMAT = "buttonHTML";
  /** Identifier of the data table */
  public static final String TABLE_DATA = "//Table[@class='DataGrid_Header_Table DataGrid_Body_Table']";
  /** Identifier of the journal section. */
  public static final String SECTION_JOURNAL = "sectionJournal";
  /** Identifier of the function calculate order journal span. */
  public static final String SPAN_FUNCTION_CALCULATE_ORDER_JOURNAL = "functionCalculateOrderJournal";

  /** Index of the account number column. */
  private static final int COLUMN_ACCOUNT_NUMBER = 5;
  /** Index of the account name column. */
  private static final int COLUMN_ACCOUNT_NAME = 6;
  /** Index of the credit column. */
  private static final int COLUMN_CREDIT = 7;
  /** Index of the debit column. */
  private static final int COLUMN_DEBIT = 8;

  /** Locator for the journal lines. */
  private static final String LOCATOR_JOURNAL_LINES = "//Table[@class='DataGrid_Header_Table DataGrid_Body_Table']//tr[@id='funcEvenOddRow1xx']";
  /** Format string used to access data from journal. */
  private static final String FORMAT_ACCESS_JOURNAL_DATA = LOCATOR_JOURNAL_LINES + "[%d]/td[%d]";

  /**
   * Class constructor
   */
  public GeneralLedgerJournalWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Class constructor
   *
   * @param title
   *          The title of the Window.
   */
  public GeneralLedgerJournalWindow(String title) {
    super(title, MENU_PATH);
  }

  /**
   * Generate the report in HTML format.
   */
  public void generateHTMLReport() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_HTML_FORMAT).click();
  }

  /**
   * Assert the number of lines in a journal.
   *
   * @param count
   *          The expected number of lines.
   */
  public void assertJournalLinesCount(int count) {
    logger.debug("<performance issues> Verifying that there are {} journal entry lines.", count);
    Instant start = Instant.now();
    assertThat(SeleniumSingleton.INSTANCE.findElementsByXPath(LOCATOR_JOURNAL_LINES).size(),
        is(equalTo(count)));
    // XXX: <performance issues> Adding debug log to check whether the very long loop time is
    // related to certain steps
    logger.debug(
        "<performance issues> <duration> Correctly counted {} journal entry lines in {} ms.", count,
        Duration.between(start, Instant.now()).toMillis());
    // SeleniumSingleton.INSTANCE.switchTo().defaultContent();
  }

  /**
   * Assert data of a general ledger journal line.
   *
   * @param index
   *          The index of the line.
   * @param accountNumber
   *          The number of the account.
   * @param accountName
   *          The name of the account.
   * @param credit
   *          The amount credited.
   * @param debit
   *          The amount of the debit.
   */
  public void assertJournalLine(int index, String accountNumber, String accountName, String credit,
      String debit) {
    SeleniumSingleton.INSTANCE.findElementByXPath(TABLE_DATA);
    assertThat(SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_ACCESS_JOURNAL_DATA, index, COLUMN_ACCOUNT_NUMBER))
        .getText(), is(equalTo(accountNumber)));
    assertThat(SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_ACCESS_JOURNAL_DATA, index, COLUMN_ACCOUNT_NAME))
        .getText(), is(equalTo(accountName)));
    assertThat(SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_ACCESS_JOURNAL_DATA, index, COLUMN_CREDIT))
        .getText(), is(equalTo(credit)));
    assertThat(SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_ACCESS_JOURNAL_DATA, index, COLUMN_DEBIT))
        .getText(), is(equalTo(debit)));
  }

  /**
   * Assert data of an array of general ledger journal lines.
   *
   * @param journalEntryLines
   *          Array of the general ledger journal lines.
   */
  public void assertJournalLines2(String[][] journalEntryLines) {
    // XXX: <performance issues> Adding debug log to check whether the very long loop time is
    // related to certain steps
    Instant start = Instant.now();
    SeleniumSingleton.INSTANCE.findElementByXPath(TABLE_DATA);
    logger.debug("<performance issues> <duration> Journal Entry Lines table data read in {} ms.",
        Duration.between(start, Instant.now()).toMillis());
    start = Instant.now();
    waitForIsJournalLinesAccountNumLoaded();
    logger.debug("<performance issues> <duration> Journal Entry Account Num. loaded in {} ms.",
        Duration.between(start, Instant.now()).toMillis());
    List<WebElement> allJournalEntryLines = SeleniumSingleton.INSTANCE
        .findElementsByXPath(LOCATOR_JOURNAL_LINES);
    int i = 0;
    // XXX: <performance issues> Adding debug log to check whether the very long loop time is
    // related to certain steps
    start = Instant.now();
    for (WebElement singleJournalEntryLine : allJournalEntryLines) {
      // TODO L4: Remove the following static sleep when possible. It is required now
      Sleep.trySleep();
      assertThat(singleJournalEntryLine
          .findElement(By.xpath(String.format("td[%d]", COLUMN_ACCOUNT_NUMBER)))
          .getText(), is(equalTo(journalEntryLines[i][0])));
      assertThat(
          singleJournalEntryLine.findElement(By.xpath(String.format("td[%d]", COLUMN_ACCOUNT_NAME)))
              .getText(),
          is(equalTo(journalEntryLines[i][1])));
      assertThat(
          singleJournalEntryLine.findElement(By.xpath(String.format("td[%d]", COLUMN_CREDIT)))
              .getText(),
          is(equalTo(journalEntryLines[i][2])));
      assertThat(singleJournalEntryLine.findElement(By.xpath(String.format("td[%d]", COLUMN_DEBIT)))
          .getText(), is(equalTo(journalEntryLines[i][3])));
      i++;
    }
    logger.debug("<performance issues> <duration> All Journal Entry lines asserted in {} ms.",
        Duration.between(start, Instant.now()).toMillis());

  }

  /**
   * Checks whether the 'Account No.' field of the general ledger journal lines is loaded
   */
  private void waitForIsJournalLinesAccountNumLoaded() {
    // Polling interval is set to 500ms
    Sleep.setExplicitWait(40000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        List<WebElement> allJournalEntryLines = SeleniumSingleton.INSTANCE
            .findElementsByXPath(LOCATOR_JOURNAL_LINES);
        for (WebElement singleJournalEntryLine : allJournalEntryLines) {
          if (singleJournalEntryLine
              .findElement(By.xpath(String.format("td[%d]", COLUMN_ACCOUNT_NUMBER)))
              .getText()
              .equals("")) {
            return false;
          }
        }
        return true;
      }
    });
  }
}
