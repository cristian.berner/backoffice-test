/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Payment In Lines tab.
 *
 * @author elopio
 *
 */
public class PaymentInLinesTab extends GeneratedTab<PaymentInLinesData> {

  /** The tab title. */
  static final String TITLE = "Lines";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "34DA12C2E9E3424E9A853563BEFDE81F";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Child tab constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public PaymentInLinesTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
