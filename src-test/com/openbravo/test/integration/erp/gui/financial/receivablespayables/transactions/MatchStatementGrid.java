/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.LinkedHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

public class MatchStatementGrid extends Grid {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Index of the edit button. */
  private final static int INDEX_BUTTON_SEARCH = 0;
  /** Index of the open button. */
  private final static int INDEX_BUTTON_ADD = 2;
  /** Index of the cancel edit button. */
  private final static int INDEX_BUTTON_CLEAR = 4;

  private final static String FORMAT_LOCATOR_MATCH_STATEMENT_BUTTON_BY_ROW_NUMBER = "%s.body.children[%d].members[%d]";
  private static final String FORMAT_SMART_CLIENT_GRID_FIELD = "%s.getRecord(%d).%s";

  public MatchStatementGrid(String tabIdentifier) {
    super(tabIdentifier);
  }

  private String getMatchStatementButtonLocatorByRowNumber(int rowIndex, int buttonIndex) {
    return String.format(FORMAT_LOCATOR_MATCH_STATEMENT_BUTTON_BY_ROW_NUMBER, objectString,
        rowIndex * 2, buttonIndex);
  }

  public FindTransactionsToMatchProcess clickSearch(int rowNumber) {
    final String buttonLocator = getMatchStatementButtonLocatorByRowNumber(rowNumber,
        INDEX_BUTTON_SEARCH);
    SeleniumSingleton.INSTANCE.executeScript(buttonLocator + ".click()");
    FindTransactionsToMatchProcess transactionsToMatchProcess = new FindTransactionsToMatchProcess();
    return transactionsToMatchProcess;
  }

  public AddTransactionProcess clickAdd(int rowNumber) {
    final String buttonLocator = getMatchStatementButtonLocatorByRowNumber(rowNumber,
        INDEX_BUTTON_ADD);
    SeleniumSingleton.INSTANCE.executeScript(buttonLocator + ".click()");
    AddTransactionProcess addTransactionProcess = new AddTransactionProcess();
    return addTransactionProcess;
  }

  public void clickClear(int rowNumber) {
    final String buttonLocator = getMatchStatementButtonLocatorByRowNumber(rowNumber,
        INDEX_BUTTON_CLEAR);
    SeleniumSingleton.INSTANCE.executeScript(buttonLocator + ".click()");
  }

  public void assertData(int rowNumber, DataObject data) {
    waitForDataToLoad();
    Sleep.trySleep(5000);
    LinkedHashMap<String, Object> fields = data.getDataFields();
    this.waitUntilLoaded();
    for (String fieldName : fields.keySet()) {
      System.out.println(fieldName);

      this.waitUntilLoaded();
      logger.debug(
          "** Data to retrieve 'field' String -> objectString: {}. rowNumber: {}. fieldName: {}.'field' just before get its value: {}",
          objectString, rowNumber, fieldName, GridInputFieldFactory.getReadFieldObject(
              String.format(FORMAT_SMART_CLIENT_GRID_FIELD, objectString, rowNumber, fieldName)));
      String field = GridInputFieldFactory.getReadFieldObject(
          String.format(FORMAT_SMART_CLIENT_GRID_FIELD, objectString, rowNumber, fieldName));

      if (fields.get(fieldName) instanceof String) {
        String expectedDisplayValue = (String) fields.get(fieldName);
        expectedDisplayValue = getDateFromTag(expectedDisplayValue);
        logger.debug("** It will be checked {} matches with the expected value: {}", field,
            expectedDisplayValue);
        assertThat(field, is(equalTo(expectedDisplayValue)));
      } else {
        logger.debug("** It will be checked {} matches with the expected value: {}", field,
            fields.get(fieldName));
        assertThat(field, is(equalTo(fields.get(fieldName))));
      }

    }
  }

  // This function manipulate certain CONSTANTS to get the Date in the correct moment. This function
  // is adapted to MatchStatementGrid requirements
  private String getDateFromTag(String expectedDisplayValue) {
    String result = null;
    if (expectedDisplayValue.contains("#currentDate#")) {
      result = expectedDisplayValue.replace("#currentDate#", OBDate.getSystemDate());
      return result;
    }
    // Manipulate #addDaysToSystemDate to get the correct OBDate.addDaysToSystemDate(days)
    // avoiding false positives
    else if (expectedDisplayValue.contains("#addDaysToSystemDate")) {
      String strDays = expectedDisplayValue.replaceAll("#addDaysToSystemDate", "");
      strDays = strDays.substring(0, strDays.indexOf('#'));
      int days = Integer.parseInt(strDays);
      result = OBDate.addDaysToSystemDate(days);
      return result;
    } else if (expectedDisplayValue.contains("#get30Days5FromSystemDate#")) {
      result = expectedDisplayValue.replace("#get30Days5FromSystemDate#",
          OBDate.get30Days5FromSystemDate());
      return result;
    } else if (expectedDisplayValue.contains("#deduct1MonthToSystemDate#")) {
      result = expectedDisplayValue.replace("#deduct1MonthToSystemDate#",
          OBDate.addMonthsToSystemDate(-1));
      return result;
    } else if (expectedDisplayValue.contains("#getYearBefore#")) {
      result = expectedDisplayValue.replace("#getYearBefore#", OBDate.getYearBefore());
      return result;
    } else if (expectedDisplayValue.contains("#getCurrentYear#")) {
      result = expectedDisplayValue.replace("#getCurrentYear#", OBDate.getCurrentYear());
      return result;
    } else {
      return expectedDisplayValue;
    }

  }

}
