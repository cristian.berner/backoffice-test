/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.businesspartnertaxcategory.classic;

import com.openbravo.test.integration.erp.data.financial.accounting.setup.businesspartnertaxcategory.BusinessPartnerTaxCategoryData;
import com.openbravo.test.integration.erp.gui.tabs.GeneratedTab;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes and verifies actions on OpenbravoERP Business Partner Tax Category tab.
 *
 * @author elopio
 *
 */
public class BusinessPartnerTaxCategoryTab extends GeneratedTab {

  /** The tab title. */
  static final String TITLE = "Business Partner Tax Category";
  /** The tab level. */
  private static final int LEVEL = 0;

  /* GUI components. */
  /** Identifier of the Business Partner Tax Category tab. */
  public static final String TAB = "tabname800231";

  /** Identifier of the name text field. */
  private static final String TEXT_FIELD_NAME = "Name";

  /**
   * Class constructor.
   *
   */
  public BusinessPartnerTaxCategoryTab() {
    super(TITLE, LEVEL, TAB);
  }

  /**
   * Create a new Business Partner Tax Category.
   *
   * @param data
   *          The data of the Business Partner Tax Category that will be created.
   */
  public void create(BusinessPartnerTaxCategoryData data) {
    clickCreateNewRecord();
    if (data.getDataField("name") != null) {
      SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME)
          .sendKeys(data.getDataField("name").toString());
    }
    clickSave();
  }

}
