/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.accounting.setup.fiscalcalendar;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Fiscal Calendar standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class FiscalCalendarWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Fiscal Calendar";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.ACCOUNTING, Menu.ACCOUNTING_SETUP, Menu.FISCAL_CALENDAR };

  /**
   * Class constructor.
   *
   */
  public FiscalCalendarWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * Load the view.
   */
  @Override
  public void load() {
    CalendarTab calendarTab = new CalendarTab();
    addTopLevelTab(calendarTab);
    super.load();
  }

  /**
   * Select and return the Heartbeat Configuration tab.
   *
   * @return the heartbeat configuration tab.
   */
  public CalendarTab selectCalendarTab() {
    return (CalendarTab) selectTab(CalendarTab.IDENTIFIER);
  }

  /**
   * Select and return the Year tab.
   *
   * @return the year tab.
   */
  public YearTab selectYearTab() {
    return (YearTab) selectTab(YearTab.IDENTIFIER);
  }

  /**
   * Select and return the Period tab.
   *
   * @return the period tab.
   */
  public PeriodTab selectPeriodTab() {
    return (PeriodTab) selectTab(PeriodTab.IDENTIFIER);
  }

}
