/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:12
 * Contributor(s):
 *   David Miguélez <david.miguelez@openbravo.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Reconciliations tab.
 *
 * @author David Miguélez
 *
 */

public class ReconciliationsTab extends GeneratedTab<ReconciliationsLinesData> {

  /** The tab title. */
  static final String TITLE = "Reconciliations";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "FF8080813320657F0133209DE21B0042";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent tab.
   */
  public ReconciliationsTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
