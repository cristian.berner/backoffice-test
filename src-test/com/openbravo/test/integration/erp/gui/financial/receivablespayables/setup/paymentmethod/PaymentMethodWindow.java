/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.financial.receivablespayables.setup.paymentmethod;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Payment Method standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class PaymentMethodWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  private static final String TITLE = "Payment Method";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.FINANCIAL_MANAGEMENT,
      Menu.RECEIVABLES_AND_PAYABLES, Menu.RECEIVABLES_AND_PAYABLES_SETUP, Menu.PAYMENT_METHOD };

  /**
   * Class constructor.
   *
   */
  public PaymentMethodWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    PaymentMethodTab paymentMethodTab = new PaymentMethodTab();
    addTopLevelTab(paymentMethodTab);
    super.load();
  }

  /**
   * Select and return the Payment Method tab.
   *
   * @return the Payment Method tab.
   */
  public PaymentMethodTab selectPaymentMethod() {
    return (PaymentMethodTab) selectTab(PaymentMethodTab.IDENTIFIER);
  }

}
