/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Luján <plu@openbravo.com>,
 *   David Miguélez <david.miguelez@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentDisplayLogic;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.outgoingshipment.ShipmentOutData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Sales Invoice Header tab.
 *
 * @author elopio
 *
 */
public class SalesInvoiceHeaderTab extends GeneratedTab<SalesInvoiceHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "263";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.263";
  /** Registry key of the create lines from order button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_ORDER = "org.openbravo.client.application.toolbar.button.createLinesFromOrder.263";
  /** Registry key of the create lines from Shipment/Receipt button. */
  private static final String REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_SHIPMENT = "org.openbravo.client.application.toolbar.button.createLinesFromShipment.263";
  /** Registry key of the Create Lines From pick and edit */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";
  /** Registry key of the Add Payment button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT = "org.openbravo.client.application.toolbar.button.aPRMAddpayment.263";
  /** Registry key of the post button. */
  private static final String REGISTRY_KEY_BUTTON_POST = "org.openbravo.client.application.toolbar.button.posted.263";
  /** Registry key of the Void button. */
  private static final String REGISTRY_KEY_BUTTON_VOID = "org.openbravo.client.application.toolbar.button.aPRMProcessinvoice.263";

  /** Identifier of the delete accounting entry checkbox */
  protected static final String IDENTIFIER_CHECKBOX_DELETE = "inpEliminar";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The create lines from order button. */
  private static Button buttonCreateLinesFromOrder;
  /** The create lines from Shipmet/Receipt button. */
  private static Button buttonCreateLinesFromShipment;
  /** The add payment button. */
  private static Button buttonAddPaymentInOut;
  /** The add void button. */
  private static Button buttonVoid;

  /**
   * Class constructor.
   *
   */
  public SalesInvoiceHeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new SalesInvoiceLinesTab(this));
    addChildTab(new PaymentInPlanTab(this));
    addChildTab(new ExchangeRatesTab(this));
    addChildTab(new BasicDiscountsTab(this));
    addChildTab(new TaxTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCreateLinesFromOrder = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_ORDER));
    buttonCreateLinesFromShipment = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_SHIPMENT));
    buttonVoid = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_VOID));
  }

  /**
   * Create sales invoice lines from order.
   *
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   */
  public void createLinesFrom(String order) {
    logger.debug("Creating lines from order '{}'.", order);
    buttonCreateLinesFromOrder.click();
    PickAndExecuteWindow<SalesOrderHeaderData> popup = new PickAndExecuteWindow<SalesOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoOrderDateGrandTotal = order.split(" - ");
    popup.filter(new SalesOrderHeaderData.Builder().documentNo(documentNoOrderDateGrandTotal[0])
        .orderDate(documentNoOrderDateGrandTotal[1])
        .grandTotalAmount(documentNoOrderDateGrandTotal[2])
        .build());
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Create lines from shipment.
   *
   * @param shipment
   *          The shipment identifier.
   */
  public void createLinesFromShipment(String shipment) {
    logger.debug("Creating lines from shipment '{}'.", shipment);
    buttonCreateLinesFromShipment.click();
    PickAndExecuteWindow<ShipmentOutData> popup = new PickAndExecuteWindow<ShipmentOutData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoMovementDateBP = shipment.split(" - ");
    popup.filter(new ShipmentOutData.Builder().documentNo(documentNoMovementDateBP[0])
        .movementDate(documentNoMovementDateBP[1])
        .build());
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Create sales invoice line from order line no.
   *
   * @param order
   *          The order data. This is a string with the form: number - date - amount.
   * @param lineNo
   *          The order line No
   */
  public void createLinesFrom(String order, int lineNo) {
    logger.debug("Creating lines from order '{}' and line no '{}'.", order, String.valueOf(lineNo));
    buttonCreateLinesFromOrder.click();
    PickAndExecuteWindow<SalesOrderHeaderData> popup = new PickAndExecuteWindow<SalesOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    String[] documentNoOrderDateGrandTotal = order.split(" - ");
    SalesOrderHeaderData orderData = new SalesOrderHeaderData.Builder()
        .documentNo(documentNoOrderDateGrandTotal[0])
        .orderDate(documentNoOrderDateGrandTotal[1])
        .grandTotalAmount(documentNoOrderDateGrandTotal[2])
        .build();
    orderData.addDataField("lineNo", String.valueOf(lineNo * 10));
    popup.filter(orderData);
    popup.process();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
    waitIfInfoMessageVisible();
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process sales invoice header with action '{}'.", action);
    // XXX workaround issue https://issues.openbravo.com/view.php?id=17409
    buttonComplete.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./SalesInvoice/Header_Edition.html");
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Complete the order.
   *
   */
  public void complete() {
    process("Complete");
    // TODO L1: Check after stabilization if following sleeps is still required
    Sleep.trySleep(7500);
    buttonAddPaymentInOut = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
  }

  /**
   * Void the invoice.
   *
   */
  public void close() {

    buttonVoid.smartClientClick();
    RequestProcessPopUp popUp = new RequestProcessPopUp(
        "org.openbravo.classicpopup./org.openbravo.advpaymentmngt.ad_actionbutton/ProcessInvoice.html");
    popUp.process("Void");
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  @Deprecated
  // This method is deprecated due to non natural way of asserting
  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    buttonAddPaymentInOut.click();
    logger.debug("Opening add payment process popup");
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(data);
    addPaymentProcess.close();
  }

  /**
   * Add a payment and process it.
   *
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String payment, String action) {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.process(payment, action);

    waitUntilMessageVisible();
  }

  /**
   * Open Add Payment popup
   *
   * @return The Add Payment PopUp
   */
  public AddPaymentProcess openAddPayment() {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    // TODO L2: Sleep added to give more time to the application to work properly. 33500In20 was
    // failing
    Sleep.trySleep();
    return addPaymentProcess;
  }

  /**
   * Add payment in details.
   *
   * @param transactionType
   *          The type of the transaction.
   * @param documentNumber
   *          The number of the document.
   * @param action
   *          The process action to request after the payment.
   */
  public void addPayment(String transactionType, String documentNumber, String action) {
    logger.debug("Adding payment out. Transaction type: '{}', Document number: '{}'.",
        transactionType, documentNumber);
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.process(transactionType, documentNumber, action);
  }

  /**
   * Check if the sales invoice is already posted.
   *
   * @return boolean with value to true if its text is equals to "Unpost"
   */
  public boolean isPosted() {
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    logger.debug(
        "Checking if the sales invoice is already posted. Expected label: 'Unpost'. Current label: '{}'.",
        buttonPost.getText());
    return (buttonPost.getText().equals("Unpost"));
  }

  /**
   * Post the sales invoice.
   */
  public void post() {
    logger.debug("Posting the invoice.");
    Button buttonPost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    logger.debug("Button post label: {}", buttonPost::getText);
    buttonPost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./SalesInvoice/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    // XXX: <performance issues> Waiting until the all Journal Entries window are open to prevent
    // overlap and/or interruption by the actions following
    Sleep.setFluentWait(300, 600000).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver driver) {
        String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "document.getElementsByClassName('OBTabBarButtonMainTitleSelected').item(0).textContent");
        String strComp = "Journal Entries Report";
        String strComp2 = " - Main";
        String strComp3 = " - USA U...";
        String strComp4 = " - F&B I...";
        if (strOfJsScript.toLowerCase().contains(strComp2.toLowerCase())) {
          logger.trace("Sleeping for the first 'Main US/A/Euro' Journal Entry tab to load");
          Sleep.trySleep(2000);
        }
        return strOfJsScript.toLowerCase().equals(strComp.toLowerCase())
            || strOfJsScript.toLowerCase().contains(strComp3.toLowerCase())
            || strOfJsScript.toLowerCase().contains(strComp4.toLowerCase());
      }
    });
  }

  /**
   * UnPost the sales invoice.
   */
  public void unpost() {
    logger.debug("Unposting the invoice.");
    Button buttonUnpost = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_POST));
    logger.debug("Button unpost label: {}", buttonUnpost::getText);
    buttonUnpost.click();
    OBClassicPopUp popUp = new OBClassicPopUp(TestRegistry
        .getObjectString("org.openbravo.classicpopup./SalesInvoice/Header_Edition.html")) {
      @Override
      protected void selectFrame() {
        SeleniumSingleton.INSTANCE.switchTo().frame("process");
      }
    };
    popUp.selectPopUpWithOKButton();
    popUp.verifyElementsAreVisible(new String[] { IDENTIFIER_CHECKBOX_DELETE });
    if (!SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).isSelected()) {
      SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_CHECKBOX_DELETE).click();
    }
    popUp.clickOK();
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    Sleep.trySleep();
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param visibleDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose visibility has to be asserted.
   */
  public void assertAddPaymentDisplayLogicVisible(AddPaymentDisplayLogic visibleDisplayLogic) {
    logger.debug("Asserting visible display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicVisible(visibleDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param enabledDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose enable status has to be asserted.
   */
  public void assertAddPaymentDisplayLogicEnabled(AddPaymentDisplayLogic enabledDisplayLogic) {
    logger.debug("Asserting enabled display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicEnabled(enabledDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert display logic from AddPayment Pop up
   *
   * @param expandedDisplayLogic
   *          AddPaymentDisplayLogic with the fields whose expansion has to be asserted.
   */
  public void assertAddPaymentDisplayLogicExpanded(AddPaymentDisplayLogic expandedDisplayLogic) {
    logger.debug("Asserting expanded display logic");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertAddPaymentDisplayLogicExpanded(expandedDisplayLogic);
    addPaymentProcess.close();
    logger.trace("Display logic assert successfully executed");
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully() {
    buttonCreateLinesFromOrder = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CREATE_LINES_FROM_ORDER));
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPaymentCreatedSuccessfully2() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT2);
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertRefundedPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_REFUNDED_PAYMENT);
  }

  /**
   * Assert that the payment creation completed successfully.
   */
  public void assertPeriodDoesNotExistOrItIsNotOpened() {
    assertThat(standardView.getMessageContents(),
        is(equalTo(AddPaymentProcess.REGEXP_ERROR_MESSAGE_PERIOD_NOT_OPENED)));
  }
}
