/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Andy Armaignac <collazoandy4@gmail.com>,
 ************************************************************************
 */
package com.openbravo.test.integration.erp.gui.sales.transactions.returnfromcustomer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInDetailsData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 *
 * @author collazoandy4
 *
 */
public class PaymentInDetailsTab extends GeneratedTab<PaymentInDetailsData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Payment In Details";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "AF4090093DB91431E040007F010048A5";
  /** The tab level. */
  private static final int LEVEL = 2;

  public PaymentInDetailsTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    logger.debug("Creating Return From Customer Payment In Detail Tab");
  }
}
