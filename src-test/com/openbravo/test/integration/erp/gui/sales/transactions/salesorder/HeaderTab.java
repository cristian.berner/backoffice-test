/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 *   Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.salesorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.popups.RequestProcessPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.WarnDialog;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verifies actions on OpenbravoERP Header tab.
 *
 * @author elopio
 *
 */
public class HeaderTab extends GeneratedTab<SalesOrderHeaderData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The tab title. */
  static final String TITLE = "Header";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "186";

  /* Registry keys. */
  /** Registry key of the complete button. */
  private static final String REGISTRY_KEY_BUTTON_COMPLETE = "org.openbravo.client.application.toolbar.button.documentAction.186";
  /** Registry key of the copy record button. */
  private static final String REGISTRY_KEY_BUTTON_CLONE = "org.openbravo.client.application.toolbar.button.clone.186";
  /** Registry key of the copy from orders button. */
  private static final String REGISTRY_KEY_BUTTON_COPY_FROM_ORDERS = "org.openbravo.client.application.toolbar.button.copyFromPO.186";
  /** Registry key of the copy from orders button pop up */
  private static final String REGISTRY_KEY_POPUP_PICKANDEDIT = "org.openbravo.client.application.process.pickandexecute.popup";
  /** Registry key of the Add Payment button. */
  private static final String REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT = "org.openbravo.client.application.toolbar.button.aPRMAddPayment.186";
  /** Registry key of the request process pop up. */
  private static final String REGISTRY_KEY_POP_UP_REQUEST_PROCESS = "org.openbravo.classicpopup./SalesOrder/Header_Edition.html";

  /* Toolbar buttons. */
  /** The complete button. */
  private static Button buttonComplete;
  /** The add payment button. */
  private static Button buttonAddPaymentInOut;
  /** The copy record button */
  private static Button buttonCopyRecord;
  /** The copy record button */
  private static Button buttonCopyFromOrders;

  /**
   * Class constructor.
   *
   */
  public HeaderTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new LinesTab(this));
    addChildTab(new BasicDiscountsTab(this));
    addChildTab(new TaxTab(this));
    addChildTab(new PaymentInPlanTab(this));
    buttonComplete = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COMPLETE));
    buttonCopyRecord = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CLONE));
    buttonCopyFromOrders = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_COPY_FROM_ORDERS));
  }

  /**
   * Process the order.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action) {
    logger.debug("Process sales order header with action '{}'.", action);
    buttonComplete.click();
    RequestProcessPopUp popUp = new RequestProcessPopUp(REGISTRY_KEY_POP_UP_REQUEST_PROCESS);
    popUp.process(action);
    SeleniumSingleton.INSTANCE.switchTo().defaultContent();
    waitUntilMessageVisible();
  }

  /**
   * Book the order.
   *
   */
  public void book() {
    process("Book");
  }

  public void assertPaymentCreatedSuccessfully() {
    standardView.assertProcessCompletedSuccessfullyContentMatch(
        AddPaymentProcess.REGEXP_MESSAGE_CREATED_PAYMENT);
  }

  /**
   * Close the order.
   *
   */
  public void close() {
    process("Close");
  }

  /**
   * Reactivate the order.
   *
   */
  public void reactivate() {
    process("Reactivate");
  }

  /**
   * Add a payment and process it.
   *
   * @param account
   *          The financial account.
   * @param payment
   *          The payment amount.
   * @param action
   *          The process action to request after the payment.
   * @param addPaymentPopUpData
   *          The verification data.
   */
  public void addPayment(String account, String payment, String action,
      AddPaymentPopUpData addPaymentPopUpData) {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    // TODO L1: Reduce the following static sleep if not required
    Sleep.trySleep(4000);
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    if (account != null) {
      addPaymentProcess.setParameterValue("fin_financial_account_id", account);
    }
    if (payment != null) {
      addPaymentProcess.setParameterValue("actual_payment", payment);
      // TODO L1: Reduce the following static sleep if not required
      Sleep.trySleep();
      addPaymentProcess.getField("reference_no").focus();
      // TODO L1: Reduce the following static sleep if not required
      Sleep.trySleep(1000);
    }
    if (addPaymentPopUpData != null) {
      addPaymentProcess.assertData(addPaymentPopUpData);
    }
    // TODO L1: Reduce the following static sleep if not required
    Sleep.trySleep(1350);
    addPaymentProcess.process(action);

    waitUntilMessageVisible();
  }

  /**
   * Open Add Payment popup
   *
   * @return The Add Payment PopUp
   */
  public AddPaymentProcess openAddPayment() {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();

    AddPaymentProcess addPaymentProcess = new AddPaymentProcess(this);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    return addPaymentProcess;
  }

  /**
   * Assert the data displayed in the add payment in pop up.
   *
   * @param data
   *          The data of the payment.
   */
  public void assertAddPaymentInData(AddPaymentPopUpData data) {
    logger.debug("Adding payment.");
    if (buttonAddPaymentInOut == null) {
      buttonAddPaymentInOut = new Button(
          TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_ADD_PAYMENT_IN_OUT));
    }
    buttonAddPaymentInOut.click();
    logger.debug("Opening add payment process popup");
    AddPaymentProcess addPaymentProcess = new AddPaymentProcess();
    addPaymentProcess.assertData(data);
    addPaymentProcess.close();
  }

  public void copyRecord() {
    buttonCopyRecord.click();

    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickYes();
  }

  public PickAndExecuteWindow<SalesOrderHeaderData> copyFromOrders() {
    buttonCopyFromOrders.click();
    PickAndExecuteWindow<SalesOrderHeaderData> popup = new PickAndExecuteWindow<SalesOrderHeaderData>(
        TestRegistry.getObjectString(REGISTRY_KEY_POPUP_PICKANDEDIT));
    return popup;
  }
}
