/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.generateinvoices;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on Generate Invoices Pop Up.
 *
 * @author elopio
 *
 */
public class GenerateInvoicesPopUp extends OBClassicPopUp {

  /** The pop up menu path. */
  public static final String[] MENU_PATH = new String[] { Menu.SALES_MANAGEMENT,
      Menu.SALES_MANAGEMENT_TRANSACTIONS, Menu.GENERATE_INVOICES };

  /* Component identifiers. */
  /** The identifier of the date invoiced text field. */
  private static final String IDENTIFIER_TEXT_FIELD_DATE_INVOICED = "DateInvoiced";
  /** The identifier of the organization combo box. */
  private static final String IDENTIFIER_COMBO_BOX_ORGANIZATION = "reportAD_Org_ID_S";

  /**
   * Class constructor.
   *
   */
  public GenerateInvoicesPopUp() {
    super(TestRegistry.getObjectString(
        "org.openbravo.classicpopup./ad_actionButton/ActionButton_Responser.html?Command=BUTTON119"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame("119");
  }

  /**
   * Generate invoices.
   *
   * @param invoiceDate
   *          The date of the invoices.
   * @param organization
   *          The name of the organization.
   */
  public void generateInvoices(String invoiceDate, String organization) {
    selectPopUpWithOKButton();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_TEXT_FIELD_DATE_INVOICED)
        .sendKeys(invoiceDate);
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_COMBO_BOX_ORGANIZATION))
        .selectByVisibleText(organization);
    clickOK();
    Sleep.trySleep();
    clickClose();
  }
}
