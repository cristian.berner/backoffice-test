/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.goodsshipment;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Goods Shipment standard window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class GoodsShipmentWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  public static final String TITLE = "Goods Shipment";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.SALES_MANAGEMENT,
      Menu.SALES_MANAGEMENT_TRANSACTIONS, Menu.GOODS_SHIPMENT };

  /**
   * Class constructor.
   */
  public GoodsShipmentWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    GoodsShipmentHeaderTab headerTab = new GoodsShipmentHeaderTab();
    addTopLevelTab(headerTab);
    super.load();
  }

  /**
   * Select and return the Header tab.
   *
   * @return the Header tab.
   */
  public GoodsShipmentHeaderTab selectHeaderTab() {
    return (GoodsShipmentHeaderTab) selectTab(GoodsShipmentHeaderTab.IDENTIFIER);
  }

  /**
   * Select and return the Lines tab.
   *
   * @return the Header tab.
   */
  public GoodsShipmentLinesTab selectLinesTab() {
    return (GoodsShipmentLinesTab) selectTab(GoodsShipmentLinesTab.IDENTIFIER);
  }
}
