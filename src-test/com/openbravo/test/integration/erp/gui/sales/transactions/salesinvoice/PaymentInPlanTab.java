/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;

/**
 * Executes and verifies actions on OpenbravoERP Payment In Plan tab.
 *
 * @author elopio
 *
 */
public class PaymentInPlanTab extends GeneratedTab<PaymentPlanData> {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /** The tab title. */
  static final String TITLE = "Payment Plan";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "60825C9E68644DBC9C530DDCABE05A6E";
  /** The tab level. */
  private static final int LEVEL = 1;
  /** Registry key of the Modify Payment Plan button. */
  private static final String REGISTRY_KEY_BUTTON_MODIFY_PAYMENT_PLAN = "org.openbravo.client.application.toolbar.button.aPRMModifyPaymentInPlan.263";
  /** The Modify Payment Plan button. */
  private static Button buttonModifyPaymentPlan;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public PaymentInPlanTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
    addChildTab(new PaymentInDetailsTab(this));
  }

  /**
   * Modify a payment it.
   */
  public void modifyPaymentPlan() {
    logger.debug("Modifying payment plan.");
    buttonModifyPaymentPlan = new Button(
        TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_MODIFY_PAYMENT_PLAN));
    buttonModifyPaymentPlan.click();
    // TODO: Verify if that way of calling the Pick and Execute window is the best according design
  }

  @Override
  public void clearFilters() {
    if (isOnFormView()) {
      switchToGridView();
    }
    super.clearFilters();
  }

}
