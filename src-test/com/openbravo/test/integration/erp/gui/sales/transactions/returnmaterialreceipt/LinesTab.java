/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Andy Armaignac <collazoandy4@gmail.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.gui.sales.transactions.returnmaterialreceipt;

import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptLinesData;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;

/**
 * Executes and verifies actions on OpenbravoERP Lines tab.
 *
 * @author collazoandy4
 *
 */
public class LinesTab extends GeneratedTab<ReturnMaterialReceiptLinesData> {

  /** The tab title. */
  static final String TITLE = "Lines";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  public static final String IDENTIFIER = "CF6D6C7B85EB473CB88E757E14D1D022";
  /** The tab level. */
  private static final int LEVEL = 1;

  /**
   * Class constructor.
   *
   * @param parentTab
   *          The parent of the tab.
   */
  public LinesTab(GeneratedTab<?> parentTab) {
    super(TITLE, IDENTIFIER, LEVEL, parentTab);
  }

}
