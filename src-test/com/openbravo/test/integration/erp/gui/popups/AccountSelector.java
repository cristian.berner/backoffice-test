/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Executes actions on Account selector PopUp
 *
 * @author elopio
 *
 */
public class AccountSelector extends SelectorPopUp {

  /** Identifier of the account button */
  public static final String BUTTON_ACCOUNT = "buttonAccount";

  /** Locator of the account combination text field */
  private static final String TEXT_FIELD_ACCOUNT_COMBINATION = "inpCombination";

  /** Index of the alias column */
  private static final int COLUMN_ACCOUNT_COMBINATION = 2;

  /**
   * Class constructor
   */
  public AccountSelector() {
    super();
  }

  /**
   * Select the first account
   */
  public void selectAccount() {
    selectPopUp();
    Tables.clickCell(TABLE_RESULTS, 0, 1);
    clickOK();
  }

  /**
   * Select an account
   *
   * @param accountCombination
   *          the name of the cell to be selected
   */
  public void selectAccount(String accountCombination) {
    selectPopUp();
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_ACCOUNT_COMBINATION)
        .sendKeys(accountCombination);
    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_ACCOUNT_COMBINATION, accountCombination);

    clickOK();
  }

}
