/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Arun kumar<arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Executes and verifies on Openbravo ERP Product Selector popup.
 *
 * @author arun
 *
 */
public class ProductSelector extends SelectorPopUp {

  /** Identifier of the select product button. */
  public static final String BUTTON_SELECT_PRODUCT = "buttonProduct";
  // TODO see issue 11540 - https://issues.openbravo.com/view.php?id=11540.
  /** Identifier of the select product button. */
  public static final String BUTTON_SELECT_PRODUCT_2 = "buttonProductComplete";

  /** Identifier of the Key text field. */
  private static final String TEXT_FIELD_KEY = "paramKey";
  /** Identifier of the Name text field. */
  private static final String TEXT_FIELD_NAME = "paramName";

  /** Identifier of the price list combo box. */
  private static final String COMBO_BOX_PRICE_LIST = "sectionPriceList";

  /** Index of the key column. */
  private static final int COLUMN_KEY = 1;

  /**
   * Class constructor.
   *
   */
  public ProductSelector() {
    super();
  }

  /**
   * Select a product.
   *
   * @param key
   *          The key of the product that will be selected.
   */
  public void selectProduct(String key) {
    selectPopUp();

    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_KEY).sendKeys(key);
    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_KEY, key);

    clickOK();
  }

  /**
   * Select a product.
   *
   * @param Name
   *          The Name of the product that will be selected.
   */

  public void selectProductInProcessPlan(String Name) {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_SELECT_PRODUCT_2).click();
    selectPopUp();
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_NAME).sendKeys(Name);
    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_KEY, Name);
    clickOK();
  }

  /**
   * Select a product.
   *
   * @param key
   *          The key of the product that will be selected.
   * @param priceListVersion
   *          The version of the products price list.
   */
  public void selectProduct(String key, String priceListVersion) {
    selectPopUp();

    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_KEY).sendKeys(key);
    new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_PRICE_LIST))
        .selectByVisibleText(priceListVersion);

    clickSearch();
    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_KEY, key);

    clickOK();
  }

}
