/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on Address selector PopUp opened with Smartclient.
 *
 * @author caristu
 *
 */
public class AddressSelectorPopUpSc extends OBClassicPopUp {

  /** Identifier of the address button. */
  public static final String BUTTON_ADDRESS = "buttonLocation";

  /** Identifier of the 1st line text field. */
  private static final String TEXT_FIELD_FIRST_LINE = "inpAddress1";
  /** Identifier of the 2nd line text field. */
  private static final String TEXT_FIELD_SECOND_LINE = "inpAddress2";
  /** Identifier of the postal code text field. */
  private static final String TEXT_FIELD_POSTAL_CODE = "inpPostal";
  /** Identifier of the city text field. */
  private static final String TEXT_FIELD_CITY = "inpCity";
  /** Identifier of the country combo box. */
  private static final String COMBO_BOX_COUNTRY = "inpCCountryId";
  /** Identifier of the region combo box. */
  private static final String COMBO_BOX_REGION = "inpCRegionId";

  /**
   * Class constructor.
   *
   */
  public AddressSelectorPopUpSc() {
    super(TestRegistry.getObjectString("org.openbravo.classicpopup./info/Location_FS.html"));
  }

  /**
   * Select the pop up frame.
   */
  @Override
  protected void selectFrame() {
    SeleniumSingleton.INSTANCE.switchTo().frame(0);
    SeleniumSingleton.INSTANCE.switchTo().frame("superior");
  }

  /**
   * Fill the pop up.
   *
   * @param data
   *          The location data.
   */
  @SuppressWarnings("unlikely-arg-type")
  public void fill(LocationSelectorData data) {
    if (data.getFirstLine() != null) {
      SeleniumSingleton.INSTANCE.findElementByName(TEXT_FIELD_FIRST_LINE)
          .sendKeys(data.getFirstLine());
    }
    if (data.getSecondLine() != null) {
      SeleniumSingleton.INSTANCE.findElementByName(TEXT_FIELD_SECOND_LINE)
          .sendKeys(data.getSecondLine());
    }
    if (data.getPostalCode() != null) {
      SeleniumSingleton.INSTANCE.findElementByName(TEXT_FIELD_POSTAL_CODE)
          .sendKeys(data.getPostalCode());
    }
    if (data.getCity() != null) {
      SeleniumSingleton.INSTANCE.findElementByName(TEXT_FIELD_CITY).sendKeys(data.getCity());
    }
    if (data.getCountry() != null) {
      Select comboBoxCountry = new Select(
          SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_COUNTRY));
      if (!comboBoxCountry.getFirstSelectedOption().equals(data.getCountry())) {
        comboBoxCountry.selectByVisibleText(data.getCountry());
      }
    }
    if (data.getRegion() != null) {
      new Select(SeleniumSingleton.INSTANCE.findElementById(COMBO_BOX_REGION))
          .selectByVisibleText(data.getRegion());
    }
  }

  /**
   * Select an Address.
   *
   * @param location
   *          The address location to select.
   */
  public void selectAddress(LocationSelectorData location) {
    selectPopUp(BUTTON_OK);
    fill(location);
    clickOK();
  }
}
