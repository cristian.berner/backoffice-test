/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.openqa.selenium.support.ui.Select;

import com.openbravo.test.integration.erp.modules.client.application.gui.OBClassicPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes actions on the Sales Order Request Process pop up.
 *
 * @author elopio
 *
 */
public class RequestProcessPopUp extends OBClassicPopUp {

  /* Pop up components. */
  /** Identifier of the action combo box. */
  protected static final String IDENTIFIER_COMBO_BOX_ACTION = "reportdocaction";
  protected static final String IDENTIFIER_PAYMENT_COMBO_BOX_ACTION = "reportaction_S";
  protected static final String IDENTIFIER_PAYMENT_COMBO_BOX_DATE = "paymentDate";
  protected static final String IDENTIFIER_RECEIPT_COMBO_BOX_PRICELIST = "reportM_PriceList_Version_ID_S";
  protected static final String IDENTIFIER_PARAM_VOIDED_DOCUMENT_DATE = "paramVoidedDocumentDate";
  protected static final String IDENTIFIER_PARAM_VOIDED_DOCUMENT_ACCT_DATE = "paramVoidedDocumentAcctDate";
  static String DATE_FORMAT = ConfigurationProperties.INSTANCE.getDateFormat();

  /**
   * Class constructor.
   *
   * @param registryKey
   *          The pop up key in the Openbravo Test Registry array.
   */
  public RequestProcessPopUp(String registryKey) {
    super(TestRegistry.getObjectString(registryKey));
  }

  /**
   * Process an action.
   *
   * @param action
   *          The action to process.
   */
  public void process(String action) {
    process(action, IDENTIFIER_COMBO_BOX_ACTION);
  }

  /**
   * Process a payment with an action.
   *
   * @param action
   *          The action to process.
   */
  public void processPayment(String action) {
    process(action, IDENTIFIER_PAYMENT_COMBO_BOX_ACTION);
  }

  /**
   * Generate Invoice from Receipt with a price list version.
   *
   * @param priceListVersion
   *          The price list version to process.
   */
  public void processReceipt(String priceListVersion) {
    process(priceListVersion, IDENTIFIER_RECEIPT_COMBO_BOX_PRICELIST);
  }

  /**
   * Process a payment with an action.
   *
   * @param action
   *          The action to process.
   * @param date
   *          The date in the paymentDate field
   */
  public void reversePayment(String action, String date) {
    selectPopUpWithOKButton();
    SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_PAYMENT_COMBO_BOX_DATE).sendKeys(date);
    new Select(SeleniumSingleton.INSTANCE.findElementById(IDENTIFIER_PAYMENT_COMBO_BOX_ACTION))
        .selectByVisibleText(action);
    clickOK();
  }

  /**
   * Process a document with an action.
   *
   * @param action
   *          The action to process.
   */
  private void process(String action, String comboId) {
    selectPopUpWithOKButton();
    new Select(SeleniumSingleton.INSTANCE.findElementById(comboId)).selectByVisibleText(action);
    if (action.equals("Void")) {
      final Calendar calendar = Calendar.getInstance();
      String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
      SeleniumSingleton.INSTANCE.executeScript("document.getElementById('"
          + IDENTIFIER_PARAM_VOIDED_DOCUMENT_DATE + "').value ='" + currentDate + "'", "");
      SeleniumSingleton.INSTANCE.executeScript("document.getElementById('"
          + IDENTIFIER_PARAM_VOIDED_DOCUMENT_DATE + "').setAttribute('displayformat','DD-MM-YYYY')",
          "");
      SeleniumSingleton.INSTANCE.executeScript("document.getElementById('"
          + IDENTIFIER_PARAM_VOIDED_DOCUMENT_ACCT_DATE + "').value ='" + currentDate + "'", "");
      SeleniumSingleton.INSTANCE
          .executeScript("document.getElementById('" + IDENTIFIER_PARAM_VOIDED_DOCUMENT_ACCT_DATE
              + "').setAttribute('displayformat','DD-MM-YYYY')", "");
    }
    clickOK();
    Sleep.trySleep();
  }

}
