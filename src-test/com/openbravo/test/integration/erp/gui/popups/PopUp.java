/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Class that represents a PopUp on Openbravo ERP
 *
 * @author elopio
 *
 */
public class PopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Name of the PopUp */
  protected static final String POPUP_NAME = "BUTTON";

  /** Identifier of the ok link */
  protected static final String LINK_BUTTON_OK = "linkButtonOk";
  /** Identifier of the ok button */
  protected static final String BUTTON_OK = "buttonOK";

  /** Identifier fo the ok button */
  protected static final String BUTTON_OK_2 = "tdButtonOk";

  /** The ok button. */
  WebElement buttonOk;

  /**
   * Class constructor
   */
  public PopUp() {
  }

  /**
   * Select a PopUp
   *
   * @param name
   *          name of the PopUp Window
   */
  protected void selectPopUp(String name) {
    SeleniumSingleton.INSTANCE.switchTo().window(name);
  }

  /**
   * Select the pop up window.
   *
   * @param name
   *          The name of the pop up window.
   */
  protected void selectWindow(String name) {
    SeleniumSingleton.INSTANCE.switchTo().window(name);
  }

  /**
   * Select a PopUp This is an alternate way, that waits for an element Use it only if the
   * selectPopUp doesn't work
   *
   * @param name
   *          name of the PopUp Window
   * @param elementLocator
   *          the locator of the element to wait
   */
  protected void selectPopUp(String name, final String elementLocator) {
    selectWindow(name);
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for element {} to be loaded.", elementLocator);
        try {
          SeleniumSingleton.INSTANCE.findElementById(elementLocator);
        } catch (NoSuchElementException exception) {
          return false;
        }
        return true;
      }
    });
  }

  /**
   * Select a Pop Up with ok button. This function waits until the ok button is present.
   */
  public void selectPopUpWithOKButton() {
    SeleniumSingleton.INSTANCE.switchTo().window(POPUP_NAME);
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for button OK to be loaded.");
        try {
          buttonOk = SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK);
        } catch (NoSuchElementException exception) {
          try {
            buttonOk = SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK_2);
          } catch (NoSuchElementException exception2) {
            try {
              buttonOk = SeleniumSingleton.INSTANCE.findElementById(LINK_BUTTON_OK);
            } catch (NoSuchElementException exception3) {
              return false;
            }
          }
        }
        return true;
      }
    });
  }

  /**
   * Select Product Filter PopUp
   */
  public void selectPopUp() {
    selectPopUpWithOKButton();
  }

  /**
   * Click OK button and wait for the page to load
   */
  public void clickOK() {
    buttonOk.click();
  }
}
