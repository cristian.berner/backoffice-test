/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Class that represents a PopUp on Openbravo ERP
 *
 * @author elopio
 *
 */
public class SelectorPopUp extends PopUp {

  // TODO: Unnecessary @SuppressWarnings("hiding")
  /** Name of the popup */
  @SuppressWarnings("hiding")
  protected String POPUP_NAME = "SELECTOR";

  /** Identifier of the search button */
  private static final String BUTTON_SEARCH = "buttonSearch";

  /**
   * Locator of the table displayed on grid view. TODO see bug 11201:
   * https://issues.openbravo.com/view.php?id=11201 when they resolve the bug, this has to be
   * replaced by the identifier
   */
  protected static final String TABLE_RESULTS = "//table[@class='DataGrid_Body_Table']";

  /**
   * Class constructor
   */
  public SelectorPopUp() {
    super();
  }

  /**
   * Select a Selector PopUp
   *
   * @param name
   *          name of the Selector PopUp
   */
  @Override
  protected void selectPopUp(String name) {
    selectPopUp(name, BUTTON_OK);
    waitForTableToLoad();
  }

  /**
   * Select Account Selector PopUp
   */
  @Override
  public void selectPopUp() {
    selectPopUp(POPUP_NAME);
  }

  /**
   * Click the search button
   */
  protected void clickSearch() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_SEARCH).click();
    waitForTableToLoad();
  }

  /**
   * Wait until the table loads
   */
  private void waitForTableToLoad() {
    /*
     * XXX avoid fixed times new Wait() { public boolean until() { return
     * (table[0][0].backgroundcolor != gris || table[0][0].text != ""); }
     * }.wait("table not loaded");
     */
    Sleep.trySleep();
  }

  /**
   * Click the ok button.
   */
  @Override
  public void clickOK() {
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_OK).click();
    Sleep.trySleep(1000);
  }
}
