/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo LujÃ¡n <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.openqa.selenium.NoSuchElementException;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on Attribute Set PopUp
 *
 * @author elopio
 *
 */
public class AttributeSetPopUp extends SelectorPopUp {

  /** Identifier of the attribute set button */
  public static final String BUTTON_ATTRIBUTE_SET = "buttonAttributeSetInstance";

  /**
   * Class constructor
   */
  public AttributeSetPopUp() {
    super();
  }

  /**
   * Set the attribute of the product
   *
   * @param value
   *          The value of the attribute
   */
  public void setAttribute(String value) {
    selectPopUp(POPUP_NAME, BUTTON_OK);
    // TODO see issue 11536 -
    // https://issues.openbravo.com/view.php?id=11536.
    // XXX attributes vary, and when it's a text field it has no id.
    try {
      SeleniumSingleton.INSTANCE.findElementByXPath("//input[@type='text']").sendKeys(value);
    } catch (NoSuchElementException e) {
      SeleniumSingleton.INSTANCE.findElementByXPath("//select ").sendKeys(value);
    }
    clickOK();
    Sleep.trySleep();
  }
}
