/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.gui.popups;

import org.openqa.selenium.WebElement;

import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Tables;

/**
 * Executes actions on Payment selector PopUp
 *
 * @author elopio
 *
 */
public class PaymentSelector extends SelectorPopUp {

  /** Identifier of the payment button */
  public static final String BUTTON_PAYMENT = "buttonDebtPayment";

  /**
   * Identifier of the business partner button.
   */
  private static final String BUTTON_BUSINESS_PARTNER = "buttonBusinessPartner";
  /** Identifier of the date planned from text field */
  private static final String TEXT_FIELD_DATE_PLANNED_FROM = "paramDateFrom";
  /** Identifier of the date planned to text field */
  private static final String TEXT_FIELD_DATE_PLANNED_TO = "paramDateTo";
  /** Identifier of the receipt check box */
  private static final String CHECK_BOX_RECEIPT = "inpIsReceipt";

  /** Index of the invoice column */
  private static final int COLUMN_INVOICE = 3;

  /**
   * Class constructor
   */
  public PaymentSelector() {
    super();
  }

  /**
   * Select a payment
   *
   * @param businessPartnerKey
   *          The key of the business partner
   * @param receipt
   *          Indicates if a receipt is required
   * @param datePlannedFrom
   *          Start date of the payments that will be searched
   * @param datePlannedTo
   *          End date of the payments that will be searched
   * @param invoice
   *          The number of the invoice to pay
   */
  public void selectPayment(String businessPartnerKey, boolean receipt, String datePlannedFrom,
      String datePlannedTo, String invoice) {
    selectPopUp(POPUP_NAME, BUTTON_OK);
    SeleniumSingleton.INSTANCE.findElementById(BUTTON_BUSINESS_PARTNER).click();
    // XXX this business partner selector has a different name
    final BusinessPartnerSelector businessPartnerSelector = new BusinessPartnerSelector(
        "SELECTOR_BUSINESS");
    businessPartnerSelector.selectBusinessPartner(businessPartnerKey);
    SeleniumSingleton.INSTANCE.switchTo().window(POPUP_NAME);
    WebElement checkBoxRecipt = SeleniumSingleton.INSTANCE.findElementById(CHECK_BOX_RECEIPT);
    if (checkBoxRecipt.isSelected() != receipt) {
      checkBoxRecipt.click();
    }
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_DATE_PLANNED_FROM)
        .sendKeys(datePlannedFrom);
    SeleniumSingleton.INSTANCE.findElementById(TEXT_FIELD_DATE_PLANNED_TO).sendKeys(datePlannedTo);
    clickSearch();

    Tables.clickCellWithText(TABLE_RESULTS, COLUMN_INVOICE, invoice);
    clickOK();
  }

}
