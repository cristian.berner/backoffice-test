/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostCostData;
import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.landedcost.LandedCostReceiptData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocatorSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ShipmentReceiptLineSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.LandedCost;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Order to invoice.
 *
 * @author Pablo Luján
 */
@RunWith(Parameterized.class)
public class PROe_OrderToLandedCost extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [PROe010] Create purchase order. */
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the purchase order header creation. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  /** The purchase order line data. */
  PurchaseOrderLinesData purchaseOrderLinesData;
  /** The data to verify the purchase order line creation. */
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  /** The booked purchase order verification data. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData;

  /* Data for [PROe020] Create Goods Receipt. */
  /** The goods receipt header data. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  /** The data to verify the goods receipt header creation. */
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  /** The data to verify the goods receipt lines creation. */
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  /** The data to verify the goods receipt header after completion. */
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  /* Data for [PROe030] Create Landed Cost. */
  /** The purchase invoice header data. */
  LandedCostHeaderData landedCostHeaderData;
  /** The data to verify the purchase invoice header creation. */
  LandedCostCostData landedCostCostData;
  /** The data to verify the purchase invoice header after completion. */
  LandedCostReceiptData landedCostReceiptData;

  /**
   * Class constructor.
   *
   * @param purchaseOrderHeaderData
   *          The purchase order header data.
   * @param purchaseOrderHeaderVerficationData
   *          The data to verify the purchase order header creation.
   * @param purchaseOrderLinesData
   *          The purchase order line data.
   * @param purchaseOrderLinesVerificationData
   *          The data to verify the purchase order line creation.
   * @param bookedPurchaseOrderHeaderVerificationData
   *          The booked purchase order verification data.
   * @param purchaseOrderPaymentOutPlanData
   *          The data to verify the payment out plan.
   * @param goodsReceiptHeaderData
   *          The goods receipt header data.
   * @param goodsReceiptHeaderVerificationData
   *          The data to verify the goods receipt header lines creation.
   * @param goodsReceiptLinesVerificationData
   *          The data to verify the goods receipt lines creation.
   * @param completedGoodsReceiptHeaderVerificationData
   *          The data to verify the goods receipt header after verification.
   */
  public PROe_OrderToLandedCost(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      LandedCostHeaderData landedCostHeaderData, LandedCostCostData landedCostCostData,
      LandedCostReceiptData landedCostReceiptData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.purchaseOrderPaymentOutPlanData = purchaseOrderPaymentOutPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.landedCostHeaderData = landedCostHeaderData;
    this.landedCostCostData = landedCostCostData;
    this.landedCostReceiptData = landedCostReceiptData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToInvoiceValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [PROa010] Create purchase order.
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().value("Costing Business partner").build())
            .build(),
        new PurchaseOrderHeaderData.Builder().partnerAddress("., Street")
            .warehouse("CostingWarehouse")
            .priceList("Costing Price list")
            .paymentMethod("1 (USA)")
            .paymentTerms("90 days")
            .documentStatus("Draft")
            .currency("USD")
            .build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("costingproduct").build())
            .orderedQuantity("1")
            .build(),
        new PurchaseOrderLinesData.Builder().unitPrice("50.00")
            .listPrice("50.00")
            .uOM("Unit")
            .tax("Costing Tax Rate")
            .lineNetAmount("50.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().summedLineAmount("50.00")
            .grandTotalAmount("50.00")
            .documentStatus("Booked")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .paymentMethod("1 (USA)")
            .expected("50.00")
            .paid("0.00")
            .outstanding("50.00")
            .numberOfPayments("0")
            .currency("USD")
            .build(),
        // Parameters for [PROa020] Create Goods Receipt.
        new GoodsReceiptHeaderData.Builder().organization("USA")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().value("Costing Business partner").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Draft").build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("costingproduct").build())
            .storageBin(new LocatorSelectorData.Builder().alias("COS").build())
            .movementQuantity("1")
            .uOM("Ud")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),
        // Parameters for [PROa030] Create Landed Cost.
        new LandedCostHeaderData.Builder().organization("USA").documentType("Landed Cost").build(),
        new LandedCostCostData.Builder().landedCostType("Landed Cost - Cost")
            .landedCostType("test 2")
            .build(),
        new LandedCostReceiptData.Builder().build() // All
                                                    // data
                                                    // is
                                                    // added
                                                    // on
                                                    // runtime
        } });
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void PROa_createPurchaseOrderAndInvoice() {
    logger.info("** Start of test case [PROe010] Create Purchase Order. **");
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    DataObject bookedPurchaseOrderHeaderData = purchaseOrder.getData();
    PurchaseOrder.PaymentOutPlan purchaseOrderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(
        mainPage);
    purchaseOrderPaymentOutPlan.assertCount(1);
    purchaseOrderPaymentOutPlan.assertData(purchaseOrderPaymentOutPlanData);
    logger.info("** End of test case [PROe010] Create Purchase Order. **");

    logger.info("** Start of test case [PROe020] Create Goods Receipt. **");
    final GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    String goodsReceiptNumber = (String) goodsReceipt.getData("documentNo");
    // TODO when the fields in status bar are automated, we can take the grand total amount from the
    // same object.
    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        bookedPurchaseOrderHeaderData.getDataField("documentNo"),
        bookedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    goodsReceipt.createLinesFrom("COS", purchaseOrderIdentifier);
    goodsReceipt.assertProcessCompletedSuccessfully();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);
    goodsReceipt.complete();
    // Workaround for issue 20118
    for (int i = 0; i < 20 && !(goodsReceipt.getData("documentStatus")).equals("Completed"); i++) {
      Sleep.trySleep(200);
    }

    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);
    logger.info("** End of test case [PROe020] Create Goods Receipt. **");

    logger.info("** Start of test case [PROe030] Create Landed Cost. **");
    final LandedCost landedCost = new LandedCost(mainPage).open();
    landedCost.create(landedCostHeaderData);
    landedCost.assertSaved();
    LandedCost.Cost landedCostCost = landedCost.new Cost(mainPage);
    landedCostCost.create(landedCostCostData);
    LandedCost.Receipt landedCostReceipt = landedCost.new Receipt(mainPage);
    landedCostReceiptData.addDataField("goodsShipmentLine",
        new ShipmentReceiptLineSelectorData.Builder().documentNo(goodsReceiptNumber));
    landedCostReceipt.create(landedCostReceiptData);
    landedCost.process();
    logger.info("** End of test case [PROe030] Create Landed Cost. **");
  }
}
