/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration.HeartbeatConfiguration;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.heartbeatconfiguration.HeartbeatConfigurationTabScript;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.modulemanagement.ModuleManagement;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the second part of Setup Instance flow of the smoke test suite.
 *
 * @author Leo Arias
 */
public class ADMbc_SetupInstance extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test constructor.
   */
  public ADMbc_SetupInstance() {
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test the setup instance flow.
   */
  @Test
  public void instanceShouldBeSetUp() {
    mainPage.verifyProfessionalLogo();
    logger.info("** End of test case [ADMb030] Activate Instance. **");
    ModuleManagement.InstalledModules.enableAllModules(mainPage);

    logger.info("** Start of test case [ADMb040] Enable Heartbeat. **");
    if (mainPage.isOnNewLayout()) {
      HeartbeatConfigurationTabScript heartbeatConfigurationTabScript = new HeartbeatConfigurationTabScript(
          mainPage).open();
      if (heartbeatConfigurationTabScript.enableHeartbeat()) {
        heartbeatConfigurationTabScript.assertEnabled();
      }
    } else {
      HeartbeatConfiguration.HeartbeatConfigurationTab.enableHeartbeat(mainPage);
      HeartbeatConfiguration.HeartbeatConfigurationTab.verifyEnabled(mainPage);
      // TODO Verify that only "Disable Heartbeat" button is shown.

    }
    // TODO Verify Heartbeat Log.
    logger.info("** End of test case [ADMb040] Enable Heartbeat. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }

}
