/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.sales;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.CreateShipmentsFromOrders;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GenerateInvoices;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test invoice generation.
 *
 * @author Leo Arias
 */
@RunWith(Parameterized.class)
public class SALc_GenerateInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [SALc010] Create Sales Orders. */
  /** The first sales order header data. */
  SalesOrderHeaderData firstSalesOrderHeaderData;
  /** The first sales order lines data. */
  SalesOrderLinesData firstSalesOrderLinesData;
  /** The second sales order header data. */
  SalesOrderHeaderData secondSalesOrderHeaderData;
  /** The first sales order lines data. */
  SalesOrderLinesData secondSalesOrderLinesData;

  /**
   * Class constructor.
   *
   * @param firstSalesOrderHeaderData
   *          The first sales order header data
   * @param firstSalesOrderLinesData
   *          The first sales order lines data.
   * @param secondSalesOrderHeaderData
   *          The second sales order header data
   * @param secondSalesOrderLinesData
   *          The second sales order lines data.
   */
  public SALc_GenerateInvoice(SalesOrderHeaderData firstSalesOrderHeaderData,
      SalesOrderLinesData firstSalesOrderLinesData, SalesOrderHeaderData secondSalesOrderHeaderData,
      SalesOrderLinesData secondSalesOrderLinesData) {
    this.firstSalesOrderHeaderData = firstSalesOrderHeaderData;
    this.firstSalesOrderLinesData = firstSalesOrderLinesData;
    this.secondSalesOrderHeaderData = secondSalesOrderHeaderData;
    this.secondSalesOrderLinesData = secondSalesOrderLinesData;
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [SALc010] Create Sales Orders. */
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .orderDate(OBDate.DEDUCT_1_MONTH_TO_SYSTEM_DATE)
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("FGA").build())
            .orderedQuantity("75.25")
            .build(),
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .orderDate(OBDate.DEDUCT_1_MONTH_TO_SYSTEM_DATE)
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .build(),
        new SalesOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("FGB").build())
            .orderedQuantity("75.25")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and shipments from that order.
   *
   * @throws ParseException
   */
  @Test
  public void shipmentsShouldBeCreatedFromOrder() throws ParseException {
    logger.info("** Start of test case [SALc010] Create Sales Orders. **");
    int customerAOrderCount = 3;
    int customerBOrderCount = 3;
    String[] salesOrdersNumbers = new String[customerAOrderCount + customerBOrderCount - 1];
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    for (int index = 0; index < customerAOrderCount; index++) {
      salesOrder.create(firstSalesOrderHeaderData);
      salesOrder.assertSaved();
      // TODO L1: Remove the following static sleep once it has been stabilized
      Sleep.trySleep();
      salesOrderLines.create(firstSalesOrderLinesData);
      salesOrderLines.assertSaved();
      salesOrder.book();
      salesOrder.assertProcessCompletedSuccessfully2();
      salesOrdersNumbers[index] = (String) salesOrder.getData().getDataField("documentNo");
    }
    for (int index = 0; index < customerBOrderCount; index++) {
      salesOrder.create(secondSalesOrderHeaderData);
      salesOrder.assertSaved();
      // TODO L1: Remove the following static sleep once it has been stabilized
      Sleep.trySleep();
      salesOrderLines.create(secondSalesOrderLinesData);
      salesOrderLines.assertSaved();
      salesOrder.book();
      salesOrder.assertProcessCompletedSuccessfully2();
      if (index != customerBOrderCount - 1) {
        salesOrdersNumbers[index + customerAOrderCount] = (String) salesOrder.getData()
            .getDataField("documentNo");
      }
    }
    // TODO L: Decrease the following logger.info to logger.debug once the random error has
    // disappeared
    for (int i = 0; i < salesOrdersNumbers.length; i++) {
      logger.info("***The Sales Order with Document No. has been created.***",
          salesOrdersNumbers[i]);
    }
    CreateShipmentsFromOrders createShipmentsFromOrder = new CreateShipmentsFromOrders(mainPage)
        .open();
    createShipmentsFromOrder.create(salesOrdersNumbers);
    createShipmentsFromOrder.assertProcessCompletedSuccessfully(salesOrdersNumbers);
    logger.info("** Start of test case [SALc020] Generate invoices. **");
    GenerateInvoices generateInvoices = new GenerateInvoices(mainPage).open();
    generateInvoices.generateInvoices(OBDate.getLastDayOfCurrentMonth(), "Spain");
    // TODO L1: Remove static sleep. It has been created due to Gecko inserting and problems closing
    // the popup
    Sleep.trySleep(10000);
    logger.info("** End of test case [SALc020] Generate invoices. **");
  }

  @AfterClass
  public static void tearDown() {
    // OpenbravoERPTest.forceLoginRequired();
  }
}
