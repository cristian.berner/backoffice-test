/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.LocationAddressData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.businesspartnercategory.BusinessPartnerCategoryData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.invoiceschedule.InvoiceScheduleData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartnersetup.paymentterm.PaymentTermHeaderData;
import com.openbravo.test.integration.erp.data.selectors.LocationSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartnersetup.businesspartnercategory.BusinessPartnerCategory;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartnersetup.invoiceschedule.InvoiceSchedule;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartnersetup.paymentterm.PaymentTerm;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class ADMf_SetupBusinessPartner extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PaymentTermHeaderData paymentTermHeader30d5Data;
  PaymentTermHeaderData paymentTermHeader90dData;

  InvoiceScheduleData invoiceScheduleData;

  BusinessPartnerCategoryData businessPartnerCategoryData;
  // AccountingData accountingData;

  BusinessPartnerData businessPartnerData;
  CustomerData businessPartnerCustomerData;
  LocationAddressData businessPartnerLocationData;

  /**
   * Class constructor.
   */
  public ADMf_SetupBusinessPartner(PaymentTermHeaderData paymentTermHeader30d5Data,
      PaymentTermHeaderData paymentTermHeader90dData, InvoiceScheduleData invoiceScheduleData,
      BusinessPartnerCategoryData businessPartnerCategoryData, // AccountingData accountingData,
      BusinessPartnerData businessPartnerData, CustomerData businessPartnerCustomerData,
      LocationAddressData businessPartnerLocationData) {

    this.paymentTermHeader30d5Data = paymentTermHeader30d5Data;
    this.paymentTermHeader90dData = paymentTermHeader90dData;
    this.invoiceScheduleData = invoiceScheduleData;
    this.businessPartnerCategoryData = businessPartnerCategoryData;
    // this.accountingData = accountingData;
    this.businessPartnerData = businessPartnerData;
    this.businessPartnerCustomerData = businessPartnerCustomerData;
    this.businessPartnerLocationData = businessPartnerLocationData;

    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getClientAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getClientAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Create Payment Term 30d/5
        new PaymentTermHeaderData.Builder().searchKey("30d/5")
            .name("30 days, 5")
            .fixedDueDate(true)
            .maturityDate1("5")
            .offsetMonthDue("1")
            .build(),

        // Create Payment Term 90d
        new PaymentTermHeaderData.Builder().searchKey("90d")
            .name("90 days")
            .fixedDueDate(false)
            .offsetMonthDue("3")
            .build(),

        // Create Invoice Schedule
        new InvoiceScheduleData.Builder().name("Schedule Monthly")
            .invoiceFrequency("Monthly")
            .dayOfTheMonth("1")
            .invoiceCutOffDay("31")
            .build(),

        // Create Business Partner Category
        new BusinessPartnerCategoryData.Builder().searchKey("WHS").name("Wholesale").build(),

        // Create Business Partner
        new BusinessPartnerData.Builder().searchKey("Gold House")
            .name("Oragadam Gold House")
            .businessPartnerCategory("Wholesale")
            .build(),

        new CustomerData.Builder().priceList("Based_pricelist")
            .invoiceTerms("Immediate")
            .paymentTerms("90 days")
            .build(),

        new LocationAddressData.Builder()
            .locationAddress(new LocationSelectorData.Builder().firstLine("Sample street 234")
                .city("Pamplona")
                .country("Spain")
                .region("NAVARRA")
                .build())
            .shipToAddress(true)
            .invoiceToAddress(true)
            .build()

        } });
  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void businessPartnerShouldBeSetUp() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [ADMf010]Create payment term **");
    PaymentTerm.Header.create(mainPage, paymentTermHeader30d5Data);
    PaymentTerm.Header.create(mainPage, paymentTermHeader90dData);
    logger.info("** End of test case [ADMf010]Create payment term **");

    logger.info("** Start of test case [ADMf020] Create Invoice Schedule **");
    InvoiceSchedule.InvoiceScheduleTab.create(mainPage, invoiceScheduleData);
    logger.info("** End of test case [ADMf020] Create Invoice Schedule **");

    logger.info("** Start of test case [ADMf030] Create Business partner category **");
    BusinessPartnerCategory.BusinessPartnerCategoryTab.create(mainPage,
        businessPartnerCategoryData);
    // TODO Verify accounting tab.
    // BusinessPartnerCategory.Accounting.verify(mainPage, accountingData);
    logger.info("** End of test case [ADMf030] Create Business partner category **");

    logger.info("** Start of test case [ADMf040] Create Business partner **");
    BusinessPartner.BusinessPartnerTab.create(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, businessPartnerCustomerData);
    BusinessPartner.Location.create(mainPage, businessPartnerLocationData);
    logger.info("** End of test case [ADMf040] Create Business partner **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
