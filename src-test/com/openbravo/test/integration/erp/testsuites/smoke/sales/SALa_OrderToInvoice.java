/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.sales;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesinvoice.SalesInvoiceWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences.SessionPreferences;
import com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest.ProcessRequest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test to create a sales order, and a goods receipt and sales invoice from that order.
 *
 * @author Leo Arias
 */
@RunWith(Parameterized.class)
public class SALa_OrderToInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [SALa010] Create Sales Order. */
  /** The sales order header data. */
  SalesOrderHeaderData salesOrderHeaderData;
  /** The data to verify the creation of the sales order header. */
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  /** The sales order lines data. */
  SalesOrderLinesData salesOrderLinesData;
  /** The data to verify the creation of the sales order line. */
  SalesOrderLinesData salesOrderLinesVerificationData;
  /** The booked sales order header verification data. */
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  /** The taxes data. */
  TaxData[] taxesData;

  /* Data for [SALa020] Create Good Shipment. */
  /** The Goods Shipment header data. */
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  /** The data to verify the creation of the goods shipment header. */
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  /** The data to edit the goods shipment line. */
  GoodsShipmentLinesData goodsShipmentLineEditData;
  /** The data to verify the edition of the goods shipment line. */
  GoodsShipmentLinesData goodsShipmentLineVerificationData;
  /** The data to verify the completion of the goods shipment. */
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;

  /* Data for [SALa030] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;

  /**
   * Class constructor.
   *
   * @param salesOrderHeaderData
   *          The sales order header data.
   * @param salesOrderHeaderVerficationData
   *          The data to verify the creation of the sales order header.
   * @param salesOrderLinesData
   *          The sales order lines data.
   * @param salesOrderLinesVerificationData
   *          The data to verify the creation of the sales order line.
   * @param bookedSalesOrderHeaderVerificationData
   *          The booked sales order header verification data.
   * @param taxesData
   *          The tax data.
   * @param goodsShipmentHeaderData
   *          The Goods Shipment header data.
   * @param goodsShipmentHeaderVerificationData
   *          The data to verify the creation of the goods shipment header.
   * @param goodsShipmentLineEditData
   *          The data to edit the goods shipment line.
   * @param goodsShipmentLineVerificationData
   *          The data to verify the edition of the goods shipment line.
   * @param completedGoodsShipmentHeaderVerificationData
   *          The data to verify the completion of the goods shipment.
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param salesInvoiceLineData
   *          The data to verify the creation of the Sales Invoice lines data.
   * @param paymentInPlanData
   *          The data to verify the payment in plan.
   * @param journalEntryLines
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   */
  public SALa_OrderToInvoice(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData, TaxData[] taxesData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLineEditData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData, PaymentPlanData paymentInPlanData,
      String[][] journalEntryLines) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.taxesData = taxesData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLineEditData = goodsShipmentLineEditData;
    this.goodsShipmentLineVerificationData = goodsShipmentLineVerificationData;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.paymentInPlanData = paymentInPlanData;
    this.journalEntryLines = journalEntryLines;
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [SALa010] Create Sales Order. */
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .invoiceTerms("Customer Schedule After Delivery")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("11.2").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("22.40")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("22.40")
            .grandTotalAmount("23.18")
            .currency("EUR")
            .build(),
        new TaxData[] {
            new TaxData.Builder().tax("VAT(3) - VAT 3%")
                .taxAmount("0.67")
                .taxableAmount("22.40")
                .build(),
            new TaxData.Builder().tax("CHARGE 0.5% - VAT 3%")
                .taxAmount("0.11")
                .taxableAmount("22.40")
                .build() },
        /* Parameters for [SALa020] Create Good Shipment. */
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().searchKey("FGA")
                .storageBin("spain111")
                .build())
            .build(),
        new GoodsShipmentLinesData.Builder().attributeSetValue("L56")
            .storageBin("spain111")
            .movementQuantity("11.2")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),
        /* Parameters for [SALa030] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("23.18")
            .documentStatus("Completed")
            .summedLineAmount("22.40")
            .grandTotalAmount("23.18")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("11.2")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("22.40")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("23.18")
            .received("0.00")
            .outstanding("23.18")
            .build(),
        new String[][] { { "43000", "Clientes (euros)", "23.18", "" },
            { "70000", "Ventas de mercaderías", "", "22.40" },
            { "47700", "Hacienda Pública IVA repercutido", "", "0.78" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void goodsReceiptAndSalesInvoiceShouldBeCreatedFromOrder() throws ParseException {
    logger.info("** Start of test case [SALa010] Create Sales Order. **");
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    DataObject completedSalesOrderHeaderData = salesOrder.getData();
    SalesOrder.Tax tax = salesOrder.new Tax(mainPage);
    tax.assertCount(taxesData.length);
    for (TaxData taxData : taxesData) {
      tax.select(taxData);
      tax.assertData(taxData);
    }
    logger.info("** End of test case [SALa010] Create Sales Order. **");

    logger.info("** Start of test case [SALa020] Create Good Shipment. **");
    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    // TODO when the fields in status bar are automated, we can take the grand total amount from the
    // same object.
    String salesOrderIdentifier = String.format("%s - %s - %s",
        completedSalesOrderHeaderData.getDataField("documentNo"),
        completedSalesOrderHeaderData.getDataField("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    goodsShipment.createLinesFrom("spain111", salesOrderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(1);
    goodsShipmentLines.edit(goodsShipmentLineEditData);
    goodsShipmentLines.assertSaved();
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);
    logger.info("** End of test case [SALa020] Create Good Shipment. **");

    logger.info("** Start of test case [SALa030] Create Sales Invoice. **");
    ProcessRequest processRequest = new ProcessRequest(mainPage).open();
    processRequest.select(
        new ProcessRequestData.Builder().process("Acct Server Process").organization("*").build());
    // TODO L1: Remove Sleep.trySleep(15000);
    Sleep.trySleep(15000);
    if (processRequest.unscheduleProcess()) {
      processRequest.assertProcessUnscheduleSucceeded();
    }
    // TODO L1: Remove Sleep.trySleep();
    Sleep.trySleep();
    SessionPreferences sessionPreferences = new SessionPreferences(mainPage).open();
    sessionPreferences.checkShowAccountingTabs();
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.assertCount(1);
    salesInvoiceLines.assertData(salesInvoiceLineData);
    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);
    salesInvoice.post();
    // salesInvoice.assertProcessCompletedSuccessfully();
    // XXX This is required because currently we can use only one tab at a time.
    mainPage.closeView(SalesInvoiceWindow.TITLE);
    mainPage.loadView(new PostWindow());
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    for (int i = 0; i < journalEntryLines.length; i++) {
      post.assertJournalLine(i + 1, journalEntryLines[i][0], journalEntryLines[i][1],
          journalEntryLines[i][2], journalEntryLines[i][3]);
    }
    logger.info("** End of test case [SALa030] Create Sales Invoice. **");
  }

  @AfterClass
  public static void tearDown() {
    // OpenbravoERPTest.forceLoginRequired();
  }

}
