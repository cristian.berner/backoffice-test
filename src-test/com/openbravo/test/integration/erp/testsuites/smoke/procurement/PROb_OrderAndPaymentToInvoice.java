/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.util.OBDate;

/**
 * Executes the Order and Payment to Invoice suite.
 *
 * @author Pablo Luján
 */
@RunWith(Parameterized.class)
public class PROb_OrderAndPaymentToInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [PROb010] Create purchase order. */
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the purchase order header creation. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerificationData;
  /** The purchase order lines data. */
  PurchaseOrderLinesData purchaseOrderLinesData;
  /** The data to verify the purchase order lines creation. */
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  /** The data to verify the purchase order header after booking. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderData;
  /** The data to verify the payment out plan. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData;

  /* Data for [PROb020] Create payment out. */
  /** The payment out header data. */
  PaymentOutHeaderData paymentOutHeaderData;
  /** The data to verify the payment out header creation. */
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  /** The data to verify the payment out lines creation. */
  PaymentOutLinesData paymentOutLinesVerificationData;

  /* Data for [PROb030] Create Purchase Invoice. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The data of the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData;
  /** The data to verify the payment out plan. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData;
  /** The data to verify the payment out details. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData purchaseInvoicePaymentDetailsData;

  /**
   * Class constructor.
   *
   * @param purchaseOrderHeaderData
   *          The purchase order header data.
   * @param purchaseOrderHeaderVerificationData
   *          The data to verify the purchase order header creation.
   * @param purchaseOrderLinesData
   *          The purchase order lines data.
   * @param purchaseOrderLinesVerificationData
   *          The data to verify the purchase order lines creation.
   * @param bookedPurchaseOrderHeaderData
   *          The data to verify the purchase order header after booking.
   * @param purchaseOrderPaymentOutPlanData
   *          The data to verify the payment out plan data.
   * @param paymentOutHeaderData
   *          The payment out header data.
   * @param paymentOutHeaderVerificationData
   *          The data to verify the payment out header creation.
   * @param paymentOutLinesVerificationData
   *          The data to verify the payment out lines creation.
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header data.
   * @param completedPurchaseInvoiceHeaderData
   *          The data of the completed purchase invoice header.
   * @param purchaseInvoicePaymentOutPlanData
   *          The data to verify the payment out plan data.
   * @param purchaseInvoicePaymentDetailsData
   *          The data to verify the payment out details.
   */
  public PROb_OrderAndPaymentToInvoice(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerificationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData,
      PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData purchaseInvoicePaymentDetailsData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerificationData = purchaseOrderHeaderVerificationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderData = bookedPurchaseOrderHeaderData;
    this.purchaseOrderPaymentOutPlanData = purchaseOrderPaymentOutPlanData;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderData = completedPurchaseInvoiceHeaderData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.purchaseInvoicePaymentDetailsData = purchaseInvoicePaymentDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToInvoiceValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [PROb010] Create purchase order. */
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .documentStatus("Draft")
            .currency("EUR")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("11.2")
            .build(),
        new PurchaseOrderLinesData.Builder().unitPrice("2.00")
            .listPrice("2.00")
            .uOM("Bag")
            .tax("VAT 10%")
            .lineNetAmount("22.40")
            .build(),
        new PurchaseOrderHeaderData.Builder().summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Booked")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .paymentMethod("1 (Spain)")
            .expected("24.64")
            .paid("0.00")
            .outstanding("24.64")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        // Parameters for [PROb020] Create payment out.
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PaymentOutHeaderData.Builder().amount("24.64")
            .status("Payment Made")
            .usedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("24.64")
            .expected("24.64")
            .paid("24.64")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().paymentComplete(true)
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("24.64")
            .paid("24.64")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .paid("24.64")
            .build() } });

  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void PROb_CreatePurchaseOrderPaymentAndInvoice() {
    logger.info("** Start of test case [PROb010] Create Purchase Order. **");
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerificationData);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderData);
    DataObject fullPurchaseOrderHeaderData = purchaseOrder.getData();
    PurchaseOrder.PaymentOutPlan purchaseOrderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(
        mainPage);
    purchaseOrderPaymentOutPlan.assertCount(1);
    purchaseOrderPaymentOutPlan.assertData(purchaseOrderPaymentOutPlanData);
    logger.info("** End of test case [PROb010] Create Purchase Order. **");

    logger.info("** Start of test case [PROb0020] Create payment out. **");
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertAddPaymentOutData("Vendor A", "0.00", "Invoices");
    paymentOut.addDetails("Orders",
        fullPurchaseOrderHeaderData.getDataField("documentNo").toString(),
        "Process Made Payment(s)");
    paymentOut.assertPaymentCreatedSuccessfully();
    paymentOut.assertData((PaymentOutHeaderData) paymentOutHeaderVerificationData.addDataField(
        "description",
        String.format("Order No.: %s\n", fullPurchaseOrderHeaderData.getDataField("documentNo"))));
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLines.assertData(paymentOutLinesVerificationData);
    logger.info("** End of test case [PROb0020] Create payment out. **");

    logger.info("** Start of test case [PROb0030] Create Purchase Invoice. **");
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        fullPurchaseOrderHeaderData.getDataField("documentNo"),
        fullPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderData.getDataField("grandTotalAmount"));
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderData);
    final PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(1);
    purchaseInvoicePaymentOutDetails
        .assertData((PaymentDetailsData) purchaseInvoicePaymentDetailsData);
    logger.info("** End of test case [PROb0030] Create Purchase Invoice. **");
  }
}
