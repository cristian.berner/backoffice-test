/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Suite to test dev modules in production
 *
 * @author jarmendariz
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ ADMia_UnmarkDevelopmentModuleInProduction.class,
    ADMib_ProductionInstanceCannotHaveDevModules.class })
public class ADMi_DevelopmentModulesInProduction {
  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
    OpenbravoERPTest.seleniumStarted = false;
    SeleniumSingleton.INSTANCE.quit();
  }
}
