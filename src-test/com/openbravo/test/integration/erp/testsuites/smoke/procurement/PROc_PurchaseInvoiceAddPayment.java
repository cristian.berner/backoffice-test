/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Lujan <pablo.lujan@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.testsuites.smoke.procurement;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Execute the Setup Client and Organization flow of the smoke test suite.
 *
 * @author Pablo Lujan
 */
@RunWith(Parameterized.class)
public class PROc_PurchaseInvoiceAddPayment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /* Data for [PROc010] Create Purchase Invoice */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The purchase invoice lines data. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  /** The data to verify the purchase invoice lines creation. */
  PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData;
  /** The data to verify the completed purchase invoice header. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData;
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the payed purchase invoice header. */
  PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderData;
  /** The data to verify the payment out plan. */
  PaymentPlanData purchaseInvoicePaymentOutPlanData;
  /** The data to verify the payment out details. */
  PaymentDetailsData purchaseInvoicePaymentDetailsData;

  /**
   * Class constructor.
   *
   * @param purchaseInvoiceHeaderData
   *          The purchase invoice header data.
   * @param purchaseInvoiceHeaderVerificationData
   *          The data to verify the purchase invoice header.
   * @param purchaseInvoiceLinesData
   *          The purchase invoice lines data.
   * @param purchaseInvoiceLinesVerificationData
   *          The data to verify the purchase invoice lines creation.
   * @param completedPurchaseInvoiceHeaderData
   *          The data to verify the completed purchase invoice header.
   * @param addPaymentInVerificationData
   *          The data to verify the data in the add payment pop up.
   * @param payedPurchaseInvoiceHeaderData
   *          The data to verify the payed purchase invoice header.
   * @param purchaseInvoicePaymentOutPlanData
   *          The data to verify the payment out plan data.
   * @param purchaseInvoicePaymentDetailsData
   *          The data to verify the payment out details.
   */
  public PROc_PurchaseInvoiceAddPayment(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderData,
      AddPaymentPopUpData addPaymentInVerificationData,
      PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderData,
      PaymentPlanData purchaseInvoicePaymentOutPlanData,
      PaymentDetailsData purchaseInvoicePaymentDetailsData) {
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceLinesVerificationData = purchaseInvoiceLinesVerificationData;
    this.completedPurchaseInvoiceHeaderData = completedPurchaseInvoiceHeaderData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.payedPurchaseInvoiceHeaderData = payedPurchaseInvoiceHeaderData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.purchaseInvoicePaymentDetailsData = purchaseInvoicePaymentDetailsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseInvoiceAddPaymentValues() throws IOException {
    return Arrays.asList(new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("11.2")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .listPrice("2.00")
            .unitPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("22.40")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("24.64")
            .daysTillDue("90")
            .dueAmount("0.00")
            .paymentComplete(false)
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("EUR - Spain Bank")
            .c_currency_id("EUR")
            .expected_payment("24.64")
            .actual_payment("24.64")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().paymentComplete(true)
            .summedLineAmount("22.40")
            .grandTotalAmount("24.64")
            .documentStatus("Completed")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("24.64")
            .paid("24.64")
            .outstanding("0.00")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .paid("24.64")
            .build()

        } });

  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void PROc_CreatePurchaseInvoice() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [PROc0010] Create Purchase Invoice and add Payment **");
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLinesVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderData);
    purchaseInvoice.addPayment("Process Made Payment(s)");

    // TODO see issue https://issues.openbravo.com/view.php?id=17291
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice.assertData(payedPurchaseInvoiceHeaderData);
    final PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(1);
    purchaseInvoicePaymentOutDetails
        .assertData((PaymentDetailsData) purchaseInvoicePaymentDetailsData);
    logger.info("** End of test case [PROc0010] Create Purchase Invoice and add Payment **");
  }
}
