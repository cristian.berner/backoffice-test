/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.testsuites.smoke.masterdata;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.applicationdictionary.module.ModuleData;
import com.openbravo.test.integration.erp.testscripts.applicationdictionary.module.Module;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test that once a instance its set as production, all development modules should be unmarked
 *
 * @author jarmendariz
 */
@RunWith(Parameterized.class)
public class ADMia_UnmarkDevelopmentModuleInProduction extends ADMi_ActivateInstanceBaseTest {

  public ADMia_UnmarkDevelopmentModuleInProduction(LogInData logInData) {
    this.logInData = logInData;
  }

  @Parameters
  public static Collection<LogInData[]> parameters() {
    return Arrays.asList(new LogInData[][] { { new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build() } });
  }

  @Test
  public void testModuleIsNotInDevelopmentInProduction() {
    changeRoleToSystemAdmin();
    setupInstanceAsTesting();
    setCoreModuleAsInDevelopment();
    setupInstanceAsProduction();

    verifyCoreModuleIsNotInDevelopment();
    setupInstanceAsTesting();
  }

  private void verifyCoreModuleIsNotInDevelopment() {
    Module module = new Module(mainPage).open();
    Sleep.trySleep();
    module.select(new ModuleData.Builder().name("Core").build());
    Boolean inDevelopment = (Boolean) module.getData().getDataField("inDevelopment");

    assertFalse("Core module should not be in development", inDevelopment);
  }

  private void setCoreModuleAsInDevelopment() {
    Module module = new Module(mainPage).open();
    module.select(new ModuleData.Builder().name("Core").build());
    Boolean inDevelopment = (Boolean) module.getData().getDataField("inDevelopment");
    if (!inDevelopment) {
      module.edit(new ModuleData.Builder().inDevelopment(Boolean.TRUE).build());
    }
  }
}
