/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2013 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.roletest.testsuites;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test to create a sales order, and a goods receipt and sales invoice from that order.
 *
 * @author Pandeeswari Ramakrishnan
 */
@RunWith(Parameterized.class)
public class ROL_userA_SALe_CloseSalesOrder extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [SALa010] Create Sales Order. */
  /** The sales order header data. */
  SalesOrderHeaderData salesOrderHeaderData;
  /** The data to verify the creation of the sales order header. */
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  /** The sales order lines data. */
  SalesOrderLinesData salesOrderLinesData;
  /** The data to verify the creation of the sales order line. */
  SalesOrderLinesData salesOrderLinesVerificationData;
  /** The booked sales order header verification data. */
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  /** The booked sales order header verification data. */
  SalesOrderHeaderData closedSalesOrderHeaderVerificationData;
  /** The taxes data. */
  TaxData[] taxesData;

  /**
   * Class constructor.
   *
   * @param salesOrderHeaderData
   *          The sales order header data.
   * @param salesOrderHeaderVerficationData
   *          The data to verify the creation of the sales order header.
   * @param salesOrderLinesData
   *          The sales order lines data.
   * @param salesOrderLinesVerificationData
   *          The data to verify the creation of the sales order line.
   * @param bookedSalesOrderHeaderVerificationData
   *          The booked sales order header verification data.
   * @param closedSalesOrderHeaderVerificationData
   *          The closed sales order header verification data.
   * @param taxesData
   *          The tax data.
   */
  public ROL_userA_SALe_CloseSalesOrder(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      SalesOrderHeaderData closedSalesOrderHeaderVerificationData, TaxData[] taxesData) {

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.closedSalesOrderHeaderVerificationData = closedSalesOrderHeaderVerificationData;
    this.taxesData = taxesData;
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [SALa010] Create Sales Order. */
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .invoiceTerms("Customer Schedule After Delivery")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("11.2").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("22.40")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("22.40")
            .grandTotalAmount("23.18")
            .currency("EUR")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Closed")
            .summedLineAmount("0.00")
            .grandTotalAmount("0.00")
            .currency("EUR")
            .build(),
        new TaxData[] {
            new TaxData.Builder().tax("VAT(3) - VAT 3%")
                .taxAmount("0.00")
                .taxableAmount("0.00")
                .build(),
            new TaxData.Builder().tax("CHARGE 0.5% - VAT 3%")
                .taxAmount("0.00")
                .taxableAmount("0.00")
                .build() } } };
    return Arrays.asList(data);
  }

  @Test
  public void orderShouldBeClosed() {
    logger.info("** Start of test case [SALe_CloseOrder] Create Sales Order. **");
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    salesOrder.close();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(closedSalesOrderHeaderVerificationData);
    SalesOrder.Tax tax = salesOrder.new Tax(mainPage);
    tax.assertCount(taxesData.length);
    for (TaxData taxData : taxesData) {
      tax.select(taxData);
      tax.assertData(taxData);
    }
  }

  @AfterClass
  public static void tearDown() {
    // OpenbravoERPTest.forceLoginRequired();
  }
}
