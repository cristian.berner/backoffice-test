/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Tests to delete a row in the grid.
 *
 * @author Augusto Mauch
 *
 */
public abstract class DeleteMultipleRecordsOneCheckbox extends OpenbravoERPTest {
  /** The first data object. */
  protected final DataObject firstDataObject;
  /** The second data object. */
  protected final DataObject secondDataObject;
  /** The data object to be used in the filtering. */
  private final DataObject dataObjectForFiltering;

  /**
   * * Class constructor.
   *
   * @param firstDataObject
   *          The new data object.
   * @param secondDataObject
   *          The new data object.
   * @param dataObjectForFiltering
   *          The new data object.
   */
  public DeleteMultipleRecordsOneCheckbox(DataObject firstDataObject, DataObject secondDataObject,
      DataObject dataObjectForFiltering) {
    this.firstDataObject = firstDataObject;
    this.secondDataObject = secondDataObject;
    this.dataObjectForFiltering = dataObjectForFiltering;
  }

  /**
   * Test to insert a new row in the grid.
   */
  @Test
  public void rowsShouldBeDeleted() {
    GeneratedTab<DataObject> tab = getTab();

    // // DEBUG: Force a wait and a change to grid view
    // tab.switchToGridView();
    // Sleep.trySleep(1000);

    int rowsCount = tab.getRecordCount();
    tab.select(dataObjectForFiltering);
    tab.deleteMultipleRecordsOneCheckboxOnGrid();
    tab.clearFilters();
    tab.assertCount(rowsCount - 2);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
