/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.selectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;

import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.PopUpWindow;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP selector pop up window.
 *
 * @author elopio
 * @param <T>
 *          The data object of the selector.
 *
 */
public class OBSelectorPopUpWindow<T extends SelectorDataObject> extends PopUpWindow {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string to get the selector grid. The parameter is object string.
   */
  protected final static String FORMAT_SMART_CLIENT_GET_SELECTOR_GRID = "%s.selectorGrid";

  /* Components */
  /** The selector grid. */
  private final Grid grid;

  /** The OK button. */
  private final Button okButton;
  /** The Cancel button. */
  @SuppressWarnings("unused")
  private final Button cancelButton;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public OBSelectorPopUpWindow(String objectString) {
    super(objectString);
    logger.debug("Before initializing PopUp grid");
    grid = new Grid(String.format(FORMAT_SMART_CLIENT_GET_SELECTOR_GRID, objectString));
    // Now we sleep until OK button is enabled in Grid to assure Grid is completely loaded
    Sleep.smartWaitButtonEnabledUsingTestRegistry(
        objectString + ".children[1].children[1].children[1]", 300);
    Sleep.smartWaitButtonEnabledUsingTestRegistry(objectString + ".children[1]", 300);
    Sleep.trySleep(400);
    // XXX use better locators for the buttons.
    logger.debug("Before initializing okButton of PopUp");
    okButton = new Button(String.format("%s.children[1].children[1].children[1]", objectString));
    cancelButton = new Button(
        String.format("%s.children[1].children[1].children[3]", objectString));
    grid.waitForDataToLoad();
    logger.debug("The index of the first row is: {}. The index of the last row is: {}.",
        grid::getFirstVisibleRowIndex, grid::getLastVisibleRowIndex);

  }

  /**
   * Filter the grid.
   *
   * @param dataObject
   *          Data object with the values of the filter.
   */
  public void filter(T dataObject) {
    logger.debug("Filtering the selector with '{}'", dataObject);
    for (final String key : dataObject.getDataFields().keySet()) {
      String value = dataObject.getDataFields().get(key) instanceof Boolean
          ? (((Boolean) dataObject.getDataFields().get(key)).booleanValue() ? "Yes" : "No")
          : dataObject.getDataFields().get(key).toString();
      grid.filter(key, value);
    }
    // TODO: check this, after setting values sometimes some time is required to sleep
    Sleep.trySleep(500);
    waitForDataToLoad();
  }

  public void waitForDataToLoad() {
    grid.waitForDataToLoad();
  }

  /**
   * Select a record from the grid.
   *
   * @param dataObject
   *          The data of the record that will be selected
   */
  public void select(T dataObject) {
    logger.debug("Selecting '{}' from the selector.", dataObject);
    grid.clearFilters();
    filter(dataObject);

    // TODO get the row count.
    // TODO log a warning if there are more that two rows.
    try {
      // grid.clickCell(0, 0); XXX: not using DOM click because it requires of some sleep before to
      // be effective, using SC javascript instead
      grid.selectRecord(0);
    } catch (NoSuchElementException nsee1) {
      logger.trace("NoSuchElementException has been thrown. Let's try again");
      try {
        grid.selectRecord(0);
      } catch (NoSuchElementException nsee2) {
        throw new NoSuchElementException("No records were found with the filter applied.", nsee2);
      }
    }
    logger.trace("[Refresh] First row clicked.");
    okButton.smartClientClick();
  }

  public void clearFilters() {
    grid.clearFilters();
  }

  /**
   * Get the value present on the filter of a selector field.
   *
   * @param key
   *          The name of the field whose filter value will be returned.
   */
  public Object getFilterValue(String key) {
    String fieldSmartClientLocator = String.format("%s/editRowForm/item[name=%s]",
        grid.getFilterLocator(), TestUtils.convertFieldSeparator(key, false));
    InputField filterInputField = GridInputFieldFactory
        .getFilterInputFieldObject(grid.getObjectString(), key, fieldSmartClientLocator);
    filterInputField.focus();
    return filterInputField.getValue();
  }
}
