/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.fields;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBForm;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the entering of values in a field.
 *
 * @author elopio
 *
 */
public abstract class EnterValueOnField extends OpenbravoERPTest {

  /* Data for this test. */
  /** The name of the field. */
  private final String fieldName;
  /** The data to enter in the field. */
  private final String fieldData;

  /**
   * Class constructor.
   *
   * @param fieldName
   *          The name of the field.
   * @param fieldData
   *          The data to enter.
   */
  public EnterValueOnField(String fieldName, String fieldData) {
    this.fieldName = fieldName;
    this.fieldData = fieldData;
  }

  /**
   * Test the entering of values on the field.
   */
  @Test
  public void valueShouldBeEnteredOnTheField() {
    GeneratedTab<DataObject> tab = getTab();
    tab.clickCreateNewInForm();
    OBForm form = tab.getForm();
    form.fill(fieldName, fieldData);
    form.assertField(fieldName, fieldData);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();

}
