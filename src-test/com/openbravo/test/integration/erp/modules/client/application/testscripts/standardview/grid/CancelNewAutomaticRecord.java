/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the cancellation of the creation of a new automatic record.
 *
 * @author AugustoMauch
 *
 */
public abstract class CancelNewAutomaticRecord extends OpenbravoERPTest {
  /** The first data object. */
  protected final DataObject firstDataObject;

  /**
   * Class constructor.
   *
   * @param firstDataObject
   *          The new data object.
   */
  public CancelNewAutomaticRecord(DataObject firstDataObject) {
    this.firstDataObject = firstDataObject;
  }

  /**
   * Test the record creation by pressing the arrow down key while editing the last grid record.
   */
  @Test
  public void rowShouldBeInserted() {
    GeneratedTab<DataObject> tab = getTab();

    // // DEBUG: Force a wait and a change to grid view
    // tab.switchToGridView();
    // Sleep.trySleep(1000);

    int rowsCount = tab.getRecordCount();
    tab.select(firstDataObject);
    tab.startEditionOnGrid(0);
    tab.moveToNextRecordWhileEditingInGrid();
    tab.cancelOnGrid();
    tab.clearFilters();
    tab.assertCount(rowsCount);
    cleanUp(tab);
  }

  public void cleanUp(GeneratedTab<DataObject> tab) {
    tab.select(firstDataObject);
    tab.deleteOnGrid();
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
