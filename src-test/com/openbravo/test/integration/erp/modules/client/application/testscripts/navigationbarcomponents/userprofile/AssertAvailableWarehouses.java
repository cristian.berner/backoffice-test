/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.UserProfile.ProfileTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the available warehouses for an organization.
 *
 * @author elopio
 *
 */
public class AssertAvailableWarehouses extends OpenbravoERPTest {

  /* Data for this test. */
  /** The role to test. */
  private final String role;
  /** The organization to test. */
  private final String organization;
  /** The warehouses that have to be available for the organization. */
  private final String[] availableWarehouses;

  /**
   * Class constructor.
   *
   * @param role
   *          The role to test.
   * @param organization
   *          The organization to test.
   * @param availableWarehouses
   *          The warehouses that have to be available for the organization.
   */
  public AssertAvailableWarehouses(String role, String organization, String[] availableWarehouses) {
    this.role = role;
    this.organization = organization;
    this.availableWarehouses = availableWarehouses;
  }

  /**
   * Test selecting an organization and assert the available warehouses for that organization.
   */
  @Test
  public void onlyTheWarehousesOfTheOrganizationShouldBeAvailable() {
    UserProfile componentUserProfile = mainPage.getNavigationBar().getComponentUserProfile();
    ProfileTab profileTab = componentUserProfile.openProfileTab();
    profileTab.fill(new ProfileData.Builder().role(role).organization(organization).build());
    profileTab.assertWarehousesCount(availableWarehouses.length);
    profileTab.assertWarehouses(availableWarehouses);
  }
}
