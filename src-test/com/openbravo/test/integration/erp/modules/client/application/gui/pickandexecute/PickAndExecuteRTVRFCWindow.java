/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2012-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 * Executes actions on OpenbravoERP pick and execute pop up window.
 *
 * @author plujan
 * @param <T>
 *          The data object of the selector.
 *
 */
public class PickAndExecuteRTVRFCWindow<T extends DataObject> extends PickAndExecuteWindow<T> {

  /**
   * Grid identifier to be used.
   */
  protected final static String REGISTRY_KEY_RTV_RFC_GRID = "org.openbravo.client.application.ParameterWindow_Grid_grid_A2C19D0EF6594D14A64BC62E99A89CC3";

  public PickAndExecuteRTVRFCWindow(String objectString) {
    super(objectString, REGISTRY_KEY_RTV_RFC_GRID);
  }

}
