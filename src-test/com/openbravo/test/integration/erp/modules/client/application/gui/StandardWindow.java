/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;

/**
 * Executes actions on OpenbravoERP standard windows build upon SmartClient user interface library.
 *
 * @author lorenzo.fidalgo
 *
 */
public abstract class StandardWindow extends View {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  // TODO is there a better way to do this?
  /** The window title. This must be overridden by subclasses. */
  public static final String TITLE = "";

  /** The current tab on the top of the window. */
  public GeneratedTab<? extends DataObject> topTab;
  /** The current tab on the bottom of the window. */
  public GeneratedTab<? extends DataObject> bottomTab;

  /** The currently selected tab. */
  protected GeneratedTab<? extends DataObject> currentTab;
  /**
   * The tab set of the standard window. Every entry of the list represents a level in the tab set.
   * The key of the levels map is the tab identifier.
   */
  private List<Map<String, GeneratedTab<? extends DataObject>>> tabSet;

  protected final LoadingPopUp loadingPopUp;

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   * @param parentTab
   *          The initial parent tab of the window.
   */
  public StandardWindow(String title, GeneratedTab<?> parentTab) {
    super(title);
    loadingPopUp = new LoadingPopUp();
    tabSet = new ArrayList<Map<String, GeneratedTab<? extends DataObject>>>();
  }

  /**
   * Class constructor.
   *
   * @param title
   *          The title of the view.
   * @param menuPath
   *          The path to open the view from the menu.
   */
  public StandardWindow(String title, String... menuPath) {
    super(title, menuPath);
    loadingPopUp = new LoadingPopUp();
    tabSet = new ArrayList<Map<String, GeneratedTab<? extends DataObject>>>();
  }

  /**
   * Class constructor
   *
   * @param title
   *          The title of the view.
   */
  public StandardWindow(String title) {
    super(title);
    loadingPopUp = new LoadingPopUp();
    tabSet = new ArrayList<Map<String, GeneratedTab<? extends DataObject>>>();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    topTab.waitForDataToLoad();
    bottomTab = topTab.getSelectedChildTab();
  }

  /**
   * Add a top level tab. The last top level tab added should be the current tab.
   *
   * @param tab
   *          The tab to add.
   */
  protected void addTopLevelTab(GeneratedTab<? extends DataObject> tab) {
    if (tabSet.isEmpty()) {
      tabSet.add(new HashMap<String, GeneratedTab<? extends DataObject>>());
    } else {
      tabSet.get(0).put(tab.getIdentifier(), tab);
    }
    topTab = tab;
    setCurrentTab(tab);
  }

  /**
   * Get the top level tab, and wait until it is selected.
   *
   * This function should be called after opening a new window, or selecting the window tab in the
   * main tab set in order to get back to the top level tab.
   *
   * @return the top level tab.
   */
  public GeneratedTab<? extends DataObject> getTopLevelTab() {
    currentTab = topTab;
    currentTab.waitUntilSelected();
    return currentTab;
  }

  /**
   * Wait for data to be fully loaded.
   */
  public void waitForDataToLoad() {
    loadingPopUp.waitForDataToLoad();
  }

  /**
   * Set the currently open tab.
   *
   * @param tab
   *          The currently open tab.
   */
  private void setCurrentTab(GeneratedTab<? extends DataObject> tab) {
    currentTab = tab;
    final int currentTabLevel = tab.getLevel();
    // Remove children tabs of previously selected tab, if any.
    for (int index = currentTabLevel + 1; index < tabSet.size(); index++) {
      tabSet.remove(index);
    }
    tabSet.add(currentTab.getChildrenTabs());
  }

  /**
   * Select a tab on the window.
   *
   * @param identifier
   *          The identifier of the tab.
   * @return The selected tab.
   */
  public GeneratedTab<? extends DataObject> selectTab(String identifier) {
    logger.info("Select tab with id '{}'.", identifier);
    GeneratedTab<?> selectedTab = null;
    // FIXME could the current tab be null?
    if (!currentTab.getIdentifier().equals(identifier)) {
      boolean found = false;
      int tabSetLevels = tabSet.size();
      for (int index = tabSetLevels - 1; index >= 0; index--) {
        if (tabSet.get(index).containsKey(identifier)) {
          found = true;
          if (index == 0) {
            // TODO it is a top tab. Click the main tab set button.
          } else {
            selectedTab = tabSet.get(index).get(identifier);
            logger.info("Select the tab '{}'.", selectedTab::getTitle);
            selectSubTab(selectedTab.getParentTab().getIdentifier(), identifier);
            selectedTab.waitUntilSelected();
            setCurrentTab(selectedTab);
            // TODO wait here for the buttons of this tab to be visible.
          }
          break;
        }
      }
      if (!found) {
        throw new IllegalStateException(String.format(
            "The tab with identifier '%s' can't be selected from the current tab.", identifier));
      }
    } else {
      logger.debug("The tab with identifier '{}' is already selected.", identifier);
      selectedTab = currentTab;
    }
    return selectedTab;
  }

  /**
   * Select a child tab.
   *
   * @param parentIdentifier
   *          The identifier of the parent tab.
   * @param childIdentifier
   *          The identifier of the child tab.
   */
  protected void selectSubTab(String parentIdentifier, String childIdentifier) {
    logger.debug("Selecting from the tab '{}' the child tab '{}'.", parentIdentifier,
        childIdentifier);
    Button childTabButton = new Button(TestRegistry.getObjectString(String.format(
        "org.openbravo.client.application.ChildTab_%s_%s", parentIdentifier, childIdentifier)));
    childTabButton.virtualClick();
  }

}
