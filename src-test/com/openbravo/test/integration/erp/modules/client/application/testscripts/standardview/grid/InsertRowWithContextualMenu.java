/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Tests to insert a new row in the grid using the contextual menu.
 *
 * @author AugustoMauch
 *
 */
public abstract class InsertRowWithContextualMenu extends OpenbravoERPTest {
  /** The first data object. */
  protected final DataObject firstDataObject;
  /** The second data object. */
  private final DataObject secondDataObject;

  /**
   * Class constructor.
   *
   * @param firstDataObject
   *          The record on which the contextual menu will be used.
   * @param secondDataObject
   *          The record which will be added using the contextual menu.
   */
  public InsertRowWithContextualMenu(DataObject firstDataObject, DataObject secondDataObject) {
    this.firstDataObject = firstDataObject;
    this.secondDataObject = secondDataObject;
  }

  /**
   * Test to insert a new row in the grid.
   */
  @Test
  public void rowShouldBeInserted() {
    GeneratedTab<DataObject> tab = getTab();
    int rowsCount = tab.getRecordCount();
    tab.select(firstDataObject);
    tab.createOnGridWithContextualMenu(secondDataObject);
    tab.saveEdition();
    tab.clearFilters();
    tab.assertCount(rowsCount + 1);

    cleanUp(tab);
  }

  public void cleanUp(GeneratedTab<DataObject> tab) {
    tab.select(firstDataObject);
    tab.deleteOnGrid();
    tab.select(secondDataObject);
    tab.deleteOnGrid();
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
