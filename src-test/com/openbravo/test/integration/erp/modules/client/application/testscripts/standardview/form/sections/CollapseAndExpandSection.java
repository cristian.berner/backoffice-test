/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.sections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SectionItem;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test to collapse and to expanded sections of a form.
 *
 * @author elopio
 */
public abstract class CollapseAndExpandSection extends OpenbravoERPTest {

  /* Data for this test. */
  /** The name of the section that will be collapsed and expanded. */
  private final String sectionName;
  /** An array with the names of the fields of the section. */
  private final String[] fieldNames;

  /**
   * Class constructor.
   *
   * @param sectionName
   *          The name of the section that will be collapsed and expanded.
   * @param fieldNames
   *          An array with the names of the fields of the section.
   */
  public CollapseAndExpandSection(String sectionName, String[] fieldNames) {
    this.sectionName = sectionName;
    this.fieldNames = fieldNames;
  }

  /**
   * Test to collapse and to expand sections of a form.
   */
  @Test
  public void collapseShouldHideFieldsAndExpandShowThem() {
    GeneratedTab<DataObject> tab = getTab();
    tab.clickCreateNewInForm();
    SectionItem sectionItem = tab.getForm().getSection(sectionName);
    sectionItem.collapse();
    sectionItem.expand();
    sectionItem.waitUntilExpanded();
    for (String fieldName : fieldNames) {
      InputField field = tab.getForm().getField(fieldName);
      assertTrue(String.format("Field '%s' is not visible.", fieldName),
          field.isPresent() && field.isVisible());
    }
    sectionItem.collapse();
    sectionItem.waitUntilCollapsed();
    for (String fieldName : fieldNames) {
      InputField field = tab.getForm().getField(fieldName);
      assertFalse(String.format("Field '%s' is visible.", fieldName),
          field.isPresent() && field.isVisible());
    }
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
