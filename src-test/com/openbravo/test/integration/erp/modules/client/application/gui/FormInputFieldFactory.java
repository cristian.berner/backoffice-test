/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.AttributeSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.CurrencySearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.FinaccTransactionSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.LocationSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.LocatorSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.OrderLineSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.OrderSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.PaymentMethodSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.ProjectSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.searchitems.ShipmentInOutLineSearchItem;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.BusinessPartnerSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.OrganizationSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.PaymentSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.ProductSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.ProductSimpleSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.ProjectSelector;
import com.openbravo.test.integration.erp.modules.client.application.gui.selectors.ShipmentReceiptLineSelector;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentSelectorItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.CheckBox;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.DateItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.HiddenItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.NumberItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SearchItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectorItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TextItem;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Instantiates SmartClient objects based on the class name.
 *
 * @author elopio
 *
 */
public class FormInputFieldFactory {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to get the class of a SmartClient object. The parameter is object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_CLASS = "%s.Class";

  /**
   * Instantiate and return an input field object.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @return the input field object.
   */
  public static InputField getInputFieldObject(String objectString) {
    String className = (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_CLASS, objectString));
    if (className.equals(ClassNamesConstants.CLASS_TEXT_ITEM)
        || className.equals(ClassNamesConstants.CLASS_TEXT_AREA_ITEM)) {
      return new TextItem(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_DATE_ITEM)) {
      return new DateItem(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_LIST_ITEM)
        || className.equals(ClassNamesConstants.CLASS_FOREIGN_KEY_ITEM)
        || className.equals(ClassNamesConstants.CLASS_FOREIGN_KEY_COMBO_ITEM)) {
      return new SelectItem(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_NUMBER_ITEM)
        || className.equals(ClassNamesConstants.CLASS_CANVAS_ITEM)) {
      return new NumberItem(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_ENCRYPTED_ITEM)) {
      // FIXME create a new class for encrypted items.
      return new TextItem(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_CHECK_BOX_ITEM)) {
      return new CheckBox(objectString);
    } else if (className.equals(ClassNamesConstants.CLASS_SEARCH_ITEM)
        || className.equals(ClassNamesConstants.CLASS_SEARCH_ITEM_ATTRIBUTE)) {
      SearchItem searchItem = new SearchItem(objectString);
      String targetEntity = searchItem.getTargetEntity();
      if (targetEntity.equals("Location")) {
        return new LocationSearchItem(searchItem);
      } else if (targetEntity.equals("AttributeSetInstance")) {
        return new AttributeSearchItem(searchItem);
      } else if (targetEntity.equals("Locator")) {
        return new LocatorSearchItem(searchItem);
      } else if (targetEntity.equals("Project")) {
        return new ProjectSearchItem(searchItem);
      } else if (targetEntity.equals("FIN_PaymentMethod")) {
        return new PaymentMethodSearchItem(searchItem);
      } else if (targetEntity.equals("Currency")) {
        return new CurrencySearchItem(searchItem);
      } else if (targetEntity.equals("OrderLine")) {
        return new OrderLineSearchItem(searchItem);
      } else if (targetEntity.equals("Order")) {
        return new OrderSearchItem(searchItem);
      } else if (targetEntity.equals("FIN_Finacc_Transaction")) {
        return new FinaccTransactionSearchItem(searchItem);
      } else if (targetEntity.equals("MaterialMgmtShipmentInOutLine")) {
        return new ShipmentInOutLineSearchItem(searchItem);
      } else {
        throw new NotImplementedException(
            String.format("Search item %s has as entity %s.", objectString, targetEntity));
      }
    } else if (className.equals(ClassNamesConstants.CLASS_SELECTOR_ITEM)) {
      SelectorItem selectorItem = new SelectorItem(objectString);
      String targetEntity = selectorItem.getTargetEntity();
      if (targetEntity != null) {

        if ("BusinessPartner".equals(targetEntity)) {
          return new BusinessPartnerSelector(selectorItem);
        } else if ("Product".equals(targetEntity)) {
          if (selectorItem.getType().equals("_id_800060")) {
            return new ProductSelector(selectorItem);
          } else {
            return new ProductSimpleSelector(selectorItem);
          }
        } else if ("Project".equals(targetEntity)) {
          return new ProjectSelector(selectorItem);
        } else if ("Organization".equals(targetEntity)) {
          return new OrganizationSelector(selectorItem);
        } else if ("FIN_Payment".equals(targetEntity)) {
          return new PaymentSelector(new AddPaymentSelectorItem(objectString));
        } else if ("MaterialMgmtShipmentInOutLine".equals(targetEntity)) {
          return new ShipmentReceiptLineSelector(selectorItem);
        } else if ("ADList".equals(targetEntity)) {
          return new SelectItem(objectString);
        } else {
          logger.warn("Selector with type {} could not be resolved, using SelectItem.",
              targetEntity);
          // If a selector is not one of the previous, could be a specific implementation. In this
          // case, the best option is to fill it as a test field
          return new SelectItem(objectString);
          // throw new NotImplementedException(String.format("Selector item %s has as entity %s.",
          // objectString, targetEntity));
        }
      } else {
        logger.warn("Selector with type {} could not be resolved, using default text input.",
            targetEntity);
        return new TextItem(objectString);
      }
    } else if (className.equals(ClassNamesConstants.CLASS_HIDDEN_ITEM)) {
      // XXX A hidden item is probably shown in the status bar. The verification should be done
      // there.
      return new HiddenItem(objectString);
    } else {
      throw new NotImplementedException(
          String.format("%s is of class %s.", objectString, className));
    }
  }
}
