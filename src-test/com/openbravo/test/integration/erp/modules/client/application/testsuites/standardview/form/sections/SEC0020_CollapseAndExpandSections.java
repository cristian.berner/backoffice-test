/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form.sections;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.sections.CollapseAndExpandSection;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test to collapsed and to expanded sections of a form.
 *
 * @author elopio
 */
@RunWith(Parameterized.class)
public class SEC0020_CollapseAndExpandSections extends CollapseAndExpandSection {

  /**
   * Class constructor.
   *
   * @param sectionName
   *          The name of the section that will be collapsed and expanded.
   * @param fieldNames
   *          An array with the names of the fields of the section.
   */
  public SEC0020_CollapseAndExpandSections(String sectionName, String[] fieldNames) {
    super(sectionName, fieldNames);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> sectionsValues() {
    Object[][] data = new Object[][] { {
        // More information
        "402880E72F1C15A5012F1C7AA98B00E8", new String[] { "orderReference", "salesRepresentative",
            "description", "invoiceAddress", "deliveryLocation" } } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    mainPage.changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
        .client("QA Testing")
        .organization("*")
        .warehouse("Spain East warehouse")
        .build());
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    GeneratedTab<DataObject> headerTab = (GeneratedTab<DataObject>) salesOrderWindow
        .selectTab(HeaderTab.IDENTIFIER);
    return headerTab;
  }

}
