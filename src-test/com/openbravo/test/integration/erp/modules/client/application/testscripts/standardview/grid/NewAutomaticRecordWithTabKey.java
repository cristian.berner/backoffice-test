/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  amauch@openbravo.com
 *  plujan@openbravo.com
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the record creation by pressing the arrow down key while editing the last grid record.
 *
 * @author AugustoMauch
 *
 */
public abstract class NewAutomaticRecordWithTabKey extends OpenbravoERPTest {
  /** The first data object. */
  protected final DataObject firstDataObject;
  /** The second data object. */
  private final DataObject secondDataObject;

  /**
   * Class constructor.
   *
   * @param firstDataObject
   *          The new data object.
   * @param secondDataObject
   *          The new data object.
   */
  public NewAutomaticRecordWithTabKey(DataObject firstDataObject, DataObject secondDataObject) {
    this.firstDataObject = firstDataObject;
    this.secondDataObject = secondDataObject;
  }

  /**
   * Test the record creation by pressing the arrow down key while editing the last grid record.
   */
  @Test
  public void rowShouldBeInserted() {
    GeneratedTab<DataObject> tab = getTab();
    int rowsCount = tab.getRecordCount();
    tab.select(firstDataObject);
    tab.startEditionOnGrid(0);
    moveToNextRecordWithTabKey(tab);
    tab.fillRow(secondDataObject);
    tab.assertInEditMode();
    tab.saveEdition();
    tab.assertData(secondDataObject);
    tab.switchToGridView();
    tab.clearFilters();
    Sleep.trySleep();
    tab.assertCount(rowsCount + 1);
    cleanUp(tab);
  }

  public void cleanUp(GeneratedTab<DataObject> tab) {
    tab.select(firstDataObject);
    tab.deleteOnGrid();
    tab.select(secondDataObject);
    tab.deleteOnGrid();
  }

  private void moveToNextRecordWithTabKey(GeneratedTab<DataObject> tab) {
    boolean onlyAllowTextItems = false;
    InputField lastField = tab.getField("default", onlyAllowTextItems);
    if (lastField instanceof Canvas) {
      lastField.focus();
      Canvas lastFieldCanvas = (Canvas) lastField;
      lastFieldCanvas.pressTab();
    }
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
