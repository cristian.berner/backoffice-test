/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form.fields;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.OBForm;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.form.fields.EnterValueOnSelectorField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectorField;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the entering of values in a business partner selector field.
 *
 * @author elopio
 *
 */
@RunWith(Parameterized.class)
public class FIE0010_EnterValueInBusinessPartnerSelector extends EnterValueOnSelectorField {

  /**
   * Class constructor.
   *
   * @param businessPartnerSelectorData
   *          The data of the business partner to enter.
   */
  public FIE0010_EnterValueInBusinessPartnerSelector(
      BusinessPartnerSelectorData businessPartnerSelectorData) {
    super(businessPartnerSelectorData);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<BusinessPartnerSelectorData[]> fieldValues() throws IOException {
    return Arrays.asList(new BusinessPartnerSelectorData[][] {
        { new BusinessPartnerSelectorData.Builder().name("Customer B")
            .value("CUSB")
            .customer(true)
            .vendor(false)
            .build() } });
  }

  /**
   * Open and return the selector field that will be used for the test.
   *
   * @return the form that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public SelectorField<SelectorDataObject> getSelectorField() {

    mainPage.changeProfile(new ProfileData.Builder().role("QA Testing Admin - QA Testing")
        .client("QA Testing")
        .organization("*")
        .warehouse("Spain East warehouse")
        .build());

    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);

    HeaderTab headerTab = (HeaderTab) salesOrderWindow.selectTab(HeaderTab.IDENTIFIER);
    headerTab.clickCreateNewInForm();

    OBForm form = headerTab.getForm();
    return (SelectorField<SelectorDataObject>) form.getField("businessPartner");
  }

  @AfterClass
  public static void tearDown() {
  }
}
