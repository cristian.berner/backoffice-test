/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.NumberItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TextItem;
import com.openbravo.test.integration.selenium.NotImplementedException;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Instantiates SmartClient objects based on the class name.
 *
 * @author elopio
 *
 */
public class GridInputFieldFactory {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  /**
   * Formatting string used to get the class of a SmartClient object. The parameter is object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_CLASS = "%s.Class";
  /**
   * Formatting string used to get the class of a SmartClient object. The first parameter is object
   * string of the grid. The second parameter is the name of the field.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FILTER_EDITOR_FIELD_CLASS = "%s.filterEditor.getField(%s).editorType";
  /**
   * Formatting string used to locate a grid column filter by its name. The first parameter is the
   * grid filter locator. The second parameter is the column name.
   */
  protected static final String FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME = "%s/editRowForm/item[name=%s]";
  /**
   * Formatting string used to get the year of a Data object. The parameter is object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_YEAR = "%s.getFullYear()";
  /**
   * Formatting string used to get the year of a Data object. The parameter is object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_MONTH = "%s.getMonth()";
  /**
   * Formatting string used to get the year of a Data object. The parameter is object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_DAY = "%s.getDate()";

  /**
   * Instantiate and return an input field object from the filter.
   *
   * @param gridObjectString
   *          The string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the grid.
   * @param fieldName
   *          The name of the field.
   * @param fieldSmartClientLocator
   *          The smart client locator of the field.
   * @return the input field object.
   */
  public static InputField getFilterInputFieldObject(String gridObjectString, String fieldName,
      String fieldSmartClientLocator) {
    String className;
    try {
      className = (String) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FILTER_EDITOR_FIELD_CLASS,
              gridObjectString, TestUtils.convertFieldSeparator(fieldName)));
    } catch (JavascriptException jse) {
      logger.warn("JavascriptException was thrown. Sleeping and trying again.");
      // TODO L1: Reduce the following static sleep once it is stable
      Sleep.trySleep(5000);
      try {
        className = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            String.format(FORMAT_SMART_CLIENT_GET_FILTER_EDITOR_FIELD_CLASS, gridObjectString,
                TestUtils.convertFieldSeparator(fieldName)));
      } catch (JavascriptException jse2) {
        logger.warn("Second JavascriptException thrown in a row. Sleeping and trying again.");
        // TODO L1: Reduce the following static sleep once it is stable
        Sleep.trySleep(7500);
        className = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            String.format(FORMAT_SMART_CLIENT_GET_FILTER_EDITOR_FIELD_CLASS, gridObjectString,
                TestUtils.convertFieldSeparator(fieldName)));
      }
    }
    if (className.equals(ClassNamesConstants.CLASS_TEXT_ITEM)
        || className.equals(ClassNamesConstants.CLASS_FOREIGN_KEY_ITEM)
        || className.equals(ClassNamesConstants.CLASS_SELECTOR_ITEM)
        || className.equals(ClassNamesConstants.CLASS_TEXT_AREA_ITEM)
        || className.endsWith(ClassNamesConstants.CLASS_SEARCH_ITEM)
        || className.equals(ClassNamesConstants.CLASS_POP_UP_TEXT_AREA_ITEM)
        || className.equals(ClassNamesConstants.CLASS_LIST_ITEM)
        || className.equals(ClassNamesConstants.CLASS_FOREIGN_KEY_COMBO_ITEM)
        || className.equals(ClassNamesConstants.CLASS_SEARCH_ITEM_ATTRIBUTE)) {
      TextItem textItem = new TextItem();
      textItem.setSmartClientLocator(fieldSmartClientLocator);
      return textItem;
    } else if (className.equals(ClassNamesConstants.CLASS_DATE_ITEM)) {
      TextItem textItem = new TextItem();
      textItem.setSmartClientLocator(fieldSmartClientLocator);
      return textItem;
    } else if (className.equals(ClassNamesConstants.CLASS_CHECK_BOX_ITEM)) {
      logger.trace("Item class '{}' on the column '{}'.", className, fieldName);
      SelectItem selectItem = new SelectItem();
      selectItem.setSmartClientLocator(fieldSmartClientLocator);
      return selectItem;
    } else if (className.equals(ClassNamesConstants.CLASS_NUMBER_ITEM)) {
      NumberItem numberItem = new NumberItem();
      numberItem.setSmartClientLocator(fieldSmartClientLocator);
      return numberItem;
    } else {
      throw new NotImplementedException(className);
    }
  }

  /**
   * Instantiate and return an read field object.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @return the input field object.
   */
  public static String getReadFieldObject(String objectString) {
    String className = null;
    try {
      className = (String) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_CLASS, objectString));
    } catch (JavascriptException jse) {
      logger.warn("A JavascriptException was thrown. Sleep and try it again.");
      // TODO L4: Reduce the following static sleep.
      Sleep.trySleep(7500);
      try {
        className = (String) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_CLASS, objectString));
      } catch (JavascriptException jse2) {
        logger.warn(
            "Second JavascriptException thrown in a row. It will be tried to continue anyway.");
      }
    }
    // The following IF clause (className == null) is required since Selenium 3.3.1 due to new
    // problems evaluating the
    // "%s.Class", being "%s" the "objectString" variable.
    if (className == null) {
      className = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format("typeof(%s)", objectString.replaceFirst("dom=", "")));
    }
    if (className.equals("Date") || className.equals("date")) {
      int year = ((Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_YEAR, objectString)))
              .intValue();
      int month = ((Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_MONTH, objectString)))
              .intValue();
      int day = ((Long) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_DAY, objectString)))
              .intValue();
      return OBDate.getGivenDate(year, month, day);
    } else {
      return SeleniumSingleton.INSTANCE.executeScriptWithReturn(objectString).toString();
    }
  }

  /**
   * Instantiate and return an input field object.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   * @param onlyAllowTextItems
   *          Boolean that must be set to true if it is wanted that only TextItems are allowed
   * @return the input field object.
   */
  public static InputField getInputFieldObject(String objectString, Boolean onlyAllowTextItems) {
    String className;
    try {
      className = (String) SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_CLASS, objectString));
    } catch (JavascriptException jse) {
      // The following catch is required since Selenium 3.3.1 due to new
      // problems evaluating the
      // "%s.Class", being "%s" the "objectString" variable.
      Sleep.trySleep(10000);
      className = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format("typeof(%s)", objectString.replaceFirst("dom=", "")));
    }
    if (onlyAllowTextItems) {
      if (className.equals(ClassNamesConstants.CLASS_TEXT_ITEM)) {
        return new TextItem(objectString);
      } else {
        throw new NotImplementedException(
            String.format("%s is of class %s.", objectString, className));
      }
    } else {
      // If an input that is not a string needs to be retrieved, delegate on FormInputFieldFactory
      return FormInputFieldFactory.getInputFieldObject(objectString);
    }
  }

  public static InputField getInputFieldObject(String objectString) {
    boolean onlyAllowTextItems = true;
    return getInputFieldObject(objectString, onlyAllowTextItems);
  }
}
