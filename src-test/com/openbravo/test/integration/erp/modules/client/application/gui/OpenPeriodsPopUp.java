package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

public class OpenPeriodsPopUp extends OBClassicPopUp {

  private static final String IDENTIFIER_COMBO_BOX_ACTION = "Action";
  @SuppressWarnings("hiding")
  private static final String BUTTON_OK = "org.openbravo.client.application.OpenClosePeriod.popup.okButton";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public OpenPeriodsPopUp(String objectString) {
    super(objectString);
  }

  public void select(String action) {
    SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_COMBO_BOX_ACTION).clear();
    // FIXME: <chromedriver> implementing sendKeys alternative/workaround
    if (!SeleniumSingleton.runsChrome()) {
      SeleniumSingleton.INSTANCE.findElementByName(IDENTIFIER_COMBO_BOX_ACTION).sendKeys(action);
    } else {
      String scrToExec = String.format("document.getElementsByName('%s')[0]",
          IDENTIFIER_COMBO_BOX_ACTION);
      logger.debug(
          "[chromedriver] Sending text with sendKeys() method workaround in Chrome for Process Request popup");
      SeleniumSingleton.INSTANCE.executeScriptWithReturn(scrToExec + ".value = '" + action + "';");
    }
  }

  /**
   * Select a PopUp with a web element. This function waits until the element is present and
   * visible.
   */
  public void selectPopUp() {
    logger.debug("Selecting the pop up.");
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the pop up to be loaded.");
        try {
          Button buttonOK = new Button(TestRegistry.getObjectString(BUTTON_OK));
          return buttonOK.isVisible();
        } catch (final NoSuchElementException exception) {
          return false;
        }
      }
    });
  }

  @Override
  public void clickOK() {
    logger.debug("Clicking the OK button.");
    Button buttonOK = new Button(TestRegistry.getObjectString(BUTTON_OK));
    buttonOK.click();
  }

}
