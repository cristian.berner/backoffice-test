/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure.UnitOfMeasureData;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureTab;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid.SortRecords;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the grid filtering by product selector
 *
 * @author igarcia
 */
@RunWith(Parameterized.class)
public class GRD0220_SortByColumn extends SortRecords {

  /**
   * Class constructor.
   *
   * @param dataObjectExpectedFirstSorting
   * @param dataObjectExpectedSecondSorting
   *
   */
  public GRD0220_SortByColumn(DataObject dataObjectExpectedFirstSorting,
      DataObject dataObjectExpectedSecondSorting) {
    super(dataObjectExpectedFirstSorting, dataObjectExpectedSecondSorting);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * Replace "UnitOfMeasure" with the desired test subject (BusinessPartner,
   * BusinessPartnerCategory, etc.) for window and tab, and "name" key field with the column to sort
   * by, as well as the expected values. Make the correct imports, typically under
   * "com.openbravo.test.integration.erp.data/gui.masterdata.*"
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<DataObject[]> filterValues() {
    DataObject[][] data = new DataObject[][] {
        { new UnitOfMeasureData.Builder().name("Year").build(),
            new UnitOfMeasureData.Builder().name("Centimeter").build() } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    mainPage.getNavigationBar()
        .changeProfile(new ProfileData.Builder()
            .role("F&B International Group Admin - F&B International Group")
            .client("F&B International Group")
            .organization("*")
            .warehouse("España Región Norte")
            .build());
    UnitOfMeasureWindow unitOfMeasureWindow = new UnitOfMeasureWindow();
    mainPage.openView(unitOfMeasureWindow);
    GeneratedTab<DataObject> unitOfMeasureTab = (GeneratedTab<DataObject>) unitOfMeasureWindow
        .selectTab(UnitOfMeasureTab.IDENTIFIER);
    return unitOfMeasureTab;
  }
}
