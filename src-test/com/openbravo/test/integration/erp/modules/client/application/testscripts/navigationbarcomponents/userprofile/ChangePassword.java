/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.userprofile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.data.ChangePasswordData;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.change_password.CreateTemporaryUserTest;

/**
 * Test the password change from the user profile widget added by Navigation Bar Components module.
 *
 * @author elopio
 *
 */
public class ChangePassword extends CreateTemporaryUserTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String TEST_USER_PREFIX = "testpassword_";
  private static final String TEST_USER_ROLE = "F&BUser";

  /* Data for this tests. */
  /** The password data. */
  private final ChangePasswordData passwordData;

  /**
   * Class constructor.
   *
   * @param passwordData
   *          The password data.
   */
  public ChangePassword(ChangePasswordData passwordData) {
    super();
    this.passwordData = passwordData;
  }

  /**
   * Test password change. [NAV0050a] Change password
   */
  @Test
  public void passwordShouldBeChanged() {
    logger.info("[NAV0050a] Change password");
    loginWithTemporaryUser();
    mainPage.getNavigationBar().changePassword(passwordData);
  }

  private void loginWithTemporaryUser() {
    logger.info("Creating test user");
    String testUser = TEST_USER_PREFIX + System.currentTimeMillis();
    createTestUser(testUser, passwordData.getCurrentPassword(), TEST_USER_ROLE);

    logger.info("Moving to login page");
    logout();
    loginWith(new LogInData.Builder().userName(testUser)
        .password(passwordData.getCurrentPassword())
        .build());
  }

}
