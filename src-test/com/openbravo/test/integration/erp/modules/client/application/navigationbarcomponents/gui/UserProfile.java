/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriverException;

import com.openbravo.test.integration.erp.data.ChangePasswordData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.modules.client.application.gui.LoadingPopUp;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.client.application.gui.WarnDialog;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.CheckBox;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SelectItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TabSet;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TextItem;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes and verify actions on OpenbravoERP user profile, added by the Navigation Bar Components
 * module.
 *
 * @author elopio
 *
 */
public class UserProfile {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Component elements. */
  /** The user name button. */
  private Button buttonUserName;
  /** The profile tab set. */
  private TabSet tabSetProfile;
  /** The profile tab. */
  private ProfileTab profileTab;
  /** The password tab. */
  private PasswordTab passwordTab;
  /** The Loading Popup. */
  private LoadingPopUp lPU;

  /* Registry keys. */
  /** Registry key of the user profile button. */
  private static final String REGISTRY_KEY_BUTTON_USER_NAME = "org.openbravo.client.application.navigationbarcomponents.UserProfileButton";
  // Registry key of the tab set. */
  private static final String REGISTRY_KEY_TAB_SET_PROFILE = "org.openbravo.client.application.navigationbarcomponents.UserProfile.Tabset";

  /**
   * Class constructor
   *
   */
  public UserProfile() {
    this.buttonUserName = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_USER_NAME));
  }

  /**
   * Get the user name
   *
   * @return the name of the user name
   */
  public String getUserName() {
    return buttonUserName.getTitle();
  }

  /**
   * Verify the displayed user name
   *
   * @param userName
   *          The expected user name.
   */
  void verifyUserName(String userName) {
    assertThat("Wrong user name.", buttonUserName.getTitle(), is(equalTo(userName)));
  }

  /**
   * Check if the widget is open.
   *
   * @return true if the widget is open. Otherwise, false.
   */
  public boolean isOpen() {
    // XXX it would be better to use the parent component.
    return tabSetProfile != null && tabSetProfile.isVisible();
  }

  /**
   * Open the UserProfile tab set
   */
  public void open() {
    if (!isOpen()) {
      logger.debug("Opening the profile pop up.");
      // lPU.waitForDataToLoad() to let application to load before using smartClientClick();
      lPU = new LoadingPopUp();
      lPU.waitForDataToLoad();
      buttonUserName.smartClientClick();
      tabSetProfile = new TabSet(TestRegistry.getObjectString(REGISTRY_KEY_TAB_SET_PROFILE));
      tabSetProfile.waitUntilVisible();
      profileTab = new ProfileTab();
    }
  }

  /**
   * Close the UserProfile tab set
   */
  public void close() {
    if (isOpen()) {
      logger.debug("Closing the profile pop up.");
      buttonUserName.smartClientClick();
      tabSetProfile = null;
    }
  }

  /**
   * Check if the profile tab is open.
   *
   * @return true if the profile tab is open. Otherwise, false.
   */
  public boolean isProfileTabOpen() {
    return tabSetProfile != null && tabSetProfile.isTabSelected(ProfileTab.TAB_TITLE);
  }

  /**
   * Check if the password tab is open
   *
   * @return true if the tab is opened. Otherwise, false
   */
  public boolean isPasswordTabOpen() {
    return tabSetProfile != null && tabSetProfile.isTabSelected(PasswordTab.TAB_TITLE);
  }

  /**
   * Open the profile tab.
   *
   * @return the profile tab.
   */
  public ProfileTab openProfileTab() {
    open();
    // XXX Always click the profile tab to hide the tooltip shown after clicking the user profile
    // widget, because it hides the first combobox.
    // This should be fixed hovering to the button after clicking it, but this is still not
    // implemented in the Firefox Driver.
    // if (!isProfileTabOpen()) {
    logger.debug("Opening the profile tab.");
    tabSetProfile.selectTab(ProfileTab.TAB_TITLE);
    return profileTab;
    // }
  }

  /**
   * Open the password tab.
   *
   * @return the password tab.
   */
  public PasswordTab openPasswordTab() {
    open();
    if (!isPasswordTabOpen()) {
      logger.info("Opening the password tab.");
      tabSetProfile.selectTab(PasswordTab.TAB_TITLE);
      passwordTab = new PasswordTab();
    }
    return passwordTab;
  }

  /**
   * Change the user profile.
   *
   * @param newProfile
   *          The new profile of the user.
   */
  public void changeProfile(ProfileData newProfile) {
    changeProfile(newProfile, true);
  }

  /**
   * Change the user profile.
   *
   * @param newProfile
   *          The new profile of the user.
   * @param apply
   *          Boolean that indicates whether the changes should be applied or canceled.
   */
  public void changeProfile(ProfileData newProfile, boolean apply) {
    openProfileTab();
    profileTab.changeProfile(newProfile, apply);
    tabSetProfile = null;
    if (apply) {
      buttonUserName = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_USER_NAME));
    }
  }

  /**
   * Assert the user profile information.
   *
   * @param expectedProfileData
   *          The expected profile data of the user.
   */
  public void assertProfile(ProfileData expectedProfileData) {
    openProfileTab();
    profileTab.assertProfile(expectedProfileData);
  }

  /**
   * Get current user profile information.
   *
   * @return the current profile information.
   */
  public ProfileData getProfile() {
    // previouslyClosed is used to leave Profile widget in the same status than before getProfile()
    // function
    boolean previouslyClosed = !isOpen();
    if (previouslyClosed) {
      openProfileTab();
    }
    ProfileData profile = profileTab.getProfile();
    if (previouslyClosed) {
      close();
    }
    return profile;
  }

  /**
   * Change the user password.
   *
   * @param password
   *          The data for the password change.
   */
  void changePassword(ChangePasswordData password) {
    openPasswordTab();
    passwordTab.changePassword(password);
    tabSetProfile = null;
    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickOk();
  }

  /**
   * Executes and verify actions on OpenbravoERP profile tab, added by the Navigation Bar Components
   * module.
   *
   * @author elopio
   *
   */
  public class ProfileTab {

    /** The title of the tab. */
    private static final String TAB_TITLE = "Profile";

    /* Component elements. */
    /** The role combo box. */
    private final SelectItem comboBoxRole;
    /** The client combo box. */
    private final SelectItem comboBoxClient;
    /** The organization combo box. */
    private final SelectItem comboBoxOrganization;
    /** The warehouse combo box. */
    private final SelectItem comboBoxWarehouse;
    /** The language combo box. */
    private final SelectItem comboBoxLanguage;
    /** The set as default check box. */
    private final CheckBox checkBoxSetAsDefault;
    /** The save button. */
    private final Button buttonSave;
    /** The cancel button. */
    private final Button buttonCancel;

    /* Registry keys. */
    /** Registry key of the role combo box. */
    private static final String REGISTRY_KEY_COMBO_BOX_ROLE = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.RoleField";
    /** Registry key of the client combo box. */
    private static final String REGISTRY_KEY_COMBO_BOX_CLIENT = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.ClientField";
    /** Registry key of the organization combo box. */
    private static final String REGISTRY_KEY_COMBO_BOX_ORGANIZATION = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.OrgField";
    /** Registry key of the warehouse combo box. */
    private static final String REGISTRY_KEY_COMBOBOX_WAREHOUSE = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.WarehouseField";
    /** Registry key of the language combo box. */
    private static final String REGISTRY_KEY_COMBO_BOX_LANGUAGE = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.LanguageField";
    /** Registry key of the set as default check box. */
    private static final String REGISTRY_KEY_CHECK_BOX_SET_AS_DEFAULT = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.DefaultField";
    /** Registry key of the save button. */
    private static final String REGISTRY_KEY_BUTTON_SAVE = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.SaveButton";
    /** Registry key of the cancel button. */
    private static final String REGISTRY_KEY_BUTTON_CANCEL = "org.openbravo.client.application.navigationbarcomponents.UserProfileRole.CancelButton";

    /**
     * Class constructor.
     *
     */
    private ProfileTab() {
      this.comboBoxRole = new SelectItem(TestRegistry.getObjectString(REGISTRY_KEY_COMBO_BOX_ROLE));
      this.comboBoxClient = new SelectItem(
          TestRegistry.getObjectString(REGISTRY_KEY_COMBO_BOX_CLIENT));
      this.comboBoxOrganization = new SelectItem(
          TestRegistry.getObjectString(REGISTRY_KEY_COMBO_BOX_ORGANIZATION));
      this.comboBoxWarehouse = new SelectItem(
          TestRegistry.getObjectString(REGISTRY_KEY_COMBOBOX_WAREHOUSE));
      this.comboBoxLanguage = new SelectItem(
          TestRegistry.getObjectString(REGISTRY_KEY_COMBO_BOX_LANGUAGE));
      this.checkBoxSetAsDefault = new CheckBox(
          TestRegistry.getObjectString(REGISTRY_KEY_CHECK_BOX_SET_AS_DEFAULT));
      this.buttonSave = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_SAVE));
      this.buttonCancel = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CANCEL));
    }

    /**
     * Fill the profile form.
     *
     * @param newProfile
     *          The profile data.
     */
    public void fill(ProfileData newProfile) {
      logger.debug("Filling the profile form with {}.", newProfile::toString);
      comboBoxRole.waitUntilVisible();
      if (newProfile.getRole() != null) {
        comboBoxRole.selectItem(newProfile.getRole());
      }
      if (newProfile.getClient() != null) {
        comboBoxClient.selectItem(newProfile.getClient());
      }
      if (newProfile.getOrganization() != null) {
        comboBoxOrganization.selectItem(newProfile.getOrganization());
      }
      if (newProfile.getWarehouse() != null) {
        comboBoxWarehouse.selectItem(newProfile.getWarehouse());
      }
      if (newProfile.getLanguage() != null) {
        comboBoxLanguage.selectItem(newProfile.getLanguage());
      }
      checkBoxSetAsDefault.setValue(newProfile.isDefault());
    }

    /**
     * Change the user profile.
     *
     * @param newProfile
     *          The new profile of the user.
     */
    @SuppressWarnings("unused")
    private void changeProfile(ProfileData newProfile) {
      changeProfile(newProfile, true);
    }

    /**
     * Change the user profile.
     *
     * @param newProfile
     *          The new profile of the user.
     * @param apply
     *          Boolean that indicates whether the changes should be applied or canceled.
     */
    private void changeProfile(ProfileData newProfile, boolean apply) {
      fill(newProfile);
      if (apply) {
        // Check here newProfile matches with the data in the Profile Widget
        // It must be checked that all the fields in newProfile match with the correspondent key
        // value from getProfile()
        try {
          assertProfile(newProfile);
        } catch (AssertionError ae) {
          logger.warn(
              "Current profile data has not matched with the profile data that must be introduced. Trying again.");
          logger.debug("Current values in ProfileTab: {}", getProfile()::toString);
          logger.debug("Values that should have been inserted: {}", newProfile::toString);
          assertProfile(newProfile);
        }
        buttonSave.smartClientClick();
      } else {
        buttonCancel.smartClientClick();
      }
      Sleep.smartWaitExecuteScript(
          "document.getElementsByClassName('OBNavBarTextButton')[0] != null", 2000);
      try {
        Sleep.smartWaitExecuteScript(
            "window.OB.TestRegistry.registry['org.openbravo.client.application.navigationbarcomponents.UserProfileButton'].isVisible()",
            200);
      } catch (WebDriverException wde) {
        Sleep.trySleepWithMessage(3000,
            "UserProfileButton was not ready or visible and throwed a WebDriverException, sleeping a bit");
      }
    }

    /**
     * Assert the user profile information.
     *
     * @param expectedProfileData
     *          The expected profile data of the user.
     */
    private void assertProfile(ProfileData expectedProfileData) {
      logger.info("Verifying the profile information. Expected data: {}.",
          expectedProfileData::toString);
      comboBoxRole.waitUntilVisible();
      if (expectedProfileData.getRole() != null) {
        assertThat("Wrong user role.", comboBoxRole.getSelectedText(),
            is(equalTo(expectedProfileData.getRole())));
      }
      if (expectedProfileData.getClient() != null) {
        assertThat("Wrong user client.", comboBoxClient.getSelectedText(),
            is(equalTo(expectedProfileData.getClient())));
      }
      if (expectedProfileData.getOrganization() != null) {
        assertThat("Wrong user organization.", comboBoxOrganization.getSelectedText(),
            is(equalTo(expectedProfileData.getOrganization())));
      }
      if (expectedProfileData.getWarehouse() != null) {
        assertThat("Wrong user warehouse.", comboBoxWarehouse.getSelectedText(),
            is(equalTo(expectedProfileData.getWarehouse())));
      }
      if (expectedProfileData.getLanguage() != null) {
        assertThat("Wrong user languaje.", comboBoxLanguage.getSelectedText(),
            is(equalTo(expectedProfileData.getLanguage())));
      }
    }

    /**
     * Assert that the number of available organizations is the expected.
     *
     * @param count
     *          The expected number of available organizations.
     */
    public void assertOrganizationsCount(int count) {
      assertThat("Wrong number of organizations.", comboBoxOrganization.getItemCount(),
          is(equalTo(count)));
    }

    /**
     * Assert that the available organizations are the expected.
     *
     * @param organizations
     *          An array with the names of the expected organizations.
     */
    public void assertOrganizations(String[] organizations) {
      String currentOrganization = comboBoxOrganization.getSelectedText();
      for (String organization : organizations) {
        comboBoxOrganization.selectItem(organization);
      }
      comboBoxOrganization.selectItem(currentOrganization);
    }

    /**
     * Assert that the number of available warehouses is the expected.
     *
     * @param count
     *          The expected number of available warehouses.
     */
    public void assertWarehousesCount(int count) {
      assertThat("Wrong number of warehouses.", comboBoxWarehouse.getItemCount(),
          is(equalTo(count)));
    }

    /**
     * Assert that the available warehouses are the expected.
     *
     * @param warehouses
     *          An array with the names of the expected warehouses.
     */
    public void assertWarehouses(String[] warehouses) {
      String currentWarehouse = comboBoxWarehouse.getSelectedText();
      for (String warehouse : warehouses) {
        comboBoxWarehouse.selectItem(warehouse);
      }
      comboBoxWarehouse.selectItem(currentWarehouse);
    }

    /**
     * Get current user profile information.
     */
    ProfileData getProfile() {
      comboBoxRole.waitUntilVisible();
      final ProfileData currentProfileData = new ProfileData.Builder()
          .role(comboBoxRole.getSelectedText())
          .client(comboBoxClient.getSelectedText())
          .organization(comboBoxOrganization.getSelectedText())
          .warehouse(comboBoxWarehouse.getSelectedText())
          .language(comboBoxLanguage.getSelectedText())
          .build();
      return currentProfileData;
    }
  }

  /**
   * Executes and verify actions on OpenbravoERP password tab, added by the Navigation Bar
   * Components module.
   *
   * @author elopio
   *
   */
  public class PasswordTab {

    /** The title of the tab. */
    private final static String TAB_TITLE = "Change Password";

    /* Component elements. */
    /** The current password text field. */
    private final TextItem textBoxCurrentPassword;
    /** The new password text field. */
    private final TextItem textBoxNewPassword;
    /** The confirm password text field. */
    private final TextItem textBoxConfirmPassword;
    /** The save button. */
    private final Button buttonSave;
    /** The cancel button. */
    private final Button buttonCancel;

    /* Registry keys. */
    /** Registry key of the current password text box. */
    private static final String REGISTRY_KEY_TEXT_BOX_CURRENT_PASSWORD = "org.openbravo.client.application.navigationbarcomponents.UserProfilePassword.CurrentPasswordField";
    /** Registry key of the new password text box. */
    private static final String REGISTRY_KEY_TEXT_BOX_NEW_PASSWORD = "org.openbravo.client.application.navigationbarcomponents.UserProfilePassword.NewPasswordField";
    /** Registry key of the confirm password text box. */
    private static final String REGISTRY_KEY_TEXT_BOX_CONFIRM_PASSWORD = "org.openbravo.client.application.navigationbarcomponents.UserProfilePassword.ConfirmPasswordField";
    /** Registry key of the save button. */
    private static final String REGISTRY_KEY_BUTTON_SAVE = "org.openbravo.client.application.navigationbarcomponents.UserProfilePassword.SaveButton";
    /** Registry key of the cancel button. */
    private static final String REGISTRY_KEY_BUTTON_CANCEL = "org.openbravo.client.application.navigationbarcomponents.UserProfilePassword.CancelButton";

    /**
     * Formatting string used to get the error icon identifier. The parameter is the object string
     * of the field.
     */
    private static final String FORMAT_SMART_CLIENT_GET_ERROR_ICON_IDENTIFIER = "%s.getErrorIconId()";
    /**
     * Formatting string used to check if the text item has errors. The parameter is the object
     * string of the text item.
     */
    private static final String FORMAT_SMART_CLIENT_HAS_ERRORS = "%s.hasErrors()";

    /**
     * Class constructor.
     *
     */
    private PasswordTab() {
      this.textBoxCurrentPassword = new TextItem(
          TestRegistry.getObjectString(REGISTRY_KEY_TEXT_BOX_CURRENT_PASSWORD));
      this.textBoxNewPassword = new TextItem(
          TestRegistry.getObjectString(REGISTRY_KEY_TEXT_BOX_NEW_PASSWORD));
      this.textBoxConfirmPassword = new TextItem(
          TestRegistry.getObjectString(REGISTRY_KEY_TEXT_BOX_CONFIRM_PASSWORD));
      this.buttonSave = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_SAVE));
      this.buttonCancel = new Button(TestRegistry.getObjectString(REGISTRY_KEY_BUTTON_CANCEL));
    }

    /**
     * Check if the save button is enabled.
     *
     * @return true if the save button is enabled. Otherwise, false.
     */
    public boolean isSaveButtonEnabled() {
      return buttonSave.isEnabled();
    }

    /**
     * Fill the password form.
     *
     * @param password
     *          The password data.
     */
    public void fill(ChangePasswordData password) {
      logger.info("Filling the password form with {}.", password::toString);
      textBoxCurrentPassword.waitUntilVisible();
      if (password.getCurrentPassword() != null) {
        textBoxCurrentPassword.setValue(password.getCurrentPassword());
      }
      if (password.getNewPassword() != null) {
        textBoxNewPassword.setValue(password.getNewPassword());
      }
      if (password.getNewPasswordConfirmation() != null) {
        textBoxConfirmPassword.setValue(password.getNewPasswordConfirmation());
      }
    }

    /**
     * Cancel the password change.
     */
    public void cancel() {
      buttonCancel.smartClientClick();
    }

    /**
     * Change the user password.
     *
     * @param password
     *          The data for the password change.
     */
    public void changePassword(ChangePasswordData password) {
      logger.info("Change the password information to {}.", password);
      fill(password);
      buttonSave.smartClientClick();
    }

    /**
     * Get the identifier of the confirmation error icon.
     *
     * @return the identifier of the confirmation error icon.
     */
    @SuppressWarnings("unused")
    private String getConfirmationErrorIconId() {
      String objectStringTextItemConfirmPassword = TestRegistry
          .getObjectString(REGISTRY_KEY_TEXT_BOX_CONFIRM_PASSWORD);
      return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(String.format(
          FORMAT_SMART_CLIENT_GET_ERROR_ICON_IDENTIFIER, objectStringTextItemConfirmPassword));
    }

    /**
     * Check if the password error is visible.
     *
     * @return true if the password error is visible. Otherwise, false.
     *
     */
    public boolean isPasswordErrorVisible() {
      // TODO we are not verifying the icon. XT_BOX_CONFIRM_PASSWORD)),
      // getConfirmationErrorIconId())))
      // .isDisplayed();
      String objectStringCurrentPassword = TestRegistry
          .getObjectString(REGISTRY_KEY_TEXT_BOX_CURRENT_PASSWORD);
      return (Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          String.format(FORMAT_SMART_CLIENT_HAS_ERRORS, objectStringCurrentPassword));
    }

    /**
     * Check if the new password error is visible.
     *
     * @return true if the new password error is visible. Otherwise, false.
     *
     */
    public boolean isNewPasswordErrorVisible() {
      return (Boolean) SeleniumSingleton.INSTANCE
          .executeScript(String.format("return window.OB.TestRegistry.registry['%s'].hasErrors()",
              REGISTRY_KEY_TEXT_BOX_NEW_PASSWORD));
    }

    /**
     * Check if the password confirmation error is visible.
     *
     * @return true if the password confirmation error is visible. Otherwise, false.
     *
     */
    public boolean isPasswordConfirmationErrorVisible() {
      // TODO we are not verifying the icon. It always says that is visible.
      // return ((RenderedWebElement)
      // SeleniumSingleton.INSTANCE.findElementByScLocator(String.format(
      // "%s/[icon='%s']", SeleniumSingleton.INSTANCE.executeScript(String.format(
      // "return window.OB.TestRegistry.registry['%s'].form.getLocator()",
      // REGISTRY_KEY_TEXT_BOX_CONFIRM_PASSWORD)), getConfirmationErrorIconId())))
      // .isDisplayed();
      return (Boolean) SeleniumSingleton.INSTANCE
          .executeScript(String.format("return window.OB.TestRegistry.registry['%s'].hasErrors()",
              REGISTRY_KEY_TEXT_BOX_CONFIRM_PASSWORD));
    }

  }

}
