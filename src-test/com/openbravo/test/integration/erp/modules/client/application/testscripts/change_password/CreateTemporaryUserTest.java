/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2018 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.client.application.testscripts.change_password;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserData;
import com.openbravo.test.integration.erp.data.generalsetup.security.user.UserRolesData;
import com.openbravo.test.integration.erp.gui.LoginPage;
import com.openbravo.test.integration.erp.gui.general.security.user.UserRolesTab;
import com.openbravo.test.integration.erp.gui.general.security.user.UserTab;
import com.openbravo.test.integration.erp.gui.general.security.user.UserWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.util.ConfigurationProperties;

public abstract class CreateTemporaryUserTest extends OpenbravoERPTest {

  public CreateTemporaryUserTest() {
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  protected LoginPage loginWith(LogInData data) {
    final LoginPage loginPage = new LoginPage();
    loginPage.open();
    loginPage.logIn(data);

    return loginPage;
  }

  protected void createTestUser(String username, String password, String role) {
    createTestUser(new UserData.Builder().username(username)
        .name(username)
        .password(password)
        .isPasswordExpired(false)
        .build(), role);
  }

  protected void createTestUserWithExpiredPassword(String username, String password, String role) {
    createTestUser(new UserData.Builder().username(username)
        .name(username)
        .password(password)
        .isPasswordExpired(true)
        .build(), role);
  }

  protected void createTestUser(UserData data, String role) {
    UserWindow userWindow = new UserWindow();
    mainPage.openView(userWindow);

    UserTab userTab = userWindow.selectUserTab();

    userTab.createRecord(data);

    UserRolesTab rolesTab = userWindow.selectUserRolesTab();
    rolesTab.createRecord(new UserRolesData.Builder().role(role).build());
  }
}
