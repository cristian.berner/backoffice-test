/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Arun Kumar <arun.kumar@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the launch of a view from the menu widget added by Navigation Bar Components module.
 *
 * @author elopio
 *
 */
public abstract class LaunchViewFromMenuWidget extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The item path to select from the menu. */
  private final String[] items;

  /**
   * Class constructor.
   *
   * @param items
   *          the path of the item to select.
   */
  public LaunchViewFromMenuWidget(String[] items) {
    this.items = items;
  }

  /**
   * Test launch view selecting its path from the menu. Testlink: Corresponds to [NAV0010a] Open
   * window selecting its path from the menu
   */
  @Test
  public void viewShouldOpenSelectingPath() {
    logger.info("[NAV0010a] Open window selecting its path from the menu");
    Menu componentMenu = mainPage.getNavigationBar().getComponentMenu();
    componentMenu.select(items);
    String windowName = items[items.length - 1];
    mainPage.getTabSetMain().waitUntilTabIsOpen(windowName);

    logger.info("[NAV0010b] Open window from recent views in the menu");
    Sleep.trySleep(1200);
    mainPage.getTabSetMain().closeTab(windowName);
    logger.info("Close window");
    mainPage.getTabSetMain().waitUntilTabIsClosed(windowName);
    logger.info("Open window from recent views within the menu");
    componentMenu.select(windowName);
    mainPage.getTabSetMain().waitUntilTabIsOpen(windowName);
  }
}
