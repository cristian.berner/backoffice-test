/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.alert;

import org.junit.Test;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test the opening and closing of an Alert.
 *
 * @author rwngallego
 *
 */
public abstract class OpenAlertsFromNavigationBar extends OpenbravoERPTest {

  // FIXME It does not work with this window in particular. So, its problem is with the name of the
  // tab.
  private static final String WINDOW_NAME_ALERT_MANAGEMENT = "Alert Management";

  /**
   * Test the alert button of the Navigation Bar. NAV0090 Open Alerts window
   */
  @Test
  public void alertShouldBeOpenedAndClosed() {
    mainPage.getNavigationBar().openAlerts();
    mainPage.getTabSetMain().waitUntilTabIsOpen(WINDOW_NAME_ALERT_MANAGEMENT);
  }
}
