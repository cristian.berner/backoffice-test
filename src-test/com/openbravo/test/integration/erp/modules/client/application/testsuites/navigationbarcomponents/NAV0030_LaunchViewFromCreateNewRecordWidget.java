/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.navigationbarcomponents;

import java.util.Arrays;
import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.createnew.LaunchViewFromCreateNewRecordWidget;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test launching a view from the Create New widget added by Navigation Bar Components module.
 *
 * @author Leo Arias
 */
@RunWith(Parameterized.class)
public class NAV0030_LaunchViewFromCreateNewRecordWidget
    extends LaunchViewFromCreateNewRecordWidget {

  /**
   * Class constructor.
   *
   * @param standardWindow
   *          The standard window that will be opened on create new mode.
   */
  public NAV0030_LaunchViewFromCreateNewRecordWidget(StandardWindow standardWindow) {
    super(standardWindow);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> createNewRecordValues() {
    return Arrays.asList(new Object[][] { { new UnitOfMeasureWindow() } });
  }

  @BeforeClass
  public static void beginning() {
  }
}
