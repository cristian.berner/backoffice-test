/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.junit.Test;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Canvas;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Tests to delete a row in the grid.
 *
 * @author AugustoMauch
 *
 */
public abstract class MoveToNextRecordWithTabKeyWhileEditingInGrid extends OpenbravoERPTest {
  /** The first data object. */
  protected final DataObject firstDataObject;
  /** The second data object. */
  protected final DataObject secondDataObject;
  /** The data object to be used in the filtering. */
  private final DataObject dataObjectForFiltering;

  /**
   * Class constructor.
   *
   * @param newDataObject
   *          The new data object.
   */
  /**
   * @param firstDataObject
   *          The new data object.
   * @param secondDataObject
   *          The new data object.
   * @param dataObjectForFiltering
   *          The new data object.
   */
  public MoveToNextRecordWithTabKeyWhileEditingInGrid(DataObject firstDataObject,
      DataObject secondDataObject, DataObject dataObjectForFiltering) {
    this.firstDataObject = firstDataObject;
    this.secondDataObject = secondDataObject;
    this.dataObjectForFiltering = dataObjectForFiltering;
  }

  /**
   * Test to move to the next record while editing in grid.
   */
  @Test
  public void nextRecordShouldSelectedForEdition() {
    GeneratedTab<DataObject> tab = getTab();
    tab.select(dataObjectForFiltering);
    DataObject currentData = tab.getData();
    tab.switchToGridView();
    DataObject nextRecord = null;
    if (isContained(firstDataObject, currentData)) {
      nextRecord = secondDataObject;
    } else {
      nextRecord = firstDataObject;
    }
    tab.startEditionOnGrid(0);
    moveToNextRecordWithTabKey(tab);
    tab.assertInEditMode();
    tab.cancelOnGrid();
    tab.assertData(nextRecord);
    cleanUp(tab);
  }

  public void cleanUp(GeneratedTab<DataObject> tab) {
    tab.select(firstDataObject);
    tab.deleteOnGrid();
    tab.select(secondDataObject);
    tab.deleteOnGrid();
  }

  private void moveToNextRecordWithTabKey(GeneratedTab<DataObject> tab) {
    boolean onlyAllowTextItems = false;
    InputField lastField = tab.getField("default", onlyAllowTextItems);
    if (lastField instanceof Canvas) {
      lastField.focus();
      Canvas lastFieldCanvas = (Canvas) lastField;
      lastFieldCanvas.pressTab();
    }
  }

  private boolean isContained(DataObject dataSubset, DataObject data) {
    boolean isContained = true;
    LinkedHashMap<String, Object> hashMapDataSubset = dataSubset.getDataFields();
    LinkedHashMap<String, Object> hashMapData = data.getDataFields();
    Set<String> keySet = hashMapDataSubset.keySet();
    Iterator<String> keyIterator = keySet.iterator();
    while (keyIterator.hasNext() && isContained) {
      String nextKey = keyIterator.next();
      String firstValue = (String) hashMapDataSubset.get(nextKey);
      String secondValue = (String) hashMapData.get(nextKey);
      isContained = firstValue.equals(secondValue);
    }
    return isContained;
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  public abstract GeneratedTab<DataObject> getTab();
}
