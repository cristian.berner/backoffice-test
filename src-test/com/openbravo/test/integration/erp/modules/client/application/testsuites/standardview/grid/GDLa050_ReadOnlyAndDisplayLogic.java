/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.AccountTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.FinancialAccountWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.financialaccount.TransactionTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * This tests ensures that read only works properly combined with display logic in grid view. This
 * test is created to cover this regression #32240.
 *
 * @author inigo.sanchez
 */
@RunWith(Parameterized.class)
public class GDLa050_ReadOnlyAndDisplayLogic extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String CURRENCY_FIELD = "currency";

  // Data for [GDLa050]
  AccountData accountData;
  TransactionsData transactionsData;

  /**
   * Class constructor.
   *
   */
  public GDLa050_ReadOnlyAndDisplayLogic(AccountData accountData,
      TransactionsData transactionsData) {
    this.accountData = accountData;
    this.transactionsData = transactionsData;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return Collection of AccountData arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> assetsValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Cuenta de Banco").build(),
        new TransactionsData.Builder().documentNo("1000142").build() } };
    return Arrays.asList(data);
  }

  /**
   * This test case checks that read only works properly combined with display logic in grid view.
   * This test select "Cuenta de Banco" record in Financial Account window. Then go to Transaction
   * subtab and select any processed transaction and verify if Currency cell have its correct
   * visibility.
   *
   * This test is created to cover this regression #32240.
   *
   */
  @Test
  public void testReadOnlyAndDisplayLogic() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [GDLa050]**");

    // Open Financial Account window.
    FinancialAccountWindow financialAccountWindow = new FinancialAccountWindow();
    mainPage.openView(financialAccountWindow);
    AccountTab financialAccountHeaderTab = (AccountTab) financialAccountWindow.selectAccountTab();
    financialAccountWindow.selectAccountTab();

    // Select "Cuenta de Banco" record in header tab and go to Transaction subtab.
    financialAccountHeaderTab.select(accountData);
    TransactionTab transactionTab = (TransactionTab) financialAccountWindow
        .selectTab(TransactionTab.IDENTIFIER);

    // Select and start editing record and check if "currency" field is displayed as read only
    transactionTab.select(transactionsData);
    transactionTab.startEditionOnGrid(0);
    assertThat("Currency visibility is not correct.",
        transactionTab.getField(CURRENCY_FIELD, false).isEnabled(), is(false));

    // Save record.
    transactionTab.saveEdition();
    transactionTab.assertSaved();
    logger.info("** End of test case [GDLa050]**");
  }

}
