/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.HeaderTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.LinesTab;
import com.openbravo.test.integration.erp.gui.sales.transactions.salesorder.SalesOrderWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid.FilterRecords;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the grid filtering by product selector
 *
 * @author rwngallego
 */
@RunWith(Parameterized.class)
public class GRD0040_FilterRecordsByProductSelector extends FilterRecords {

  /**
   * Class constructor.
   *
   * @param filterDataObject
   *          The filter data object.
   */
  public GRD0040_FilterRecordsByProductSelector(DataObject filterDataObject) {
    super(filterDataObject);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<DataObject[]> filterValues() {
    DataObject[][] data = new DataObject[][] { { new SalesOrderLinesData.Builder()
        .product(new ProductSelectorData.Builder().name("Cerveza Lager 0,5L").build())
        .build() } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B International Group Admin - F&B International Group")
            .build());
    SalesOrderWindow salesOrderWindow = new SalesOrderWindow();
    mainPage.openView(salesOrderWindow);
    HeaderTab headerTab = (HeaderTab) salesOrderWindow.selectTab(HeaderTab.IDENTIFIER);
    headerTab.select(new SalesOrderHeaderData.Builder().documentNo("PR/1").build());
    return (GeneratedTab<DataObject>) salesOrderWindow.selectTab(LinesTab.IDENTIFIER);
  }
}
