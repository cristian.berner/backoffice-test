/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.help;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.HelpWidget;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;

/**
 * Test that the Help widget added by Navigation Bar Components module can be properly displayed and
 * hidden.
 *
 * @author elopio
 *
 */
public class DisplayAndHideHelpWidget extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test the display and hide help component. [NAV0130] Display and hide Help component
   */
  @Test
  public void createNewWidgetShouldBeDisplayedAndHidden() {
    HelpWidget componentHelp = mainPage.getNavigationBar().getComponentHelp();
    logger.info("[NAV0130] Display and hide Help component");
    componentHelp.open();
    componentHelp.verifyOpen();
    componentHelp.close();
    componentHelp.verifyClosed();
  }

}
