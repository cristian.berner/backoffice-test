/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.navigationbarcomponents.help;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.HelpWidget;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the opening of the Help widget without windows opened.
 *
 * @author rwngallego
 *
 */
public abstract class OpenHelpWidgetWithNoWindowsOpen extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Test the help button of the Navigation Bar [NAV0100] Open Help component Verify the help link
   * is not present but just the about link when there is no windows open
   */
  @Test
  public void helpLinkShouldNotBeShown() {
    HelpWidget componentHelpWidget = mainPage.getNavigationBar().getComponentHelp();
    logger.info("[NAV0100] Open Help component");
    componentHelpWidget.open();
    componentHelpWidget.verifyAboutLinkVisible();
    logger.info("Verifying About component is present");
    componentHelpWidget.verifyHelpLinkNotVisible();
    logger.info("Verifying Help component is not present");
    logger.info("Closing Help component");
    // TODO L10: Remove following static sleep. It was not required in gecko branch but it was in
    // stable one. Check it.
    Sleep.trySleep();
    componentHelpWidget.close();
    // TODO L10: Remove following static sleep. It was not required in gecko branch but it was in
    // stable one. Check it.
    Sleep.trySleep();
  }
}
