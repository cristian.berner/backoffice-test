/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2012-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute;

import static org.junit.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.modules.client.application.gui.GridInputFieldFactory;
import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.PopUpWindow;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP pick and execute pop up window.
 *
 * @author plujan
 * @param <T>
 *          The data object of the selector.
 *
 */
public class PickAndExecuteWindow<T extends DataObject> extends PopUpWindow {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Grid and Cancel button are the only standard objects.
   */
  protected final static String REGISTRY_KEY_GRID = "org.openbravo.client.application.process.pickandexecute.Grid";
  protected final static String REGISTRY_KEY_DONE_BUTTON = "org.openbravo.client.application.process.pickandexecute.button.ok";
  protected final static String REGISTRY_KEY_CANCEL_BUTTON = "org.openbravo.client.application.process.pickandexecute.button.cancel";
  protected final static String REGISTRY_KEY_NEW_VERSION_BUTTON = "org.openbravo.client.application.process.pickandexecute.button.newVersion";
  private static final String FORMAT_SMART_CLIENT_GRID_FIELD = "%s.getEditForm().getField('%s')";

  /* Components */
  /** The selector grid. */
  private Grid grid;

  /** The Done button. */
  private Button doneButton;

  /** The Cancel button. */
  private Button cancelButton;

  /** The new Version button. */
  private Button newVersionButton;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public PickAndExecuteWindow(String objectString) {
    super(objectString);
    grid = new Grid(TestRegistry.getObjectString(REGISTRY_KEY_GRID));
  }

  protected PickAndExecuteWindow(String objectString, String gridId) {
    super(objectString);
    grid = new Grid(TestRegistry.getObjectString(gridId));
  }

  public void select(String element, String... values) {
    for (int i = 0; i < values.length; i++) {
      logger.debug("Looking the row containing '{}'", values[i]);
      Sleep.trySleep();
      grid.filter(element, values[i]);
      Sleep.trySleep();
      try {
        grid.selectRowByContents(element, values[i]);
      } catch (StaleElementReferenceException e) {
        grid.selectRowByContents(element, values[i]);
      } catch (NoSuchElementException snee) {
        Sleep.trySleep(5000);
        grid.selectRowByContents(element, values[i]);
      }
    }
  }

  public void selectAll() {
    grid.selectAll();
  }

  public void unselectAll() {
    grid.unselectAll();
  }

  public void process() {
    logger.debug("Processing the selection.");
    if (doneButton == null) {
      doneButton = new Button(TestRegistry.getObjectString(REGISTRY_KEY_DONE_BUTTON));
    }
    doneButton.click();
  }

  public void cancel() {
    logger.debug("Canceling the selection.");
    if (cancelButton == null) {
      cancelButton = new Button(TestRegistry.getObjectString(REGISTRY_KEY_CANCEL_BUTTON));
    }
    cancelButton.click();
  }

  public void newVersion() {
    logger.debug("Creating a new version with the selected data.");
    if (newVersionButton == null) {
      newVersionButton = new Button(TestRegistry.getObjectString(REGISTRY_KEY_NEW_VERSION_BUTTON));
    }
    newVersionButton.click();
  }

  public void filter(T data) {
    grid.waitForDataToLoad();
    internalFilter(data, true);
  }

  private void internalFilter(T data, boolean unselect) {
    if (unselect) {
      grid.unselectAll();
    }
    grid.clearFilters();
    LinkedHashMap<String, Object> fields = data.getDataFields();
    for (String fieldName : fields.keySet()) {
      grid.pickAndExecutefilter(fieldName, fields.get(fieldName).toString());
    }
    Sleep.trySleep();
    grid.selectAll();
  }

  public void filterAndKeepSelection(T data) {
    internalFilter(data, false);
  }

  @Override
  public void focus() {
    grid.focus();
  }

  public void edit(T data, int rowNumber) {
    try {
      grid.scrollToLeft();
      Sleep.trySleep();
      grid.clickCellByRowNumber(rowNumber, data.getNextFieldName());
    } catch (Exception e) {
      grid.scrollToRight();
      Sleep.trySleep();
      grid.clickCellByRowNumber(rowNumber, data.getNextFieldName());
    }
    LinkedHashMap<String, Object> fields = data.getDataFields();
    for (String fieldName : fields.keySet()) {
      InputField field;
      try {
        field = GridInputFieldFactory.getInputFieldObject(
            String.format(FORMAT_SMART_CLIENT_GRID_FIELD, grid.getObjectString(), fieldName),
            false);
      } catch (JavascriptException jse) {
        logger.info(
            "JavascriptException has been thrown. Let's scroll to right in case it is not visible");
        Sleep.trySleep();
        grid.scrollToRight();
        field = GridInputFieldFactory.getInputFieldObject(
            String.format(FORMAT_SMART_CLIENT_GRID_FIELD, grid.getObjectString(), fieldName),
            false);
      }
      field.setValue(fields.get(fieldName));
    }
  }

  public void edit(T data) {
    edit(data, 0);
  }

  /**
   * Get rows on grid
   *
   * @return List of rows on grid
   */
  @SuppressWarnings("unchecked")
  public List<Map<String, Object>> getRows() {
    return (List<Map<String, Object>>) grid.getRows();
  }

  /**
   * Get selected rows on grid
   *
   * @return List of rows on grid
   */
  @SuppressWarnings("unchecked")
  public List<Map<String, Object>> getSelectedRows() {
    return (List<Map<String, Object>>) grid.getSelectedRows();
  }

  /**
   * Verifies there is an error message in the message bar
   *
   * @param errorMessage
   *          The error message shown in the message bar
   */
  public void assertError(String errorMessage) {
    Boolean messageBarVisible = (Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        "dom=window.OB.TestRegistry.registry['org.openbravo.client.application.process.pickandexecute.popup'].view.messageBar.isVisible()");
    if (messageBarVisible) {
      String messageClass = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
          "dom=window.OB.TestRegistry.registry['org.openbravo.client.application.process.pickandexecute.popup'].view.messageBar._clipDiv.className");
      if (messageClass.equals("OBMessageBar_error")) {
        String messageText = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            "dom=window.OB.TestRegistry.registry['org.openbravo.client.application.process.pickandexecute.popup'].view.messageBar.members[1].contents");
        assertTrue(messageText.contains(errorMessage));
        return;
      }
    }
    assertTrue(false);
  }

  /**
   * Clear all grid filters
   */
  public void clearFilters() {
    grid.clearFilters();
  }

  public void selectRecord(int rowNumber) {
    grid.selectRecord(rowNumber);
  }

}
