/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.form;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.assets.assets.AssetsData;
import com.openbravo.test.integration.erp.data.financial.assets.assets.AssetsDisplayLogic;
import com.openbravo.test.integration.erp.gui.financial.assets.AssetsHeaderTab;
import com.openbravo.test.integration.erp.gui.financial.assets.AssetsWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * This tests ensures that display logic works properly in form view.
 *
 * @author inigo.sanchez
 */
@RunWith(Parameterized.class)
public class GDLb010_DisplayLogicInForm extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  // Data for [GDLb010]
  AssetsData assetsData;
  AssetsDisplayLogic assetsDataDisplayLogic;
  AssetsData assetsDataDepreciated;
  AssetsDisplayLogic assetsDataDepreciatedDisplayLogic;
  AssetsData assetsDataCalculateType;
  AssetsDisplayLogic assetsDataCalculateTypeDisplayLogic;
  AssetsData assetsDataMonthsAndAmortize;
  AssetsDisplayLogic assetsDataMonthsAndAmortizeDisplayLogic;
  AssetsData assetsDataYearly;
  AssetsDisplayLogic assetsDataYearlyDisplayLogic;

  /**
   * Class constructor
   *
   * @param assetsData
   * @param assetsDataDisplayLogic
   * @param assetsDataDepreciated
   * @param assetsDataDepreciatedDisplayLogic
   * @param assetsDataCalculateType
   * @param assetsDataCalculateTypeDisplayLogic
   * @param assetsDataMonthsAndAmortize
   * @param assetsDataMonthsAndAmortizeDisplayLogic
   * @param assetsDataYearly
   * @param assetsDataYearlyDisplayLogic
   */
  public GDLb010_DisplayLogicInForm(AssetsData assetsData,
      AssetsDisplayLogic assetsDataDisplayLogic, AssetsData assetsDataDepreciated,
      AssetsDisplayLogic assetsDataDepreciatedDisplayLogic, AssetsData assetsDataCalculateType,
      AssetsDisplayLogic assetsDataCalculateTypeDisplayLogic,
      AssetsData assetsDataMonthsAndAmortize,
      AssetsDisplayLogic assetsDataMonthsAndAmortizeDisplayLogic, AssetsData assetsDataYearly,
      AssetsDisplayLogic assetsDataYearlyDisplayLogic) {
    this.assetsData = assetsData;
    this.assetsDataDisplayLogic = assetsDataDisplayLogic;
    this.assetsDataDepreciated = assetsDataDepreciated;
    this.assetsDataDepreciatedDisplayLogic = assetsDataDepreciatedDisplayLogic;
    this.assetsDataCalculateType = assetsDataCalculateType;
    this.assetsDataCalculateTypeDisplayLogic = assetsDataCalculateTypeDisplayLogic;
    this.assetsDataMonthsAndAmortize = assetsDataMonthsAndAmortize;
    this.assetsDataMonthsAndAmortizeDisplayLogic = assetsDataMonthsAndAmortizeDisplayLogic;
    this.assetsDataYearly = assetsDataYearly;
    this.assetsDataYearlyDisplayLogic = assetsDataYearlyDisplayLogic;

    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return Collection of AssetsData arrays with data for the test
   *
   */

  @Parameters
  public static Collection<Object[]> assetsValues() {
    Object[][] data = new Object[][] {
        { new AssetsData.Builder().searchKey("TestAssetForm").name("Test Asset Form").build(),
            new AssetsDisplayLogic.Builder().depreciate(true)
                .calculateType(false)
                .amortize(false)
                .usableLifeMonths(false)
                .usableLifeYears(false)
                .build(),
            new AssetsData.Builder().depreciate(true).build(),
            new AssetsDisplayLogic.Builder().depreciate(true)
                .calculateType(true)
                .amortize(false)
                .usableLifeMonths(false)
                .usableLifeYears(false)
                .build(),
            new AssetsData.Builder().calculateType("Time").build(),
            new AssetsDisplayLogic.Builder().depreciate(true)
                .calculateType(true)
                .amortize(true)
                .usableLifeMonths(true)
                .usableLifeYears(false)
                .build(),
            new AssetsData.Builder().usableLifeMonths("12").amortize("Yearly").build(),
            new AssetsDisplayLogic.Builder().depreciate(true)
                .calculateType(true)
                .amortize(true)
                .usableLifeMonths(false)
                .usableLifeYears(true)
                .build(),
            new AssetsData.Builder().usableLifeYears("0").build(),
            new AssetsDisplayLogic.Builder().depreciate(true)
                .calculateType(true)
                .amortize(true)
                .usableLifeMonths(false)
                .usableLifeYears(true)
                .build() } };
    return Arrays.asList(data);
  }

  /**
   * This test case checks that display logic works properly in form view. This test create a record
   * in Assets window and verify if some fields have their correct visibility.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void testDisplayLogicForm() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [GDLb010]**");

    // Open Assets window and select header tab.
    AssetsWindow assetsWindow = new AssetsWindow();
    mainPage.openView(assetsWindow);
    AssetsHeaderTab assetsHeaderTab = (AssetsHeaderTab) assetsWindow
        .selectTab(AssetsHeaderTab.IDENTIFIER);

    // Create a new asset in form view for testing.
    assetsHeaderTab.createRecord(assetsData);
    assetsHeaderTab.assertSaved();

    // Check that visibility is correct. Depreciate field should be shown.
    assetsHeaderTab.assertDisplayLogicVisible(assetsDataDisplayLogic);

    // Select Depreciate checkbox and check that visibility is correct.
    assetsHeaderTab.editOnForm(assetsDataDepreciated);
    assetsHeaderTab.assertDisplayLogicVisible(assetsDataDepreciatedDisplayLogic);

    // Select Time value in Calculate type selector and check that visibility is correct.
    assetsHeaderTab.editOnForm(assetsDataCalculateType);
    assetsHeaderTab.assertDisplayLogicVisible(assetsDataCalculateTypeDisplayLogic);

    // Write a number value in usableLifeMonths and select Yearly value in Amortize selector.
    // Check that visibility is correct.
    assetsHeaderTab.editOnForm(assetsDataMonthsAndAmortize);
    assetsHeaderTab.assertDisplayLogicVisible(assetsDataMonthsAndAmortizeDisplayLogic);

    // Write a number value in usableLifeYears and check that visibility is correct.
    assetsHeaderTab.editOnForm(assetsDataYearly);
    assetsHeaderTab.assertDisplayLogicVisible(assetsDataYearlyDisplayLogic);
    assetsHeaderTab.assertSaved();
    logger.info("** End of test case [GDLb010]**");
  }
}
