/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Inigo Sanchez <inigo.sanchez@openbravo.com>.
 *  Nono Carballo <f.carballo@nectus.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.DateItem;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Grid;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.InputField;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.TextItem;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP grids built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class OBGrid extends Grid {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to get the grid from the registry. The parameter is the tab identifier.
   */
  private static final String FORMAT_REGISTRY_KEY_GRID = "org.openbravo.client.application.ViewGrid_%s";

  /**
   * Formatting string used to get the total rows of the grid. The parameter is the object string.
   */
  @SuppressWarnings("unused")
  private final static String FORMAT_SMART_CLIENT_GET_TOTAL_ROWS = "%s.data.totalRows";
  // XXX do not use indexes for the buttons.
  /**
   * Formatting string to locate the grid buttons by row number. The first parameter is the grid
   * locator. The second parameter is the row number. The third parameter is the index of the
   * button.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_ROW_NUMBER = "%s/body/child[index=%d]/member[index=0]/member[index=%d]/";
  /**
   * Formatting string to locate the grid buttons by the last selected record. The first parameter
   * is the object string. The second parameter is the index of the button.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_LAST_SELECTION = "%s.selection.lastSelectionItem.editColumnLayout.children[%d].getLocatorRoot()";
  /**
   * Formatting string used to the grid clear filter button. The parameter is the object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_BUTTON_CLEAR_FILTER = "%s.filterEditor.filterImage";
  /** The index of the row count column on the filter. */
  private static final String INDEX_ROW_COUNT = "1";
  /**
   * Formatting string used to get the edit form. The parameter is the object string of the grid.
   */
  private static final String FORMAT_SMART_CLIENT_GET_EDIT_FORM = "%s.getEditForm()";
  /**
   * Formatting string used to get a field by its name or index. The first parameter is object
   * string of the edit form. The second parameter can be the field name or the field index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_EDIT_FORM_FIELD = "%s.getField(%s)";

  /**
   * Formatting string used get rows on grid
   */
  private static final String FORMAT_SMART_CLIENT_GRID_GET_DATA = "dom=" + "(function(){"
      + "  var result = [];" + "  var x = {};"
      + "  %s.getData().localData.forEach(function(data, index, all){" + "    for(k in data){"
      + "      if(data[k] != null && typeof data[k] != 'function'){"
      + "        x[k]=data[k].toString();" + "      }else{" + "        x[k]=null;" + "      }"
      + "    }" + "    result.push(x);" + "  });" + "  return result;" + "})()";

  /**
   * Formatting string used get rows on grid
   */
  private static final String FORMAT_SMART_CLIENT_GRID_GET_SELECTED_DATA = "%s.getSelectedRecords()";

  /** Index of the edit button. */
  private final static int INDEX_BUTTON_EDIT = 2;
  /** Index of the open button. */
  private final static int INDEX_BUTTON_OPEN = 0;
  /** Index of the cancel edit button. */
  private final static int INDEX_BUTTON_CANCEL_EDIT = 3;
  /** Index of the save button. */
  private final static int INDEX_BUTTON_SAVE = 5;

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   *
   * @param waitForLoad
   *          Boolean to determine if the system should wait the object be ready to continue.
   */
  public OBGrid(String tabIdentifier, Boolean waitForLoad) {
    super(TestRegistry.getObjectString(String.format(FORMAT_REGISTRY_KEY_GRID, tabIdentifier)),
        waitForLoad);
  }

  /**
   * Class constructor.
   *
   * @param tabIdentifier
   *          The tab identifier.
   */
  public OBGrid(String tabIdentifier) {
    this(tabIdentifier, true);
  }

  /**
   * Get the row count displayed by the grid.
   *
   * @return the row count displayed by the grid.
   */
  private String getRowCount() {
    final TextItem rowCountTextItem = new TextItem();
    rowCountTextItem.setSmartClientLocator(String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_INDEX, getFilterLocator(), INDEX_ROW_COUNT));
    return rowCountTextItem.getText();
  }

  /**
   * Get the number of records in the grid.
   *
   * @return The number of records in the grid.
   */
  public int getRecordCount() {
    final String rowCount = getRowCount();
    int recordCount;
    this.waitUntilLoaded();
    if (rowCount.trim().equals("")) {
      recordCount = 0;
    } else if (!rowCount.equals(">100")) {
      recordCount = Integer.parseInt(rowCount);
    } else {
      // FIXME
      return -1;
    }
    return recordCount;
  }

  /**
   * Get the button locator.
   *
   * @param index
   *          The index of the button.
   * @return The edit button locator.
   */
  private String getButtonLocatorByLastSelection(int buttonIndex) {
    // Return result
    String res = new String();
    // Catch forces existence of a last selection item
    try {
      res = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(String
          .format(FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_LAST_SELECTION, objectString, buttonIndex));
      //
    } catch (WebDriverException wde) {
      // TODO L00:It is possible the following sleep is not required
      Sleep.trySleep(4000);
      SeleniumSingleton.INSTANCE
          .executeScriptWithReturn(String.format("%s.selection.selectSingle()", objectString));
      try {
        res = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(String.format(
            FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_LAST_SELECTION, objectString, buttonIndex));
      } catch (JavascriptException jse) {
        Sleep.trySleep(10000);
        // It is possible it is required reorder the following javascript command to work properly
        logger.debug("The order that is going to be executed is: {}", String.format(
            FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_LAST_SELECTION, objectString, buttonIndex));
        res = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(String.format(
            FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_LAST_SELECTION, objectString, buttonIndex));
      }
    }
    return res;
  }

  /**
   * Get the button locator.
   *
   * @param index
   *          The index of the button.
   * @return The edit button locator.
   */
  private String getButtonLocatorByRowNumber(int rowIndex, int buttonIndex) {
    return getSmartClientLocator()
        + String.format(FORMAT_LOCATOR_SMART_CLIENT_BUTTON_BY_ROW_NUMBER, rowIndex, buttonIndex);
  }

  /**
   * Click the edit button of the last selected row.
   */
  public void clickEdit() {
    logger.debug("Clicking the edit button of the last selected row.");
    final String buttonLocator = getButtonLocatorByLastSelection(INDEX_BUTTON_EDIT);
    final Button buttonEdit = new Button();
    buttonEdit.setSmartClientLocator(buttonLocator);
    buttonEdit.click();
    // FIXME wait for data to load
    Sleep.trySleep();
  }

  /**
   * Click the edit button of a row.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void clickEdit(int rowNumber) {
    logger.debug("Clicking the edit button of the row '{}.", rowNumber);
    final String buttonLocator = getButtonLocatorByRowNumber(rowNumber, INDEX_BUTTON_EDIT);
    final Button buttonEdit = new Button();
    buttonEdit.setSmartClientLocator(buttonLocator);
    buttonEdit.click();
  }

  /**
   * Click the open button of the last selected row.
   */
  public void clickOpen() {
    logger.debug("Clicking the open button of the last selected row.");
    String buttonLocator;
    // TODO L1: Testing if the following sleep is required to be able to work properly
    Sleep.trySleep();
    buttonLocator = getButtonLocatorByLastSelection(INDEX_BUTTON_OPEN);
    final Button buttonOpen = new Button();
    buttonOpen.setSmartClientLocator(buttonLocator);
    buttonOpen.click();
  }

  /**
   * Click the open button of a row.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void clickOpen(int rowNumber) {
    logger.debug("Clicking the open button of the row '{}.", rowNumber);
    final String buttonLocator = getButtonLocatorByRowNumber(rowNumber, INDEX_BUTTON_OPEN);
    final Button buttonOpen = new Button();
    buttonOpen.setSmartClientLocator(buttonLocator);
    buttonOpen.click();
  }

  /**
   * Click the cancel edit button of the last selected row.
   */
  public void clickCancelEdit() {
    logger.debug("Clicking the cancel button of the last selected row.");
    final String buttonLocator = getButtonLocatorByLastSelection(INDEX_BUTTON_CANCEL_EDIT);
    final Button buttonCancelEdit = new Button();
    buttonCancelEdit.setSmartClientLocator(buttonLocator);
    buttonCancelEdit.click();
  }

  /**
   * Click the cancel edit button of a row.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void clickCancelEdit(int rowNumber) {
    logger.debug("Clicking the cancel button of the row '{}.", rowNumber);
    final String buttonLocator = getButtonLocatorByRowNumber(rowNumber, INDEX_BUTTON_CANCEL_EDIT);
    final Button buttonCancelEdit = new Button();
    buttonCancelEdit.setSmartClientLocator(buttonLocator);
    buttonCancelEdit.click();
  }

  /**
   * Confirm the edit cancellation.
   */
  public void confirmCancelEdit() {
    logger.debug("Confirming cancelled edition.");
    final WarnDialog warnDialog = new WarnDialog();
    warnDialog.waitUntilVisible();
    warnDialog.clickYes();
  }

  /**
   * Click the save button of the last selected row.
   *
   */
  public void clickSave() {
    logger.debug("Clicking the save button of the last selected row.");
    final String buttonLocator = getButtonLocatorByLastSelection(INDEX_BUTTON_SAVE);
    final Button buttonSave = new Button();
    buttonSave.setSmartClientLocator(buttonLocator);
    buttonSave.click();
    // FIXME wait for the grid to be saved.
    Sleep.trySleep();
  }

  /**
   * Click the save button of a row.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void clickSave(int rowNumber) {
    logger.debug("Clicking the save button of the row '{}.", rowNumber);
    final String buttonLocator = getButtonLocatorByRowNumber(rowNumber, INDEX_BUTTON_SAVE);
    final Button buttonSave = new Button();
    buttonSave.setSmartClientLocator(buttonLocator);
    buttonSave.click();
  }

  /**
   * Deletes the selected record by pressing the delete key.
   */
  public void deleteRecordWithShortcut() {
    logger.debug("Deleting a record by pressing the delete key");
    // If the grid is in edit mode, it focus the cancel button
    final String cancelEditButtonLocator = getButtonLocatorByLastSelection(
        INDEX_BUTTON_CANCEL_EDIT);
    final Button buttonCancelEdit = new Button();
    buttonCancelEdit.setSmartClientLocator(cancelEditButtonLocator);
    if (buttonCancelEdit.isVisible()) {
      buttonCancelEdit.pressDelete();
    } else {
      // If the grid is not in edit mode, it focus the edit button
      final String editButtonLocator = getButtonLocatorByLastSelection(INDEX_BUTTON_EDIT);
      final Button buttonEdit = new Button();
      buttonEdit.setSmartClientLocator(editButtonLocator);
      if (buttonEdit.isVisible()) {
        buttonEdit.pressDelete();
      }
    }

  }

  /**
   * Filter the grid rows.
   *
   * @param columnName
   *          The name of the column.
   * @param filterValue
   *          The value of the filter.
   */
  public void filter(String columnName, Object filterValue) {
    logger.debug("Filtering the column '{}' with the value '{}'.", columnName, filterValue);
    final String fieldSmartClientLocator = String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getFilterLocator(),
        TestUtils.convertFieldSeparator(columnName, false));
    final InputField filterInputField = GridInputFieldFactory
        .getFilterInputFieldObject(objectString, columnName, fieldSmartClientLocator);
    final String value = filterValue instanceof Boolean
        ? (((Boolean) filterValue).booleanValue() ? "Yes" : "No")
        : filterValue.toString();
    filterInputField.setValue(value);
    if (filterInputField instanceof DateItem) {
      // TODO see issue 16015 - https://issues.openbravo.com/view.php?id=16015
      ((DateItem) filterInputField).pressEnter();
    }
    Sleep.trySleep();
    waitForDataToLoad();
  }

  /**
   * Get the object string of the edit form.
   *
   * @return the object string of the edit form.
   */
  private String getEditFormObjectString() {
    return String.format(FORMAT_SMART_CLIENT_GET_EDIT_FORM, objectString);
  }

  /**
   * Check if the edit form is visible.
   *
   * @return true if the edit form is visible. Otherwise, false.
   */
  public boolean isEditFormVisible() {
    String objectStringEditForm = getEditFormObjectString();
    return (Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(objectStringEditForm + " != null");
  }

  /**
   * Wait for the edit form to be visible.
   */
  public void waitUntilEditFormIsVisible() {
    SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the edit form to be visible. Info: {}", getInformationForLog());
        return isEditFormVisible();
      }
    });
  }

  /**
   * Wait for the edit form to be invisible.
   */
  public void waitUntilEditFormIsNotVisible() {
    if (isEditFormVisible()) {
      SeleniumSingleton.WAIT.until(new ExpectedCondition<Boolean>() {
        @Override
        public Boolean apply(WebDriver webDriver) {
          logger.debug("Waiting for the edit form to dissapear. Info: {}", getInformationForLog());
          return !isEditFormVisible();
        }
      });
    }
  }

  /**
   * Get a field object.
   *
   * @param fieldName
   *          The name of the field.
   *
   * @return the field object.
   */
  public InputField getField(String fieldName) {
    boolean onlyAllowTextItems = false;
    return getField(fieldName, onlyAllowTextItems);
  }

  /**
   * Get a field object.
   *
   * @param fieldName
   *          The name of the field.
   * @param onlyAllowTextItems
   *          flag to specify if only text items should be returned
   *
   * @return the field object. If onlyAllowTextItems and the field is not a text item, an exception
   *         is rised
   */
  public InputField getField(String fieldName, boolean onlyAllowTextItems) {
    String objectStringEditForm = getEditFormObjectString();
    return GridInputFieldFactory
        .getInputFieldObject(String.format(FORMAT_SMART_CLIENT_GET_EDIT_FORM_FIELD,
            objectStringEditForm, TestUtils.convertFieldSeparator(fieldName)), onlyAllowTextItems);
  }

  /**
   * Fill a cell value of the selected row. The row should be on edit mode.
   *
   * @param columnName
   *          The name of the column
   * @param newValue
   *          The new value to enter on the cell.
   */
  public void fill(String columnName, Object newValue) {
    logger
        .trace(String.format("Filling the column '%s' with the value '%s'.", columnName, newValue));
    final InputField inputField = getField(columnName);
    inputField.waitUntilEnabled();
    inputField.setValue(newValue);
    inputField.waitUntilEnabled();
  }

  /**
   * Clear the filters.
   */
  @Override
  public void clearFilters() {
    logger.debug("Clearing the filter.");
    final Button buttonClearFilter = new Button(
        String.format(FORMAT_SMART_CLIENT_GET_BUTTON_CLEAR_FILTER, objectString));
    if (buttonClearFilter.isVisible()) {
      buttonClearFilter.click();
      waitForDataToLoad();
    } else {
      logger.debug("Grid had no filter.");
    }
  }

  /**
   * Assert a field.
   *
   * @param fieldName
   *          The name of the field.
   * @param expectedValue
   *          The expected value of the field.
   */
  public void assertField(String fieldName, Object expectedValue) {
    // FIXME find a way to locate the field without opening the edit mode.
    clickEdit();
    final String fieldSmartClientLocator = String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getSmartClientLocator(),
        TestUtils.convertFieldSeparator(fieldName, false));
    final InputField recordInputField = GridInputFieldFactory
        .getFilterInputFieldObject(objectString, fieldName, fieldSmartClientLocator);
    assertThat(recordInputField.getValue(), is(equalTo(expectedValue)));
    clickCancelEdit();
    confirmCancelEdit();
  }

  /**
   * Get the value present on the filter of a field.
   *
   * @param fieldName
   *          The name of the field whose filter value will be returned.
   */
  public Object getFilterValue(String fieldName) {
    String fieldSmartClientLocator = String.format(
        FORMAT_LOCATOR_SMART_CLIENT_FORM_ROW_EDITOR_BY_NAME, getFilterLocator(),
        TestUtils.convertFieldSeparator(fieldName, false));
    InputField filterInputField = GridInputFieldFactory.getFilterInputFieldObject(objectString,
        fieldName, fieldSmartClientLocator);
    return filterInputField.getValue();
  }

  /**
   * Assert that the record was saved.
   */
  public void assertSaved() {
    // FIMXE this doesn't really verify that the record was saved. Only that the selected record
    // shows the open and edit buttons.
    assertNotInEditMode();
  }

  public void assertNotInEditMode() {
    final String locatorButtonOpen = getButtonLocatorByLastSelection(INDEX_BUTTON_OPEN);
    final Button buttonOpen = new Button();
    buttonOpen.setSmartClientLocator(locatorButtonOpen);
    final String locatorButtonEdit = getButtonLocatorByLastSelection(INDEX_BUTTON_EDIT);
    final Button buttonEdit = new Button();
    buttonEdit.setSmartClientLocator(locatorButtonEdit);
    assertTrue("Open and edit buttons should be visible",
        buttonOpen.isVisible() && buttonEdit.isVisible());
  }

  public void assertInEditMode() {

    final String locatorButtonCancel = getButtonLocatorByLastSelection(INDEX_BUTTON_CANCEL_EDIT);
    final Button buttonCancel = new Button();
    buttonCancel.setSmartClientLocator(locatorButtonCancel);
    final String locatorButtonSave = getButtonLocatorByLastSelection(INDEX_BUTTON_SAVE);
    final Button buttonSave = new Button();
    buttonSave.setSmartClientLocator(locatorButtonSave);

    assertTrue("Cancel and save buttons should be visible",
        buttonCancel.isVisible() && buttonSave.isVisible());
  }

  public void selectAllRecordsOneCheckbox() {
    logger
        .trace("Deselecting first to make sure no records are selected before selecting them all.");
    String deselectAllCommand = this.objectString + ".deselectAllRecords()";
    SeleniumSingleton.INSTANCE.executeScript(deselectAllCommand);
    // Testing to avoid "batches" false positives
    Sleep.trySleep(1500);
    this.waitUntilLoaded();
    Sleep.trySleep(1500);
    // END Testing to avoid "batches" false positives
    logger.debug("Selecting all the records by pressing the select all checkbox.");
    String selectAllCommand = this.objectString + ".headerClick(0)";
    // If there are more than 100 records. It can not be clicked manually. If it is tried to be
    // clicked using JS order, the "batches" alert error will be shown
    SeleniumSingleton.INSTANCE.executeScript(selectAllCommand);
  }

  public void selectMultipleRecords(int[] indexesToBeSelected) {
    logger.debug("Selecting multiple records.");
    for (int i = 0; i < indexesToBeSelected.length; i++) {
      String selectRecordCommand = this.objectString + ".selectRecord(" + indexesToBeSelected[i]
          + ")";
      SeleniumSingleton.INSTANCE.executeScript(selectRecordCommand);
    }

  }

  /**
   * Cancels en edition using OB.KeyboardManager.Shortcuts.
   */
  public void cancelWithShortcut() {
    logger.debug("Canceling an edition using OB.KeyboardManager.Shortcuts.");
    String cancelRecordsCommand = "OB.KeyboardManager.Shortcuts.execute(OB.KeyboardManager.Shortcuts.getProperty('position', 'ViewGrid_CancelEditing', 'id'))";
    SeleniumSingleton.INSTANCE.executeScript(cancelRecordsCommand);
  }

  public void moveToNextRecordWhileEditing() {
    logger.debug("Moving to the next record using OB.KeyboardManager.Shortcuts.");
    String moveToNextRecordCommand = "OB.KeyboardManager.Shortcuts.execute(OB.KeyboardManager.Shortcuts.getProperty('position', 'ViewGrid_MoveDownWhileEditing', 'id'))";
    SeleniumSingleton.INSTANCE.executeScript(moveToNextRecordCommand);
  }

  public void deleteUsingContextualMenu() {
    logger.debug("Using the contextual menu to delete a record");
    String deleteWithContextualMenuCommand = "var rec = " + this.objectString
        + ".getCellRecord(0);";
    deleteWithContextualMenuCommand += "var contextMenuItems = " + this.objectString
        + ".makeCellContextItems(rec,0,3);";
    deleteWithContextualMenuCommand += "contextMenuItems[3].click();";
    SeleniumSingleton.INSTANCE.executeScript(deleteWithContextualMenuCommand);
  }

  public void editSelectedRecordUsingContextualMenu() {
    logger.debug("Using the contextual menu to edit a record");
    String editWithContextualMenuCommand = "var rec = " + this.objectString + ".getCellRecord(0);";
    editWithContextualMenuCommand += "var contextMenuItems = " + this.objectString
        + ".makeCellContextItems(rec,0,3);";
    editWithContextualMenuCommand += "contextMenuItems[0].click();";
    SeleniumSingleton.INSTANCE.executeScript(editWithContextualMenuCommand);
  }

  @Override
  public void selectRecord(int row) {
    logger.debug("Selecting record.");
    String selectRecordCommand = this.objectString + ".selectRecord(" + row + ")";
    SeleniumSingleton.INSTANCE.executeScript(selectRecordCommand);
  }

  public void openFirstRecordInFormWithDoubleClick() {
    logger.debug("Opening the selected record using OB.KeyboardManager.Shortcuts.");

    StringBuilder openWithDoubleClickCommandBuilder = new StringBuilder();
    openWithDoubleClickCommandBuilder.append("rec = " + this.objectString + ".getCellRecord(0);");
    openWithDoubleClickCommandBuilder.append(this.objectString + ".rowDoubleClick(rec,0,0);");

    String openWithDoubleClickCommand = openWithDoubleClickCommandBuilder.toString();

    SeleniumSingleton.INSTANCE.executeScript(openWithDoubleClickCommand);
  }

  @Override
  public Object getRows() {
    logger.trace("Get rows on grid {}",
        String.format(FORMAT_SMART_CLIENT_GRID_GET_DATA, objectString));
    return SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GRID_GET_DATA, objectString));
  }

  @Override
  public Object getSelectedRows() {
    logger.trace("Get rows on grid");
    return SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GRID_GET_SELECTED_DATA, objectString));
  }

  public void createWithContextualMenu() {
    logger.debug("Using the contextual menu to create a record");
    String createWithContextualMenuCommand = "var rec = " + this.objectString
        + ".getCellRecord(0);";
    createWithContextualMenuCommand += "var contextMenuItems = " + this.objectString
        + ".makeCellContextItems(rec,0,3);";
    createWithContextualMenuCommand += "contextMenuItems[1].click();";
    SeleniumSingleton.INSTANCE.executeScript(createWithContextualMenuCommand);
  }

  /**
   * Inserts a new record in grid view using OB.KeyboardManager.Shortcuts.
   */
  public void createNewRecordWithShortcut() {
    logger.debug("Inserting a new record using OB.KeyboardManager.Shortcuts.");
    String createRecordCommand = "OB.KeyboardManager.Shortcuts.execute(OB.KeyboardManager.Shortcuts.getProperty('position', 'ToolBar_NewRow', 'id'))";
    SeleniumSingleton.INSTANCE.executeScript(createRecordCommand);
  }

  /**
   * Show a column that is hidden on grid view.
   */
  public void showColumnOnGrid(String fieldName) {
    logger.debug("Showing a hidden column on grid view.");
    // If the grid is in edit mode an exception is risen
    String showColumnField = this.objectString + ".showField(\"" + fieldName + "\");";
    SeleniumSingleton.INSTANCE.executeScript(showColumnField);
  }

  /**
   * Hide a column that is showing on grid view.
   */
  public void hideColumnOnGrid(String fieldName) {
    logger.debug("Hide a showing column on grid view.");
    String hideColumnField = this.objectString + ".hideField(\"" + fieldName + "\");";
    SeleniumSingleton.INSTANCE.executeScript(hideColumnField);
  }
}
