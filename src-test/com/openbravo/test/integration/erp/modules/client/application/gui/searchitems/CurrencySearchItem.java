/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui.searchitems;

import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.SearchItem;
import com.openbravo.test.integration.selenium.NotImplementedException;

/**
 * Executes actions on OpenbravoERP currency search item.
 *
 * @author elopio
 *
 */
public class CurrencySearchItem extends OBSearchItem {

  /* Components. */
  /** The payment method selector pop up. */
  // TODO private AttributeSetPopUp popUp;

  /**
   * Class constructor.
   *
   * @param searchItem
   *          The search item of this selector.
   */
  public CurrencySearchItem(SearchItem searchItem) {
    super(searchItem);
  }

  /**
   * Set the attribute search item value.
   *
   * @param value
   *          The value to set.
   */
  @Override
  public void setValue(Object value) {
    throw new NotImplementedException();
  }

  /**
   * Get the title of the item.
   */
  @Override
  public String getTitle() {
    // TODO
    throw new NotImplementedException();
  }

}
