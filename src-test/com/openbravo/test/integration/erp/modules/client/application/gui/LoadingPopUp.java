/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.erp.common.TimeoutConstants;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Class for actions related to the Openbravo ERP Loading pop up.
 *
 * @author elopio
 *
 */
public class LoadingPopUp {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Locators. */
  /** Locator of the loading image. */
  private static final String LOCATOR_IMAGE_LOADING = "//img[contains(@src, 'web/org.openbravo.userinterface.smartclient/openbravo/skins/3.00/org.openbravo.client.application/images/system/windowLoading.gif')]";

  /**
   * Class constructor
   *
   */
  public LoadingPopUp() {
  }

  /**
   * Wait for data to be fully loaded.
   */
  public void waitForDataToLoad() {
    waitForDataToLoad(TimeoutConstants.PAGE_LOAD_TIMEOUT);
  }

  /**
   * Wait for data to be fully loaded.
   *
   * @param timeout
   *          The time to wait for the pop up to disappear before reporting an error.
   */
  public void waitForDataToLoad(long timeout) {
    Wait<WebDriver> dataWait = new WebDriverWait(SeleniumSingleton.INSTANCE, timeout);
    dataWait.until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        logger.debug("Waiting for the loading image to disappear.");
        try {
          SeleniumSingleton.INSTANCE.findElementByXPath(LOCATOR_IMAGE_LOADING);
        } catch (NoSuchElementException exception) {
          return true;
        }
        return false;
      }
    });
  }
}
