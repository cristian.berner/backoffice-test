/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testsuites.standardview.grid;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.productsetup.unitofmeasure.UnitOfMeasureData;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureTab;
import com.openbravo.test.integration.erp.gui.masterdata.productsetup.unitofmeasure.UnitOfMeasureWindow;
import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.client.application.testscripts.standardview.grid.InsertRowAndSave;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test the record creation from grid.
 *
 * @author elopio
 */
@RunWith(Parameterized.class)
public class GRD0060_CreateRecord extends InsertRowAndSave {

  /**
   * Class constructor.
   *
   * @param newDataObject
   *          The new data object.
   */
  public GRD0060_CreateRecord(DataObject newDataObject) {
    super(newDataObject);
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<DataObject[]> createRecordValues() {
    Random randomGenerator = new Random();
    int originalRandomNumber = randomGenerator.nextInt();
    DataObject[][] data = new DataObject[][] { { new UnitOfMeasureData.Builder().eDICode("QA")
        .name("Test UOM " + originalRandomNumber)
        .build() } };
    return Arrays.asList(data);
  }

  /**
   * Open and return the generated tab that will be used for the test.
   *
   * @return the generated tab that will be used for the test.
   */
  @SuppressWarnings("unchecked")
  @Override
  public GeneratedTab<DataObject> getTab() {
    UnitOfMeasureWindow unitOfMeasureWindow = new UnitOfMeasureWindow();
    mainPage.openView(unitOfMeasureWindow);
    return (GeneratedTab<DataObject>) unitOfMeasureWindow.selectTab(UnitOfMeasureTab.IDENTIFIER);
  }

}
