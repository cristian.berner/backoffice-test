/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2014 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rowinson Gallego <rwn.gallego@gmail.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.client.application.testscripts.maintabset;

import org.junit.Test;

import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test the opening and closing of a View.
 *
 * @author rwngallego
 *
 */
public abstract class OpenAndCloseView extends OpenbravoERPTest {

  /** The item path to select from the menu. */
  private final String[] items;

  /**
   * Class constructor.
   *
   * @param items
   *          the path of the item to select.
   */
  public OpenAndCloseView(String[] items) {
    this.items = items;
  }

  /**
   * Test menu item selection.
   */
  @Test
  public void viewShouldBeOpenedAndClosed() {
    Menu componentMenu = mainPage.getNavigationBar().getComponentMenu();
    componentMenu.select(items);
    mainPage.getTabSetMain().waitUntilTabIsOpen(items[items.length - 1]);

    // after the tab is open, wait 3 secs the window to be loaded. TODO: smarter wait
    Sleep.trySleep(3000);

    mainPage.getTabSetMain().closeTab(items[items.length - 1]);
    mainPage.getTabSetMain().waitUntilTabIsClosed(items[items.length - 1]);
  }
}
