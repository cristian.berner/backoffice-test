/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.setup.entitydefaultvalues;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

/**
 * Executes and verify actions on the Entity Default Values classic window of OpenbravoERP.
 *
 * @author elopio
 *
 */
public class EntityDefaultValuesWindow extends StandardWindow {

  /** The window title. */
  @SuppressWarnings("hiding")
  private static final String TITLE = "Entity Default Values";
  /** The window menu path. */
  private static final String[] MENU_PATH = new String[] { Menu.MASTER_DATA_MANAGEMENT,
      Menu.INITIAL_DATA_LOAD, Menu.INITIAL_DATA_LOAD_SETUP,
      Menu.INITIAL_DATA_LOAD_ENTITY_DEFAULT_VALUES };

  /**
   * Class constructor.
   *
   */
  public EntityDefaultValuesWindow() {
    super(TITLE, MENU_PATH);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void load() {
    EntitiesTab entitiesTab = new EntitiesTab();
    addTopLevelTab(entitiesTab);
    super.load();
  }

  /**
   * Select and return the Entities tab.
   *
   * @return the Entities tab.
   */
  public EntitiesTab selectEntitiesTab() {
    return (EntitiesTab) selectTab(EntitiesTab.IDENTIFIER);
  }

  /**
   * Select and return the Fields tab.
   *
   * @return the Fields tab.
   */
  public FieldsTab selectFieldsTab() {
    return (FieldsTab) selectTab(FieldsTab.IDENTIFIER);
  }
}
