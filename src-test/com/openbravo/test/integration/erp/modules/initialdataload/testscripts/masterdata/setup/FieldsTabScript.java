/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.testscripts.masterdata.setup;

import com.openbravo.test.integration.erp.modules.client.application.gui.MainPage;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.FieldsData;
import com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.setup.entitydefaultvalues.EntityDefaultValuesWindow;
import com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.setup.entitydefaultvalues.FieldsTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;

/**
 * Executes and verifies actions on the Feilds tab of OpenbravoERP.
 *
 * @author elopio
 *
 */
class FieldsTabScript {

  /**
   * Select a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The data of the record that will be selected.
   * @throws OpenbravoERPTestException
   */
  static void select(MainPage mainPage, FieldsData data) throws OpenbravoERPTestException {
    EntityDefaultValuesWindow entityDefaultValuesWindow = new EntityDefaultValuesWindow();
    mainPage.openView(entityDefaultValuesWindow);

    entityDefaultValuesWindow.selectEntitiesTab();
    FieldsTab fieldsTab = entityDefaultValuesWindow.selectFieldsTab();
    fieldsTab.select(data);
  }

  /**
   * Edit a record.
   *
   * @param selenium
   *          Object used to execute actions on the application.
   * @param mainPage
   *          The application main page.
   * @param data
   *          The edited record data.
   * @throws OpenbravoERPTestException
   */
  static void edit(MainPage mainPage, FieldsData data) throws OpenbravoERPTestException {
    EntityDefaultValuesWindow entityDefaultValuesWindow = new EntityDefaultValuesWindow();
    mainPage.openView(entityDefaultValuesWindow);

    entityDefaultValuesWindow.selectEntitiesTab();

    FieldsTab fieldsTab = entityDefaultValuesWindow.selectFieldsTab();

    fieldsTab.editRecord(data);
    fieldsTab.assertSaved();
  }
}
