/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Pablo Lujan <pablo.lujan@openbravo.com>.
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

/**
 *
 * @author plujan
 *
 */
public class FieldsData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    /** The data fields. */
    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<String, Object>();

    /**
     * Set the Field Name value.
     *
     * @param value
     *          The Field Name value.
     * @return The builder for this class.
     */
    public Builder name(String value) {
      this.dataFields.put("name", value);
      return this;
    }

    /**
     * Set the Default Value value.
     *
     * @param value
     *          The Default Value value.
     * @return The builder for this class.
     */
    public Builder defaultValue(String value) {
      this.dataFields.put("defaultValue", value);
      return this;
    }

    /**
     * Build the data object.
     *
     * @return The data object
     */
    public FieldsData build() {
      return new FieldsData(this);
    }

  }

  /**
   * Build the data object.
   *
   * @param builder
   *          The object builder
   */
  private FieldsData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
