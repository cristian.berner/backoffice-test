/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.initialdataload.gui.masterdata.setup.entitydefaultvalues;

import com.openbravo.test.integration.erp.modules.client.application.gui.GeneratedTab;
import com.openbravo.test.integration.erp.modules.initialdataload.data.masterdata.setup.entitydefaultvalues.EntitiesData;

/**
 * Executes and verifies actions on OpenbravoERP Entities tab.
 *
 * @author elopio
 *
 */
public class EntitiesTab extends GeneratedTab<EntitiesData> {

  /** The tab title. */
  static final String TITLE = "Entities";
  /** The tab identifier. */
  @SuppressWarnings("hiding")
  static final String IDENTIFIER = "E6A83361201B4261B9EF57BE4A62D1C6";
  /** The tab level. */
  @SuppressWarnings("unused")
  private static final int LEVEL = 0;

  /**
   * Class constructor.
   *
   */
  public EntitiesTab() {
    super(TITLE, IDENTIFIER);
    addChildTab(new FieldsTab(this));
  }

}
