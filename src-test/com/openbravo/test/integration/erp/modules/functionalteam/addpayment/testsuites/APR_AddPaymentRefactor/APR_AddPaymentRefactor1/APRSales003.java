/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;

/**
 * Adding a G/L Item transaction type payment.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales003 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_REFERENCE_NO = "reference_no";
  private static final String PARAM_ACTUAL_PAYMENT = "actual_payment";

  /* Data for this test. */
  /** The payment in header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The payment in line data. */
  PaymentInLinesData paymentInLinesData;

  /**
   * Class constructor.
   *
   * @param paymentInHeaderData
   * @param addPaymentTotalsVerificationData
   * @param paymentInLinesData
   */
  public APRSales003(PaymentInHeaderData paymentInHeaderData,
      AddPaymentPopUpData addPaymentTotalsVerificationData, PaymentInLinesData paymentInLinesData) {
    this.paymentInHeaderData = paymentInHeaderData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.paymentInLinesData = paymentInLinesData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToOrderValues() {

    return Arrays
        .asList(new Object[][] { {
            new PaymentInHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Customer A").build())
                .paymentMethod("1 (Spain)")
                .build(),
            new AddPaymentPopUpData.Builder()
                .amount_gl_items("20,000.00")
                .amount_inv_ords("0.00")
                .total("20,000.00")
                .difference("0.00")
                .build(),
            new PaymentInLinesData.Builder().received("20,000.00")
                .glitemname("Salaries")
                .build() } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void APRSales003Test() {
    logger
        .info("** Start of test case [APRSales003] Adding a G/L Item transaction type payment. **");

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Salaries").receivedIn("20,000.00").build());
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.setParameterValue(PARAM_ACTUAL_PAYMENT, "20,000.00");
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Received Payment(s)");
    paymentIn.assertPaymentCreatedSuccessfully();

    PaymentIn.Lines paymentInLine = paymentIn.new Lines(mainPage);
    paymentInLine.assertData(paymentInLinesData);

    logger.info("** End of test case [APRSales003] Adding a G/L Item transaction type payment. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
