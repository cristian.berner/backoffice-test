/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  David Miguélez <david.miguelez@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.accounting.testsuites.FAMc_MulticurrencySchema;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.ReconciliationsLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * This test case posts a payment, a transaction and a reconciliation.
 *
 * @author David Miguélez
 */

@RunWith(Parameterized.class)
public class FAMc_030SalesPostPAY_TRX_REC extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /* Data for [FAMc_030] Post a payment, transaction and reconciliation. */
  /** The sales order header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines1;
  private String[][] journalEntryLines2;
  /** The Payment In header data */
  PaymentInHeaderData paymentInHeaderData;
  /** Expected journal entry lines for the payment in. */
  private String[][] paymentInJournalEntryLines1;
  private String[][] paymentInJournalEntryLines2;
  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction header data. */
  TransactionsData transactionsHeaderData;
  /** Expected journal entry lines for the transaction. */
  private String[][] transactionJournalEntryLines1;
  private String[][] transactionJournalEntryLines2;
  /** The reconciliation lines data. */
  ReconciliationsLinesData reconciliationsLinesHeaderData;
  /** Expected journal entry lines for the reconciliation. */
  private String[][] reconciliationJournalEntryLines1;
  private String[][] reconciliationJournalEntryLines2;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The sales invoice header data.
   * @param journalEntryLines1
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param journalEntryLines2
   *          Expected journal entry lines. Is an array of arrays. Each line has account number,
   *          name, debit and credit.
   * @param paymentInHeaderData
   *          The payment in header data.
   * @param paymentInJournalEntryLines1
   *          Expected jounal entry lines for the payment in.
   * @param paymentInJournalEntryLines2
   *          Expected jounal entry lines for the payment in.
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionsHeaderData
   *          The transaction Header Data.
   * @param transactionJournalEntryLines1
   *          Expected journal entry lines for the transaction.
   * @param transactionJournalEntryLines2
   *          Expected journal entry lines for the transaction.
   * @param reconciliationsLinesHeaderData
   *          The reconciliation lines Header Data.
   * @param reconciliationJournalEntryLines1
   *          Expected journal entry lines for the reconciliation.
   * @param reconciliationJournalEntryLines2
   *          Expected journal entry lines for the reconciliation.
   */

  public FAMc_030SalesPostPAY_TRX_REC(SalesInvoiceHeaderData salesInvoiceHeaderData,
      String[][] journalEntryLines1, String[][] journalEntryLines2,
      PaymentInHeaderData paymentInHeaderData, String[][] paymentInJournalEntryLines1,
      String[][] paymentInJournalEntryLines2, AccountData accountHeaderData,
      TransactionsData transactionsHeaderData, String[][] transactionJournalEntryLines1,
      String[][] transactionJournalEntryLines2,
      ReconciliationsLinesData reconciliationsLinesHeaderData,
      String[][] reconciliationJournalEntryLines1, String[][] reconciliationJournalEntryLines2) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.journalEntryLines1 = journalEntryLines1;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInJournalEntryLines1 = paymentInJournalEntryLines1;
    this.paymentInJournalEntryLines2 = paymentInJournalEntryLines2;
    this.accountHeaderData = accountHeaderData;
    this.transactionsHeaderData = transactionsHeaderData;
    this.transactionJournalEntryLines1 = transactionJournalEntryLines1;
    this.transactionJournalEntryLines2 = transactionJournalEntryLines2;
    this.reconciliationsLinesHeaderData = reconciliationsLinesHeaderData;
    this.reconciliationJournalEntryLines1 = reconciliationJournalEntryLines1;
    this.reconciliationJournalEntryLines2 = reconciliationJournalEntryLines2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [FAMc_030] Post a payment, transaction and reconciliation.
        new SalesInvoiceHeaderData.Builder().documentNo("I/8").build(),
        new String[][] { { "43000", "Clientes (euros)", "100.00", "" },
            { "70000", "Ventas de mercaderías", "", "100.00" } },
        new String[][] { { "11400", "Accounts receivable", "200.00", "" },
            { "41100", "Sales", "", "200.00" } },
        new PaymentInHeaderData.Builder().documentNo("400010").build(),
        new String[][] { { "43100", "Efectos comerciales en cartera", "100.00", "" },
            { "43000", "Clientes (euros)", "", "100.00" } },
        new String[][] { { "11300", "Bank in transit", "500.00", "" },
            { "45200", "Bank revaluation gain", "", "300.00" },
            { "11400", "Accounts receivable", "", "200.00" } },
        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),
        new TransactionsData.Builder().documentNo("400010")
            .description("Invoice No.: I/8.")
            .build(),
        new String[][] { { "55500", "Partidas pendientes de aplicación", "100.00", "" },
            { "43100", "Efectos comerciales en cartera", "", "100.00" } },
        new String[][] { { "11200", "Bank Account", "500.00", "" },
            { "11300", "Bank in transit", "", "500.00" } },
        new ReconciliationsLinesData.Builder().documentNo("1000040").build(),
        new String[][] {
            { "57200", "Bancos e instituciones de crédito c/c vista euros", "100.00", "" },
            { "55500", "Partidas pendientes de aplicación", "", "100.00" } },
        new String[][] { { "11100", "Petty Cash", "500.00", "" },
            { "11200", "Bank Account", "", "500.00" } } } });
  }

  /**
   * Test for posting a payment, a transaction and a reconciliation.
   */
  @Test
  public void FAMc_030PostPaymentTransactionAndReconciliation() {
    logger.info(
        "** Start of test case [FAMc_030] Post a payment, transaction and reconciliation. **");

    final SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.select(salesInvoiceHeaderData);
    if (salesInvoice.isPosted()) {
      salesInvoice.unpost();
      // salesInvoice
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    salesInvoice.post();
    mainPage.closeView("Sales Invoice");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines1.length);
    post.assertJournalLines2(journalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(paymentInHeaderData);
    if (paymentIn.isPosted()) {
      paymentIn.unpost();
      // paymentIn
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    paymentIn.post();
    mainPage.closeView("Payment In");

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInJournalEntryLines1.length);
    post.assertJournalLines2(paymentInJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(paymentInJournalEntryLines2.length);
    post.assertJournalLines2(paymentInJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    final FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionsHeaderData);
    if (transaction.isPosted()) {
      transaction.unpost();
      // transaction
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    transaction.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines1.length);
    post.assertJournalLines2(transactionJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(transactionJournalEntryLines2.length);
    post.assertJournalLines2(transactionJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Since changing the tabs does not close automatically the post one, it is advised to close it
    // manually to avoid further issues
    FinancialAccount.Reconciliations reconciliations = financialAccount.new Reconciliations(
        mainPage);
    reconciliations.select(reconciliationsLinesHeaderData);
    if (reconciliations.isPosted()) {
      reconciliations.unpost();
      // reconciliations
      // .assertProcessCompletedSuccessfully2("Number of unposted documents = 1, Number of deleted
      // journal entries = 2");
    }
    reconciliations.post();

    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines1.length);
    post.assertJournalLines2(reconciliationJournalEntryLines1);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");

    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(reconciliationJournalEntryLines2.length);
    post.assertJournalLines2(reconciliationJournalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");
    logger
        .info("** End of test case [FAMc_030] Post a payment, transaction and reconciliation. **");
  }

}
