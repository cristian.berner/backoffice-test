/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rafael Queralta Pozo<rafaelcuba81@gmail.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.Requisition;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.RequisitionToOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * @author Rafael Queralta Pozo
 */
@RunWith(Parameterized.class)
public class AUMPROd_RequisitionToOrder extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  static String DATE_FORMAT = ConfigurationProperties.INSTANCE.getDateFormat();

  /* Data for this test. */
  /* Data for [PROd010]Create requisition. */
  /** The requisition header data. */
  RequisitionHeaderData requisitionHeaderData;
  /** The data to verify the requisition creation. */
  RequisitionHeaderData requisitionHeaderVerificationData;
  /** The requisition lines data. */
  RequisitionLinesData requisitionLinesData;
  /** The data to verify the requisition creation. */
  RequisitionLinesData requisitionLinesVerificationData;
  /** The completed requisition verification data. */
  RequisitionHeaderData completedRequisitionData;
  /** The data to verify the purchase order created. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;

  /**
   * Class constructor.
   *
   * @param requisitionHeaderData
   *          The requisition header data.
   * @param requisitionHeaderVerificationData
   *          The data to verify the requisition creation.
   * @param requisitionLinesData
   *          The requisition lines data.
   * @param requisitionLinesVerificationData
   *          The data to verify the requisition creation.
   * @param completedRequisitionData
   *          The completed requisition verification data.
   * @param purchaseOrderHeaderData
   *          The data to verify the purchaseOrderHeaderData order created.
   */
  public AUMPROd_RequisitionToOrder(RequisitionHeaderData requisitionHeaderData,
      RequisitionHeaderData requisitionHeaderVerificationData,
      RequisitionLinesData requisitionLinesData,
      RequisitionLinesData requisitionLinesVerificationData,
      RequisitionHeaderData completedRequisitionData,
      PurchaseOrderHeaderData purchaseOrderHeaderData) {
    this.requisitionHeaderData = requisitionHeaderData;
    this.requisitionHeaderVerificationData = requisitionHeaderVerificationData;
    this.requisitionLinesData = requisitionLinesData;
    this.requisitionLinesVerificationData = requisitionLinesVerificationData;
    this.completedRequisitionData = completedRequisitionData;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> requisitionToOrder() throws IOException {
    return Arrays.asList(new Object[][] { {
        /* Parameters for for [PROd010]Create requisition. */
        new RequisitionHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new RequisitionHeaderData.Builder().documentStatus("Draft")
            .priceList("Purchase")
            .currency("EUR")
            .userContact("QAAdmin")
            .build(),
        new RequisitionLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMA")
                .quantityOnHand("33.6")
                .uOM("Bag")
                .build())
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .operativeQuantity("11.2")
            .build(),
        new RequisitionLinesData.Builder().lineNetAmount("44.80")
            .requisitionLineStatus("Open")
            .priceList("Purchase")
            .build(),
        new RequisitionHeaderData.Builder().documentStatus("Completed").build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .documentStatus("Booked")
            .currency("EUR")
            .grandTotalAmount("73.92")
            .build() } });
  }

  /**
   * Test the requisition to order flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void PROd_CreateRequisitionAndOrder() throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [AUMPROd010] Create Requisition **");
    final Requisition requisition = new Requisition(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    requisition.create(requisitionHeaderData);
    requisition.assertSaved();
    requisition.assertData(requisitionHeaderVerificationData);

    final Requisition.Lines requisitionLines = requisition.new Lines(mainPage);
    final Calendar calendar = Calendar.getInstance();
    String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    requisitionLines.createOnFormWithoutSaving(new RequisitionLinesData.Builder().build());
    requisitionLines.setNeedByDate(currentDate);
    requisitionLines.edit(requisitionLinesData);
    requisitionLines.assertSaved();
    requisition.complete();
    requisition.assertProcessCompletedSuccessfully2();
    requisition.assertData(completedRequisitionData);
    logger.info("** End of test case [AUMPROd010] Create Requisition **");

    logger.info(
        "** Start of test case [AUMPROd030] Create Purchase Orders from Requisition To Order window **");
    final RequisitionToOrder requisitionToOrder = new RequisitionToOrder();
    mainPage.openView(requisitionToOrder);
    requisitionToOrder.selectOrders();
    requisitionToOrder.selectOrdersForCreatePurchases();
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder
        .filter(new PurchaseOrderHeaderData.Builder().documentNo(requisitionToOrder.purchaseOrderId)
            .build());
    purchaseOrder.assertData(purchaseOrderHeaderData);
    logger.info(
        "** End of test case [AUMPROd030] Create Purchase Orders from Requisition To Order window **");
  }
}
