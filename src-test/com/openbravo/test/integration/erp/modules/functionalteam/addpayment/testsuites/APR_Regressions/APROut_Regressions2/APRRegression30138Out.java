/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 30138
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30138Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData3;
  PaymentOutLinesData paymentOutLinesVerificationData4;

  /**
   * Class constructor.
   *
   */
  public APRRegression30138Out(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData3,
      PaymentOutLinesData paymentOutLinesVerificationData4) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    this.paymentOutLinesVerificationData3 = paymentOutLinesVerificationData3;
    this.paymentOutLinesVerificationData4 = paymentOutLinesVerificationData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .generatedCredit("11.00")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("10.00")
            .expected_payment("0.00")
            .amount_gl_items("-1.00")
            .amount_inv_ords("0.00")
            .total("-1.00")
            .difference("11.00")
            .build(),

        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared")
            .amount("10.00")
            .usedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().paid("-1.00").build(),
        new PaymentOutLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .paid("-1.00")
            .orderno("")
            .invoiceno("")
            .glitemname("Fees")
            .build(),
        new PaymentOutLinesData.Builder().paid("11.00").build(),
        new PaymentOutLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .paid("11.00")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30138 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression30138OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression30138Out] Test regression 30138 - Payment Out flow. **");

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    paymentOut.assertData(paymentOutHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcess
        .addGLItem(new SelectedPaymentData.Builder().gLItem("Fees").receivedIn("1.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Made Payment(s) and Withdrawal",
        "Leave the credit to be used later", addPaymentVerificationData);

    paymentOut.assertPaymentCreatedSuccessfully();
    paymentOut.assertData(paymentOutHeaderVerificationData2);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(2);
    // TODO L: Check the following static sleep. This test was failing without it.
    Sleep.trySleep();
    paymentOutLines.select(paymentOutLinesVerificationData);
    paymentOutLines.assertData(paymentOutLinesVerificationData2);
    paymentOutLines.cancelOnGrid();
    paymentOutLines.getTab().clearFilters();
    paymentOutLines.getTab().unselectAll();
    Sleep.trySleep(1000);
    paymentOutLines.select(paymentOutLinesVerificationData3);
    paymentOutLines.assertData(paymentOutLinesVerificationData4);

    logger.info(
        "** End of test case [APRRegression30138Out] Test regression 30138 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
