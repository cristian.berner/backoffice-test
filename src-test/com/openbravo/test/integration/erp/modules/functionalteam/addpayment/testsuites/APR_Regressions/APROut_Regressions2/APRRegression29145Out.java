/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29145
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29145Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PaymentOutHeaderData paymentOutHeaderData2;
  PaymentOutHeaderData paymentOutHeaderVerificationData3;
  AddPaymentPopUpData addPaymentVerificationData2;
  AddPaymentPopUpData addPaymentVerificationData3;
  PaymentOutHeaderData paymentOutHeaderVerificationData4;
  PaymentOutLinesData paymentOutLinesVerificationData2;
  UsedCreditSourceData paymentOutUsedCreditVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData6;

  /**
   * Class constructor.
   *
   */
  public APRRegression29145Out(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PaymentOutHeaderData paymentOutHeaderData2,
      PaymentOutHeaderData paymentOutHeaderVerificationData3,
      AddPaymentPopUpData addPaymentVerificationData2,
      AddPaymentPopUpData addPaymentVerificationData3,
      PaymentOutHeaderData paymentOutHeaderVerificationData4,
      PaymentOutLinesData paymentOutLinesVerificationData2,
      UsedCreditSourceData paymentOutUsedCreditVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData6) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.paymentOutHeaderData2 = paymentOutHeaderData2;
    this.paymentOutHeaderVerificationData3 = paymentOutHeaderVerificationData3;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.paymentOutHeaderVerificationData4 = paymentOutHeaderVerificationData4;
    this.paymentOutLinesVerificationData2 = paymentOutLinesVerificationData2;
    this.paymentOutUsedCreditVerificationData = paymentOutUsedCreditVerificationData;
    this.paymentOutHeaderVerificationData6 = paymentOutHeaderVerificationData6;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .generatedCredit("3,000.00")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("3,000.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("3,000.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("3,000.00")
            .usedCredit("0.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .paid("3,000.00")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("1 (Spain)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("1,200")
            .tax("VAT 10%")
            .build(),
        new PurchaseInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("2,400.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("2,640.00")
            .summedLineAmount("2,400.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .generatedCredit("0.00")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("2,640.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("2,640.00")
            .total("2,640.00")
            .difference("360.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("2,640.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("2,640.00")
            .total("2,640.00")
            .difference("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("0.00")
            .usedCredit("2,640.00")
            .build(),
        new PaymentOutLinesData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .invoiceAmount("2,640.00")
            .expected("2,640.00")
            .paid("2,640.00")
            .orderno("")
            .glitemname("")
            .build(),
        new UsedCreditSourceData.Builder().amount("2,640").toCurrency("EUR").build(),

        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("3,000.00")
            .usedCredit("2,640.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29145 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29145OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29145Out] Test regression 29145 - Payment Out flow. **");

    PaymentOut paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.create(paymentOutHeaderData);
    paymentOut1.assertSaved();
    paymentOut1.assertData(paymentOutHeaderVerificationData);
    AddPaymentProcess addPaymentProcess1 = paymentOut1.addDetailsOpen();
    String paymentNo = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    addPaymentProcess1.process("Process Made Payment(s)", "Leave the credit to be used later",
        addPaymentVerificationData);
    paymentOut1.assertPaymentCreatedSuccessfully();
    paymentOut1.assertData(paymentOutHeaderVerificationData2);
    PaymentOut.Lines paymentOutLines1 = paymentOut1.new Lines(mainPage);
    paymentOutLines1.assertCount(1);
    paymentOutLines1.assertData(paymentOutLinesVerificationData);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    Sleep.trySleep(15000);
    paymentOut2.create(paymentOutHeaderData2);
    paymentOut2.assertSaved();
    paymentOut2.assertData(paymentOutHeaderVerificationData3);
    AddPaymentProcess addPaymentProcess2 = paymentOut2.addDetailsOpen();
    addPaymentProcess2.processWithError("Invoices", invoiceNo, paymentNo, "Process Made Payment(s)",
        "Leave the credit to be used later", addPaymentVerificationData2,
        AddPaymentProcess.REGEXP_ERROR_MESSAGE_CREDIT_GREATER_THAN_TOTAL);
    addPaymentProcess2.collapseTotals();
    addPaymentProcess2.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("2,640.00").build());
    addPaymentProcess2.getField("reference_no").focus();
    addPaymentProcess2.process("Process Made Payment(s)", addPaymentVerificationData3);
    paymentOut2.assertPaymentCreatedSuccessfully();
    paymentOut2.assertData(paymentOutHeaderVerificationData4);
    PaymentOut.Lines paymentOutLines2 = paymentOut2.new Lines(mainPage);
    paymentOutLines2.assertCount(1);
    paymentOutLines2.assertData((PaymentOutLinesData) paymentOutLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));
    PaymentOut.UsedCreditSource paymentOutUsedCredit2 = paymentOut2.new UsedCreditSource(mainPage);
    paymentOutUsedCredit2.assertCount(1);
    paymentOutUsedCredit2.assertData((UsedCreditSourceData) paymentOutUsedCreditVerificationData
        .addDataField("creditPaymentUsed", paymentNo));

    paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut1.assertData(paymentOutHeaderVerificationData6);

    logger.info(
        "** End of test case [APRRegression29145Out] Test regression 29145 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
