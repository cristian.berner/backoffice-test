/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 32678
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression32678In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  PaymentPlanData orderPaymentInPlanData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData;
  PaymentPlanData orderPaymentInPlanData2;
  PaymentDetailsData orderPaymentInDetailsData2;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PendingPaymentInPlanData;
  AddPaymentPopUpData addPayment2VerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentInDetailsData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression32678In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,

      SalesOrderLinesData salesOrderLinesData, SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,

      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      PaymentPlanData orderPaymentInPlanData, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      PaymentPlanData orderPaymentInPlanData2, PaymentDetailsData orderPaymentInDetailsData2,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData completedSalesInvoice2HeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PendingPaymentInPlanData,
      AddPaymentPopUpData addPayment2VerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoice2PaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoice2PaymentInDetailsData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData2) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.orderPaymentInPlanData2 = orderPaymentInPlanData2;
    this.orderPaymentInDetailsData2 = orderPaymentInDetailsData2;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.completedSalesInvoice2HeaderVerificationData = completedSalesInvoice2HeaderVerificationData;
    this.invoice2PendingPaymentInPlanData = invoice2PendingPaymentInPlanData;
    this.addPayment2VerificationData = addPayment2VerificationData;
    this.invoice2PaymentInPlanData = invoice2PaymentInPlanData;
    this.invoice2PaymentInDetailsData = invoice2PaymentInDetailsData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Customer A")
            .invoiceTerms("Immediate")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("800").unitPrice("1.00").tax("Exempt").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1.00")
            .listPrice("2.00")
            .lineNetAmount("800.00")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("200").unitPrice("1.00").tax("Exempt").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("1.00")
            .listPrice("2.00")
            .lineNetAmount("200.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,000.00")
            .grandTotalAmount("1,000.00")
            .currency("EUR")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,000.00")
            .received("0.00")
            .outstanding("1,000.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30-60")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("800").lineNetAmount("800.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("800.00")
            .summedLineAmount("800.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("400.00")
            .received("0.00")
            .outstanding("400.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("800.00")
            .expected_payment("800.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("800.00")
            .total("800.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("400.00")
            .received("400.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("400.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("400.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentPlanData.Builder().dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("1,000.00")
            .received("800.00")
            .outstanding("200.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .expected("400.00")
            .received("400.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("800.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("800.00")
            .expected("400.00")
            .received("400.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-320.00")
            .summedLineAmount("-320.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-160.00")
            .received("0.00")
            .outstanding("-160.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .c_currency_id("EUR")
            .actual_payment("-320.00")
            .expected_payment("-320.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-320.00")
            .total("-320.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-160.00")
            .received("-160.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("-160.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-160.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-320.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("-320.00")
            .expected("-160.00")
            .received("-160.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32678
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32678Test() throws ParseException {
    logger.info("** Start of test case [APRRegression32678In] Test regression 32678. **");

    // Register a sales order
    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData2);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData2);

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"), "1000.00");

    // Book the order
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Create a sales invoice from order line
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);

    salesInvoice.createLinesFrom(salesOrderIdentifier, 1);
    salesInvoice.assertProcessCompletedSuccessfully();
    String invoice1No = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    // Create second invoice from same order line
    salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);

    salesInvoice.createLinesFrom(salesOrderIdentifier, 1);
    salesInvoice.assertProcessCompletedSuccessfully();
    String invoice2No = salesInvoice.getData("documentNo").toString();
    salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);
    salesInvoiceLine.edit(new SalesInvoiceLinesData.Builder().invoicedQuantity("-320").build());

    // Select and complete the first invoice
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check first invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Check first invoice second payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Pay the invoice
    salesInvoice.addPayment("800.00", "Process Received Payment(s) and Deposit");
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check first invoice payment plan and payment details
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.clearFilters();
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Check first invoice second payment plan and payment details
    invoicePaymentInDetails.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Check Sales order Payment in plan
    salesOrder = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData2);

    // Check Sales order Payment in details
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.selectWithoutFiltering(0);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    orderPaymentInDetails.selectWithoutFiltering(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    String paymentNo = orderPaymentInDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // Check Payment in
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLinesVerificationData.addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoice1No);
    paymentInLines.selectWithoutFiltering(0);
    paymentInLines.assertData(paymentInLinesVerificationData);
    paymentInLines.closeForm();

    paymentInLines.selectWithoutFiltering(1);
    paymentInLines.assertData(paymentInLinesVerificationData);
    paymentInLines.closeForm();

    // Select and complete the invoice2
    salesInvoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice2No).build());
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoice2HeaderVerificationData);

    // Check invoice2 payment plan
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PendingPaymentInPlanData);

    // Check invoice2 second payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoice2PendingPaymentInPlanData);

    // Pay the invoice2
    salesInvoice.addPayment("-320", "Process Received Payment(s) and Deposit");
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check invoice2 payment plan and payment details
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.clearFilters();
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoice2PaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaymentInDetailsData);

    // Check invoice2 second payment plan and payment details
    invoicePaymentInDetails.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoice2PaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoice2PaymentInDetailsData);

    String payment2No = invoicePaymentInDetails.getData("paymentDetails$finPayment").toString();
    payment2No = payment2No.substring(0, payment2No.indexOf("-") - 1);

    // Check first invoice payment plan and payment details
    salesInvoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoice1No).build());
    invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.clearFilters();
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Check first invoice second payment plan and payment details
    invoicePaymentInDetails.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Check Sales order Payment in plan
    salesOrder = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData2);

    // Check Sales order Payment in details
    orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.selectWithoutFiltering(0);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    orderPaymentInDetails.selectWithoutFiltering(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    // Check Payment1
    paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines.selectWithoutFiltering(0);
    paymentInLines.assertData(paymentInLinesVerificationData);
    paymentInLines.closeForm();

    paymentInLines.selectWithoutFiltering(1);
    paymentInLines.assertData(paymentInLinesVerificationData);
    paymentInLines.closeForm();

    // Check Payment2
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    paymentIn.assertData(paymentInHeaderVerificationData2);

    paymentInLinesVerificationData2.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoice2No);

    paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(2);
    paymentInLines.selectWithoutFiltering(0);
    paymentInLines.assertData(paymentInLinesVerificationData2);
    paymentInLines.closeForm();

    paymentInLines.selectWithoutFiltering(1);
    paymentInLines.assertData(paymentInLinesVerificationData2);
    paymentInLines.closeForm();

    logger.info("** End of test case [APRRegression32678In] Test regression 32678. **");
  }
}
