/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions4;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * Test regression 32573
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class APRRegression32573In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodVerifyData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  TransactionsData transactionLinesData;
  TransactionsData transactionLinesData2;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentMethodData paymentMethodRestoreData;

  /**
   * Class constructor.
   *
   */
  public APRRegression32573In(AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData paymentMethodVerifyData, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      TransactionsData transactionLinesData, TransactionsData transactionLinesData2,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentMethodData paymentMethodRestoreData) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodVerifyData = paymentMethodVerifyData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.transactionLinesData = transactionLinesData;
    this.transactionLinesData2 = transactionLinesData2;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentMethodRestoreData = paymentMethodRestoreData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Bank").build(),
        new PaymentMethodData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
        new PaymentMethodData.Builder().payinAllow(true)
            .automaticDeposit(true)
            .payinExecutionType("Automatic")
            .uponDepositUse("Deposited Payment Account")
            .payinExecutionProcess("Print Check simple process")
            .payinDeferred(true)
            .uponReceiptUse("In Transit Payment Account")
            .iNUponClearingUse("")
            .build(),
        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-8 (Transactions)")
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("Acc-8 (Transactions)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("447")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("894.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("925.29")
            .summedLineAmount("894.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new TransactionsData.Builder().transactionType("BP Deposit").build(),
        new TransactionsData.Builder().status("Awaiting Payment")
            .currency("EUR")
            .depositAmount("925.29")
            .paymentAmount("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new PaymentInHeaderData.Builder().build(),
        new PaymentInHeaderData.Builder().status("Deposited not Cleared").build(),

        new PaymentMethodData.Builder().payinAllow(true)
            .automaticDeposit(false)
            .payinExecutionType("Manual")
            .uponDepositUse("Cleared Payment Account")
            .uponReceiptUse("")
            .iNUponClearingUse("")
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32573 - It is not possible to execute a payment created in the financial
   * account if it is set up as "Automatic Deposit".
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32573InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression32573In] Test regression 32573 -  It is not possible to execute a payment created in the financial account if it is set up as Automatic Deposit. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodVerifyData);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoiceLine.assertSaved();
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.createWithoutSaving(transactionLinesData);
    AddPaymentProcess addPaymentProcess = transactions.openAddPayment();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Customer A").build());
    addPaymentProcess.setParameterValue("actual_payment", "925.29");
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "Acc-8 (Transactions)");
    AddPaymentGrid orderGrid = addPaymentProcess.getOrderInvoiceGrid();
    orderGrid.clearFilters();
    orderGrid.unselectAll();
    orderGrid.filter(new SelectedPaymentData.Builder().invoiceNo(invoiceNo).build());
    addPaymentProcess.process("Process Received Payment(s)");
    transactionLinesData.addDataField("finPayment",
        new PaymentSelectorData.Builder().documentNo(paymentNo).build());
    transactions.save();
    transactions.assertSaved();

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentInHeaderData.addDataField("documentNo", paymentNo);
    paymentIn.select(paymentInHeaderData);
    paymentIn.execute(paymentNo);
    paymentIn.assertData(paymentInHeaderVerificationData);
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    transactions = financialAccount.new Transaction(mainPage);
    transactions.select(transactionLinesData);
    transactions.assertData(new TransactionsData.Builder().status("Deposited not Cleared").build());

    // Restore the default payment in configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodRestoreData);

    logger.info(
        "** End of test case [APRRegression3573In] Test regression 32573 -  It is not possible to execute a payment created in the financial account if it is set up as Automatic Deposit. **");
  }
}
