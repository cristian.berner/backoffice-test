/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor3;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Refunding a credit payment.
 *
 */
@RunWith(Parameterized.class)
public class APRSales013 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_OVERPYMT_ACTION = "overpayment_action";
  /* Data for this test. */
  /** The payment in header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PaymentInHeaderData paymentInHeaderDatacredit;
  AddPaymentPopUpData addPaymentTotalsVerificationDatacredit;

  /**
   * Class constructor.
   *
   * @param addPaymentTotalsVerificationData
   *          The data to verify the totals data in the add payment pop up.
   *
   */
  public APRSales013(PaymentInHeaderData paymentInHeaderDatacredit,
      AddPaymentPopUpData addPaymentTotalsVerificationDatacredit,
      PaymentInHeaderData paymentInHeaderData,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    this.paymentInHeaderDatacredit = paymentInHeaderDatacredit;
    this.addPaymentTotalsVerificationDatacredit = addPaymentTotalsVerificationDatacredit;
    this.paymentInHeaderData = paymentInHeaderData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToOrderValues() {

    return Arrays
        .asList(new Object[][] { {
            new PaymentInHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Customer A").build())
                .amount("100.00")
                .paymentMethod("1 (Spain)")
                .build(),
            new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
                .amount_inv_ords("0.00")
                .total("0.00")
                .difference("100.00")
                .build(),
            new PaymentInHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Customer A").build())
                .amount("0.00")
                .paymentMethod("1 (Spain)")
                .build(),
            new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
                .amount_inv_ords("0.00")
                .total("0.00")
                .difference("100.00")
                .build() } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void APRSales013Test() {
    logger.info("** Start of test case [APRSales013] Refunding a credit payment **");

    PaymentIn paymentIncredit = new PaymentIn(mainPage).open();
    paymentIncredit.create(paymentInHeaderDatacredit);
    paymentIncredit.assertSaved();
    String paymentDocumentNo = paymentIncredit.getData("documentNo").toString();
    AddPaymentProcess addPaymentProcesscredit = paymentIncredit.addDetailsOpen();
    addPaymentProcesscredit.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcesscredit.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcesscredit.scrollToBottom();
    addPaymentProcesscredit.assertTotalsData(addPaymentTotalsVerificationDatacredit);
    addPaymentProcesscredit.setParameterValue(PARAM_OVERPYMT_ACTION,
        "Leave the credit to be used later");
    addPaymentProcesscredit.process("Process Received Payment(s)");

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcesscredit.scrollToBottom();

    addPaymentProcess.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess.getCreditToUseGrid().filter("documentNo", paymentDocumentNo);
    Sleep.trySleep(2000);
    addPaymentProcess.getCreditToUseGrid().selectRowByContents("documentNo", paymentDocumentNo);
    addPaymentProcess.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("100.00").build());
    addPaymentProcess.getField("reference_no").focus();

    Sleep.trySleep(1000);
    addPaymentProcesscredit.scrollToTop();
    addPaymentProcesscredit.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcesscredit.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcesscredit.scrollToBottom();
    Sleep.trySleep(1000);

    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.setParameterValue(PARAM_OVERPYMT_ACTION, "Refund amount to customer");
    addPaymentProcess.process("Process Received Payment(s)");
    paymentIn.assertProcessCompletedSuccessfully2();

    PaymentIn.UsedCreditSource usedCreditSource = paymentIn.new UsedCreditSource(mainPage);
    usedCreditSource
        .assertData(new UsedCreditSourceData.Builder().creditPaymentUsed(paymentDocumentNo)
            .amount("100")
            .build());

    logger.info("** End of test case [APRSales013] Refunding a credit payment **");

  }
}
