/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.FindTransactionsToMatchProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.util.OBDate;

/**
 * Match Automatically a Transaction and a Bank Statement Line
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRWithdrawalReconciliation002 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction data. */
  TransactionsData transactionData;
  /** The bank statement data. */
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  MatchStatementData matchStatementData2;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param bankStatementHeaderData
   *          The bank statement Header Data.
   * @param bankStatementLinesData
   *          The bank statement lines Data.
   */
  public APRWithdrawalReconciliation002(AccountData accountHeaderData,
      TransactionsData transactionData, BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      MatchStatementData matchStatementData2) {
    this.accountHeaderData = accountHeaderData;
    this.transactionData = transactionData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.matchStatementData2 = matchStatementData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> aPRWithdrawalReconciliation002Values() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRWithdrawalReconciliation002Test] */
        new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .gLItem("Salaries")
            .paymentAmount("200.00")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor B").build())
            .build(),
        new BankStatementHeaderData.Builder().name("TestReconciliationW002").build(),
        new BankStatementLinesData.Builder().referenceNo("W002")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor B").build())
            .gLItem("Salaries")
            .dramount("200.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor B")
            .referenceNo("W002")
            .glitem("Salaries")
            .amount("-200")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Vendor B")
            .referenceNo("W002")
            .glitem("Salaries")
            .amount("-200")
            .affinity("MA")
            .matchedDocument("T")
            .matchingType("MA")
            .businessPartner("Vendor B")
            .transactionAmount("-200")
            .transactionGLItem("Salaries")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Match Automatically a Transaction and a Bank Statement Line
   *
   * @throws ParseException
   */
  @Test
  public void APRWithdrawalReconciliation002Test() throws ParseException {
    logger.info("** Start of test case [APRWithdrawalReconciliation002Test]");

    // Select Financial Account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    // Get the date of the latest Bank Statement
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }

    // Create a Transaction
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactionData.addDataField("transactionDate", date);
    transactionData.addDataField("dateAcct", date);
    transactions.create(transactionData);
    transactions.assertSaved();
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();

    // Create Bank Statement
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    // Select Transaction
    FindTransactionsToMatchProcess findTransactionsToMatchProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickSearch(0);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().clearFilters();
    findTransactionsToMatchProcess.getTransactionsToMatchGrid()
        .filter("paymentAmount", (String) transactionData.getDataField("paymentAmount"));
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().filter("transactionDate", date);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid()
        .filter("businessPartner", "Vendor B");
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().selectRecord(0);
    findTransactionsToMatchProcess.process();
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);

    // Process Match Statement
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    logger.info("** End of test case [APRWithdrawalReconciliation002Test]");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
