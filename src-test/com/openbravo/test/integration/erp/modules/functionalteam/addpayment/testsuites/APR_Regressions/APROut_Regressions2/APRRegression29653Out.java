/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29653
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29653Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentOutHeaderData paymentOutHeaderVerificationData2;
  AccountData accountHeaderData;
  TransactionsData transactionLinesData;
  TransactionsData transactionLinesVerificationData;

  /**
   * Class constructor.
   *
   */
  public APRRegression29653Out(PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentOutHeaderData paymentOutHeaderVerificationData2, AccountData accountHeaderData,
      TransactionsData transactionLinesData, TransactionsData transactionLinesVerificationData) {
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentOutHeaderVerificationData2 = paymentOutHeaderVerificationData2;
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;
    this.transactionLinesVerificationData = transactionLinesVerificationData;
    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentOutHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .paymentMethod("Cheque")
            .build(),
        new PaymentOutHeaderData.Builder().organization("F&B España - Región Norte")
            .documentType("AP Payment")
            .account("Cuenta de Banco - EUR")
            .currency("EUR")
            .generatedCredit("0.00")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Bebidas Alegres, S.L.")
            .fin_paymentmethod_id("Cheque")
            .fin_financial_account_id("Cuenta de Banco - EUR")
            .c_currency_id("EUR")
            .actual_payment("10.00")
            .expected_payment("0.00")
            .amount_gl_items("10.00")
            .amount_inv_ords("0.00")
            .total("10.00")
            .difference("0.00")
            .build(),
        new PaymentOutHeaderData.Builder().status("Payment Made")
            .amount("10.00")
            .usedCredit("0.00")
            .build(),

        new AccountData.Builder().name("Cuenta de Banco").build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal").build(),
        new TransactionsData.Builder().transactionDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .description("GL Item: Capital social\n")
            .currency("EUR")
            .depositAmount("0.00")
            .paymentAmount("10.00")
            .organization("F&B España, S.A")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Bebidas Alegres, S.L.").build())
            .project("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29653 - Payment Out flow
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29653OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29653Out] Test regression 29653 - Payment Out flow. **");

    mainPage.getNavigationBar()
        .changeProfile(
            new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
                .build());
    Sleep.smartWaitButtonEnabled("UINAVBA_RecentLaunchList_BUTTON", 200);
    Sleep.trySleep(10000);

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();
    paymentOut.assertData(paymentOutHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Capital social").paidOut("10.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Made Payment(s)", addPaymentVerificationData);

    paymentOut.assertPaymentCreatedSuccessfully();
    paymentOut.assertData(paymentOutHeaderVerificationData2);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.create((TransactionsData) transactionLinesData.addDataField("finPayment",
        new PaymentSelectorData.Builder().documentNo(paymentNo).build()));
    transactions.assertSaved();
    transactions.assertData(transactionLinesVerificationData);
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();

    logger.info(
        "** End of test case [APRRegression29653Out] Test regression 29653 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
