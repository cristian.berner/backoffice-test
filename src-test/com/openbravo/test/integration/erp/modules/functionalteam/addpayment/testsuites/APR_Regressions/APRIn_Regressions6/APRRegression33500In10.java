/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions6;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.PaymentInPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnfromcustomer.ReturnFromCustomerHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.returnmaterialreceipt.ReturnMaterialReceiptHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnFromCustomerLineSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnMaterialReceiptLineSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer.PaymentInPlan;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnFromCustomer.PaymentInPlan.PaymentInDetails;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.ReturnMaterialReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test Regression 33500
 *
 * @author collazoandy4
 *
 */
@RunWith(Parameterized.class)
public class APRRegression33500In10 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentVerificationHeaderData;
  GoodsShipmentLinesData goodsShipmentLinesData;
  ReturnFromCustomerHeaderData returnFromCustomerHeaderData;
  ReturnFromCustomerHeaderData returnFromCustomerVerificationHeaderData;
  ReturnFromCustomerLineSelectorData pickEditLineEdit;
  ReturnFromCustomerHeaderData rfcBookedVerificationHeaderData;
  PaymentInPlanData rfcPaymentInVerificationData;
  ReturnMaterialReceiptHeaderData returnMaterialReceiptHeaderData;
  ReturnMaterialReceiptHeaderData returnMaterialReceiptVerificationHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeader;
  SalesInvoiceHeaderData salesInvoiceVerificationHeaderData;
  PaymentPlanData invoicePaymentInVerificationData;
  PaymentPlanData paidInvoicePaymentInVerificationData;
  PaymentDetailsData paidInvoicePaymentDetailsVerificationData;

  public APRRegression33500In10(GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptLinesData goodsReceiptLinesData, GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentVerificationHeaderData,
      GoodsShipmentLinesData goodsShipmentLinesData,
      ReturnFromCustomerHeaderData returnFromCustomerHeaderData,
      ReturnFromCustomerHeaderData returnFromCustomerVerificationHeaderData,
      ReturnFromCustomerLineSelectorData pickEditLineEdit,
      ReturnFromCustomerHeaderData rfcBookedVerificationHeaderData,
      PaymentInPlanData rfcPaymentInVerificationData,
      ReturnMaterialReceiptHeaderData returnMaterialReceiptHeaderData,
      ReturnMaterialReceiptHeaderData returnMaterialReceiptVerificationHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeader,
      SalesInvoiceHeaderData salesInvoiceVerificationHeaderData,
      PaymentPlanData invoicePaymentInVerificationData,
      PaymentPlanData paidInvoicePaymentInVerificationData,
      PaymentDetailsData paidInvoicePaymentDetailsVerificationData) {
    super();
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentVerificationHeaderData = goodsShipmentVerificationHeaderData;
    this.goodsShipmentLinesData = goodsShipmentLinesData;
    this.returnFromCustomerHeaderData = returnFromCustomerHeaderData;
    this.returnFromCustomerVerificationHeaderData = returnFromCustomerVerificationHeaderData;
    this.pickEditLineEdit = pickEditLineEdit;
    this.rfcBookedVerificationHeaderData = rfcBookedVerificationHeaderData;
    this.rfcPaymentInVerificationData = rfcPaymentInVerificationData;
    this.returnMaterialReceiptHeaderData = returnMaterialReceiptHeaderData;
    this.returnMaterialReceiptVerificationHeaderData = returnMaterialReceiptVerificationHeaderData;
    this.salesInvoiceHeader = salesInvoiceHeader;
    this.salesInvoiceVerificationHeaderData = salesInvoiceVerificationHeaderData;
    this.invoicePaymentInVerificationData = invoicePaymentInVerificationData;
    this.paidInvoicePaymentInVerificationData = paidInvoicePaymentInVerificationData;
    this.paidInvoicePaymentDetailsVerificationData = paidInvoicePaymentDetailsVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> values() {
    Object[][] data = new Object[][] { {

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain East warehouse")
            .build(),

        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Volley Ball").build())
            .movementQuantity("2025")
            .storageBin("Return bin")
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .movementDate(OBDate.CURRENT_DATE)
            .build(),

        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Volley Ball").build())
            .movementQuantity("2025")
            .build(),

        new ReturnFromCustomerHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .invoiceTerms("Immediate")
            .build(),

        new ReturnFromCustomerHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .invoiceTerms("Immediate")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .build(),

        new ReturnFromCustomerLineSelectorData.Builder().returned("2025")
            .unitPrice("28.50")
            .build(),

        new ReturnFromCustomerHeaderData.Builder().documentStatus("Booked")
            .grandTotalAmount("63,483.75")
            .build(),

        new PaymentInPlanData.Builder().expected("-63,483.75")
            .outstanding("-63,483.75")
            .numberOfPayments("0")
            .build(),

        new ReturnMaterialReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain East warehouse")
            .build(),

        new ReturnMaterialReceiptHeaderData.Builder().accountingDate(OBDate.CURRENT_DATE)
            .warehouse("Spain East warehouse")
            .build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .transactionDocument("Reversed Sales Invoice")
            .build(),

        new SalesInvoiceHeaderData.Builder().invoiceDate(OBDate.CURRENT_DATE).build(),

        new PaymentPlanData.Builder().expected("-63,483.75")
            .outstanding("-63,483.75")
            .numberOfPayments("0")
            .build(),

        new PaymentPlanData.Builder().expected("-63,483.75")
            .outstanding("0.00")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().amount("-63,483.75").build(),

        } };

    return Arrays.asList(data);
  }

  @Test
  public void APRRegression33500In10Test() {

    logger.info(
        "** Start of test case [APRRegression33500In10] Test regression 33500 Return From Customer - Paid Reversed Sales Invoice. **");

    // Create a goods receipt for the product
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();

    String receiptNo = (String) goodsReceipt.getData().getDataField("documentNo");
    logger.info("** Goods receipt Document No. {} **", receiptNo);

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    String storageBin = (String) goodsReceiptLines.getData("storageBin");
    goodsShipmentLinesData.addDataField("storageBin", storageBin);

    // Ship the product units
    GoodsShipment shipment = new GoodsShipment(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    shipment.create(goodsShipmentHeaderData);
    shipment.assertSaved();
    shipment.assertData(goodsShipmentVerificationHeaderData);

    String shipmentNo = (String) shipment.getData().getDataField("documentNo");
    logger.info("** Goods Shipment Document No. {} **", shipmentNo);

    GoodsShipment.Lines shipmentLines = shipment.new Lines(mainPage);
    shipmentLines.create(goodsShipmentLinesData);
    shipmentLines.assertSaved();

    // Complete the shipment
    shipment.complete();
    shipment.assertProcessCompletedSuccessfully2();

    // Return from customer the good shipment
    ReturnFromCustomer returnFromCustomer = new ReturnFromCustomer(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnFromCustomer.create(returnFromCustomerHeaderData);
    returnFromCustomer.assertSaved();
    returnFromCustomer.assertData(returnFromCustomerVerificationHeaderData);

    String rfcDocumentNo = (String) returnFromCustomer.getData("documentNo");
    logger.info("** Return From Customer Document No. {} **", rfcDocumentNo);

    PickAndExecuteWindow<ReturnFromCustomerLineSelectorData> pickEdit = returnFromCustomer
        .openPickAndEditLines();
    pickEdit.filter(
        new ReturnFromCustomerLineSelectorData.Builder().shipmentNumber(shipmentNo).build());
    pickEdit.edit(pickEditLineEdit);
    pickEdit.process();
    returnFromCustomer.book();

    returnFromCustomer.assertData(rfcBookedVerificationHeaderData);

    // Check the RFC Payment In Plan
    PaymentInPlan paymentInPlan = returnFromCustomer.new PaymentInPlan(mainPage);
    paymentInPlan.assertData(rfcPaymentInVerificationData);

    // Create the Return Material Receipt from Return From Customer
    ReturnMaterialReceipt materialReceipt = new ReturnMaterialReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    materialReceipt.create(returnMaterialReceiptHeaderData);
    materialReceipt.assertSaved();
    materialReceipt.assertData(returnMaterialReceiptVerificationHeaderData);

    // Create Receipt lines
    PickAndExecuteWindow<ReturnMaterialReceiptLineSelectorData> pickEditMaterialReceipt = materialReceipt
        .openPickAndEditLines();
    pickEditMaterialReceipt.filter(
        new ReturnMaterialReceiptLineSelectorData.Builder().rMOrderNo(rfcDocumentNo).build());
    pickEditMaterialReceipt
        .edit(new ReturnMaterialReceiptLineSelectorData.Builder().receiving("2025")
            .storageBin("Return bin")
            .build(), 0);
    pickEditMaterialReceipt.process();

    materialReceipt.complete();
    materialReceipt.assertProcessCompletedSuccessfully2();

    String receiptDocumentNo = (String) materialReceipt.getData("documentNo");
    String receiptId = String.format("%s - %s - %s", receiptDocumentNo,
        (String) materialReceipt.getData("movementDate"),
        (String) materialReceipt.getData("businessPartner"));
    logger.info("** Return Material Receipt Document No. {} **", receiptId);

    // Create the Reversed Sales Invoice
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.create(salesInvoiceHeader);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceVerificationHeaderData);

    String invoiceNo = (String) invoice.getTab().getData("documentNo");
    logger.info("** Reversed Sales Invoice Document No. {} **", invoiceNo);
    invoice.createLinesFromShipment(receiptId);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(new SalesInvoiceHeaderData.Builder().grandTotalAmount("-63,483.75").build());

    // Check invoice Payment in
    SalesInvoice.PaymentInPlan invoicePaymentIn = invoice.new PaymentInPlan(mainPage);
    invoicePaymentIn.assertData(invoicePaymentInVerificationData);

    // Pay the credit memo invoice
    AddPaymentProcess adp = invoice.openAddPayment();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> row = (List<Map<String, Object>>) adp.getOrderInvoiceGrid()
        .getSelectedRows();
    assertTrue("There must be one selected row", row.size() == 1);
    assertTrue(row.get(0).get("salesOrderNo").equals(rfcDocumentNo));
    assertTrue(row.get(0).get("invoiceNo").equals(invoiceNo));
    adp.process("Process Received Payment(s) and Deposit");

    // Check the payment in created by the add payment process
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    String payInDescription = String.format("Invoice No.: %s\nOrder No.: %s\n", invoiceNo,
        rfcDocumentNo);
    logger.info(payInDescription);
    Sleep.trySleep();
    paymentIn.select(new PaymentInHeaderData.Builder().amount("-63,483.75")
        .description(String.format("Invoice No.: %s", invoiceNo))
        .build());
    String paymentinDocument = (String) ((PaymentInTab) paymentIn.getTab()).getData("documentNo");
    String paymentDate = (String) ((PaymentInTab) paymentIn.getTab()).getData("paymentDate");
    String paymentBP = (String) ((PaymentInTab) paymentIn.getTab()).getData("businessPartner");
    String paymentInId = String.format("%s - %s - %s - -63483.75", paymentinDocument, paymentDate,
        paymentBP);
    logger.info("** Payment In {} **", paymentInId);

    // Check Return From Customer Payment In
    returnFromCustomer = new ReturnFromCustomer(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnFromCustomer
        .select(new ReturnFromCustomerHeaderData.Builder().documentNo(rfcDocumentNo).build());

    paymentInPlan = returnFromCustomer.new PaymentInPlan(mainPage);
    paymentInPlan.assertData(new PaymentInPlanData.Builder().expected("-63,483.75")
        .outstanding("0.00")
        .numberOfPayments("1")
        .build());

    // Check Return From Customer Payment In Details
    PaymentInDetails detailTab = paymentInPlan.new PaymentInDetails(mainPage);
    detailTab.selectWithoutFiltering(0);
    detailTab.assertData(new PaymentInDetailsData.Builder().payment(paymentInId).build());

    // Open the invoice
    invoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    invoice.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo).build());

    // Check Invoice Payment In
    invoicePaymentIn = invoice.new PaymentInPlan(mainPage);
    invoicePaymentIn.assertData(paidInvoicePaymentInVerificationData);

    // Check Invoice Payment In Details
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentDetails = invoicePaymentIn.new PaymentInDetails(
        mainPage);
    invoicePaymentDetails.assertData(paidInvoicePaymentDetailsVerificationData);

    // Check that the invoice and order is not shown in the waiting payment grid
    paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();

    paymentIn.create(new PaymentInHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
        .build());
    AddPaymentProcess addDetails = paymentIn.addDetailsOpen();
    addDetails.setParameterValue("transaction_type", "Orders and Invoices");
    AddPaymentGrid grid = addDetails.getOrderInvoiceGrid();
    Sleep.trySleep();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> rows = (List<Map<String, Object>>) grid.getRows();
    for (Map<String, Object> rowg : rows) {
      logger.info(rowg.get("invoiceNo"));
      assertFalse("Invoice should not appear", rowg.get("invoiceNo").equals(invoiceNo));
    }

    logger.info(
        "** End of test case [APRRegression33500In10] Test regression 33500 Return From Customer - Paid Reversed Sales Invoice. **");
  }
}
