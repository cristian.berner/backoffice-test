/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Multicurrency Payments.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales010 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_REFERENCE_NO = "reference_no";

  /* Data for this test. */

  /* Data for [APRSales010] Create Sales Invoice. */
  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;

  /* Data for [APRSales010] Add Payment. */
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;
  /** The data to verify the multicurrency data in the add payment pop up. */
  AddPaymentPopUpData addPaymentMulticurrencyVerificationData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  /** The data to verify the application of the payment. */
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;

  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData2;
  /** The data to verify the payment in details data. */
  PaymentDetailsData paymentInDetailsData;
  PaymentInHeaderData paymentInHeaderData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data. @param salesInvoiceHeaderVerificationData The data to
   *          verify the creation of the Sales Invoice header data. @param
   *          completedSalesInvoiceHeaderVerificationData The data to verify the completion of the
   *          sales invoice. @param salesInvoiceLineData The data to verify the creation of the
   *          Sales Invoice lines data. @param salesInvoiceLineVerificationData The data to verify
   *          the creation of the Sales Invoice line. @param paymentInPlanData The data to verify
   *          the payment in plan. @param addPaymentInVerificationData The data to verify the data
   *          in the add payment pop up. @param addPaymentMulticurrencyVerificationData The data to
   *          verify the multicurrency data in the add payment pop up. @param
   *          addPaymentTotalsVerificationData The data to verify the totals data in the add payment
   *          pop up. @param payedSalesInvoiceHeaderVerificationData The data to verify the
   *          application of the payment.
   */
  public APRSales010(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData, PaymentPlanData paymentInPlanData,
      AddPaymentPopUpData addPaymentInVerificationData,
      AddPaymentPopUpData addPaymentMulticurrencyVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData,
      PaymentPlanData paymentInPlanData2, PaymentDetailsData paymentInDetailsData,
      PaymentInHeaderData paymentInHeaderData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.addPaymentMulticurrencyVerificationData = addPaymentMulticurrencyVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;
    this.paymentInPlanData2 = paymentInPlanData2;
    this.paymentInDetailsData = paymentInDetailsData;
    this.paymentInHeaderData = paymentInHeaderData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRSales010] Create Sales Invoice. */
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-8 (Transactions)")
            .priceList("Customer A USD")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A USD")
            .paymentMethod("Acc-8 (Transactions)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("200.00")
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("200.00")
            .currency("USD")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("100")
            .tax("Exempt 3%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 3%")
            .lineNetAmount("200.00")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expected("200.00")
            .received("0.00")
            .outstanding("200.00")
            .build(),

        /* Data for [APRSales010] Add Payment. */
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("USD")
            .expected_payment("200.00")
            .actual_payment("200.00")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().c_currency_to_id("EUR")
            .converted_amount("80.00")
            .conversion_rate("0.4")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("200.00")
            .total("200.00")
            .difference("0.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("200.00")
            .currency("USD")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-8 (Transactions)")
            .expected("200.00")
            .received("200.00")
            .outstanding("0.00")
            .build(),
        new PaymentDetailsData.Builder().paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-8 (Transactions)")
            .received("200.00")
            .amount("200.00")
            .account("Spain Bank - EUR")
            .build(),
        new PaymentInHeaderData.Builder().organization("USA")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Germany").build())
            .paymentMethod("Acc-6 (Transactions)")
            .account("Accounting Documents EURO - EUR")
            .currency("USD")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRSales010Test() throws ParseException {
    logger.info("** Start of test case [APRSales010] Multicurrency Payments. **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentInVerificationData);
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();

    // Sleep to execute display logics
    Sleep.trySleep(1000);
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();

    addPaymentProcess.assertMulticurrencyData(addPaymentMulticurrencyVerificationData);
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Received Payment(s)");

    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);

    paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData2);
    SalesInvoice.PaymentInPlan.PaymentInDetails paymentInDetails = paymentInPlan.new PaymentInDetails(
        mainPage);
    paymentInDetails.assertCount(1);
    paymentInDetails.assertData(paymentInDetailsData);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    AddPaymentProcess addPaymentProcess2 = paymentIn.addDetailsOpen();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();

    addPaymentProcess2.close();

    logger.info("** End of test case [APRSales010] Multicurrency Payments. **");
  }
}
