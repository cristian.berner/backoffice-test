/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.sharedtabs.UsedCreditSourceData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 29615
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression29615In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  PaymentInHeaderData paymentInHeaderData2;
  PaymentInHeaderData paymentInHeaderVerificationData3;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentInHeaderData paymentInHeaderVerificationData4;
  PaymentInLinesData paymentInLinesVerificationData2;
  UsedCreditSourceData paymentInUsedCreditVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData5;
  PaymentInLinesData paymentInLinesVerificationData3;
  UsedCreditSourceData paymentInUsedCreditVerificationData2;
  PaymentInHeaderData paymentInHeaderVerificationData6;

  /**
   * Class constructor.
   *
   */
  public APRRegression29615In(PaymentInHeaderData paymentInHeaderData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      PaymentInHeaderData paymentInHeaderData2,
      PaymentInHeaderData paymentInHeaderVerificationData3,
      AddPaymentPopUpData addPaymentVerificationData2,
      PaymentInHeaderData paymentInHeaderVerificationData4,
      PaymentInLinesData paymentInLinesVerificationData2,
      UsedCreditSourceData paymentInUsedCreditVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData5,
      PaymentInLinesData paymentInLinesVerificationData3,
      UsedCreditSourceData paymentInUsedCreditVerificationData2,
      PaymentInHeaderData paymentInHeaderVerificationData6) {
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.paymentInHeaderData2 = paymentInHeaderData2;
    this.paymentInHeaderVerificationData3 = paymentInHeaderVerificationData3;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.paymentInHeaderVerificationData4 = paymentInHeaderVerificationData4;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    this.paymentInUsedCreditVerificationData = paymentInUsedCreditVerificationData;
    this.paymentInHeaderVerificationData5 = paymentInHeaderVerificationData5;
    this.paymentInLinesVerificationData3 = paymentInLinesVerificationData3;
    this.paymentInUsedCreditVerificationData2 = paymentInUsedCreditVerificationData2;
    this.paymentInHeaderVerificationData6 = paymentInHeaderVerificationData6;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("100.00")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("1 (Spain)")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("100.00")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("100.00")
            .build(),
        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("100.00")
            .usedCredit("0.00")
            .build(),
        new PaymentInLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .received("100.00")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("36")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("72.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("74.52")
            .summedLineAmount("72.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("1 (Spain)")
            .amount("0.00")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("0.00")
            .expected_payment("74.52")
            .amount_gl_items("0.00")
            .amount_inv_ords("74.52")
            .total("74.52")
            .difference("25.48")
            .build(),
        new PaymentInHeaderData.Builder().status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("74.52")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("74.52")
            .expected("74.52")
            .received("74.52")
            .orderno("")
            .glitemname("")
            .build(),
        new UsedCreditSourceData.Builder().amount("74.52").toCurrency("EUR").build(),

        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("25.48")
            .organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("-25.48")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new PaymentInLinesData.Builder().dueDate("")
            .invoiceAmount("")
            .expected("")
            .received("-25.48")
            .orderno("")
            .invoiceno("")
            .glitemname("")
            .build(),
        new UsedCreditSourceData.Builder().amount("25.48").toCurrency("EUR").build(),

        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("100.00")
            .usedCredit("100.00")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29615 - Payment In flow
   *
   * Payment from Order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression29615InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression29615In] Test regression 29615 - Payment In flow. **");

    PaymentIn paymentIn1 = new PaymentIn(mainPage).open();
    paymentIn1.create(paymentInHeaderData);
    paymentIn1.assertSaved();
    paymentIn1.assertData(paymentInHeaderVerificationData);
    AddPaymentProcess addPaymentProcess1 = paymentIn1.addDetailsOpen();
    String paymentNo = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    addPaymentProcess1.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess1.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep(1000);
    addPaymentProcess1.process("Process Received Payment(s) and Deposit",
        "Leave the credit to be used later", addPaymentVerificationData);
    paymentIn1.assertPaymentCreatedSuccessfully();
    paymentIn1.assertData(paymentInHeaderVerificationData2);
    PaymentIn.Lines paymentInLines1 = paymentIn1.new Lines(mainPage);
    paymentInLines1.assertCount(1);
    paymentInLines1.assertData(paymentInLinesVerificationData);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    PaymentIn paymentIn2 = new PaymentIn(mainPage).open();
    Sleep.trySleep(15000);
    paymentIn2.create(paymentInHeaderData2);
    paymentIn2.assertSaved();
    paymentIn2.assertData(paymentInHeaderVerificationData3);
    AddPaymentProcess addPaymentProcess2 = paymentIn2.addDetailsOpen();
    addPaymentProcess2.process("Invoices", invoiceNo, paymentNo,
        "Process Received Payment(s) and Deposit", "Refund amount to customer",
        addPaymentVerificationData2);
    paymentIn2.assertRefundedPaymentCreatedSuccessfully();
    paymentIn2.assertData(paymentInHeaderVerificationData4);
    PaymentIn.Lines paymentInLines2 = paymentIn2.new Lines(mainPage);
    paymentInLines2.assertCount(1);
    paymentInLines2.assertData((PaymentInLinesData) paymentInLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo));
    PaymentIn.UsedCreditSource paymentInUsedCredit2 = paymentIn2.new UsedCreditSource(mainPage);
    paymentInUsedCredit2.assertCount(1);
    paymentInUsedCredit2.assertData((UsedCreditSourceData) paymentInUsedCreditVerificationData
        .addDataField("creditPaymentUsed", paymentNo));

    PaymentIn paymentIn3 = new PaymentIn(mainPage).open();
    paymentIn3.select(
        new PaymentInHeaderData.Builder().documentNo(String.valueOf(Integer.valueOf(paymentNo) + 2))
            .build());
    paymentIn3.assertData(paymentInHeaderVerificationData5);
    PaymentIn.Lines paymentInLines3 = paymentIn3.new Lines(mainPage);
    paymentInLines3.assertCount(1);
    paymentInLines3.assertData(paymentInLinesVerificationData3);
    PaymentIn.UsedCreditSource paymentInUsedCredit3 = paymentIn3.new UsedCreditSource(mainPage);
    paymentInUsedCredit3.assertCount(1);
    paymentInUsedCredit3.assertData((UsedCreditSourceData) paymentInUsedCreditVerificationData2
        .addDataField("creditPaymentUsed", paymentNo));

    paymentIn1 = new PaymentIn(mainPage).open();
    paymentIn1.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn1.assertData(paymentInHeaderVerificationData6);
    paymentInLines1 = paymentIn1.new Lines(mainPage);
    paymentInLines1.assertCount(1);
    paymentInLines1.assertData(paymentInLinesVerificationData);
    PaymentIn.UsedCreditSource paymentInUsedCredit1 = paymentIn1.new UsedCreditSource(mainPage);
    paymentInUsedCredit1.assertCount(0);

    logger.info(
        "** End of test case [APRRegression29615In] Test regression 29615 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
