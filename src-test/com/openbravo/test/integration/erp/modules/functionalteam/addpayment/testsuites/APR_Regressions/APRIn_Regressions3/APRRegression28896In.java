/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.InformationData;
import com.openbravo.test.integration.erp.data.generalsetup.enterprise.organization.OrganizationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.generalsetup.enterprise.Organization;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 28896
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression28896In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  OrganizationData organizationData;
  InformationData informationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;
  InformationData informationData2;

  /**
   * Class constructor.
   *
   */
  public APRRegression28896In(OrganizationData organizationData, InformationData informationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      InformationData informationData2) {
    this.organizationData = organizationData;
    this.informationData = informationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    this.informationData2 = informationData2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new OrganizationData.Builder().name("Spain").build(),
        new InformationData.Builder().purchaseInvoiceReference("Supplier Reference").build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("1")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("2.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("2.07")
            .summedLineAmount("2.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("2.07")
            .expected_payment("2.07")
            .amount_gl_items("0.00")
            .amount_inv_ords("2.07")
            .total("2.07")
            .difference("0.00")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("2.07")
            .summedLineAmount("2.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),

        new InformationData.Builder().purchaseInvoiceReference("Invoice Document Number")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28896 - Payment In flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression28896InTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression28896In] Test regression 28896 - Payment In flow. **");

    Organization organization = new Organization(mainPage).open();
    organization.select(organizationData);
    Organization.Information information = organization.new Information(mainPage);
    information.edit(informationData);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.process("Invoices", invoiceNo, "Process Received Payment(s) and Deposit",
        addPaymentVerificationData);
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData2);

    organization = new Organization(mainPage).open();
    Sleep.trySleep(15000);
    organization.select(organizationData);
    information = organization.new Information(mainPage);
    information.edit(informationData2);

    logger.info(
        "** End of test case [APRRegression28896In] Test regression 28896 - Payment In flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
