/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.aum;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 *
 * Class for Test UOMa020
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class AUMa020DisablePreference extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  PreferenceData disablePreference;

  public AUMa020DisablePreference(PreferenceData disablePreference) {
    super();
    this.disablePreference = disablePreference;
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
  }

  @Parameters
  public static Collection<Object[]> returnValues() {
    Object[][] data = new Object[][] {
        { new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void UOMa020Test() throws ParseException {

    logger.debug("** Start test case [UOMa020] Disable UomManagement Preference **");

    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System")
        .client("System")
        .organization("*")
        .build());

    Preference.PreferenceTab.delete(mainPage, disablePreference);

    logger.debug("** End test case [UOMa020] Disable UomManagement Preference **");
  }

}
