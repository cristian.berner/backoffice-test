/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018-2020 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.createlinesfrom.createlinesfrom3;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;

/**
 * Sales Invoice Create Line From. Suite testing Sales Invoice Create Line From flows
 *
 * @author Andy Armaignac
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ CLF_TwoDeliveredSalesOrderOneShipment.class,
    CLF_TwoDeliveredSalesOrderTwoShipment.class, CLF_TwoGoodsShipmentRelatedOneSalesOrder.class,
    CLF_TwoGoodsShipmentWithTwoOrders.class })
public class CLF_SalSuite3 {

}
