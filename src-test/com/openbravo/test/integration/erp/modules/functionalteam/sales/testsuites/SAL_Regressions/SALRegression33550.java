/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.setup.paymentmethod.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.CustomerData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.paymentmethod.PaymentMethod;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 33550
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class SALRegression33550 extends OpenbravoERPTest {

  private static final String PAYMENT_METHOD_TEST33550 = "Test33550";
  private static final String PAYMENT_METHOD = "paymentMethod";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PaymentMethodData paymentMethodData;
  PaymentMethodData verifyPaymentMethodData;
  AccountData accountHeaderData;
  com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData finPaymentMethodData;
  AccountData accountHeaderData2;
  com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData finPaymentMethodData2;
  BusinessPartnerData businessPartnerData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceLinesData salesInvoiceLineData;

  /**
   * Class constructor.
   *
   */
  public SALRegression33550(PaymentMethodData paymentMethodData,
      PaymentMethodData verifyPaymentMethodData, AccountData accountHeaderData,
      com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData finPaymentMethodData,
      AccountData accountHeaderData2,
      com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData finPaymentMethodData2,
      BusinessPartnerData businessPartnerData, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceLinesData salesInvoiceLineData) {
    this.paymentMethodData = paymentMethodData;
    this.verifyPaymentMethodData = verifyPaymentMethodData;
    this.accountHeaderData = accountHeaderData;
    this.finPaymentMethodData = finPaymentMethodData;
    this.accountHeaderData2 = accountHeaderData2;
    this.finPaymentMethodData2 = finPaymentMethodData2;
    this.businessPartnerData = businessPartnerData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceLineData = salesInvoiceLineData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PaymentMethodData.Builder().organization("*")
            .payinAllow(true)
            .payinIsMulticurrency(true)
            .build(),
        new PaymentMethodData.Builder().uponDepositUse("Cleared Payment Account").build(),
        new AccountData.Builder().name("Spain Bank").build(),
        new com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData.Builder()
            .pmdefault(false)
            .build(),

        new AccountData.Builder().name("USA Bank").build(),
        new com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData.Builder()
            .pmdefault(true)
            .build(),
        new BusinessPartnerData.Builder().searchKey("CUSA").build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("1")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33550 - Cannot void an invoice if the business partner does not have a default
   * financial account defined.
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void SALRegression33550Test() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [SALRegression33550] Test regression 33550 - Cannot void an invoice if the business partner does not have a default financial account defined. **");

    int recordCount = PaymentMethod.PaymentMethodTab.getRecordCount(mainPage,
        new PaymentMethodData.Builder().name(PAYMENT_METHOD_TEST33550).build());
    String paymentMethodName = PAYMENT_METHOD_TEST33550 + "-" + (recordCount + 1);

    paymentMethodData.addDataField("name", paymentMethodName);
    finPaymentMethodData.addDataField(PAYMENT_METHOD, paymentMethodName);
    finPaymentMethodData2.addDataField(PAYMENT_METHOD, paymentMethodName);
    salesInvoiceHeaderData.addDataField(PAYMENT_METHOD, paymentMethodName);

    // Check payment method is configured to post only financial account

    PaymentMethod.PaymentMethodTab.create(mainPage, paymentMethodData);
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.create(finPaymentMethodData);
    financialAccount.select(accountHeaderData2);
    paymentMethod.create(finPaymentMethodData2);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.Customer.edit(mainPage, new CustomerData.Builder().account("").build());
    BusinessPartner.BusinessPartnerTab.close(mainPage);
    Sleep.trySleep(1500);
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoiceLine.assertSaved();
    String documentNo = salesInvoice.getData("documentNo").toString();
    documentNo = documentNo.substring(0, 2)
        .concat(String.valueOf((Integer.valueOf(documentNo.substring(2)) + 1)));
    salesInvoice.complete();
    salesInvoice.close();
    salesInvoice.assertProcessCompletedSuccessfully2("Reversed by document: " + documentNo);

    logger.info(
        "** End of test case [SALRegression33550] Test regression 33550 - Cannot void an invoice if the business partner does not have a default financial account defined. **");
  }
}
