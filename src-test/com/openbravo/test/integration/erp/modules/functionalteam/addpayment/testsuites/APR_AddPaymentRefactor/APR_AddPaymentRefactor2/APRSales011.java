/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.generalsetup.application.currency.ConversionRatesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.conversionrates.ConversionRates;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Multicurrency Payments.
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRSales011 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_REFERENCE_NO = "reference_no";
  private static final String PARAM_FIN_ACCT = "fin_financial_account_id";

  /* Data for this test. */

  ConversionRatesData conversionRatesData;
  ConversionRatesData conversionRatesData2;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  AddPaymentPopUpData addPaymentInVerificationData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData;

  AccountData accountHeaderData2;
  TransactionsData transactionLinesData;

  /**
   * Class constructor.
   *
   * @param salesInvoiceHeaderData
   *          The Sales Invoice header data.
   * @param salesInvoiceHeaderVerificationData
   *          The data to verify the creation of the Sales Invoice header data.
   * @param completedSalesInvoiceHeaderVerificationData
   *          The data to verify the completion of the sales invoice.
   * @param salesInvoiceLineData
   *          The data to verify the creation of the Sales Invoice lines data.
   * @param salesInvoiceLineVerificationData
   *          The data to verify the creation of the Sales Invoice line.
   * @param addPaymentInVerificationData
   *          The data to verify the data in the add payment pop up.
   * @param addPaymentTotalsVerificationData
   *          The data to verify the totals data in the add payment pop up.
   * @param payedSalesInvoiceHeaderVerificationData
   *          The data to verify the application of the payment.
   */
  public APRSales011(ConversionRatesData conversionRatesData,
      ConversionRatesData conversionRatesData2, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentInVerificationData,
      AddPaymentPopUpData addPaymentTotalsVerificationData,
      SalesInvoiceHeaderData payedSalesInvoiceHeaderVerificationData,
      AccountData accountHeaderData2, TransactionsData transactionLinesData) {

    this.conversionRatesData = conversionRatesData;
    this.conversionRatesData2 = conversionRatesData2;

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;

    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedSalesInvoiceHeaderVerificationData = payedSalesInvoiceHeaderVerificationData;

    this.accountHeaderData2 = accountHeaderData2;
    this.transactionLinesData = transactionLinesData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new ConversionRatesData.Builder().currency("EUR")
            .toCurrency("USD")
            .multipleRateBy("2.5")
            .divideRateBy("0.4")
            .build(),
        new ConversionRatesData.Builder().organization("*")
            .currency("EUR")
            .toCurrency("USD")
            .validFromDate("01-10-2011")
            .validToDate("31-12-9999")
            .multipleRateBy("2.5")
            .divideRateBy("0.4")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Mexico").build())
            .paymentMethod("Acc-8 (Transactions)")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress("Mexico")
            .priceList("Customer A")
            .paymentMethod("Acc-8 (Transactions)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("100")
            .tax("Exempt 3%")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("Exempt 3%")
            .lineNetAmount("200.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("200.00")
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("200.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Customer Mexico")
            .fin_paymentmethod_id("Acc-8 (Transactions)")
            .fin_financial_account_id("Mexico Bank - USD")
            .c_currency_id("EUR")
            .actual_payment("200.00")
            .expected_payment("200.00")
            .c_currency_to_id("USD")
            .converted_amount("500.00")
            .conversion_rate("2.5")
            .payment_date(OBDate.CURRENT_DATE)
            .transaction_type("Invoices")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("200.00")
            .total("200.00")
            .difference("0.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("200.00")
            .currency("EUR")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .transactionDate(OBDate.CURRENT_DATE)
            .currency("EUR")
            .depositAmount("200.00")
            .paymentAmount("0.00")
            .organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Customer Mexico").build())
            .build() } };

    return Arrays.asList(data);
  }

  /**
   * Test to create a sales order, and a goods receipt and sales invoice from that order.
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRSales011Test() throws ParseException, OpenbravoERPTestException {
    logger.info("** Start of test case [APRSales011] Multicurrency Payments. **");

    ConversionRates conversionRates = new ConversionRates(mainPage).open();
    conversionRates.select(conversionRatesData);
    conversionRates.assertData(conversionRatesData2);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertData(addPaymentInVerificationData);
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);

    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();
    addPaymentProcess.setParameterValue(PARAM_FIN_ACCT, "Spain Bank - EUR");
    addPaymentProcess.getField(PARAM_REFERENCE_NO).focus();

    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Received Payment(s) and Deposit");
    salesInvoice.assertPaymentCreatedSuccessfully();
    salesInvoice.assertData(payedSalesInvoiceHeaderVerificationData);

    FinancialAccount financialAccount2 = new FinancialAccount(mainPage).open();
    financialAccount2.select(accountHeaderData2);
    FinancialAccount.Transaction transactions = financialAccount2.new Transaction(mainPage);
    transactions.select(new TransactionsData.Builder().documentNo(paymentNo).build());
    transactions.assertData(transactionLinesData);

    logger.info("** End of test case [APRSales011] Multicurrency Payments. **");
  }
}
