/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 31873
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class SALRegression31873In1 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData3;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData4;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData5;
  PaymentInLinesData paymentInLinesVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData3;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public SALRegression31873In1(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData3,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData4,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData5,
      PaymentInLinesData paymentInLinesVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData3, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.orderPaymentInDetailsData = orderPaymentInDetailsData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentInPlanData2 = orderPaymentInPlanData2;
    this.orderPaymentInDetailsData2 = orderPaymentInDetailsData2;
    this.orderPaymentInDetailsData3 = orderPaymentInDetailsData3;
    this.orderPaymentInDetailsData4 = orderPaymentInDetailsData4;
    this.orderPaymentInDetailsData5 = orderPaymentInDetailsData5;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    this.paymentInLinesVerificationData3 = paymentInLinesVerificationData3;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .invoiceTerms("Immediate")
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("2").tax("VAT 10% USA").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("4.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("4.00")
            .grandTotalAmount("4.40")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents DOLLAR - USD")
            .c_currency_id("EUR")
            .actual_payment("4.40")
            .expected_payment("4.40")
            .amount_gl_items("0.00")
            .amount_inv_ords("4.40")
            .total("4.40")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .received("4.40")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("4.40")
            .received("4.40")
            .writeoff("0.00")
            .expectedConverted("11.00")
            .receivedConverted("11.00")
            .exchangeRate("2.5")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("4.40")
            .account("Accounting Documents DOLLAR - USD")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("4.40")
            .expected("4.40")
            .received("4.40")
            .invoiceno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder().invoicedQuantity("1").build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("2.20")
            .summedLineAmount("2.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good C").build())
            .invoicedQuantity("1")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10% USA")
            .lineNetAmount("2.00")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("2.20")
            .received("2.20")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("2.20")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("2.20")
            .account("Accounting Documents DOLLAR - USD")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "43800", "438. Anticipos de clientes", "2.20", "" },
            { "70000", "Ventas de mercaderías", "", "2.00" },
            { "47700", "Hacienda Pública IVA repercutido", "", "0.20" } },
        new String[][] { { "2450", "Customer Prepayments", "5.50", "" },
            { "41100", "Sales", "", "5.00" }, { "21400", "Tax Due", "", "0.50" } },

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("4.40")
            .received("4.40")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("4.40")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("4.40")
            .received("2.20")
            .writeoff("0.00")
            .expectedConverted("11.00")
            .receivedConverted("5.50")
            .exchangeRate("2.5")
            .status("Deposited not Cleared")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .expected("2.20")
            .build(),
        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .expected("2.20")
            .received("2.20")
            .writeoff("0.00")
            .expectedConverted("5.50")
            .receivedConverted("5.50")
            .exchangeRate("2.5")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("4.40")
            .expected("4.40")
            .received("2.20")
            .glitemname("")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("2.20")
            .expected("2.20")
            .received("2.20")
            .glitemname("")
            .build(),

        new String[][] { { "43100", "Efectos comerciales en cartera", "4.40", "" },
            { "43800", "438. Anticipos de clientes", "", "4.40" } },
        new String[][] { { "11300", "Bank in transit", "11.00", "" },
            { "2450", "Customer Prepayments", "", "11.00" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 31873 - Full prepayment - Decrease invoice
   *
   * @throws ParseException
   */
  @Test
  public void SALRegression31873In1Test() throws ParseException {
    logger.info(
        "** Start of test case [SALRegression31873In1] Test regression 31873 - Full prepayment - Decrease invoice. **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    salesOrder.addPayment("Accounting Documents DOLLAR - USD",
        "Process Received Payment(s) and Deposit", addPaymentVerificationData);
    salesOrder.assertPaymentCreatedSuccessfully();
    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    SalesOrder.PaymentInPlan orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData);
    String paymentNo = orderPaymentInDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(1);
    paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.edit(salesInvoiceLineData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    salesInvoiceLine.assertCount(1);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    SalesInvoice.PaymentInPlan invoicePaymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // TODO 1L: Temporal static sleep to check if post() works properly
    Sleep.trySleep(10000);
    salesInvoice.post();
    // TODO 1L: Temporal static sleep to check if post() works properly
    Sleep.trySleep(10000);
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post1 = new Post(mainPage);
    post1.assertJournalLinesCount(journalEntryLines.length);
    post1.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post1 = new Post(mainPage);
    post1.assertJournalLinesCount(journalEntryLines2.length);
    post1.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = salesOrder.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData2);
    orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.select(orderPaymentInDetailsData2);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData3);
    orderPaymentInDetails.cancelOnGrid();
    orderPaymentInDetails.getTab().clearFilters();
    orderPaymentInDetails.getTab().unselectAll();
    orderPaymentInDetails.select(orderPaymentInDetailsData4);
    // TODO 3L: Testing performance issue. If not required, delete it.
    Sleep.trySleep(10000);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData5);

    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    paymentIn.assertData(paymentInHeaderVerificationData);
    paymentInLines = paymentIn.new Lines(mainPage);
    paymentInLines.assertCount(2);
    // FIXME: Uncomment once issue 32234 is fixed
    // paymentInLines.select(new PaymentInLinesData.Builder().invoiceno("#").build());
    // paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData2.addDataField(
    // "orderPaymentSchedule$order$documentNo", orderNo).addDataField(
    // "invoicePaymentSchedule$invoice$documentNo", ""));
    // paymentInLines.cancelOnGrid();
    // paymentInLines.getTab().clearFilters();
    // paymentInLines.getTab().unselectAll();
    // paymentInLines.select(new PaymentInLinesData.Builder().invoiceno("!#").build());
    // paymentInLines.assertData((PaymentInLinesData) paymentInLinesVerificationData3.addDataField(
    // "orderPaymentSchedule$order$documentNo", orderNo).addDataField(
    // "invoicePaymentSchedule$invoice$documentNo", invoiceNo));

    paymentIn.open();
    paymentIn.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines3.length);
    post2.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines4.length);
    post2.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [SALRegression31873In1] Test regression 31873 - Full prepayment - Decrease invoice. **");
  }
}
