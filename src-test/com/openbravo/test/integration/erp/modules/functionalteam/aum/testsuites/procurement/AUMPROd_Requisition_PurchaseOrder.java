/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.ManageRequisitionsHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.ManageRequisitionsLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.managerequisitions.MatchedPOLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.requisition.RequisitionLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ManageRequisitions;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.Requisition;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;
import com.openbravo.test.integration.util.OBDate;

/**
 * AUMPROd010 and AUMPROd020 Test cases
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class AUMPROd_Requisition_PurchaseOrder extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();
  static String DATE_FORMAT = ConfigurationProperties.INSTANCE.getDateFormat();

  /* Data for this test. */
  /* Data for [AUMPROd010]Create requisition. */
  /** The requisition header data. */
  RequisitionHeaderData requisitionHeaderData;
  /** The data to verify the requisition creation. */
  RequisitionHeaderData requisitionHeaderVerificationData;
  /** The requisition lines data. */
  RequisitionLinesData requisitionLinesData;
  /** The data to verify the requisition creation. */
  RequisitionLinesData requisitionLinesVerificationData;
  /** The completed requisition verification data. */
  RequisitionHeaderData completedRequisitionData;

  /* Data for [AUMPROd020]Create Purchase order. */
  /** The data to verify the purchase order creation on manage requisition header. */
  ManageRequisitionsHeaderData manageRequisitionsHeaderVerificationData;
  /** The data to verify the manage requisitions line. */
  ManageRequisitionsLinesData manageRequisitionLinesVerificationData;
  /** The data to verify the matched po line. */
  MatchedPOLinesData matchedPOLinesData;

  public AUMPROd_Requisition_PurchaseOrder(RequisitionHeaderData requisitionHeaderData,
      RequisitionHeaderData requisitionHeaderVerificationData,
      RequisitionLinesData requisitionLinesData,
      RequisitionLinesData requisitionLinesVerificationData,
      RequisitionHeaderData completedRequisitionData,
      ManageRequisitionsHeaderData manageRequisitionsHeaderVerificationData,
      ManageRequisitionsLinesData manageRequisitionLinesVerificationData,
      MatchedPOLinesData matchedPOLinesData) {
    this.requisitionHeaderData = requisitionHeaderData;
    this.requisitionHeaderVerificationData = requisitionHeaderVerificationData;
    this.requisitionLinesData = requisitionLinesData;
    this.requisitionLinesVerificationData = requisitionLinesVerificationData;
    this.completedRequisitionData = completedRequisitionData;
    this.manageRequisitionsHeaderVerificationData = manageRequisitionsHeaderVerificationData;
    this.manageRequisitionLinesVerificationData = manageRequisitionLinesVerificationData;
    this.matchedPOLinesData = matchedPOLinesData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> createValues() throws IOException {
    return Arrays.asList(new Object[][] { {
        /* Parameters for for [AUMPROd010]Create requisition. */
        new RequisitionHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new RequisitionHeaderData.Builder().documentStatus("Draft")
            .priceList("Purchase")
            .currency("EUR")
            .userContact("QAAdmin")
            .build(),
        new RequisitionLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMA")
                .quantityOnHand("33.6")
                .uOM("Bag")
                .build())
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .operativeQuantity("11.2")
            .build(),
        new RequisitionLinesData.Builder().lineNetAmount("67.20")
            .requisitionLineStatus("Open")
            .priceList("Purchase")
            .build(),
        new RequisitionHeaderData.Builder().documentStatus("Completed").build(),

        /* Parameters for [AUMPROd020]Create Purchase order. */
        new ManageRequisitionsHeaderData.Builder().documentStatus("Closed").build(),
        new ManageRequisitionsLinesData.Builder().quantity("33.6").build(),
        new MatchedPOLinesData.Builder().quantity("33.6").build() } });
  }

  @Test
  public void AUMPROd_Requisition_PurchaseOrder_test()
      throws IOException, OpenbravoERPTestException {
    logger.info("** Start of test case [AUMPROd010] Create Requisition **");
    final Requisition requisition = new Requisition(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    requisition.create(requisitionHeaderData);
    requisition.assertSaved();
    requisition.assertData(requisitionHeaderVerificationData);

    final Requisition.Lines requisitionLines = requisition.new Lines(mainPage);
    final Calendar calendar = Calendar.getInstance();
    String currentDate = new SimpleDateFormat(DATE_FORMAT, Locale.US).format(calendar.getTime());
    requisitionLines.createOnFormWithoutSaving(new RequisitionLinesData.Builder().build());
    requisitionLines.setNeedByDate(currentDate);
    requisitionLines.edit(requisitionLinesData);
    requisitionLines.assertSaved();
    requisition.complete();
    requisition.assertProcessCompletedSuccessfully2();
    requisition.assertData(completedRequisitionData);
    DataObject fullRequisitionData = requisition.getData();
    logger.info("** End of test case [AUMPROd010] Create Requisition **");

    logger.info("** Start of test case [AUMPROd020] Create Purchase Order **");
    final ManageRequisitions manageRequisitions = new ManageRequisitions(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    String documentNo = (String) fullRequisitionData.getDataField("documentNo");
    ManageRequisitionsHeaderData manageRequisitionsHeaderData = new ManageRequisitionsHeaderData.Builder()
        .documentNo(documentNo)
        .build();
    manageRequisitions.select(manageRequisitionsHeaderData);
    manageRequisitions.createPurchaseOrder(OBDate.getSystemDate(), "Spain", "Spain warehouse");
    manageRequisitions.assertOrderCreateSuccessfully();
    manageRequisitions.assertData(manageRequisitionsHeaderData);
    final ManageRequisitions.Lines manageRequisitionsLines = manageRequisitions.new Lines(mainPage);
    manageRequisitionsLines.assertCount(1);
    manageRequisitionsLines.assertData(manageRequisitionLinesVerificationData);
    final ManageRequisitions.Lines.MatchedPOLines matchedPOLines = manageRequisitionsLines.new MatchedPOLines(
        mainPage);
    matchedPOLines.assertCount(1);
    matchedPOLines.assertData(matchedPOLinesData);
    logger.info("** End of test case [AUMPROd020] Create Purchase Order **");
  }
}
