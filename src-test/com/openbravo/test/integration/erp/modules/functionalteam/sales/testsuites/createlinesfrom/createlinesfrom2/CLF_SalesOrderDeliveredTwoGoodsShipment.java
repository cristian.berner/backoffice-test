/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.createlinesfrom.createlinesfrom2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Test Create Lines From Sales Order Delivered in Two Shipments
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_SalesOrderDeliveredTwoGoodsShipment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderLinesData salesOrderLinesData3;
  SalesOrderLinesData salesOrderLinesVerificationData3;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData2;
  GoodsShipmentLinesData goodsShipmentLineVerificationData3;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData2;
  SalesInvoiceLinesData salesInvoiceLineVerificationData3;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_SalesOrderDeliveredTwoGoodsShipment(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData, SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderLinesData salesOrderLinesData3,
      SalesOrderLinesData salesOrderLinesVerificationData3,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData2,
      GoodsShipmentLinesData goodsShipmentLineVerificationData3,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData2,
      SalesInvoiceLinesData salesInvoiceLineVerificationData3,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.salesOrderLinesData3 = salesOrderLinesData3;
    this.salesOrderLinesVerificationData3 = salesOrderLinesVerificationData3;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentLineVerificationData = goodsShipmentLineVerificationData;
    this.goodsShipmentLineVerificationData2 = goodsShipmentLineVerificationData2;
    this.goodsShipmentLineVerificationData3 = goodsShipmentLineVerificationData3;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.salesInvoiceLineVerificationData2 = salesInvoiceLineVerificationData2;
    this.salesInvoiceLineVerificationData3 = salesInvoiceLineVerificationData3;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .invoiceTerms("Immediate")
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("DGA")
            .priceListVersion("Customer A")
            .warehouse("Spain warehouse")
            .build()).orderedQuantity("10").tax("VAT 10%").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("3.00")
            .listPrice("3.00")
            .lineNetAmount("30.00")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("DGB")
            .priceListVersion("Customer A")
            .warehouse("Spain warehouse")
            .build()).orderedQuantity("15").tax("VAT 10%").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("4.00")
            .listPrice("4.00")
            .lineNetAmount("60.00")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("DGC")
            .priceListVersion("Customer A")
            .warehouse("Spain warehouse")
            .build()).orderedQuantity("20").tax("VAT 10%").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("5.00")
            .listPrice("5.00")
            .lineNetAmount("100.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("190.00")
            .grandTotalAmount("209.00")
            .currency("EUR")
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Distribution good A").build())
            .movementQuantity("10")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Distribution good B").build())
            .movementQuantity("15")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Distribution good C").build())
            .movementQuantity("20")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good A").build())
            .invoicedQuantity("10")
            .uOM("Bag")
            .unitPrice("3.00")
            .listPrice("3.00")
            .tax("VAT 10%")
            .lineNetAmount("30.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good B").build())
            .invoicedQuantity("15")
            .uOM("Bag")
            .unitPrice("4.00")
            .listPrice("4.00")
            .tax("VAT 10%")
            .lineNetAmount("60.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Distribution good C").build())
            .invoicedQuantity("20")
            .uOM("Bag")
            .unitPrice("5.00")
            .listPrice("5.00")
            .tax("VAT 10%")
            .lineNetAmount("100.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("209.00")
            .summedLineAmount("190.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test Create Lines From Sales Order Delivered in Two Shipments
   *
   * @throws ParseException
   */
  @Test
  public void CLF_SalesOrderDeliveredTwoGoodsShipmentTest() throws ParseException {
    logger.info(
        "** Start of test case [CLF_SalesOrderDeliveredTwoGoodsShipmentTest] Test Create Lines From Sales Order Delivered in Two Shipments **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);

    String orderNo = salesOrder.getData("documentNo").toString();
    bookedSalesOrderHeaderVerificationData.addDataField("documentNo", orderNo);

    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    salesOrderLines.create(salesOrderLinesData2);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData2);

    salesOrderLines.create(salesOrderLinesData3);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData3);

    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    String salesOrderLineIdentifier1 = String.format("%s - %s - %s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"), "10",
        salesOrderLinesVerificationData.getDataField("lineNetAmount"));

    String salesOrderLineIdentifier2 = String.format("%s - %s - %s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"), "20",
        salesOrderLinesVerificationData2.getDataField("lineNetAmount"));

    String salesOrderLineIdentifier3 = String.format("%s - %s - %s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"), "30",
        salesOrderLinesVerificationData3.getDataField("lineNetAmount"));

    // Deliver the order
    completedGoodsShipmentHeaderVerificationData.addDataField("salesOrder", salesOrderIdentifier);
    goodsShipmentLineVerificationData.addDataField("salesOrderLine", salesOrderLineIdentifier1);
    goodsShipmentLineVerificationData2.addDataField("salesOrderLine", salesOrderLineIdentifier2);

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("L01", salesOrderIdentifier, 1);
    goodsShipment.assertProcessCompletedSuccessfully();
    goodsShipment.createLinesFrom("L01", salesOrderIdentifier, 2);
    goodsShipment.assertProcessCompletedSuccessfully();

    String goodsShipmentNo = goodsShipment.getData("documentNo").toString();
    completedGoodsShipmentHeaderVerificationData.addDataField("documentNo", goodsShipmentNo);
    completedGoodsShipmentHeaderVerificationData.addDataField("salesOrder", salesOrderIdentifier);

    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(2);
    goodsShipmentLines.select(new GoodsShipmentLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Distribution good A").build())
        .movementQuantity("10")
        .build());
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);

    String goodsShipmentLineIdentifier = String.format("%s - %s - %s - %s - %s - %s",
        goodsShipmentNo, goodsShipment.getData("movementDate"), "Customer A",
        goodsShipmentLines.getData("lineNo"), "Distribution good A", "10");

    goodsShipmentLines.select(new GoodsShipmentLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Distribution good B").build())
        .movementQuantity("15")
        .build());
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData2);

    String goodsShipmentLineIdentifier2 = String.format("%s - %s - %s - %s - %s - %s",
        goodsShipmentNo, goodsShipment.getData("movementDate"), "Customer A",
        goodsShipmentLines.getData("lineNo"), "Distribution good B", "15");

    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("L01", salesOrderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();

    String goodsShipmentNo2 = goodsShipment.getData("documentNo").toString();
    completedGoodsShipmentHeaderVerificationData2.addDataField("documentNo", goodsShipmentNo2);
    completedGoodsShipmentHeaderVerificationData2.addDataField("salesOrder", salesOrderIdentifier);

    goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.clearFilters();
    goodsShipmentLines.assertCount(1);
    goodsShipmentLines.selectWithoutFiltering(0);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData3);

    String goodsShipmentLineIdentifier3 = String.format("%s - %s - %s - %s - %s - %s",
        goodsShipmentNo2, goodsShipment.getData("movementDate"), "Customer A",
        goodsShipmentLines.getData("lineNo"), "Distribution good C", "20");

    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    // Check delivered order Order
    bookedSalesOrderHeaderVerificationData.addDataField("deliveryStatus", "100 %");
    salesOrderLinesVerificationData.addDataField("deliveredQuantity", "10");
    salesOrderLinesVerificationData.addDataField("invoicedQuantity", "0");
    salesOrderLinesVerificationData2.addDataField("deliveredQuantity", "15");
    salesOrderLinesVerificationData2.addDataField("invoicedQuantity", "0");
    salesOrderLinesVerificationData3.addDataField("deliveredQuantity", "20");
    salesOrderLinesVerificationData3.addDataField("invoicedQuantity", "0");

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.selectWithoutFiltering(0);
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    salesOrderLines.selectWithoutFiltering(1);
    salesOrderLines.assertData(salesOrderLinesVerificationData2);

    salesOrderLines.selectWithoutFiltering(2);
    salesOrderLines.assertData(salesOrderLinesVerificationData3);

    // Invoice the order
    completedSalesInvoiceHeaderVerificationData.addDataField("salesOrder", salesOrderIdentifier);

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();

    salesInvoiceLineVerificationData.addDataField("salesOrderLine", salesOrderLineIdentifier1);
    salesInvoiceLineVerificationData.addDataField("goodsShipmentLine", goodsShipmentLineIdentifier);
    salesInvoiceLineVerificationData2.addDataField("salesOrderLine", salesOrderLineIdentifier2);
    salesInvoiceLineVerificationData2.addDataField("goodsShipmentLine",
        goodsShipmentLineIdentifier2);
    salesInvoiceLineVerificationData3.addDataField("salesOrderLine", salesOrderLineIdentifier3);
    salesInvoiceLineVerificationData3.addDataField("goodsShipmentLine",
        goodsShipmentLineIdentifier3);

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(3);

    salesInvoiceLine.select(new SalesInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Distribution good A").build())
        .invoicedQuantity("10")
        .build());
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    salesInvoiceLine.select(new SalesInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Distribution good B").build())
        .invoicedQuantity("15")
        .build());
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData2);

    salesInvoiceLine.select(new SalesInvoiceLinesData.Builder()
        .product(new ProductSimpleSelectorData.Builder().productName("Distribution good C").build())
        .invoicedQuantity("20")
        .build());
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData3);

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check invoiced and delivered order
    bookedSalesOrderHeaderVerificationData.addDataField("invoiceStatus", "100 %");
    salesOrderLinesVerificationData.addDataField("invoicedQuantity", "10");
    salesOrderLinesVerificationData2.addDataField("invoicedQuantity", "15");
    salesOrderLinesVerificationData3.addDataField("invoicedQuantity", "20");

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.selectWithoutFiltering(0);
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    salesOrderLines.selectWithoutFiltering(1);
    salesOrderLines.assertData(salesOrderLinesVerificationData2);

    salesOrderLines.selectWithoutFiltering(2);
    salesOrderLines.assertData(salesOrderLinesVerificationData3);

    // Check invoiced shipment
    completedGoodsShipmentHeaderVerificationData.addDataField("invoiceStatus", "100 %");
    completedGoodsShipmentHeaderVerificationData2.addDataField("invoiceStatus", "100 %");

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(goodsShipmentNo).build());
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(2);
    goodsShipmentLines.select(new GoodsShipmentLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Distribution good A").build())
        .movementQuantity("10")
        .build());
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);

    goodsShipmentLines.select(new GoodsShipmentLinesData.Builder()
        .product(new ProductCompleteSelectorData.Builder().name("Distribution good B").build())
        .movementQuantity("15")
        .build());
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData2);

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment
        .select(new GoodsShipmentHeaderData.Builder().documentNo(goodsShipmentNo2).build());
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.clearFilters();
    goodsShipmentLines.assertCount(1);
    goodsShipmentLines.selectWithoutFiltering(0);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData3);

    logger.info(
        "** End of test case [CLF_SalesOrderDeliveredTwoGoodsShipmentTest] Test Create Lines From Sales Order Delivered in Two Shipments **");
  }
}
