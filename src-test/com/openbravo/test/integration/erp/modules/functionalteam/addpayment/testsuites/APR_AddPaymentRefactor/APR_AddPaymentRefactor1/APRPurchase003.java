/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;

@RunWith(Parameterized.class)
public class APRPurchase003 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PaymentOutHeaderData paymentOutHeaderData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;

  public APRPurchase003(PaymentOutHeaderData paymentOutHeaderData,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {

    this.paymentOutHeaderData = paymentOutHeaderData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> PaymentOutDisplaysValues() {

    return Arrays
        .asList(
            new Object[][] { {
                new PaymentOutHeaderData.Builder()
                    .businessPartner(
                        new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
                    .paymentMethod("1 (Spain)")
                    .account("Spain Bank - EUR")
                    .build(),
                new AddPaymentPopUpData.Builder().amount_gl_items("2,000.00")
                    .amount_inv_ords("0.00")
                    .total("2,000.00")
                    .difference("0.00")
                    .build() } });

  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void executeAPRPurchase003() {
    logger.info("** Start of test case [APRPurchase003] **");

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.create(paymentOutHeaderData);
    paymentOut.assertSaved();

    AddPaymentProcess addPaymentProcess = paymentOut.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.addGLItem(
        new SelectedPaymentData.Builder().gLItem("Salaries").paidOut("2,000.00").build());
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Made Payment(s)");
    paymentOut.assertPaymentCreatedSuccessfully();

    logger.info("** End of test case [APRPurchase003] **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
