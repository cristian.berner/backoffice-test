/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

@RunWith(Parameterized.class)
public class APRPurchase006 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PaymentPlanData purchaseInvoicePaymentOutPlanData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;

  public APRPurchase006(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PaymentPlanData purchaseInvoicePaymentOutPlanData,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> PaymentOutDisplaysValues() {

    return Arrays.asList(new Object[][] { {
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("100")
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("220.00")
            .daysTillDue("90")
            .dueAmount("0.00")
            .summedLineAmount("200.00")
            .grandTotalAmount("220.00")
            .documentStatus("Completed")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("220.00")
            .paid("0.00")
            .outstanding("220.00")
            .lastPaymentDate("")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().transaction_type("Invoices")
            .amount_gl_items("0.00")
            .amount_inv_ords("110.00")
            .total("110.00")
            .difference("0.00")
            .build() } });
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void executeAPRPurchase006() {
    logger.info("** Start of test case [APRPurchase006] **");
    // Test APRPurchase006
    // Create Purchase Invoice
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();

    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);

    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();

    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(0);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);

    AddPaymentProcess addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();

    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep(2000);
    addPaymentProcess.getOrderInvoiceGrid().selectRowByContents("invoiceNo", invoiceNo);

    addPaymentProcess.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("110.00").build());
    Sleep.trySleep(3000);
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.process("Process Made Payment(s)");
    purchaseInvoice.assertPaymentCreatedSuccessfully();

    AddPaymentProcess addPaymentProcess2 = purchaseInvoice.openAddPayment();
    addPaymentProcess2.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess2.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess2.process("Process Made Payment(s)");
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    logger.info("** End of test case [APRPurchase006] **");
  }
}
