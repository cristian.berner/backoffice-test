/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2019 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-03-03 21:37:14
 * Contributor(s):
 *   Rafael Queralta <r.queralta@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorLineData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVd010 Create a Return to vendor document
 *
 * @author Rafael Queralta
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVd010CreateReversedInvoiceFromReturnToVendorShipment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  ReturnToVendorHeaderData returnToVendorHeaderData;
  ReturnToVendorHeaderData returnToVendorHeaderVerificationData;
  ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData;
  ReturnToVendorLineData returnToVendorLineVerificationData;

  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData;
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData;
  ReturnToVendorShipmentLineData returnToVendorShipmentLineData;
  ReturnToVendorShipmentHeaderData completeReturnToVendorShipmentVerificationData;

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineShipmentVerificationData;

  public AUMRTVd010CreateReversedInvoiceFromReturnToVendorShipment(
      GoodsReceiptHeaderData goodsReceiptHeader, GoodsReceiptLinesData goodsReceiptLine,
      ReturnToVendorHeaderData returnToVendorHeaderData,
      ReturnToVendorHeaderData returnToVendorHeaderVerificationData,
      ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData,
      ReturnToVendorLineData returnToVendorLineVerificationData,

      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData,
      ReturnToVendorShipmentHeaderData completeReturnToVendorShipmentVerificationData,
      ReturnToVendorShipmentLineData returnToVendorShipmentLineData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineShipmentVerificationData) {
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.returnToVendorHeaderData = returnToVendorHeaderData;
    this.returnToVendorHeaderVerificationData = returnToVendorHeaderVerificationData;
    this.bookedReturnToVendorHeaderVerificationData = bookedReturnToVendorHeaderVerificationData;
    this.returnToVendorLineVerificationData = returnToVendorLineVerificationData;

    this.returnToVendorShipmentHeaderData = returnToVendorShipmentHeaderData;
    this.returnToVendorShipmentHeaderVerificationData = returnToVendorShipmentHeaderVerificationData;
    this.completeReturnToVendorShipmentVerificationData = completeReturnToVendorShipmentVerificationData;
    this.returnToVendorShipmentLineData = returnToVendorShipmentLineData;

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineShipmentVerificationData = purchaseInvoiceLineShipmentVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("10")
            .storageBin("O02")
            .build(),
        new ReturnToVendorHeaderData.Builder().organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .orderReference("2334")
            .build(),
        new ReturnToVendorHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("RTV Order")
            .build(),
        new ReturnToVendorHeaderData.Builder().documentStatus("Booked").build(),
        new ReturnToVendorLineData.Builder().lineNo("10")
            .product("Basket Ball")
            .orderedQuantity("4")
            .uOM("Unit")
            .unitPrice("28.50")
            .lineNetAmount("114.00")
            .tax("VAT 10%")
            .returnReason("Defective")
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .orderReference("2334")
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .documentType("RTV Shipment")
            .documentStatus("Draft")
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().documentStatus("Completed").build(),
        new ReturnToVendorShipmentLineData.Builder().lineNo("10")
            .product("Basket Ball")
            .movementQuantity("4")
            .storageBin("O02")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("Reversed Purchase Invoice")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("Reversed Purchase Invoice")
            .grandTotalAmount("-11.00")
            .currency("EUR")
            .paymentComplete(false)
            .summedLineAmount("-10.00")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10")
            .product(new ProductSimpleSelectorData.Builder().productName("Basket Ball").build())
            .invoicedQuantity("-4")
            .uOM("Unit")
            .unitPrice("28.50")
            .lineNetAmount("-114.00")
            .tax("VAT 10%")
            .build() } };
    logger.debug("Building parameters");
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVd010test() throws ParseException {
    logger.debug(
        "** Start test case [AUMRTVd010] Create a Reversed Invoice from Return to Vendor Shipment **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    String receiptDocumentNo = (String) goodsReceipt.getData("documentNo");
    String receiptLineIdentifier = String.format("%s - %s - %s - %s", receiptDocumentNo,
        goodsReceipt.getData("movementDate").toString(),
        goodsReceipt.getData("businessPartner").toString(), "10 - Basket Ball - 20");

    final ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(returnToVendorHeaderData);
    returnToVendor.assertSaved();
    returnToVendor.assertData(returnToVendorHeaderVerificationData);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(new ReturnToVendorLineSelectorData.Builder().product("Basket Ball")
        .shipmentNumber(receiptDocumentNo)
        .build());
    popup.edit(new ReturnToVendorLineSelectorData.Builder().returned("2")
        .returnedUOM("Ounce")
        .returnReason("Defective")
        .build());
    popup.process();

    returnToVendorLineVerificationData.addDataField("goodsShipmentLine", receiptLineIdentifier);
    ReturnToVendor.Lines returnToVendorLines = returnToVendor.new Lines(mainPage);
    returnToVendorLines.assertData(returnToVendorLineVerificationData);

    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(bookedReturnToVendorHeaderVerificationData);

    String rtvDocumentNo = (String) returnToVendor.getData("documentNo");

    ReturnToVendorShipment returnToVendorShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendorShipment.create(returnToVendorShipmentHeaderData);
    returnToVendorShipment.assertSaved();
    returnToVendorShipment.assertData(returnToVendorShipmentHeaderVerificationData);

    /** Create a line **/
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = returnToVendorShipment
        .openPickAndEditLines();
    popUp.filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(rtvDocumentNo)
        .operativeQuantity("02")
        .uOM("Unit")
        .storageBin("O02")
        .lineNo("10")
        .build());
    popUp.process();

    ReturnToVendorShipment.Lines returnToVendorShipmentLines = returnToVendorShipment.new Lines(
        mainPage);
    returnToVendorShipmentLines.assertData(returnToVendorShipmentLineData);

    returnToVendorShipment.complete();
    returnToVendorShipment.assertProcessCompletedSuccessfully2();
    returnToVendorShipment.assertData(completeReturnToVendorShipmentVerificationData);

    String shipmentIdentifier = String.format("%s - %s - %s",
        returnToVendorShipment.getData("documentNo").toString(),
        returnToVendorShipment.getData("movementDate").toString(),
        returnToVendorShipment.getData("businessPartner").toString());

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.createLinesFromShipment(shipmentIdentifier);

    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertData(purchaseInvoiceLineShipmentVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();

    logger.debug(
        "** End test case [AUMRTVd010] Create a Reversed Invoice from Return to Vendor Shipment **");
  }

}
