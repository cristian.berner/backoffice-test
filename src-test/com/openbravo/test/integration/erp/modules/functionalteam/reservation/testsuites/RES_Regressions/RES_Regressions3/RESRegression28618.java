/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions3;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.ManageReservationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation.Stock;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 28618
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class RESRegression28618 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  ManageReservationData manageReservationData;
  ManageReservationData manageReservationData2;
  SalesOrderLinesData salesOrderLinesData2;
  ReservationData reservationVerificationData;
  StockData stockVerificationData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLineData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  ReservationData reservationVerificationData2;
  StockData stockVerificationData2;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;
  ReservationData reservationData;
  ReservationData reservationData2;
  ReservationData reservationVerificationData3;
  StockData stockVerificationData3;

  /**
   * Class constructor.
   *
   */
  public RESRegression28618(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      ManageReservationData manageReservationData, ManageReservationData manageReservationData2,
      SalesOrderLinesData salesOrderLinesData2, ReservationData reservationVerificationData,
      StockData stockVerificationData, GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLineData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      ReservationData reservationVerificationData2, StockData stockVerificationData2,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      ReservationData reservationData, ReservationData reservationData2,
      ReservationData reservationVerificationData3, StockData stockVerificationData3) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.manageReservationData = manageReservationData;
    this.manageReservationData2 = manageReservationData2;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.reservationVerificationData = reservationVerificationData;
    this.stockVerificationData = stockVerificationData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLineData = goodsShipmentLineData;
    this.goodsShipmentLineVerificationData = goodsShipmentLineVerificationData;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.reservationVerificationData2 = reservationVerificationData2;
    this.stockVerificationData2 = stockVerificationData2;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.reservationData = reservationData;
    this.reservationData2 = reservationData2;
    this.reservationVerificationData3 = reservationVerificationData3;
    this.stockVerificationData3 = stockVerificationData3;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGB")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("1").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("2.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("2.00")
            .grandTotalAmount("2.07")
            .currency("EUR")
            .build(),

        new ManageReservationData.Builder().quantity("9999999").build(),
        new ManageReservationData.Builder().quantity("2").build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("1").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("1")
            .reservedQuantity("2")
            .releasedQuantity("0")
            .uom("Bag")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("spain111")
            .attributeSetValue("L45")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("2")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new GoodsShipmentLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().searchKey("FGC")
                .storageBin("spain111")
                .build())
            .movementQuantity("2")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .attributeSetValue("L45")
            .movementQuantity("2")
            .uOM("Bag")
            .storageBin("spain111")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("1")
            .reservedQuantity("2")
            .releasedQuantity("2")
            .uom("Bag")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("spain111")
            .attributeSetValue("L45")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("2")
            .releasedQuantity("2")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build(),

        new ReservationData.Builder().status("Closed").build(),
        new ReservationData.Builder().status("Completed").build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("1")
            .reservedQuantity("1")
            .releasedQuantity("0")
            .uom("Bag")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("spain111")
            .attributeSetValue("L45")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("1")
            .allocated(false)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28618
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression28618Test() throws ParseException {
    logger.info("** Start of test case [RESRegression28618] Test regression 28618. **");

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    // TODO L2: Remove the following static sleep if not required once executions are stable
    Sleep.trySleep();
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    String orderLineIdentifier = String.format(" - %s - %s", salesOrderLines.getData("lineNo"),
        salesOrderLines.getData("lineNetAmount"));
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLineIdentifier = orderIdentifier + orderLineIdentifier;
    // TODO L00: Adjust the following static sleep ASAP if not required once executions are stable
    Sleep.trySleep(10000);
    salesOrderLines.manageReservationWithError(manageReservationData);
    // TODO L00: Adjust the following static sleep ASAP if not required once executions are stable
    Sleep.trySleep(10000);
    salesOrderLines.manageReservationWithError(manageReservationData2);
    // TODO L00: Adjust the following static sleep ASAP if not required once executions are stable
    Sleep.trySleep(10000);

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrder.reactivate();
    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.edit(salesOrderLinesData2);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    salesOrderLines.manageReservationWithError(manageReservationData);
    salesOrderLines.manageReservation(manageReservationData2);

    StockReservation stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    stockReservation.assertCount(1);
    stockReservation.assertData((ReservationData) reservationVerificationData
        .addDataField("salesOrderLine", orderLineIdentifier));
    Stock stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData);

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("spain111", orderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    String shipmentNo = goodsShipment.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(1);
    goodsShipmentLines.edit(goodsShipmentLineData);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData);
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    stockReservation.assertCount(1);
    stockReservation.assertData((ReservationData) reservationVerificationData2
        .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData2);

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(shipmentNo).build());
    goodsShipment.close();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    stockReservation.assertCount(2);
    stockReservation.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLineIdentifier));
    stockReservation.assertCount(1);
    stockReservation.assertData((ReservationData) reservationVerificationData2
        .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData2);
    stockReservation.select(
        (ReservationData) reservationData2.addDataField("salesOrderLine", orderLineIdentifier));
    stockReservation.assertCount(1);
    stockReservation.assertData((ReservationData) reservationVerificationData3
        .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    stock.assertCount(1);
    stock.assertData(stockVerificationData3);

    logger.info("** End of test case [RESRegression28618] Test regression 28618. **");
  }
}
