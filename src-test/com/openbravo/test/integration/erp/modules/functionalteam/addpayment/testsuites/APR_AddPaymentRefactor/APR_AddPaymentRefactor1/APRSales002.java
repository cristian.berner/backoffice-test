/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Lujan <plu@openbravo.com>,
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Executes the Check Displayed Fields in Add Payment popup from Sales Order suite.
 *
 * @author Unai Martirena
 */
@RunWith(Parameterized.class)
public class APRSales002 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  /** The sales order header data. */
  SalesOrderHeaderData salesOrderHeaderData;
  /** The sales order header verification data. */
  SalesOrderHeaderData salesOrderHeaderVerificationData;
  /** The sales order lines data. */
  SalesOrderLinesData salesOrderLinesData;
  /** The data to verify the creation of the sales order line. */
  SalesOrderLinesData salesOrderLinesVerificationData;
  /** The booked sales order header verification data. */
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  /** The data to verify the data in the add payment pop up. */
  AddPaymentPopUpData addPaymentInVerificationData;

  /** The data to verify the application of the payment. */
  SalesOrderHeaderData payedSalesOrderHeaderVerificationData;
  PaymentPlanData paymentPlanData;
  PaymentDetailsData paymentDetailsData;

  /**
   * Class constructor.
   *
   * @param salesOrderHeaderData
   *          The sales order header data.
   * @param salesOrderHeaderVerificationData
   *          The data to verify the creation of the sales order header.
   * @param salesOrderLinesData
   *          The sales order lines data.
   * @param salesOrderLinesVerificationData
   *          The data to verify the creation of the sales order line.
   * @param bookedSalesOrderHeaderVerificationData
   *          The booked sales order header verification data.
   * @param addPaymentInVerificationData
   *          The data to verify the data in the add payment pop up.
   */
  public APRSales002(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerificationData,
      SalesOrderLinesData salesOrderLinesData, SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      AddPaymentPopUpData addPaymentInVerificationData,
      SalesOrderHeaderData payedSalesOrderHeaderVerificationData) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerificationData = salesOrderHeaderVerificationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.addPaymentInVerificationData = addPaymentInVerificationData;
    this.payedSalesOrderHeaderVerificationData = payedSalesOrderHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToOrderValues() {

    return Arrays.asList(new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .invoiceTerms("Customer Schedule After Delivery")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("10").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("20.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.70")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("20.70")
            .total("20.70")
            .difference("0.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("20.70")
            .currency("EUR")
            .build() } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void APRSales002Test() {
    logger.info(
        "** Start of test case [APRSales002] Adding a Payment to a sales order (pre-payment). **");
    final SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerificationData);
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    AddPaymentProcess addPaymentProcess = salesOrder.openAddPayment();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.assertTotalsData(addPaymentInVerificationData);
    addPaymentProcess.process("Process Received Payment(s)");
    salesOrder.assertData(payedSalesOrderHeaderVerificationData);
    logger.info(
        "** End of test case [APRSales002] Adding a Payment to a sales order (pre-payment). **");
  }
}
