/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 28591
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression28591In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData2;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData2;
  SalesInvoiceLinesData salesInvoiceLineData2;
  SalesInvoiceLinesData salesInvoiceLineVerificationData2;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2;
  PaymentInHeaderData paymentInHeaderData;
  PaymentInHeaderData paymentInHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PaymentInHeaderData paymentInHeaderVerificationData2;
  PaymentInLinesData paymentInLinesVerificationData;
  PaymentInLinesData paymentInLinesVerificationData2;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData3;
  PaymentPlanData invoicePaymentInPlanData;
  PaymentDetailsData invoicePaymentInDetailsData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData4;
  PaymentPlanData invoicePaymentInPlanData2;
  PaymentDetailsData invoicePaymentInDetailsData2;
  PaymentInHeaderData paymentInHeaderVerificationData3;
  PaymentInLinesData paymentInLinesVerificationData3;
  PaymentInLinesData paymentInLinesVerificationData4;
  PaymentPlanData invoicePaymentInPlanData3;
  PaymentDetailsData invoicePaymentInDetailsData3;
  PaymentDetailsData invoicePaymentInDetailsData4;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentPlanData invoicePaymentInPlanData4;
  PaymentDetailsData invoicePaymentInDetailsData5;
  PaymentPlanData invoicePaymentInPlanData5;
  PaymentDetailsData invoicePaymentInDetailsData6;
  PaymentDetailsData invoicePaymentInDetailsData7;
  AddPaymentPopUpData addPaymentVerificationData3;
  PaymentPlanData invoicePaymentInPlanData6;
  PaymentDetailsData invoicePaymentInDetailsData8;

  /**
   * Class constructor.
   *
   */
  public APRRegression28591In(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData2,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData2,
      SalesInvoiceLinesData salesInvoiceLineData2,
      SalesInvoiceLinesData salesInvoiceLineVerificationData2,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData2,
      PaymentInHeaderData paymentInHeaderData, PaymentInHeaderData paymentInHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData2,
      PaymentInLinesData paymentInLinesVerificationData,
      PaymentInLinesData paymentInLinesVerificationData2,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData3,
      PaymentPlanData invoicePaymentInPlanData, PaymentDetailsData invoicePaymentInDetailsData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData4,
      PaymentPlanData invoicePaymentInPlanData2, PaymentDetailsData invoicePaymentInDetailsData2,
      PaymentInHeaderData paymentInHeaderVerificationData3,
      PaymentInLinesData paymentInLinesVerificationData3,
      PaymentInLinesData paymentInLinesVerificationData4, PaymentPlanData invoicePaymentInPlanData3,
      PaymentDetailsData invoicePaymentInDetailsData3,
      PaymentDetailsData invoicePaymentInDetailsData4,
      AddPaymentPopUpData addPaymentVerificationData2, PaymentPlanData invoicePaymentInPlanData4,
      PaymentDetailsData invoicePaymentInDetailsData5, PaymentPlanData invoicePaymentInPlanData5,
      PaymentDetailsData invoicePaymentInDetailsData6,
      PaymentDetailsData invoicePaymentInDetailsData7,
      AddPaymentPopUpData addPaymentVerificationData3, PaymentPlanData invoicePaymentInPlanData6,
      PaymentDetailsData invoicePaymentInDetailsData8) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceHeaderData2 = salesInvoiceHeaderData2;
    this.salesInvoiceHeaderVerificationData2 = salesInvoiceHeaderVerificationData2;
    this.salesInvoiceLineData2 = salesInvoiceLineData2;
    this.salesInvoiceLineVerificationData2 = salesInvoiceLineVerificationData2;
    this.completedSalesInvoiceHeaderVerificationData2 = completedSalesInvoiceHeaderVerificationData2;
    this.paymentInHeaderData = paymentInHeaderData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.paymentInHeaderVerificationData2 = paymentInHeaderVerificationData2;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.paymentInLinesVerificationData2 = paymentInLinesVerificationData2;
    this.completedSalesInvoiceHeaderVerificationData3 = completedSalesInvoiceHeaderVerificationData3;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.completedSalesInvoiceHeaderVerificationData4 = completedSalesInvoiceHeaderVerificationData4;
    this.invoicePaymentInPlanData2 = invoicePaymentInPlanData2;
    this.invoicePaymentInDetailsData2 = invoicePaymentInDetailsData2;
    this.paymentInHeaderVerificationData3 = paymentInHeaderVerificationData3;
    this.paymentInLinesVerificationData3 = paymentInLinesVerificationData3;
    this.paymentInLinesVerificationData4 = paymentInLinesVerificationData4;
    this.invoicePaymentInPlanData3 = invoicePaymentInPlanData3;
    this.invoicePaymentInDetailsData3 = invoicePaymentInDetailsData3;
    this.invoicePaymentInDetailsData4 = invoicePaymentInDetailsData4;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.invoicePaymentInPlanData4 = invoicePaymentInPlanData4;
    this.invoicePaymentInDetailsData5 = invoicePaymentInDetailsData5;
    this.invoicePaymentInPlanData5 = invoicePaymentInPlanData5;
    this.invoicePaymentInDetailsData6 = invoicePaymentInDetailsData6;
    this.invoicePaymentInDetailsData7 = invoicePaymentInDetailsData7;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.invoicePaymentInPlanData6 = invoicePaymentInPlanData6;
    this.invoicePaymentInDetailsData8 = invoicePaymentInDetailsData8;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("10")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("20.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("20.70")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .totalPaid("0.00")
            .outstandingAmount("20.70")
            .dueAmount("0.00")
            .build(),

        new SalesInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good B")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("20")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("40.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("41.40")
            .summedLineAmount("40.00")
            .currency("EUR")
            .paymentComplete(false)
            .totalPaid("0.00")
            .outstandingAmount("41.40")
            .dueAmount("0.00")
            .build(),

        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("62.10")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .paymentMethod("1 (Spain)")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("62.10")
            .expected_payment("62.10")
            .amount_gl_items("0.00")
            .amount_inv_ords("62.10")
            .total("62.10")
            .difference("0.00")
            .build(),
        new PaymentInHeaderData.Builder().status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("20.70")
            .expected("20.70")
            .received("20.70")
            .orderno("")
            .glitemname("")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("41.40")
            .expected("41.40")
            .received("41.40")
            .orderno("")
            .glitemname("")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("20.70")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("20.70")
            .received("20.70")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().received("20.70")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("20.70")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("41.40")
            .summedLineAmount("40.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("41.40")
            .received("41.40")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),
        new PaymentDetailsData.Builder().received("41.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("41.40")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().status("Payment Received")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .amount("-62.10")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("20.70")
            .expected("20.70")
            .received("-20.70")
            .orderno("")
            .glitemname("")
            .build(),
        new PaymentInLinesData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .invoiceAmount("41.40")
            .expected("41.40")
            .received("-41.40")
            .orderno("")
            .glitemname("")
            .build(),

        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("20.70")
            .received("0.00")
            .outstanding("20.70")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("2")
            .build(),
        new PaymentDetailsData.Builder().received("20.70")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("20.70")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),
        new PaymentDetailsData.Builder().received("-20.70")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("20.70")
            .account("Spain Bank - EUR")
            .status("Payment Received")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("20.70")
            .expected_payment("20.70")
            .amount_gl_items("0.00")
            .amount_inv_ords("20.70")
            .total("20.70")
            .difference("0.00")
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("20.70")
            .received("20.70")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("3")
            .build(),
        new PaymentDetailsData.Builder().received("20.70")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("20.70")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),

        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("41.40")
            .received("0.00")
            .outstanding("41.40")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("2")
            .build(),
        new PaymentDetailsData.Builder().received("41.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("41.40")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build(),
        new PaymentDetailsData.Builder().received("-41.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("41.40")
            .account("Spain Bank - EUR")
            .status("Payment Received")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Customer A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Bank - EUR")
            .c_currency_id("EUR")
            .actual_payment("41.40")
            .expected_payment("41.40")
            .amount_gl_items("0.00")
            .amount_inv_ords("41.40")
            .total("41.40")
            .difference("0.00")
            .build(),
        new PaymentPlanData.Builder().expectedDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("41.40")
            .received("41.40")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("3")
            .build(),
        new PaymentDetailsData.Builder().received("41.40")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("1 (Spain)")
            .amount("41.40")
            .account("Spain Bank - EUR")
            .status("Deposited not Cleared")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28591 - Payment In flow
   *
   * Payment from Order
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression28591InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression28591In] Test regression 28591 - Payment In flow. **");

    SalesInvoice salesInvoice1 = new SalesInvoice(mainPage).open();
    salesInvoice1.create(salesInvoiceHeaderData);
    salesInvoice1.assertSaved();
    salesInvoice1.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo1 = salesInvoice1.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines1 = salesInvoice1.new Lines(mainPage);
    salesInvoiceLines1.create(salesInvoiceLineData);
    salesInvoiceLines1.assertSaved();
    salesInvoiceLines1.assertData(salesInvoiceLineVerificationData);
    salesInvoice1.complete();
    salesInvoice1.assertProcessCompletedSuccessfully2();
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData);

    SalesInvoice salesInvoice2 = new SalesInvoice(mainPage).open();
    salesInvoice2.create(salesInvoiceHeaderData2);
    salesInvoice2.assertSaved();
    salesInvoice2.assertData(salesInvoiceHeaderVerificationData2);
    String invoiceNo2 = salesInvoice2.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLines2 = salesInvoice2.new Lines(mainPage);
    salesInvoiceLines2.create(salesInvoiceLineData2);
    salesInvoiceLines2.assertSaved();
    salesInvoiceLines2.assertData(salesInvoiceLineVerificationData2);
    salesInvoice2.complete();
    salesInvoice2.assertProcessCompletedSuccessfully2();
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData2);

    PaymentIn paymentIn1 = new PaymentIn(mainPage).open();
    paymentIn1.create(paymentInHeaderData);
    paymentIn1.assertSaved();
    paymentIn1.assertData(paymentInHeaderVerificationData);
    String invoiceNo = invoiceNo1 + " or " + invoiceNo2;
    AddPaymentProcess addPaymentProcess = paymentIn1.addDetailsOpen();
    String payment1No = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceNo);
    Sleep.trySleep(1000);
    addPaymentProcess.getField("reference_no").focus();
    addPaymentProcess.process("Process Received Payment(s) and Deposit",
        addPaymentVerificationData);
    paymentIn1.assertPaymentCreatedSuccessfully();
    paymentIn1.assertData(paymentInHeaderVerificationData2);
    PaymentIn.Lines paymentInLines1 = paymentIn1.new Lines(mainPage);
    paymentInLines1.assertCount(2);
    paymentInLines1.select(new PaymentInLinesData.Builder().invoiceno(invoiceNo1).build());
    paymentInLines1.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo1));
    paymentInLines1.select(new PaymentInLinesData.Builder().invoiceno(invoiceNo2).build());
    paymentInLines1.assertData((PaymentInLinesData) paymentInLinesVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo2));

    salesInvoice1.open();
    Sleep.trySleep(20000);
    salesInvoice1.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo1).build());
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData3);
    SalesInvoice.PaymentInPlan invoicePaymentInPlan1 = salesInvoice1.new PaymentInPlan(mainPage);
    invoicePaymentInPlan1.assertCount(1);
    invoicePaymentInPlan1.assertData(invoicePaymentInPlanData);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails1 = invoicePaymentInPlan1.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails1.assertCount(1);
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData);

    salesInvoice2.open();
    Sleep.trySleep(20000);
    salesInvoice2.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo2).build());
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData4);
    SalesInvoice.PaymentInPlan invoicePaymentInPlan2 = salesInvoice1.new PaymentInPlan(mainPage);
    invoicePaymentInPlan2.assertCount(1);
    invoicePaymentInPlan2.assertData(invoicePaymentInPlanData2);
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails2 = invoicePaymentInPlan2.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails2.assertCount(1);
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData2);

    paymentIn1.open();
    Sleep.trySleep(20000);
    paymentIn1.select(new PaymentInHeaderData.Builder().documentNo(payment1No).build());
    paymentIn1.reverse("Reverse Payment", OBDate.getSystemDate());
    paymentIn1.assertProcessCompletedSuccessfully2();

    String payment2No = "*R*" + String.valueOf(Integer.valueOf(payment1No) + 1);
    PaymentIn paymentIn2 = new PaymentIn(mainPage).open();
    paymentIn2.getTab().refresh();
    paymentIn2.select(new PaymentInHeaderData.Builder().documentNo(payment2No).build());
    paymentIn2.assertSaved();
    paymentIn2.assertData(paymentInHeaderVerificationData3);
    PaymentIn.Lines paymentInLines2 = paymentIn1.new Lines(mainPage);
    paymentInLines2.assertCount(2);
    paymentInLines2.select(new PaymentInLinesData.Builder().invoiceno(invoiceNo1).build());
    paymentInLines2.assertData((PaymentInLinesData) paymentInLinesVerificationData3
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo1));
    paymentInLines2.select(new PaymentInLinesData.Builder().invoiceno(invoiceNo2).build());
    paymentInLines2.assertData((PaymentInLinesData) paymentInLinesVerificationData4
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo2));

    salesInvoice1.open();
    Sleep.trySleep(20000);
    salesInvoice1.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo1).build());
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData);
    invoicePaymentInPlan1 = salesInvoice1.new PaymentInPlan(mainPage);
    invoicePaymentInPlan1.assertCount(1);
    invoicePaymentInPlan1.assertData(invoicePaymentInPlanData3);
    invoicePaymentInDetails1 = invoicePaymentInPlan1.new PaymentInDetails(mainPage);
    invoicePaymentInDetails1.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentInDetails1.assertCount(2);
    invoicePaymentInDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData3);
    invoicePaymentInDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData4);

    AddPaymentProcess addPaymentProcess1 = salesInvoice1.openAddPayment();
    String payment3No = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    payment3No = payment3No.substring(1, payment3No.length() - 1);
    addPaymentProcess1.assertData(addPaymentVerificationData2);
    addPaymentProcess1.process("Process Received Payment(s) and Deposit");
    salesInvoice1.assertPaymentCreatedSuccessfully();
    salesInvoice1.assertData(completedSalesInvoiceHeaderVerificationData3);
    invoicePaymentInPlan1 = salesInvoice1.new PaymentInPlan(mainPage);
    invoicePaymentInPlan1.assertCount(1);
    invoicePaymentInPlan1.assertData(invoicePaymentInPlanData4);
    invoicePaymentInDetails1 = invoicePaymentInPlan1.new PaymentInDetails(mainPage);
    invoicePaymentInDetails1.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentInDetails1.assertCount(3);
    invoicePaymentInDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData3);
    invoicePaymentInDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData4);
    invoicePaymentInDetails1
        .select(new PaymentDetailsData.Builder().documentNo(payment3No).build());
    invoicePaymentInDetails1.assertData(invoicePaymentInDetailsData5);

    salesInvoice2.open();
    Sleep.trySleep(20000);
    salesInvoice2.select(new SalesInvoiceHeaderData.Builder().documentNo(invoiceNo2).build());
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData2);
    invoicePaymentInPlan2 = salesInvoice2.new PaymentInPlan(mainPage);
    invoicePaymentInPlan2.assertCount(1);
    invoicePaymentInPlan2.assertData(invoicePaymentInPlanData5);
    invoicePaymentInDetails2 = invoicePaymentInPlan2.new PaymentInDetails(mainPage);
    invoicePaymentInDetails2.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentInDetails2.assertCount(2);
    invoicePaymentInDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData6);
    invoicePaymentInDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData7);

    AddPaymentProcess addPaymentProcess2 = salesInvoice2.openAddPayment();
    String payment4No = addPaymentProcess2.getParameterValue("payment_documentno").toString();
    payment4No = payment4No.substring(1, payment4No.length() - 1);
    addPaymentProcess2.assertData(addPaymentVerificationData3);
    addPaymentProcess2.process("Process Received Payment(s) and Deposit");
    salesInvoice2.assertPaymentCreatedSuccessfully();
    salesInvoice2.assertData(completedSalesInvoiceHeaderVerificationData4);
    invoicePaymentInPlan2 = salesInvoice2.new PaymentInPlan(mainPage);
    invoicePaymentInPlan2.assertCount(1);
    invoicePaymentInPlan2.assertData(invoicePaymentInPlanData6);
    invoicePaymentInDetails2 = invoicePaymentInPlan2.new PaymentInDetails(mainPage);
    invoicePaymentInDetails2.filter(new PaymentDetailsData.Builder().documentNo("").build());
    Sleep.trySleep(1000);
    invoicePaymentInDetails2.assertCount(3);
    invoicePaymentInDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment1No).build());
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData6);
    invoicePaymentInDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment2No).build());
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData7);
    invoicePaymentInDetails2
        .select(new PaymentDetailsData.Builder().documentNo(payment4No).build());
    invoicePaymentInDetails2.assertData(invoicePaymentInDetailsData8);

    logger.info(
        "** End of test case [APRRegression28591In] Test regression 28591 - Payment In flow. **");
  }
}
