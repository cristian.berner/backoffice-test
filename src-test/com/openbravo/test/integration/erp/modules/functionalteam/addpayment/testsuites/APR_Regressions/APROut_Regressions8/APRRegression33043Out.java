/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions8;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutWindow;
import com.openbravo.test.integration.erp.gui.general.security.role.RoleWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test Regression 33043
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class APRRegression33043Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  RoleData financeRole;
  OrgAccessData orgAccessData;
  PaymentOutHeaderData paymentOutHeader;

  public APRRegression33043Out(RoleData financeRole, OrgAccessData orgAccessData,
      PaymentOutHeaderData paymentOutHeader) {
    super();
    this.financeRole = financeRole;
    this.orgAccessData = orgAccessData;
    this.paymentOutHeader = paymentOutHeader;

    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new RoleData.Builder().name("Espa Finance").build(),

        new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("Sur or S.A").build())
            .orgAdmin(false)
            .build(),

        new PaymentOutHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("La Fruta es la Vida, S.L.").build())
            .build()

        } };
    return Arrays.asList(data);
  }

  @Test
  public void Test33043() {
    logger.info("** Start of test case [PRO33043] Permissions problem after process payment **");

    RoleWindow roleWindow = new RoleWindow();
    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectOrgAccessTab().filter(orgAccessData);
    Sleep.trySleep();
    int orgCount = roleWindow.selectOrgAccessTab().getRecordCount();
    if (orgCount > 0) {
      roleWindow.selectOrgAccessTab().deleteMultipleRecordsOneCheckboxOnGrid();
    }
    roleWindow.selectOrgAccessTab().clearFilters();
    Sleep.trySleep();
    roleWindow.selectOrgAccessTab().assertCount(1);
    assertTrue(roleWindow.selectOrgAccessTab()
        .getData()
        .getDataField("organization")
        .equals("F&B España - Región Norte"));

    mainPage.closeAllViews();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    PaymentOut paymentOut = new PaymentOut(mainPage).open();

    PaymentOutTab tab = (PaymentOutTab) paymentOut.getTab();
    tab.waitUntilSelected();
    List<Map<String, Object>> paymentRows = tab.getRows();
    for (Map<String, Object> row : paymentRows) {
      assertTrue(row.get("organization$_identifier").equals("F&B España - Región Norte")
          || row.get("organization$_identifier").equals("F&B España, S.A"));
    }
    int before = tab.getRecordCount();
    paymentOut.create(paymentOutHeader);
    paymentOut.assertSaved();

    AddPaymentProcess addDetails = paymentOut.addDetailsOpen();
    addDetails.getOrderInvoiceGrid().selectRecord(0);
    addDetails.process("Process Made Payment(s) and Withdrawal");

    mainPage.openView(mainPage.getView(PaymentOutWindow.TITLE));
    tab = (PaymentOutTab) paymentOut.getTab();

    paymentRows = tab.getRows();
    for (Map<String, Object> row : paymentRows) {
      assertTrue(row.get("organization$_identifier").equals("F&B España - Región Norte")
          || row.get("organization$_identifier").equals("F&B España, S.A"));
    }
    int after = tab.getRecordCount();

    assertTrue(after == (before + 1));

    mainPage.closeAllViews();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B International Group Admin - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectOrgAccessTab()
        .createRecord(new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("Sur").build())
            .build());
    roleWindow.selectOrgAccessTab()
        .createRecord(new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("S.A").build())
            .build());

    logger.info("** End of test case [PRO33043] Permissions problem after process payment **");
  }
}
