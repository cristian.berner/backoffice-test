/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.FindTransactionsToMatchProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Manually match 2 Transactions (100 + 200) and a Bank Statement Line and Bank Statement Line
 * (299). Pending amount (-1) is created
 */
@RunWith(Parameterized.class)
public class MatchSeveralTransaction003 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The account header data. */
  AccountData accountHeaderData;
  /** The transaction data. */
  TransactionsData transactionData1;
  TransactionsData transactionData2;
  /** The bank statement data. */
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  MatchStatementData matchStatementData2;
  MatchStatementData matchStatementData3;
  MatchStatementData matchStatementData4;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param bankStatementHeaderData
   *          The bank statement Header Data.
   * @param bankStatementLinesData
   *          The bank statement lines Data.
   */
  public MatchSeveralTransaction003(AccountData accountHeaderData,
      TransactionsData transactionData1, TransactionsData transactionData2,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      MatchStatementData matchStatementData2, MatchStatementData matchStatementData3,
      MatchStatementData matchStatementData4) {
    this.accountHeaderData = accountHeaderData;
    this.transactionData1 = transactionData1;
    this.transactionData2 = transactionData2;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.matchStatementData2 = matchStatementData2;
    this.matchStatementData3 = matchStatementData3;
    this.matchStatementData4 = matchStatementData4;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> matchSeveralTransaction003Values() {
    Object[][] data = new Object[][] { {
        /* Parameters for [MatchSeveralTransaction003Test] */
        new AccountData.Builder().name("Spain Bank").build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .gLItem("Salaries")
            .depositAmount("100.00")
            .build(),
        new TransactionsData.Builder().transactionType("BP Deposit")
            .gLItem("Salaries")
            .depositAmount("200.00")
            .build(),
        new BankStatementHeaderData.Builder().name("TestMatchStatementMSTBSL003").build(),
        new BankStatementLinesData.Builder().referenceNo("MSTBSL003")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .cramount("299.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("MSTBSL003")
            .glitem("")
            .amount("299")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("MSTBSL003")
            .glitem("")
            .amount("100")
            .affinity("MA")
            .matchedDocument("T")
            .matchingType("MA")
            .businessPartner("")
            .transactionAmount("100")
            .transactionGLItem("Salaries")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("MSTBSL003")
            .glitem("")
            .amount("200")
            .affinity("MA")
            .matchedDocument("T")
            .matchingType("MA")
            .businessPartner("")
            .transactionAmount("200")
            .transactionGLItem("Salaries")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Customer A")
            .referenceNo("MSTBSL003")
            .glitem("")
            .amount("-1")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void MatchSeveralTransaction003Test() throws ParseException {
    logger.info("** Start of test case [MatchSeveralTransaction003Test]");

    // Select Financial Account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    // Get the date of the latest Bank Statement
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }

    // Create first Transaction
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactionData1.addDataField("transactionDate", date);
    transactionData1.addDataField("dateAcct", date);
    transactions.create(transactionData1);
    transactions.assertSaved();
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();

    // Create second Transaction
    transactions = financialAccount.new Transaction(mainPage);
    transactionData2.addDataField("transactionDate", date);
    transactionData2.addDataField("dateAcct", date);
    transactions.create(transactionData2);
    transactions.assertSaved();
    transactions.process();
    transactions.assertProcessCompletedSuccessfully2();

    // Create Bank Statement
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    Sleep.trySleep(1000);
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));

    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");

    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    // Select Transaction
    FindTransactionsToMatchProcess findTransactionsToMatchProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickSearch(0);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().clearFilters();
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().filter("transactionDate", date);
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().filter("businessPartner", "");
    findTransactionsToMatchProcess.getTransactionsToMatchGrid().selectAll();
    findTransactionsToMatchProcess.process();

    // Check first matched transaction with bank statement line
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    Sleep.trySleep(1000);
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("amount", "100");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));

    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", date);

    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    Sleep.trySleep();
    // Check second matched transaction with bank statement line
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("amount", "200");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));

    matchStatementData3.addDataField("banklineDate", date);
    matchStatementData3.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData3);

    // Check Pending Amount Bank Statement Line
    matchStatementProcess.getMatchStatementGrid().filter("amount", "-1");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));

    matchStatementData4.addDataField("banklineDate", date);

    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData4);

    // Process Match Statement
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    logger.info("** End of test case [MatchSeveralTransaction003Test]");
  }

}
