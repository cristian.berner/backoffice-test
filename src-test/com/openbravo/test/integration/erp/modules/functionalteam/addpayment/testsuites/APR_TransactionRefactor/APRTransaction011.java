/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_TransactionRefactor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.util.OBDate;

/**
 * Create a Transaction with an existing payment.
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRTransaction011 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // /** The account header data. */

  // /** The transaction header data. */

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceLinesData purchaseInvoiceLinesData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  AddPaymentPopUpData paymentOutPlanData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderVerificationData;
  AccountData accountHeaderData;
  TransactionsData transactionLinesData;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param transactionLinesData
   *          The transaction Header Data.
   */

  public APRTransaction011(PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceLinesData purchaseInvoiceLinesData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      AddPaymentPopUpData paymentOutPlanData, AddPaymentPopUpData addPaymentTotalsVerificationData,
      PurchaseInvoiceHeaderData payedPurchaseInvoiceHeaderVerificationData,
      AccountData accountHeaderData, TransactionsData transactionLinesData) {

    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceLinesData = purchaseInvoiceLinesData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.paymentOutPlanData = paymentOutPlanData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.payedPurchaseInvoiceHeaderVerificationData = payedPurchaseInvoiceHeaderVerificationData;
    this.accountHeaderData = accountHeaderData;
    this.transactionLinesData = transactionLinesData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor B").build())
            .paymentTerms("30 days, 5")
            .paymentMethod("1 (Spain)")
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("10")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº12")
            .priceList("Purchase")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .build(),

        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("22.00")
            .documentStatus("Completed")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AddPaymentPopUpData.Builder().fin_paymentmethod_id("1 (Spain)").build(),

        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("22.00")
            .total("22.00")
            .difference("0.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().paymentComplete(true)
            .documentStatus("Completed")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .build(),

        new AccountData.Builder().name("Spain Bank").build(),

        new TransactionsData.Builder().transactionType("BP Withdrawal").build() } };

    return Arrays.asList(data);
  }

  /**
   * Test to create a purchase order, and a goods receipt and purchase invoice from that order.
   *
   * @throws ParseException
   */
  @Test
  public void APRTransaction011Test() throws ParseException {
    logger.info(
        "** Start of test case [APRTransaction011] Create a withdrawal transaction creating a payment in the selector without modifying the document. **");

    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();

    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLinesData);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);

    PaymentSelectorData addPaymentData = new PaymentSelectorData.Builder().amount("22.00")
        .addPayment(addPaymentTotalsVerificationData)
        .addEditPayment(paymentOutPlanData)
        .build();
    addPaymentData.addDataField("invoiceNo", invoiceNo);
    transactionLinesData.addDataField("finPayment", addPaymentData);
    addPaymentData.addDataField("currentTab", financialAccount.getTab());
    addPaymentData.addDataField("transactions", transactions);

    transactions.create(transactionLinesData);
    transactions.assertSaved();
    transactions.process();

    purchaseInvoice.open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    purchaseInvoice.assertData(payedPurchaseInvoiceHeaderVerificationData);

    logger.info(
        "** End of test case [APRTransaction011] Create a withdrawal transaction creating a payment in the selector without modifying the document. **");
  }
}
