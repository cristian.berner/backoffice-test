/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * Álvaro Ferraz <alvaro.ferrazo@openbravo.com>,
 * Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ PRORegression29171.class, PRORegression28426.class, PRORegression28226.class,
    PRORegression29963.class, PRORegression31873Out1.class, PRORegression31873Out2.class,
    PRORegression31873Out3.class, PRORegression31873Out4.class, PRORegression32323.class,
    PRORegression33382.class, PRORegression33040.class, PRORegression32428.class,
    PRORegression34392.class, PRORegression33550.class, PRORegression39448.class })
public class PRO_RegressionSuite {
  // No content is required, this is just the definition of a test suite.
  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
