/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Test regression 29171
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class PRORegression29171 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public PRORegression29171(ProductData productData, PriceData productPriceData,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(true)
            .sale(false)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Purchase")
            .standardPrice("0.00")
            .listPrice("5.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .build(),
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseOrderLinesData.Builder().orderedQuantity("1").build(),
        new PurchaseOrderLinesData.Builder().uOM("Unit")
            .unitPrice("0.00")
            .listPrice("5.00")
            .tax("VAT 10%")
            .lineNetAmount("0.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("0.00")
            .grandTotalAmount("0.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 29171
   *
   * @throws ParseException
   */
  @Test
  public void PRORegression29171Test() throws ParseException {
    logger.info("** Start of test case [PRORegression29171] Test regression 29171. **");

    String productName = "Test29171Purchase-";
    int num = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num);

    productData.addDataField("searchKey", productName);
    productData.addDataField("name", productName);
    Product.ProductTab.create(mainPage, productData);
    Product.Price.create(mainPage, productPriceData);

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLinesData.addDataField("product",
        new ProductSelectorData.Builder().searchKey(productName)
            .priceListVersion("Purchase")
            .build());
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    logger.info("** End of test case [PRORegression29171] Test regression 29171. **");
  }
}
