/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions5;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test Issue 37209 Create a credit payment, Create a Sales invoice, Create payment for Sales
 * Invoice with fully paid sales invoice, select the credit payment
 *
 */
@RunWith(Parameterized.class)
public class APRRegression37209In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  private static final String PARAM_OVERPYMT_ACTION = "overpayment_action";
  /* Data for this test. */
  /** The payment in header data. */
  PaymentInHeaderData paymentInHeaderData;
  /** The data to verify the totals data in the add payment pop up. */
  AddPaymentPopUpData addPaymentTotalsVerificationData;
  PaymentInHeaderData paymentInHeaderDatacredit;
  AddPaymentPopUpData addPaymentTotalsVerificationDatacredit;

  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData;
  /** The data to verify the creation of the Sales Invoice line. */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  /** The data to verify the payment in plan. */
  PaymentPlanData paymentInPlanData;
  PaymentInHeaderData paymentInHeaderVerificationData;

  /**
   * Class constructor.
   */
  public APRRegression37209In(PaymentInHeaderData paymentInHeaderDatacredit,
      AddPaymentPopUpData addPaymentTotalsVerificationDatacredit,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData, PaymentPlanData paymentInPlanData,
      PaymentInHeaderData paymentInHeaderData, AddPaymentPopUpData addPaymentTotalsVerificationData,
      PaymentInHeaderData paymentInHeaderVerificationData) {
    this.paymentInHeaderDatacredit = paymentInHeaderDatacredit;
    this.addPaymentTotalsVerificationDatacredit = addPaymentTotalsVerificationDatacredit;
    this.paymentInHeaderData = paymentInHeaderData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.paymentInPlanData = paymentInPlanData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToOrderValues() {

    return Arrays.asList(new Object[][] { {
        // Credit Payment
        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("100.00")
            .paymentMethod("1 (Spain)")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("100.00")
            .build(),
        // Sales Invoice
        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("207.00")
            .documentStatus("Completed")
            .summedLineAmount("200.00")
            .grandTotalAmount("207.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("100")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT(3)+CHARGE(0.5)")
            .lineNetAmount("200.00")
            .build(),
        new PaymentPlanData.Builder().dueDate(OBDate.GET_30_DAYS_5_FROM_SYSTEM_DATE)
            .paymentMethod("1 (Spain)")
            .expected("207.00")
            .received("0.00")
            .outstanding("207.00")
            .build(),
        // Payment In
        new PaymentInHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .amount("207.00")
            .paymentMethod("1 (Spain)")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("207.00")
            .total("207.00")
            .difference("100.00")
            .build(),
        new PaymentInHeaderData.Builder().amount("207.00")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(), } });
  }

  /**
   * Test the log in and log out.
   */
  @Test
  public void APRRegression37209InTest() {
    logger.info("** Start of test case [APRRegression37209In] Test Issue 37209**");

    // Credit payment
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentIn.create(paymentInHeaderDatacredit);
    paymentIn.assertSaved();
    String paymentDocumentNo = paymentIn.getData("documentNo").toString();
    AddPaymentProcess addPaymentProcesscredit = paymentIn.addDetailsOpen();
    addPaymentProcesscredit.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcesscredit.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep();
    addPaymentProcesscredit.scrollToBottom();
    addPaymentProcesscredit.assertTotalsData(addPaymentTotalsVerificationDatacredit);
    addPaymentProcesscredit.setParameterValue(PARAM_OVERPYMT_ACTION,
        "Leave the credit to be used later");
    addPaymentProcesscredit.process("Process Received Payment(s)");

    // Create Sales Invoice
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    String invoiceDocumentNo = salesInvoice.getData("documentNo").toString();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.assertData(salesInvoiceLineVerificationData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    SalesInvoice.PaymentInPlan paymentInPlan = salesInvoice.new PaymentInPlan(mainPage);
    paymentInPlan.assertCount(1);
    paymentInPlan.assertData(paymentInPlanData);

    // Create another Payment In, link sales invoice, credit payment with full amount
    Sleep.trySleep();
    paymentIn = new PaymentIn(mainPage).open();
    Sleep.trySleep();
    mainPage.waitForDataToLoad();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();

    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    Sleep.trySleep();

    addPaymentProcess.getOrderInvoiceGrid().waitForDataToLoad();
    addPaymentProcess.getOrderInvoiceGrid().clearFilters();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    Sleep.trySleep();
    addPaymentProcess.getOrderInvoiceGrid().filter("invoiceNo", invoiceDocumentNo);
    Sleep.trySleep();

    addPaymentProcess.scrollToBottom();

    addPaymentProcess.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess.getCreditToUseGrid().clearFilters();
    addPaymentProcess.getCreditToUseGrid().unselectAll();
    addPaymentProcess.getCreditToUseGrid().filter("documentNo", paymentDocumentNo);
    Sleep.trySleep();
    addPaymentProcess.getCreditToUseGrid().selectRowByContents("documentNo", paymentDocumentNo);
    addPaymentProcess.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("100.00").build());
    addPaymentProcess.getField("reference_no").focus();

    addPaymentProcess.scrollToBottom();
    Sleep.trySleep();

    addPaymentProcess.assertTotalsData(addPaymentTotalsVerificationData);
    addPaymentProcess.setParameterValue(PARAM_OVERPYMT_ACTION, "Leave the credit to be used later");
    addPaymentProcess.process("Process Received Payment(s)");

    paymentIn.getTab().refresh();
    paymentIn.assertData(paymentInHeaderVerificationData);

    PaymentIn.UsedCreditSource usedCreditSourceTab = paymentIn.new UsedCreditSource(mainPage);
    usedCreditSourceTab.assertCount(0);

    logger.info("** End of test case [APRRegression37209In] Test Issue 37209 **");

  }
}
