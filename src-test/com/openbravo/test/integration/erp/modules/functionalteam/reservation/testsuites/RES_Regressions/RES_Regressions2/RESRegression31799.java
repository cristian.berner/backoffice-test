/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 31799
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class RESRegression31799 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLinesData1;
  GoodsReceiptLinesData goodsReceiptLineVerificationData1;
  GoodsReceiptLinesData goodsReceiptLinesData2;
  GoodsReceiptLinesData goodsReceiptLineVerificationData2;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData1;
  SalesOrderLinesData salesOrderLinesVerificationData1;
  SalesOrderLinesData salesOrderLinesData2;
  SalesOrderLinesData salesOrderLinesVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  SalesOrderLinesData salesOrderLinesVerificationData3;
  SalesOrderLinesData salesOrderLinesVerificationData4;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData3;
  ReservationData reservationVerificationData1;
  StockData stockVerificationData1;
  ReservationData reservationVerificationData2;
  StockData stockVerificationData2;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLinesData;
  GoodsShipmentLinesData goodsShipmentLineVerificationData1;
  GoodsShipmentLinesData goodsShipmentLinesData2;
  GoodsShipmentLinesData goodsShipmentLinesData3;
  GoodsShipmentLinesData goodsShipmentLineVerificationData2;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  ReservationData reservationVerificationData3;
  StockData stockVerificationData3;
  ReservationData reservationVerificationData4;
  StockData stockVerificationData4;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;
  ReservationData reservationData;

  /**
   * Class constructor.
   *
   */
  public RESRegression31799(ProductData productData, PriceData productPriceData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesData1,
      GoodsReceiptLinesData goodsReceiptLineVerificationData1,
      GoodsReceiptLinesData goodsReceiptLinesData2,
      GoodsReceiptLinesData goodsReceiptLineVerificationData2,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderLinesData1,
      SalesOrderLinesData salesOrderLinesVerificationData1,
      SalesOrderLinesData salesOrderLinesData2,
      SalesOrderLinesData salesOrderLinesVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      SalesOrderLinesData salesOrderLinesVerificationData3,
      SalesOrderLinesData salesOrderLinesVerificationData4,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData3,
      ReservationData reservationVerificationData1, StockData stockVerificationData1,
      ReservationData reservationVerificationData2, StockData stockVerificationData2,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLinesData,
      GoodsShipmentLinesData goodsShipmentLineVerificationData1,
      GoodsShipmentLinesData goodsShipmentLinesData2,
      GoodsShipmentLinesData goodsShipmentLinesData3,
      GoodsShipmentLinesData goodsShipmentLineVerificationData2,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      ReservationData reservationVerificationData3, StockData stockVerificationData3,
      ReservationData reservationVerificationData4, StockData stockVerificationData4,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      ReservationData reservationData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesData1 = goodsReceiptLinesData1;
    this.goodsReceiptLineVerificationData1 = goodsReceiptLineVerificationData1;
    this.goodsReceiptLinesData2 = goodsReceiptLinesData2;
    this.goodsReceiptLineVerificationData2 = goodsReceiptLineVerificationData2;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData1 = salesOrderLinesData1;
    this.salesOrderLinesVerificationData1 = salesOrderLinesVerificationData1;
    this.salesOrderLinesData2 = salesOrderLinesData2;
    this.salesOrderLinesVerificationData2 = salesOrderLinesVerificationData2;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.salesOrderLinesVerificationData3 = salesOrderLinesVerificationData3;
    this.salesOrderLinesVerificationData4 = salesOrderLinesVerificationData4;
    this.bookedSalesOrderHeaderVerificationData2 = bookedSalesOrderHeaderVerificationData2;
    this.bookedSalesOrderHeaderVerificationData3 = bookedSalesOrderHeaderVerificationData3;
    this.reservationVerificationData1 = reservationVerificationData1;
    this.stockVerificationData1 = stockVerificationData1;
    this.reservationVerificationData2 = reservationVerificationData2;
    this.stockVerificationData2 = stockVerificationData2;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLinesData = goodsShipmentLinesData;
    this.goodsShipmentLineVerificationData1 = goodsShipmentLineVerificationData1;
    this.goodsShipmentLinesData2 = goodsShipmentLinesData2;
    this.goodsShipmentLinesData3 = goodsShipmentLinesData3;
    this.goodsShipmentLineVerificationData2 = goodsShipmentLineVerificationData2;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.reservationVerificationData3 = reservationVerificationData3;
    this.stockVerificationData3 = stockVerificationData3;
    this.reservationVerificationData4 = reservationVerificationData4;
    this.stockVerificationData4 = stockVerificationData4;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.reservationData = reservationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().storageBin("L01").build(),
        new GoodsReceiptLinesData.Builder().lineNo("10").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptLinesData.Builder().storageBin("L02").build(),
        new GoodsReceiptLinesData.Builder().lineNo("20").uOM("Unit").organization("Spain").build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("10").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("20.00")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("20").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("40.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("66.00")
            .currency("EUR")
            .reservationStatus("Not Reserved")
            .build(),
        new SalesOrderLinesData.Builder().reservationStatus("Not Reserved").build(),
        new SalesOrderLinesData.Builder().reservationStatus("Completely Reserved").build(),
        new SalesOrderHeaderData.Builder().reservationStatus("Partially Reserved").build(),
        new SalesOrderHeaderData.Builder().reservationStatus("Completely Reserved").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("10")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("20")
            .status("Completed")
            .reservedQuantity("20")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("10").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("10")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().lineNo("20").build(),
        new GoodsShipmentLinesData.Builder().storageBin("L02").build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("20")
            .uOM("Unit")
            .storageBin("L02")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("10")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .allocated(false)
            .build(),
        new ReservationData.Builder().organization("Spain")
            .quantity("20")
            .reservedQuantity("20")
            .releasedQuantity("20")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L02")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .releasedQuantity("20")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build(),

        new ReservationData.Builder().status("Completed").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 31799
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression31799Test() throws ParseException {
    logger.info("** Start of test case [RESRegression31799] Test regression 31799. **");

    String productName1 = "Test31799A-";
    int num1 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName1).build());
    productName1 = productName1 + String.valueOf(num1);
    ProductData productData1 = productData;
    productData1.addDataField("searchKey", productName1);
    productData1.addDataField("name", productName1);
    Product.ProductTab.create(mainPage, productData1);
    Product.Price.create(mainPage, productPriceData);

    String productName2 = "Test31799B-";
    int num2 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName2).build());
    productName2 = productName2 + String.valueOf(num2);
    ProductData productData2 = productData;
    productData2.addDataField("searchKey", productName2);
    productData2.addDataField("name", productName2);
    Product.ProductTab.create(mainPage, productData2);
    Product.Price.create(mainPage, productPriceData);

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines1 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData1.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    goodsReceiptLinesData1.addDataField("movementQuantity", "10");
    goodsReceiptLines1.create(goodsReceiptLinesData1);
    goodsReceiptLines1.assertSaved();
    goodsReceiptLines1.assertData(goodsReceiptLineVerificationData1);
    GoodsReceipt.Lines goodsReceiptLines2 = goodsReceipt.new Lines(mainPage);
    goodsReceiptLinesData2.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    goodsReceiptLinesData2.addDataField("movementQuantity", "20");
    goodsReceiptLines2.create(goodsReceiptLinesData2);
    goodsReceiptLines2.assertSaved();
    goodsReceiptLines2.assertData(goodsReceiptLineVerificationData2);
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines1 = salesOrder.new Lines(mainPage);
    salesOrderLinesData1.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName1).build());
    salesOrderLines1.create(salesOrderLinesData1);
    salesOrderLines1.assertSaved();
    salesOrderLines1.assertData(salesOrderLinesVerificationData1);
    String orderLine1Identifier = String.format(" - %s - %s", salesOrderLines1.getData("lineNo"),
        salesOrderLines1.getData("lineNetAmount"));
    SalesOrder.Lines salesOrderLines2 = salesOrder.new Lines(mainPage);
    salesOrderLinesData2.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName2).build());
    salesOrderLines2.create(salesOrderLinesData2);
    salesOrderLines2.assertSaved();
    salesOrderLines2.assertData(salesOrderLinesVerificationData2);
    String orderLine2Identifier = String.format(" - %s - %s", salesOrderLines2.getData("lineNo"),
        salesOrderLines2.getData("lineNetAmount"));
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLine1Identifier = orderIdentifier + orderLine1Identifier;
    orderLine2Identifier = orderIdentifier + orderLine2Identifier;

    salesOrderLines1.select(salesOrderLinesData1);
    salesOrderLines1.assertData(salesOrderLinesVerificationData3);
    salesOrderLines1.manageReservation();
    Sleep.trySleep(6000);
    salesOrderLines1.assertData(salesOrderLinesVerificationData4);
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData2);
    salesOrderLines2.select(salesOrderLinesData2);
    salesOrderLines2.assertData(salesOrderLinesVerificationData3);
    salesOrderLines2.manageReservation();
    Sleep.trySleep(6000);
    salesOrderLines2.assertData(salesOrderLinesVerificationData4);
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData3);

    StockReservation stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    StockReservation.Stock stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertData(stockVerificationData1);
    StockReservation stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    StockReservation.Stock stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertData(stockVerificationData2);

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("L01", orderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    String shipmentNo = goodsShipment.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(2);
    goodsShipmentLines.select(goodsShipmentLinesData);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData1);
    goodsShipmentLines.cancelOnGrid();
    goodsShipmentLines.getTab().clearFilters();
    goodsShipmentLines.getTab().unselectAll();
    goodsShipmentLines.select(goodsShipmentLinesData2);
    Sleep.trySleep();
    goodsShipmentLines.edit(goodsShipmentLinesData3);
    goodsShipmentLines.assertData(goodsShipmentLineVerificationData2);
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData);

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData3);
    salesOrderLines1.select(salesOrderLinesData1);
    salesOrderLines1.assertData(salesOrderLinesVerificationData4);
    salesOrderLines2.select(salesOrderLinesData2);
    salesOrderLines2.assertData(salesOrderLinesVerificationData4);

    stockReservation1 = new StockReservation(mainPage).open();
    // stockReservation1.getTab().clearFilters();
    stockReservation1
        .select(new ReservationData.Builder().salesOrderLine(orderLine1Identifier).build());
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData3.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertData(stockVerificationData3);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2
        .select(new ReservationData.Builder().salesOrderLine(orderLine2Identifier).build());
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData4.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertData(stockVerificationData4);

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(shipmentNo).build());
    goodsShipment.close();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData3);
    salesOrderLines1.select(salesOrderLinesData1);
    salesOrderLines1.assertData(salesOrderLinesVerificationData4);
    salesOrderLines2.select(salesOrderLinesData2);
    salesOrderLines2.assertData(salesOrderLinesVerificationData4);

    stockReservation1 = new StockReservation(mainPage).open();
    stockReservation1.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine1Identifier));
    stockReservation1.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName1)
            .addDataField("salesOrderLine", orderLine1Identifier));
    stock1 = stockReservation1.new Stock(mainPage);
    stock1.assertData(stockVerificationData1);
    stockReservation2 = new StockReservation(mainPage).open();
    stockReservation2.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLine2Identifier)
            .addDataField("warehouse", ""));
    Sleep.trySleep();
    stockReservation2.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName2)
            .addDataField("salesOrderLine", orderLine2Identifier));
    stock2 = stockReservation2.new Stock(mainPage);
    stock2.assertData(stockVerificationData2);

    logger.info("** End of test case [RESRegression31799] Test regression 31799. **");
  }

  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
