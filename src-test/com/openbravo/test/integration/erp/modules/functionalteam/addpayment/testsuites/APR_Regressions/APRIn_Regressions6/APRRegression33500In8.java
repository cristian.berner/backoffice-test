/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions6;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500In8 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesVerificationData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500In8(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesVerificationData, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesVerificationData = paymentInLinesVerificationData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .transactionDocument("AR Credit Memo")
            .paymentTerms("90 days")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("90 days")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGC")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("10")
            .tax("VAT 10% USA")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("10").lineNetAmount("20.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .received("0.00")
            .outstanding("-22.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .received("-11.00")
            .outstanding("-11.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("-11.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-22.00")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "70000", "Ventas de mercaderías", "20.00", "" },
            { "47700", "Hacienda Pública IVA repercutido", "2.00", "" },
            { "43000", "Clientes (euros)", "", "22.00" } },

        new String[][] { { "41100", "Sales", "50.00", "" }, { "21400", "Tax Due", "5.00", "" },
            { "11400", "Accounts receivable", "", "55.00" }, },

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-11.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("-22.00")
            .expected("-22.00")
            .received("-11.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "", "-11.00" },
            { "43100", "Efectos comerciales en cartera", "-11.00", "" } },
        new String[][] { { "11400", "Accounts receivable", "", "-27.50" },
            { "11300", "Bank in transit", "-27.50", "" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Positive AR Credit Memo Invoice Partially paid – Simple Payment Terms
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500In8Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33500In8] Test regression 33500 - Positive AR Credit Memo Invoice Partially paid – Simple Payment Terms. **");

    // Create a Sales invoice
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(salesInvoiceLineData);
    invoiceLines.assertSaved();
    invoiceLines.assertData(salesInvoiceLineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check invoice pending payment in plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Pay the invoice
    AddPaymentProcess paymentDetails = invoice.openAddPayment();

    AddPaymentGrid orderInvoiceGrid = paymentDetails.getOrderInvoiceGrid();
    orderInvoiceGrid.unselectAll();
    orderInvoiceGrid.filter(new SelectedPaymentData.Builder().invoiceNo(invoiceNo).build());

    paymentDetails.setParameterValue("actual_payment", "-11.00");
    paymentDetails.getField("reference_no").focus();

    paymentDetails.editOrderInvoiceRecord(0,
        new SelectedPaymentData.Builder().amount("-11.00").build());
    paymentDetails.process("Process Received Payment(s) and Deposit");

    invoice.assertPaymentCreatedSuccessfully();
    invoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(false).build());

    // Check invoice payment in plan
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    // Check invoice payment in details
    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    String paymentNo = invoicePaymentInDetails.getData("paymentDetails$finPayment$documentNo")
        .toString();

    // Post invoice and check post
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post1 = new Post(mainPage);
    post1.assertJournalLinesCount(journalEntryLines.length);
    post1.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post1 = new Post(mainPage);
    post1.assertJournalLinesCount(journalEntryLines2.length);
    post1.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check payment in
    PaymentIn payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(1);
    paymentLines.assertData((PaymentInLinesData) paymentInLinesVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("dueDate", OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
        .addDataField("invoiceAmount", "22.00"));

    // Post payment and check
    payment.open();
    payment.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines3.length);
    post2.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines4.length);
    post2.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [APRRegression33500In8] Test regression 33500 - Positive AR Credit Memo Invoice Partially paid – Simple Payment Terms. **");
  }
}
