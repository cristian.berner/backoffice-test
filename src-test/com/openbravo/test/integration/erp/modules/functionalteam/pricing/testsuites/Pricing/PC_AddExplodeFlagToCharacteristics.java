/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Jonathan Bueno <jonathan.bueno@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.pricing.testsuites.Pricing;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.CharacteristicsData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductCharacteristicValueData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.ProductCharacteristic;

/**
 * Execute the Setup User and Role flow of the smoke test suite.
 *
 * @author JBU
 */
@RunWith(Parameterized.class)
public class PC_AddExplodeFlagToCharacteristics extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  private ProductData productData;
  private ProductCharacteristicValueData productCharacteristicValueData;
  private ProductCharacteristicValueData productCharacteristicValueData2;
  private ProductCharacteristicData productCharacteristicData;
  private ProductCharacteristicData productCharacteristicData2;
  private CharacteristicsData characteristicsData;
  private CharacteristicsData characteristicsData2;
  private CharacteristicsData characteristicsData3;
  private CharacteristicsData characteristicsData4;

  /**
   * Class constructor.
   */
  public PC_AddExplodeFlagToCharacteristics(ProductCharacteristicData productCharacteristicData,
      ProductCharacteristicValueData productCharacteristicValueData,
      ProductCharacteristicValueData productCharacteristicValueData2,
      ProductCharacteristicData productCharacteristicData2, ProductData productData,
      CharacteristicsData characteristicsData, CharacteristicsData characteristicsData2,
      CharacteristicsData characteristicsData3, CharacteristicsData characteristicsData4) {

    this.productData = productData;
    this.productCharacteristicValueData = productCharacteristicValueData;
    this.productCharacteristicValueData2 = productCharacteristicValueData2;
    this.productCharacteristicData = productCharacteristicData;
    this.productCharacteristicData2 = productCharacteristicData2;
    this.characteristicsData = characteristicsData;
    this.characteristicsData2 = characteristicsData2;
    this.characteristicsData3 = characteristicsData3;
    this.characteristicsData4 = characteristicsData4;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   * @throws IOException
   *
   */
  @Parameters
  public static Collection<Object[]> setupProductValues() throws IOException {
    return Arrays.asList(new Object[][] { {

        // Create a Product
        new ProductCharacteristicData.Builder().name("Explode Characteristic")
            .active(true)
            .variant(true)
            .explode(true)
            .build(),
        new ProductCharacteristicValueData.Builder().name("Value1").build(),
        new ProductCharacteristicValueData.Builder().name("Value2").build(),
        new ProductCharacteristicData.Builder().name("No Explode Characteristic")
            .active(true)
            .variant(true)
            .explode(false)
            .build(),
        new ProductData.Builder().searchKey("ECTTP")
            .name("Explode config tab test product")
            .productCategory("Services")
            .uOM("Unit")
            .productType("Item")
            .purchase(true)
            .sale(true)
            .taxCategory("VAT 3%")
            .generic(true)
            .build(),
        new CharacteristicsData.Builder().characteristic("Explode Characteristic")
            .variant(true)
            .build(),
        new CharacteristicsData.Builder().characteristic("No Explode Characteristic")
            .variant(true)
            .build(),
        new CharacteristicsData.Builder().characteristic("No Explode Characteristic")
            .variant(true)
            .explode(true)
            .build(),
        new CharacteristicsData.Builder().characteristic("Explode Characteristic")
            .variant(true)
            .explode(false)
            .build() } });

  }

  /**
   * Test the setup client and organization flow.
   *
   * @throws IOException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void testExplodeFlagInCharacteristics() throws IOException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [PC040] Is explode flag characteristic test (Communit-9874) **");
    ProductCharacteristic.ProductCharacteristicsTab.create(mainPage, productCharacteristicData);
    ProductCharacteristic.ProductCharacteristicValueTab.create(mainPage,
        productCharacteristicValueData);
    ProductCharacteristic.ProductCharacteristicValueTab.create(mainPage,
        productCharacteristicValueData2);
    ProductCharacteristic.ProductCharacteristicsTab.create(mainPage, productCharacteristicData2);
    ProductCharacteristic.ProductCharacteristicValueTab.create(mainPage,
        productCharacteristicValueData);
    ProductCharacteristic.ProductCharacteristicValueTab.create(mainPage,
        productCharacteristicValueData2);
    Product.ProductTab.create(mainPage, productData);
    // Assign "Explode characteristic" to product
    Product.ProductCharacteristicsValueTab.create(mainPage, characteristicsData);
    Product.ProductCharacteristicsValueTab.select(mainPage, characteristicsData);
    // Check that values have exploded
    Product.CharacteristicsConfiguration.verifyCount(mainPage, 2);

    // Assign "No Explode characteristic" to product
    Product.ProductCharacteristicsValueTab.create(mainPage, characteristicsData2);
    Product.ProductCharacteristicsValueTab.select(mainPage, characteristicsData2);
    // Check that values have not exploded
    Product.CharacteristicsConfiguration.verifyCount(mainPage, 0);
    // Delete characteristics assigned to product
    Product.ProductCharacteristicsValueTab.delete(mainPage);

    logger.info(
        "** Start of test case [PC050] Is explode flag add new values to existing characteristic (Communit-9875) **");
    // Assign "No Explode characteristic" (Explode ->True)to product
    Product.ProductCharacteristicsValueTab.create(mainPage, characteristicsData3);

    Product.ProductCharacteristicsValueTab.select(mainPage, characteristicsData3);
    // Check that values have exploded
    Product.CharacteristicsConfiguration.verifyCount(mainPage, 2);

    // Assign "Explode characteristic" (Explode ->False)to product
    Product.ProductCharacteristicsValueTab.create(mainPage, characteristicsData4);
    Product.ProductCharacteristicsValueTab.select(mainPage, characteristicsData4);
    // Check that values have not exploded
    Product.CharacteristicsConfiguration.verifyCount(mainPage, 0);

    // Delete the created product
    Product.ProductTab.delete(mainPage, productData);
  }
}
