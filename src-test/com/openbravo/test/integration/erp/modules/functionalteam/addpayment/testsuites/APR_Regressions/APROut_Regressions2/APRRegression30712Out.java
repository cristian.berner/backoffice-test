/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.BusinessPartnerData;
import com.openbravo.test.integration.erp.data.masterdata.businesspartner.VendorCreditorData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.masterdata.businesspartner.BusinessPartner;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;

/**
 * Test regression 30712
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRRegression30712Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  BusinessPartnerData businessPartnerData;
  VendorCreditorData vendorData;
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodData2;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  AddPaymentPopUpData addPaymentVerificationData2;
  PaymentMethodData paymentMethodData3;
  PaymentMethodData paymentMethodData4;
  PaymentMethodData paymentMethodData5;
  AccountData accountHeaderData2;
  AccountData accountHeaderData3;
  PaymentMethodData paymentMethodData6;
  AddPaymentPopUpData addPaymentVerificationData3;
  VendorCreditorData vendorData2;
  AccountData accountHeaderData4;
  PaymentMethodData paymentMethodData7;
  VendorCreditorData vendorData3;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2;
  VendorCreditorData vendorData4;

  /**
   * Class constructor.
   *
   */
  public APRRegression30712Out(BusinessPartnerData businessPartnerData,
      VendorCreditorData vendorData, AccountData accountHeaderData,
      PaymentMethodData paymentMethodData, PaymentMethodData paymentMethodData2,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      AddPaymentPopUpData addPaymentVerificationData2, PaymentMethodData paymentMethodData3,
      PaymentMethodData paymentMethodData4, PaymentMethodData paymentMethodData5,
      AccountData accountHeaderData2, AccountData accountHeaderData3,
      PaymentMethodData paymentMethodData6, AddPaymentPopUpData addPaymentVerificationData3,
      VendorCreditorData vendorData2, AccountData accountHeaderData4,
      PaymentMethodData paymentMethodData7, VendorCreditorData vendorData3,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData2,
      VendorCreditorData vendorData4) {
    this.businessPartnerData = businessPartnerData;
    this.vendorData = vendorData;
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodData2 = paymentMethodData2;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.paymentMethodData3 = paymentMethodData3;
    this.paymentMethodData4 = paymentMethodData4;
    this.paymentMethodData5 = paymentMethodData5;
    this.accountHeaderData2 = accountHeaderData2;
    this.accountHeaderData3 = accountHeaderData3;
    this.paymentMethodData6 = paymentMethodData6;
    this.addPaymentVerificationData3 = addPaymentVerificationData3;
    this.vendorData2 = vendorData2;
    this.accountHeaderData4 = accountHeaderData4;
    this.paymentMethodData7 = paymentMethodData7;
    this.vendorData3 = vendorData3;
    this.completedPurchaseInvoiceHeaderVerificationData2 = completedPurchaseInvoiceHeaderVerificationData2;
    this.vendorData4 = vendorData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] {
        { new BusinessPartnerData.Builder().name("Vendor Barcelona").build(),
            new VendorCreditorData.Builder().pOPaymentMethod("").pOFinancialAccount("").build(),

            new AccountData.Builder().name("Spain Bank").build(),
            new PaymentMethodData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
            new PaymentMethodData.Builder().pmdefault(true).build(),

            new PurchaseInvoiceHeaderData.Builder()
                .businessPartner(
                    new BusinessPartnerSelectorData.Builder().name("Vendor Barcelona").build())
                .paymentMethod("Acc-8 (Transactions)")
                .priceList("Purchase USD")
                .build(),
            new PurchaseInvoiceHeaderData.Builder().organization("Spain")
                .transactionDocument("AP Invoice")
                .partnerAddress(".Barcelona, Enorme")
                .paymentTerms("90 days")
                .build(),
            new PurchaseInvoiceLinesData.Builder()
                .product(new ProductSimpleSelectorData.Builder().productName("USD Cost").build())
                .invoicedQuantity("5")
                .tax("VAT 10%")
                .build(),
            new PurchaseInvoiceLinesData.Builder().uOM("Unit")
                .unitPrice("10.00")
                .listPrice("10.00")
                .lineNetAmount("50.00")
                .build(),
            new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
                .grandTotalAmount("55.00")
                .summedLineAmount("50.00")
                .currency("USD")
                .paymentComplete(false)
                .build(),

            new AddPaymentPopUpData.Builder().received_from("Vendor Barcelona")
                .fin_paymentmethod_id("Acc-8 (Transactions)")
                .fin_financial_account_id("Spain Bank - EUR")
                .c_currency_id("USD")
                .actual_payment("55.00")
                .expected_payment("55.00")
                .c_currency_to_id("EUR")
                .converted_amount("22.00")
                .conversion_rate("0.4")
                .amount_gl_items("0.00")
                .amount_inv_ords("55.00")
                .total("55.00")
                .difference("0.00")
                .build(),
            new AddPaymentPopUpData.Builder().received_from("Vendor Barcelona")
                .fin_paymentmethod_id("Acc-8 (Transactions)")
                .fin_financial_account_id("Mexico Bank - USD")
                .c_currency_id("USD")
                .actual_payment("55.00")
                .expected_payment("55.00")
                .amount_gl_items("0.00")
                .amount_inv_ords("55.00")
                .total("55.00")
                .difference("0.00")
                .build(),

            new PaymentMethodData.Builder().pmdefault(false)
                .payoutAllow(false)
                .payoutMulticurrencyAllow(true)
                .build(),

            new PaymentMethodData.Builder().pmdefault(false)
                .payoutAllow(true)
                .payoutMulticurrencyAllow(false)
                .build(),

            new PaymentMethodData.Builder().pmdefault(false)
                .payoutAllow(true)
                .payoutMulticurrencyAllow(true)
                .build(),
            new AccountData.Builder().active(false).build(),

            new AccountData.Builder().name("Mexico Bank").build(),
            new PaymentMethodData.Builder().active(false).build(),

            new AddPaymentPopUpData.Builder().received_from("Vendor Barcelona")
                .fin_paymentmethod_id("")
                .fin_financial_account_id("")
                .c_currency_id("USD")
                .actual_payment("55.00")
                .expected_payment("55.00")
                .amount_gl_items("0.00")
                .amount_inv_ords("55.00")
                .total("55.00")
                .difference("0.00")
                .build(),

            new VendorCreditorData.Builder().pOPaymentMethod("Acc-8 (Transactions)")
                .pOFinancialAccount("Mexico Bank")
                .build(),

            new AccountData.Builder().active(true).build(),
            new PaymentMethodData.Builder().active(true).build(),

            new VendorCreditorData.Builder().pOPaymentMethod("6 (Spain)")
                .pOFinancialAccount("Spain Bank")
                .build(),

            new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
                .grandTotalAmount("55.00")
                .summedLineAmount("50.00")
                .currency("USD")
                .paymentComplete(true)
                .build(),

            new VendorCreditorData.Builder().pOPaymentMethod("6 (Spain)")
                .pOFinancialAccount("")
                .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 30712 - Payment Out flow
   *
   * @throws ParseException
   * @throws OpenbravoERPTestException
   */
  @Test
  public void APRRegression30712OutTest() throws ParseException, OpenbravoERPTestException {
    logger.info(
        "** Start of test case [APRRegression30712Out] Test regression 30712 - Payment Out flow. **");

    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.VendorCreditor.edit(mainPage, vendorData);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Set payment method as default = true, paymentIn = true and paymentInMulticurrency = true for
    // Spain Bank
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData2);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Spain Bank and Mexico Bank financial accounts are available, Spain Bank as default
    AddPaymentProcess addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Mexico Bank - USD");
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentOut = false and paymentOutMulticurrency = true
    // for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData3);

    // Mexico Bank is available
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentOut = true and paymentOutMulticurrency = false
    // for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData4);

    // Mexico Bank is available
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as default = false, paymentOut = true and paymentOutMulticurrency = true
    // and financial account as active = false for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData5);
    financialAccount.edit(accountHeaderData2);

    // Mexico Bank is available
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.close();

    // Set payment method as active = false for Mexico Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData6);

    // No financial accounts are available
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData3);
    addPaymentProcess.close();

    // Set financial account as active = true for Spain Bank
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    financialAccount.edit(accountHeaderData4);

    // Set payment method as active = true for Mexico Bank
    financialAccount.select(accountHeaderData3);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodData7);

    // Set payment method and Mexico Bank financial account as default for Business Partner
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.VendorCreditor.edit(mainPage, vendorData2);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Spain Bank and Mexico Bank financial accounts are available, Mexico Bank as default
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Spain Bank - EUR");
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.close();

    // Set original payment method and Spain Bank financial account as default for Business Partner
    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.VendorCreditor.edit(mainPage, vendorData3);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    // Spain Bank and Mexico Bank financial accounts are available, Spain Bank as default
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    addPaymentProcess = purchaseInvoice.openAddPayment();
    addPaymentProcess.assertData(addPaymentVerificationData);
    addPaymentProcess.setParameterValue("fin_financial_account_id", "Mexico Bank - USD");
    addPaymentProcess.assertData(addPaymentVerificationData2);
    addPaymentProcess.process("Process Made Payment(s) and Withdrawal");
    purchaseInvoice.assertPaymentCreatedSuccessfully();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData2);

    BusinessPartner.BusinessPartnerTab.open(mainPage);
    BusinessPartner.BusinessPartnerTab.select(mainPage, businessPartnerData);
    BusinessPartner.VendorCreditor.edit(mainPage, vendorData4);
    BusinessPartner.BusinessPartnerTab.close(mainPage);

    logger.info(
        "** End of test case [APRRegression30712Out] Test regression 30712 - Payment Out flow. **");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
