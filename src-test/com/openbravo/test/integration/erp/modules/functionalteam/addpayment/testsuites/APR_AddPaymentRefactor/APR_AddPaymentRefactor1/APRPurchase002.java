/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.

 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor1;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

@RunWith(Parameterized.class)
public class APRPurchase002 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the purchase order header creation. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  /** The purchase order line data. */
  PurchaseOrderLinesData purchaseOrderLinesData;
  /** The data to verify the purchase order line creation. */
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  /** The booked purchase order verification data. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData;
  AddPaymentPopUpData addPaymentTotalsVerificationData;

  public APRPurchase002(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentPlanData purchaseOrderPaymentOutPlanData,
      AddPaymentPopUpData addPaymentTotalsVerificationData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.purchaseOrderPaymentOutPlanData = purchaseOrderPaymentOutPlanData;
    this.addPaymentTotalsVerificationData = addPaymentTotalsVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> APRPurchase001Values() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [PROa010] Create purchase order.
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .documentStatus("Draft")
            .currency("EUR")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .orderedQuantity("100")
            .build(),
        new PurchaseOrderLinesData.Builder().unitPrice("2.00")
            .listPrice("2.00")
            .uOM("Bag")
            .tax("VAT 10%")
            .lineNetAmount("200.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().summedLineAmount("200.00")
            .grandTotalAmount("220.00")
            .documentStatus("Booked")
            .build(),
        new PaymentPlanData.Builder().paymentMethod("1 (Spain)")
            .expected("220.00")
            .paid("0.00")
            .outstanding("220.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().amount_gl_items("0.00")
            .amount_inv_ords("220.00")
            .total("220.00")
            .difference("0.00")
            .build() } });
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void executeAPRPurchase002() {
    logger.info("** Start of test case [APRPurchase002] **");
    // Create Purchase Order
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    PurchaseOrder.PaymentOutPlan purchaseOrderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(
        mainPage);
    purchaseOrderPaymentOutPlan.assertCount(1);
    purchaseOrderPaymentOutPlan.assertData(purchaseOrderPaymentOutPlanData);

    // Add Payment
    purchaseOrder.addPayment("Process Made Payment(s)", addPaymentTotalsVerificationData);
    purchaseOrder.assertPaymentCreatedSuccessfully();
    logger.info("** End of test case [APRPurchase002] **");
  }
}
