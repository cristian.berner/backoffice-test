/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.DiscountsData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.TaxData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

@RunWith(Parameterized.class)
public class SALRegression34392 extends OpenbravoERPTest {
  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** The sales invoice header data */
  SalesInvoiceHeaderData salesInvoiceHeaderData;

  /** The data to verify creation of the sales invoice header */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;

  /** The sales invoice Line Data */
  SalesInvoiceLinesData salesInvoiceLineData;

  /** The sales invoice Line verification data */
  SalesInvoiceLinesData salesInvoiceLineVerificationData;

  /** The sales invoice discount data */
  DiscountsData salesInvoiceDiscountData;

  /** The completed sales invoice header verification data */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  /** The sales invoice discount data line filter */
  SalesInvoiceLinesData salesInvoiceDiscountLineFilterData;

  /** The sales invoice discount line verification data */
  SalesInvoiceLinesData salesInvoiceDiscountLineVerificationData;

  /** The sales invoice tax data */
  TaxData salesInvoiceTaxData;

  /** The voided sales invoice header verification data */
  SalesInvoiceHeaderData voidSalesInvoiceHeaderVerificationData;

  /** The reversed sales invoice header verification data */
  SalesInvoiceHeaderData reversedSalesInvoiceHeaderVerificationData;

  /** The reverse sales invoice line data filter */
  SalesInvoiceLinesData reverseSalesInvoiceLineFilterData;

  /** The reversed sales invoice line verification data */
  SalesInvoiceLinesData reversedSalesInvoiceLineVerificationData;

  /** The reversed sales invoice Discount line verification data */
  SalesInvoiceLinesData reversedSalesInvoiceDiscountLineVerificationData;

  /** The reversed sales invoice tax data */
  TaxData reversedSalesInvoiceTaxData;

  public SALRegression34392(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      DiscountsData salesInvoiceDiscountData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceDiscountLineFilterData,
      SalesInvoiceLinesData salesInvoiceDiscountLineVerificationData, TaxData salesInvoiceTaxData,
      SalesInvoiceHeaderData voidSalesInvoiceHeaderVerificationData,
      SalesInvoiceHeaderData reversedSalesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData reverseSalesInvoiceLineFilterData,
      SalesInvoiceLinesData reversedSalesInvoiceLineVerificationData,
      SalesInvoiceLinesData reversedSalesInvoiceDiscountLineVerificationData,
      TaxData reversedSalesInvoiceTaxData) {

    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.salesInvoiceDiscountData = salesInvoiceDiscountData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceDiscountLineFilterData = salesInvoiceDiscountLineFilterData;
    this.salesInvoiceDiscountLineVerificationData = salesInvoiceDiscountLineVerificationData;
    this.salesInvoiceTaxData = salesInvoiceTaxData;
    this.voidSalesInvoiceHeaderVerificationData = voidSalesInvoiceHeaderVerificationData;
    this.reversedSalesInvoiceHeaderVerificationData = reversedSalesInvoiceHeaderVerificationData;
    this.reversedSalesInvoiceLineVerificationData = reversedSalesInvoiceLineVerificationData;
    this.reverseSalesInvoiceLineFilterData = reverseSalesInvoiceLineFilterData;
    this.reversedSalesInvoiceDiscountLineVerificationData = reversedSalesInvoiceDiscountLineVerificationData;
    this.reversedSalesInvoiceTaxData = reversedSalesInvoiceTaxData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesInvoiceValues() {
    Object[][] data = new Object[][] { {

        // Create Sales Invoice header
        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),
        // Verify Sales Invoice Header
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .priceList("Customer A")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().netListPrice("2.00")
                .productName("Final good A")
                .build())
            .invoicedQuantity("100")
            .tax("VAT 10% USA")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("200.00")
            .build(),
        new DiscountsData.Builder().discount("Discount 10%").build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .summedLineAmount("180.00")
            .grandTotalAmount("198.00")
            .currency("EUR")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("20").invoicedQuantity("1").build(),
        new SalesInvoiceLinesData.Builder().uOM("Unit")
            .unitPrice("-20.00")
            .lineNetAmount("-20.00")
            .tax("VAT 10% USA")
            .build(),
        new TaxData.Builder().tax("VAT 10% USA - VAT 10%")
            .taxAmount("18.00")
            .taxableAmount("180.00")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Voided")
            .summedLineAmount("180.00")
            .grandTotalAmount("198.00")
            .currency("EUR")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Voided")
            .summedLineAmount("-180.00")
            .grandTotalAmount("-198.00")
            .currency("EUR")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("10").invoicedQuantity("-100").build(),
        new SalesInvoiceLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("-200.00")
            .tax("VAT 10% USA")
            .build(),
        new SalesInvoiceLinesData.Builder().uOM("Unit")
            .unitPrice("20.00")
            .lineNetAmount("20.00")
            .tax("VAT 10% USA")
            .invoicedQuantity("1")
            .build(),
        new TaxData.Builder().tax("VAT 10% USA - VAT 10%")
            .taxAmount("-18.00")
            .taxableAmount("-180.00")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void SAL_Regression34392InTest() throws ParseException {

    logger.info("** Start of test case SAL_Regression34392In. **");

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();

    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.BasicDiscounts basicDiscounts = salesInvoice.new BasicDiscounts(mainPage);
    basicDiscounts.create(salesInvoiceDiscountData);

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);
    salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(2);
    salesInvoiceLine.select(salesInvoiceDiscountLineFilterData);
    salesInvoiceLine.assertData(salesInvoiceDiscountLineVerificationData);
    SalesInvoice.Tax tax = salesInvoice.new Tax(mainPage);
    tax.assertCount(1);
    tax.assertData(salesInvoiceTaxData);

    // Store invoice documentno
    final String invoiceNo = salesInvoice.getData("documentNo").toString();

    if (salesInvoice.isPosted()) {
      salesInvoice.unpost();
    }
    String reversedDocumentNo = invoiceNo.substring(0, 2)
        .concat(String.valueOf((Integer.valueOf(invoiceNo.substring(2)) + 1)));
    salesInvoice.close();
    salesInvoice.assertProcessCompletedSuccessfully2("Reversed by document: " + reversedDocumentNo);

    mainPage.closeAllViews();
    salesInvoice = new SalesInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    salesInvoice
        .select(new SalesInvoiceHeaderData.Builder().documentNo(reversedDocumentNo).build());

    salesInvoice.assertData(reversedSalesInvoiceHeaderVerificationData);
    salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.getTab().clearFilters();
    salesInvoiceLine.select(reverseSalesInvoiceLineFilterData);
    salesInvoiceLine.assertData(reversedSalesInvoiceLineVerificationData);
    salesInvoiceLine.getTab().clearFilters();
    salesInvoiceLine.select(salesInvoiceDiscountLineFilterData);
    salesInvoiceLine.assertData(reversedSalesInvoiceDiscountLineVerificationData);
    basicDiscounts = salesInvoice.new BasicDiscounts(mainPage);
    basicDiscounts.assertCount(1);
    basicDiscounts.assertData(salesInvoiceDiscountData);
    tax = salesInvoice.new Tax(mainPage);
    tax.assertCount(1);
    tax.assertData(reversedSalesInvoiceTaxData);

    logger.info("** End of test case SAL_Regression34392In. **");
  }
}
