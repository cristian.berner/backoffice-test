/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions7;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.PaymentOutPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentout.PaymentOutTab;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice.Lines;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor.PaymentOutPlan;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor.PaymentOutPlan.PaymentOutDetails;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test Regression 33500
 *
 * @author collazoandy4
 *
 */
@RunWith(Parameterized.class)
public class APRRegression33500Out20 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  ReturnToVendorHeaderData rtvHeaderData;
  ReturnToVendorLineSelectorData rtvLineSelectorData;
  ReturnToVendorHeaderData rtvHeaderVerificationData;
  PaymentOutPlanData rtvPendingPaymentData;
  ReturnToVendorShipmentHeaderData rtvShipmentHeaderData;
  ReturnToVendorShipmentHeaderData rtvShipmentHeaderVerificationData;
  PurchaseInvoiceHeaderData invoiceHeaderData;
  PurchaseInvoiceHeaderData invoiceHeaderVerificationData;
  PaymentPlanData invoicePendingPaymentPlan;
  PaymentPlanData invoicePendingPaymentPlan2;
  PaymentOutLinesData paymentLineVerificationData;
  PaymentOutLinesData paymentLineVerificationData2;
  PaymentOutPlanData rtvPaymentData;

  public APRRegression33500Out20(GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptLinesData goodsReceiptLinesData, ReturnToVendorHeaderData rtvHeaderData,
      ReturnToVendorLineSelectorData rtvLineSelectorData,
      ReturnToVendorHeaderData rtvHeaderVerificationData, PaymentOutPlanData rtvPendingPaymentData,
      ReturnToVendorShipmentHeaderData rtvShipmentHeaderData,
      ReturnToVendorShipmentHeaderData rtvShipmentHeaderVerificationData,
      PurchaseInvoiceHeaderData invoiceHeaderData,
      PurchaseInvoiceHeaderData invoiceHeaderVerificationData,
      PaymentPlanData invoicePendingPaymentPlan, PaymentPlanData invoicePendingPaymentPlan2,
      PaymentOutLinesData paymentLineVerificationData,
      PaymentOutLinesData paymentLineVerificationData2, PaymentOutPlanData rtvPaymentData) {
    super();
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.rtvHeaderData = rtvHeaderData;
    this.rtvLineSelectorData = rtvLineSelectorData;
    this.rtvHeaderVerificationData = rtvHeaderVerificationData;
    this.rtvPendingPaymentData = rtvPendingPaymentData;
    this.rtvShipmentHeaderData = rtvShipmentHeaderData;
    this.rtvShipmentHeaderVerificationData = rtvShipmentHeaderVerificationData;
    this.invoiceHeaderData = invoiceHeaderData;
    this.invoiceHeaderVerificationData = invoiceHeaderVerificationData;
    this.invoicePendingPaymentPlan = invoicePendingPaymentPlan;
    this.invoicePendingPaymentPlan2 = invoicePendingPaymentPlan2;
    this.paymentLineVerificationData = paymentLineVerificationData;
    this.paymentLineVerificationData2 = paymentLineVerificationData2;
    this.rtvPaymentData = rtvPaymentData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> values() {
    Object[][] data = new Object[][] { {

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Volley Ball").build())
            .movementQuantity("2025")
            .build(),

        new ReturnToVendorHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentTerms("30-60")
            .build(),

        new ReturnToVendorLineSelectorData.Builder().returned("2025").unitPrice("28.50").build(),

        new ReturnToVendorHeaderData.Builder().documentStatus("Booked")
            .grandTotalAmount("63,483.75")
            .paymentTerms("30-60")
            .build(),

        new PaymentOutPlanData.Builder().expected("-63,483.75")
            .outstanding("-63,483.75")
            .paid("0.00")
            .numberOfPayments("0")
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .transactionDocument("Reversed Purchase Invoice")
            .paymentTerms("30-60")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("Reversed Purchase Invoice")
            .paymentTerms("30-60")
            .documentStatus("Completed")
            .grandTotalAmount("-63,483.75")
            .paymentComplete(false)
            .build(),

        new PaymentPlanData.Builder().expected("-31,741.88")
            .outstanding("-31,741.88")
            .numberOfPayments("0")
            .build(),

        new PaymentPlanData.Builder().expected("-31,741.87")
            .outstanding("-31,741.87")
            .numberOfPayments("0")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("-63,483.75")
            .expected("-31,741.88")
            .paid("-31,741.88")
            .build(),

        new PaymentOutLinesData.Builder().invoiceAmount("-63,483.75")
            .expected("-31,741.87")
            .paid("-31,741.87")
            .build(),

        new PaymentOutPlanData.Builder().expected("-63,483.75")
            .outstanding("0.00")
            .paid("-63,483.75")
            .numberOfPayments("2")
            .build() } };

    return Arrays.asList(data);
  }

  @Test
  public void APRRegression33500Out20Test() {

    logger.info(
        "** Start of test case [APRRegression33500Out20] Return to Vendor - Paid Reversed Purchase Invoice with Payment Plan divided**");

    logger.info("Reversed Purchase Invoice");

    // Create a goods receipt for the product
    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();

    String receiptNo = (String) goodsReceipt.getData().getDataField("documentNo");
    logger.info("** Goods receipt Document No. {} **", receiptNo);

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    // Return to vendor the good receipt
    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(rtvHeaderData);
    returnToVendor.assertSaved();
    String returnToVendorNo = (String) returnToVendor.getData().getDataField("documentNo");
    logger.info("** Return to vendor Document No. {} **", returnToVendorNo);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> rtvPickEdit = returnToVendor
        .openPickAndEditLines();
    rtvPickEdit
        .filter(new ReturnToVendorLineSelectorData.Builder().shipmentNumber(receiptNo).build());
    rtvPickEdit.edit(rtvLineSelectorData);
    rtvPickEdit.process();

    // Book the RTV
    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(rtvHeaderVerificationData);

    // Check the RTV Payment Out Plan
    PaymentOutPlan poutPlan = returnToVendor.new PaymentOutPlan(mainPage);
    poutPlan.assertData(rtvPendingPaymentData);

    // Create the Return To Vendor Shipment from RTV
    ReturnToVendorShipment rtvShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    rtvShipment.create(rtvShipmentHeaderData);
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> rtvShipmentPickEdit = rtvShipment
        .openPickAndEditLines();
    rtvShipmentPickEdit
        .filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(returnToVendorNo).build());
    rtvShipmentPickEdit.process();
    // TODO L00: The following static sleep seems that can be removed. Check it with several
    // executions before removing it
    Sleep.trySleep();

    // Complete the shipment
    rtvShipment.complete();
    rtvShipment.assertProcessCompletedSuccessfully2();
    rtvShipment.assertData(rtvShipmentHeaderVerificationData);

    String rtvShipmentNo = (String) rtvShipment.getData().getDataField("documentNo");
    String rtvShipmentId = String.format("%s - %s - %s",
        (String) rtvShipment.getData().getDataField("documentNo"),
        (String) rtvShipment.getData().getDataField("movementDate"),
        (String) rtvShipment.getData().getDataField("businessPartner"));
    logger.info("** Return to vendor Shipment Document No. {} **", rtvShipmentId);

    // Create the Credit Memo Purchase Invoice from Shipment
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    Sleep.trySleep(5000);
    purchaseInvoice.create(invoiceHeaderData);
    purchaseInvoice.assertSaved();

    String invoiceNo = (String) purchaseInvoice.getTab().getData("documentNo");
    logger.info("** Credit Memo Purchase Invoice Document No. {} **", invoiceNo);

    purchaseInvoice.createLinesFromShipment(rtvShipmentId);
    Lines invoiceLines = purchaseInvoice.new Lines(mainPage);
    invoiceLines.assertCount(1);

    // Complete the invoice
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(invoiceHeaderVerificationData);

    // Check first invoice payment plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentInPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePendingPaymentPlan);

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentPlan2);

    // Pay the credit memo invoice
    AddPaymentProcess paymentProcess = purchaseInvoice.openAddPayment();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> row = (List<Map<String, Object>>) paymentProcess.getOrderInvoiceGrid()
        .getSelectedRows();
    assertTrue("There must be one selected row", row.size() == 2);
    assertTrue(row.get(0).get("salesOrderNo").equals(returnToVendorNo));
    assertTrue(row.get(0).get("invoiceNo").equals(invoiceNo));
    paymentProcess.process("Process Made Payment(s) and Withdrawal");

    // Check the payment out created by the add payment process
    PaymentOut payment = new PaymentOut(mainPage).open();
    String payOutDescription = String.format("Invoice No.: %s\nOrder No.: %s\n", invoiceNo,
        returnToVendorNo);
    logger.info(payOutDescription);
    Sleep.trySleep();
    payment.select(new PaymentOutHeaderData.Builder().amount("-63,483.75")
        .description(String.format("Invoice No.: %s", invoiceNo))
        .build());

    String paymentOutDocument = (String) ((PaymentOutTab) payment.getTab()).getData("documentNo");
    String paymentDate = (String) ((PaymentOutTab) payment.getTab()).getData("paymentDate");
    String paymentBP = (String) ((PaymentOutTab) payment.getTab()).getData("businessPartner");
    String paymentOutId = String.format("%s - %s - %s - -63483.75", paymentOutDocument, paymentDate,
        paymentBP);
    logger.info("** Payment Out {} **", paymentOutId);

    PaymentOut.Lines paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);
    paymentLines.select(new PaymentOutLinesData.Builder().paid("-31,741.88").build());
    paymentLines.assertData((PaymentOutLinesData) paymentLineVerificationData
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("orderPaymentSchedule$order$documentNo", returnToVendorNo));

    paymentLines.closeForm();
    paymentLines.clearFilters();
    mainPage.waitForDataToLoad();
    paymentLines.select(
        new PaymentOutLinesData.Builder().paid("==-31,741.87").invoiceno(invoiceNo).build());
    paymentLines.assertData((PaymentOutLinesData) paymentLineVerificationData2
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("orderPaymentSchedule$order$documentNo", returnToVendorNo));

    // Check RTV Payment out plan
    returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    Sleep.trySleep(5000);
    returnToVendor
        .filter(new ReturnToVendorHeaderData.Builder().documentNo(returnToVendorNo).build());
    PaymentOutPlan rtvPaymentOutPlan = returnToVendor.new PaymentOutPlan(mainPage);
    rtvPaymentOutPlan.assertCount(1);
    rtvPaymentOutPlan.assertData(rtvPaymentData);

    // Check RTV Payment out details
    PaymentOutDetails detailTab = rtvPaymentOutPlan.new PaymentOutDetails(mainPage);
    detailTab.selectWithoutFiltering(0);
    detailTab.assertData(new PaymentOutDetailsData.Builder().payment(paymentOutId).build());

    detailTab.selectWithoutFiltering(0);
    detailTab.assertData(new PaymentOutDetailsData.Builder().payment(paymentOutId).build());

    // Check that the invoice and order is not shown in the waiting payment grid
    payment = new PaymentOut(mainPage).open();
    Sleep.trySleep(5000);
    payment.create(new PaymentOutHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
        .build());
    paymentProcess = payment.addDetailsOpen();
    paymentProcess.setParameterValue("transaction_type", "Orders and Invoices");
    AddPaymentGrid grid = paymentProcess.getOrderInvoiceGrid();
    Sleep.trySleep();
    @SuppressWarnings("unchecked")
    List<Map<String, Object>> gridRows = (List<Map<String, Object>>) grid.getRows();
    for (Map<String, Object> gridRow : gridRows) {
      logger.info(gridRow.get("invoiceNo"));
      assertFalse("Invoice should not appear", gridRow.get("invoiceNo").equals(rtvShipmentNo));
    }

    logger.info(
        "** End of test case [APRRegression33500Out10] Return to Vendor - Paid Reversed Purchase Invoice with Payment Plan divided**");
  }
}
