/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions6;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500Out5 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPendingPaymentOutPlanData;
  AddPaymentPopUpData addPaymentVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  PaymentOutLinesData paymentOutLinesVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2;
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500Out5(PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPendingPaymentOutPlanData,
      AddPaymentPopUpData addPaymentVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      PaymentOutLinesData paymentOutLinesVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData invoicePaymentOutPlanData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData invoicePaymentOutDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData orderPaymentOutPlanData2,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData orderPaymentOutDetailsData2,
      String[][] journalEntryLines3, String[][] journalEntryLines4) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.orderPendingPaymentOutPlanData = orderPendingPaymentOutPlanData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.orderPaymentOutPlanData = orderPaymentOutPlanData;
    this.orderPaymentOutDetailsData = orderPaymentOutDetailsData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.paymentOutLinesVerificationData = paymentOutLinesVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.invoicePaymentOutPlanData = invoicePaymentOutPlanData;
    this.invoicePaymentOutDetailsData = invoicePaymentOutDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentOutPlanData2 = orderPaymentOutPlanData2;
    this.orderPaymentOutDetailsData2 = orderPaymentOutDetailsData2;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),

        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .orderedQuantity("-10")
            .tax("VAT 10% USA")
            .build(),

        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("-20.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("-20.00")
            .grandTotalAmount("-22.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .paid("0.00")
            .outstanding("-22.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("Acc-3 (Payment-Trx-Reconciliation)")
            .fin_financial_account_id("Accounting Documents DOLLAR - USD")
            .c_currency_id("EUR")
            .actual_payment("-22.00")
            .expected_payment("-22.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("-22.00")
            .total("-22.00")
            .difference("0.00")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .paid("-22.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .dueDate(OBDate.CURRENT_DATE)
            .expected("-22.00")
            .paid("-22.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new PaymentOutHeaderData.Builder().organization("USA")
            .documentType("AP Payment")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-22.00")
            .account("Accounting Documents DOLLAR - USD")
            .currency("EUR")
            .status("Withdrawn not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentOutLinesData.Builder().dueDate(OBDate.CURRENT_DATE)
            .invoiceAmount("-22.00")
            .expected("-22.00")
            .paid("-22.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .transactionDocument("Reversed Purchase Invoice")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("-10")
            .lineNetAmount("-20.00")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-22.00")
            .summedLineAmount("-20.00")
            .currency("EUR")
            .paymentComplete(true)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .expectedDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .paid("-22.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData.Builder()
            .paid("-22.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-22.00")
            .account("Accounting Documents DOLLAR - USD")
            .status("Withdrawn not Cleared")
            .build(),

        new String[][] { { "40700", "Anticipos a proveedores", "", "-22.00" },
            { "47200", "Hacienda Pública IVA soportado", "-2.00", "" },
            { "60000", "Compras de mercaderías", "-20.00", "" } },

        new String[][] { { "1410", "Vendor prepayment", "", "-55.00" },
            { "11600", "Tax Receivables", "-5.00", "" },
            { "51200", "Service costs", "-50.00", "" } },

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-22.00")
            .paid("-22.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("1")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentDetailsData.Builder()
            .expected("-22.00")
            .paid("-22.00")
            .writeoff("0.00")
            .status("Withdrawn not Cleared")
            .build(),

        new String[][] { { "40100", "Proveedores efectos comerciales a pagar", "", "-22.00" },
            { "40700", "Anticipos a proveedores", "-22.00", "" } },
        new String[][] { { "11300", "Bank in transit", "", "-55.00" },
            { "1410", "Vendor prepayment", "-55.00", "" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Paid Negative standard order - Negative reversed invoice
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500Out5Test() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33500Out5] Test regression 33500 - Paid Negative standard order - Negative reversed invoice. **");

    // Register a purchase order
    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    String orderNo = purchaseOrder.getData("documentNo").toString();

    // Create order lines
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    // Book the order
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    // Check pending order payment plan
    PurchaseOrder.PaymentOutPlan orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPendingPaymentOutPlanData);

    // Pay the order
    purchaseOrder.addPayment("Accounting Documents DOLLAR - USD",
        "Process Made Payment(s) and Withdrawal", addPaymentVerificationData);
    purchaseOrder.assertPaymentCreatedSuccessfully();
    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Check order payment out plan
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData);

    // Check order payment out details
    PurchaseOrder.PaymentOutPlan.PaymentOutDetails orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(
        mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData);
    String paymentNo = orderPaymentOutDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // Check payment out
    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    PaymentOut.Lines paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo));

    // Create a purchase invoice from order
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check invoice payment out plan
    PurchaseInvoice.PaymentOutPlan invoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.assertData(invoicePaymentOutPlanData);

    // Check invoice payment out details
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaymentOutDetailsData);

    // Post invoice and check post
    purchaseInvoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Purchase order payment plan
    purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(mainPage);
    orderPaymentOutPlan.assertCount(1);
    orderPaymentOutPlan.assertData(orderPaymentOutPlanData2);

    // Check Purchase order payment out details
    orderPaymentOutDetails = orderPaymentOutPlan.new PaymentOutDetails(mainPage);
    orderPaymentOutDetails.assertCount(1);
    orderPaymentOutDetails.select(orderPaymentOutDetailsData2);
    orderPaymentOutDetails.assertData(orderPaymentOutDetailsData2);

    // Check Payment out
    paymentOut = new PaymentOut(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    paymentOut.select(new PaymentOutHeaderData.Builder().documentNo(paymentNo).build());
    paymentOut.assertData(paymentOutHeaderVerificationData);
    paymentOutLines = paymentOut.new Lines(mainPage);
    paymentOutLines.assertCount(1);
    paymentOutLines.assertData((PaymentOutLinesData) paymentOutLinesVerificationData
        .addDataField("orderPaymentSchedule$order$documentNo", orderNo)
        .addDataField("invoicePaymentSchedule$invoice$documentNo", invoiceNo)
        .addDataField("dueDate", OBDate.ADD_DAYS_TO_SYSTEM_DATE_90));

    // Post Payment out and check
    paymentOut.open();
    paymentOut.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines3.length);
    post2.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post2 = new Post(mainPage);
    post2.assertJournalLinesCount(journalEntryLines4.length);
    post2.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [APRRegression33500Out5] Test regression 33500 - Paid Negative standard order - Negative reversed invoice. **");
  }
}
