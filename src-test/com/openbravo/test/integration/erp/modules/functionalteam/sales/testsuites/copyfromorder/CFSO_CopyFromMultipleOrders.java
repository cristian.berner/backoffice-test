/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.copyfromorder;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;

/**
 * Testing Copy From Multiple Orders
 *
 * @author Mark
 *
 */

@RunWith(Parameterized.class)
public class CFSO_CopyFromMultipleOrders extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  // Sales Order Copied from 1
  SalesOrderHeaderData salesOrderHeaderFromData1;
  SalesOrderHeaderData salesOrderHeaderFromVerficationData1;
  SalesOrderLinesData salesOrderFirstLineData1;
  SalesOrderLinesData salesOrderFirstLineVerificationData1;
  SalesOrderLinesData salesOrderSecondLineData1;
  SalesOrderLinesData salesOrderSecondLineVerificationData1;
  SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData1;

  // Sales Order Copied from 2
  SalesOrderHeaderData salesOrderHeaderFromData2;
  SalesOrderHeaderData salesOrderHeaderFromVerficationData2;
  SalesOrderLinesData salesOrderFirstLineData2;
  SalesOrderLinesData salesOrderFirstLineVerificationData2;
  SalesOrderLinesData salesOrderSecondLineData2;
  SalesOrderLinesData salesOrderSecondLineVerificationData2;
  SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData2;

  // Sales Order Copied To
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderFirstLineVerificationData;
  SalesOrderLinesData salesOrderSecondLineVerificationData;
  SalesOrderLinesData salesOrderThirdLineVerificationData;
  SalesOrderLinesData salesOrderFourthLineVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CFSO_CopyFromMultipleOrders(SalesOrderHeaderData salesOrderHeaderFromData1,
      SalesOrderHeaderData salesOrderHeaderFromVerficationData1,
      SalesOrderLinesData salesOrderFirstLineData1,
      SalesOrderLinesData salesOrderFirstLineVerificationData1,
      SalesOrderLinesData salesOrderSecondLineData1,
      SalesOrderLinesData salesOrderSecondLineVerificationData1,
      SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData1,
      SalesOrderHeaderData salesOrderHeaderFromData2,
      SalesOrderHeaderData salesOrderHeaderFromVerficationData2,
      SalesOrderLinesData salesOrderFirstLineData2,
      SalesOrderLinesData salesOrderFirstLineVerificationData2,
      SalesOrderLinesData salesOrderSecondLineData2,
      SalesOrderLinesData salesOrderSecondLineVerificationData2,
      SalesOrderHeaderData bookedSalesOrderHeaderFromVerificationData2,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData,
      SalesOrderLinesData salesOrderFirstLineVerificationData,
      SalesOrderLinesData salesOrderSecondLineVerificationData,
      SalesOrderLinesData salesOrderThirdLineVerificationData,
      SalesOrderLinesData salesOrderFourthLineVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData) {
    this.salesOrderHeaderFromData1 = salesOrderHeaderFromData1;
    this.salesOrderHeaderFromVerficationData1 = salesOrderHeaderFromVerficationData1;
    this.salesOrderFirstLineData1 = salesOrderFirstLineData1;
    this.salesOrderFirstLineVerificationData1 = salesOrderFirstLineVerificationData1;
    this.salesOrderSecondLineData1 = salesOrderSecondLineData1;
    this.salesOrderSecondLineVerificationData1 = salesOrderSecondLineVerificationData1;
    this.bookedSalesOrderHeaderFromVerificationData1 = bookedSalesOrderHeaderFromVerificationData1;

    this.salesOrderHeaderFromData2 = salesOrderHeaderFromData2;
    this.salesOrderHeaderFromVerficationData2 = salesOrderHeaderFromVerficationData2;
    this.salesOrderFirstLineData2 = salesOrderFirstLineData2;
    this.salesOrderFirstLineVerificationData2 = salesOrderFirstLineVerificationData2;
    this.salesOrderSecondLineData2 = salesOrderSecondLineData2;
    this.salesOrderSecondLineVerificationData2 = salesOrderSecondLineVerificationData2;
    this.bookedSalesOrderHeaderFromVerificationData2 = bookedSalesOrderHeaderFromVerificationData2;

    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderFirstLineVerificationData = salesOrderFirstLineVerificationData;
    this.salesOrderSecondLineVerificationData = salesOrderSecondLineVerificationData;
    this.salesOrderThirdLineVerificationData = salesOrderThirdLineVerificationData;
    this.salesOrderFourthLineVerificationData = salesOrderFourthLineVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        // salesOrderHeaderFromData1
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),
        // salesOrderHeaderFromVerficationData1
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),
        // salesOrderFirstLineData1
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("10").tax("Exempt 3%").build(),
        // salesOrderFirstLineVerificationData1
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        // salesOrderSecondLineData1
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGB")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("20").tax("Exempt 3%").build(),
        // salesOrderSecondLineVerificationData1
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("40.00")
            .build(),
        // bookedSalesOrderHeaderFromVerificationData1
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("60.00")
            .currency("EUR")
            .build(),

        // salesOrderHeaderFromData2
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer B").build())
            .paymentMethod("1 (USA)")
            .paymentTerms("30 days, 5")
            .invoiceTerms("Immediate")
            .build(),
        // salesOrderHeaderFromVerficationData2
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº23")
            .warehouse("USA warehouse")
            .paymentTerms("30 days, 5")
            .priceList("Customer B")
            .paymentMethod("1 (USA)")
            .build(),
        // salesOrderFirstLineData2
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer B")
            .build()).orderedQuantity("30").tax("Exempt 3%").build(),
        // salesOrderFirstLineVerificationData2
        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("60.00")
            .build(),
        // salesOrderSecondLineData2
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("LT")
            .priceListVersion("Customer B")
            .build()).orderedQuantity("1").tax("Exempt 10%").build(),
        // salesOrderSecondLineVerificationData2
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("1,250.00")
            .listPrice("1,250.00")
            .lineNetAmount("1,250.00")
            .build(),
        // bookedSalesOrderHeaderFromVerificationData2
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,310.00")
            .grandTotalAmount("1,310.00")
            .currency("EUR")
            .build(),

        // salesOrderHeaderData
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),
        // salesOrderHeaderVerficationData
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("90 days")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("90 days")
            .invoiceTerms("Immediate")
            .build(),
        // salesOrderFirstLineVerificationData
        new SalesOrderLinesData.Builder().orderedQuantity("10")
            .tax("Exempt 3%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        // salesOrderSecondLineVerificationData
        new SalesOrderLinesData.Builder().orderedQuantity("20")
            .tax("Exempt 3%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("40.00")
            .build(),
        // salesOrderThirdLineVerificationData
        new SalesOrderLinesData.Builder().orderedQuantity("30")
            .tax("Exempt 3%")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("60.00")
            .build(),
        // salesOrderFourthLineVerificationData
        new SalesOrderLinesData.Builder().orderedQuantity("1")
            .tax("Exempt 10%")
            .uOM("Unit")
            .unitPrice("1,300.00")
            .listPrice("1,300.00")
            .lineNetAmount("1,300.00")
            .build(),

        // bookedSalesOrderHeaderVerificationData
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("1,420.00")
            .grandTotalAmount("1,420.00")
            .currency("EUR")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test copy lines from multiple orders
   */
  @Test
  public void CFSO_CopyFromMultipleOrdersTest() {
    logger.info(
        "** Start of test case [CFSO_CopyFromMultipleOrders]. Copy lines from multiple orders **");

    /** Register the first Sales order will be copied from */
    SalesOrder orderFrom1 = new SalesOrder(mainPage).open();
    orderFrom1.create(salesOrderHeaderFromData1);
    orderFrom1.assertSaved();
    orderFrom1.assertData(salesOrderHeaderFromVerficationData1);
    String orderNoFrom1 = orderFrom1.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLinesFrom = orderFrom1.new Lines(mainPage);
    orderLinesFrom.create(salesOrderFirstLineData1);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(salesOrderFirstLineVerificationData1);

    orderLinesFrom.create(salesOrderSecondLineData1);
    orderLinesFrom.assertSaved();
    orderLinesFrom.assertData(salesOrderSecondLineVerificationData1);

    // Book the order
    orderFrom1.book();
    orderFrom1.assertProcessCompletedSuccessfully2();
    orderFrom1.assertData(bookedSalesOrderHeaderFromVerificationData1);

    mainPage.closeAllViews();

    /** Register the second Sales order will be copied from */
    SalesOrder orderFrom2 = new SalesOrder(mainPage).open();
    orderFrom2.create(salesOrderHeaderFromData2);
    orderFrom2.assertSaved();
    orderFrom2.assertData(salesOrderHeaderFromVerficationData2);
    String orderNoFrom2 = orderFrom2.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLinesFrom2 = orderFrom2.new Lines(mainPage);
    orderLinesFrom2.create(salesOrderFirstLineData2);
    orderLinesFrom2.assertSaved();
    orderLinesFrom2.assertData(salesOrderFirstLineVerificationData2);

    orderLinesFrom2.create(salesOrderSecondLineData2);
    orderLinesFrom2.assertSaved();
    orderLinesFrom2.assertData(salesOrderSecondLineVerificationData2);

    // Book the order
    orderFrom2.book();
    orderFrom2.assertProcessCompletedSuccessfully2();
    orderFrom2.assertData(bookedSalesOrderHeaderFromVerificationData2);

    /** Register a Sales order and copy from the previously order */
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);

    PickAndExecuteWindow<SalesOrderHeaderData> popup = order.copyFromOrders();
    popup.filterAndKeepSelection(
        new SalesOrderHeaderData.Builder().documentNo(orderNoFrom1).build());
    popup.filterAndKeepSelection(
        new SalesOrderHeaderData.Builder().documentNo(orderNoFrom2).build());
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 2);
    popup.process();

    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.assertCount(4);
    orderLines.selectWithoutFiltering(0);
    orderLines.assertData(salesOrderFirstLineVerificationData);
    orderLines.selectWithoutFiltering(1);
    orderLines.assertData(salesOrderSecondLineVerificationData);
    orderLines.selectWithoutFiltering(2);
    orderLines.assertData(salesOrderThirdLineVerificationData);
    orderLines.selectWithoutFiltering(3);
    orderLines.assertData(salesOrderFourthLineVerificationData);

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    logger.info(
        "** End of test case [CFSO_CopyFromMultipleOrders]. Copy lines from multiple orders**");
  }
}
