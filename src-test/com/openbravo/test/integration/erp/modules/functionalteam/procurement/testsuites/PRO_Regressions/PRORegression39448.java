/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.procurement.transactions.goodsreceipt.CreateLinesFromData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test regression 39448
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class PRORegression39448 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PreferenceData aumPreferenceData;
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData1;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData1;
  PurchaseOrderLinesData purchaseOrderLinesData2;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData2;
  PurchaseOrderLinesData purchaseOrderLinesData3;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData3;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData1;
  GoodsReceiptLinesData goodsReceiptLineVerificationData2;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public PRORegression39448(PreferenceData aumPreferenceData,
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData1,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData1,
      PurchaseOrderLinesData purchaseOrderLinesData2,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData2,
      PurchaseOrderLinesData purchaseOrderLinesData3,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData3,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData1,
      GoodsReceiptLinesData goodsReceiptLineVerificationData2,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData) {

    this.aumPreferenceData = aumPreferenceData;
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData1 = purchaseOrderLinesData1;
    this.purchaseOrderLinesVerificationData1 = purchaseOrderLinesVerificationData1;
    this.purchaseOrderLinesData2 = purchaseOrderLinesData2;
    this.purchaseOrderLinesVerificationData2 = purchaseOrderLinesVerificationData2;
    this.purchaseOrderLinesData3 = purchaseOrderLinesData3;
    this.purchaseOrderLinesVerificationData3 = purchaseOrderLinesVerificationData3;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLineVerificationData1 = goodsReceiptLineVerificationData1;
    this.goodsReceiptLineVerificationData2 = goodsReceiptLineVerificationData2;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PreferenceData.Builder().property("Enable UOM Management")
            .propertyList(Boolean.TRUE)
            .value("Y")
            .visibleAtUser("")
            .build(),
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .paymentTerms("90 days")
            .priceList("Purchase")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .operativeQuantity("10")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("10")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMB").priceListVersion("Purchase").build())
            .operativeQuantity("10")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("10")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMC").priceListVersion("Purchase").build())
            .operativeQuantity("10")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("10")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),
        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("66.00")
            .currency("EUR")
            .build(),
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentType("MM Receipt")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptLinesData.Builder().lineNo("10")
            .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
            .movementQuantity("10")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .businessPartner("Vendor A")
            .build(),
        new GoodsReceiptLinesData.Builder().lineNo("20")
            .product(new ProductCompleteSelectorData.Builder().name("Raw material B").build())
            .movementQuantity("8")
            .uOM("Bag")
            .storageBin("L01")
            .organization("Spain")
            .businessPartner("Vendor A")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 39448
   *
   * @throws ParseException
   */
  @Test
  public void PRORegression39448Test() throws ParseException {
    logger.info("** Start of test case [PRORegression39448] Test regression 39448. **");

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Activate AUM Preference
    Preference.PreferenceTab.create(mainPage, aumPreferenceData);

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData1);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData1);
    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData2);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData2);
    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData3);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData3);
    // TODO L2: Adjust as much as possible the following static sleep
    Sleep.trySleep(4000);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    DataObject completedPurchaseOrderHeaderData = purchaseOrder.getData();
    String documentNo = (String) completedPurchaseOrderHeaderData.getDataField("documentNo");
    String purchaseOrderIdentifier = String.format("%s - %s - %s", documentNo,
        completedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();

    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);

    goodsReceipt.createLinesFrom(new CreateLinesFromData.Builder().warehouse("L01")
        .purchaseOrder(purchaseOrderIdentifier)
        .attribute(documentNo)
        .lineNumber(1)
        .build());
    goodsReceipt.assertProcessCompletedSuccessfully();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData1);
    goodsReceipt.createLinesFrom(new CreateLinesFromData.Builder().warehouse("L01")
        .purchaseOrder(purchaseOrderIdentifier)
        .operativeQuantity("8")
        .attribute(documentNo)
        .lineNumber(2)
        .build());
    goodsReceiptLines.assertCount(2);
    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData1);
    goodsReceiptLines.selectWithoutFiltering(1);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData2);
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Remove AUM Preference
    Preference.PreferenceTab.delete(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build());

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    logger.info("** End of test case [PRORegression39448] Test regression 39448. **");
  }
}
