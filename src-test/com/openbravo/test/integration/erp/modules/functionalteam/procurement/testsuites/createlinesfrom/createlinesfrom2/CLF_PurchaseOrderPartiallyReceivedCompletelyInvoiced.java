/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.createlinesfrom.createlinesfrom2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;

/**
 * Test Create Lines From Purchase Order Partially Received and Completely Invoiced in two Invoices
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_PurchaseOrderPartiallyReceivedCompletelyInvoiced extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PurchaseOrderHeaderData purchaseOrderHeaderData;
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  PurchaseOrderLinesData purchaseOrderLinesData;
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;

  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_PurchaseOrderPartiallyReceivedCompletelyInvoiced(
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoice2LineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoice2HeaderVerificationData) {
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptLineVerificationData = goodsReceiptLineVerificationData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoice2LineVerificationData = purchaseInvoice2LineVerificationData;
    this.completedPurchaseInvoice2HeaderVerificationData = completedPurchaseInvoice2HeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> purchaseOrderValues() {
    Object[][] data = new Object[][] { {
        new PurchaseOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Purchase Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseOrderLinesData.Builder()
            .product(new ProductSelectorData.Builder().searchKey("RMA")
                .priceListVersion("Purchase")
                .warehouse("Spain warehouse")
                .build())
            .orderedQuantity("10")
            .tax("VAT 10%")
            .build(),
        new PurchaseOrderLinesData.Builder().uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("20.00")
            .build(),

        new PurchaseOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("20.00")
            .grandTotalAmount("22.00")
            .currency("EUR")
            .build(),

        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentType("MM Receipt")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
            .movementQuantity("6")
            .uOM("Bag")
            .storageBin("L01")
            .attributeSetValue("#7890")
            .organization("Spain")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("6")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("12.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("13.20")
            .summedLineAmount("12.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("4")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("8.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("8.80")
            .summedLineAmount("8.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test Create Lines From Purchase Order Partially Receipt and Completely Invoiced in two Invoices
   *
   * @throws ParseException
   */
  @Test
  public void CLF_PurchaseOrderPartiallyReceivedCompletelyInvoicedTest() throws ParseException {
    logger.info(
        "** Start of test case [CLF_PurchaseOrderPartiallyReceivedCompletelyInvoiced] Test Create Lines From Purchase Order Partially Receipt and Completely Invoiced in two Invoices**");

    PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);

    String orderNo = purchaseOrder.getData("documentNo").toString();
    bookedPurchaseOrderHeaderVerificationData.addDataField("documentNo", orderNo);

    PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    String purchaseOrderIdentifier = String.format("%s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    String purchaseOrderLineIdentifier1 = String.format("%s - %s - %s - %s - %s", orderNo,
        purchaseOrder.getData("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"), "10",
        purchaseOrderLinesVerificationData.getDataField("lineNetAmount"));

    // Partially Deliver the order
    completedGoodsReceiptHeaderVerificationData.addDataField("salesOrder", purchaseOrderIdentifier);
    goodsReceiptLineVerificationData.addDataField("salesOrderLine", purchaseOrderLineIdentifier1);

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerificationData);
    goodsReceipt.createLinesFrom("L01", purchaseOrderIdentifier, "7890");
    goodsReceipt.assertProcessCompletedSuccessfully();

    String goodsReceiptNo = goodsReceipt.getData("documentNo").toString();
    completedGoodsReceiptHeaderVerificationData.addDataField("documentNo", goodsReceiptNo);

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.edit(new GoodsReceiptLinesData.Builder().movementQuantity("6").build());
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData);

    String goodsReceiptLineIdentifier = String.format("%s - %s - %s - %s - %s - %s", goodsReceiptNo,
        goodsReceipt.getData("movementDate"), "Vendor A", goodsReceiptLines.getData("lineNo"),
        "Raw material A", "6");

    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    // Check order partially delivered
    purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.selectWithoutFiltering(0);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    // Invoice the order
    completedPurchaseInvoiceHeaderVerificationData.addDataField("salesOrder",
        purchaseOrderIdentifier);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();

    purchaseInvoiceLineVerificationData.addDataField("salesOrderLine",
        purchaseOrderLineIdentifier1);
    purchaseInvoiceLineVerificationData.addDataField("goodsShipmentLine",
        goodsReceiptLineIdentifier);

    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.selectWithoutFiltering(0);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check partially invoiced and partially delivered order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.selectWithoutFiltering(0);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    // Check invoiced shipment
    goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder().documentNo(goodsReceiptNo).build());
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData);

    // Create a new purchase invoice from order
    purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();

    purchaseInvoiceLineVerificationData.addDataField("salesOrderLine",
        purchaseOrderLineIdentifier1);
    purchaseInvoiceLineVerificationData.addDataField("goodsShipmentLine", "&nbsp;");

    purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);
    purchaseInvoiceLine.selectWithoutFiltering(0);
    purchaseInvoiceLine.assertData(purchaseInvoice2LineVerificationData);

    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoice2HeaderVerificationData);

    // Check invoiced and partially delivered order
    purchaseOrder = new PurchaseOrder(mainPage).open();
    purchaseOrder.select(new PurchaseOrderHeaderData.Builder().documentNo(orderNo).build());
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);

    purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.selectWithoutFiltering(0);
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);

    // Check invoiced shipment
    goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder().documentNo(goodsReceiptNo).build());
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);

    goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.selectWithoutFiltering(0);
    goodsReceiptLines.assertData(goodsReceiptLineVerificationData);

    logger.info(
        "** End of test case [CLF_PurchaseOrderPartiallyReceivedCompletelyInvoiced] Test Create Lines From Purchase Order Partially Receipt and Completely Invoiced in two Invoices**");
  }
}
