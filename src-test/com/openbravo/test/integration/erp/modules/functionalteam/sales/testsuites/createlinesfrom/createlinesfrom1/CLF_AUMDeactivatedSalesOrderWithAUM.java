/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.createlinesfrom.createlinesfrom1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.data.masterdata.product.AUMData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test with AUM preference deactivated Create Lines From Sales Order created with AUM preference
 * activated
 *
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_AUMDeactivatedSalesOrderWithAUM extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  PreferenceData aumPreferenceData;
  ProductData selectProduct;
  AUMData createAUM;

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_AUMDeactivatedSalesOrderWithAUM(PreferenceData aumPreferenceData,
      ProductData selectProduct, AUMData createAUM, SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData) {
    this.aumPreferenceData = aumPreferenceData;
    this.selectProduct = selectProduct;
    this.createAUM = createAUM;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new PreferenceData.Builder().property("Enable UOM Management")
            .propertyList(Boolean.TRUE)
            .value("Y")
            .visibleAtUser("")
            .build(),

        new ProductData.Builder().name("Final good A").build(),

        new AUMData.Builder().uOM("Unit")
            .conversionRate("0.5")
            .sales("Primary")
            .purchase("Primary")
            .logistics("Primary")
            .gtin("1234567890")
            .build(),

        new SalesOrderHeaderData.Builder().organization("Spain")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .invoiceTerms("Immediate")
            .build(),
        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .warehouse("Spain warehouse")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGA")
            .priceListVersion("Customer A")
            .warehouse("Spain warehouse")
            .build()).operativeQuantity("200").operativeUOM("Unit").tax("VAT 10%").build(),
        new SalesOrderLinesData.Builder().uOM("Bag")
            .orderedQuantity("100")
            .unitPrice("2.00")
            .listPrice("2.00")
            .lineNetAmount("200.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("200.00")
            .grandTotalAmount("220.00")
            .currency("EUR")
            .build(),

        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new SalesInvoiceHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder().lineNo("10")
            .product(new ProductSimpleSelectorData.Builder().productName("Final good A").build())
            .invoicedQuantity("100")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("200.00")
            .organization("Spain")
            .build(),
        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("220.00")
            .summedLineAmount("200.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test with AUM preference deactivated Create Lines From Sales Order created with AUM preference
   * activated
   *
   * @throws ParseException
   */
  @Test
  public void CLF_AUMDeactivatedSalesOrderWithAUMTest() throws ParseException {
    logger.info(
        "** Start of test case [CLF_AUMDeactivatedSalesOrderWithAUMTest] Test with AUM preference deactivated Create Lines From Sales Order created with AUM preference activated **");

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Activate AUM Preference
    Preference.PreferenceTab.create(mainPage, aumPreferenceData);

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(selectProduct);

    AUMTab aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);

    String orderNo = salesOrder.getData("documentNo").toString();

    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    String salesOrderLineIdentifier = String.format("%s - %s - %s - %s - %s", orderNo,
        salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"), "10",
        bookedSalesOrderHeaderVerificationData.getDataField("summedLineAmount"));

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Remove AUM Preference
    Preference.PreferenceTab.delete(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build());

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    // Invoice the order
    completedSalesInvoiceHeaderVerificationData.addDataField("salesOrder", salesOrderIdentifier);
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    salesInvoice.createLinesFrom(salesOrderIdentifier);
    salesInvoice.assertProcessCompletedSuccessfully();

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.assertCount(1);

    salesInvoiceLineVerificationData.addDataField("salesOrderLine", salesOrderLineIdentifier);
    salesInvoiceLine.assertData(salesInvoiceLineVerificationData);

    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check invoiced sales order
    bookedSalesOrderHeaderVerificationData.addDataField("invoiceStatus", "100 %");
    salesOrder = new SalesOrder(mainPage).open();
    salesOrder.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());

    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);

    salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLinesVerificationData.addDataField("invoicedQuantity", "100");
    salesOrderLines.assertData(salesOrderLinesVerificationData);

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Activate AUM Preference
    Preference.PreferenceTab.create(mainPage, aumPreferenceData);

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProduct);

    aumTab = productWindow.selectAUMTab();
    aumTab.filter(new AUMData.Builder().uOM("Unit").build());
    aumTab.deleteOnGrid();

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Remove AUM Preference
    Preference.PreferenceTab.delete(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build());

    logger.info(
        "** End of test case [CLF_AUMDeactivatedSalesOrderWithAUMTest] Test with AUM preference deactivated Create Lines From Sales Order created with AUM preference activated **");
  }
}
