/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * Contributor(s):
 *
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorLineData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test RTVc010 Invoice a Return to Vendor Shipment
 *
 * @author Rafael Queralta
 *
 */
@RunWith(Parameterized.class)
public class RTVc010InvoiceAReturnToVendorShipment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;

  ReturnToVendorHeaderData returnToVendorHeaderData;
  ReturnToVendorHeaderData returnToVendorHeaderVerificationData;
  ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData;
  ReturnToVendorLineData returnToVendorLineVerificationData;

  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData;
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData;
  ReturnToVendorShipmentLineData returnToVendorShipmentLineData;
  ReturnToVendorShipmentHeaderData completeReturnToVendorShipmentVerificationData;

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;

  public RTVc010InvoiceAReturnToVendorShipment(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData returnToVendorHeaderData,
      ReturnToVendorHeaderData returnToVendorHeaderVerificationData,
      ReturnToVendorHeaderData bookedReturnToVendorHeaderVerificationData,
      ReturnToVendorLineData returnToVendorLineVerificationData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData,
      ReturnToVendorShipmentHeaderData completeReturnToVendorShipmentVerificationData,
      ReturnToVendorShipmentLineData returnToVendorShipmentLineData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineShipmentVerificationData) {
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.returnToVendorHeaderData = returnToVendorHeaderData;
    this.returnToVendorHeaderVerificationData = returnToVendorHeaderVerificationData;
    this.bookedReturnToVendorHeaderVerificationData = bookedReturnToVendorHeaderVerificationData;
    this.returnToVendorLineVerificationData = returnToVendorLineVerificationData;
    this.returnToVendorShipmentHeaderData = returnToVendorShipmentHeaderData;
    this.returnToVendorShipmentHeaderVerificationData = returnToVendorShipmentHeaderVerificationData;
    this.completeReturnToVendorShipmentVerificationData = completeReturnToVendorShipmentVerificationData;
    this.returnToVendorShipmentLineData = returnToVendorShipmentLineData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineShipmentVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Volley Ball").build())
            .movementQuantity("10")
            .storageBin("B01")
            .build(),
        new ReturnToVendorHeaderData.Builder().organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .orderReference("2233")
            .build(),
        new ReturnToVendorHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("RTV Order")
            .build(),
        new ReturnToVendorHeaderData.Builder().documentStatus("Booked").build(),
        new ReturnToVendorLineData.Builder().lineNo("10")
            .product("Volley Ball")
            .orderedQuantity("3")
            .uOM("Unit")
            .unitPrice("28.50")
            .lineNetAmount("85.50")
            .tax("VAT 10%")
            .returnReason("Defective")
            .build(),

        new ReturnToVendorShipmentHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .orderReference("2233")
            .warehouse("Spain warehouse")
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .documentType("RTV Shipment")
            .warehouse("Spain warehouse")
            .documentStatus("Draft")
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().documentStatus("Completed").build(),
        new ReturnToVendorShipmentLineData.Builder().lineNo("10")
            .product("Volley Ball")
            .movementQuantity("3")
            .storageBin("B01")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("AP Invoice")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .transactionDocument("AP Invoice")
            .documentStatus("Completed")
            .grandTotalAmount("-94.05")
            .currency("EUR")
            .paymentComplete(false)
            .summedLineAmount("-85.50")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10")
            .product(new ProductSimpleSelectorData.Builder().productName("Volley Ball").build())
            .invoicedQuantity("-3")
            .uOM("Unit")
            .unitPrice("28.50")
            .lineNetAmount("-85.50")
            .tax("VAT 10%")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void RTVc010test() throws ParseException {
    logger.debug("** Start test case [RTVc010] Invoice a Return to Vendor Shipment **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();

    String receiptDocumentNo = (String) goodsReceipt.getData("documentNo");
    String receiptLineIdentifier = String.format("%s - %s - %s - %s", receiptDocumentNo,
        goodsReceipt.getData("movementDate").toString(),
        goodsReceipt.getData("businessPartner").toString(), "10 - Volley Ball - 10");

    final ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(returnToVendorHeaderData);
    returnToVendor.assertSaved();
    returnToVendor.assertData(returnToVendorHeaderVerificationData);
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(new ReturnToVendorLineSelectorData.Builder().product("Volley Ball")
        .shipmentNumber(receiptDocumentNo)
        .build());
    popup.edit(new ReturnToVendorLineSelectorData.Builder().returned("3")
        .returnReason("Defective")
        .build());
    popup.process();

    returnToVendorLineVerificationData.addDataField("goodsShipmentLine", receiptLineIdentifier);
    ReturnToVendor.Lines returnToVendorLines = returnToVendor.new Lines(mainPage);
    returnToVendorLines.assertData(returnToVendorLineVerificationData);

    returnToVendor.book();
    returnToVendor.assertProcessCompletedSuccessfully2();
    returnToVendor.assertData(bookedReturnToVendorHeaderVerificationData);

    String rtvDocumentNo = (String) returnToVendor.getData("documentNo");
    String rtvIdentifier = String.format("%s - %s - -%s", rtvDocumentNo,
        returnToVendor.getData("orderDate").toString(),
        returnToVendor.getData("grandTotalAmount").toString());

    final ReturnToVendorShipment returnToVendorShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendorShipment.create(returnToVendorShipmentHeaderData);
    returnToVendorShipment.assertSaved();
    returnToVendorShipment.assertData(returnToVendorShipmentHeaderVerificationData);

    /** Create a line **/
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = returnToVendorShipment
        .openPickAndEditLines();
    popUp.filter(new ReturnToVendorShipmentLineData.Builder().rMOrderNo(rtvDocumentNo)
        .returned("03")
        .uOM("Unit")
        .storageBin("B01")
        .lineNo(">0")
        .product("Volley Ball")
        .build());
    popUp.process();

    ReturnToVendorShipment.Lines returnToVendorShipmentLines = returnToVendorShipment.new Lines(
        mainPage);
    returnToVendorShipmentLines.assertData(returnToVendorShipmentLineData);

    returnToVendorShipment.complete();
    returnToVendorShipment.assertProcessCompletedSuccessfully2();
    returnToVendorShipment.assertData(completeReturnToVendorShipmentVerificationData);

    String shipmentIdentifier = String.format("%s - %s - %s",
        returnToVendorShipment.getData("documentNo").toString(),
        returnToVendorShipment.getData("movementDate").toString(),
        returnToVendorShipment.getData("businessPartner").toString());

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();

    purchaseInvoice.createLinesFrom(rtvIdentifier);

    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoiceLines.getTab().closeForm();
    purchaseInvoiceLines.getTab().deleteOnGrid();

    purchaseInvoice.createLinesFromShipment(shipmentIdentifier);
    purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();

    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);

    logger.debug("** End test case [RTVc010] Invoice a Return to Vendor Shipment **");
  }
}
