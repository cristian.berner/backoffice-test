/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.createlinesfrom.createlinesfrom1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.generalsetup.application.preference.PreferenceData;
import com.openbravo.test.integration.erp.data.masterdata.product.AUMData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.masterdata.product.AUMTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductTab;
import com.openbravo.test.integration.erp.gui.masterdata.product.ProductWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.preference.Preference;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Test with AUM preference activated Create Lines From Goods Receipt created without AUM preference
 * activated
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class CLF_AUMActivatedGoodsReceiptWithoutAUM extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerficationData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  GoodsReceiptHeaderData bookedGoodsReceiptHeaderVerificationData;

  PreferenceData aumPreferenceData;
  ProductData selectProduct;
  AUMData createAUM;

  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;

  /**
   * Class constructor.
   *
   */
  public CLF_AUMActivatedGoodsReceiptWithoutAUM(GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerficationData,
      GoodsReceiptLinesData goodsReceiptLinesData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      GoodsReceiptHeaderData bookedGoodsReceiptHeaderVerificationData,
      PreferenceData aumPreferenceData, ProductData selectProduct, AUMData createAUM,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData) {
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerficationData = goodsReceiptHeaderVerficationData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.bookedGoodsReceiptHeaderVerificationData = bookedGoodsReceiptHeaderVerificationData;
    this.aumPreferenceData = aumPreferenceData;
    this.selectProduct = selectProduct;
    this.createAUM = createAUM;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> goodsReceiptValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptHeaderData.Builder().partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().searchKey("RMA").build())
            .movementQuantity("100")
            .storageBin("L01")
            .attributeSetValue("789")
            .build(),
        new GoodsReceiptLinesData.Builder().movementQuantity("100")
            .storageBin("L01")
            .attributeSetValue("#789")
            .uOM("Bag")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new PreferenceData.Builder().property("Enable UOM Management")
            .propertyList(Boolean.TRUE)
            .value("Y")
            .visibleAtUser("")
            .build(),

        new ProductData.Builder().name("Raw Material A").build(),

        new AUMData.Builder().uOM("Unit")
            .conversionRate("2")
            .sales("Primary")
            .purchase("Primary")
            .logistics("Primary")
            .gtin("1234567890")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("1 (Spain)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList("Purchase")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10")
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A").build())
            .invoicedQuantity("100")
            .operativeQuantity("50")
            .operativeUOM("Unit")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("200.00")
            .organization("Spain")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("220.00")
            .summedLineAmount("200.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test with AUM preference activated Create Lines From Goods Receipt created without AUM
   * preference activated
   *
   * @throws ParseException
   */
  @Test
  public void CLF_AUMActivatedGoodsReceiptWithoutAUMTest() throws ParseException {
    logger.info(
        "** End of test case [CLF_AUMActivatedGoodsReceiptWithoutAUMTest] Test with AUM preference activated Create Lines From Goods Receipt created without AUM preference activated **");

    GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    goodsReceipt.assertData(goodsReceiptHeaderVerficationData);

    String goodsReceiptNo = goodsReceipt.getData("documentNo").toString();

    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLinesData);
    goodsReceiptLines.assertSaved();
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);

    goodsReceipt.complete();
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(bookedGoodsReceiptHeaderVerificationData);

    String goodsReceiptIdentifier = String.format("%s - %s - %s", goodsReceiptNo,
        goodsReceipt.getData("movementDate"), "Vendor A");

    String goodsReceiptLineIdentifier = String.format("%s - %s - %s - %s - %s - %s", goodsReceiptNo,
        goodsReceipt.getData("movementDate"), "Vendor A", goodsReceiptLines.getData("lineNo"),
        "Raw material A", "100");

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Activate AUM Preference
    Preference.PreferenceTab.create(mainPage, aumPreferenceData);

    // Login as QAAdmin user
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
    login();

    ProductWindow productWindow = Product.ProductTab.open(mainPage);
    ProductTab productTab = productWindow.selectProductTab();
    productTab.filter(selectProduct);

    AUMTab aumTab = productWindow.selectAUMTab();
    aumTab.createRecord(createAUM);

    // Create a line from Good Receipt
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFromShipment(goodsReceiptIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();

    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.assertCount(1);

    purchaseInvoiceLineVerificationData.addDataField("goodsShipmentLine",
        goodsReceiptLineIdentifier);
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);

    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    // Check invoiced receipt
    goodsReceipt = new GoodsReceipt(mainPage).open();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder().documentNo(goodsReceiptNo).build());

    goodsReceipt.assertData(bookedGoodsReceiptHeaderVerificationData);

    goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);

    productWindow = Product.ProductTab.open(mainPage);
    productTab = productWindow.selectProductTab();
    productTab.filter(selectProduct);

    aumTab = productWindow.selectAUMTab();
    aumTab.filter(new AUMData.Builder().uOM("Unit").build());
    aumTab.deleteOnGrid();

    // Login as System admin
    logInData = new LogInData.Builder()
        .userName(ConfigurationProperties.INSTANCE.getSystemAdministratorUser())
        .password(ConfigurationProperties.INSTANCE.getSystemAdministratorPassword())
        .build();
    login();
    mainPage.closeAllViews();
    mainPage.changeProfile(new ProfileData.Builder().role("System Administrator - System").build());

    // Remove AUM Preference
    Preference.PreferenceTab.delete(mainPage,
        new PreferenceData.Builder().property("Enable UOM Management")
            .active(Boolean.TRUE)
            .build());

    logger.info(
        "** End of test case [CLF_AUMActivatedGoodsReceiptWithoutAUMTest] Test with AUM preference activated Create Lines From Goods Receipt created without AUM preference activated **");
  }
}
