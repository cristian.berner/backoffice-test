/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions8;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test regression 25312
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression25312In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentVerificationHeaderData;
  GoodsShipmentLinesData goodsShipmentLinesVerificationData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2;
  com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2;
  PaymentInHeaderData paymentInHeaderVerificationData;
  PaymentInLinesData paymentInLinesInvoicedVerificationData;
  String[][] journalEntryLines3;
  String[][] journalEntryLines4;

  /**
   * Class constructor.
   *
   */
  public APRRegression25312In(SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData,
      GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentVerificationHeaderData,
      GoodsShipmentLinesData goodsShipmentLinesData, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePendingPaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData invoicePaymentInDetailsData,
      String[][] journalEntryLines, String[][] journalEntryLines2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData orderPaymentInPlanData2,
      com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData orderPaymentInDetailsData2,
      PaymentInHeaderData paymentInHeaderVerificationData,
      PaymentInLinesData paymentInLinesInvoicedVerificationData, String[][] journalEntryLines3,
      String[][] journalEntryLines4) {
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.orderPaymentInPlanData = orderPaymentInPlanData;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentVerificationHeaderData = goodsShipmentVerificationHeaderData;
    this.goodsShipmentLinesVerificationData = goodsShipmentLinesData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.invoicePendingPaymentInPlanData = invoicePendingPaymentInPlanData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.invoicePaymentInDetailsData = invoicePaymentInDetailsData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    this.orderPaymentInPlanData2 = orderPaymentInPlanData2;
    this.orderPaymentInDetailsData2 = orderPaymentInDetailsData2;
    this.paymentInHeaderVerificationData = paymentInHeaderVerificationData;
    this.paymentInLinesInvoicedVerificationData = paymentInLinesInvoicedVerificationData;
    this.journalEntryLines3 = journalEntryLines3;
    this.journalEntryLines4 = journalEntryLines4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesOrderHeaderData.Builder().organization("USA")
            .transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .paymentTerms("30-60")
            .invoiceTerms("After Delivery")
            .build(),

        new SalesOrderHeaderData.Builder().partnerAddress(".Pamplona, Street Customer center nº1")
            .warehouse("USA warehouse")
            .paymentTerms("30-60")
            .priceList("Customer A")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .invoiceTerms("After Delivery")
            .build(),

        new SalesOrderLinesData.Builder().product(new ProductSelectorData.Builder().searchKey("FGC")
            .priceListVersion("Customer A")
            .build()).orderedQuantity("-1").tax("Exempt").unitPrice("360").build(),

        new SalesOrderLinesData.Builder().uOM("Bag")
            .unitPrice("360.00")
            .listPrice("2.00")
            .lineNetAmount("-360.00")
            .build(),

        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("-360.00")
            .grandTotalAmount("-360.00")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-360.00")
            .received("0.00")
            .outstanding("-360.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),

        new GoodsShipmentHeaderData.Builder().organization("USA")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .movementDate(OBDate.CURRENT_DATE)
            .build(),

        new GoodsShipmentLinesData.Builder().movementQuantity("-1").storageBin("USA111").build(),

        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Invoice")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30-60")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("-1").lineNetAmount("-360.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("-360.00")
            .summedLineAmount("-360.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-180.00")
            .received("0.00")
            .outstanding("-180.00")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-180.00")
            .received("-180.00")
            .outstanding("0.00")
            .currency("EUR")
            .lastPaymentDate(OBDate.CURRENT_DATE)
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentDetailsData.Builder()
            .received("-180.00")
            .writeoff("0.00")
            .paymentDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-180.00")
            .status("Deposited not Cleared")
            .build(),

        new String[][] { { "70000", "Ventas de mercaderías", "", "-360.00" },
            { "43000", "Clientes (euros)", "-360.00", "" }, },

        new String[][] { { "41100", "Sales", "", "-900.00" },
            { "11400", "Accounts receivable", "-900.00", "" }, },

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentPlanData.Builder()
            .dueDate(OBDate.CURRENT_DATE)
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-360.00")
            .received("-360.00")
            .outstanding("0.00")
            .lastPayment(OBDate.CURRENT_DATE)
            .numberOfPayments("2")
            .currency("EUR")
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesorder.PaymentDetailsData.Builder()
            .paymentDate(OBDate.CURRENT_DATE)
            .expected("-180.00")
            .received("-180.00")
            .writeoff("0.00")
            .status("Deposited not Cleared")
            .build(),

        new PaymentInHeaderData.Builder().organization("USA")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .amount("-360.00")
            .currency("EUR")
            .status("Deposited not Cleared")
            .generatedCredit("0.00")
            .usedCredit("0.00")
            .build(),

        new PaymentInLinesData.Builder().invoiceAmount("-360.00")
            .expected("-180.00")
            .received("-180.00")
            .invoiceno("")
            .glitemname("")
            .build(),

        new String[][] { { "43000", "Clientes (euros)", "", "-360.00" },
            { "43100", "Efectos comerciales en cartera", "-360.00", "" }, },

        new String[][] { { "11400", "Accounts receivable", "", "-900.00" },
            { "11300", "Bank in transit", "-900.00", "" } } } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 25312
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression25312InTest() throws ParseException {
    logger.info("** Start of test case [APRRegression25312In] Test regression 25312**");

    // Register a Sales order
    SalesOrder order = new SalesOrder(mainPage).open();
    order.create(salesOrderHeaderData);
    order.assertSaved();
    order.assertData(salesOrderHeaderVerficationData);
    String orderNo = order.getData("documentNo").toString();

    // Create order lines
    SalesOrder.Lines orderLines = order.new Lines(mainPage);
    orderLines.create(salesOrderLinesData);
    orderLines.assertSaved();
    orderLines.assertData(salesOrderLinesVerificationData);
    String salesOrderIdentifier = String.format("%s - %s - %s", orderNo, order.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));

    // Book the order
    order.book();
    order.assertProcessCompletedSuccessfully2();
    order.assertData(bookedSalesOrderHeaderVerificationData);

    // Check pending order payment plan
    SalesOrder.PaymentInPlan orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData);

    // Ship the product units
    GoodsShipment shipment = new GoodsShipment(mainPage).open();
    shipment.create(goodsShipmentHeaderData);
    shipment.assertSaved();
    shipment.assertData(goodsShipmentVerificationHeaderData);

    String shipmentNo = (String) shipment.getData().getDataField("documentNo");
    logger.info("** Goods Shipment Document No. {} **", shipmentNo);

    shipment.createLinesFrom("USA111", salesOrderIdentifier);
    GoodsShipment.Lines shipmentLines = shipment.new Lines(mainPage);
    shipmentLines.assertCount(1);
    shipmentLines.edit(new GoodsShipmentLinesData.Builder().attributeSetValue(orderNo).build());
    shipmentLines.assertData(goodsShipmentLinesVerificationData);

    // Complete the shipment
    shipment.complete();
    shipment.assertProcessCompletedSuccessfully2();

    // Create a Sales invoice from order
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);
    invoice.createLinesFrom(salesOrderIdentifier);
    invoice.assertProcessCompletedSuccessfully();
    SalesInvoice.Lines invoiceLine = invoice.new Lines(mainPage);
    invoiceLine.assertCount(1);
    invoiceLine.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    // Check first invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePendingPaymentInPlanData);

    // Pay the invoice
    AddPaymentProcess addPaymentProcess = invoice.openAddPayment();
    addPaymentProcess.process("Invoices", invoiceNo, "Process Received Payment(s) and Deposit",
        null);
    invoice.assertPaymentCreatedSuccessfully();
    invoice.assertData(new SalesInvoiceHeaderData.Builder().paymentComplete(true).build());

    // Check first invoice payment plan and payment details
    invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.clearFilters();
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    SalesInvoice.PaymentInPlan.PaymentInDetails invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(
        mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Check second invoice payment plan and payment details
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    invoicePaymentInDetails = invoicePaymentInPlan.new PaymentInDetails(mainPage);
    invoicePaymentInDetails.assertCount(1);
    invoicePaymentInDetails.assertData(invoicePaymentInDetailsData);

    // Post invoice and check post
    invoice.post();
    Sleep.trySleep();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    // Check Sales order payment in plan
    order = new SalesOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    order.select(new SalesOrderHeaderData.Builder().documentNo(orderNo).build());
    orderPaymentInPlan = order.new PaymentInPlan(mainPage);
    orderPaymentInPlan.assertCount(1);
    orderPaymentInPlan.assertData(orderPaymentInPlanData2);

    // Check Sales order payment in details
    SalesOrder.PaymentInPlan.PaymentInDetails orderPaymentInDetails = orderPaymentInPlan.new PaymentInDetails(
        mainPage);
    orderPaymentInDetails.assertCount(2);
    orderPaymentInDetails.selectWithoutFiltering(0);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    orderPaymentInDetails.selectWithoutFiltering(1);
    orderPaymentInDetails.assertData(orderPaymentInDetailsData2);

    String paymentNo = orderPaymentInDetails.getData("payment").toString();
    paymentNo = paymentNo.substring(0, paymentNo.indexOf("-") - 1);

    // Check Payment In
    PaymentIn payment = new PaymentIn(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    payment.select(new PaymentInHeaderData.Builder().documentNo(paymentNo).build());
    payment.assertData(paymentInHeaderVerificationData);
    PaymentIn.Lines paymentLines = payment.new Lines(mainPage);
    paymentLines.assertCount(2);

    paymentInLinesInvoicedVerificationData.addDataField("orderPaymentSchedule$order$documentNo",
        orderNo);
    paymentInLinesInvoicedVerificationData.addDataField("invoicePaymentSchedule$invoice$documentNo",
        invoiceNo);
    paymentLines.selectWithoutFiltering(0);
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    paymentLines.selectWithoutFiltering(1);
    paymentLines.assertData(paymentInLinesInvoicedVerificationData);
    paymentLines.closeForm();

    payment.open();
    payment.post();
    Sleep.trySleep();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines3.length);
    post.assertJournalLines2(journalEntryLines3);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines4.length);
    post.assertJournalLines2(journalEntryLines4);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info("** End of test case [APRRegression25312In] Test regression 25312**");
  }
}
