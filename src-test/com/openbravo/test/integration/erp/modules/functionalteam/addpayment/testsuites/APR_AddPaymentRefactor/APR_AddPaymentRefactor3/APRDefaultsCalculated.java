/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_AddPaymentRefactor.APR_AddPaymentRefactor3;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

@RunWith(Parameterized.class)
public class APRDefaultsCalculated extends OpenbravoERPTest {
  private static final String PARAM_EXPECTED_PAYMENT = "expected_payment";
  private static Logger logger = LogManager.getLogger();

  /** The Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  /** The data to verify the creation of the Sales Invoice header data. */
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData1;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData2;
  /** The data to verify the creation of the Sales Invoice lines data. */
  SalesInvoiceLinesData salesInvoiceLineData3;
  /** The data to verify the completion of the sales invoice. */
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;

  /**
   * @param salesInvoiceHeaderData
   * @param salesInvoiceHeaderVerificationData
   * @param salesInvoiceLineData1
   * @param salesInvoiceLineData2
   * @param salesInvoiceLineData3
   * @param completedSalesInvoiceHeaderVerificationData
   */
  public APRDefaultsCalculated(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData1, SalesInvoiceLinesData salesInvoiceLineData2,
      SalesInvoiceLinesData salesInvoiceLineData3,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData1 = salesInvoiceLineData1;
    this.salesInvoiceLineData2 = salesInvoiceLineData2;
    this.salesInvoiceLineData3 = salesInvoiceLineData3;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    logInData = new LogInData.Builder().userName("userC").password("userC").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */

  @Parameters
  public static Collection<Object[]> APRDefaultsCalculatedValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("LT")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("1")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("LT")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("1")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("LT")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("1")
            .build(),

        new SalesInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("4,290.00")
            .documentStatus("Completed")
            .summedLineAmount("3,900.00")
            .grandTotalAmount("4,290.00")
            .currency("EUR")
            .paymentComplete(false)
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test the creation of a purchase order and an invoice.
   */
  @Test
  public void executeAPRDefaultsCalculated() {

    logger.info("** Start of test case [APRDefaultsCalculated] **");
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    salesInvoice.assertData(salesInvoiceHeaderVerificationData);
    SalesInvoice.Lines salesInvoiceLines = salesInvoice.new Lines(mainPage);
    salesInvoiceLines.create(salesInvoiceLineData1);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.create(salesInvoiceLineData2);
    salesInvoiceLines.assertSaved();
    salesInvoiceLines.create(salesInvoiceLineData3);
    salesInvoiceLines.assertSaved();
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();
    salesInvoice.assertData(completedSalesInvoiceHeaderVerificationData);

    AddPaymentProcess addPaymentProcess = salesInvoice.openAddPayment();
    Sleep.trySleep(2000);
    assertTrue(addPaymentProcess.getParameterValue(PARAM_EXPECTED_PAYMENT).equals("4,290.00"));

    logger.info("** End of test case [APRDefaultsCalculated] **");
  }
}
