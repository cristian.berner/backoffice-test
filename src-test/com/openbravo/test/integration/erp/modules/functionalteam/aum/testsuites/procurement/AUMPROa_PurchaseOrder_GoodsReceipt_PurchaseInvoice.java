/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Pablo Lujan <pablo.lujan@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Nono Carballo <nonofce@gmail.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.procurement;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.generalsetup.processscheduling.processrequest.ProcessRequestData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PurchaseOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.LocatorSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.gui.procurement.transactions.purchaseinvoice.PurchaseInvoiceWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.generalsetup.application.sessionpreferences.SessionPreferences;
import com.openbravo.test.integration.erp.testscripts.generalsetup.processscheduling.processrequest.ProcessRequest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseOrder;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * AUMPROa010, AUMPROa020, AUMPROa030 Test cases
 *
 * @author nonofce
 *
 */

@RunWith(Parameterized.class)
public class AUMPROa_PurchaseOrder_GoodsReceipt_PurchaseInvoice extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager
      .getLogger();

  /* Data for [AUMPROa010] Create purchase order. */
  /** The purchase order header data. */
  PurchaseOrderHeaderData purchaseOrderHeaderData;
  /** The data to verify the purchase order header creation. */
  PurchaseOrderHeaderData purchaseOrderHeaderVerficationData;
  /** The purchase order line data. */
  PurchaseOrderLinesData purchaseOrderLinesData;
  /** The data to verify the purchase order line creation. */
  PurchaseOrderLinesData purchaseOrderLinesVerificationData;
  /** The booked purchase order verification data. */
  PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData purchaseOrderPaymentOutPlanData;

  /* Data for [AUMPROa020] Create Goods Receipt. */
  /** The goods receipt header data. */
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  /** The data to verify the goods receipt header creation. */
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  /** The data to verify the goods receipt lines creation. */
  GoodsReceiptLinesData goodsReceiptLinesVerificationData;
  /** The data to verify the goods receipt header after completion. */
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;

  /* Data for [AUMPROa030] Create Purchase Invoice. */
  /** The purchase invoice header data. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  /** The data to verify the purchase invoice header creation. */
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  /** The data to verify the purchase invoice header after completion. */
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  /** The data to verify the payment out plan data. */
  com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData;
  /**
   * Expected journal entry lines. Is an array of arrays. Each line has account number, name, debit
   * and credit.
   */
  private String[][] journalEntryLines;

  public AUMPROa_PurchaseOrder_GoodsReceipt_PurchaseInvoice(
      PurchaseOrderHeaderData purchaseOrderHeaderData,
      PurchaseOrderHeaderData purchaseOrderHeaderVerficationData,
      PurchaseOrderLinesData purchaseOrderLinesData,
      PurchaseOrderLinesData purchaseOrderLinesVerificationData,
      PurchaseOrderHeaderData bookedPurchaseOrderHeaderVerificationData,
      PaymentPlanData purchaseOrderPaymentOutPlanData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData purchaseInvoicePaymentOutPlanData,
      String[][] journalEntryLines) {
    super();
    this.purchaseOrderHeaderData = purchaseOrderHeaderData;
    this.purchaseOrderHeaderVerficationData = purchaseOrderHeaderVerficationData;
    this.purchaseOrderLinesData = purchaseOrderLinesData;
    this.purchaseOrderLinesVerificationData = purchaseOrderLinesVerificationData;
    this.bookedPurchaseOrderHeaderVerificationData = bookedPurchaseOrderHeaderVerificationData;
    this.purchaseOrderPaymentOutPlanData = purchaseOrderPaymentOutPlanData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesVerificationData = goodsReceiptLinesVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoicePaymentOutPlanData = purchaseInvoicePaymentOutPlanData;
    this.journalEntryLines = journalEntryLines;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> createValues() {

    return Arrays.asList(new Object[][] { {
        // Parameters for [AUMPROa010]
        new PurchaseOrderHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseOrderHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .warehouse("Spain warehouse")
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .documentStatus("Draft")
            .currency("EUR")
            .build(),
        new PurchaseOrderLinesData.Builder().product(
            new ProductSelectorData.Builder().searchKey("RMA").priceListVersion("Purchase").build())
            .operativeQuantity("11.2")
            .build(),
        new PurchaseOrderLinesData.Builder().unitPrice("2.00")
            .listPrice("2.00")
            .uOM("Bag")
            .tax("VAT 10%")
            .lineNetAmount("67.20")
            .build(),
        new PurchaseOrderHeaderData.Builder().summedLineAmount("67.20")
            .grandTotalAmount("73.92")
            .documentStatus("Booked")
            .build(),
        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseorder.PaymentPlanData.Builder()
            .paymentMethod("1 (Spain)")
            .expected("73.92")
            .paid("0.00")
            .outstanding("73.92")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        // Parameters for [AUMPROa020]
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Draft").build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Raw material A").build())
            .attributeSetValue("#123")
            .storageBin(new LocatorSelectorData.Builder().alias("spain111").build())
            .operativeQuantity("11.2")
            .uOM("Bag")
            .movementQuantity("33.6")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),
        // Parameters for [AUMPROa030]
        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().value("VA").build())
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .invoiceDate(OBDate.CURRENT_DATE)
            .accountingDate(OBDate.CURRENT_DATE)
            .priceList("Purchase")
            .paymentMethod("1 (Spain)")
            .paymentTerms("90 days")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().totalPaid("0.00")
            .outstandingAmount("73.92")
            .daysTillDue("90")
            .dueAmount("0.00")
            .summedLineAmount("67.20")
            .grandTotalAmount("73.92")
            .documentStatus("Completed")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData.Builder()
            .dueDate(OBDate.ADD_DAYS_TO_SYSTEM_DATE_90)
            .paymentMethod("1 (Spain)")
            .expected("73.92")
            .paid("0.00")
            .outstanding("73.92")
            .lastPaymentDate("")
            .numberOfPayments("0")
            .currency("EUR")
            .build(),
        new String[][] { { "60000", "Compras de mercaderías", "67.20", "" },
            { "47200", "Hacienda Pública IVA soportado", "6.72", "" },
            { "40000", "Proveedores (euros)", "", "73.92" } } } });
  }

  @Test
  public void AUMPROa_PurchaseOrder_GoodsReceipt_PurchaseInvoice_test() {
    logger.info("** Start of test case [AUMPROa010] Create Purchase Order. **");
    final PurchaseOrder purchaseOrder = new PurchaseOrder(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseOrder.create(purchaseOrderHeaderData);
    purchaseOrder.assertSaved();
    purchaseOrder.assertData(purchaseOrderHeaderVerficationData);
    final PurchaseOrder.Lines purchaseOrderLines = purchaseOrder.new Lines(mainPage);
    purchaseOrderLines.create(purchaseOrderLinesData);
    purchaseOrderLines.assertSaved();
    purchaseOrderLines.assertData(purchaseOrderLinesVerificationData);
    purchaseOrder.book();
    purchaseOrder.assertProcessCompletedSuccessfully2();
    purchaseOrder.assertData(bookedPurchaseOrderHeaderVerificationData);
    DataObject bookedPurchaseOrderHeaderData = purchaseOrder.getData();
    PurchaseOrder.PaymentOutPlan purchaseOrderPaymentOutPlan = purchaseOrder.new PaymentOutPlan(
        mainPage);
    purchaseOrderPaymentOutPlan.assertCount(1);
    purchaseOrderPaymentOutPlan.assertData(purchaseOrderPaymentOutPlanData);
    logger.info("** End of test case [AUMPROa010] Create Purchase Order. **");

    logger.info("** Start of test case [AUMPROa020] Create Goods Receipt. **");
    final GoodsReceipt goodsReceipt = new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeaderData);
    goodsReceipt.assertSaved();
    String purchaseOrderIdentifier = String.format("%s - %s - %s",
        bookedPurchaseOrderHeaderData.getDataField("documentNo"),
        bookedPurchaseOrderHeaderData.getDataField("orderDate"),
        bookedPurchaseOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    goodsReceipt.createLinesFrom("spain111", purchaseOrderIdentifier, "123");
    goodsReceipt.assertProcessCompletedSuccessfully();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.assertCount(1);
    goodsReceiptLines.assertData(goodsReceiptLinesVerificationData);
    goodsReceipt.complete();
    for (int i = 0; i < 20 && !(goodsReceipt.getData("documentStatus")).equals("Completed"); i++) {
      Sleep.trySleep(200);
    }
    goodsReceipt.assertProcessCompletedSuccessfully2();
    goodsReceipt.assertData(completedGoodsReceiptHeaderVerificationData);
    logger.info("** End of test case [AUMPROa020] Create Goods Receipt. **");

    logger.info("** Start of test case [AUMPROa030] Create Purchase Invoice. **");
    Sleep.trySleep(1000);
    ProcessRequest processRequest = new ProcessRequest(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    processRequest.select(
        new ProcessRequestData.Builder().process("Acct Server Process").organization("*").build());
    if (processRequest.unscheduleProcess()) {
      processRequest.assertProcessUnscheduleSucceeded();
    }
    Sleep.trySleep();
    SessionPreferences sessionPreferences = new SessionPreferences(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    sessionPreferences.checkShowAccountingTabs();
    Sleep.trySleep();
    final PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    purchaseInvoice.createLinesFrom(purchaseOrderIdentifier);
    purchaseInvoice.assertProcessCompletedSuccessfully();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.assertCount(1);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    PurchaseInvoice.PaymentOutPlan purchaseInvoicePaymentOutPlan = purchaseInvoice.new PaymentOutPlan(
        mainPage);
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    PurchaseInvoice.PaymentOutPlan.PaymentOutDetails purchaseInvoicePaymentOutDetails = purchaseInvoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    purchaseInvoicePaymentOutDetails.assertCount(0);
    // FIXME: Returns to Payment Plan to try to avoid the deleted accounting date issue in SCv10
    purchaseInvoicePaymentOutPlan.assertCount(1);
    purchaseInvoicePaymentOutPlan.assertData(purchaseInvoicePaymentOutPlanData);
    // // FIXME: Add here a click in the Cancel button to avoid the clearing of the Accounting Date
    // // field in SCv10
    // purchaseInvoice.cancelOnGrid();
    purchaseInvoice.post();
    // XXX This is required because currently we can use only one tab at a time.
    mainPage.closeView(PurchaseInvoiceWindow.TITLE);
    mainPage.loadView(new PostWindow());
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    for (int i = 0; i < journalEntryLines.length; i++) {
      post.assertJournalLine(i + 1, journalEntryLines[i][0], journalEntryLines[i][1],
          journalEntryLines[i][2], journalEntryLines[i][3]);
    }
    logger.info("** End of test case [AUMPROa030] Create Purchase Invoice. **");

  }
}
