/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.reservation.testsuites.RES_Regressions.RES_Regressions2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.goodsshipment.GoodsShipmentLinesData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.ManageReservationData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesorder.SalesOrderLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.ReservationData;
import com.openbravo.test.integration.erp.data.warehouse.transactions.stockreservation.StockData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.GoodsShipment;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesOrder;
import com.openbravo.test.integration.erp.testscripts.warehouse.transactions.stockreservation.StockReservation;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 28907 - 27848 - 29003
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class RESRegression28907R27848R29003 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  GoodsReceiptHeaderData goodsReceiptHeaderData;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData;
  GoodsReceiptLinesData goodsReceiptLinesData;
  GoodsReceiptLinesData goodsReceiptLineVerificationData;
  GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData;
  GoodsReceiptHeaderData goodsReceiptHeaderData2;
  GoodsReceiptHeaderData goodsReceiptHeaderVerificationData2;
  GoodsReceiptLinesData goodsReceiptLinesData2;
  GoodsReceiptLinesData goodsReceiptLineVerificationData2;
  SalesOrderHeaderData salesOrderHeaderData;
  SalesOrderHeaderData salesOrderHeaderVerficationData;
  SalesOrderLinesData salesOrderLinesData;
  SalesOrderLinesData salesOrderLinesVerificationData;
  SalesOrderHeaderData bookedSalesOrderHeaderVerificationData;
  ManageReservationData manageReservationFilterData;
  ReservationData reservationVerificationData1;
  StockData stockVerificationData1;
  StockData stockVerificationData2;
  GoodsShipmentHeaderData goodsShipmentHeaderData;
  GoodsShipmentHeaderData goodsShipmentHeaderVerificationData;
  GoodsShipmentLinesData goodsShipmentLinesData1;
  GoodsShipmentLinesData goodsShipmentLineVerificationData1;
  GoodsShipmentLinesData goodsShipmentLinesData2;
  GoodsShipmentLinesData goodsShipmentLineVerificationData2;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData;
  ReservationData reservationVerificationData2;
  StockData stockVerificationData3;
  StockData stockVerificationData4;
  GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2;
  ReservationData reservationData;

  /**
   * Class constructor.
   *
   */
  public RESRegression28907R27848R29003(ProductData productData, PriceData productPriceData,
      GoodsReceiptHeaderData goodsReceiptHeaderData,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData,
      GoodsReceiptLinesData goodsReceiptLinesData,
      GoodsReceiptLinesData goodsReceiptLineVerificationData,
      GoodsReceiptHeaderData completedGoodsReceiptHeaderVerificationData,
      GoodsReceiptHeaderData goodsReceiptHeaderData2,
      GoodsReceiptHeaderData goodsReceiptHeaderVerificationData2,
      GoodsReceiptLinesData goodsReceiptLinesData2,
      GoodsReceiptLinesData goodsReceiptLineVerificationData2,
      SalesOrderHeaderData salesOrderHeaderData,
      SalesOrderHeaderData salesOrderHeaderVerficationData, SalesOrderLinesData salesOrderLinesData,
      SalesOrderLinesData salesOrderLinesVerificationData,
      SalesOrderHeaderData bookedSalesOrderHeaderVerificationData,
      ManageReservationData manageReservationFilterData,
      ReservationData reservationVerificationData1, StockData stockVerificationData1,
      StockData stockVerificationData2, GoodsShipmentHeaderData goodsShipmentHeaderData,
      GoodsShipmentHeaderData goodsShipmentHeaderVerificationData,
      GoodsShipmentLinesData goodsShipmentLinesData1,
      GoodsShipmentLinesData goodsShipmentLineVerificationData1,
      GoodsShipmentLinesData goodsShipmentLinesData2,
      GoodsShipmentLinesData goodsShipmentLineVerificationData2,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData,
      ReservationData reservationVerificationData2, StockData stockVerificationData3,
      StockData stockVerificationData4,
      GoodsShipmentHeaderData completedGoodsShipmentHeaderVerificationData2,
      ReservationData reservationData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.goodsReceiptHeaderData = goodsReceiptHeaderData;
    this.goodsReceiptHeaderVerificationData = goodsReceiptHeaderVerificationData;
    this.goodsReceiptLinesData = goodsReceiptLinesData;
    this.goodsReceiptLineVerificationData = goodsReceiptLineVerificationData;
    this.completedGoodsReceiptHeaderVerificationData = completedGoodsReceiptHeaderVerificationData;
    this.goodsReceiptHeaderData2 = goodsReceiptHeaderData2;
    this.goodsReceiptHeaderVerificationData2 = goodsReceiptHeaderVerificationData2;
    this.goodsReceiptLinesData2 = goodsReceiptLinesData2;
    this.goodsReceiptLineVerificationData2 = goodsReceiptLineVerificationData2;
    this.salesOrderHeaderData = salesOrderHeaderData;
    this.salesOrderHeaderVerficationData = salesOrderHeaderVerficationData;
    this.salesOrderLinesData = salesOrderLinesData;
    this.salesOrderLinesVerificationData = salesOrderLinesVerificationData;
    this.bookedSalesOrderHeaderVerificationData = bookedSalesOrderHeaderVerificationData;
    this.manageReservationFilterData = manageReservationFilterData;
    this.reservationVerificationData1 = reservationVerificationData1;
    this.stockVerificationData1 = stockVerificationData1;
    this.stockVerificationData2 = stockVerificationData2;
    this.goodsShipmentHeaderData = goodsShipmentHeaderData;
    this.goodsShipmentHeaderVerificationData = goodsShipmentHeaderVerificationData;
    this.goodsShipmentLinesData1 = goodsShipmentLinesData1;
    this.goodsShipmentLineVerificationData1 = goodsShipmentLineVerificationData1;
    this.goodsShipmentLinesData2 = goodsShipmentLinesData2;
    this.goodsShipmentLineVerificationData2 = goodsShipmentLineVerificationData2;
    this.completedGoodsShipmentHeaderVerificationData = completedGoodsShipmentHeaderVerificationData;
    this.reservationVerificationData2 = reservationVerificationData2;
    this.stockVerificationData3 = stockVerificationData3;
    this.stockVerificationData4 = stockVerificationData4;
    this.completedGoodsShipmentHeaderVerificationData2 = completedGoodsShipmentHeaderVerificationData2;
    this.reservationData = reservationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Finished Goods")
            .taxCategory("VAT 10%")
            .purchase(false)
            .sale(true)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Customer A")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().build(),
        new GoodsReceiptLinesData.Builder().lineNo("10")
            .movementQuantity("10")
            .uOM("Unit")
            .organization("Spain")
            .build(),
        new GoodsReceiptHeaderData.Builder().documentStatus("Completed").build(),

        new GoodsReceiptHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .warehouse("Spain East warehouse")
            .build(),
        new GoodsReceiptHeaderData.Builder().organization("Spain")
            .documentType("MM Receipt")
            .warehouse("Spain East warehouse")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .build(),
        new GoodsReceiptLinesData.Builder().build(),
        new GoodsReceiptLinesData.Builder().lineNo("10")
            .movementQuantity("20")
            .uOM("Unit")
            .organization("Spain")
            .build(),

        new SalesOrderHeaderData.Builder().transactionDocument("Standard Order")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new SalesOrderHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentMethod("1 (Spain)")
            .paymentTerms("30 days, 5")
            .warehouse("Spain warehouse")
            .invoiceTerms("Customer Schedule After Delivery")
            .invoiceAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new SalesOrderLinesData.Builder().orderedQuantity("30").build(),
        new SalesOrderLinesData.Builder().uOM("Unit")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("60.00")
            .build(),
        new SalesOrderHeaderData.Builder().documentStatus("Booked")
            .summedLineAmount("60.00")
            .grandTotalAmount("66.00")
            .currency("EUR")
            .build(),

        new ManageReservationData.Builder().warehouse("==Spain East warehouse or ==Spain warehouse")
            .build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("30")
            .reservedQuantity("30")
            .releasedQuantity("0")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Completed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("M01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .build(),
        new GoodsShipmentHeaderData.Builder().documentType("MM Shipment")
            .warehouse("Spain warehouse")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("10").build(),
        new GoodsShipmentLinesData.Builder().lineNo("10")
            .movementQuantity("10")
            .uOM("Unit")
            .storageBin("L01")
            .organization("Spain")
            .build(),
        new GoodsShipmentLinesData.Builder().movementQuantity("20").build(),
        new GoodsShipmentLinesData.Builder().lineNo("20")
            .movementQuantity("20")
            .uOM("Unit")
            .storageBin("M01")
            .organization("Spain")
            .build(),
        new GoodsShipmentHeaderData.Builder().documentStatus("Completed").build(),

        new ReservationData.Builder().organization("Spain")
            .quantity("30")
            .reservedQuantity("30")
            .releasedQuantity("30")
            .uom("Unit")
            .warehouse("")
            .storageBin("&nbsp;")
            .attributeSetValue("&nbsp;")
            .status("Closed")
            .build(),
        new StockData.Builder().storageBin("L01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("10")
            .releasedQuantity("10")
            .allocated(false)
            .build(),
        new StockData.Builder().storageBin("M01")
            .attributeSetValue("&nbsp;")
            .purchaseOrderLine("&nbsp;")
            .reservedQuantity("20")
            .releasedQuantity("20")
            .allocated(false)
            .build(),

        new GoodsShipmentHeaderData.Builder().documentStatus("Voided").build(),

        new ReservationData.Builder().status("Completed").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28907 - 27848 - 29003
   *
   * @throws ParseException
   */
  @Test
  public void RESRegression28907R27848R29003Test() throws ParseException {
    logger.info(
        "** Start of test case [RESRegression28907R27848R29003] Test regression 28907 - 27848 - 29003. **");

    String productName = "Test28907-";
    int num1 = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num1);
    ProductData productData1 = productData;
    productData1.addDataField("searchKey", productName);
    productData1.addDataField("name", productName);
    Product.ProductTab.create(mainPage, productData1);
    Product.Price.create(mainPage, productPriceData);

    GoodsReceipt goodsReceipt1 = new GoodsReceipt(mainPage).open();
    goodsReceipt1.create(goodsReceiptHeaderData);
    goodsReceipt1.assertSaved();
    goodsReceipt1.assertData(goodsReceiptHeaderVerificationData);
    GoodsReceipt.Lines goodsReceiptLines1 = goodsReceipt1.new Lines(mainPage);
    goodsReceiptLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    goodsReceiptLinesData.addDataField("movementQuantity", "10");
    goodsReceiptLinesData.addDataField("storageBin", "L01");
    goodsReceiptLines1.create(goodsReceiptLinesData);
    goodsReceiptLines1.assertSaved();
    goodsReceiptLines1.assertData(goodsReceiptLineVerificationData);
    goodsReceipt1.complete();
    goodsReceipt1.assertProcessCompletedSuccessfully2();
    goodsReceipt1.assertData(completedGoodsReceiptHeaderVerificationData);

    GoodsReceipt goodsReceipt2 = new GoodsReceipt(mainPage).open();
    goodsReceipt2.create(goodsReceiptHeaderData2);
    goodsReceipt2.assertSaved();
    goodsReceipt2.assertData(goodsReceiptHeaderVerificationData2);
    GoodsReceipt.Lines goodsReceiptLines2 = goodsReceipt2.new Lines(mainPage);
    goodsReceiptLinesData2.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    goodsReceiptLinesData2.addDataField("movementQuantity", "20");
    goodsReceiptLinesData2.addDataField("storageBin", "M01");
    goodsReceiptLines2.create(goodsReceiptLinesData2);
    goodsReceiptLines2.assertSaved();
    goodsReceiptLines2.assertData(goodsReceiptLineVerificationData2);
    goodsReceipt2.complete();
    goodsReceipt2.assertProcessCompletedSuccessfully2();
    goodsReceipt2.assertData(completedGoodsReceiptHeaderVerificationData);

    SalesOrder salesOrder = new SalesOrder(mainPage).open();
    salesOrder.create(salesOrderHeaderData);
    salesOrder.assertSaved();
    salesOrder.assertData(salesOrderHeaderVerficationData);
    String orderNo = salesOrder.getData("documentNo").toString();
    SalesOrder.Lines salesOrderLines = salesOrder.new Lines(mainPage);
    salesOrderLinesData.addDataField("product",
        new ProductCompleteSelectorData.Builder().name(productName).build());
    salesOrderLines.create(salesOrderLinesData);
    salesOrderLines.assertSaved();
    salesOrderLines.assertData(salesOrderLinesVerificationData);
    String orderLineIdentifier = String.format(" - %s - %s", salesOrderLines.getData("lineNo"),
        salesOrderLines.getData("lineNetAmount"));
    salesOrder.book();
    salesOrder.assertProcessCompletedSuccessfully2();
    salesOrder.assertData(bookedSalesOrderHeaderVerificationData);
    String orderIdentifier = String.format("%s - %s - %s", orderNo, salesOrder.getData("orderDate"),
        bookedSalesOrderHeaderVerificationData.getDataField("grandTotalAmount"));
    orderLineIdentifier = orderIdentifier + orderLineIdentifier;
    // Required sleep
    Sleep.trySleep();
    salesOrderLines.manageReservation(manageReservationFilterData, null);

    StockReservation stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    StockReservation.Stock stock = stockReservation.new Stock(mainPage);
    stock.assertCount(2);
    stock.selectWithoutFiltering(0);
    stock.assertData(stockVerificationData1);
    stock.selectWithoutFiltering(1);
    stock.assertData(stockVerificationData2);

    GoodsShipment goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.create(goodsShipmentHeaderData);
    goodsShipment.assertSaved();
    goodsShipment.assertData(goodsShipmentHeaderVerificationData);
    goodsShipment.createLinesFrom("L01", orderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    goodsShipment.createLinesFrom("M01", orderIdentifier);
    goodsShipment.assertProcessCompletedSuccessfully();
    String shipmentNo = goodsShipment.getData("documentNo").toString();
    GoodsShipment.Lines goodsShipmentLines = goodsShipment.new Lines(mainPage);
    goodsShipmentLines.assertCount(2);
    goodsShipmentLines.selectWithoutFiltering(0);
    goodsShipmentLines.edit(goodsShipmentLinesData1);
    goodsShipmentLines.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData1
        .addDataField("product", productName)
        .addDataField("salesOrderLine", orderLineIdentifier));
    goodsShipmentLines.selectWithoutFiltering(1);
    goodsShipmentLines.edit(goodsShipmentLinesData2);
    goodsShipmentLines.assertData((GoodsShipmentLinesData) goodsShipmentLineVerificationData2
        .addDataField("product", productName)
        .addDataField("salesOrderLine", orderLineIdentifier));
    goodsShipment.complete();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData((GoodsShipmentHeaderData) completedGoodsShipmentHeaderVerificationData
        .addDataField("salesOrder", orderIdentifier));

    stockReservation = new StockReservation(mainPage).open();
    stockReservation
        .select(new ReservationData.Builder().salesOrderLine(orderLineIdentifier).build());
    Sleep.trySleep();
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData2.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    Sleep.trySleep();
    stock.assertCount(2);
    stock.selectWithoutFiltering(0);
    stock.assertData(stockVerificationData3);
    stock.selectWithoutFiltering(1);
    stock.assertData(stockVerificationData4);

    goodsShipment = new GoodsShipment(mainPage).open();
    goodsShipment.select(new GoodsShipmentHeaderData.Builder().documentNo(shipmentNo).build());
    goodsShipment.close();
    goodsShipment.assertProcessCompletedSuccessfully2();
    goodsShipment.assertData(completedGoodsShipmentHeaderVerificationData2);

    stockReservation = new StockReservation(mainPage).open();
    stockReservation.select(
        (ReservationData) reservationData.addDataField("salesOrderLine", orderLineIdentifier));
    stockReservation.assertCount(1);
    stockReservation.assertData(
        (ReservationData) reservationVerificationData1.addDataField("product", productName)
            .addDataField("salesOrderLine", orderLineIdentifier));
    stock = stockReservation.new Stock(mainPage);
    stock.assertCount(2);
    stock.selectWithoutFiltering(0);
    stock.assertData(stockVerificationData1);
    stock.selectWithoutFiltering(1);
    stock.assertData(stockVerificationData2);

    logger.info(
        "** End of test case [RESRegression28907R27848R29003] Test regression 28907 - 27848 - 29003. **");
  }
}
