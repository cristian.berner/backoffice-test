/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions7;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.accounting.analysis.PostWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.accounting.analysis.Post;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;

/**
 * Test regression 33500
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRRegression33500In17 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceHeaderData salesInvoiceHeaderVerificationData;
  SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData;
  SalesInvoiceLinesData salesInvoiceLineData;
  SalesInvoiceLinesData salesInvoiceLineVerificationData;
  com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData;
  String[][] journalEntryLines;
  String[][] journalEntryLines2;

  /**
   * Class constructor.
   *
   */
  public APRRegression33500In17(SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceHeaderData salesInvoiceHeaderVerificationData,
      SalesInvoiceLinesData salesInvoiceLineData,
      SalesInvoiceLinesData salesInvoiceLineVerificationData,
      SalesInvoiceHeaderData completedSalesInvoiceHeaderVerificationData,
      com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData invoicePaymentInPlanData,
      String[][] journalEntryLines, String[][] journalEntryLines2) {
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceHeaderVerificationData = salesInvoiceHeaderVerificationData;
    this.completedSalesInvoiceHeaderVerificationData = completedSalesInvoiceHeaderVerificationData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.salesInvoiceLineVerificationData = salesInvoiceLineVerificationData;
    this.invoicePaymentInPlanData = invoicePaymentInPlanData;
    this.journalEntryLines = journalEntryLines;
    this.journalEntryLines2 = journalEntryLines2;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> SalesOrderValues() {
    Object[][] data = new Object[][] { {
        new SalesInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AR Credit Memo")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentTerms("30-60")
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .build(),

        new SalesInvoiceHeaderData.Builder().transactionDocument("AR Credit Memo")
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .priceList("Customer A")
            .paymentTerms("30-60")
            .build(),

        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGC")
                .priceListVersionName("Customer A")
                .build())
            .invoicedQuantity("10")
            .tax("VAT 10% USA")
            .build(),

        new SalesInvoiceLinesData.Builder().invoicedQuantity("10").lineNetAmount("20.00").build(),

        new SalesInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("22.00")
            .summedLineAmount("20.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.PaymentPlanData.Builder()
            .paymentMethod("Acc-3 (Payment-Trx-Reconciliation)")
            .expected("-11.00")
            .received("0.00")
            .outstanding("-11.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("0")
            .build(),

        new String[][] { { "70000", "Ventas de mercaderías", "20.00", "" },
            { "47700", "Hacienda Pública IVA repercutido", "2.00", "" },
            { "43000", "Clientes (euros)", "", "22.00" }, },

        new String[][] { { "41100", "Sales", "50.00", "" }, { "21400", "Tax Due", "5.00", "" },
            { "11400", "Accounts receivable", "", "55.00" } },

        } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33500 - Positive Credit Memo Invoice with Payment Plan divided
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33500In17Test() throws ParseException {
    logger.info(
        "** Start of test case [PRORegression33500In17] Test regression 33500 - Positive Credit Memo Invoice with Payment Plan divided. **");

    // Create a Sales invoice
    SalesInvoice invoice = new SalesInvoice(mainPage).open();
    invoice.create(salesInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(salesInvoiceHeaderVerificationData);

    SalesInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(salesInvoiceLineData);
    invoiceLines.assertSaved();
    invoiceLines.assertData(salesInvoiceLineVerificationData);
    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedSalesInvoiceHeaderVerificationData);

    // Check first invoice payment plan
    SalesInvoice.PaymentInPlan invoicePaymentInPlan = invoice.new PaymentInPlan(mainPage);
    invoicePaymentInPlan.assertCount(2);
    invoicePaymentInPlan.selectWithoutFiltering(0);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    // Check second invoice payment plan
    invoicePaymentInPlan.closeForm();
    invoicePaymentInPlan.selectWithoutFiltering(1);
    invoicePaymentInPlan.assertData(invoicePaymentInPlanData);

    // Post invoice and check post
    invoice.post();
    mainPage.loadView(new PostWindow("Journal Entries Report - Main US/A/Euro"));
    Post post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines.length);
    post.assertJournalLines2(journalEntryLines);
    mainPage.closeView("Journal Entries Report - Main US/A/Euro");
    mainPage.loadView(new PostWindow("Journal Entries Report - USA US/A/US Dollar"));
    post = new Post(mainPage);
    post.assertJournalLinesCount(journalEntryLines2.length);
    post.assertJournalLines2(journalEntryLines2);
    mainPage.closeView("Journal Entries Report - USA US/A/US Dollar");

    logger.info(
        "** End of test case [APRRegression33500In17] Test regression 33500 - Positive Credit Memo Invoice with Payment Plan divided. **");
  }
}
