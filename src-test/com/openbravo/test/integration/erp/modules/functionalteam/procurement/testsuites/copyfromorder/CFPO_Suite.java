/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.copyfromorder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;

/**
 * Copy From Orders. Suite testing Purchase Orders flows
 *
 * @author Mark
 *
 */
@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ CFPO_SamePriceList.class, CFPO_NextLineNoTest.class,
    CFPO_DifferentBPandPriceList.class, CFPO_ProductPriceNotDefinedInPL.class,
    CFPO_CopyFromMultipleOrders.class })
public class CFPO_Suite {

}
