/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions8;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.ProfileData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.OrgAccessData;
import com.openbravo.test.integration.erp.data.generalsetup.security.role.RoleData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.OrganizationSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInTab;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.paymentin.PaymentInWindow;
import com.openbravo.test.integration.erp.gui.general.security.role.RoleWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test Regression 33043
 *
 * @author nonofce
 *
 */
@RunWith(Parameterized.class)
public class APRRegression33043In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  RoleData financeRole;
  OrgAccessData orgAccessData;
  PaymentInHeaderData paymentInHeader;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceLinesData salesInvoiceLineData;

  public APRRegression33043In(RoleData financeRole, OrgAccessData orgAccessData,
      PaymentInHeaderData paymentInHeader, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceLinesData salesInvoiceLineData) {
    super();
    this.financeRole = financeRole;
    this.orgAccessData = orgAccessData;
    this.paymentInHeader = paymentInHeader;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceLineData = salesInvoiceLineData;

    logInData = new LogInData.Builder().userName("Openbravo").password("openbravo").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {

        new RoleData.Builder().name("Espa Finance").build(),

        new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("Sur or S.A").build())
            .orgAdmin(false)
            .build(),

        new PaymentInHeaderData.Builder().amount("10.00")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .build(),
        new SalesInvoiceHeaderData.Builder().organization("F&B España - Región Norte")
            .transactionDocument("AR Invoice")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Alimentos y Supermercados, S.A")
                    .build())
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(
                new ProductSimpleSelectorData.Builder().productName("Cerveza Ale 0,5L").build())
            .invoicedQuantity("5")
            .unitPrice("2.00")
            .build()

        } };
    return Arrays.asList(data);
  }

  @Test
  public void APRRegression33043Test() {
    logger.info(
        "** Start of test case [APRRegression33043In] Permissions problem after process payment **");

    RoleWindow roleWindow = new RoleWindow();
    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectOrgAccessTab().filter(orgAccessData);
    Sleep.trySleep();
    int orgCount = roleWindow.selectOrgAccessTab().getRecordCount();
    if (orgCount > 0) {
      roleWindow.selectOrgAccessTab().deleteMultipleRecordsOneCheckboxOnGrid();
    }
    roleWindow.selectOrgAccessTab().clearFilters();
    Sleep.trySleep();
    roleWindow.selectOrgAccessTab().assertCount(1);
    assertTrue(roleWindow.selectOrgAccessTab()
        .getData()
        .getDataField("organization")
        .equals("F&B España - Región Norte"));

    mainPage.closeAllViews();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B España, S.A - Finance - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();

    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoice.complete();
    salesInvoice.assertProcessCompletedSuccessfully2();

    PaymentIn paymentIn = new PaymentIn(mainPage).open();

    PaymentInTab tab = (PaymentInTab) paymentIn.getTab();
    tab.waitUntilSelected();
    List<Map<String, Object>> paymentRows = tab.getRows();
    for (Map<String, Object> row : paymentRows) {
      assertTrue(row.get("organization$_identifier").equals("F&B España - Región Norte")
          || row.get("organization$_identifier").equals("F&B España, S.A"));
    }
    int before = tab.getRecordCount();
    paymentIn.create(paymentInHeader);
    paymentIn.assertSaved();

    AddPaymentProcess addDetails = paymentIn.addDetailsOpen();
    addDetails.getOrderInvoiceGrid().selectRecord(0);
    addDetails.process("Process Received Payment(s) and Deposit");

    mainPage.openView(mainPage.getView(PaymentInWindow.TITLE));
    tab = (PaymentInTab) paymentIn.getTab();

    paymentRows = tab.getRows();
    for (Map<String, Object> row : paymentRows) {
      assertTrue(row.get("organization$_identifier").equals("F&B España - Región Norte")
          || row.get("organization$_identifier").equals("F&B España, S.A"));
    }
    int after = tab.getRecordCount();

    assertTrue(after == (before + 1));

    mainPage.closeAllViews();
    mainPage.changeProfile(
        new ProfileData.Builder().role("F&B International Group Admin - F&B International Group")
            .organization("F&B España - Región Norte")
            .build());

    mainPage.openView(roleWindow);
    roleWindow.selectRoleTab().filter(financeRole);
    roleWindow.selectOrgAccessTab()
        .createRecord(new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("Sur").build())
            .build());
    roleWindow.selectOrgAccessTab()
        .createRecord(new OrgAccessData.Builder()
            .organization(new OrganizationSelectorData.Builder().name("S.A").build())
            .build());

    logger.info(
        "** End of test case [APRRegression33043In] Permissions problem after process payment **");
  }
}
