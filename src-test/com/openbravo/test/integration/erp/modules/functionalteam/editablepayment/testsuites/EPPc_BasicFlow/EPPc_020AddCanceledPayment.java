/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Pablo Luján <plu@openbravo.com>
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.editablepayment.testsuites.EPPc_BasicFlow;

import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;

/**
 * This test case posts a reconciliation.
 *
 * @author Pablo Luján <plu@openbravo.com>
 */

@RunWith(Parameterized.class)
public class EPPc_020AddCanceledPayment extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The payment in header data. */
  PaymentInHeaderData paymentInHeaderData;

  /**
   * Class constructor.
   *
   * @param paymentInHeaderData
   *          The header data.
   */

  public EPPc_020AddCanceledPayment(PaymentInHeaderData paymentInHeaderData) {

    this.paymentInHeaderData = paymentInHeaderData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of string arrays with data for the test.
   *
   */
  @Parameters
  public static Collection<Object[]> orderToCashValues() {

    Object[][] data = new Object[][] { { new PaymentInHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
        .paymentMethod("2.1 (Spain)")
        .amount("70.38")
        .build(), } };
    return Arrays.asList(data);
  }

  /**
   * Test case post a reconciliation.
   */
  @Test
  public void EPPc020_AddCanceledPayment() {

    // TODO: How to get the DocumentNo.
    String documentNo = "100000";
    logger.info("** Start of test case [EPPc020] Add Canceled Payment. **");
    PaymentIn paymentIn = new PaymentIn(mainPage);
    paymentIn.create(paymentInHeaderData);
    paymentIn.addDetails("Invoices", documentNo, "Process Received Payment(s)");
    paymentIn.process("Void");
    paymentIn.assertProcessCompletedSuccessfully2();
    // TODO: Assert this (data object required)
    // paymentIn.assertData(paymentInHeaderData);

    logger.info("** End of test case [EPPc020] Add Canceled Payment. **");

  }
}
