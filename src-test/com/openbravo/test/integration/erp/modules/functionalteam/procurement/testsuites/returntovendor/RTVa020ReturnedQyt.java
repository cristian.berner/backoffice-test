/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2017 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <f.carballo@nectus.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.returntovendor;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test RTVa020
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class RTVa020ReturnedQyt extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  ReturnToVendorHeaderData documentCreated;
  ReturnToVendorLineSelectorData linePicked;
  ReturnToVendorLineSelectorData linePickedEdit;
  String editLineErrorMessage = "Returned Qty, considering quantity returned in other RM Orders, exceeds the Quantity. Please enter a valid range: 0 - 10.";

  public RTVa020ReturnedQyt(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData documentCreated,
      ReturnToVendorLineSelectorData linePicked, ReturnToVendorLineSelectorData linePickedEdit) {
    super();
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.documentCreated = documentCreated;
    this.linePicked = linePicked;
    this.linePickedEdit = linePickedEdit;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Volley Ball").build())
            .movementQuantity("10")
            .storageBin("Y02")
            .build(),
        new ReturnToVendorHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorLineSelectorData.Builder().product("Volley Ball").build(),
        new ReturnToVendorLineSelectorData.Builder().returned("16").build() } };
    return Arrays.asList(data);
  }

  @Test
  public void RTVa020test() throws ParseException {
    logger.debug("** Start test case [RTVa020] Returned qty bigger than Ship/Receipt **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    String documentNo = (String) goodsReceipt.getData("documentNo");
    linePicked.addDataField("shipmentNumber", documentNo);

    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(documentCreated);
    returnToVendor.assertSaved();
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(linePicked);
    popup.edit(linePickedEdit);
    popup.process();
    popup.assertError(editLineErrorMessage);

    // Void the goods receipts since the stock created was not used...
    goodsReceipt.open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.select(new GoodsReceiptHeaderData.Builder()
        .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
        .documentNo(documentNo)
        .build());
    goodsReceipt.close();

    logger.debug("** End test case [RTVa020] Returned qty bigger than Ship/Receipt **");
  }
}
