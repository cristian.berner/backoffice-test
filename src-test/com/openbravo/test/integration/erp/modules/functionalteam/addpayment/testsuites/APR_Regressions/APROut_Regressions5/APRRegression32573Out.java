/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Rafael Queralta <rafaelcuba81@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.PaymentSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.AddPaymentGrid;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 32573
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class APRRegression32573Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData paymentMethodVerifyData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  TransactionsData transactionLinesData;
  TransactionsData transactionLinesData2;
  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;

  /**
   * Class constructor.
   *
   */
  public APRRegression32573Out(AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData paymentMethodVerifyData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      TransactionsData transactionLinesData, TransactionsData transactionLinesData2,
      PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.paymentMethodVerifyData = paymentMethodVerifyData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.transactionLinesData = transactionLinesData;
    this.transactionLinesData2 = transactionLinesData2;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Bank").build(),
        new PaymentMethodData.Builder().paymentMethod("Acc-8 (Transactions)").build(),
        new PaymentMethodData.Builder().payoutAllow(true)
            .automaticWithdrawn(true)
            .payoutExecutionType("Automatic")
            .uponWithdrawalUse("Withdrawn Payment Account")
            .payoutExecutionProcess("Print Check simple process")
            .payoutDeferred(true)
            .uponPaymentUse("In Transit Payment Account")
            .oUTUponClearingUse("")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .paymentMethod("Acc-8 (Transactions)")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .paymentTerms("90 days")
            .paymentMethod("Acc-8 (Transactions)")
            .priceList("Purchase")
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("420")
            .build(),
        new PurchaseInvoiceLinesData.Builder().lineNo("10")
            .uOM("Bag")
            .unitPrice("2.00")
            .listPrice("2.00")
            .tax("VAT 10%")
            .lineNetAmount("840.00")
            .build(),
        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount("924.00")
            .summedLineAmount("840.00")
            .currency("EUR")
            .paymentComplete(false)
            .build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal").build(),
        new TransactionsData.Builder().status("Awaiting Payment")
            .currency("EUR")
            .depositAmount("924.00")
            .paymentAmount("0.00")
            .organization("Spain")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),

        new PaymentOutHeaderData.Builder().build(),
        new PaymentOutHeaderData.Builder().status("Withdrawn not Cleared").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 32573 - It is not possible to execute a payment created in the financial
   * account if it is set up as "Automatic Deposit".
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression32573OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression32573Out] Test regression 32573 -  It is not possible to execute a payment created in the financial account if it is set up as Automatic Deposit. **");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(paymentMethodVerifyData);

    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    purchaseInvoice.assertData(purchaseInvoiceHeaderVerificationData);
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLine = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLine.create(purchaseInvoiceLineData);
    purchaseInvoiceLine.assertSaved();
    purchaseInvoiceLine.assertData(purchaseInvoiceLineVerificationData);
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();
    purchaseInvoice.assertData(completedPurchaseInvoiceHeaderVerificationData);

    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.createWithoutSaving(transactionLinesData);
    AddPaymentProcess addPaymentProcess = transactions.openAddPayment();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    paymentNo = paymentNo.substring(1, paymentNo.length() - 1);
    addPaymentProcess.setParameterValue("received_from",
        new BusinessPartnerSelectorData.Builder().name("Vendor A").build());
    addPaymentProcess.setParameterValue("fin_paymentmethod_id", "Acc-8 (Transactions)");
    AddPaymentGrid orderGrid = addPaymentProcess.getOrderInvoiceGrid();
    orderGrid.clearFilters();
    orderGrid.unselectAll();
    orderGrid.filter(new SelectedPaymentData.Builder().invoiceNo(invoiceNo).build());
    Sleep.trySleep(2000);
    orderGrid.selectAll();
    addPaymentProcess.process("Process Made Payment(s)");
    transactionLinesData.addDataField("finPayment",
        new PaymentSelectorData.Builder().documentNo(paymentNo).build());
    transactions.save();
    transactions.assertSaved();

    PaymentOut paymentOut = new PaymentOut(mainPage).open();
    paymentOutHeaderData.addDataField("documentNo", paymentNo);
    paymentOut.select(paymentOutHeaderData);
    paymentOut.execute(paymentNo);
    paymentOut.assertPaymentExecutedSuccessfully();
    paymentOut.assertData(paymentOutHeaderVerificationData);

    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    transactions = financialAccount.new Transaction(mainPage);
    transactions.select(transactionLinesData);
    transactions.assertData(new TransactionsData.Builder().status("Withdrawn not Cleared").build());

    logger.info(
        "** End of test case [APRRegression3573Out] Test regression 32573 -  It is not possible to execute a payment created in the financial account if it is set up as Automatic Deposit. **");
  }
}
