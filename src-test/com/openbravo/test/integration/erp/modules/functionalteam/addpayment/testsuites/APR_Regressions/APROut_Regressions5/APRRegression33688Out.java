/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APROut_Regressions5;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentout.PaymentOutHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentOut;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 33688
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class APRRegression33688Out extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData verifyPaymentMethodData;
  PaymentOutHeaderData paymentOutHeaderData;
  PaymentOutHeaderData paymentOutHeaderVerificationData;
  AddPaymentPopUpData addPaymentVerificationData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PaymentOutHeaderData paymentOutHeaderData2;
  AddPaymentPopUpData addPaymentVerificationData2;
  TransactionsData transactionsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression33688Out(AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData verifyPaymentMethodData, PaymentOutHeaderData paymentOutHeaderData,
      PaymentOutHeaderData paymentOutHeaderVerificationData,
      AddPaymentPopUpData addPaymentVerificationData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData, PaymentOutHeaderData paymentOutHeaderData2,
      AddPaymentPopUpData addPaymentVerificationData2, TransactionsData transactionsData) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.verifyPaymentMethodData = verifyPaymentMethodData;
    this.paymentOutHeaderData = paymentOutHeaderData;
    this.paymentOutHeaderVerificationData = paymentOutHeaderVerificationData;
    this.addPaymentVerificationData = addPaymentVerificationData;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.paymentOutHeaderData2 = paymentOutHeaderData2;
    this.addPaymentVerificationData2 = addPaymentVerificationData2;
    this.transactionsData = transactionsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Cashbook").build(),
        new PaymentMethodData.Builder().paymentMethod("1 (Spain)").build(),
        new PaymentMethodData.Builder().uponWithdrawalUse("Cleared Payment Account").build(),
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .generatedCredit("408.20")
            .build(),
        new PaymentOutHeaderData.Builder().organization("Spain")
            .documentType("AP Payment")
            .paymentMethod("1 (Spain)")
            .account("Spain Cashbook - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .actual_payment("408.20")
            .expected_payment("0.00")
            .amount_gl_items("0.00")
            .amount_inv_ords("0.00")
            .total("0.00")
            .difference("408.20")
            .build(),
        new PurchaseInvoiceHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().productName("Raw material A")
                .priceListVersionName("Purchase")
                .build())
            .invoicedQuantity("1")
            .tax("Exempt")
            .unitPrice("3.38")
            .build(),
        new PaymentOutHeaderData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .build(),
        new AddPaymentPopUpData.Builder().received_from("Vendor A")
            .fin_paymentmethod_id("1 (Spain)")
            .fin_financial_account_id("Spain Cashbook - EUR")
            .c_currency_id("EUR")
            .amount_gl_items("0.00")
            .amount_inv_ords("3.38")
            .difference("0.00")
            .total("3.38")
            .build(),
        new TransactionsData.Builder().transactionType("BP Withdrawal")
            .paymentAmount("1.38")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33688 - Cannot post a financial account transaction if the invoice is partially
   * paid by using credit.
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33688OutTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33688Out] Test regression 33688 - Cannot post a financial account transaction if the invoice is partially paid by using credit. **");

    // Check payment method is configured to post only financial account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(verifyPaymentMethodData);

    // Create a Payment Out of 408.20 for Customer A. and leave the amount to be used later
    PaymentOut paymentOut1 = new PaymentOut(mainPage).open();
    paymentOut1.create(paymentOutHeaderData);
    paymentOut1.assertSaved();
    paymentOut1.assertData(paymentOutHeaderVerificationData);
    AddPaymentProcess addPaymentProcess1 = paymentOut1.addDetailsOpen();
    String paymentNo = addPaymentProcess1.getParameterValue("payment_documentno").toString();
    addPaymentProcess1.process("Process Made Payment(s)", "Leave the credit to be used later",
        addPaymentVerificationData);
    paymentOut1.assertPaymentCreatedSuccessfully();

    // Create a Purchase Invoice with a total gross amount of 3.38 EUR. Do not use credit
    PurchaseInvoice purchaseInvoice = new PurchaseInvoice(mainPage).open();
    purchaseInvoice.create(purchaseInvoiceHeaderData);
    purchaseInvoice.assertSaved();
    String invoiceNo = purchaseInvoice.getData("documentNo").toString();
    PurchaseInvoice.Lines purchaseInvoiceLines = purchaseInvoice.new Lines(mainPage);
    purchaseInvoiceLines.create(purchaseInvoiceLineData);
    purchaseInvoiceLines.assertSaved();
    purchaseInvoice.complete();
    purchaseInvoice.assertProcessCompletedSuccessfully2();

    // Create a new Payment Out of 1.38 EUR
    PaymentOut paymentOut2 = new PaymentOut(mainPage).open();
    Sleep.trySleep(15000);
    paymentOut2.create(paymentOutHeaderData2);
    paymentOut2.assertSaved();
    AddPaymentProcess addPaymentProcess2 = paymentOut2.addDetailsOpen();
    addPaymentProcess2.collapseOrder();
    addPaymentProcess2
        .selectCredit(new SelectedPaymentData.Builder().documentNo(paymentNo).build());
    addPaymentProcess2.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("2").build());
    addPaymentProcess2.expandOrder();
    addPaymentProcess2.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess2
        .selectPayment(new SelectedPaymentData.Builder().invoiceNo(invoiceNo).build());
    Sleep.trySleep(3500);
    addPaymentProcess2.getField("reference_no").focus();
    addPaymentProcess2.process("Process Made Payment(s) and Withdrawal",
        addPaymentVerificationData2);
    paymentOut2.assertPaymentCreatedSuccessfully();

    paymentNo = (String) paymentOut2.getData("documentNo");
    transactionsData.addDataField("finPayment$documentNo", paymentNo);

    // Navigate to the financial account create a new transaction and try to post it
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionsData);
    Sleep.trySleep(3500);
    transaction.post();

    logger.info(
        "** End of test case [APRRegression33688Out] Test regression 33688 - Cannot post a financial account transaction if the invoice is partially paid by using credit. **");
  }
}
