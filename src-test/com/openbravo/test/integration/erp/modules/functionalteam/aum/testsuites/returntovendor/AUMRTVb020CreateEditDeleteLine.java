/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2018 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2016-03-03 21:37:14
 * Contributor(s):
 *   Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */
package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebDriverException;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorShipmentLineData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendorShipment;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVb020 Create, edit and delete a line
 *
 * @author Nono Carballo
 *
 */
@RunWith(Parameterized.class)
public class AUMRTVb020CreateEditDeleteLine extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */

  /** The return to vendor header data. */
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData;
  ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData;
  ReturnToVendorShipmentLineData returnToVendorShipmentFilterLine;
  ReturnToVendorShipmentLineData returnToVendorShipmentEditSelectedLine;
  ReturnToVendorShipmentLineData returnToVendorShipmentCreateLine;
  ReturnToVendorShipmentLineData returnToVendorShipmentModifySelectedLine;
  ReturnToVendorShipmentLineData returnToVendorShipmentEditLine;

  public AUMRTVb020CreateEditDeleteLine(
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderData,
      ReturnToVendorShipmentHeaderData returnToVendorShipmentHeaderVerificationData,
      ReturnToVendorShipmentLineData returnToVendorShipmentFilterLine,
      ReturnToVendorShipmentLineData returnToVendorShipmentEditSelectedLine,
      ReturnToVendorShipmentLineData returnToVendorShipmentCreateLine,
      ReturnToVendorShipmentLineData returnToVendorShipmentModifySelectedLine,
      ReturnToVendorShipmentLineData returnToVendorShipmentEditLine) {
    this.returnToVendorShipmentHeaderData = returnToVendorShipmentHeaderData;
    this.returnToVendorShipmentHeaderVerificationData = returnToVendorShipmentHeaderVerificationData;
    this.returnToVendorShipmentFilterLine = returnToVendorShipmentFilterLine;
    this.returnToVendorShipmentEditSelectedLine = returnToVendorShipmentEditSelectedLine;
    this.returnToVendorShipmentCreateLine = returnToVendorShipmentCreateLine;
    this.returnToVendorShipmentModifySelectedLine = returnToVendorShipmentModifySelectedLine;
    this.returnToVendorShipmentEditLine = returnToVendorShipmentEditLine;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorShipmentValues() {
    Object[][] data = new Object[][] { {
        new ReturnToVendorShipmentHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorShipmentHeaderData.Builder().organization("Spain")
            .partnerAddress(".Pamplona, Pamplona")
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .documentType("RTV Shipment")
            .warehouse("Spain warehouse")
            .documentStatus("Draft")
            .build(),
        new ReturnToVendorShipmentLineData.Builder().returned("16")
            .alternativeUOM("Ounce")
            .storageBin("L02")
            .product("Basket Ball")
            .build(),
        new ReturnToVendorShipmentLineData.Builder().returnedUOM("Ounce")
            .movementQuantity("2")
            .build(),
        new ReturnToVendorShipmentLineData.Builder().product("Basket Ball")
            .operativeUOM("Ounce")
            .movementQuantity("4")
            .build(),
        new ReturnToVendorShipmentLineData.Builder().returnedUOM("Ounce")
            .movementQuantity("1")
            .build(),
        new ReturnToVendorShipmentLineData.Builder().product("Basket Ball")
            .operativeUOM("Ounce")
            .movementQuantity("2")
            .storageBin("L02")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVb020test() throws ParseException {
    logger.debug("** Start test case [AUMRTVb020] Create, Edit and Delete a line **");

    final ReturnToVendorShipment returnToVendorShipment = (ReturnToVendorShipment) new ReturnToVendorShipment(
        mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendorShipment.create(returnToVendorShipmentHeaderData);
    returnToVendorShipment.assertSaved();
    returnToVendorShipment.assertData(returnToVendorShipmentHeaderVerificationData);

    /** Create a line **/
    PickAndExecuteWindow<ReturnToVendorShipmentLineData> popUp = returnToVendorShipment
        .openPickAndEditLines();
    popUp.filter(returnToVendorShipmentFilterLine);
    popUp.unselectAll();
    popUp.selectRecord(0);
    assertTrue(popUp.getSelectedRows().size() == 1);
    List<Map<String, Object>> rows = popUp.getSelectedRows();
    try {
      popUp.edit(returnToVendorShipmentEditSelectedLine);
    } catch (WebDriverException wde) {
      popUp.edit(returnToVendorShipmentEditSelectedLine);
    }
    Sleep.trySleep();
    popUp.process();
    ReturnToVendorShipment.Lines returnToVendorShipmentLines = returnToVendorShipment.new Lines(
        mainPage);
    returnToVendorShipmentLines.assertData(returnToVendorShipmentCreateLine);

    /** Edit a line **/
    popUp = returnToVendorShipment.openPickAndEditLines();
    Sleep.trySleep(3000);
    assertTrue(popUp.getRows().size() > 0);
    rows = popUp.getSelectedRows();
    if (rows.size() > 0) {
      assertTrue(rows.get(0).get("storageBin$_identifier").equals("L02"));
    } else {
      assertTrue(false);
    }

    popUp.unselectAll();
    popUp.selectRecord(0);
    popUp.edit(returnToVendorShipmentModifySelectedLine);
    popUp.process();
    Sleep.trySleep(3000);
    returnToVendorShipmentLines.assertData(returnToVendorShipmentEditLine);

    /** Delete a line **/
    popUp = returnToVendorShipment.openPickAndEditLines();
    popUp.unselectAll();
    popUp.process();
    returnToVendorShipmentLines.assertCount(0);

    logger.debug("** End test case [AUMRTVb020] Create, Edit and Delete a line **");
  }

}
