/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 *
 * This class was automatically generated on 2011-03-09 21:37:14
 * Contributor(s):
 *   Nono Carballo <nonofce@gmail.com>
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.aum.testsuites.returntovendor;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.goodsreceipt.GoodsReceiptLinesData;
import com.openbravo.test.integration.erp.data.procurement.transactions.returntovendor.ReturnToVendorHeaderData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductCompleteSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ReturnToVendorLineSelectorData;
import com.openbravo.test.integration.erp.modules.client.application.gui.pickandexecute.PickAndExecuteWindow;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.GoodsReceipt;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.ReturnToVendor;
import com.openbravo.test.integration.selenium.Sleep;

/**
 *
 * Class for Test AUMRTVa050
 *
 * @author Nono Carballo
 *
 */

@RunWith(Parameterized.class)
public class AUMRTVa050ReturnedQtyInOthers extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /** Data for Test */
  GoodsReceiptHeaderData goodsReceiptHeader;
  GoodsReceiptLinesData goodsReceiptLine;
  ReturnToVendorHeaderData documentCreated;
  ReturnToVendorLineSelectorData linePicked;
  ReturnToVendorLineSelectorData linePickedEdit;

  public AUMRTVa050ReturnedQtyInOthers(GoodsReceiptHeaderData goodsReceiptHeader,
      GoodsReceiptLinesData goodsReceiptLine, ReturnToVendorHeaderData documentCreated,
      ReturnToVendorLineSelectorData linePicked, ReturnToVendorLineSelectorData linePickedEdit) {
    super();
    this.goodsReceiptHeader = goodsReceiptHeader;
    this.goodsReceiptLine = goodsReceiptLine;
    this.documentCreated = documentCreated;
    this.linePicked = linePicked;
    this.linePickedEdit = linePickedEdit;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  @Parameters
  public static Collection<Object[]> returnToVendorValues() {
    Object[][] data = new Object[][] { {
        new GoodsReceiptHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new GoodsReceiptLinesData.Builder()
            .product(new ProductCompleteSelectorData.Builder().name("Basket Ball").build())
            .operativeQuantity("10")
            .storageBin("L01")
            .build(),
        new ReturnToVendorHeaderData.Builder()
            .businessPartner(
                new BusinessPartnerSelectorData.Builder().name("Vendor sports").build())
            .build(),
        new ReturnToVendorLineSelectorData.Builder().product("Basket Ball").build(),
        new ReturnToVendorLineSelectorData.Builder().returned("3")
            .returnReason("Defective")
            .returnedUOM("Ounce")
            .build() } };
    return Arrays.asList(data);
  }

  @Test
  public void AUMRTVa050test() throws ParseException {
    logger.debug("** Start test case [AUMRTVa050] Verify Returned Qty in Others in PE Window **");

    GoodsReceipt goodsReceipt = (GoodsReceipt) new GoodsReceipt(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    goodsReceipt.create(goodsReceiptHeader);
    goodsReceipt.assertSaved();
    GoodsReceipt.Lines goodsReceiptLines = goodsReceipt.new Lines(mainPage);
    goodsReceiptLines.create(goodsReceiptLine);
    goodsReceiptLines.assertSaved();
    goodsReceipt.complete();
    String documentNo = (String) goodsReceipt.getData("documentNo");
    linePicked.addDataField("shipmentNumber", documentNo);

    ReturnToVendor returnToVendor = (ReturnToVendor) new ReturnToVendor(mainPage).open();
    mainPage.waitForDataToLoad();
    Sleep.trySleep();
    returnToVendor.create(documentCreated);
    returnToVendor.assertSaved();
    PickAndExecuteWindow<ReturnToVendorLineSelectorData> popup = returnToVendor
        .openPickAndEditLines();
    popup.filter(linePicked);
    popup.edit(linePickedEdit);
    popup.process();
    returnToVendor.book();

    returnToVendor.create(documentCreated);
    popup = returnToVendor.openPickAndEditLines();
    popup.filter(linePicked);
    List<Map<String, Object>> rows = popup.getSelectedRows();
    assertTrue(rows.size() == 1);
    assertTrue(((Long) rows.get(0).get("returnQtyOtherRM")).intValue() == 6);

    logger.debug("** End test case [AUMRTVa050] Verify Returned Qty in Others in PE Window **");

  }

}
