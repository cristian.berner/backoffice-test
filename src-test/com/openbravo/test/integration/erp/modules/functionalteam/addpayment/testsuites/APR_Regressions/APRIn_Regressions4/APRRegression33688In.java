/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2016-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_Regressions.APRIn_Regressions4;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.SelectedPaymentData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.paymentin.PaymentInHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.sales.transactions.salesinvoice.SalesInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.PaymentIn;
import com.openbravo.test.integration.erp.testscripts.sales.transactions.SalesInvoice;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Test regression 33688
 *
 * @author rqueralta
 */
@RunWith(Parameterized.class)
public class APRRegression33688In extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  AccountData accountHeaderData;
  PaymentMethodData paymentMethodData;
  PaymentMethodData verifyPaymentMethodData;
  PaymentInHeaderData paymentInHeaderData;
  AddPaymentPopUpData addPaymentPopUpData;
  SalesInvoiceHeaderData salesInvoiceHeaderData;
  SalesInvoiceLinesData salesInvoiceLineData;
  PaymentInHeaderData paymentInHeaderData2;
  TransactionsData transactionsData;

  /**
   * Class constructor.
   *
   */
  public APRRegression33688In(AccountData accountHeaderData, PaymentMethodData paymentMethodData,
      PaymentMethodData verifyPaymentMethodData, PaymentInHeaderData paymentInHeaderData,
      AddPaymentPopUpData addPaymentPopUpData, SalesInvoiceHeaderData salesInvoiceHeaderData,
      SalesInvoiceLinesData salesInvoiceLineData, PaymentInHeaderData paymentInHeaderData2,
      TransactionsData transactionsData) {
    this.accountHeaderData = accountHeaderData;
    this.paymentMethodData = paymentMethodData;
    this.verifyPaymentMethodData = verifyPaymentMethodData;
    this.paymentInHeaderData = paymentInHeaderData;
    this.addPaymentPopUpData = addPaymentPopUpData;
    this.salesInvoiceHeaderData = salesInvoiceHeaderData;
    this.salesInvoiceLineData = salesInvoiceLineData;
    this.paymentInHeaderData2 = paymentInHeaderData2;
    this.transactionsData = transactionsData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { { new AccountData.Builder().name("Spain Bank").build(),
        new PaymentMethodData.Builder().paymentMethod("Acc-8").build(),
        new PaymentMethodData.Builder().uponDepositUse("Cleared Payment Account").build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-8 (Transactions)")
            .amount("408.20")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new AddPaymentPopUpData.Builder().build(),
        new SalesInvoiceHeaderData.Builder().organization("Spain")
            .transactionDocument("AR Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .partnerAddress(".Pamplona, Street Customer center nº1")
            .paymentTerms("30 days, 5")
            .paymentMethod("Acc-8 (Transactions)")
            .build(),
        new SalesInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("FGA")
                .priceListVersionName("Customer A")
                .build())
            .tax("Exempt")
            .invoicedQuantity("1")
            .unitPrice("3.38")
            .build(),
        new PaymentInHeaderData.Builder().organization("Spain")
            .documentType("AR Receipt")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .paymentMethod("Acc-8 (Transactions)")
            .amount("1.38")
            .account("Spain Bank - EUR")
            .currency("EUR")
            .build(),
        new TransactionsData.Builder().depositAmount("1.38").documentNo("").build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 33688 - Cannot post a financial account transaction if the invoice is partially
   * paid by using credit.
   *
   * @throws ParseException
   */
  @Test
  public void APRRegression33688InTest() throws ParseException {
    logger.info(
        "** Start of test case [APRRegression33688In] Test regression 33688 - Cannot post a financial account transaction if the invoice is partially paid by using credit. **");

    // Check payment method is configured to post only financial account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.PaymentMethod paymentMethod = financialAccount.new PaymentMethod(mainPage);
    paymentMethod.select(paymentMethodData);
    paymentMethod.edit(verifyPaymentMethodData);
    // Create a Payment In of 408.20 for Customer A. and leave the amount to be used later
    PaymentIn paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData);
    paymentIn.assertSaved();
    AddPaymentProcess addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    String paymentNo = addPaymentProcess.getParameterValue("payment_documentno").toString();
    addPaymentProcess.process("Process Received Payment(s) and Deposit",
        "Leave the credit to be used later", addPaymentPopUpData);
    paymentIn.assertData(new PaymentInHeaderData.Builder().status("Deposited not Cleared").build());
    // Create a Sales Invoice with a total gross amount of 3.38 EUR. Do not use credit
    SalesInvoice salesInvoice = new SalesInvoice(mainPage).open();
    salesInvoice.create(salesInvoiceHeaderData);
    salesInvoice.assertSaved();
    String invoiceNo = salesInvoice.getData("documentNo").toString();
    SalesInvoice.Lines salesInvoiceLine = salesInvoice.new Lines(mainPage);
    salesInvoiceLine.create(salesInvoiceLineData);
    salesInvoiceLine.assertSaved();
    salesInvoice.complete();
    // Create a new Payment In of 1.38 EUR
    paymentIn = new PaymentIn(mainPage).open();
    paymentIn.create(paymentInHeaderData2);
    paymentIn.assertSaved();
    addPaymentProcess = paymentIn.addDetailsOpen();
    addPaymentProcess.collapseOrder();
    addPaymentProcess.selectCredit(new SelectedPaymentData.Builder().documentNo(paymentNo).build());
    addPaymentProcess.getCreditToUseGrid().filter("description", "Amount");
    Sleep.trySleep(3500);
    addPaymentProcess.editCreditToUseRecord(0,
        new SelectedPaymentData.Builder().paymentAmount("2.00").build());
    Sleep.trySleep(3500);
    addPaymentProcess.getCreditToUseGrid().waitForDataToLoad();
    addPaymentProcess.expandOrder();
    addPaymentProcess.getOrderInvoiceGrid().unselectAll();
    addPaymentProcess.selectPayment(new SelectedPaymentData.Builder().invoiceNo(invoiceNo).build());
    addPaymentProcess.getOrderInvoiceGrid().filter("outstandingAmount", "3.38");
    Sleep.trySleep(3500);
    addPaymentProcess.process("Process Received Payment(s) and Deposit");

    paymentNo = (String) paymentIn.getData("documentNo");
    transactionsData.addDataField("finPayment$documentNo", paymentNo);

    // Navigate to the financial account transaction created and try to post it
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);
    FinancialAccount.Transaction transaction = financialAccount.new Transaction(mainPage);
    transaction.select(transactionsData);
    Sleep.trySleep(3500);
    transaction.post();

    logger.info(
        "** End of test case [APRRegression33688In] Test regression 33688 - Cannot post a financial account transaction if the invoice is partially paid by using credit. **");
  }
}
