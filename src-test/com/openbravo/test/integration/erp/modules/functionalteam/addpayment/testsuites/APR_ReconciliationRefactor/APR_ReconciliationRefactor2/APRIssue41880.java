/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2019 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Andy Armaignac <collazoandy4@gmail.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor2;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.AddPaymentPopUpData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.PaymentMethodData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.PriceListVersionData;
import com.openbravo.test.integration.erp.data.masterdata.pricing.pricelist.ProductPriceData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentDetailsData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PaymentPlanData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceHeaderData;
import com.openbravo.test.integration.erp.data.procurement.transactions.purchaseinvoice.PurchaseInvoiceLinesData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.data.selectors.ProductSimpleSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddPaymentProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListVersionTab;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.PriceListWindow;
import com.openbravo.test.integration.erp.gui.masterdata.pricing.pricelist.ProductPriceTab;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTestException;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.BankStatements;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount.BankStatements.BankStatementLines;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice.PaymentOutPlan;
import com.openbravo.test.integration.erp.testscripts.procurement.transactions.PurchaseInvoice.PaymentOutPlan.PaymentOutDetails;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.OBDate;

/**
 * Test issue 41880
 *
 * @author collazoandy4
 */
@RunWith(Parameterized.class)
public class APRIssue41880 extends OpenbravoERPTest {

  private static final String TRANSACTION_DATE_FIELD = "transactionDate";

  private static final String PRICE_LIST_NAME = "Test Price List Purchase 41880";

  private static final String PAYMENT_METHOD_NAME = "Acc-3 (Payment-Trx-Reconciliation)";

  private static final String TOTAL_AMOUNT = "50.00";

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */
  PriceListData priceList;
  PriceListVersionData priceListVersion;
  ProductPriceData productPrice;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderData;
  PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData;
  PurchaseInvoiceLinesData purchaseInvoiceLineData;
  PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData;
  AccountData accountSelectData;
  PaymentMethodData paymentMethodSelectData;
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;

  PaymentPlanData invoicePaidPaymentOutPlanData;
  PaymentDetailsData invoicePaidPaymentOutDetailsData;

  /**
   * Class constructor.
   *
   */
  public APRIssue41880(PriceListData priceList, PriceListVersionData priceListVersion,
      ProductPriceData productPrice, PurchaseInvoiceHeaderData purchaseInvoiceHeaderData,
      PurchaseInvoiceHeaderData purchaseInvoiceHeaderVerificationData,
      PurchaseInvoiceLinesData purchaseInvoiceLineData,
      PurchaseInvoiceLinesData purchaseInvoiceLineVerificationData,
      PurchaseInvoiceHeaderData completedPurchaseInvoiceHeaderVerificationData,
      AccountData accountSelectData, PaymentMethodData paymentMethodSelectData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, PaymentPlanData invoicePaidPaymentOutPlanData,
      PaymentDetailsData invoicePaidPaymentOutDetailsData) {

    this.priceList = priceList;
    this.priceListVersion = priceListVersion;
    this.productPrice = productPrice;
    this.purchaseInvoiceHeaderData = purchaseInvoiceHeaderData;
    this.purchaseInvoiceHeaderVerificationData = purchaseInvoiceHeaderVerificationData;
    this.completedPurchaseInvoiceHeaderVerificationData = completedPurchaseInvoiceHeaderVerificationData;
    this.purchaseInvoiceLineData = purchaseInvoiceLineData;
    this.purchaseInvoiceLineVerificationData = purchaseInvoiceLineVerificationData;
    this.accountSelectData = accountSelectData;
    this.paymentMethodSelectData = paymentMethodSelectData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.invoicePaidPaymentOutPlanData = invoicePaidPaymentOutPlanData;
    this.invoicePaidPaymentOutDetailsData = invoicePaidPaymentOutDetailsData;

    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> aprIssue41880Values() {
    Object[][] data = new Object[][] { {

        new PriceListData.Builder().organization("USA")
            .name(PRICE_LIST_NAME)
            .currency("EUR")
            .build(),
        new PriceListVersionData.Builder().name("Test Price List Version").build(),

        new ProductPriceData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA").build())
            .listPrice("0.50")
            .standardPrice("0.50")
            .build(),

        new PurchaseInvoiceHeaderData.Builder().organization("USA")
            .transactionDocument("AP Invoice")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .priceList(PRICE_LIST_NAME)
            .paymentTerms("90 days")
            .paymentMethod(PAYMENT_METHOD_NAME)
            .build(),

        new PurchaseInvoiceHeaderData.Builder().transactionDocument("AP Invoice")
            .partnerAddress(".Pamplona, Street Purchase center nº1")
            .priceList(PRICE_LIST_NAME)
            .paymentMethod(PAYMENT_METHOD_NAME)
            .paymentTerms("90 days")
            .build(),

        new PurchaseInvoiceLinesData.Builder()
            .product(new ProductSimpleSelectorData.Builder().searchKey("RMA").build())
            .invoicedQuantity("100")
            .build(),

        new PurchaseInvoiceLinesData.Builder().invoicedQuantity("100")
            .lineNetAmount(TOTAL_AMOUNT)
            .build(),

        new PurchaseInvoiceHeaderData.Builder().documentStatus("Completed")
            .grandTotalAmount(TOTAL_AMOUNT)
            .summedLineAmount(TOTAL_AMOUNT)
            .currency("EUR")
            .paymentComplete(false)
            .build(),

        new AccountData.Builder().name("Accounting Documents DOLLAR").build(),

        new PaymentMethodData.Builder().paymentMethod(PAYMENT_METHOD_NAME).build(),

        new BankStatementHeaderData.Builder().name("Test41880").build(),

        new BankStatementLinesData.Builder()
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Vendor A").build())
            .dramount("125")
            .build(),

        new PaymentPlanData.Builder().paymentMethod(PAYMENT_METHOD_NAME)
            .expected(TOTAL_AMOUNT)
            .paid(TOTAL_AMOUNT)
            .outstanding("0.00")
            .currency("EUR")
            .daysOverdue("0")
            .numberOfPayments("1")
            .build(),

        new PaymentDetailsData.Builder().paid(TOTAL_AMOUNT)
            .writeoff("0.00")
            .paymentMethod(PAYMENT_METHOD_NAME)
            .amount(TOTAL_AMOUNT)
            .account("Accounting Documents DOLLAR - USD")
            .status("Payment Cleared")
            .build(),

        } };
    return Arrays.asList(data);
  }

  /**
   * Test issue 41880 Match Statements for operations with different currencies.
   *
   * @throws OpenbravoERPTestException
   * @throws ParseException
   */
  @Test
  public void APRIssue41880Test() throws OpenbravoERPTestException, ParseException {
    logger.info(
        "** Start of test case [APRIssue41880Test] Test issue 41880 Match Statements for operations with different currencies.**");

    PriceListWindow priceListWindow = new PriceListWindow();
    mainPage.openView(priceListWindow);
    PriceListTab priceListTab = priceListWindow.selectPriceListTab();
    priceListTab.filter(new PriceListData.Builder().name("Test Price List Purchase").build());
    if (priceListTab.getRecordCount() == 0) {
      priceListTab.clearFilters();
      priceListTab.createRecord(priceList);
      PriceListVersionTab priceListVersionTab = priceListWindow.selectPriceListVersionTab();
      priceListVersionTab.createRecord(priceListVersion);
      priceListVersionTab.assertSaved();
      priceListVersionTab.process();
      ProductPriceTab productPriceTab = priceListWindow.selectProductPriceTab();
      productPriceTab.createRecord(productPrice);
    }

    // Create a Purchase invoice
    PurchaseInvoice invoice = new PurchaseInvoice(mainPage).open();
    invoice.create(purchaseInvoiceHeaderData);
    invoice.assertSaved();
    invoice.assertData(purchaseInvoiceHeaderVerificationData);

    PurchaseInvoice.Lines invoiceLines = invoice.new Lines(mainPage);
    invoiceLines.create(purchaseInvoiceLineData);
    invoiceLines.assertSaved();
    invoiceLines.assertData(purchaseInvoiceLineVerificationData);

    invoice.complete();
    invoice.assertProcessCompletedSuccessfully2();
    invoice.assertData(completedPurchaseInvoiceHeaderVerificationData);
    String invoiceNo = (String) invoice.getData("documentNo");

    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountSelectData);
    financialAccount.edit(new AccountData.Builder().matchingAlgorithm("Standard").build());

    BankStatements bankStatements = financialAccount.new BankStatements(mainPage);
    String date = OBDate.CURRENT_DATE;
    if (bankStatements.getRecordCount() != 0) {
      bankStatements.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatements.getData(TRANSACTION_DATE_FIELD), 1);
    }

    bankStatementHeaderData.addDataField(TRANSACTION_DATE_FIELD, date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatements.create(bankStatementHeaderData);
    bankStatements.assertSaved();

    BankStatementLines statementLines = bankStatements.new BankStatementLines(mainPage);
    bankStatementLinesData.addDataField(TRANSACTION_DATE_FIELD, date);
    bankStatementLinesData.addDataField("referenceNo", invoiceNo);
    statementLines.create(bankStatementLinesData);
    statementLines.assertSaved();

    bankStatements.process();
    bankStatements.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    Sleep.trySleep(1000);
    matchStatementProcess.getMatchStatementGrid().filter("referenceNo", invoiceNo);

    AddTransactionProcess transactionProcess = matchStatementProcess.clickAdd(0);

    AddPaymentProcess paymentProcess = transactionProcess.openAddPayment();
    paymentProcess.setParameterValue("fin_paymentmethod_id", PAYMENT_METHOD_NAME);
    paymentProcess.assertData(
        new AddPaymentPopUpData.Builder().c_currency_id("USD").actual_payment("125.00").build());
    paymentProcess.setParameterValue("c_currency_id", "EUR");
    paymentProcess.setParameterValue("conversion_rate", "2.5");
    paymentProcess.assertData(new AddPaymentPopUpData.Builder().actual_payment(TOTAL_AMOUNT)
        .expected_payment(TOTAL_AMOUNT)
        .converted_amount("125.00")
        .build());
    paymentProcess.process("Process Made Payment(s)");

    transactionProcess.process();
    matchStatementProcess.process();

    // Check invoice payment plan
    invoice = new PurchaseInvoice(mainPage).open();
    invoice.select(new PurchaseInvoiceHeaderData.Builder().documentNo(invoiceNo).build());
    PaymentOutPlan invoicePaymentOutPlan = invoice.new PaymentOutPlan(mainPage);
    invoicePaymentOutPlan.assertCount(1);
    invoicePaymentOutPlan.selectWithoutFiltering(0);
    invoicePaymentOutPlan.assertData(invoicePaidPaymentOutPlanData);

    PaymentOutDetails invoicePaymentOutDetails = invoicePaymentOutPlan.new PaymentOutDetails(
        mainPage);
    invoicePaymentOutDetails.assertCount(1);
    invoicePaymentOutDetails.assertData(invoicePaidPaymentOutDetailsData);

    // Restore financial account configuration
    financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountSelectData);
    financialAccount.edit(new AccountData.Builder().matchingAlgorithm("").build());
    financialAccount.assertSaved();

    logger.info(
        "** End of test case [APRIssue41880Test] Test issue 41880 Match Statements for operations with different currencies.**");
  }
}
