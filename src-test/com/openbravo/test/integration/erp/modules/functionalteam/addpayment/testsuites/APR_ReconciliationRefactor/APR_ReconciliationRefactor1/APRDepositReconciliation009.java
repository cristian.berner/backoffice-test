/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2014-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.addpayment.testsuites.APR_ReconciliationRefactor.APR_ReconciliationRefactor1;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.MatchStatementData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.bankstatement.BankStatementHeaderData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.AccountData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.BankStatementLinesData;
import com.openbravo.test.integration.erp.data.financial.receivablespayables.transactions.financialaccount.TransactionsData;
import com.openbravo.test.integration.erp.data.selectors.BusinessPartnerSelectorData;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.AddTransactionProcess;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementGrid;
import com.openbravo.test.integration.erp.gui.financial.receivablespayables.transactions.MatchStatementProcess;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.financial.receivablespayables.transactions.FinancialAccount;
import com.openbravo.test.integration.util.OBDate;

/**
 * Match Automatically a Transaction and a Bank Statement Line
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class APRDepositReconciliation009 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  /** The account header data. */
  AccountData accountHeaderData;
  /** The bank statement data. */
  BankStatementHeaderData bankStatementHeaderData;
  BankStatementLinesData bankStatementLinesData;
  MatchStatementData matchStatementData;
  MatchStatementData matchStatementData2;
  MatchStatementData matchStatementData3;
  MatchStatementData matchStatementData4;

  /**
   * Class constructor.
   *
   * @param accountHeaderData
   *          The account Header Data.
   * @param bankStatementHeaderData
   *          The bank statement Header Data.
   * @param bankStatementLinesData
   *          The bank statement lines Data.
   */
  public APRDepositReconciliation009(AccountData accountHeaderData,
      BankStatementHeaderData bankStatementHeaderData,
      BankStatementLinesData bankStatementLinesData, MatchStatementData matchStatementData,
      MatchStatementData matchStatementData2, MatchStatementData matchStatementData3,
      MatchStatementData matchStatementData4) {
    this.accountHeaderData = accountHeaderData;
    this.bankStatementHeaderData = bankStatementHeaderData;
    this.bankStatementLinesData = bankStatementLinesData;
    this.matchStatementData = matchStatementData;
    this.matchStatementData2 = matchStatementData2;
    this.matchStatementData3 = matchStatementData3;
    this.matchStatementData4 = matchStatementData4;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> APRDepositReconciliation009Values() {
    Object[][] data = new Object[][] { {
        /* Parameters for [APRDepositReconciliation009Test] */
        new AccountData.Builder().name("Spain Bank").build(),
        new BankStatementHeaderData.Builder().name("TestReconciliationD009").build(),
        new BankStatementLinesData.Builder().referenceNo("D009")
            .businessPartner(new BusinessPartnerSelectorData.Builder().name("Customer A").build())
            .gLItem("Salaries")
            .cramount("80.00")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("D009")
            .glitem("Salaries")
            .amount("80")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("P")
            .sender("Customer A")
            .referenceNo("D009")
            .glitem("Salaries")
            .amount("-19")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("D009")
            .glitem("Salaries")
            .amount("99")
            .affinity("AD")
            .matchedDocument("T")
            .matchingType("AD")
            .businessPartner("Customer A")
            .transactionAmount("99")
            .transactionGLItem("Salaries")
            .build(),
        new MatchStatementData.Builder().bankStatementType("D")
            .sender("Customer A")
            .referenceNo("D009")
            .glitem("Salaries")
            .amount("80")
            .affinity("")
            .matchedDocument("")
            .matchingType("")
            .businessPartner("")
            .transactionAmount("")
            .transactionGLItem("")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Match Automatically a Transaction and a Bank Statement Line
   *
   * @throws ParseException
   */
  @Test
  public void APRDepositReconciliation009Test() throws ParseException {
    logger.info("** Start of test case [APRDepositReconciliation009Test]");

    // Select Financial Account
    FinancialAccount financialAccount = new FinancialAccount(mainPage).open();
    financialAccount.select(accountHeaderData);

    // Get the date of the latest Bank Statement
    FinancialAccount.BankStatements bankStatement = financialAccount.new BankStatements(mainPage);
    String date;
    if (bankStatement.getRecordCount() == 0) {
      date = OBDate.CURRENT_DATE;
    } else {
      bankStatement.selectWithoutFiltering(0);
      date = OBDate.addDaysToDate((String) bankStatement.getData("transactionDate"), 1);
    }

    // Create Bank Statement
    bankStatementHeaderData.addDataField("transactionDate", date);
    bankStatementHeaderData.addDataField("importdate", date);
    bankStatement.create(bankStatementHeaderData);
    bankStatement.assertSaved();
    FinancialAccount.BankStatements.BankStatementLines bankStatementLines = bankStatement.new BankStatementLines(
        mainPage);
    bankStatementLinesData.addDataField("transactionDate", date);
    bankStatementLines.create(bankStatementLinesData);
    bankStatementLines.assertSaved();
    bankStatement.process();
    bankStatement.assertProcessCompletedSuccessfully2();

    // Open Match Statement Grid
    @SuppressWarnings("rawtypes")
    MatchStatementProcess matchStatementProcess = financialAccount.openMatchStatement(false);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData.addDataField("banklineDate", date);
    matchStatementData.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData);

    // Add new Transaction
    AddTransactionProcess addTransactionProcess = ((MatchStatementGrid) matchStatementProcess
        .getMatchStatementGrid()).clickAdd(0);
    addTransactionProcess.setParameterValue("depositamt", "99.00");
    addTransactionProcess.process(true);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "-19");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Customer A");
    matchStatementData2.addDataField("banklineDate", date);
    matchStatementData2.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData2);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("amount", "99");
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid().filter("businessPartner", "Customer A");
    matchStatementData3.addDataField("banklineDate", date);
    matchStatementData3.addDataField("trxDate", date);
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData3);

    // Clear matching
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).clickClear(0);
    matchStatementProcess.getMatchStatementGrid().clearFilters();
    matchStatementProcess.getMatchStatementGrid().filter("banklineDate", date);
    matchStatementProcess.getMatchStatementGrid()
        .filter("referenceNo", (String) bankStatementLinesData.getDataField("referenceNo"));
    matchStatementData4.addDataField("banklineDate", date);
    matchStatementData4.addDataField("trxDate", "");
    ((MatchStatementGrid) matchStatementProcess.getMatchStatementGrid()).assertData(0,
        matchStatementData4);

    // Process Match Statement
    matchStatementProcess.process();
    financialAccount.assertProcessCompletedSuccessfully2();

    // Reactivate transaction
    FinancialAccount.Transaction transactions = financialAccount.new Transaction(mainPage);
    transactions.select(new TransactionsData.Builder().status("Deposited not Cleared")
        .transactionDate(date)
        .transactionType("BP Deposit")
        .build());
    transactions.reactivate();
    transactions.assertProcessCompletedSuccessfully2();

    logger.info("** End of test case [APRDepositReconciliation009Test]");
  }

  @AfterClass
  public static void tearDown() {
    OpenbravoERPTest.forceLoginRequired();
  }
}
