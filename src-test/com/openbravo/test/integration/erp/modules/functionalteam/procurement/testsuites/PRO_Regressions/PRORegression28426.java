/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.procurement.testsuites.PRO_Regressions;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.openbravo.test.integration.erp.data.LogInData;
import com.openbravo.test.integration.erp.data.masterdata.product.PriceData;
import com.openbravo.test.integration.erp.data.masterdata.product.ProductData;
import com.openbravo.test.integration.erp.data.sales.setup.commission.CommissionHeaderData;
import com.openbravo.test.integration.erp.data.selectors.ProductSelectorData;
import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.masterdata.product.Product;
import com.openbravo.test.integration.erp.testscripts.sales.setup.Commission;

/**
 * Test regression 28426
 *
 * @author aferraz
 */
@RunWith(Parameterized.class)
public class PRORegression28426 extends OpenbravoERPTest {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /* Data for this test. */

  ProductData productData;
  PriceData productPriceData;
  CommissionHeaderData commissionData;

  /**
   * Class constructor.
   *
   */
  public PRORegression28426(ProductData productData, PriceData productPriceData,
      CommissionHeaderData commissionData) {
    this.productData = productData;
    this.productPriceData = productPriceData;
    this.commissionData = commissionData;
    logInData = new LogInData.Builder().userName("QAAdmin").password("QAAdmin").build();
  }

  /**
   * Test parameters.
   *
   * @return collection of object arrays with data for the test
   *
   */
  @Parameters
  public static Collection<Object[]> salesOrderValues() {
    Object[][] data = new Object[][] { {
        new ProductData.Builder().organization("Spain")
            .uOM("Unit")
            .productCategory("Raw materials")
            .taxCategory("VAT 10%")
            .purchase(true)
            .sale(false)
            .productType("Item")
            .stocked(true)
            .build(),
        new PriceData.Builder().priceListVersion("Purchase")
            .standardPrice("2.00")
            .listPrice("2.00")
            .build(),

        new CommissionHeaderData.Builder().organization("Spain")
            .businessPartner("Employee A")
            .currency("EUR")
            .frequencyType("Monthly")
            .build() } };
    return Arrays.asList(data);
  }

  /**
   * Test regression 28426
   *
   * @throws ParseException
   */
  @Test
  public void PRORegression28426Test() throws ParseException {
    logger.info("** Start of test case [PRORegression28426] Test regression 28426. **");

    String productName = "Test28426Purchase-";
    int num = Product.ProductTab.getRecordCount(mainPage,
        new ProductData.Builder().name(productName).build());
    productName = productName + String.valueOf(num);

    productData.addDataField("searchKey", productName);
    productData.addDataField("name", productName);
    Product.ProductTab.create(mainPage, productData);
    Product.Price.create(mainPage, productPriceData);

    commissionData.addDataField("name", productName);
    commissionData.addDataField("product",
        new ProductSelectorData.Builder().name(productName).priceListVersion("Purchase").build());
    Commission.CommissionTab.create(mainPage, commissionData);

    logger.info("** End of test case [PRORegression28426] Test regression 28426. **");
  }
}
