/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2015-2018 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.functionalteam.sales.testsuites.SAL_Regressions;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.openbravo.test.integration.erp.testscripts.OpenbravoERPTest;
import com.openbravo.test.integration.erp.testscripts.SuiteThatStopsIfFailure;
import com.openbravo.test.integration.selenium.SeleniumSingleton;

@RunWith(SuiteThatStopsIfFailure.class)
@Suite.SuiteClasses({ SALRegression29171.class, SALRegression28426.class, SALRegression28226.class,
    SALRegression29963.class, SALRegression31873In1.class, SALRegression31873In3.class,
    SALRegression31873In4.class, SALRegression33382.class, SALRegression33040.class,
    SALRegression32428.class, SALRegression34392.class, SALRegression33550.class,
    SALRegression41174.class })
// Tests temporally commented to check the rest of the tests are working properly
// SALRegression33877.class,
public class SAL_RegressionSuite {
  // No content is required, this is just the definition of a test suite.
  @AfterClass
  public static void tearDown() {
    SeleniumSingleton.INSTANCE.quit();
    OpenbravoERPTest.seleniumStarted = false;
    OpenbravoERPTest.forceLoginRequired();
  }
}
