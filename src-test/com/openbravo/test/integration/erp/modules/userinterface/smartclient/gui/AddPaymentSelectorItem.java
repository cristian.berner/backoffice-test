/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Executes actions on OpenbravoERP selector item built upon SmartClient user interface library.
 *
 * @author lorenzo.fidalgo
 *
 */
public class AddPaymentSelectorItem extends SelectorItem {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  public AddPaymentSelectorItem(String objectString) {
    super(objectString);
  }

  /**
   * Open the process definition pop up clicking on the icon.
   */
  public void openProcessDefinitionSelectorPopUp() {
    logger.trace("Opening the process definition selector pop up.");
    buttonProcess.click();
  }

}
