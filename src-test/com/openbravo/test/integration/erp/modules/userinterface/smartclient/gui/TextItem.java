/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2017 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>,
 *  Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.openbravo.test.integration.selenium.Attribute;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.ConfigurationProperties;

/**
 * Executes actions on OpenbravoERP text items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class TextItem extends FormItem implements InputField {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string to locate the text box. The parameter is the form item SmartClient locator.
   */
  private final static String FORMAT_LOCATOR_SMART_CLIENT_TEXTBOX = "%s/textbox";
  /**
   * Formatting string used to get the display value of the SmartClient object. The parameter is the
   * object string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_DISPLAY_VALUE = "%s.getDisplayValue()";
  /**
   *
   */
  private int charCount = 0;
  /**
   * Indicates whether the contents of the text item had to be cleared
   */
  private boolean textFieldCleared = false;

  // TODO: <chromedriver> Remove this, since it's probably no longer used
  /**
   * Field that can be used to identify a text item by its type of field
   */
  // private String fieldType = "";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public TextItem(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the text item, but after this the text item will not be
   * ready to be used. It has to be initialized using the function setSmartClientLocator in order to
   * be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public TextItem() {
  }

  /**
   * Set the SmartClient Locator of the text item.
   *
   * @param smartClientLocator
   *          The SmartClient locator of the text item.
   */
  @Override
  public void setSmartClientLocator(String smartClientLocator) {
    super.setSmartClientLocator(
        String.format(FORMAT_LOCATOR_SMART_CLIENT_TEXTBOX, smartClientLocator));
  }

  /**
   * Get the combo box locator. This is the full path, including the form locator and the item
   * locator.
   *
   * @return The form item locator.
   */
  @Override
  protected String getSmartClientLocator() {
    final String formItemLocator = super.getSmartClientLocator();
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_TEXTBOX, formItemLocator);
  }

  /**
   * Clear the contents of the text item.
   */
  public void clear() {
    logger.trace("Clearing the contents of the text item {}", this::getSmartClientLocator);
    // XXX this was not working on the Sales Order Lines quantity field. The workaround bellow was
    // added instead.
    // SeleniumSingleton.INSTANCE.findElementByScLocator(smartClientLocator).clear();
    if (!(this instanceof NumberItem)) {
      if (getValue().trim().length() != 0) {
        charCount = getValue().trim().length() + 1;
      }
    }

    try {
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(Keys.END);
    } catch (StaleElementReferenceException e) {
      // Stale exception was repeating the previous command, creating an Element Not Visible
      // exception but preventing below catch to happen. So I copied what is done for focusing the
      // element here.
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.END);
      } catch (StaleElementReferenceException sere) {
        logger.trace(
            "StaleElementReferenceException was thrown and caught. Sleep and try sendKeys again.");
        Sleep.trySleep(7500);
        try {
          SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
              .sendKeys(Keys.END);
        } catch (StaleElementReferenceException sere2) {
          Sleep.trySleep();
          logger.warn(
              "Two StaleElementReferenceException in a row. Avoiding to sendKeys. Test could end as a 'failure'");
        }
      }
    } catch (ElementNotVisibleException e2) {
      // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
      // Test ADMe failed to create a Price List Version since the scroll did not automatically put
      // the first field visible.
      // So the solution is to catch the exception and force the scroll to get the field into view.
      // Note: Probably this focusing could have been moved to be the standard behavior, but I
      // prefer to keep it as an exception since it is like this for us.
      // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
      // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(Keys.END);
    } catch (WebDriverException exception) {
      // This extra catch is required because "WebDriverException" started to be thrown after
      // updating to Geckodriver 0.15.0 + Se 3.3.1 + FF 52.1.2esr. It is now required sometimes to
      // wait before clicking.
      // TODO L2: Remove static Sleep.trySleep(10000);
      Sleep.trySleep(10000);
      logger.warn("WebDriverException .Exception: {}", exception::getMessage);
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(Keys.END);
    }
    textFieldCleared = !isEmpty();
    while (!isEmpty()) {
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.BACK_SPACE);
      } catch (StaleElementReferenceException sere) {
        logger.warn(sere::getMessage);
        Sleep.trySleep(2000);
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
            .sendKeys(Keys.BACK_SPACE);
      } catch (ElementNotInteractableException enie) {
        logger.warn(enie::getMessage);
        Actions actionToBackSpace = new Actions(SeleniumSingleton.INSTANCE);
        actionToBackSpace
            .sendKeys(SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()),
                Keys.BACK_SPACE)
            .build()
            .perform();
      }
    }
    // TODO L00: Remove ASAP if not required
    Sleep.trySleep();
  }

  /**
   * Check if the text field is empty.
   *
   * @return true if the text field is empty. Otherwise, false.
   */
  public boolean isEmpty() {
    // Workaround to ensure that fields are cleared properly
    // This prevents that fields finished from clearing (thus, in blank or "" state) are written
    // back again
    // due to a bug related with the command in lines 156-157 when clearing "*" string
    if (charCount > 0 && ConfigurationProperties.INSTANCE.getSelectorInputPref() == 't') {
      charCount--;
      if (charCount == 0) {
        if (getValue().trim().length() != charCount) {
          SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
              .sendKeys(Keys.SPACE);
        }
      }
    }
    //
    return getValue().trim().length() == 0;
  }

  /**
   * Enter a text on the text item.
   *
   * This function will clear all the contents of the text item before entering the text.
   *
   * @param text
   *          The text to enter.
   */
  public void enterText(String text) {
    enterText(text, true);
  }

  /**
   * Enter a text on the text item.
   *
   * @param text
   *          The text to enter.
   * @param clear
   *          If true, the contents of the text item will be cleared before entering the text.
   *          Otherwise, the text will be appended to the contents.
   */
  public void enterText(String text, boolean clear) {
    if (clear) {
      Sleep.setFluentWait(300, 4000, WebDriverException.class).until(wd -> {
        clear();
        return new Object();
      });
    }
    logger.trace("Entering the text '{}'.", text);
    try {
      if (text.contains("100") || text.contains("200") || text.contains("-1")) {
        logger.info("Sleeping to avoid performance problems in MatchSeveralTransaction tests");
        // TODO L1:Remove the following static sleep once it is stable.
        Sleep.trySleep();
      }
      // FIXME: <chromedriver> This is a workaround to the non functioning sendKeys method when
      // sending text
      if (!SeleniumSingleton.runsChrome()) {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(text);
        SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(getSmartClientLocator() + ".storeValue(" + text + ");");
      } else {
        // What each character causes: 5 -> Backspace, 6 -> Tab, / -> S
        if (!text.matches(".*[56/].*")) {
          logger.debug("Sending text with the Selenium API sendKeys(), default way");
          SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(text);
        } else {
          logger.debug(
              "[chromedriver] The text to be sent contains characters which could cause undesirable actions when parsed by sendKeys() method in Chrome");
          logger.debug(
              "Skipping the use of sendKeys() entirely and thus leaving the text field blank to be completed by the workaround");
        }
        String dispValue = getValueChrome();
        logger.debug("The TEXT RECEIVED is: '{}'", dispValue);
        if (!dispValue.contentEquals(text) || textFieldCleared) {
          String scrToExec = String.format(FORMAT_WEB_ELEMENT_SMART_CLIENT_LOCATOR,
              getSmartClientLocator());
          if (!dispValue.contentEquals(text)) {
            logger.warn(
                "[chromedriver] The text received doesn't match with the text sent, possibly due to the known issue of sendKeys() method with Chrome");
            logger.debug("Attempting to send text in the alternate way for ChromeDriver");
            Sleep.trySleep(3000);
            SeleniumSingleton.INSTANCE
                .executeScriptWithReturn(scrToExec + ".value = '" + text + "';");
            SeleniumSingleton.INSTANCE
                .executeScriptWithReturn(scrToExec + ".setAttribute('value','" + text + "');");
            logger.debug("The TEXT RECEIVED with the method for ChromeDriver is: '{}'",
                getValueChrome());
          } else if (textFieldCleared && !(this instanceof NumberItem)) {
            logger.debug(
                "[chromedriver] Text field contents needed to be cleared, proceeding with sendKeys() workaround for Chrome");
            textFieldCleared = false;
          }
          logger.debug("[chromedriver] Sleeping before further applying the workaround steps");
          Sleep.trySleep(3000);
          String textItemClass = (String) SeleniumSingleton.INSTANCE
              .executeScriptWithReturn(scrToExec + ".getAttribute('class');");
          String textItemName = (String) SeleniumSingleton.INSTANCE
              .executeScriptWithReturn(scrToExec + ".getAttribute('name');");
          if (this instanceof NumberItem && !scrToExec.contains("filterEditor")) {
            // Trying to tab to make effective the change in a NumberItem type field
            logger.debug(
                "[chromedriver] Sending tab key to submit the value of the NumberItem type field");
            SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
                .sendKeys(Keys.TAB);
          } else if (!scrToExec.contains("OBSelectorItem")
              || (textItemClass.contains("OBFormFieldSelectInput")
                  && textItemName.equals("finPayment"))) {
            // Checking whether sending a space works to do backspace, since it doesn't work for
            // some fields
            logger.debug(
                "[chromedriver] Typing a space (' ') and removing it to force a refresh in the grid or field");
            SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
                .sendKeys(Keys.END);
            SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
                .sendKeys(Keys.SPACE);
            if (getValueChrome().length() == text.length() && textItemName.equals("finPayment")) {
              // Typing text with one extra space, so it is erased by the backspace and then can
              // force refresh on the combo box
              SeleniumSingleton.INSTANCE
                  .executeScriptWithReturn(scrToExec + ".value = '" + text + " ';");
              SeleniumSingleton.INSTANCE
                  .executeScriptWithReturn(scrToExec + ".setAttribute('value','" + text + " ');");
              SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
                  .sendKeys(Keys.END);
            }
            while (getValueChrome().length() > text.length()) {
              SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
                  .sendKeys(Keys.BACK_SPACE);
            }
          }
          // Press enter when the field filled is a filter to force grid refresh on popups
          // Contents to check if contained have been extracted from the following SmartClient
          // locator
          // //autoID[Class=OBSelectorPopupWindow||index=35||length=37||classIndex=0||classLength=1||roleIndex=0||roleLength=1
          // ||title=Business%20Partner||scRole=dialog]/item[0][Class="OBGrid"]/filterEditor/editRowForm/item[name=name]/textbox
          // if (scrToExec.contains("filterEditor") && scrToExec.contains("Popup")
          // && !fieldType.equals(ClassNamesConstants.CLASS_FOREIGN_KEY_COMBO_ITEM)) {
          // logger
          // .debug("[chromedriver] Hitting 'Enter' to force a refresh in the grid and sleeping");
          // SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(
          // Keys.ENTER);
          // }
          Sleep.trySleep(3000);
        }
      }
    } catch (StaleElementReferenceException e) {
      // Stale exception was repeating the previous command, creating an Element Not Visible
      // exception but preventing below catch to happen. So I copied what is done for focusing the
      // element here.
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          this::getSmartClientLocator);
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      // FIXME: <chromedriver> Retrying to enter text including the above workaround
      if (!SeleniumSingleton.runsChrome()) {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(text);
        SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(getSmartClientLocator() + ".storeValue(" + text + ");");
      } else {
        logger.debug("Retrying to enter the text after StaleElementReferenceException");
        enterText(text);
      }
    } catch (ElementNotVisibleException e2) {
      // This is a workaround made when updating from FF19 & Selenium2.33 to FF33 & Selenium2.44
      // Test ADMe failed to create a Price List Version since the scroll did not automatically put
      // the first field visible.
      // So the solution is to catch the exception and force the scroll to get the field into view.
      // Note: Probably this focusing could have been moved to be the standard behavior, but I
      // prefer to keep it as an exception since it is like this for us.
      // Note 2: I do not understand why the update failed, but I've tested other FF and Selenium
      // combinations and as low as FF24 and Selenium2.37 the issue can be reproduced.
      logger.warn(
          "Interaction with field [{}] is not possible since it is not visibile, forcing the scroll.",
          getSmartClientLocator());
      WebElement obj = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator());
      SeleniumSingleton.INSTANCE.executeScript("arguments[0].scrollIntoView(false);", obj);
      // Trying once again
      // FIXME: <chromedriver> Retrying to enter text including the above workaround
      if (!SeleniumSingleton.runsChrome()) {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator()).sendKeys(text);
        SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(getSmartClientLocator() + ".storeValue(" + text + ");");
      } else {
        logger.debug("Retrying to enter the text after StaleElementReferenceException");
        enterText(text);
      }
    }

  }

  /**
   * Enter a text on the box.
   *
   * @param value
   *          The text to enter.
   */
  @Override
  public void setValue(Object value) {
    if (value instanceof String) {
      enterText((String) value);
    } else {
      throw new IllegalArgumentException("Valid values for text items are only Strings.");
    }
  }

  /**
   * Get the value of the text box.
   *
   * @return the value of the text box.
   */
  @Override
  public String getValue() {
    String value;
    try {
      value = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
          .getAttribute(Attribute.VALUE.getName());
    } catch (InvalidElementStateException exception) {
      logger.info("Item '{}' has no value attribute. Getting the display value instead.",
          getInformationForLog());
      value = getDisplayValue();
    } catch (StaleElementReferenceException | NoSuchElementException exception) {
      return getValue();
    }
    if (value == null) {
      logger.info("Item '{}' has no value attribute. Getting the display value instead.",
          getInformationForLog());
      value = getDisplayValue();
    }
    return value;
  }

  // FIXME: <chromedriver> Temporary method to get the value of a text field in Chrome
  /**
   * Get the value of the text box.
   *
   * @return the value of the text box.
   */
  public String getValueChrome() {
    String value;
    try {
      value = SeleniumSingleton.INSTANCE.findElementByScLocator(getSmartClientLocator())
          .getAttribute("value");
    } catch (InvalidElementStateException exception) {
      logger.info("Item '{}' has no value attribute. Getting the display value instead.",
          getInformationForLog());
      value = getDisplayValue();
    } catch (StaleElementReferenceException exception) {
      return getValueChrome();
    } catch (WebDriverException wde2) {
      try {
        value = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            String.format("window.isc.AutoTest.getElement(\"%s\")", getSmartClientLocator())
                + ".getAttribute('value')");
      } catch (WebDriverException wde3) {
        value = null;
      }
    }
    if (value == null) {
      logger.info("Item '{}' has no value attribute. Getting the display value instead.",
          getInformationForLog());
      value = getDisplayValue();
    }
    return value;
  }

  /**
   * Get the display value of the text item.
   *
   * @return the value displayed in the text item.
   */
  public String getDisplayValue() {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_DISPLAY_VALUE, objectString));
  }
}
