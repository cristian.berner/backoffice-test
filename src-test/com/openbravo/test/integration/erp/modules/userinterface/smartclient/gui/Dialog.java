/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP dialogs built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class Dialog extends Canvas {

  /** Formatting string to get the dialog title. The parameter is the object string. */
  private final static String FORMAT_SMART_CLIENT_GET_TITLE = "%s.title";
  /** Formatting string to get the dialog message. The parameter is the object string. */
  private final static String FORMAT_SMART_CLIENT_GET_MESSAGE = "%s.message";

  /**
   * Class constructor.
   *
   * @param objectString
   *          the string that can be used with SeleniumSingleton.INSTANCE.getEval function to get
   *          the object.
   */
  public Dialog(String objectString) {
    super(objectString);
  }

  /**
   * Get the dialog title.
   *
   * @return the dialog title.
   */
  @Override
  public String getTitle() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_TITLE, objectString));
  }

  /**
   * Get the dialog message.
   *
   * @return the dialog message.
   */
  private String getMessage() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_MESSAGE, objectString));
  }

  /**
   * Assert the message of the dialog.
   *
   * @param message
   *          The expected message.
   */
  public void assertMessage(String message) {
    assertThat(getMessage(), is(equalTo(message)));
  }
}
