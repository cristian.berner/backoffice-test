/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

/**
 * Interface for the input fields.
 *
 * @author elopio
 *
 */
public interface InputField {

  /**
   * Set the value of the input field.
   *
   * @param value
   *          The value to set on the field.
   */
  public void setValue(Object value);

  /**
   * Get the value of the input field.
   *
   * @return the current value of the field.
   */
  public Object getValue();

  /**
   * Get the title of the input field.
   *
   * @return the title of the input field.
   */
  public String getTitle();

  /**
   * Set the focus to the field.
   */
  public void focus();

  /**
   * Wait until the field is enabled.
   */
  public void waitUntilEnabled();

  /**
   * Return true if the field is present.
   *
   * @return true if the field is present. Otherwise, false.
   */
  public boolean isPresent();

  /**
   * Return true if the field is visible.
   *
   * @return true if the field is visible. Otherwise, false.
   */
  public boolean isVisible();

  /**
   * Return true if the field is enabled.
   *
   * @return true if the field is enabled. Otherwise, false.
   */
  public boolean isEnabled();
}
