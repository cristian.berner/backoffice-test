/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>
 *  Iñaki Garcia <inaki.garcia@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;

/**
 * Executes actions on OpenbravoERP selector item built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class SelectorItem extends FormItem {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string to get the target entity of the searth item. The parameter is object string.
   */
  private final static String FORMAT_SMART_CLIENT_GET_TARGET_ENTITY = "%s.targetEntity";
  /**
   * Formatting string to get the type item. The parameter is object string.
   */
  private final static String FORMAT_SMART_CLIENT_GET_TYPE = "%s.type";
  /**
   * Formatting string to get the selector pop up window. The parameter is the object string.
   */
  protected final static String FORMAT_SMART_CLIENT_GET_SELECTOR_WINDOW = "%s.selectorWindow";
  /**
   * Formatting string to get the SmartClient locator of the search button. The parameter is the
   * selector item locator.
   */
  // XXX this is fragile.
  protected final static String FORMAT_SMART_CLIENT_LOCATOR_BUTTON_SEARCH = "%s/[icon='_0']";
  @Deprecated
  protected final static String FORMAT_SMART_CLIENT_LOCATOR_BUTTON_OPEN_PROCESS = "%s/[icon='_1']";

  /* Components. */
  /** The select item with the list of possible values. */
  private final SelectItem selectItem;
  /** The button that opens the selector pop up. */
  private final Button buttonSearch;
  /** The button that opens the selector process definition. */
  protected final Button buttonProcess;

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public SelectorItem(String objectString) {
    super(objectString);
    this.selectItem = new SelectItem(objectString);
    this.buttonSearch = new Button();
    this.buttonSearch.setSmartClientLocator(getSearchButtonLocator());
    this.buttonProcess = new Button();
    this.buttonProcess.setSmartClientLocator(getOpenProcessButtonLocator());
  }

  /**
   * Click the selector item.
   */
  @Override
  public void click() {
    selectItem.click();
  }

  /**
   * Get the SmartClient locator of the picker button component.
   *
   * @return the SmartClient locator of the picker button component.
   */
  private String getSearchButtonLocator() {
    return String.format(FORMAT_SMART_CLIENT_LOCATOR_BUTTON_SEARCH, getSmartClientLocator());
  }

  /**
   * Get the SmartClient locator of the picker button component.
   *
   * @return the SmartClient locator of the picker button component.
   */
  @Deprecated
  private String getOpenProcessButtonLocator() {
    return String.format(FORMAT_SMART_CLIENT_LOCATOR_BUTTON_OPEN_PROCESS, getSmartClientLocator());
  }

  /**
   * Get the object string of the selector window.
   *
   * @return the object string of the selector window.
   */
  public String getSelectorWindowObjectString() {
    return String.format(FORMAT_SMART_CLIENT_GET_SELECTOR_WINDOW, objectString);
  }

  /**
   * Open the selector pop up.
   */
  public void openSelectorPopUp() {
    logger.trace("Opening the selector pop up.");
    buttonSearch.click();
  }

  /**
   * Get the target entity of the search item.
   *
   * @return the target entity.
   */
  public String getTargetEntity() {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_TARGET_ENTITY, objectString));
  }

  /**
   * Get the type of the search item.
   *
   * @return the type.
   */
  public String getType() {
    return (String) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_TYPE, objectString));
  }

  /**
   * Enter a text on the combo box of the selector item.
   *
   * @param text
   *          The text to enter.
   */
  public void enterText(String text) {
    selectItem.enterText(text);
  }

  /**
   * Select a record from the selector using the combo box.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void selectFromComboBox(DataObject data) {
    selectItem.selectItem(data);
  }

  /**
   * Set a record from the selector entering the search key on the text field and selecting the
   * value from the list item.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void selectEnteringSearchKey(SelectorDataObject data) {
    enterText((String) data.getSearchKey());
    Sleep.trySleep();
    ListItem listItem = selectItem.getListItem();
    listItem.selectItem(data);
  }

  /**
   * Set a record from the selector entering a search criteria (display value or search key) on the
   * text field and selecting the value from the list item.
   *
   * @param data
   *          The data of the record that will be selected.
   */
  public void selectEnteringSearchCriteria(SelectorDataObject data) {
    if (data.getDisplayValue() == null) {
      enterText((String) data.getSearchKey());
      Sleep.trySleep();
      ListItem listItem = selectItem.getListItem();
      // Take the first item in the shown list, regardless if more than one is displayed; this is
      // done since the search key doesn't necessarily have to match with the list results. It is
      // also a workaround to pressing TAB button, which has been discarded to be used with Selenium
      // due to the data not being filled up correctly.
      // Displays a warning in case there is more than one listed result
      // TODO <selenium waits>: temporarily commenting out code which causes fix in findByScLocator
      // method to fail
      // int itc = listItem.getItemCount();
      int itc = 1;
      listItem.selectItem(0);
      if (itc > 1) {
        logger.info("Selecting first element out of the {} listed results", itc);
      }
    } else {
      enterText((String) data.getDisplayValue());
      Sleep.trySleep();
      ListItem listItem = selectItem.getListItem();
      // TODO <selenium waits>: temporarily commenting out code which causes fix in findByScLocator
      // method to fail
      // int itc = listItem.getItemCount();
      int itc = 1;
      listItem.selectItem(0);
      if (itc > 1) {
        logger.info("Selecting first element out of the {} listed results", itc);
      }
    }
  }

  /**
   * Get the selected text.
   *
   * @return the selected text.
   */
  @Override
  public String getText() {
    return selectItem.getSelectedText();
  }

  /**
   * Check if the Search button (magnifier) of the SelectorItem (object 'this') is present
   *
   * @return true if the Search button is present
   */
  public boolean isButtonSearchPresent() {
    return buttonSearch.isPresent();
  }

}
