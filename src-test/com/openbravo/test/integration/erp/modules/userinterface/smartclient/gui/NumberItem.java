/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2011-2016 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *  Leo Arias <leo.arias@openbravo.com>,
 *  Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openbravo.test.integration.selenium.SeleniumSingleton;

/**
 * Executes actions on OpenbravoERP numeric text boxes built upon SmartClient user interface
 * library.
 *
 * @author elopio
 *
 */
public class NumberItem extends TextItem {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public NumberItem(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the number item, but after this the number item will not
   * be ready to be used. It has to be initialized using the function setSmartClientLocator in order
   * to be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public NumberItem() {
  }

  /**
   * Enter a text on the number item.
   *
   * @param value
   *          The text to enter.
   */
  @Override
  public void setValue(Object value) {
    logger.trace("Entering the text '{}'.", value);
    // TODO Integers are only valid values. But we shouldn't restrict it from here because some
    // tests might require to enter values that are not numeric.
    if (value instanceof String) {
      enterText((String) value);
    } else {
      throw new IllegalArgumentException("Valid values for number items are only Strings.");
    }
  }

  /**
   * Get the value of the number item.
   *
   * @return the value of the text box.
   */
  @Override
  public String getValue() {
    // make sure that the field does not have the focus, so it is properly formatted.
    if (isFocused()) {
      pressTab();
    }
    boolean isCanvasDefined = !(Boolean) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(objectString + ".canvas === undefined");
    // If the object that is going to be considered is OBGridFormLabel class in HTML
    // (OBTruncAddMinusDisplay class) it must have a different treatment. We are not able to
    // retrieve the Sc locator, so a different way to
    // getValue() must be applied.
    if (isCanvasDefined) {
      String objectType = null;
      String value = null;
      try {
        objectType = (String) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(objectString + ".canvas.ID");
        value = (String) SeleniumSingleton.INSTANCE
            .executeScriptWithReturn(objectString + ".canvas.contents");
      } catch (Exception jse) {
        logger
            .warn("There has been a JavaScript problem with the 'executeScriptWithReturn' method");
      }
      if (objectType.contains("OBTruncAddMinusDisplay")) {
        return value;
      }
    }
    return super.getValue();
  }

  /**
   * Check if the text field is empty.
   *
   * @return true if the text field is empty. Otherwise, false.
   */
  @Override
  public boolean isEmpty() {
    return super.getValue().equals("");
  }
}
