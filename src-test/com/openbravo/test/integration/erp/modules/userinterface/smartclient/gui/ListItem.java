/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2010-2015 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Leo Arias <leo.arias@openbravo.com>,
 *   Lorenzo Fidalgo <lorenzo.fidalgo@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;

import com.openbravo.test.integration.erp.data.DataObject;
import com.openbravo.test.integration.erp.data.selectors.SelectorDataObject;
import com.openbravo.test.integration.selenium.SeleniumSingleton;
import com.openbravo.test.integration.selenium.Sleep;
import com.openbravo.test.integration.util.TestUtils;

/**
 * Executes actions on OpenbravoERP list items built upon SmartClient user interface library.
 *
 * @author elopio
 *
 */
public class ListItem extends Canvas {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * Formatting string used to locate items on the list, by its index. The first parameter is the
   * list item locator, the second is the index of the item.
   */
  private static final String FORMAT_LOCATOR_SMART_CLIENT_ITEM_BY_INDEX = "%s/body/row[%d]/col[%s]";
  /**
   * Formatting string used to add the field name to the item locator. The parameter is the name of
   * the field.
   */
  private static final String FORMAT_LOCATOR_PART_SMART_CLIENT_FIELD_NAME = "fieldName=%s";
  /**
   * Formatting string used to make a row visible. The first parameter is the object string. The
   * second parameter is the row number.
   */
  private static final String FORMAT_SMART_CLIENT_MAKE_ROW_VISIBLE = "%s.scrollRecordIntoView(%d)";
  /**
   * Formatting string used to get the number of fields in the list. The parameter is the object
   * string.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELDS_COUNT = "%s.getFields().size()";
  /**
   * Formatting string used to get the name of a field. The first parameter is the object string.
   * The second is the item index.
   */
  private static final String FORMAT_SMART_CLIENT_GET_FIELD_NAME = "%s.getField(%d).name";

  /**
   * Class constructor.
   *
   * @param objectString
   *          JavaScript expression that can be evaluated to get a reference to the SmartClient GUI
   *          object.
   */
  public ListItem(String objectString) {
    super(objectString);
  }

  /**
   * Class constructor.
   *
   * This constructor will just instantiate the list item, but after this the list item will not be
   * ready to be used. It has to be initialized using the function setSmartClientLocator in order to
   * be used with Selenium.
   *
   * This is an alternate way to access the SmartClient object, and should be used only if there is
   * no way to get the object string. Always prefer to call the other constructor with the object
   * string as a parameter, because it will set the SmartClient Locator. With this method the
   * actions that require to call SmartClient functions and attributes directly will not be
   * available.
   */
  public ListItem() {
  }

  /**
   * Get the number of fields in the list item.
   *
   * @return the number of fields in the list item.
   */
  private Long getFieldsCount() {
    return (Long) SeleniumSingleton.INSTANCE
        .executeScriptWithReturn(String.format(FORMAT_SMART_CLIENT_GET_FIELDS_COUNT, objectString));
  }

  /**
   * Get the name of a field.
   *
   * @param index
   *          The index of the field. @return the name of the field.
   */
  private String getFieldName(long index) {
    return (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(
        String.format(FORMAT_SMART_CLIENT_GET_FIELD_NAME, objectString, index));
  }

  /**
   * Check if the list has a field.
   *
   * @param name
   *          The name of the field to check. @return true if the list has the field. Otherwise,
   *          false.
   */
  private boolean hasField(String name) {
    long fieldsCount = getFieldsCount();
    boolean fieldPresent = false;
    for (long index = 0; index < fieldsCount; index++) {
      if (name.equals(getFieldName(index))) {
        fieldPresent = true;
        break;
      }
    }
    return fieldPresent;
  }

  /**
   * Get the SmartClient locator of an item.
   *
   * @param rowIndex
   *          The row index of the item. @return the SmartClient locator of the item.
   */
  private String getListItemLocator(int rowIndex) {
    // When updating from FF19 and Selenium2.33 to FF33 and Selenium2.44 the getSmartClientLocator
    // did not work if the element was not visible, so the makeVisible method has been added,
    this.makeRowVisible(rowIndex);
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_ITEM_BY_INDEX, getSmartClientLocator(),
        rowIndex, 0);
  }

  /**
   * Get the SmartClient locator of an item.
   *
   * @param rowIndex
   *          The row index of the item. @param fieldName The name of the field. @return the
   *          SmartClient locator of the item.
   */
  private String getListItemLocator(int rowIndex, String fieldName) {
    return String.format(FORMAT_LOCATOR_SMART_CLIENT_ITEM_BY_INDEX, getSmartClientLocator(),
        rowIndex, String.format(FORMAT_LOCATOR_PART_SMART_CLIENT_FIELD_NAME,
            TestUtils.convertFieldSeparator(fieldName, false)));
  }

  /**
   * Get the text of an item.
   *
   * @param index
   *          The index of the item.
   *
   * @return the text of the item.
   */
  public String getItemText(int index) {
    // TODO Throw an exception if the element is not present.
    return SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index)).getText();
  }

  /**
   * Get the text of an item.
   *
   * @param index
   *          The index of the item. @param fieldName The name of the field.
   *
   * @return the text of the item.
   */
  public String getItemText(int index, String fieldName) {
    return SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index, fieldName))
        .getText();
  }

  /**
   * Make a row visible.
   *
   * @param rowNumber
   *          The number of the row.
   */
  public void makeRowVisible(int rowNumber) {
    logger.trace("Making visible the row number {}.", rowNumber);
    // TODO L1: Some errors are appearing adding the Business Partner. This sleep tries to fix them
    Sleep.trySleep();
    if (objectString != null) {
      try {
        SeleniumSingleton.INSTANCE.executeScriptWithReturn(
            String.format(FORMAT_SMART_CLIENT_MAKE_ROW_VISIBLE, objectString, rowNumber));
      } catch (Exception e) {
        logger.warn("Error while making row visible, let's try it anyway", e);
      }
    } else {
      // TODO find a way to make it visible.
      logger.error("The only way to make the row visible is with the object string.");
    }
  }

  /**
   * Get the number of items in the list.
   *
   * @return the number of items in the list.
   */
  public int getItemCount() {
    // XXX this steps through all the list of items because some of them might not be loaded.
    int index = 0;
    while (true) {
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index));
        makeRowVisible(index);
        index++;
      } catch (NoSuchElementException exception) {
        break;
      }
    }
    return index;
  }

  /**
   * Select an item from the list.
   *
   * @param index
   *          The index of the item.
   */
  public void selectItem(int index) {
    logger.trace("Selecting the item with index {}.", index);
    makeRowVisible(index);
    try {
      // TODO L1: Some errors are appearing adding the Business Partner. This sleep tries to fix
      // them
      Sleep.trySleep();
      SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index), true).click();
    } catch (StaleElementReferenceException sere) {
      logger.warn("StaleElementReferenceException was thrown");
      // TODO L1: Remove the following static sleep and add a smartSleep
      Sleep.trySleep(4000);
      // TODO L: It is possible the following makeRowVisible(index); is not required. In that case,
      // remove it
      makeRowVisible(index);
      SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index), true).click();
    }
  }

  /**
   * Select an item from the list.
   *
   * @param index
   *          The index of the item.
   *
   * @param fieldName
   *          The name of the field.
   */
  public void selectItem(int index, String fieldName) {
    logger.trace("Selecting the item with index {}.", index);
    makeRowVisible(index);
    SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index, fieldName)).click();
  }

  /**
   * Select an item from the list.
   *
   * @param text
   *          The text displayed on the item.
   */
  public void selectItem(String text) {
    logger.trace("Searching for the item with text '{}'.", text);
    // TODO implement this with an iterator.
    int index = 0;
    boolean present = false;
    int attempt = 0;
    while (attempt < 100 && index < 1_000) {
      try {
        attempt += 1;
        if (getItemText(index).equals(text)) {
          present = true;
          break;
        }
        index++;
        attempt = 0;
      } catch (StaleElementReferenceException | IllegalArgumentException
          | NoSuchElementException e) {
        logger.warn("{} while item, continue anyway", e.getClass()::getSimpleName);
      }
    }

    if (present) {
      selectItem(index);
    } else {
      throw new IllegalArgumentException(
          String.format("The item with text '%s' is not present on the items list.", text));
    }
  }

  /**
   * Select an item from the list.
   *
   * @param text
   *          The text displayed on the item. @param fieldName The name of the field.
   */
  public void selectItem(String text, String fieldName) {
    logger.trace("Searching for the item with text {}.", text);
    // TODO implement this with an iterator.
    int index = 0;
    boolean present = false;
    while (true) {
      try {
        SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index, fieldName));
        // XXX does this work only in combos?
        SeleniumSingleton.INSTANCE.executeScript(
            String.format("window.isc.AutoTest.getLocatorFormItem('%s').pickList.scrollToRow(%d)",
                getSmartClientLocator(), index));
        if (getItemText(index, fieldName).equals(text)) {
          present = true;
          break;
        }
        index++;
      } catch (NoSuchElementException exception) {
        break;
      }
    }
    if (present) {
      selectItem(index, fieldName);
    } else {
      throw new IllegalArgumentException(
          String.format("The item with text %s is not present on the items list.", text));
    }
  }

  /**
   * Get a data object with the fields that are present in the items list and in the data object
   * passed as a parameter.
   *
   * @param data
   *          The data object with fields and values. @return a data object with the fields of the
   *          parameter that are in the items list.
   */
  private DataObject getDataFieldsInListItem(DataObject data) {
    DataObject dataInListItem = new DataObject();
    for (final String fieldName : data.getDataFields().keySet()) {
      if (hasField(fieldName)) {
        dataInListItem.addDataField(fieldName, data.getDataField(fieldName));
      } else {
        logger.warn(
            "Field with name '{}' is not present in the list, and will not be used in the selection.",
            fieldName);
      }
    }
    return dataInListItem;
  }

  /**
   * Select an item from the list.
   *
   * @param data
   *          The object with the data to select.
   */
  public void selectItem(DataObject data) {
    logger.trace("Searching for the item with data '{}'.", data);
    if (getFieldsCount() == 1) {
      if (data instanceof SelectorDataObject) {
        selectItem(((SelectorDataObject) data).getDisplayValue());
      } else {
        selectItem(data.toString());
      }
    } else {
      DataObject dataInListItem = getDataFieldsInListItem(data);
      if (dataInListItem.isEmpty()) {
        throw new IllegalArgumentException(String
            .format("None of the fields in data object '%s' are present in the list item.", data));
      }
      // TODO implement this with an iterator.
      int index = 0;
      boolean present = false;
      while (true) {
        try {
          for (final String fieldName : dataInListItem.getDataFields().keySet()) {
            SeleniumSingleton.INSTANCE.findElementByScLocator(getListItemLocator(index, fieldName));
            // XXX does this work only in combos?
            SeleniumSingleton.INSTANCE.executeScript(String.format(
                "window.isc.AutoTest.getLocatorFormItem('%s').pickList.scrollToRow(%d)",
                getSmartClientLocator(), index));
            if (getItemText(index, fieldName).equals(dataInListItem.getDataField(fieldName))) {
              logger.trace("Found the item with data '{}' on index {}.", dataInListItem, index);
              present = true;
            } else {
              present = false;
              break;
            }
          }
          if (present) {
            break;
          } else {
            index++;
          }
        } catch (NoSuchElementException exception) {
          // End of the list items.
          break;
        }
      }
      if (present) {
        selectItem(index);
      } else {
        throw new IllegalArgumentException(String
            .format("The item with data '%s' is not present on the items list.", dataInListItem));
      }
    }
  }
}
