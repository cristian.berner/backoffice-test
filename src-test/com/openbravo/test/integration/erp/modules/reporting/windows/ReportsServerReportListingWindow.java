/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2020 Openbravo S.L.U.
 * All Rights Reserved.
 ************************************************************************
 */

package com.openbravo.test.integration.erp.modules.reporting.windows;

import com.openbravo.test.integration.erp.modules.client.application.gui.StandardWindow;
import com.openbravo.test.integration.erp.modules.client.application.navigationbarcomponents.gui.Menu;

public class ReportsServerReportListingWindow extends StandardWindow {

  public static final String WINDOW_NAME = "Reports Server - Report Listing";
  private static final String[] MENU_PATH = new String[] { Menu.GENERAL_SETUP, "Reporting",
      WINDOW_NAME };

  public ReportsServerReportListingWindow() {
    super(WINDOW_NAME, MENU_PATH);
  }

  @Override
  public void load() {
    ReportsServerReportListingTab reportListTab = new ReportsServerReportListingTab();
    addTopLevelTab(reportListTab);
    super.load();
  }
}
