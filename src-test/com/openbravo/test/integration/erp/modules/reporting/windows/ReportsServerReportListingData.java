/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2020 Openbravo S.L.U.
 * All Rights Reserved.
 *
 *************************************************************************
 */

package com.openbravo.test.integration.erp.modules.reporting.windows;

import java.util.LinkedHashMap;

import com.openbravo.test.integration.erp.data.DataObject;

public class ReportsServerReportListingData extends DataObject {

  /**
   * Class builder
   *
   * @author plujan
   *
   */
  public static class Builder {

    private LinkedHashMap<String, Object> dataFields = new LinkedHashMap<>();

    public Builder label(String value) {
      this.dataFields.put("label", value);
      return this;
    }

    public Builder description(String value) {
      this.dataFields.put("description", value);
      return this;
    }

    public ReportsServerReportListingData build() {
      return new ReportsServerReportListingData(this);
    }

  }

  private ReportsServerReportListingData(Builder builder) {
    dataFields = builder.dataFields;
  }

}
