/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *   Pablo Luján <plu@openbravo.com>,
 *   Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

/**
 * Enum with the values of the Attributes
 *
 * @author elopio
 *
 */
public enum Attribute {

  /**
   * The name attribute.
   */
  NAME("Name"),
  /**
   * The value attribute.
   */
  VALUE("value"),
  /**
   * The class attribute.
   */
  CLASS("class"),
  /**
   * The src attribute.
   */
  SRC("src"),
  /**
   * The id attribute.
   */
  ID("id"),
  /**
   * The title attribute.
   */
  TITLE("title");

  /** The attribute name. */
  private final String name;

  /**
   * Enum constructor.
   *
   * @param name
   *          the attribute name.
   */
  private Attribute(String name) {
    this.name = name;
  }

  /**
   * Get the attribute locator part. It should be concatenated to a selenium element locator.
   *
   * @return The attribute locator part.
   */
  public String getName() {
    return name;
  }
}
