/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2008-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 * 	Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */

package com.openbravo.test.integration.selenium;

import org.openqa.selenium.WebElement;

/**
 * Class used to access Tables with Selenium
 *
 * @author elopio
 *
 */
public class Tables {

  /** Format string used to access a cell with xpath */
  @SuppressWarnings("unused")
  private static final String FORMAT_LOCATOR_CELL = "%s.%d.%d";
  /** Format string used to click a header with xpath */
  private static final String FORMAT_CLICK_HEADER = "%s/tbody/tr[%d]/th[%d]";
  /** Format string used to click a cell with xpath */
  private static final String FORMAT_CLICK_CELL = "%s/tbody/tr[%d]/td[%d]";

  /**
   * Click a header
   *
   * @param tableLocator
   *          a table locator
   * @param column
   *          the column of the cell, starting from 0
   */
  public static void clickHeader(String tableLocator, int column) {
    SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_CLICK_HEADER, tableLocator, 1, column + 1))
        .click();
  }

  /**
   * Click a cell
   *
   * @param tableLocator
   *          a table locator
   * @param row
   *          the row of the cell, starting from 0
   * @param column
   *          the column of the cell, starting from 0
   */
  public static void clickCell(String tableLocator, int row, int column) {
    SeleniumSingleton.INSTANCE
        .findElementByXPath(String.format(FORMAT_CLICK_CELL, tableLocator, row + 1, column + 1))
        .click();
  }

  /**
   * Click the cell that has the text specified
   *
   * @param tableLocator
   *          a table locator
   * @param column
   *          the column where the text is present
   * @param text
   *          the text of the cell
   */
  public static void clickCellWithText(String tableLocator, int column, String text) {
    for (int i = 0; !getCellText(tableLocator, i, 0).equals(""); i++) {
      if (getCellText(tableLocator, i, column).equals(text)) {
        clickCell(tableLocator, i, column);
        break;
      }
    }
    // TODO throw an exception if the text was not found
  }

  /**
   * Get the text of a cell
   *
   * @param tableLocator
   *          a table locator
   * @param row
   *          the row of the cell
   * @param column
   *          the column of the cell
   * @return the text of the cell
   */
  public static String getCellText(String tableLocator, int row, int column) {
    WebElement table;
    if (tableLocator.substring(0, 2).equals("//")) {
      table = SeleniumSingleton.INSTANCE.findElementByXPath(tableLocator);
    } else {
      table = SeleniumSingleton.INSTANCE.findElementById(tableLocator);
    }
    WebElement rowElement = table.findElements(By.tagName("tr")).get(row);
    return rowElement.findElements(By.tagName("td")).get(column).getText();
  }
}
