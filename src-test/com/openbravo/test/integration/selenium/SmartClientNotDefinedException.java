package com.openbravo.test.integration.selenium;

public class SmartClientNotDefinedException extends UnsupportedOperationException {

  /**
   *
   */
  private static final long serialVersionUID = 8702376897820873442L;

  public SmartClientNotDefinedException() {
    super("The SmartClient the object string is not defined. It must be defined.");
  }

  public SmartClientNotDefinedException(String message) {
    super(message);
  }
}
