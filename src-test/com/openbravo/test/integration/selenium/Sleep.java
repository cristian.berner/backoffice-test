/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo S.L.U.
 * All portions are Copyright (C) 2009-2012 Openbravo S.L.U.
 * All Rights Reserved.
 * Contributor(s):
 *      Leo Arias <leo.arias@openbravo.com>.
 ************************************************************************
 */
package com.openbravo.test.integration.selenium;

import java.time.Duration;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.openbravo.test.integration.erp.modules.client.application.gui.TestRegistry;
import com.openbravo.test.integration.erp.modules.userinterface.smartclient.gui.Button;

/**
 * Class used to send Selenium to sleep for some time.
 *
 * XXX fixed waiting times should be avoided.
 *
 * @author elopio
 *
 */
public class Sleep {

  /** Log4j logger for this class. */
  private static Logger logger = LogManager.getLogger();

  /**
   * The default amount of milliseconds used to send Selenium to sleep for a fixed time.
   */
  private static final int DEFAULT_FIXED_SLEEP_TIME = 3000;

  /**
   * The default amount of milliseconds used to send Selenium to sleep as maximum timeout for
   * dynamic waits.
   */
  private static final int DEFAULT_MAX_TIMEOUT_TIME = 10000;

  /**
   * The default amount of milliseconds for the polling interval used in dynamic waits.
   */
  private static final int DEFAULT_POLLING_INTERVAL_TIME = 100;

  /**
   * Send selenium to sleep for a fixed amount of time.
   */
  public static void trySleep() {
    trySleep(DEFAULT_FIXED_SLEEP_TIME);
  }

  /**
   * Send Selenium to sleep.
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep.
   */
  public static void trySleep(int milliseconds) {
    logger.trace("Sleep for {} milliseconds", milliseconds);
    try {
      Thread.sleep(milliseconds);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Send Selenium to sleep.
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep.
   * @param message
   *          The string that is wanted to appear in the log while sleeping
   */
  public static void trySleepWithMessage(int milliseconds, String message) {
    logger.trace(message);
    try {
      Thread.sleep(milliseconds);
    } catch (final Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param buttonIdentifier
   *          The identifier of the button that must be enabled
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitButtonEnabled(String buttonIdentifier, int milliseconds) {
    Button button;

    try {
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    } catch (WebDriverException wde) {
      Sleep.trySleep();
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    }

    for (int i = 0; i < 20 && !button.isEnabled(); i++) {
      Sleep.trySleep(milliseconds);
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param buttonIdentifier
   *          The identifier of the button that must be disabled
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitButtonDisabled(String buttonIdentifier, int milliseconds) {
    Button button;

    try {
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    } catch (WebDriverException wde) {
      Sleep.trySleep();
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    }

    for (int i = 0; i < 20 && button.isEnabled(); i++) {
      Sleep.trySleep(milliseconds);
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param testRegistry
   *          The complete testRegistry of the button that must be enabled. Example pattern:
   *          dom=window.OB.TestRegistry.registry[
   *          'org.openbravo.client.application.ViewForm_263'].getField('businessPartner').selectorWindow.children[1]
   *          . c h i l d r e n [ 1 ] . c h i l d r e n [ 1 ]
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitButtonEnabledUsingTestRegistry(String testRegistry,
      int milliseconds) {
    Button button;
    button = Sleep.setFluentWait(milliseconds, 6000, WebDriverException.class)
        .until(wd -> new Button(testRegistry));
    for (int i = 0; i < 20 && !button.isEnabled(); i++) {
      Sleep.trySleep(milliseconds);
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param testRegistry
   *          The complete testRegistry of the button that must be enabled. Example pattern:
   *          dom=window.OB.TestRegistry.registry[
   *          'org.openbravo.client.application.ViewForm_263'].getField('businessPartner').selectorWindow.children[1]
   *          . c h i l d r e n [ 1 ] . c h i l d r e n [ 1 ]
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitButtonDisabledUsingTestRegistry(String testRegistry,
      int milliseconds) {
    Button button;
    button = Sleep.setFluentWait(milliseconds, 6000, WebDriverException.class)
        .until(wd -> new Button(testRegistry));
    for (int i = 0; i < 20 && button.isEnabled(); i++) {
      Sleep.trySleep(milliseconds);
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param buttonIdentifier
   *          The identifier of the button that must be visible
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitButtonVisible(String buttonIdentifier, int milliseconds) {
    Button button;

    try {
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    } catch (WebDriverException wde) {
      Sleep.trySleep();
      button = new Button(TestRegistry.getObjectString(buttonIdentifier));
    }

    for (int i = 0; i < 20 && !button.isVisible(); i++) {
      Sleep.trySleep(milliseconds);
    }
  }

  /**
   * Send selenium to sleep.
   *
   * @param condition
   *          The script that must be evaluated to true to continue
   *
   * @param milliseconds
   *          The number of milliseconds for the sleep intervals.
   */
  public static void smartWaitExecuteScript(String condition, int milliseconds) {
    boolean bol = false;
    for (int i = 0; i < 20 && !bol; i++) {
      Sleep.trySleep(milliseconds);
      bol = (Boolean) SeleniumSingleton.INSTANCE.executeScriptWithReturn(condition);
    }
  }

  public static void explicitWaitExecuteScript(String condition) {
    // Polling interval is set to 500ms
    setExplicitWait().until(wd -> SeleniumSingleton.INSTANCE.executeScriptWithReturn(condition));
  }

  /**
   * Fluent Wait for script execution with condition
   *
   * @param condition
   *          The script with the condition that must be evaluated to true to continue
   * @param exceptionToIgnore
   *          The exception to ignore for the fluent wait
   */
  public static void fluentWaitExecuteScript(String condition,
      Class<? extends Throwable> exceptionToIgnore) {
    setFluentWait(exceptionToIgnore)
        .until(wd -> SeleniumSingleton.INSTANCE.executeScriptWithReturn(condition));
  }

  /**
   * Fluent Wait for script execution with condition
   * 
   * @param condition
   *          The script with the condition that must be evaluated to true to continue
   * @param pollInterInMS
   *          The polling interval of the Fluent Wait in milliseconds
   * @param timeoutInMs
   *          The timeout of the Fluent Wait in milliseconds
   */
  public static void fluentWaitExecuteScript(String condition, int pollInterInMS, int timeoutInMs) {
    setFluentWait(pollInterInMS, timeoutInMs)
        .until(wd -> SeleniumSingleton.INSTANCE.executeScriptWithReturn(condition));
  }

  /**
   * Fluent Wait of script execution for string comparison
   * 
   * @param jsScript
   *          The script that returns a the string to be compared
   * @param strComp
   *          The string to be compared with the output string of the JavaScript script
   * @param pollInterInMS
   *          The polling interval of the Fluent Wait in milliseconds
   * @param timeoutInMs
   *          The timeout of the Fluent Wait in milliseconds
   */
  public static void fluentWaitExecuteScriptStringsCompare(String jsScript, String strComp,
      int pollInterInMS, int timeoutInMs) {
    setFluentWait(pollInterInMS, timeoutInMs).until(wd -> {
      String strOfJsScript = (String) SeleniumSingleton.INSTANCE.executeScriptWithReturn(jsScript);
      return strOfJsScript.toLowerCase().contains(strComp.toLowerCase());
    });
  }

  /**
   * Set up an explicit wait to be used in methods
   *
   * @param timeoutInMs
   *          The timeout for the wait in milliseconds
   * @return An explicit wait suitable for any of the SeleniumSingleton.INSTANCE web driver
   *         configurations
   */
  public static WebDriverWait setExplicitWait(int timeoutInMs) {
    return new WebDriverWait(SeleniumSingleton.INSTANCE,
        Duration.ofMillis(timeoutInMs).getSeconds());
  }

  public static WebDriverWait setExplicitWait() {
    return setExplicitWait(DEFAULT_MAX_TIMEOUT_TIME);
  }

  /**
   * Waits for a WebElement to become visible. Can be used to prevent the ElementNotVisible and
   * NoSuchElement exceptions from happening.
   *
   * @param element
   * @param timeoutInMs
   */
  public static void explicitWaitForVisibilityOfElement(WebElement element, int timeoutInMs) {
    // Polling interval is set to 500ms
    setExplicitWait(timeoutInMs).until(ExpectedConditions.visibilityOf(element));
  }

  public static void explicitWaitForVisibilityOfElement(WebElement element) {
    // Polling interval is set to 500ms
    setExplicitWait().until(ExpectedConditions.visibilityOf(element));
  }

  /**
   * Waits for a WebElement to become clickable. Can be used to prevent the ElementNotClickable and
   * NoSuchElement exceptions from happening.
   *
   * @param element
   * @param timeoutInMs
   */
  public static void explicitWaitForElementToBeClickable(WebElement element, int timeoutInMs) {
    // Polling interval is set to 500ms
    setExplicitWait(timeoutInMs).until(ExpectedConditions.elementToBeClickable(element));
  }

  public static void explicitWaitForElementToBeClickable(WebElement element) {
    // Polling interval is set to 500ms
    setExplicitWait().until(ExpectedConditions.elementToBeClickable(element));
  }

  /**
   * Waits until the given condition is met
   *
   * @param condition
   * @param timeoutInMs
   */
  public static void explicitWaitForCondition(boolean condition, int timeoutInMs) {
    // Polling interval is set to 500ms
    setExplicitWait(timeoutInMs).until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        return condition;
      }
    });
  }

  public static void explicitWaitForCondition(boolean condition) {
    // Polling interval is set to 500ms
    setExplicitWait().until(new ExpectedCondition<Boolean>() {
      @Override
      public Boolean apply(WebDriver webDriver) {
        return condition;
      }
    });
  }

  /**
   * Fluent Wait declaration with the ability to throw the exception that led to it timing out,
   * instead of the TimeoutException itself
   *
   * @return A Fluent Wait declaration that doesn't hide the cause and its exceptions that led to
   *         its timeout
   */
  private static FluentWait<RemoteWebDriverInt> fluentWaitWithThrowable() {
    return new FluentWait<RemoteWebDriverInt>(SeleniumSingleton.INSTANCE) {
      @Override
      protected RuntimeException timeoutException(String message, Throwable lastException) {
        logger.error(message);
        throw new WebDriverException(lastException);
      }
    };
  }

  /**
   * Set up a fluent wait to be used in methods
   *
   * @param pollInterInMS
   *          The polling interval of the wait in milliseconds
   * @param timeoutInMs
   *          The timeout for the wait in milliseconds
   * @param exceptionToIgnore
   *          The exception thrown of the actions done to ignore during the wait
   * @return A fluent wait suitable for any of the SeleniumSingleton.INSTANCE web driver
   *         configurations
   */
  public static Wait<RemoteWebDriverInt> setFluentWait(int pollInterInMS, int timeoutInMs,
      Class<? extends Throwable> exceptionToIgnore) {
    return fluentWaitWithThrowable()
        .withTimeout(Duration.ofSeconds(Duration.ofMillis(timeoutInMs).getSeconds()))
        .pollingEvery(Duration.ofMillis(pollInterInMS))
        .ignoring(exceptionToIgnore);
  }

  public static Wait<RemoteWebDriverInt> setFluentWait(
      Class<? extends Throwable> exceptionToIgnore) {
    return setFluentWait(DEFAULT_POLLING_INTERVAL_TIME, DEFAULT_MAX_TIMEOUT_TIME,
        exceptionToIgnore);
  }

  public static Wait<RemoteWebDriverInt> setFluentWait(int pollInterInMS,
      Class<? extends Throwable> exceptionToIgnore) {
    return setFluentWait(pollInterInMS, DEFAULT_MAX_TIMEOUT_TIME, exceptionToIgnore);
  }

  public static Wait<RemoteWebDriverInt> setFluentWait(int pollInterInMS, int timeoutInMs) {
    return fluentWaitWithThrowable()
        .withTimeout(Duration.ofSeconds(Duration.ofMillis(timeoutInMs).getSeconds()))
        .pollingEvery(Duration.ofMillis(pollInterInMS));
  }

  public static Wait<RemoteWebDriverInt> setFluentWait(int pollInterInMS) {
    return setFluentWait(pollInterInMS, DEFAULT_MAX_TIMEOUT_TIME);
  }

  public static Wait<RemoteWebDriverInt> setFluentWait() {
    return setFluentWait(DEFAULT_POLLING_INTERVAL_TIME, DEFAULT_MAX_TIMEOUT_TIME);
  }

  /**
   * Set a Fluent Wait with timeout interval for a WebDriver specific call
   *
   * @param wd
   * @param exceptionToIgnore
   * @param timeoutInMs
   */
  public static WebDriver execSingletonActionWithFWait(WebDriver wd,
      Class<? extends Exception> exceptionToIgnore, int timeoutInMs) {
    return Sleep.setFluentWait(DEFAULT_POLLING_INTERVAL_TIME, timeoutInMs, exceptionToIgnore)
        .until(new Function<WebDriver, WebDriver>() {
          @Override
          public WebDriver apply(WebDriver webDriver) {
            return wd;
          }
        });
  }

  /**
   * Set a Fluent Wait with boolean condition and timeout interval for a WebDriver specific call
   *
   * @param wdWithCondition
   * @param exceptionToIgnore
   * @param timeoutInMs
   */
  public static Boolean execSingletonActionWithFWaitAndCond(Boolean wdWithCondition,
      Class<? extends Exception> exceptionToIgnore, int timeoutInMs) {
    return Sleep.setFluentWait(DEFAULT_POLLING_INTERVAL_TIME, timeoutInMs, exceptionToIgnore)
        .until(new ExpectedCondition<Boolean>() {
          @Override
          public Boolean apply(WebDriver webDriver) {
            return wdWithCondition;
          }
        });
  }
}
