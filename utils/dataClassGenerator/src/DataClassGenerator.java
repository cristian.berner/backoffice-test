import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataClassGenerator {

  private String fileName = "";
  private DocumentBuilderFactory dbf = null;
  private DocumentBuilder db = null;
  private Document doc = null;
  private String className = "";
  private List<Field> fl = new ArrayList<Field>();

  public DataClassGenerator(String fileName) {
    this.fileName = fileName;
    this.className = getClassFromFile(fileName);
    dbf = DocumentBuilderFactory.newInstance();
    try {
      db = dbf.newDocumentBuilder();
    } catch (final ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    try {
      doc = db.parse(fileName);
    } catch (final SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    doc.getDocumentElement().normalize();
  }

  private String getClassFromFile(String fileName) {
    final String result = fileName.substring(fileName.lastIndexOf("\\") + 1, fileName
        .lastIndexOf("."));
    return result;
  }

  public String generate() {
    String result = "";
    fl = this.getFieldList();
    result = result + this.getHeader();
    result = result + this.getPackageName();
    result = result + this.getClassName();
    result = result + this.getFormatString();
    result = result + this.getDataFields();
    result = result + this.getBuilderClass();
    result = result + this.getBuilderFieldsClass();
    result = result + this.getSetters();
    result = result + this.getRequiredFields();
    result = result + this.getBuild();
    result = result + this.getClassConstructor();
    result = result + "\n}\n";
    // result = result + this.getGetters();
    // result = result + this.getToString();
    System.out.println(result);
    return result;
  }

  private List<Field> getFieldList() {
    final List<Field> result = new ArrayList<Field>();

    final NodeList nodeLst = doc.getElementsByTagName("field");

    for (int s = 0; s < nodeLst.getLength(); s++) {

      final Node fstNode = nodeLst.item(s);

      if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
        final Element fstElmnt = (Element) fstNode;
        final NodeList descElmntLst = fstElmnt.getElementsByTagName("description");
        final Element descElmnt = (Element) descElmntLst.item(0);
        final NodeList desc = descElmnt.getChildNodes();
        // System.out.println("Description  : " + ((Node) desc.item(0)).getNodeValue());
        final NodeList typeElmntLst = fstElmnt.getElementsByTagName("type");
        final Element typeElmnt = (Element) typeElmntLst.item(0);
        final NodeList type = typeElmnt.getChildNodes();
        // System.out.println("Type : " + ((Node) type.item(0)).getNodeValue());
        final NodeList nameElmntLst = fstElmnt.getElementsByTagName("name");
        final Element nameElmnt = (Element) nameElmntLst.item(0);
        final NodeList name = nameElmnt.getChildNodes();
        // System.out.println("Name : " + ((Node) name.item(0)).getNodeValue());

        final Field f = new Field((desc.item(0)).getNodeValue(), (type.item(0)).getNodeValue(),
            (name.item(0)).getNodeValue());
        result.add(f);
      }
    }
    // System.out.println(result.toString());

    return result;
  }

  private String getHeader() {
    String result = "/*\n";
    result = result
        + " *************************************************************************\n";
    result = result
        + " * The contents of this file are subject to the Openbravo  Public  License\n";
    result = result
        + " * Version  1.0  (the  \"License\"),  being   the  Mozilla   Public  License\n";
    result = result
        + " * Version 1.1  with a permitted attribution clause; you may not  use this\n";
    result = result
        + " * file except in compliance with the License. You  may  obtain  a copy of\n";
    result = result + " * the License at http://www.openbravo.com/legal/license.html\n";
    result = result
        + " * Software distributed under the License  is  distributed  on  an \"AS IS\"\n";
    result = result
        + " * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the\n";
    result = result
        + " * License for the specific  language  governing  rights  and  limitations\n";
    result = result + " * under the License.\n";
    result = result + " * The Original Code is Openbravo ERP.\n";
    result = result + " * The Initial Developer of the Original Code is Openbravo S.L.U. \n";
    result = result + " * All portions are Copyright (C) 2010 Openbravo S.L.U.\n";
    result = result + " * All Rights Reserved.\n";
    result = result + " * \n";
    result = result + " * This class was automatically generated on " + now() + "\n";
    result = result + " * Contributor(s):\n";
    result = result
        + " *************************************************************************\n";
    result = result + " */\n";
    result = result + "\n";
    return result;
  }

  private String now() {
    final Calendar cal = Calendar.getInstance();
    final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    return sdf.format(cal.getTime());
  }

  private String getPackageName() {
    String result = "";
    final NodeList packageName = doc.getElementsByTagName("packageName").item(0).getChildNodes();
    result = "package " + (packageName.item(0)).getNodeValue() + ";\n";
    // System.out.println(result);
    return result;
  }

  private String getClassName() {
    String result = "";
    // final NodeList className = doc.getElementsByTagName("className").item(0).getChildNodes();
    result = result + "\n";
    result = result + "/**\n";
    result = result + " *\n";
    result = result + " * @author plujan\n";
    result = result + " *\n";
    result = result + "*/\n";
    // result = result + "public class " + (className.item(0)).getNodeValue() + " {\n";
    // La siguiente linea reemplaza a la anterior para usar la variable local
    result = result + "public class " + className + " {\n";
    result = result + "\n";
    // System.out.println(result);
    return result;
  }

  private String getFormatString() {
    final String result = "";
    // NodeList packageName=doc.getElementsByTagName("className").item(0).getChildNodes();
    // result = result +
    // "/** Formatting string used to get a string representation of this object. */\n";
    // result = result + "private static final String FORMAT_STRING_REPRESENTATION = \"[";
    // result = result + ((Node) packageName.item(0)).getNodeValue()+"\n";
    // for(int i=0;i<fl.size();i++) {
    // result = result + fl.get(i).getDescription() + ": ";
    // result = result + fl.get(i).getName() + "=%s,\n ";
    // }
    // result = result + "]\";\n";
    return result;
  }

  private String getDataFields() {
    String result = "";
    result = result + "/*Data fields */\n";
    for (int i = 0; i < fl.size(); i++) {
      result = result + "/** " + fl.get(i).getDescription() + " */\n";
      result = result + "private " + fl.get(i).getType() + " " + fl.get(i).getName() + ";\n";
    }
    result = result + "\n";
    return result;
  }

  private String getBuilderClass() {
    String result = "";
    result = result + "/**\n * Class builder\n *\n * @author plujan\n *\n */\n";
    result = result + "public static class Builder {\n";
    result = result + "\n";
    return result;
  }

  private String getBuilderFieldsClass() {
    String result = "";
    result = result + "/*Builder fields */\n";
    for (int i = 0; i < fl.size(); i++) {
      result = result + "/** " + fl.get(i).getDescription() + " */\n";
      result = result + "private " + fl.get(i).getType() + " " + fl.get(i).getName() + "=null;\n";
    }
    result = result + "\n";
    return result;
  }

  private String getSetters() {
    String result = "";
    for (int i = 0; i < fl.size(); i++) {
      result = result + "/** Set the " + fl.get(i).getDescription() + " value.\n";
      result = result + " *\n";
      result = result + " * @param value\n * The " + fl.get(i).getDescription() + " value.\n";
      result = result + " * @return The builder for this class.\n";
      result = result + " */\n";
      result = result + "public Builder " + fl.get(i).getName() + "(";
      result = result + fl.get(i).getType() + " value) {\n";
      result = result + "this." + fl.get(i).getName() + "=value;\n";
      result = result + "return this;\n}\n";
    }
    result = result + "\n";
    return result;
  }

  private String getRequiredFields() {
    String result = "";
    result = result + "/** \n * Set the fields for this object that are required on the ERP.\n";
    result = result + " *\n";
    for (int i = 0; i < fl.size(); i++) {
      result = result + " * @param " + fl.get(i).getName() + "\n * The "
          + fl.get(i).getDescription() + " value.\n";
    }
    result = result + " * @return The builder for this class\n";
    result = result + " */\n";

    result = result + "public Builder requiredFields(";
    for (int i = 0; i < fl.size(); i++) {
      result = result + fl.get(i).getType() + " " + fl.get(i).getName() + ",";
    }
    result = result.substring(0, result.lastIndexOf(",")) + ") {\n";
    for (int i = 0; i < fl.size(); i++) {
      result = result + "this." + fl.get(i).getName() + " = " + fl.get(i).getName() + ";\n";
    }
    result = result + "return this;\n}\n";

    result = result + "\n";
    return result;
  }

  private String getBuild() {
    String result = "";
    result = result + "/**\n * Build the data object.\n";
    result = result + " *\n";
    result = result + " * @return The data object\n";
    result = result + " */\n";
    // final NodeList className = doc.getElementsByTagName("className").item(0).getChildNodes();
    // result = result + "public " + (className.item(0)).getNodeValue() + " build() {\n";
    // La siguiente linea reemplaza a la anterior para usar la variable local
    result = result + "public " + className + " build() {\n";
    // result = result + "return new " + (className.item(0)).getNodeValue() + "(this);\n}\n";
    // La siguiente linea reemplaza a la anterior para usar la variable local
    result = result + "return new " + className + "(this);\n}\n";
    result = result + "\n}\n";
    result = result + "\n";
    return result;
  }

  private String getClassConstructor() {
    String result = "";
    result = result + "/**\n * Build the data object.\n";
    result = result + " *\n";
    result = result + " * @param builder\n * The object builder\n";
    result = result + " */\n";
    // final NodeList className = doc.getElementsByTagName("className").item(0).getChildNodes();
    // La siguiente linea reemplaza a la anterior para usar la variable local
    result = result + "private " + className + "(Builder builder) {\n";
    for (int i = 0; i < fl.size(); i++) {
      result = result + fl.get(i).getName() + " = " + "builder." + fl.get(i).getName() + ";\n";
    }
    result = result + "\n}\n";
    result = result + "\n";
    return result;
  }
}
