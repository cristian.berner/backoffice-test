public class Field {
  private String description = "";
  private String type = "";
  private String name = "";
  private String locator = "";

  public Field(String description, String type, String name) {
    this.description = description;
    this.type = type;
    this.name = name;
    // System.out.println(description+type+name);
  }

  public Field(String description, String type, String name, String locator) {
    this.description = description;
    this.type = type;
    this.name = name;
    this.locator = locator;
    // System.out.println(description+type+name);
  }

  public String getDescription() {
    return description;
  }

  public String getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public String getLocator() {
    return locator;
  }
}
