declare 
  v_Seed  varchar(32);
  
begin
  select SYS_GUID() into v_Seed from dual;

--AD_Client.name, AD_Client.value, AD_Client.description
update ad_client set name=substr(concat(name,v_Seed),1,60),
  value=substr(concat(value,v_Seed),1,40), 
  description=concat(description,v_Seed)
where name='SampleClient';

--AD_User.name, AD_User.description, AD_User.username
update ad_user set name=substr(concat(name,v_Seed),1,40), 
  description=substr(concat(description,v_Seed),1,40),
  username=substr(concat(username,v_Seed),1,40)
where username='SampleClientAdmin' or username='userA' or username='userB' or username='SampleClientUser';

--AD_Role.name, AD_Role.description
update ad_role set name=substr(concat(name,v_Seed),1,40), 
  description=substr(concat(description,v_Seed),1,40)
where name='SampleClient Admin' or name='SampleClient User';
  
end;



